﻿using Data;
using Entities;
using System.Data;
namespace Business
{
    public class BLActive
    {
        public bool Grabar(BEActive oDato, bool bNuevo, int Empresa, ref string cMensaje)
        {
            DAActive objDatos = new DAActive();
            return objDatos.Grabar(oDato, bNuevo, Empresa,ref cMensaje);
        }
        public DataTable Listar(string Valor, int CodigoAgencia, int CodigoEmpresa)
        {
            DAActive objDatos = new DAActive();
            return objDatos.Listar(Valor, CodigoAgencia, CodigoEmpresa);
        }
        public bool Eliminar(BEActive oDato, ref string cMensaje)
        {
            DAActive objDatos = new DAActive();
            return objDatos.Eliminar(oDato, ref cMensaje);
        }
        public bool AgregarActivoCompra(int Codigo, int CodigoActivo, decimal Cantidad, decimal Precio,   ref string cMensaje)
        {
            DAActive objDatos = new DAActive();
            return objDatos.AgregarActivoCompra(Codigo, CodigoActivo, Cantidad, Precio, ref cMensaje);
        }
        public bool EliminarActivoCompra(int Codigo, int CodigoActivo, ref string cMensaje)
        {
            DAActive objDatos = new DAActive();
            return objDatos.EliminarActivoCompra(Codigo, CodigoActivo, ref cMensaje);
        }
        public bool ModificarActivoCompra(int Codigo, int CodigoActivo, decimal Cantidad, decimal Precio, ref string cMensaje)
        {
            DAActive objDatos = new DAActive();
            return objDatos.ModificarActivoCompra(Codigo, CodigoActivo, Cantidad, Precio, ref cMensaje);
        }
        public bool AgregarActivoAlmacen(int Codigo, int CodigoActivo, decimal Cantidad, decimal Precio,   ref string cMensaje)
        {
            DAActive objDatos = new DAActive();
            return objDatos.AgregarActivoAlmacen(Codigo, CodigoActivo, Cantidad, Precio, ref cMensaje);
        }
        public bool EliminarActivoAlmacen(int Codigo, int CodigoActivo, ref string cMensaje)
        {
            DAActive objDatos = new DAActive();
            return objDatos.EliminarActivoAlmacen(Codigo, CodigoActivo, ref cMensaje);
        }
        public bool ModificarActivoAlmacen(int Codigo, int CodigoActivo, decimal Cantidad, decimal CantidadAnt, decimal Precio, ref string cMensaje)
        {
            DAActive objDatos = new DAActive();
            return objDatos.ModificarActivoAlmacen(Codigo, CodigoActivo, Cantidad, CantidadAnt, Precio, ref cMensaje);
        }
    }
}
