﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Data;

namespace Business
{
   public class BLCommons
    {
        public DataTable MostrarMaestroMantenimiento()
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarMaestroMantenimiento();
        }
        public DataTable MostrarMaestroMantenimientoValores(string Codigo)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarMaestroMantenimientoValores(Codigo);
        }
        public DataTable MostrarMaestro(string Codigo)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarMaestro(Codigo);
        }
        public DataTable MostrarVendedores()
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarVendedores();
        }
        public DataTable MostrarDireccionesCliente(int Codigo)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarDireccionesCliente(Codigo);
        }
        public DataTable MostrarMaestroUnico(int Codigo, string Valor)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarMaestroUnico(Codigo, Valor);
        }
        public DataTable MostrarAgencias(bool Todo, int CodigoEmpresa)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarAgencias(Todo, CodigoEmpresa);
        }
        public DataTable RecuperUsuariosAcceso()
        {
            DACommons objDatos = new DACommons();
            return objDatos.RecuperUsuariosAcceso();
        }
        public DataTable MostrarSeries(string TipoDocumento, int CodigoAgencia, int CodigoEmpresa)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarSeries(TipoDocumento, CodigoAgencia, CodigoEmpresa);
        }
        public DataTable MostrarDepartamento()
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarDepartamento();
        }
        public DataTable MostrarProvincia(string cCodUbi)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarProvincia(cCodUbi);
        }
        public DataTable MostrarDistrito(string cCodUbi)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarDistrito(cCodUbi);
        }
        public DataTable RecuperarDetallePagos(int Codigo, int Tipo)
        {
            DACommons objDatos = new DACommons();
            return objDatos.RecuperarDetallePagos(Codigo, Tipo);
        }
        public DataTable MostrarMovimientos()
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarMovimientos();
        }
        public DataTable MostrarEmpresas(  bool  Todos)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarEmpresas(Todos);
        }
        public DataTable MostrarAgenciasCaja(DateTime Fecha, int CodigoEmpresa)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarAgenciasCaja(Fecha, CodigoEmpresa);
        }
        public DataTable MostrarUsuariosCaja(DateTime Fecha, int Agencia, int CodigoEmpresa)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarUsuariosCaja(Fecha, Agencia, CodigoEmpresa);
        }
        public DataTable MostrarMarca()
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarMarca();
        }
        public DataTable MostrarPersonal()
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarPersonal();
        }
        public DataTable MostrarMoneda()
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarMoneda();
        }
        public DataTable RecuperarComprobantesCreditoBloque(int Cliente, int Moneda, int Empresa)
        {
            DACommons objDatos = new DACommons();
            return objDatos.RecuperarComprobantesCreditoBloque(Cliente, Moneda, Empresa);
        }
        public bool EliminarDetallePagos(int CodigoPago, int TipoPago, ref string cMensaje)
        {
            DACommons objDatos = new DACommons();
            return objDatos.EliminarDetallePagos(CodigoPago, TipoPago, ref cMensaje);
        }
        public bool RegistrarPago(int Codigo, decimal Monto, int Tipo, string Usuario, int MedioPago, DateTime Fecha,
            string NumOpe, int CodigoAgencia, int ModalidadPago, int Moneda, int CodBanco, decimal TipoCambio,
            int CodigoEmpresa, ref string cMensaje)
        {
            DACommons objDatos = new DACommons();
            return objDatos.RegistrarPago( Codigo,  Monto,  Tipo,  Usuario,  MedioPago,  Fecha,  NumOpe,  CodigoAgencia,  ModalidadPago,  Moneda, 
                 CodBanco,  TipoCambio, CodigoEmpresa, ref  cMensaje);
        }
        public bool EliminarMaestro(int Codigo, string Valor, ref string cMensaje)
        {
            DACommons objDatos = new DACommons();
            return objDatos.EliminarMaestro(Codigo, Valor, ref cMensaje);
        }
        public bool GrabarMaestro(int Codigo, string ValorAnterior, string Valor, string Descripcion, bool Nuevo, ref string cMensaje)
        {
            DACommons objDatos = new DACommons();
            return objDatos.GrabarMaestro(Codigo, ValorAnterior, Valor, Descripcion, Nuevo,ref cMensaje);
        }
        public DataTable MostrarBaseDatos()
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarBaseDatos( );
        }
        public DataTable MostrarAñoContable(int CodigoEmpresa)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarAñoContable(CodigoEmpresa);
        }
        public DataTable MostrarMesContable(int Anio, int CodigoEmpresa)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarMesContable(Anio, CodigoEmpresa);
        }
        public bool ValidaEstadoMesContable(int Codigo, DateTime Fecha, int TipoModulo, ref string cMensaje)
        {
            DACommons objDatos = new DACommons();
            return objDatos.ValidaEstadoMesContable(Codigo, Fecha, TipoModulo, ref cMensaje);
        }
        public bool AperturaCierreContable(int CodigoEmpresa, int Anio, int Mes, bool Estado, ref string cMensaje)
        {
            DACommons objDatos = new DACommons();
            return objDatos.AperturaCierreContable(CodigoEmpresa, Anio, Mes, Estado,ref cMensaje);
        }
        public bool EnvioComprobantesOffLine(int Codigo, int TipoComprobante , ref string cMensaje)
        {
            DACommons objDatos = new DACommons();
            return objDatos.EnvioComprobantesOffLine(Codigo, TipoComprobante,ref cMensaje);
        }
        public DataTable MostrarFamilia( int Tipo)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarFamilia(Tipo);
        }
        public DataTable MostrarSubFamilia(string Familia)
        {
            DACommons objDatos = new DACommons();
            return objDatos.MostrarSubFamilia(Familia);
        }
    }
}
