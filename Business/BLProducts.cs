﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Data;
using Entidades;

namespace Business
{
   public class BLProducts
    {
        public DataTable ListarProductos(string Valor, int nTipoBusqueda, int nCodigoAgencia, int CodigoEmpresa,string Familia)
        {
            DAProducts obj = new DAProducts();
            return obj.ListarProductos(Valor, nTipoBusqueda, nCodigoAgencia,  CodigoEmpresa, Familia);
        }
        public DataTable ListarProductosMantenimiento(string Valor, int nTipoBusqueda)
        {
            DAProducts obj = new DAProducts();
            return obj.ListarProductosMantenimiento(Valor, nTipoBusqueda);
        }
        public DataTable ObtenerProducto(string Valor, int nTipoBusqueda, int nCodigoAgencia, int CodigoEmpresa)
        {
            DAProducts obj = new DAProducts();
            return obj.ObtenerProducto(Valor, nTipoBusqueda, nCodigoAgencia, CodigoEmpresa);
        }
        public DataTable ListarStockAlamcen(int CodProducto, int CodigoAgencia, int CodigoEmpresa)
        {
            DAProducts obj = new DAProducts();
            return obj.ListarStockAlamcen(CodProducto, CodigoAgencia, CodigoEmpresa);
        }
        public DataTable ListarInventarioProductos(string Valor, int nTipoBusqueda, bool Descuadre,
                                           int CodigoAgencia, int CodigoEmpresa, string CodigoUsuario)
        {
            DAProducts obj = new DAProducts();
            return obj.ListarInventarioProductos(Valor, nTipoBusqueda, Descuadre, CodigoAgencia, CodigoEmpresa, CodigoUsuario);
        }
        public DataTable ListarInventarioDescuadre(string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa)
        {
            DAProducts obj = new DAProducts();
            return obj.ListarInventarioDescuadre(CodigoUsuario, CodigoAgencia, CodigoEmpresa );
        }
        public bool GrabarProducto(BEProductos oDato, bool bNuevo, ref string cMensaje)
        {
            DAProducts obj = new DAProducts();
            return obj.GrabarProducto(oDato, bNuevo, ref  cMensaje);
        }
        public bool EliminarProducto(BEProductos oDato, ref string cMensaje)
        {
            DAProducts obj = new DAProducts();
            return obj.EliminarProducto(oDato,  ref cMensaje);
        }
        public bool GrabarProductoPrecio(BEProductos oDato, int Empresa, ref string cMensaje)
        {
            DAProducts obj = new DAProducts();
            return obj.GrabarProductoPrecio(oDato, Empresa, ref cMensaje);
        }
        public bool AgregarCuadreInventario(int nCodProd, string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa,
            decimal nStockFisico, decimal nDiferencia, ref string cMensaje)
        {
            DAProducts obj = new DAProducts();
            return obj.AgregarCuadreInventario(nCodProd, CodigoUsuario, CodigoAgencia, CodigoEmpresa,
                nStockFisico, nDiferencia , ref cMensaje);
        }
        public bool QuitarCuadreInventario(int nCodProd, string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa,
         ref string cMensaje)
        {
            DAProducts obj = new DAProducts();
            return obj.QuitarCuadreInventario(nCodProd, CodigoUsuario, CodigoAgencia, CodigoEmpresa, ref cMensaje);
        }
        public bool GenerarCuadre(string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa, string cCodPer, string cNumDoc,
            DateTime dFecReg, ref string cMensaje)
        {
            DAProducts obj = new DAProducts();
            return obj.GenerarCuadre(CodigoUsuario, CodigoAgencia, CodigoEmpresa, cCodPer, cNumDoc, dFecReg,ref cMensaje);
        }
        public bool UnificarProductos(string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa, int CodigoProducto,
            ref string cMensaje)
        {
            DAProducts obj = new DAProducts();
            return obj.UnificarProductos(CodigoUsuario, CodigoAgencia, CodigoEmpresa, CodigoProducto, ref cMensaje);
        }
        public bool ReprocesarStock(int CodigoProducto, int CodigoAgencia, int CodigoEmpresa, ref string cMensaje)
        {
            DAProducts obj = new DAProducts();
            return obj.ReprocesarStock(CodigoProducto, CodigoAgencia, CodigoEmpresa, ref cMensaje);
        }
    }
}
