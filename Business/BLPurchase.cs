﻿using System;
using Data;
using Entities;
using System.Data;

namespace Business
{
    public class BLPurchase
    {
        public bool Registrar(ref BEPurchase oDato, ref string cMensaje)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.Registrar(ref oDato,ref cMensaje);
        }
        public bool Anular(BEPurchase oDato, ref string cMensaje)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.Anular(oDato, ref cMensaje);
        }
        public bool Facturar(BEPurchase oDato, ref string cMensaje)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.Facturar(oDato, ref  cMensaje);
        }
        public DataTable Listar(DateTime dFecIni, DateTime dFecFin, string Valor, int nTipoBusqueda, int CodigoEmpresa, bool Servicio)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.Listar(dFecIni, dFecFin, Valor, nTipoBusqueda, CodigoEmpresa, Servicio);
        }
        public bool AgregarProducto(int Codigo, int CodigoProducto, decimal Cantidad, decimal Precio, ref string cMensaje)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.AgregarProducto(Codigo, CodigoProducto, Cantidad, Precio,ref cMensaje);
        }
        public bool EliminarProducto(int Codigo, int CodigoProducto, ref string cMensaje)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.EliminarProducto(Codigo, CodigoProducto, ref cMensaje);
        }
        public bool ModificarProducto(int Codigo, int CodigoProducto, decimal Cantidad, decimal CantidadAnt, decimal Precio,
            ref string cMensaje)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.ModificarProducto(Codigo, CodigoProducto, Cantidad, CantidadAnt, Precio,ref cMensaje);
        }
        public DataTable RecuperarDetalle(int Codigo)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.RecuperarDetalle(Codigo);
        }
        public DataTable ListarCreditos(string Valor, int nTipoBusqueda, bool Pagado, int CodigoEmpresa)
        {
            DAPurchase objDatos = new DAPurchase();
            return objDatos.ListarCreditos(Valor, nTipoBusqueda, Pagado, CodigoEmpresa);
        }
    }
}
