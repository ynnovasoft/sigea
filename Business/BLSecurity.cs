﻿using System.Data;
using Data;

namespace Business
{
    public class BLSecurity
    {
        public DataTable RecuperaDatosLogueo(string cUsuario, int CodigoAgencia, int CodigoEmpresa, string CodigoPersonal)
        {
            DASecurity objDatos = new DASecurity();
            return objDatos.RecuperaDatosLogueo(cUsuario, CodigoAgencia, CodigoEmpresa, CodigoPersonal);
        }
        public DataTable RecuperaMenu(int CodigoUsuario, int CodigoAgencia, int CodigoEmpresa)
        {
            DASecurity objDatos = new DASecurity();
            return objDatos.RecuperaMenu(CodigoUsuario, CodigoAgencia, CodigoEmpresa);
        }
        public bool RegistraPermisos(DataTable Dato, int CodigoUsuario, int CodigoAgencia, int CodigoEmpresa, ref string cMensaje)
        {
            DASecurity objDatos = new DASecurity();
            return objDatos.RegistraPermisos(Dato, CodigoUsuario, CodigoAgencia, CodigoEmpresa, ref cMensaje);
        }
        public bool ValidaIngresoSistema(string cUsuario, string cPassword, int CodigoAgencia,
                                    int CodigoEmpresa, ref int Respuesta, ref string cMensaje)
        {
            DASecurity objDatos = new DASecurity();
            return objDatos.ValidaIngresoSistema(cUsuario, cPassword, CodigoAgencia, CodigoEmpresa, ref Respuesta, ref cMensaje);
        }
    }
}
