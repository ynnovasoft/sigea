﻿using System.Data;
using Entities;
using Data;
namespace Business
{
   public class BLSeries
    {
        public DataTable Listar(int CodigoEmpresa)
        {
            DASeries objDatos = new DASeries();
            return objDatos.Listar(CodigoEmpresa);
        }
        public bool Grabar(BESerie oDato, bool bNuevo, ref string cMensaje)
        {
            DASeries objDatos = new DASeries();
            return objDatos.Grabar(oDato, bNuevo, ref cMensaje);
        }
        public bool Eliminar(BESerie oDato, ref string cMensaje)
        {
            DASeries objDatos = new DASeries();
            return objDatos.Eliminar(oDato,ref cMensaje);
        }
    }
}
