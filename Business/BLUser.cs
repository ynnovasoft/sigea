﻿using System.Data;
using Entities;
using Data;

namespace Business
{
    public class BLUser
    {
        public DataTable Listar()
        {
            DAUser objDatos = new DAUser();
            return objDatos.Listar();
        }
        public bool Grabar(BEUser oDato, bool bNuevo, ref string cMensaje)
        {
            DAUser objDatos = new DAUser();
            return objDatos.Grabar(oDato, bNuevo, ref cMensaje);
        }
        public bool Eliminar(BEUser oDato, ref string cMensaje)
        {
            DAUser objDatos = new DAUser();
            return objDatos.Eliminar(oDato, ref cMensaje);
        }
    }
}
