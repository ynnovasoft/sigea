﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ConsultaDatosSunat
{
    public class BEClienteDNISunat
    {
        public string nombres { get; set; }
        public string tipoDocumento { get; set; }
        public string numeroDocumento { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }

    }
}
