﻿using System.IO;
using System.Net;
using Newtonsoft.Json;
using Entities;
using System.Configuration;

namespace DASunat.ConsultaDatosSunat
{
    public class ConsultaDatosSunat
    {
        public string ConsultarRUC(ref BECustomer Cliente, string Documento)
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var url = ConfigurationManager.AppSettings["UrlApiConsultaRuc"] + Documento ;
            var token = ConfigurationManager.AppSettings["TokenConsulta"];      
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.Headers.Add("Authorization: Bearer " + token);
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return "";
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            BEClienteRUCSunat oDato = JsonConvert.DeserializeObject<BEClienteRUCSunat>(responseBody);
                            Cliente.cNumDoc = oDato.numeroDocumento;
                            Cliente.cRazSoc = oDato.razonSocial;
                            Cliente.cDirCli = oDato.direccion +" "+ oDato.departamento + " - " + oDato.provincia + " - " + oDato.distrito;
                            if(Documento.Substring(0, 2)=="10")
                            {
                                Cliente.cCodUbiDep = "";
                                Cliente.cCodUbiProv = "";
                                Cliente.cCodUbiDis = "";
                            }
                            else
                            {
                                Cliente.cCodUbiDep = oDato.ubigeo.Substring(0, 2);
                                Cliente.cCodUbiProv = oDato.ubigeo.Substring(0, 4);
                                Cliente.cCodUbiDis = oDato.ubigeo;
                            }                          
                            Cliente.cTipDocCli = oDato.tipoDocumento;
                            if (oDato.EsAgenteRetencion == "true")
                            {
                                Cliente.bAgeRet =true;
                            }
                            else
                            {
                                Cliente.bAgeRet = false;
                            }                       
                            if (oDato.estado != "ACTIVO")
                            {
                                return "El contribuyente se encuentra en estado: " + oDato.estado;
                            }
                        }
                    }
                }
                return "";
            }
            catch (WebException ex)
            {
                return "DASunat-ConsultaDatosSunat MetodoEjecutado: ConsultarRUC - " + ex.Message;
            }        
        }
        public string ConsultarDNI(ref BECustomer Cliente, string Documento)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var url = ConfigurationManager.AppSettings["UrlApiConsultaDni"] + Documento;
            var token = ConfigurationManager.AppSettings["TokenConsulta"];
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.Headers.Add("Authorization: Bearer " + token);
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return "";
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            BEClienteDNISunat oDato = JsonConvert.DeserializeObject<BEClienteDNISunat>(responseBody);
                            Cliente.cNumDoc = oDato.numeroDocumento;
                            Cliente.cRazSoc = oDato.nombres +" "+ oDato.apellidoPaterno + " " + oDato.apellidoMaterno;
                            Cliente.cNombres = oDato.nombres;
                            Cliente.cApePat = oDato.apellidoPaterno;
                            Cliente.cApeMat = oDato.apellidoMaterno;
                            Cliente.cTipDocCli = oDato.tipoDocumento;
                            Cliente.cCodUbiDep = "";
                            Cliente.cCodUbiProv = "";
                            Cliente.cCodUbiDis = "";
                        }
                    }
                }
                return "";
            }
            catch (WebException ex)
            {
                return "DASunat-ConsultaDatosSunat MetodoEjecutado: ConsultarDNI - " + ex.Message;
            }
        }
        public string ConsultarTipoCambio( ref  Entities.BETipoCambio Datos, string Fecha)
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var url = ConfigurationManager.AppSettings["UrlApiConsultaTipoCambio"] + Fecha;
            var token = ConfigurationManager.AppSettings["TokenConsulta"];
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.Headers.Add("Authorization: Bearer " + token);
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return "";
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            Datos  = JsonConvert.DeserializeObject<Entities.BETipoCambio>(responseBody);
                        }
                    }
                }
                return "";
            }
            catch (WebException ex)
            {
                return "DASunat-ConsultaDatosSunat MetodoEjecutado: ConsultarTipoCambio - " + ex.Message;
            }
        }
    }
}


