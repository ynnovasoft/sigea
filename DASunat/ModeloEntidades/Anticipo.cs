﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ModeloEntidades
{
   public class Anticipo
    {
        public string NumeroComprobanteAnticipo { get; set; }
        public decimal ImporteTotalAnticipo { get; set; }
        public string CodigoTipoMoneda { get; set; }
    }
}
