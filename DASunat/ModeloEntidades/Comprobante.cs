﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ModeloEntidades
{
    public class Comprobante
    {
        public Comprobante()
        {
            this.Detalle = new List<Detalle>();
        }
        public string SerieNumeroComprobante { get; set; }
        public string FechaEmision { get; set; }
        public string FechaVencimiento { get; set; }
        public string HoraEmision { get; set; }
        public string CodigoTipoOperacion { get; set; }
        public string MontoEnLetras { get; set; }
        public string CodigoTipoMoneda { get; set; }
        public string CodigoTipoComprobante { get; set; }
        public string NumeroOrdenCompra { get; set; }
        public string NumeroDocumentoEmisor { get; set; }
        public string CodigoFiscalEmisor { get; set; }
        public string TipoDocumentoEmisor { get; set; }
        public string NombreComercialEmisor { get; set; }
        public string DireccionEmisor { get; set; }
        public string RazonSocialEmisor { get; set; }
        public string DomicilioFiscalEmisor { get; set; }
        public string CodigoAdquirente { get; set; }
        public string DireccionAdquirente { get; set; }
        public string LocalidadAdquirente { get; set; }
        public string EmailAdquiriente { get; set; }
        public string DocumentoAdquirente { get; set; }
        public string TipoDocumentoAdquirente { get; set; }
        public string RazonSocialAdquirente { get; set; }
        public decimal MontoTotalImpuestos { get; set; }
        public decimal TotalOperacionesGravadas { get; set; }
        public decimal SumatoriaIGV { get; set; }
        public decimal TotalValorVenta { get; set; }
        public decimal TotalPrecioVenta { get; set; }
        public decimal TotalAnticipos { get; set; }
        public decimal ImporteTotalComprobante { get; set; }
        public decimal ImporteNetoPago { get; set; }
        public string TipoTributo { get; set; }
        public string NombreTributo { get; set; }
        public string TipoCodigoTributo { get; set; }
        public List<Detalle> Detalle { get; set; }
        public string CodigoTipoNota { get; set; }
        public string MotivoNota { get; set; }
        public int CodigoDocumentoAfectado { get; set; }
        public string TipoDocumentoAfectado { get; set; }
        public string SerieNumeroDocAfectado { get; set; }
        public string CondicionPago { get; set; }
        public decimal MontoNetoPagoDocAfectado { get; set; }

    }
}
