﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ModeloEntidades
{
    public class ComprobanteBaja
    {
        public string IdentResumen { get; set; }
        public string FecEmiDocumento { get; set; }
        public string FecGenDocumento { get; set; }
        public string NumDocEmisor { get; set; }
        public string TipDocEmisor { get; set; }
        public string NomComEmisor { get; set; }
        public int Item { get; set; }
        public string TipoDocumento { get; set; }
        public string SerieDocumento { get; set; }
        public string NumCorreDocu { get; set; }
        public string MotivoBaja { get; set; }
        public string NumeroDocumentoReceptor { get; set; }
        public string TipoDocumentoReceptor { get; set; }
        public decimal TotalComprobante { get; set; }
        public string TipoMoneda { get; set; }
        public decimal MontoTotalGravado { get; set; }
        public decimal MontoIGV { get; set; }
    }
}
