﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ModeloEntidades
{
    public class Cuotas
    {
        public string CodigoCuota { get; set; }
        public decimal MontoCuota { get; set; }
        public string FechaVencimiento { get; set; }
    }
}
