﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ModeloEntidades
{
    public class Detalle
    {
        public string NumeroItem { get; set; }
        public string UnidadMedida { get; set; }
        public decimal Cantidad { get; set; }
        public string CodigoProducto { get; set; }
        public string DescripcionProducto { get; set; }
        public string CategoriaImpuestos { get; set; }
        public string CodigoAfectacionIgv { get; set; }
        public decimal PorcentajeImpuestos { get; set; }
        public string NombreTributo { get; set; }
        public string CodigoTributo { get; set; }
        public decimal ValorVentaItem { get; set; }
        public decimal PrecioUnitario { get; set; }
        public string CodigoTipoPrecio { get; set; }
        public decimal MontoTributoItem { get; set; }
        public decimal MontoOperacion { get; set; }
        public decimal ValorUnitarioxItem { get; set; }
        public string CodigoTipoMoneda { get; set; }
        public string IndicadorDescuento { get; set; }
        public string CodigoDescuento { get; set; }
        public decimal FactorPorcDescuento { get; set; }
        public decimal MontoDescuento { get; set; }
        public decimal BaseImponibleDescuento { get; set; }
    }
}
