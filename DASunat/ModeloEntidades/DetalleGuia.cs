﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ModeloEntidades
{
    public class DetalleGuia
    {
        public string NumeroItem { get; set; }
        public string UnidadMedida { get; set; }
        public decimal Cantidad { get; set; }
        public string CodigoProducto { get; set; }
        public string DescripcionProducto { get; set; }
 
    }
}
