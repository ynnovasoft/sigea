﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ModeloEntidades
{
   public  class Guia
    {
        public Guia()
        {
            this.Detalle = new List<DetalleGuia>();
        }
        public string SerieNumeroGuia { get; set; }
        public string FechaEmision { get; set; }
        public string HoraEmision { get; set; }
        public string CodigoTipoGuia { get; set; }
        public string NumeroDocumentoEmisor { get; set; }
        public string TipoDocumentoEmisor { get; set; }
        public string RazonSocialEmisor { get; set; }
        public string DocumentoAdquirente { get; set; }
        public string TipoDocumentoAdquirente { get; set; }
        public string RazonSocialAdquirente { get; set; }
        public string MotivoTraslado { get; set; }
        public string IndicacodorTransbordo { get; set; }
        public decimal PesoBrutoBienes { get; set; }
        public string ModalidadTraslado { get; set; }
        public string FechaInicioTraslado { get; set; }
        public string UbigeoLlegada { get; set; }
        public string DireccionLlegada { get; set; }
        public string CodigoEstablecimientoLlegada { get; set; }
        public string UbigeoPartida { get; set; }
        public string DireccionPartida { get; set; }
        public string CodigoEstablecimientoPartida { get; set; }
        public string TipoDocumentoAdjunto { get; set; }
        public string NumDocumentoAdjunto { get; set; }
        public string DesCompAdjDocumentoAdjunto { get; set; }
        public List<DetalleGuia> Detalle { get; set; }
    }
}
