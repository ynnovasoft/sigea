﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ModeloEntidades
{
   public class ListaGuia
    {
        public string NumeroGuiaRemision { get; set; }
        public string CodigoGuiaRemision { get; set; }
    }
}
