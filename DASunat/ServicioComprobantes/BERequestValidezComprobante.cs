﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ServicioComprobantes
{
    public class BERequestValidezComprobante
    {

        public string numRuc { get; set; }
        public string codComp { get; set; }
        public string numeroSerie { get; set; }
        public int numero { get; set; }
        public string fechaEmision { get; set; }
        public decimal monto { get; set; }
    }
}
