﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ServicioComprobantes
{
    public class BEResponseDetalleValidezComprobante
    {
        public int estadoCp { get; set; }
        public string estadoRuc { get; set; }
        public string condDomiRuc { get; set; }
    }
}
