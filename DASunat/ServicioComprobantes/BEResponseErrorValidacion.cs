﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ServicioComprobantes
{
    public class BEResponseErrorValidacion
    {
        public string error_description { get; set; }
        public string error { get; set; }
    }
}
