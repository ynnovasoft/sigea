﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ServicioComprobantes
{
    public class BEResponseValidezComprobante
    {
        public bool success { get; set; }
        public string message { get; set; }
        public BEResponseDetalleValidezComprobante data { get; set; }
        public string errorCode { get; set; }
    }
}
