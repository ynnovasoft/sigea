﻿using System;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using System.Configuration;

namespace DASunat.ServicioComprobantes
{
    public class ComprobantesElectronicos
    {
        public string GenerarTokenValidacion(int CodigoEmpresa, ref string token)
        {
            string Mensaje = "";
            try
            {
                string Client_Id = "";
                string Client_Secret = "";
                Mensaje = "";
                token = "";
                if (CodigoEmpresa == 1)
                {
                    Client_Id = ConfigurationManager.AppSettings["Client_IdSunatValidadorEmpresa"];
                    Client_Secret = ConfigurationManager.AppSettings["Client_SecretSunatValidadorEmpresa"];
                }
                else if (CodigoEmpresa == 2)
                {
                    Client_Id = ConfigurationManager.AppSettings["Client_IdSunatValidadorEmpresa2"];
                    Client_Secret = ConfigurationManager.AppSettings["Client_SecretSunatValidadorEmpresa2"];
                }
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var url = ConfigurationManager.AppSettings["UrlApiGeneracionTokenValidezComp"] + Client_Id + "/oauth2/token/";
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(url);
                RestRequest request = new RestRequest();
                request.Method = Method.POST;
                request.AddParameter("grant_type", "client_credentials");
                request.AddParameter("scope", "https://api.sunat.gob.pe/v1/contribuyente/contribuyentes");
                request.AddParameter("client_id", Client_Id);
                request.AddParameter("client_secret", Client_Secret);
                IRestResponse response = new RestResponse();
                response = client.Execute(request);
                if (response.ResponseStatus.ToString() == "Completed")
                {
                    string Dato = response.Content.ToString();
                    BEToken oDato = JsonConvert.DeserializeObject<BEToken>(Dato);
                    if (oDato.access_token != null)
                    {
                        token = oDato.access_token;
                    }
                    else
                    {
                        BEResponseErrorValidacion oDatoError = JsonConvert.DeserializeObject<BEResponseErrorValidacion>(Dato);
                        Mensaje = "DASunat-ServicioComprobates-ComprobantesElectronicos MetodoEjecutado: GenerarTokenValidacion - Error: " + oDatoError.error + " - Mensaje del Error: " + oDatoError.error_description;
                    }
                }
                else
                {
                    Mensaje = "DASunat-ServicioComprobates-ComprobantesElectronicos MetodoEjecutado: GenerarTokenValidacion - " + response.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Mensaje = "DASunat-ServicioComprobates-ComprobantesElectronicos MetodoEjecutado: GenerarTokenValidacion - " + ex.Message;
            }
            return Mensaje;
        }
        public string ConsultaValidez(int CodigoEmpresa, BERequestValidezComprobante oDatoRequest, ref BEResponseValidezComprobante ResponseData)
        {
            string Mensaje = "";
            try
            {
                string Token = "";
                Mensaje = "";
                Mensaje = GenerarTokenValidacion(CodigoEmpresa, ref Token);
                if (Mensaje == "")
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var url = ConfigurationManager.AppSettings["UrlApiConsultaValidezComp"] + oDatoRequest.numRuc + "/validarcomprobante";
                    RestClient client = new RestClient();
                    client.BaseUrl = new Uri(url);
                    RestRequest request = new RestRequest();
                    request.Method = Method.POST;
                    request.AddHeader("Authorization", "Bearer " + Token);
                    request.AddJsonBody(oDatoRequest);
                    IRestResponse response = new RestResponse();
                    response = client.Execute(request);
                    if (response.ResponseStatus.ToString() == "Completed")
                    {
                        string Dato = response.Content.ToString();
                        BEResponseValidezComprobante oDato = JsonConvert.DeserializeObject<BEResponseValidezComprobante>(Dato);
                        if (oDato.success == true)
                        {
                            ResponseData = oDato;
                        }
                        else
                        {
                            Mensaje = "DASunat-ServicioComprobates-ComprobantesElectronicos MetodoEjecutado: ConsultaValidez - Código Error: " + oDato.errorCode + " - Mensaje del Error: " + oDato.message;
                        }
                    }
                    else
                    {
                        Mensaje = "DASunat-ServicioComprobates-ComprobantesElectronicos MetodoEjecutado: ConsultaValidez - " + response.ErrorMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                Mensaje = "DASunat-ServicioComprobates-ComprobantesElectronicos MetodoEjecutado: ConsultaValidez - " + ex.Message;
            }
            return Mensaje;
        }
    }
}
