﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ServicioGuia
{
   public class BERequestEnvioguia 
    {
        public string nomArchivo { get; set; }
        public string arcGreZip { get; set; }
        public string hashZip { get; set; }
    }
}
