﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ServicioGuia
{
   public class BEResponseConsultaGuia
    {
        public string codRespuesta { get; set; }
        public BEResponseErrorConsultaGuia error { get; set; }
        public string arcCdr { get; set; }
        public string indCdrGenerado { get; set; }

    }
}
