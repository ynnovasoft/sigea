﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DASunat.ServicioGuia
{
   public class BEResponseErrorEnvioGuia
    {
        public string cod { get; set; }
        public string msg { get; set; }
        public string exc { get; set; }
    }
}
