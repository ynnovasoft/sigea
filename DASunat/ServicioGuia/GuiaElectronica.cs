﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web;
using Entidades;
using RestSharp;
using System.Configuration;

namespace DASunat.ServicioGuia
{
    public class GuiaElectronica
    {
        public string GenerarToken(int CodigoEmpresa, ref string token)
        {
            string Mensaje = "";
            try
            {
                string UsuarioFEEmpresa = "";
                string PasswordFEEmpresa = "";
                string Client_Id = "";
                string Client_Secret = "";
                Mensaje = "";
                token = "";
                if (CodigoEmpresa == 1)
                {
                    UsuarioFEEmpresa = ConfigurationManager.AppSettings["UsuarioFEEmpresa"];
                    PasswordFEEmpresa = ConfigurationManager.AppSettings["PasswordFEEmpresa"];
                    Client_Id = ConfigurationManager.AppSettings["Client_IdSunatGuiaEmpresa"];
                    Client_Secret = ConfigurationManager.AppSettings["Client_SecretSunatGuiaEmpresa"];
                }
                else if (CodigoEmpresa == 2)
                {
                    UsuarioFEEmpresa = ConfigurationManager.AppSettings["UsuarioFEEmpresa2"];
                    PasswordFEEmpresa = ConfigurationManager.AppSettings["PasswordFEEmpresa2"];
                    Client_Id = ConfigurationManager.AppSettings["Client_IdSunatGuiaEmpresa2"];
                    Client_Secret = ConfigurationManager.AppSettings["Client_SecretSunatGuiaEmpresa2"];
                }
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var url = ConfigurationManager.AppSettings["UrlApiGeneracionTokenGuia"] + Client_Id + "/oauth2/token/";
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(url);
                RestRequest request = new RestRequest();
                request.Method = Method.POST;
                request.AddHeader("Content-Type", "Application/json");
                request.AddParameter("grant_type", "password");
                request.AddParameter("scope", "https://api-cpe.sunat.gob.pe");
                request.AddParameter("client_id", Client_Id);
                request.AddParameter("client_secret", Client_Secret);
                request.AddParameter("username", UsuarioFEEmpresa);
                request.AddParameter("password", PasswordFEEmpresa);
                IRestResponse response = new RestResponse();
                response = client.Execute(request);
                if (response.ResponseStatus.ToString() == "Completed")
                {
                    string Dato = response.Content.ToString();
                    BEToken oDato = JsonConvert.DeserializeObject<BEToken>(Dato);
                    if (oDato.access_token != null)
                    {
                        token = oDato.access_token;
                    }
                    else
                    {
                        BEResponseErrorEnvioGuia oDatoError = JsonConvert.DeserializeObject<BEResponseErrorEnvioGuia>(Dato);
                        Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: GenerarToken - Código Error: " + oDatoError.cod + "- Mensaje del Error: " + oDatoError.msg + " - " + oDatoError.exc;
                    }
                }
                else
                {
                    Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: GenerarToken - " + response.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: GenerarToken - " + ex.Message;
            }
            return Mensaje;
        }
        public string EnviarGuia(int CodigoEmpresa, string NombreArchivo, object ObjetoZip, ref string numTicket)
        {
            string Mensaje = "";
            try
            {
                string Token = "";
                Mensaje = "";
                Mensaje = GenerarToken(CodigoEmpresa, ref Token);
                if (Mensaje == "")
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var url = ConfigurationManager.AppSettings["UrlApiEnviarGuia"] + NombreArchivo;
                    RestClient client = new RestClient();
                    client.BaseUrl = new Uri(url);
                    RestRequest request = new RestRequest();
                    request.Method = Method.POST;
                    request.AddHeader("Authorization", "Bearer " + Token);
                    request.AddJsonBody(ObjetoZip);
                    IRestResponse response = new RestResponse();
                    response = client.Execute(request);
                    if (response.ResponseStatus.ToString() == "Completed")
                    {
                        string Dato = response.Content.ToString();
                        BEResponseEnvioGuia oDato = JsonConvert.DeserializeObject<BEResponseEnvioGuia>(Dato);
                        if (oDato.numTicket != null)
                        {
                            numTicket = "";
                            numTicket = oDato.numTicket;
                        }
                        else
                        {
                            BEResponseErrorEnvioGuia oDatoError = JsonConvert.DeserializeObject<BEResponseErrorEnvioGuia>(Dato);
                            Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: EnviarGuia - Código Error: " + oDatoError.cod + " - Mensaje del Error: " + oDatoError.msg + " - " + oDatoError.exc;
                        }
                    }
                    else
                    {
                        Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: EnviarGuia - " + response.ErrorMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: EnviarGuia - " + ex.Message;
            }
            return Mensaje;
        }
        public string ConsultarEstadoEnvio(int CodigoEmpresa, string NumeroTicket, string RutaFECDRS, string NombreArchivo)
        {
            string Mensaje = "";
            try
            {
                string Token = "";
                Mensaje = "";
                Mensaje = GenerarToken(CodigoEmpresa, ref Token);
                if (Mensaje == "")
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var url = ConfigurationManager.AppSettings["UrlApiConsultaEnvioGuia"] + NumeroTicket;
                    RestClient client = new RestClient();
                    client.BaseUrl = new Uri(url);
                    RestRequest request = new RestRequest();
                    request.Method = Method.GET;
                    request.AddHeader("Authorization", "Bearer " + Token);
                    IRestResponse response = new RestResponse();
                    response = client.Execute(request);
                    if (response.ResponseStatus.ToString() == "Completed")
                    {
                        string Dato = response.Content.ToString();
                        BEResponseConsultaGuia oDato = JsonConvert.DeserializeObject<BEResponseConsultaGuia>(Dato);
                        if (oDato.codRespuesta != null)
                        {
                            int CodigoRespuesta = Convert.ToInt16(oDato.codRespuesta);
                            int CdrGenerado = Convert.ToInt16(oDato.indCdrGenerado);
                            if (CodigoRespuesta == 99 | CodigoRespuesta == 0)
                            {
                                if (CdrGenerado == 0)
                                {
                                    Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: ConsultarEstadoEnvio - Código Error: " + oDato.error.numError + " - Mensaje del Error: " + oDato.error.desError;
                                }
                                else
                                {
                                    byte[] bitArray;
                                    string base64cdr;
                                    base64cdr = oDato.arcCdr;
                                    bitArray = Convert.FromBase64String(base64cdr);
                                    System.IO.FileStream fs = new System.IO.FileStream(RutaFECDRS + "R-" + NombreArchivo + ".zip", System.IO.FileMode.Create);
                                    fs.Write(bitArray, 0, bitArray.Length);
                                    fs.Close();
                                }
                            }
                            else if (CodigoRespuesta == 98)
                            {
                                Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: ConsultarEstadoEnvio - Mensaje del Error: Intente consultar el estado de la respuesta nuevamente, ya que se presentó una intermitencia en el proceso de consulta";
                            }
                        }
                        else
                        {
                            BEResponseErrorEnvioGuia oDatoError = JsonConvert.DeserializeObject<BEResponseErrorEnvioGuia>(Dato);
                            Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: EnviarGuia - Código Error: " + oDatoError.cod + " Mensaje del Error: " + oDatoError.msg + " - " + oDatoError.exc;
                        }
                    }
                    else
                    {
                        Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: EnviarGuia - " + response.ErrorMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                Mensaje = "DASunat-ServicioGuia-GuiaElectronica MetodoEjecutado: EnviarGuia - " + ex.Message;
            }
            return Mensaje;
        }
    }
}
