﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Entidades;
using DASunat.ModeloEntidades;

namespace Data
{
    public class DABilling
    {
        public DataTable ObtenerDatosGeneralesPdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosGeneralesPdf");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosGeneralesGuiaPdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosGeneralesGuiaPdf");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosDetallePdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosDetallePdf");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosDetalleGuiaPdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosDetalleGuiaPdf");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosAdicionalPdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosAdicionalPdf");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosCuotasPdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosCuotasPdf");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerGuiasPdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerGuiasPdf");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public ComprobanteBaja ObtenerComunicacionBajaComprobante(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerComunicacionBajaSunat");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                ComprobanteBaja oDato = new ComprobanteBaja();
                adaptador.Fill(dt);
                oDato.IdentResumen = dt.Rows[0]["IdentResumen"].ToString();
                oDato.NomComEmisor = dt.Rows[0]["NomComEmisor"].ToString();
                oDato.NumDocEmisor = dt.Rows[0]["NumDocEmisor"].ToString();
                oDato.TipDocEmisor = dt.Rows[0]["TipDocEmisor"].ToString();
                oDato.FecEmiDocumento = dt.Rows[0]["FecEmiDocumento"].ToString();
                oDato.FecGenDocumento = dt.Rows[0]["FecGenDocumento"].ToString();
                oDato.Item = Convert.ToInt16(dt.Rows[0]["Item"]);
                oDato.TipoDocumento = dt.Rows[0]["TipoDocumento"].ToString();
                oDato.SerieDocumento = dt.Rows[0]["SerieDocumento"].ToString();
                oDato.NumCorreDocu = dt.Rows[0]["NumCorreDocu"].ToString();
                oDato.MotivoBaja = dt.Rows[0]["MotivoBaja"].ToString();
                oDato.MontoIGV = Convert.ToDecimal(dt.Rows[0]["MontoIGV"]);
                oDato.MontoTotalGravado = Convert.ToDecimal(dt.Rows[0]["MontoTotalGravado"]);
                oDato.TotalComprobante = Convert.ToDecimal(dt.Rows[0]["TotalComprobante"]);
                oDato.TipoMoneda = dt.Rows[0]["TipoMoneda"].ToString();
                oDato.TipoDocumentoReceptor = dt.Rows[0]["TipoDocumentoReceptor"].ToString();
                oDato.NumeroDocumentoReceptor = dt.Rows[0]["NumeroDocumentoReceptor"].ToString();
                return oDato;
            }
        }
        public ComprobanteBaja ObtenerComunicacionBajaGuia(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerComunicacionBajaSunatGuia");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                ComprobanteBaja oDato = new ComprobanteBaja();
                adaptador.Fill(dt);
                oDato.IdentResumen = dt.Rows[0]["IdentResumen"].ToString();
                oDato.NomComEmisor = dt.Rows[0]["NomComEmisor"].ToString();
                oDato.NumDocEmisor = dt.Rows[0]["NumDocEmisor"].ToString();
                oDato.TipDocEmisor = dt.Rows[0]["TipDocEmisor"].ToString();
                oDato.FecEmiDocumento = dt.Rows[0]["FecEmiDocumento"].ToString();
                oDato.FecGenDocumento = dt.Rows[0]["FecGenDocumento"].ToString();
                oDato.Item = Convert.ToInt16(dt.Rows[0]["Item"]);
                oDato.TipoDocumento = dt.Rows[0]["TipoDocumento"].ToString();
                oDato.SerieDocumento = dt.Rows[0]["SerieDocumento"].ToString();
                oDato.NumCorreDocu = dt.Rows[0]["NumCorreDocu"].ToString();
                oDato.MotivoBaja = dt.Rows[0]["MotivoBaja"].ToString();
                oDato.MontoIGV = Convert.ToDecimal(dt.Rows[0]["MontoIGV"]);
                oDato.MontoTotalGravado = Convert.ToDecimal(dt.Rows[0]["MontoTotalGravado"]);
                oDato.TotalComprobante = Convert.ToDecimal(dt.Rows[0]["TotalComprobante"]);
                oDato.TipoMoneda = dt.Rows[0]["TipoMoneda"].ToString();
                oDato.TipoDocumentoReceptor = dt.Rows[0]["TipoDocumentoReceptor"].ToString();
                oDato.NumeroDocumentoReceptor = dt.Rows[0]["NumeroDocumentoReceptor"].ToString();
                return oDato;
            }
        }
        public Comprobante ObtenerCabeceraSunat(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerCabeceraSunat");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                Comprobante oDato = new Comprobante();
                adaptador.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    oDato = new Comprobante();
                    oDato.CodigoTipoOperacion = dt.Rows[0]["CodigoTipoOperacion"].ToString();
                    oDato.SerieNumeroComprobante = dt.Rows[0]["SerieNumeroComprobante"].ToString();
                    oDato.FechaEmision = dt.Rows[0]["FechaEmision"].ToString();
                    oDato.HoraEmision = dt.Rows[0]["HoraEmision"].ToString();
                    oDato.FechaVencimiento = dt.Rows[0]["FechaVencimiento"].ToString();
                    oDato.MontoEnLetras = dt.Rows[0]["MontoEnLetras"].ToString();
                    oDato.CodigoTipoMoneda = dt.Rows[0]["CodigoTipoMoneda"].ToString();
                    oDato.CodigoTipoComprobante = dt.Rows[0]["CodigoTipoDocumento"].ToString();
                    oDato.NumeroOrdenCompra = dt.Rows[0]["NumeroOrdenCompra"].ToString();
                    oDato.NombreComercialEmisor = dt.Rows[0]["NombreComercialEmisor"].ToString();
                    oDato.RazonSocialEmisor = dt.Rows[0]["RazonSocialEmisor"].ToString();
                    oDato.NumeroDocumentoEmisor = dt.Rows[0]["NumeroDocumentoEmisor"].ToString();
                    oDato.TipoDocumentoEmisor = dt.Rows[0]["TipoDocumentoEmisor"].ToString();
                    oDato.CodigoFiscalEmisor = dt.Rows[0]["CodigoFiscalEmisor"].ToString();
                    oDato.DomicilioFiscalEmisor = dt.Rows[0]["DomicilioFiscalEmisor"].ToString();
                    oDato.TipoDocumentoAdquirente = dt.Rows[0]["TipoDocumentoAdquiriente"].ToString();
                    oDato.DocumentoAdquirente = dt.Rows[0]["DocumentoAdquiriente"].ToString();
                    oDato.RazonSocialAdquirente = dt.Rows[0]["RazonSocialAdquiriente"].ToString();
                    oDato.DireccionAdquirente = dt.Rows[0]["DireccionAdquiriente"].ToString();
                    oDato.MontoTotalImpuestos = Convert.ToDecimal(dt.Rows[0]["MontoTotalImpuestos"]);
                    oDato.TotalOperacionesGravadas = Convert.ToDecimal(dt.Rows[0]["TotalOperacionesGravadas"]);
                    oDato.SumatoriaIGV = Convert.ToDecimal(dt.Rows[0]["SumatoriaIGV"]);
                    oDato.TotalValorVenta = Convert.ToDecimal(dt.Rows[0]["TotalValorVenta"]);
                    oDato.TotalPrecioVenta = Convert.ToDecimal(dt.Rows[0]["TotalPrecioVenta"]);
                    oDato.ImporteTotalComprobante = Convert.ToDecimal(dt.Rows[0]["ImporteTotal"]);
                    oDato.TipoTributo = dt.Rows[0]["TipoTributo"].ToString();
                    oDato.NombreTributo = dt.Rows[0]["NombreTributo"].ToString();
                    oDato.TipoCodigoTributo = dt.Rows[0]["TipoCodigoTributo"].ToString();
                    oDato.CodigoTipoNota = dt.Rows[0]["CodigoTipoNota"].ToString();
                    oDato.MotivoNota = dt.Rows[0]["MotivoNota"].ToString();
                    oDato.CodigoDocumentoAfectado = Convert.ToInt16(dt.Rows[0]["CodigoDocumentoAfectado"]);
                    oDato.TipoDocumentoAfectado = dt.Rows[0]["TipoDocumentoAfectado"].ToString();
                    oDato.SerieNumeroDocAfectado = dt.Rows[0]["SerieNumeroDocAfectado"].ToString();
                    oDato.CondicionPago = dt.Rows[0]["CondicionPago"].ToString();
                    oDato.TotalAnticipos = Convert.ToDecimal(dt.Rows[0]["TotalAnticipos"]);
                    oDato.ImporteNetoPago = Convert.ToDecimal(dt.Rows[0]["MontoNetoPago"]);
                    oDato.MontoNetoPagoDocAfectado = Convert.ToDecimal(dt.Rows[0]["MontoNetoPagoDocAfectado"]);
                    return oDato;
                }

                return null;
            }
        }
        public List<ListaGuia> ObtenerGuiasFactura(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerGuiasFacturaSunat");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                List<ListaGuia> oDato = new List<ListaGuia>();
                adaptador.Fill(dt);
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    ListaGuia obj = new ListaGuia();
                    obj.NumeroGuiaRemision = dt.Rows[i]["NumeroGuiaRemision"].ToString ();
                    obj.CodigoGuiaRemision = dt.Rows[i]["CodigoGuiaRemision"].ToString();
                    oDato.Add(obj);
                }
                return oDato;
            }
        }
        public List<Detalle> ObtenerDetalleSunat(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDetalleSunat");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                List<Detalle> oDato = new List<Detalle>();
                adaptador.Fill(dt);
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    Detalle obj = new Detalle();
                    obj.Cantidad = Convert.ToDecimal(dt.Rows[i]["Cantidad"]);
                    obj.UnidadMedida = dt.Rows[i]["Unidadmedida"].ToString();
                    obj.ValorVentaItem = Convert.ToDecimal(dt.Rows[i]["ValorVentaItem"]);
                    obj.PrecioUnitario = Convert.ToDecimal(dt.Rows[i]["PrecioVentaUnitario"]);
                    obj.CodigoTipoPrecio = dt.Rows[i]["CodigoTipoPrecio"].ToString();
                    obj.MontoTributoItem = Convert.ToDecimal(dt.Rows[i]["MontoTributoItem"]);
                    obj.MontoOperacion = Convert.ToDecimal(dt.Rows[i]["MontoOperacion"]);
                    obj.DescripcionProducto = dt.Rows[i]["Descripcion"].ToString();
                    obj.CodigoProducto = dt.Rows[i]["CodigoProducto"].ToString();
                    obj.CategoriaImpuestos = dt.Rows[i]["CategoriaImpuestos"].ToString();
                    obj.CodigoAfectacionIgv = dt.Rows[i]["CodigoAfectacionIgv"].ToString();
                    obj.PorcentajeImpuestos = Convert.ToDecimal(dt.Rows[i]["PocentajeImpuestos"]);
                    obj.NombreTributo = dt.Rows[i]["NombreTributo"].ToString();
                    obj.CodigoTributo = dt.Rows[i]["CodigoTributo"].ToString();
                    obj.ValorUnitarioxItem = Convert.ToDecimal(dt.Rows[i]["ValorUnitarioItem"]);
                    obj.IndicadorDescuento = dt.Rows[i]["IndicadorDescuento"].ToString();
                    obj.CodigoDescuento = dt.Rows[i]["CodigoDescuento"].ToString();
                    obj.FactorPorcDescuento = Convert.ToDecimal(dt.Rows[i]["FactorPorcDescuento"]);
                    obj.MontoDescuento = Convert.ToDecimal(dt.Rows[i]["MontoDescuento"]);
                    obj.BaseImponibleDescuento = Convert.ToDecimal(dt.Rows[i]["BaseImponibleDescuento"]);
                    oDato.Add(obj);
                }
                return oDato;
            }
        }
        public bool ActualizarRespuestaSunat(int  Codigo , int  Estado , string  Observaciones , string CodigoQR, string  CodigoHas ,
            int TipoComprobante, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ActualizarRespuestaSunat");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodEstSut", SqlDbType.Int).Value = Estado;
                    cmd.Parameters.Add("@cObsSut", SqlDbType.VarChar).Value = Observaciones;
                    cmd.Parameters.Add("@cCodQR", SqlDbType.VarChar).Value = CodigoQR;
                    cmd.Parameters.Add("@cCodHas", SqlDbType.VarChar).Value = CodigoHas;
                    cmd.Parameters.Add("@nTipoComprobante", SqlDbType.Int).Value = TipoComprobante;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message + " <> Metodo: ActualizarRespuestaSunat";
                return false;
            }
        }
        public bool RegistrarTicketRespuestaBajaSunat(int  Codigo , string  NumTicket , string  NombreXml , int TipoDocumento, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarTicketRespuestaSunat");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@cNumTickSun", SqlDbType.VarChar).Value = NumTicket;
                    cmd.Parameters.Add("@cNombreXml", SqlDbType.VarChar).Value = NombreXml;
                    cmd.Parameters.Add("@nTipoDocumento", SqlDbType.Int).Value = TipoDocumento;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message + " <> Metodo: RegistrarTicketRespuestaBajaSunat";
                return false;
            }
        }
        public DataTable ObtenerDatosGeneralesPdfCotizacion(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosGeneralesPdfCotizacion");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosDetallePdfCotizacion(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosDetallePdfCotizacion");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool EnviaXmlPdfElectronico(int Codigo, string RutaPDF ,  string  RutaXML , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EnviaXmlPdfElectronico");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@RutaPDF", SqlDbType.VarChar).Value = RutaPDF;
                    cmd.Parameters.Add("@RutaXML", SqlDbType.VarChar).Value = RutaXML;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message + " <> Metodo: EnviaXmlPdfElectronico";
                return false;
            }
        }
        public List<Anticipo> ObtenerAnticipos(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDetalleAnticipoSunat");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                List<Anticipo> oDato = new List<Anticipo>();
                adaptador.Fill(dt);
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    Anticipo obj = new Anticipo();
                    obj.NumeroComprobanteAnticipo = dt.Rows[i]["NumeroComprobanteAnticipo"].ToString();
                    obj.CodigoTipoMoneda = dt.Rows[i]["CodigoTipoMoneda"].ToString();
                    obj.ImporteTotalAnticipo = Convert .ToDecimal(dt.Rows[i]["ImporteTotalAnticipo"]);
                    oDato.Add(obj);
                }
                return oDato;
            }
        }
        public List<Cuotas> ObtenerCuotas(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosCuotasSunat");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                List<Cuotas> oDato = new List<Cuotas>();
                adaptador.Fill(dt);
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    Cuotas obj = new Cuotas();
                    obj.CodigoCuota = dt.Rows[i]["cCodCuo"].ToString();
                    obj.MontoCuota = Convert.ToDecimal(dt.Rows[i]["nMonCuo"]);
                    obj.FechaVencimiento = dt.Rows[i]["dFecVen"].ToString();
                    oDato.Add(obj);
                }
                return oDato;
            }
        }
        public DataTable ObtenerDatosDetalleEstadoCuentaPdf(int Codigo, int  Moneda , int  CodEmp , int Modalidad )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosDetallePdfEECC");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@Moneda", SqlDbType.Int).Value = Moneda;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodEmp;
                cmd.Parameters.Add("@Modalidad", SqlDbType.Int).Value = Modalidad;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosGeneralesEstadoCuentaPdf(int Codigo,  int CodEmp)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosGeneralesPdfEECC");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodEmp;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosTotalesEstadoCuentaPdf(int Codigo, int  Moneda , int  CodEmp ,   int  Modalidad )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosTotalesPdfEECC");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@Moneda", SqlDbType.Int).Value = Moneda;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodEmp;
                cmd.Parameters.Add("@Modalidad", SqlDbType.Int).Value = Modalidad;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public Guia ObtenerCabeceraGuiaSunat(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerCabeceraGuiaSunat");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                Guia oDato = new Guia();
                adaptador.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    oDato = new Guia();               
                    oDato.SerieNumeroGuia = dt.Rows[0]["SerieNumeroGuia"].ToString();
                    oDato.FechaEmision = dt.Rows[0]["FechaEmision"].ToString();
                    oDato.HoraEmision = dt.Rows[0]["HoraEmision"].ToString();
                    oDato.CodigoTipoGuia = dt.Rows[0]["CodigoTipoGuia"].ToString();
                    oDato.RazonSocialEmisor = dt.Rows[0]["RazonSocialEmisor"].ToString();
                    oDato.NumeroDocumentoEmisor = dt.Rows[0]["NumeroDocumentoEmisor"].ToString();
                    oDato.TipoDocumentoEmisor = dt.Rows[0]["TipoDocumentoEmisor"].ToString();
                    oDato.TipoDocumentoAdquirente = dt.Rows[0]["TipoDocumentoAdquiriente"].ToString();
                    oDato.DocumentoAdquirente = dt.Rows[0]["DocumentoAdquiriente"].ToString();
                    oDato.RazonSocialAdquirente = dt.Rows[0]["RazonSocialAdquiriente"].ToString();
                    oDato.MotivoTraslado = dt.Rows[0]["MotivoTraslado"].ToString();
                    oDato.IndicacodorTransbordo = dt.Rows[0]["IndicacodorTransbordo"].ToString();
                    oDato.PesoBrutoBienes =Convert .ToDecimal( dt.Rows[0]["PesoBrutoBienes"]);
                    oDato.ModalidadTraslado = dt.Rows[0]["ModalidadTraslado"].ToString();
                    oDato.FechaInicioTraslado = dt.Rows[0]["FechaInicioTraslado"].ToString();
                    oDato.UbigeoLlegada = dt.Rows[0]["UbigeoLlegada"].ToString();
                    oDato.DireccionLlegada = dt.Rows[0]["DireccionLlegada"].ToString();
                    oDato.CodigoEstablecimientoLlegada = dt.Rows[0]["CodigoEstablecimientoLlegada"].ToString();
                    oDato.UbigeoPartida = dt.Rows[0]["UbigeoPartida"].ToString();
                    oDato.DireccionPartida = dt.Rows[0]["DireccionPartida"].ToString();
                    oDato.CodigoEstablecimientoPartida = dt.Rows[0]["CodigoEstablecimientoPartida"].ToString();
                    oDato.TipoDocumentoAdjunto = dt.Rows[0]["cTipDocAdj"].ToString();
                    oDato.NumDocumentoAdjunto = dt.Rows[0]["cNumDocAdj"].ToString();
                    oDato.DesCompAdjDocumentoAdjunto = dt.Rows[0]["cDesCompAdj"].ToString();
                    return oDato;
                }
                return null;
            }
        }
        public List<DetalleGuia> ObtenerDetalleGuiaSunat(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDetalleGuiaSunat");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                List<DetalleGuia> oDato = new List<DetalleGuia>();
                adaptador.Fill(dt);
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    DetalleGuia obj = new DetalleGuia();
                    obj.Cantidad = Convert.ToDecimal(dt.Rows[i]["Cantidad"]);
                    obj.UnidadMedida = dt.Rows[i]["Unidadmedida"].ToString();
                    obj.DescripcionProducto = dt.Rows[i]["Descripcion"].ToString();
                    obj.CodigoProducto = dt.Rows[i]["CodigoProducto"].ToString();
                    oDato.Add(obj);
                }
                return oDato;
            }
        }
        public DataTable ObtenerDatosGeneralesOrdenCompraLocalPdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosGeneralesPdfOrdenCompraLocal");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosDetalleOrdenCompraLocalPdf(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosDetalleOrdenCompraLocalPdf");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool RegistraNumeroTicketEnvioSunat(int Codigo, string numTicket, int TipoDocumento, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistraTicketEnvioSunat");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@cNumTickSun", SqlDbType.VarChar).Value = numTicket;
                    cmd.Parameters.Add("@nTipoDocumento", SqlDbType.Int).Value = TipoDocumento;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DABilling MetodoEjecutado: RegistraNumeroTicketEnvioSunat - " + ex.Message;
                return false;
            }
        }
        public DataTable ObtenerDatosGeneralesPdfAlmacen(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosGeneralesPdfAlmacen");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerDatosDetallePdfAlmacen(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerDatosDetallePdfAlmacen");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }

    }
}
