﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;

namespace Data
{
   public class DACash
    {
        public bool Registrar(BECaja oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarOperacionesDia");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@dFecha", SqlDbType.Date).Value = oDato.gdFecha;
                    cmd.Parameters.Add("@nTipCamCom", SqlDbType.Money).Value = oDato.gnTipCamCom;
                    cmd.Parameters.Add("@nTipCamVen", SqlDbType.Money).Value = oDato.gnTipCamVen;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@bAperturado", SqlDbType.Bit).Value = oDato.gbAperturado;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.gnCodEmp;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool RegistrarOperaciones(BECaja oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarOperacionesCaja");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.gnCodigoAgencia;
                    cmd.Parameters.Add("@cUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@dFecha", SqlDbType.Date).Value = oDato.gdFecha;
                    cmd.Parameters.Add("@bAperturado", SqlDbType.Bit).Value = oDato.gbAperturado;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.gnCodEmp;
                    cmd.Parameters.Add("@nMonApe", SqlDbType.Money).Value = oDato.gnMonApe;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }

    }
}
