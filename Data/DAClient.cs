﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace Data
{
    public class DAClient
    {
        public DataTable Obtener(string Valor, int nTipoBusqueda)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerCliente");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable Listar(string Valor, int nTipoBusqueda)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarCliente");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool Grabar(ref BECustomer oDato, bool bNuevo, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd;
                    if (bNuevo == true)
                    {
                        cmd = new SqlCommand("PA_RegistrarClientes");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                    }
                    else
                    {
                        cmd = new SqlCommand("PA_ModificarClientes");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                        cmd.Parameters.Add("@nCodCli", SqlDbType.Int).Value = oDato.nCodCli;
                    }
                    cmd.Parameters.Add("@cApeMat", SqlDbType.VarChar).Value = oDato.cApeMat;
                    cmd.Parameters.Add("@cApePat", SqlDbType.VarChar).Value = oDato.cApePat;
                    cmd.Parameters.Add("@cContactos", SqlDbType.VarChar).Value = oDato.cContactos;
                    cmd.Parameters.Add("@cCorreo", SqlDbType.VarChar).Value = oDato.cCorreo;
                    cmd.Parameters.Add("@cDirCli", SqlDbType.VarChar).Value = oDato.cDirCli;
                    cmd.Parameters.Add("@cNombres", SqlDbType.VarChar).Value = oDato.cNombres;
                    cmd.Parameters.Add("@cNumDoc", SqlDbType.VarChar).Value = oDato.cNumDoc;
                    cmd.Parameters.Add("@cRazSoc", SqlDbType.VarChar).Value = oDato.cRazSoc;
                    cmd.Parameters.Add("@cTipDocCli", SqlDbType.VarChar).Value = oDato.cTipDocCli;
                    cmd.Parameters.Add("@cCodUbiDep", SqlDbType.VarChar).Value = oDato.cCodUbiDep;
                    cmd.Parameters.Add("@cCodUbiProv", SqlDbType.VarChar).Value = oDato.cCodUbiProv;
                    cmd.Parameters.Add("@cCodUbiDis", SqlDbType.VarChar).Value = oDato.cCodUbiDis;
                    cmd.Parameters.Add("@nTipPer", SqlDbType.Int).Value = oDato.nTipPer;
                    cmd.Parameters.Add("@bAgeRet", SqlDbType.Bit).Value = oDato.bAgeRet;
                    cmd.Parameters.Add("@nTipCli", SqlDbType.Int).Value = oDato.nTipCli;
                    cmd.Parameters.Add("@nCodVen", SqlDbType.Int).Value = oDato.nCodVen;
                    cmd.Parameters.Add("@nCodCliOut", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        if (cMensaje == "")
                        {
                            oDato.nCodCli = Convert.ToInt16(cmd.Parameters["@nCodCliOut"].Value);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DAClient MetodoEjecutado: Grabar - " + ex.Message;
                return false;
            }
        }
        public bool Eliminar(int Codigo, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarCliente");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCli", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DAClient MetodoEjecutado: Eliminar - " + ex.Message;
                return false;
            }
        }
    }
}
