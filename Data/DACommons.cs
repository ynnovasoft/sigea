﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Data
{
    public class DACommons
    {
        public DataTable MostrarMaestroMantenimiento()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarCatalogoCodigosMantenimiento");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarMaestroMantenimientoValores(string Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarValoresCatalogoMantenimiento");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.VarChar).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarMaestro(string  Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarCatalogoCodigos");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.VarChar).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarVendedores()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarVendedores");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarDireccionesCliente(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarDireccionCliente");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarMaestroUnico(int Codigo, string Valor)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarCatalogoCodigoUnico");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarAgencias(bool Todo, int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarAgencias");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@bTodos", SqlDbType.Bit).Value = Todo;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable RecuperUsuariosAcceso()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperUsuariosAcceso");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarSeries(string TipoDocumento, int CodigoAgencia, int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarSeries");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@TipoDocumento", SqlDbType.VarChar).Value = TipoDocumento;
                cmd.Parameters.Add("@cCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarDepartamento()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarDepartamento");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarProvincia(string cCodUbi)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarProvincia");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@cCodUbi", SqlDbType.VarChar,100).Value = cCodUbi;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarDistrito(string cCodUbi)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarDistrito");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@cCodUbi", SqlDbType.VarChar).Value = cCodUbi;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable RecuperarDetallePagos(int Codigo, int Tipo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperarDetallePagos");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Tipo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarMovimientos()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarTipoMovimientos");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarEmpresas(bool Todos)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarEmpresas");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@bTodos", SqlDbType.Bit).Value = Todos;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarAgenciasCaja(DateTime Fecha, int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarAgenciasCaja");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Fecha", SqlDbType.Date).Value = Fecha;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarUsuariosCaja(DateTime Fecha, int  Agencia , int  CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarUsuariosCaja");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Fecha", SqlDbType.Date).Value = Fecha;
                cmd.Parameters.Add("@CodigoAgencia", SqlDbType.Int).Value = Agencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarMarca()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarMarca");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarPersonal()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarPersonal");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarMoneda()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarMoneda");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable RecuperarComprobantesCreditoBloque(int  Cliente, int Moneda, int Empresa )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarDocumentosCreditoBloque");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodCli", SqlDbType.Int).Value = Cliente;
                cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = Moneda;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = Empresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool EliminarDetallePagos(int CodigoPago,int TipoPago, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarDetallePagos");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@CodigoPago", SqlDbType.Int).Value = CodigoPago;
                    cmd.Parameters.Add("@nCobPag", SqlDbType.Int).Value = TipoPago;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DACommons MetodoEjecutado: EliminarDetallePagos - " + ex.Message;
                return false;
            }
        }
        public bool RegistrarPago(int Codigo , decimal  Monto , int Tipo , string Usuario , int MedioPago, DateTime Fecha , 
            string  NumOpe , int CodigoAgencia , int ModalidadPago , int Moneda , int CodBanco, decimal  TipoCambio ,
            int  CodigoEmpresa ,  ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarPago");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodDoc", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = Fecha;
                    cmd.Parameters.Add("@nMonPag", SqlDbType.Money).Value = Monto;
                    cmd.Parameters.Add("@cNumOpe", SqlDbType.VarChar).Value = NumOpe;
                    cmd.Parameters.Add("@nCobPag", SqlDbType.Int).Value = Tipo;
                    cmd.Parameters.Add("@nMedPag", SqlDbType.Int).Value = MedioPago;
                    cmd.Parameters.Add("@nModPag", SqlDbType.Int).Value = ModalidadPago;
                    cmd.Parameters.Add("@cUsuReg", SqlDbType.VarChar).Value = Usuario;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = Moneda;
                    cmd.Parameters.Add("@nCodBanco", SqlDbType.Int).Value = CodBanco;
                    cmd.Parameters.Add("@nTipCam", SqlDbType.Money).Value = TipoCambio;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DACommons MetodoEjecutado: RegistrarPago - " + ex.Message;
                return false;
            }
        }
        public bool EliminarMaestro(int Codigo, string  Valor, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarDetalleCatalogo");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DACommons MetodoEjecutado: EliminarMaestro - " +  ex.Message;
                return false;
            }
        }
        public bool GrabarMaestro(int Codigo, string ValorAnterior, string  Valor , string  Descripcion , bool  Nuevo , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_GrabarCatalogo");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@ValorAnterior", SqlDbType.VarChar).Value = ValorAnterior;
                    cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                    cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar).Value = Descripcion;
                    cmd.Parameters.Add("@Nuevo", SqlDbType.Bit).Value = Nuevo;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DACommons MetodoEjecutado: GrabarMaestro - " + ex.Message;
                return false;
            }
        }
        public DataTable MostrarBaseDatos()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarBaseDatos");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarAñoContable(int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarAñoContable");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarMesContable(int Anio, int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarMesContable");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nAnioCon", SqlDbType.Int).Value = Anio;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool ValidaEstadoMesContable(int Codigo, DateTime Fecha, int TipoModulo, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ValidaMesContable");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@dFechaActual", SqlDbType.Date).Value = Fecha;               
                    cmd.Parameters.Add("@nTipoModulo", SqlDbType.Int).Value = TipoModulo;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DACommons MetodoEjecutado: ValidaEstadoMesContable - " + ex.Message;
                return false;
            }
        }
        public bool AperturaCierreContable(int CodigoEmpresa, int Anio,int Mes,bool Estado, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistraAperturaCierreContable");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@nAnioCon", SqlDbType.Int).Value = Anio;
                    cmd.Parameters.Add("@nMesCon", SqlDbType.Int).Value = Mes;
                    cmd.Parameters.Add("@bEstado", SqlDbType.Bit).Value = Estado;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DACommons MetodoEjecutado: AperturaCierreContable - " + ex.Message;
                return false;
            }
        }
        public bool EnvioComprobantesOffLine(int Codigo, int TipoComprobante, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EnvioComprobanteOffLine");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@TipoComprobante", SqlDbType.Int).Value = TipoComprobante;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DACommons MetodoEjecutado: EnvioComprobantesOffLine - " + ex.Message;
                return false;
            }
        }
        public DataTable MostrarFamilia(int Tipo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarFamiliaproductos");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Tipo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable MostrarSubFamilia(string Familia)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarFamiliaproductos");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Familia", SqlDbType.VarChar).Value = Familia;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
    }
}
