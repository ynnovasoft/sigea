﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;

namespace Data
{
  public  class DAGuide
    {
        public bool Registrar(ref BEGuia oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarGuia");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCli", SqlDbType.Int).Value = oDato.gnCodCli;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = oDato.gnCodCot;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = oDato.gnTipMon;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = oDato.gdFecReg;
                    cmd.Parameters.Add("@cTipGuia", SqlDbType.VarChar).Value = oDato.gcTipGuia;
                    cmd.Parameters.Add("@cMotTrasl", SqlDbType.VarChar).Value = oDato.gcMotTrasl;
                    cmd.Parameters.Add("@cDirPart", SqlDbType.VarChar).Value = oDato.gcDirPart;
                    cmd.Parameters.Add("@nCodDirCli", SqlDbType.Int).Value = oDato.gnCodDirCli;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.gnCodigoAgencia;
                    cmd.Parameters.Add("@nCodSer", SqlDbType.Int).Value = oDato.gnCodSer;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@cTipDocAdj", SqlDbType.VarChar).Value = oDato.gcTipDocAdj;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.gnCodEmp;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@bParcial", SqlDbType.VarChar).Value = oDato.gbParcial;
                    cmd.Parameters.Add("@bElectronico", SqlDbType.VarChar).Value = oDato.gbElectronico;
                    cmd.Parameters.Add("@dFecTras", SqlDbType.Date).Value = oDato.gdFecTras;
                    cmd.Parameters.Add("@cModTras", SqlDbType.VarChar).Value = oDato.gcModTras;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@nCodGuia", SqlDbType.Int).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gnCodGuia = cmd.Parameters["@nCodGuia"].Value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Anular( BEGuia oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AnularGuia");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 120;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodGuia", SqlDbType.Int).Value = oDato.gnCodGuia;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cMotAnu", SqlDbType.VarChar).Value = oDato.gcMotAnu;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Facturar( ref BEGuia oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_FacturarGuia");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodGuia", SqlDbType.Int).Value = oDato.gnCodGuia;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cNumGuia", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gcNumGuia = cmd.Parameters["@cNumGuia"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable Listar(DateTime  dFecIni , DateTime  dFecFin , string  Valor , int nCodigoAgencia, int nTipoBusqueda ,
                                 int CodigoEmpresa )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarGuia");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = dFecFin;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = nCodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool AgregarProducto(int CodigoGuia , int CodigoProducto ,   decimal  Cantidad ,ref string  cMensaje )
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarProductoGuia");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodGuia", SqlDbType.Int).Value = CodigoGuia;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarProducto(int CodigoGuia, int CodigoProducto, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProductoGuia");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodGuia", SqlDbType.Int).Value = CodigoGuia;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ModificarProducto(int CodigoGuia, int CodigoProducto, decimal Cantidad, decimal  CantidadAnt ,
            string  Descripcion ,ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoGuia");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodGuia", SqlDbType.Int).Value = CodigoGuia;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@CantidadAnt", SqlDbType.Decimal).Value = CantidadAnt;
                    cmd.Parameters.Add("@cDescripcion", SqlDbType.VarChar).Value = Descripcion;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable RecuperarDetalle(int CodigoGuia)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperarDetalleGuia");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodGuia", SqlDbType.Int).Value = CodigoGuia;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }



    }
}
