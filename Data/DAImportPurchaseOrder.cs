﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;

namespace Data
{
   public  class DAImportPurchaseOrder
    {
        public bool Registrar(ref BEOrdenCompraImportacion oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarOrdenCompraImportacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@cObservaciones", SqlDbType.VarChar).Value = oDato.gcObservaciones;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = oDato.gnTipMon;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = oDato.gdFecReg;
                    cmd.Parameters.Add("@nCodProve", SqlDbType.Int).Value = oDato.gnCodProve;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.gnCodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.gnCodEmp;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cNumOrd", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gnCodOrdCom = cmd.Parameters["@nCodOrdCom"].Value;
                        oDato.gcNumOrd = cmd.Parameters["@cNumOrd"].Value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable Listar(DateTime  dFecIni , DateTime  dFecFin , string  Valor,  int  nTipoBusqueda, int  nEstado ,  int CodigoEmpresa )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarOrdenComprasImportacion");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = dFecFin;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nEstado", SqlDbType.Int).Value = nEstado;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable RecuperarDetalle( int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperarDetalleOrdenCompraImportacion");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool AgregarProducto(int  Codigo , int CodigoProducto,  decimal  Cantidad , decimal  Precio , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarProductoOrdenCompraImportacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarProducto(int Codigo, int CodigoProducto, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProductoOrdenCompraImportacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ModificarProducto(int Codigo, int CodigoProducto, decimal Cantidad,decimal CantidadAnt, decimal Precio, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoOrdenCompraImportacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@CantidadAnt", SqlDbType.Decimal).Value = CantidadAnt;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }



    }
}
