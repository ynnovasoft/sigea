﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;

namespace Data
{
  public   class DAOrder
    {
        public bool Registrar(ref BEPedido oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    if  (oDato.gcTipCom.ToString() == "07" | oDato.gcTipCom.ToString()  == "08" )
                    {
                        cmd.Parameters.Add("@cTipoComRef", SqlDbType.VarChar).Value = oDato.gcTipoComRef;
                        cmd.Parameters.Add("@cNumComRef", SqlDbType.VarChar).Value = oDato.gcNumComRef;
                        cmd.Parameters.Add("@cCodTipNot", SqlDbType.VarChar).Value = oDato.gcCodTipNot;
                        cmd.Parameters.Add("@cMotNot", SqlDbType.VarChar).Value = oDato.gcMotNot;
                    }
                    else
                    {
                        cmd.Parameters.Add("@cTipoComRef", SqlDbType.VarChar).Value = "";
                        cmd.Parameters.Add("@cNumComRef", SqlDbType.VarChar).Value = "";
                        cmd.Parameters.Add("@cCodTipNot", SqlDbType.VarChar).Value = "";
                        cmd.Parameters.Add("@cMotNot", SqlDbType.VarChar).Value = "";
                    }
                    cmd.Parameters.Add("@nCodDocRef", SqlDbType.Int).Value = oDato.gnCodDocRef;
                    cmd.Parameters.Add("@nCodVen", SqlDbType.Int).Value = oDato.gnCodVen;
                    cmd.Parameters.Add("@cTipDocAdj", SqlDbType.VarChar).Value = oDato.gcTipDocAdj;
                    cmd.Parameters.Add("@cOrdComp", SqlDbType.VarChar).Value = oDato.gcOrdComp;
                    cmd.Parameters.Add("@bAplGuia", SqlDbType.Bit).Value = oDato.gbAplGuia;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@cTipOpe", SqlDbType.VarChar).Value = oDato.gcTipOpe;
                    cmd.Parameters.Add("@nTipAfecIGV", SqlDbType.Int).Value = oDato.gnTipAfecIGV;
                    cmd.Parameters.Add("@nCodCli", SqlDbType.Int).Value = oDato.gnCodCli;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = oDato.gnCodCot;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = oDato.gdFecReg;
                    cmd.Parameters.Add("@dFecVen", SqlDbType.Date).Value = oDato.gdFecVen;
                    cmd.Parameters.Add("@nTipCam", SqlDbType.Money).Value = oDato.gnTipCam;
                    cmd.Parameters.Add("@nCondPag", SqlDbType.Int).Value = oDato.gnCondPag;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = oDato.gnTipMon;
                    cmd.Parameters.Add("@cTipCom", SqlDbType.VarChar).Value = oDato.gcTipCom;
                    cmd.Parameters.Add("@nCodSer", SqlDbType.Int).Value = oDato.gnCodSer;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.gnCodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.gnCodEmp;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cCodGui", SqlDbType.VarChar).Value = oDato.gcCodGui;
                    cmd.Parameters.Add("@bAplDedAnt", SqlDbType.Bit).Value = oDato.gbAplDedAnt;
                    cmd.Parameters.Add("@bVenSer", SqlDbType.Bit).Value = oDato.gbVenSer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gnCodPed = cmd.Parameters["@nCodPed"].Value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Anular(BEPedido oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AnularPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = oDato.gnCodPed;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cMotAnu", SqlDbType.VarChar).Value = oDato.gcMotAnu;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Facturar(ref BEPedido oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_FacturarPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = oDato.gnCodPed;
                    cmd.Parameters.Add("@nCodCli", SqlDbType.Int).Value = oDato.gnCodCli;
                    cmd.Parameters.Add("@nSubTot", SqlDbType.Money).Value = oDato.gnSubTot;
                    cmd.Parameters.Add("@nMonDes", SqlDbType.Money).Value = oDato.gnMonDes;
                    cmd.Parameters.Add("@nMonAnt", SqlDbType.Money).Value = oDato.gnMonAnt;
                    cmd.Parameters.Add("@nVenGra", SqlDbType.Money).Value = oDato.gnVenGra;
                    cmd.Parameters.Add("@nIGV", SqlDbType.Money).Value = oDato.gnIGV;
                    cmd.Parameters.Add("@nImpTot", SqlDbType.Money).Value = oDato.gnImpTot;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@nMonRet", SqlDbType.Money).Value = oDato.gnMonRet;
                    cmd.Parameters.Add("@nImpNetPag", SqlDbType.Money).Value = oDato.gnImpNetPag;
                    cmd.Parameters.Add("@nCondPag", SqlDbType.Int).Value = oDato.gnCondPag;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@bAplDet", SqlDbType.Bit).Value = oDato.gbAplDet;
                    cmd.Parameters.Add("@cOrdComp", SqlDbType.VarChar).Value = oDato.gcOrdComp;
                    cmd.Parameters.Add("@cTipCom", SqlDbType.VarChar).Value = oDato.gcTipCom;
                    cmd.Parameters.Add("@nCodVen", SqlDbType.Int).Value = oDato.gnCodVen;
                    cmd.Parameters.Add("@bAplGuia", SqlDbType.Bit).Value = oDato.gbAplGuia;
                    cmd.Parameters.Add("@nCodSer", SqlDbType.Int).Value = oDato.gnCodSer;
                    cmd.Parameters.Add("@dFecVen", SqlDbType.Date).Value = oDato.gdFecVen;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cNumComp", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gcNumComp = cmd.Parameters["@cNumComp"].Value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Clonar(ref BEPedido oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ClonarPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPedtAnt", SqlDbType.Int).Value = oDato.gnCodPed;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gnCodPed = cmd.Parameters["@nCodPed"].Value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool AgregarProducto(int  Codigo, int  CodigoProducto ,  decimal  Cantidad , decimal  Precio , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarProductoPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarProducto(int Codigo, int CodigoProducto,ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProductoPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ModificarProducto(int Codigo, int CodigoProducto, decimal Cantidad, decimal CantidadAnt, decimal Precio,
            decimal  PorDes ,  string  Descripcion , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@CantidadAnt", SqlDbType.Decimal).Value = CantidadAnt;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@nPorDes", SqlDbType.Decimal).Value = PorDes;
                    cmd.Parameters.Add("@cDescripcion", SqlDbType.VarChar).Value = Descripcion;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable RecuperarDetalle(int  Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperarDetallePedido");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarCreditos(string  Valor , int nTipoBusqueda , bool  Pagado ,  int CodigoEmpresa, int CodigoAgencia)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarDocumentosCredito");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@bPagado", SqlDbType.Bit).Value = Pagado;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.Parameters.Add("@nCodAge", SqlDbType.Int).Value = CodigoAgencia;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarComprobantesFacturacion(DateTime  dFecIni , DateTime  dFecFin , string  Valor , int nTipoBusqueda , 
            int CodigoEmpresa , int CodigoAgencia )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarComprobantesFacturacion");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = dFecFin;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool AgregarProductoNotas(int  Codigo , decimal Precio , string  Descripcion  , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarProductoPedidoNotas");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cDescripcion", SqlDbType.VarChar).Value = Descripcion;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable ObtenerGuias(int Codigo , int Tipo , string  Guias, string TipoDocumentoRef)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerGuiasReferencia");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Tipo;
                cmd.Parameters.Add("@cCodGui", SqlDbType.VarChar).Value = Guias;
                cmd.Parameters.Add("@cTipRefDoc", SqlDbType.VarChar).Value = TipoDocumentoRef;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerLetras(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerLetrasReferencia");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool ModificarProductoAnticipo(int CodigoAnticipo, int Codigo, int CodigoProducto,  ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoAnticipoPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodAnt", SqlDbType.Int).Value = CodigoAnticipo;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable ListarCuotas(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarCuotasComprobante");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool RegistrarCuota(int  Codigo , string  CodigoCuota , decimal  Monto , DateTime  Fecha , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarCuotas");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@dFecVen", SqlDbType.Date).Value = Fecha;
                    cmd.Parameters.Add("@nMonto", SqlDbType.Money).Value = Monto;
                    cmd.Parameters.Add("@cCodCuo", SqlDbType.VarChar).Value = CodigoCuota;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarCuota(int CodigoCuota,  ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarCuota");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPedCuo", SqlDbType.Int).Value = CodigoCuota;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ActualizarBaja(BEPedido oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ActualizarBaja");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = oDato.gnCodPed;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool AplicaRetencion(int Codigo, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AplicartencionPedido");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodPed", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }


    }
}
