﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;

namespace Data
{
    public class DAProducts
    {
        public DataTable ListarProductos(string Valor, int nTipoBusqueda, int nCodigoAgencia, int CodigoEmpresa, string Familia)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarProducto");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = nCodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.Parameters.Add("@cCodFam", SqlDbType.VarChar).Value = Familia;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarProductosMantenimiento(string Valor, int nTipoBusqueda)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarProductoMantenimiento");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ObtenerProducto(string Valor, int nTipoBusqueda, int nCodigoAgencia, int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerProducto");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarStockAlamcen(int CodProducto, int CodigoAgencia, int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_MostrarStockAlmacenes");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodProducto;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarInventarioProductos(string Valor, int nTipoBusqueda, bool Descuadre,
                                           int CodigoAgencia, int CodigoEmpresa, string CodigoUsuario)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarInventarioProductos");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@bDescuadre", SqlDbType.Bit).Value = Descuadre;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.Parameters.Add("@cCodUsusario", SqlDbType.VarChar).Value = CodigoUsuario;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarInventarioDescuadre(string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarInventarioDescuadre");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@cCodUsusario", SqlDbType.VarChar).Value = CodigoUsuario;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool GrabarProducto(BEProductos oDato, bool bNuevo, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd;
                    if (bNuevo == true)
                    {
                        cmd = new SqlCommand("PA_RegistrarProducto");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                        cmd.Parameters.Add("@nEmpresa", SqlDbType.VarChar).Value = oDato.gnCodEmp;
                    }
                    else
                    {
                        cmd = new SqlCommand("PA_ModificarProducto");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                        cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = oDato.gnCodProd;
                    }
                    cmd.Parameters.Add("@cCodCat", SqlDbType.VarChar).Value = oDato.gcCodCat;
                    cmd.Parameters.Add("@cDescripcion", SqlDbType.VarChar).Value = oDato.gcDescripcion;
                    cmd.Parameters.Add("@cMarca", SqlDbType.VarChar).Value = oDato.gcMarca;
                    cmd.Parameters.Add("@cUndMed", SqlDbType.VarChar).Value = oDato.gcUndMed;
                    cmd.Parameters.Add("@cSerie", SqlDbType.VarChar).Value = oDato.gcSerie;
                    cmd.Parameters.Add("@bModDes", SqlDbType.Bit).Value = oDato.gbModDes;
                    cmd.Parameters.Add("@cCodOriCom", SqlDbType.VarChar).Value = oDato.gcCodOriCom;
                    cmd.Parameters.Add("@cCodFam", SqlDbType.VarChar).Value = oDato.gcCodFam;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarProducto(BEProductos oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProducto");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = oDato.gnCodProd;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool GrabarProductoPrecio(BEProductos oDato, int Empresa, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoPrecio");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = oDato.gnCodProd;
                    cmd.Parameters.Add("@nValUni", SqlDbType.Decimal).Value = oDato.gnValUni;
                    cmd.Parameters.Add("@nPreUni", SqlDbType.Decimal).Value = oDato.gnPreUni;
                    cmd.Parameters.Add("@nStockMinimo", SqlDbType.VarChar).Value = oDato.gnStockMinimo;
                    cmd.Parameters.Add("@nMonDesUni", SqlDbType.Decimal).Value = oDato.gnMonDesUni;
                    cmd.Parameters.Add("@nMonDesMay", SqlDbType.Decimal).Value = oDato.gnMonDesMay;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = Empresa;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool AgregarCuadreInventario(int nCodProd, string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa,
            decimal nStockFisico, decimal nDiferencia, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarCuadreInventario");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = nCodProd;
                    cmd.Parameters.Add("@cCodUsusario", SqlDbType.VarChar).Value = CodigoUsuario;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@nStoFis", SqlDbType.Money).Value = nStockFisico;
                    cmd.Parameters.Add("@nDiferencia", SqlDbType.Money).Value = nDiferencia;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool QuitarCuadreInventario(int nCodProd, string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa,
         ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_QuitarCuadreInventario");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = nCodProd;
                    cmd.Parameters.Add("@cCodUsusario", SqlDbType.VarChar).Value = CodigoUsuario;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool GenerarCuadre(string  CodigoUsuario , int CodigoAgencia , int CodigoEmpresa , string  cCodPer ,
            string  cNumDoc ,  DateTime dFecReg ,
         ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_GenerarCuadreInventario");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@cCodUsusario", SqlDbType.VarChar).Value = CodigoUsuario;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = cCodPer;
                    cmd.Parameters.Add("@cNumDoc", SqlDbType.VarChar).Value = cNumDoc;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = dFecReg;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool UnificarProductos(string CodigoUsuario, int CodigoAgencia, int CodigoEmpresa, int CodigoProducto, 
            ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_UnificarProductos");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@cCodUsusario", SqlDbType.VarChar).Value = CodigoUsuario;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@nCodProdRea", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ReprocesarStock(int CodigoProducto, int CodigoAgencia, int CodigoEmpresa,ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ProcesarStockProducto");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DAProducts MetodoEjecutado: ReprocesarStock - " + ex.Message;
                return false;
            }
        }

    }
}
