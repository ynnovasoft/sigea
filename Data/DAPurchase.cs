﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace Data
{
    public class DAPurchase
    {
        public bool Registrar(ref BEPurchase oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@cNumComRef", SqlDbType.VarChar).Value = oDato.cNumComRef;
                    cmd.Parameters.Add("@cNumGuiRef", SqlDbType.VarChar).Value = oDato.cNumGuiRef;
                    cmd.Parameters.Add("@cObservaciones", SqlDbType.VarChar).Value = oDato.cObservaciones;
                    cmd.Parameters.Add("@cTipCom", SqlDbType.VarChar).Value = oDato.cTipCom;
                    cmd.Parameters.Add("@cTipMov", SqlDbType.VarChar).Value = oDato.cTipMov;
                    cmd.Parameters.Add("@nTipCam", SqlDbType.Money).Value = oDato.nTipCam;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = oDato.nTipMon;
                    cmd.Parameters.Add("@dFecEmi", SqlDbType.Date).Value = oDato.dFecEmi;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = oDato.dFecReg;
                    cmd.Parameters.Add("@dFecVen", SqlDbType.Date).Value = oDato.dFecVen;
                    cmd.Parameters.Add("@nCodProve", SqlDbType.Int).Value = oDato.nCodProve;
                    cmd.Parameters.Add("@nCondPag", SqlDbType.Int).Value = oDato.nCondPag;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.nCodigoAgencia;
                    cmd.Parameters.Add("@nCodAgeIng", SqlDbType.Int).Value = oDato.nCodAgeIng;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.cUsuRegAud;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = oDato.nCodOrdCom;
                    cmd.Parameters.Add("@cTipDocAdj", SqlDbType.VarChar).Value = oDato.cTipDocAdj;
                    cmd.Parameters.Add("@bIngPar", SqlDbType.Bit).Value = oDato.bIngPar;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.nCodEmp;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.cCodPer;
                    cmd.Parameters.Add("@bServicio", SqlDbType.Bit).Value = oDato.bServicio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@nCodCom", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cNumCom", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.nCodCom = Convert.ToInt32(cmd.Parameters["@nCodCom"].Value);
                        oDato.cNumCom = cmd.Parameters["@cNumCom"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Anular(BEPurchase oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AnularCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 180;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCom", SqlDbType.Int).Value = oDato.nCodCom;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.cUsuActAud;
                    cmd.Parameters.Add("@cMotAnu", SqlDbType.VarChar).Value = oDato.cMotAnu;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.cCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Facturar(BEPurchase oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_FacturarCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 180;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCom", SqlDbType.Int).Value = oDato.nCodCom;
                    cmd.Parameters.Add("@nVenGra", SqlDbType.Money).Value = oDato.nVenGra;
                    cmd.Parameters.Add("@nIGV", SqlDbType.Money).Value = oDato.nIGV;
                    cmd.Parameters.Add("@nImpTot", SqlDbType.Money).Value = oDato.nImpTot;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.cCodPer;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.cUsuActAud;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable Listar(DateTime dFecIni, DateTime dFecFin, string Valor, int nTipoBusqueda, int CodigoEmpresa, bool Servicio)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarCompras");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = dFecFin;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.Parameters.Add("@bServicio", SqlDbType.Bit).Value = Servicio;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool AgregarProducto(int   Codigo , int  CodigoProducto ,   decimal  Cantidad , decimal  Precio , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarProductoCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarProducto(int Codigo, int CodigoProducto, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProductoCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ModificarProducto(int Codigo, int CodigoProducto, decimal Cantidad,decimal CantidadAnt, decimal Precio,
            ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@CantidadAnt", SqlDbType.Decimal).Value = CantidadAnt;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable RecuperarDetalle( int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperarDetalleCompra");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodCom", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarCreditos(string  Valor , int  nTipoBusqueda , bool  Pagado , int CodigoEmpresa )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarDocumentosCreditoProveedor");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@bPagado", SqlDbType.Bit).Value = Pagado;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }


    }
}
