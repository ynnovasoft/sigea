﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;

namespace Data
{
    public class DAPurchaseOrder
    {
        public bool Registrar(ref BEOrdenCompra oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarOrdenCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@cObservaciones", SqlDbType.VarChar).Value = oDato.gcObservaciones;
                    cmd.Parameters.Add("@nTipCam", SqlDbType.Money).Value = oDato.gnTipCam;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = oDato.gnTipMon;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = oDato.gdFecReg;
                    cmd.Parameters.Add("@dFecVen", SqlDbType.Date).Value = oDato.gdFecVen;
                    cmd.Parameters.Add("@nCodProve", SqlDbType.Int).Value = oDato.gnCodProve;
                    cmd.Parameters.Add("@nCondPag", SqlDbType.Int).Value = oDato.gnCondPag;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.gnCodigoAgencia;
                    cmd.Parameters.Add("@cTipMov", SqlDbType.VarChar).Value = oDato.gcTipMov;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.gnCodEmp;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cNumOrd", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gnCodOrdCom = cmd.Parameters["@nCodOrdCom"].Value;
                        oDato.gcNumOrd = cmd.Parameters["@cNumOrd"].Value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Anular( BEOrdenCompra oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AnularOrdenCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 120;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = oDato.gnCodOrdCom;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Facturar(BEOrdenCompra oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_FacturarOrdenCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = oDato.gnCodOrdCom;
                    cmd.Parameters.Add("@nVenGra", SqlDbType.Money).Value = oDato.gnVenGra;
                    cmd.Parameters.Add("@nIGV", SqlDbType.Money).Value = oDato.gnIGV;
                    cmd.Parameters.Add("@nImpTot", SqlDbType.Money).Value = oDato.gnImpTot;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable Listar(DateTime  dFecIni, DateTime  dFecFin , string  Valor ,int  nTipoBusqueda , int  nEstado , int CodigoEmpresa )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarOrdenCompras");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = dFecFin;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nEstado", SqlDbType.Int).Value = nEstado;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool AgregarProducto(int  Codigo , int  CodigoProducto ,   decimal Cantidad , decimal  Precio , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarProductoOrdenCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarProducto(int Codigo, int CodigoProducto, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProductoOrdenCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ModificarProducto(int Codigo, int CodigoProducto, decimal Cantidad, decimal CantidadAnt, decimal Precio, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoOrdenCompra");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@CantidadAnt", SqlDbType.Decimal).Value = CantidadAnt;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable RecuperarDetalle(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperarDetalleOrdenCompra");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodOrdCom", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }


    }
}
