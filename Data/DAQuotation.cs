﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;
namespace Data
{
    public class DAQuotation
    {
        public DataTable Listar(DateTime dFecIni, DateTime dFecFin, string Valor, int nCodigoAgencia, int nTipoBusqueda,
            int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarCotizacion");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = dFecFin;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = nCodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable RecuperarDetalle(int Codigo)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperarDetalleCotizacion");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = Codigo;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool Registrar(ref BECotizacion oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarCotizacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCli", SqlDbType.Int).Value = oDato.gnCodCli;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = oDato.gdFecReg;
                    cmd.Parameters.Add("@dFecVen", SqlDbType.Date).Value = oDato.gdFecVen;
                    cmd.Parameters.Add("@nTipCam", SqlDbType.Money).Value = oDato.gnTipCam;
                    cmd.Parameters.Add("@nCondPag", SqlDbType.Int).Value = oDato.gnCondPag;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = oDato.gnTipMon;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.gnCodigoAgencia;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.gnCodEmp;
                    cmd.Parameters.Add("@cTieEnt", SqlDbType.VarChar).Value = oDato.gcTieEnt;
                    cmd.Parameters.Add("@cObservaciones", SqlDbType.VarChar).Value = oDato.gcObservaciones;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cNumCot", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gnCodCot = Convert.ToInt32(cmd.Parameters["@nCodCot"].Value);
                        oDato.gcNumCot = cmd.Parameters["@cNumCot"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Anular( BECotizacion oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AnularCotizacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = oDato.gnCodCot;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Editar( BECotizacion oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EditarCotizacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = oDato.gnCodCot;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Facturar( BECotizacion oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_FacturarCotizacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCli", SqlDbType.Int).Value = oDato.gnCodCli;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = oDato.gnCodCot;
                    cmd.Parameters.Add("@nSubTot", SqlDbType.Money).Value = oDato.gnSubTot;
                    cmd.Parameters.Add("@nMonDes", SqlDbType.Money).Value = oDato.gnMonDes;
                    cmd.Parameters.Add("@nVenGra", SqlDbType.Money).Value = oDato.gnVenGra;
                    cmd.Parameters.Add("@nIGV", SqlDbType.Money).Value = oDato.gnIGV;
                    cmd.Parameters.Add("@nImpTot", SqlDbType.Money).Value = oDato.gnImpTot;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuActAud;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@nCondPag", SqlDbType.Int).Value = oDato.gnCondPag;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Clonar(ref BECotizacion oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ClonarCotizacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCotAnt", SqlDbType.Int).Value = oDato.gnCodCot;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.gcCodPer;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.gcUsuRegAud;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cNumCot", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.gnCodCot = Convert.ToInt32(cmd.Parameters["@nCodCot"].Value);
                        oDato.gcNumCot = cmd.Parameters["@cNumCot"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool AgregarProducto(int Codigo , int CodigoProducto , decimal Cantidad , decimal Precio , ref string cMensaje )
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarProductoCotizacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarProducto(int Codigo , int CodigoProducto, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProductoCotizacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ModificarProducto(int Codigo, int CodigoProducto, decimal Cantidad, decimal Precio, decimal PorDes ,
                              string Descripcion, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoCotizacion");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodCot", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@nPorDes", SqlDbType.Decimal).Value = PorDes;
                    cmd.Parameters.Add("@cDescripcion", SqlDbType.VarChar).Value = Descripcion;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
    }
}
