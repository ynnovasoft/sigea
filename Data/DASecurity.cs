﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace Data
{
   public  class DASecurity
    {
        public DataTable RecuperaDatosLogueo(string cUsuario , int CodigoAgencia, int CodigoEmpresa , string  CodigoPersonal )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperaDatosLogueo");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@cUsuario", SqlDbType.VarChar).Value = cUsuario;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = CodigoPersonal;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable RecuperaMenu(int CodigoUsuario , int CodigoAgencia, int CodigoEmpresa )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperaMenus");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodUsuario", SqlDbType.Int).Value = CodigoUsuario;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool RegistraPermisos(DataTable  Dato ,int CodigoUsuario ,int CodigoAgencia ,int CodigoEmpresa ,ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistraPermisosMenu");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    Module xml = new Module();
                    cmd.Parameters.Add("@Data", SqlDbType.Xml).Value = xml.DevuelveXml(Dato);
                    cmd.Parameters.Add("@nCodUsuario", SqlDbType.Int).Value = CodigoUsuario;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DASecurity MetodoEjecutado: RegistraPermisos - " + ex.Message;
                return false;
            }
        }
        public bool  ValidaIngresoSistema(string  cUsuario , string  cPassword , int CodigoAgencia ,
                                  int CodigoEmpresa , ref int Respuesta, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ValidaIngresoSistema");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@cUsuario", SqlDbType.VarChar).Value = cUsuario;
                    cmd.Parameters.Add("@cPassword", SqlDbType.VarChar).Value = cPassword;
                    cmd.Parameters.Add("@CodigoAgencia", SqlDbType.Int).Value = CodigoAgencia;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                    cmd.Parameters.Add("@nRespuesta", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        Respuesta = Convert.ToInt16(cmd.Parameters["@nRespuesta"].Value);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DASecurity MetodoEjecutado: ValidaIngresoSistema - " + ex.Message;
                return false;
            }
        }
    }
}
