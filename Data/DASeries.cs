﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace Data
{
   public class DASeries
    {
        public DataTable Listar(int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarSeries");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool Grabar(BESerie oDato, bool bNuevo, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd;
                    if (bNuevo == true)
                    {
                        cmd = new SqlCommand("PA_RegistrarSerie");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                        cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.nCodEmp;
                    }
                    else
                    {
                        cmd = new SqlCommand("PA_ModificarSerie");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                        cmd.Parameters.Add("@nCodSer", SqlDbType.Int).Value = oDato.nCodSer;
                    }
                    cmd.Parameters.Add("@cNumCor", SqlDbType.VarChar).Value = oDato.cNumCor;
                    cmd.Parameters.Add("@cObservaciones", SqlDbType.VarChar).Value = oDato.cObservaciones;
                    cmd.Parameters.Add("@cSerie", SqlDbType.VarChar).Value = oDato.cSerie;
                    cmd.Parameters.Add("@cTipCom", SqlDbType.VarChar).Value = oDato.cTipCom;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.nCodigoAgencia;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Eliminar(BESerie oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarSerie");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodSer", SqlDbType.Int).Value = oDato.nCodSer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
    }
}
