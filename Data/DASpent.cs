﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;

namespace Data
{
   public class DASpent
    {
        public bool Registrar(BEGasto oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarGasto");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@cUsuReg", SqlDbType.VarChar).Value = oDato.gcUsuReg;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = oDato.gdFecReg;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.gnCodigoAgencia;
                    cmd.Parameters.Add("@nMedPag", SqlDbType.Int).Value = oDato.gnMedPag;
                    cmd.Parameters.Add("@nTipGas", SqlDbType.Int).Value = oDato.gnTipGas;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = oDato.gnTipMon;
                    cmd.Parameters.Add("@nMonto", SqlDbType.Money).Value = oDato.gnMonto;
                    cmd.Parameters.Add("@nFonDesc", SqlDbType.Int).Value = oDato.gnFonDesc;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.gnCodEmp;
                    cmd.Parameters.Add("@cObservaciones", SqlDbType.VarChar).Value = oDato.gcObservaciones;
                    cmd.Parameters.Add("@cNumCom", SqlDbType.VarChar).Value = oDato.gcNumCom;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public DataTable Listar(DateTime  FechaIni , DateTime  FechaFin, int CodigoEmpresa )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarGastos");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = FechaIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = FechaFin;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool Eliminar(int Codigo, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarGastos");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }



    }
}
