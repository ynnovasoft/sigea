﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entities;

namespace Data
{
    public class DAStore
    {
        public DataTable Listar(DateTime dFecIni, DateTime dFecFin, string Valor, int nCodigoAgencia, int nTipoBusqueda,
                                 int TipMov, int CodigoEmpresa,bool Servicio)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarAlmacen");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = dFecFin;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = nCodigoAgencia;
                cmd.Parameters.Add("@nTipMov", SqlDbType.Int).Value = TipMov;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.Parameters.Add("@bServicio", SqlDbType.Bit).Value = Servicio;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarMovimientos(DateTime dFecIni, DateTime dFecFin, int Codigo, int Agencia, int nTipoMovimiento,
            int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarAlmacenMovimiento");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 120;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFecIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@dFecFin", SqlDbType.Date).Value = dFecFin;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@nTipoMovimiento", SqlDbType.Int).Value = nTipoMovimiento;
                cmd.Parameters.Add("@Agencia", SqlDbType.Int).Value = Agencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ConsultaStockRegistradoProducto(int Codigo, int Movimiento, int Agencia)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ConsultaStockRegistradoProducto");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Codigo", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@Movimiento", SqlDbType.Int).Value = Movimiento;
                cmd.Parameters.Add("@Agencia", SqlDbType.Int).Value = Agencia;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarTraslados(string Valor, int nTipoBusqueda, int nCodigoAgencia,
                                string MotivoTraslado, int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarAlmacenIngreso");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = nCodigoAgencia;
                cmd.Parameters.Add("@cMotTrasl", SqlDbType.VarChar).Value = MotivoTraslado;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable ListarStockAnterior(DateTime dFecIni, int Codigo, int Agencia,
                                        int CodigoEmpresa)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RptKardexDetalladoProdStockAnt");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@dFechaIni", SqlDbType.Date).Value = dFecIni;
                cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = Codigo;
                cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = Agencia;
                cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = CodigoEmpresa;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable RecuperarDetalle(int CodigoAlmacen)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_RecuperarDetalleAlmacen");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@nCodAlm", SqlDbType.Int).Value = CodigoAlmacen;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool Registrar(ref BEStore oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_RegistrarAlmacen");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodigoAgencia", SqlDbType.Int).Value = oDato.nCodigoAgencia;
                    cmd.Parameters.Add("@nCodAgeDes", SqlDbType.Int).Value = oDato.nCodAgeDes;
                    cmd.Parameters.Add("@nCodCliProv", SqlDbType.Int).Value = oDato.nCodCliProv;
                    cmd.Parameters.Add("@cMotTrasl", SqlDbType.VarChar).Value = oDato.cMotTrasl;
                    cmd.Parameters.Add("@dFecReg", SqlDbType.Date).Value = oDato.dFecReg;
                    cmd.Parameters.Add("@nTipMov", SqlDbType.Int).Value = oDato.nTipMov;
                    cmd.Parameters.Add("@nTipCam", SqlDbType.Money).Value = oDato.nTipCam;
                    cmd.Parameters.Add("@nTipMon", SqlDbType.Int).Value = oDato.nTipMon;
                    cmd.Parameters.Add("@nCodigoDocRef", SqlDbType.Int).Value = oDato.nCodigoDocRef;
                    cmd.Parameters.Add("@cTipDocRef", SqlDbType.VarChar).Value = oDato.cTipDocRef;
                    cmd.Parameters.Add("@cObservaciones", SqlDbType.VarChar).Value = oDato.cObservaciones;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.cUsuRegAud;
                    cmd.Parameters.Add("@nCodEmp", SqlDbType.Int).Value = oDato.nCodEmp;
                    cmd.Parameters.Add("@nCodEmpDes", SqlDbType.Int).Value = oDato.nCodEmpDes;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.cCodPer;
                    cmd.Parameters.Add("@bServicio", SqlDbType.Bit).Value = oDato.bServicio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@nCodAlm", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@cNumAlm", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        oDato.nCodalm = Convert.ToInt32(cmd.Parameters["@nCodAlm"].Value);
                        oDato.cNumAlm = cmd.Parameters["@cNumAlm"].Value.ToString ();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Anular( BEStore oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AnularAlmacen");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 120;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodAlm", SqlDbType.Int).Value = oDato.nCodalm;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.cUsuActAud;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.cCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Facturar(ref BEStore oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_FacturarAlmacen");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodAlm", SqlDbType.Int).Value = oDato.nCodalm;
                    cmd.Parameters.Add("@nVenGra", SqlDbType.Money).Value = oDato.nVenGra;
                    cmd.Parameters.Add("@nIGV", SqlDbType.Money).Value = oDato.nIGV;
                    cmd.Parameters.Add("@nImpTot", SqlDbType.Money).Value = oDato.nImpTot;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = oDato.cUsuActAud;
                    cmd.Parameters.Add("@cCodPer", SqlDbType.VarChar).Value = oDato.cCodPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool AgregarProducto(int CodigoAlmacen , int CodigoProducto,decimal Cantidad, decimal Precio , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_AgregarProductoAlmacen");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodAlm", SqlDbType.Int).Value = CodigoAlmacen;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool EliminarProducto(int Codigo, int CodigoProducto, bool EliminaItem, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProductoAlmacen");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodAlm", SqlDbType.Int).Value = Codigo;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@bEditaItem", SqlDbType.Bit).Value = EliminaItem;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool ModificarProducto(int CodigoAlmacen, int CodigoProducto, decimal Cantidad,decimal CantidadAnt,
            decimal Precio, string Descripcion , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_ModificarProductoAlmacen");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodAlm", SqlDbType.Int).Value = CodigoAlmacen;
                    cmd.Parameters.Add("@nCodProd", SqlDbType.Int).Value = CodigoProducto;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad;
                    cmd.Parameters.Add("@CantidadAnt", SqlDbType.Decimal).Value = CantidadAnt;
                    cmd.Parameters.Add("@nPrecio", SqlDbType.Decimal).Value = Precio;
                    cmd.Parameters.Add("@cDescripcion", SqlDbType.VarChar).Value = Descripcion;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }



    }
}
