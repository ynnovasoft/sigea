﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entidades;
using Entities;

namespace Data
{
   public  class DASupplier
    {
        public DataTable Obtener(string  Valor, int  nTipoBusqueda )
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ObtenerProveedor");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public DataTable Listar(string Valor, int nTipoBusqueda)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarProveedor");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.Add("@Valor", SqlDbType.VarChar).Value = Valor;
                cmd.Parameters.Add("@nTipoBusqueda", SqlDbType.Int).Value = nTipoBusqueda;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool Grabar(BEProveedor oDato, bool bNuevo, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd;
                    if (bNuevo == true)
                    {
                        cmd = new SqlCommand("PA_RegistrarProveedor");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                    }
                    else
                    {
                        cmd = new SqlCommand("PA_ModificarProveedor");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                        cmd.Parameters.Add("@nCodProve", SqlDbType.Int).Value = oDato.gnCodProve;
                    }
                    cmd.Parameters.Add("@cApeMat", SqlDbType.VarChar).Value = oDato.gcApeMat;
                    cmd.Parameters.Add("@cApePat", SqlDbType.VarChar).Value = oDato.gcApePat;
                    cmd.Parameters.Add("@cContactos", SqlDbType.VarChar).Value = oDato.gcContactos;
                    cmd.Parameters.Add("@cCorreo", SqlDbType.VarChar).Value = oDato.gcCorreo;
                    cmd.Parameters.Add("@cDirProv", SqlDbType.VarChar).Value = oDato.gcDirProv;
                    cmd.Parameters.Add("@cNombres", SqlDbType.VarChar).Value = oDato.gcNombres;
                    cmd.Parameters.Add("@cNumDoc", SqlDbType.VarChar).Value = oDato.gcNumDoc;
                    cmd.Parameters.Add("@cRazSoc", SqlDbType.VarChar).Value = oDato.gcRazSoc;
                    cmd.Parameters.Add("@cTipDocProv", SqlDbType.VarChar).Value = oDato.gcTipDocProv;
                    cmd.Parameters.Add("@nCodDep", SqlDbType.VarChar).Value = oDato.gnCodDep;
                    cmd.Parameters.Add("@nCodDis", SqlDbType.VarChar).Value = oDato.gnCodDis;
                    cmd.Parameters.Add("@nCodProv", SqlDbType.VarChar).Value = oDato.gnCodProv;
                    cmd.Parameters.Add("@nTipPer", SqlDbType.Int).Value = oDato.gnTipPer;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Eliminar(BEProveedor oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarProveedor");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodProve", SqlDbType.Int).Value = oDato.gnCodProve;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }
        public bool Eliminar(BEActive oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarActivos");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodAct", SqlDbType.VarChar).Value = oDato.nCodAct;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = ex.Message;
                return false;
            }
        }




    }
}
