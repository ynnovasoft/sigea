﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace Data
{
   public  class DAUser
    {
        public DataTable Listar()
        {
            string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("PA_ListarUsuarios");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adaptador.Fill(dt);
                return dt;
            }
        }
        public bool Grabar( BEUser oDato ,  bool bNuevo , ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd;
                    if (bNuevo == true)
                    {
                        cmd = new SqlCommand("PA_RegistrarUsuario");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                    }
                    else
                    {
                        cmd = new SqlCommand("PA_ModificarUsuario");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = cn;
                        cmd.Parameters.Add("@nCodUsuario", SqlDbType.Int).Value = oDato.nCodUsuario;
                    }                  
                    cmd.Parameters.Add("@cApePat", SqlDbType.VarChar).Value = oDato.cApePat;
                    cmd.Parameters.Add("@cApeMat", SqlDbType.VarChar).Value = oDato.cApeMat;
                    cmd.Parameters.Add("@cNombres", SqlDbType.VarChar).Value = oDato.cNombres;
                    cmd.Parameters.Add("@cCodUsuario", SqlDbType.VarChar).Value = oDato.cCodUsuario;
                    cmd.Parameters.Add("@cPassword", SqlDbType.VarChar).Value = oDato.cPassword;
                    cmd.Parameters.Add("@bActivo", SqlDbType.Bit).Value = oDato.bActivo;
                    cmd.Parameters.Add("@bVendedor", SqlDbType.Bit).Value = oDato.bVendedor;
                    cmd.Parameters.Add("@bPersonal", SqlDbType.Bit).Value = oDato.bPersonal;
                    cmd.Parameters.Add("@bUsuario", SqlDbType.Bit).Value = oDato.bUsuario;
                    cmd.Parameters.Add("@bActPre", SqlDbType.Bit).Value = oDato.bActPre;
                    cmd.Parameters.Add("@bGenExcCli", SqlDbType.Bit).Value = oDato.bGenExcCli;
                    cmd.Parameters.Add("@bAjuInv", SqlDbType.Bit).Value = oDato.bAjuInv;
                    cmd.Parameters.Add("@bPagSuc", SqlDbType.Bit).Value = oDato.bPagSuc;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DAUser MetodoEjecutado: Grabar - " + ex.Message;
                return false;
            }
        }
        public bool Eliminar(BEUser oDato, ref string cMensaje)
        {
            try
            {
                string strConnString = ConfigurationManager.ConnectionStrings["CadenaConexion_One"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(strConnString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("PA_EliminarUsuario");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@nCodUsuario", SqlDbType.Int).Value = oDato.nCodUsuario;
                    cmd.Parameters.Add("@cError", SqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        cMensaje = cmd.Parameters["@cError"].Value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                cMensaje = "Data-DAUser MetodoEjecutado: Eliminar - " + ex.Message;
                return false;
            }
        }
    }
}
