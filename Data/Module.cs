﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Data
{
   public class Module
    {
        public string DevuelveXml(DataTable oTable)
        {
            string DataXml;
            string DataXml_;
            DataXml = "<HANTRY>";
            DataXml_ = "";
            for (int i = 0; i <= oTable.Rows.Count - 1; i++)
            {
                DataXml += "<DATA";
                for (int j = 0; j <= oTable.Columns.Count - 1; j++)
                {
                    if (oTable.Columns[j].ColumnName.ToUpper() == "RutaDocbyte".ToUpper())
                        DataXml_ += " " + oTable.Columns[j].ColumnName + "=\"" + oTable.Rows[i][j].ToString() + "\"";
                    else
                        DataXml_ += " " + oTable.Columns[j].ColumnName + "=\"" + oTable.Rows[i][j].ToString() + "\"";
                }
                DataXml += DataXml_ + "></DATA>";
                DataXml_ = "";
            }
            DataXml += "</HANTRY>";
            if (oTable.Rows.Count == 0)
                DataXml = null;
            return DataXml;
        }

    }
}
