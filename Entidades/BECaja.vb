﻿Public Class BECaja
    Private dFecha As Date
    Private nTipCamCom As Double
    Private nTipCamVen As Double
    Private cUsuRegAud As String
    Private nCodigoAgencia As Integer
    Private bAperturado As Boolean
    Private nCodEmp As Integer
    Private nMonApe As Double
    Public Property gnMonApe
        Get
            Return nMonApe
        End Get
        Set(value)
            nMonApe = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property

    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property

    Public Property gbAperturado
        Get
            Return bAperturado
        End Get
        Set(value)
            bAperturado = value
        End Set
    End Property

    Public Property gnTipCamVen
        Get
            Return nTipCamVen
        End Get
        Set(value)
            nTipCamVen = value
        End Set
    End Property
    Public Property gcUsuRegAud
        Get
            Return cUsuRegAud
        End Get
        Set(value)
            cUsuRegAud = value
        End Set
    End Property
    Public Property gnTipCamCom
        Get
            Return nTipCamCom
        End Get
        Set(value)
            nTipCamCom = value
        End Set
    End Property
    Public Property gdFecha
        Get
            Return dFecha
        End Get
        Set(value)
            dFecha = value
        End Set
    End Property
End Class
