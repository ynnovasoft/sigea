﻿Public Class BECotizacion
    Private nCodCot As Integer
    Private nCodCli As Integer
    Private dFecReg As Date
    Private dFecVen As Date
    Private nTipCam As Double
    Private nCondPag As Integer
    Private nTipMon As Integer
    Private nVenGra As Double
    Private nIGV As Double
    Private nImpTot As Double
    Private nEstado As Integer
    Private cUsuRegAud As String
    Private cUsuActAud As String
    Private nCodigoAgencia As Integer
    Private cMensaje As String
    Private nCodEmp As Integer
    Private cNumCot As String
    Private nSubTot As Double
    Private nMonDes As Double
    Private cTieEnt As String
    Private cObservaciones As String
    Private cCodPer As String
    Public Property gcCodPer
        Get
            Return cCodPer
        End Get
        Set(value)
            cCodPer = value
        End Set
    End Property
    Public Property gcTieEnt
        Get
            Return cTieEnt
        End Get
        Set(value)
            cTieEnt = value
        End Set
    End Property
    Public Property gcObservaciones
        Get
            Return cObservaciones
        End Get
        Set(value)
            cObservaciones = value
        End Set
    End Property
    Public Property gnSubTot
        Get
            Return nSubTot
        End Get
        Set(value)
            nSubTot = value
        End Set
    End Property
    Public Property gnMonDes
        Get
            Return nMonDes
        End Get
        Set(value)
            nMonDes = value
        End Set
    End Property
    Public Property gcNumCot
        Get
            Return cNumCot
        End Get
        Set(value)
            cNumCot = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gcMensaje
        Get
            Return cMensaje
        End Get
        Set(value)
            cMensaje = value
        End Set
    End Property
    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property
    Public Property gcUsuActAud
        Get
            Return cUsuActAud
        End Get
        Set(value)
            cUsuActAud = value
        End Set
    End Property
    Public Property gcUsuRegAud
        Get
            Return cUsuRegAud
        End Get
        Set(value)
            cUsuRegAud = value
        End Set
    End Property

    Public Property gnCodCot
        Get
            Return nCodCot
        End Get
        Set(value)
            nCodCot = value
        End Set
    End Property
    Public Property gnCodCli
        Get
            Return nCodCli
        End Get
        Set(value)
            nCodCli = value
        End Set
    End Property
    Public Property gdFecReg
        Get
            Return dFecReg
        End Get
        Set(value)
            dFecReg = value
        End Set
    End Property
    Public Property gdFecVen
        Get
            Return dFecVen
        End Get
        Set(value)
            dFecVen = value
        End Set
    End Property
    Public Property gnTipCam
        Get
            Return nTipCam
        End Get
        Set(value)
            nTipCam = value
        End Set
    End Property
    Public Property gnCondPag
        Get
            Return nCondPag
        End Get
        Set(value)
            nCondPag = value
        End Set
    End Property
    Public Property gnTipMon
        Get
            Return nTipMon
        End Get
        Set(value)
            nTipMon = value
        End Set
    End Property
    Public Property gnVenGra
        Get
            Return nVenGra
        End Get
        Set(value)
            nVenGra = value
        End Set
    End Property
    Public Property gnIGV
        Get
            Return nIGV
        End Get
        Set(value)
            nIGV = value
        End Set
    End Property
    Public Property gnImpTot
        Get
            Return nImpTot
        End Get
        Set(value)
            nImpTot = value
        End Set
    End Property
    Public Property gnEstado
        Get
            Return nEstado
        End Get
        Set(value)
            nEstado = value
        End Set
    End Property
End Class
