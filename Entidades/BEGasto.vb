﻿Public Class BEGasto
    Private nCodGas As Integer
    Private dFecReg As Date
    Private cUsuReg As String
    Private nTipMon As Integer
    Private nCodigoAgencia As Integer
    Private nTipGas As Integer
    Private nMedPag As Integer
    Private nMonto As Double
    Private nFonDesc As Integer
    Private cObservaciones As String
    Private cNumCom As String
    Private nCodEmp As Integer
    Public Property gcNumCom
        Get
            Return cNumCom
        End Get
        Set(value)
            cNumCom = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gnFonDesc
        Get
            Return nFonDesc
        End Get
        Set(value)
            nFonDesc = value
        End Set
    End Property
    Public Property gcObservaciones
        Get
            Return cObservaciones
        End Get
        Set(value)
            cObservaciones = value
        End Set
    End Property
    Public Property gnMonto
        Get
            Return nMonto
        End Get
        Set(value)
            nMonto = value
        End Set
    End Property
    Public Property gnTipGas
        Get
            Return nTipGas
        End Get
        Set(value)
            nTipGas = value
        End Set
    End Property
    Public Property gnMedPag
        Get
            Return nMedPag
        End Get
        Set(value)
            nMedPag = value
        End Set
    End Property

    Public Property gnTipMon
        Get
            Return nTipMon
        End Get
        Set(value)
            nTipMon = value
        End Set
    End Property
    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property
    Public Property gdFecReg
        Get
            Return dFecReg
        End Get
        Set(value)
            dFecReg = value
        End Set
    End Property
    Public Property gcUsuReg
        Get
            Return cUsuReg
        End Get
        Set(value)
            cUsuReg = value
        End Set
    End Property
    Public Property gnCodGas
        Get
            Return nCodGas
        End Get
        Set(value)
            nCodGas = value
        End Set
    End Property
End Class
