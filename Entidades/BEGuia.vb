﻿Public Class BEGuia
    Private nCodGuia As Integer
    Private cNumGuia As String
    Private cTipGuia As String
    Private nCodCli As Integer
    Private nCodigoAgencia As Integer
    Private dFecReg As Date
    Private nCodSer As Integer
    Private cMotTrasl As String
    Private cDirPart As String
    Private nCodDirCli As Integer
    Private nEstado As Integer
    Private cMensaje As String
    Private cUsuActAud As String
    Private cUsuRegAud As String
    Private nCodCot As Integer
    Private cTipDocAdj As String
    Private Almacen As String
    Private cNumDocAdj As String
    Private cTipoCompAdj As String
    Private nTipMon As Integer
    Private nCodEmp As Integer
    Private cMotAnu As String
    Private cCodPer As String
    Private bParcial As Boolean
    Private bElectronico As Boolean
    Private dFecTras As Date
    Private cModTras As String
    Public Property gcModTras
        Get
            Return cModTras
        End Get
        Set(value)
            cModTras = value
        End Set
    End Property
    Public Property gdFecTras
        Get
            Return dFecTras
        End Get
        Set(value)
            dFecTras = value
        End Set
    End Property
    Public Property gbElectronico
        Get
            Return bElectronico
        End Get
        Set(value)
            bElectronico = value
        End Set
    End Property
    Public Property gbParcial
        Get
            Return bParcial
        End Get
        Set(value)
            bParcial = value
        End Set
    End Property
    Public Property gcCodPer
        Get
            Return cCodPer
        End Get
        Set(value)
            cCodPer = value
        End Set
    End Property
    Public Property gcMotAnu
        Get
            Return cMotAnu
        End Get
        Set(value)
            cMotAnu = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gnTipMon
        Get
            Return nTipMon
        End Get
        Set(value)
            nTipMon = value
        End Set
    End Property
    Public Property gcTipoCompAdj
        Get
            Return cTipoCompAdj
        End Get
        Set(value)
            cTipoCompAdj = value
        End Set
    End Property
    Public Property gcNumDocAdj
        Get
            Return cNumDocAdj
        End Get
        Set(value)
            cNumDocAdj = value
        End Set
    End Property
    Public Property gcTipDocAdj
        Get
            Return cTipDocAdj
        End Get
        Set(value)
            cTipDocAdj = value
        End Set
    End Property
    Public Property gnCodCot
        Get
            Return nCodCot
        End Get
        Set(value)
            nCodCot = value
        End Set
    End Property
    Public Property gcUsuRegAud
        Get
            Return cUsuRegAud
        End Get
        Set(value)
            cUsuRegAud = value
        End Set
    End Property
    Public Property gcUsuActAud
        Get
            Return cUsuActAud
        End Get
        Set(value)
            cUsuActAud = value
        End Set
    End Property
    Public Property gnCodGuia
        Get
            Return nCodGuia
        End Get
        Set(value)
            nCodGuia = value
        End Set
    End Property
    Public Property gcNumGuia
        Get
            Return cNumGuia
        End Get
        Set(value)
            cNumGuia = value
        End Set
    End Property
    Public Property gcTipGuia
        Get
            Return cTipGuia
        End Get
        Set(value)
            cTipGuia = value
        End Set
    End Property
    Public Property gnCodCli
        Get
            Return nCodCli
        End Get
        Set(value)
            nCodCli = value
        End Set
    End Property
    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property
    Public Property gdFecReg
        Get
            Return dFecReg
        End Get
        Set(value)
            dFecReg = value
        End Set
    End Property
    Public Property gnCodSer
        Get
            Return nCodSer
        End Get
        Set(value)
            nCodSer = value
        End Set
    End Property
    Public Property gcMotTrasl
        Get
            Return cMotTrasl
        End Get
        Set(value)
            cMotTrasl = value
        End Set
    End Property
    Public Property gcDirPart
        Get
            Return cDirPart
        End Get
        Set(value)
            cDirPart = value
        End Set
    End Property
    Public Property gnCodDirCli
        Get
            Return nCodDirCli
        End Get
        Set(value)
            nCodDirCli = value
        End Set
    End Property
    Public Property gnEstado
        Get
            Return nEstado
        End Get
        Set(value)
            nEstado = value
        End Set
    End Property

    Public Property gcMensaje
        Get
            Return cMensaje
        End Get
        Set(value)
            cMensaje = value
        End Set
    End Property
    Public Property gAlmacen
        Get
            Return Almacen
        End Get
        Set(value)
            Almacen = value
        End Set
    End Property

End Class
