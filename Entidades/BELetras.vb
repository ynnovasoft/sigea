﻿Public Class BELetras
    Private nCodLet As Integer
    Private nCodCli As Integer
    Private nCodigoAgencia As Integer
    Private dFecReg As Date
    Private cNumLet As String
    Private dFecGir As Date
    Private dFecVen As Date
    Private nMonto As Double
    Private nEstado As Integer
    Private cUsuRegAud As String
    Private cUsuActAud As String
    Private nCodLetRef As Integer
    Private nMonRen As Double
    Private cNumLetRef As String
    Private nTipMon As Integer
    Private bParcial As Integer
    Private nCodEmp As Integer
    Private cMotAnu As String
    Private nMonAmo As Double
    Public Property gnMonAmo
        Get
            Return nMonAmo
        End Get
        Set(value)
            nMonAmo = value
        End Set
    End Property
    Public Property gcMotAnu
        Get
            Return cMotAnu
        End Get
        Set(value)
            cMotAnu = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gbParcial
        Get
            Return bParcial
        End Get
        Set(value)
            bParcial = value
        End Set
    End Property
    Public Property gnTipMon
        Get
            Return nTipMon
        End Get
        Set(value)
            nTipMon = value
        End Set
    End Property
    Public Property gcNumLetRef
        Get
            Return cNumLetRef
        End Get
        Set(value)
            cNumLetRef = value
        End Set
    End Property
    Public Property gnMonRen
        Get
            Return nMonRen
        End Get
        Set(value)
            nMonRen = value
        End Set
    End Property
    Public Property gnCodLetRef
        Get
            Return nCodLetRef
        End Get
        Set(value)
            nCodLetRef = value
        End Set
    End Property
    Public Property gdFecGir
        Get
            Return dFecGir
        End Get
        Set(value)
            dFecGir = value
        End Set
    End Property
    Public Property gcUsuRegAud
        Get
            Return cUsuRegAud
        End Get
        Set(value)
            cUsuRegAud = value
        End Set
    End Property
    Public Property gcUsuActAud
        Get
            Return cUsuActAud
        End Get
        Set(value)
            cUsuActAud = value
        End Set
    End Property
    Public Property gnEstado
        Get
            Return nEstado
        End Get
        Set(value)
            nEstado = value
        End Set
    End Property
    Public Property gnMonto
        Get
            Return nMonto
        End Get
        Set(value)
            nMonto = value
        End Set
    End Property
    Public Property gdFecVen
        Get
            Return dFecVen
        End Get
        Set(value)
            dFecVen = value
        End Set
    End Property

    Public Property gdFecReg
        Get
            Return dFecReg
        End Get
        Set(value)
            dFecReg = value
        End Set
    End Property
    Public Property gcNumLet
        Get
            Return cNumLet
        End Get
        Set(value)
            cNumLet = value
        End Set
    End Property
    Public Property gnCodCli
        Get
            Return nCodCli
        End Get
        Set(value)
            nCodCli = value
        End Set
    End Property
    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property

    Public Property gnCodLet
        Get
            Return nCodLet
        End Get
        Set(value)
            nCodLet = value
        End Set
    End Property
End Class
