﻿Public Class BEOrdenCompraImportacion
    Private nCodOrdCom As Integer
    Private nCodProve As Integer
    Private nCodigoAgencia As Integer
    Private dFecReg As Date
    Private nTipMon As Integer
    Private nImpTotPre As Double
    Private nEstado As Integer
    Private cObservaciones As String
    Private cUsuRegAud As String
    Private cUsuActAud As String
    Private _cMensaje As String
    Private nCodEmp As Integer
    Private cNumOrd As String
    Private cCodPer As String
    Public Property gcCodPer
        Get
            Return cCodPer
        End Get
        Set(value)
            cCodPer = value
        End Set
    End Property
    Public Property gcNumOrd
        Get
            Return cNumOrd
        End Get
        Set(value)
            cNumOrd = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gcObservaciones
        Get
            Return cObservaciones
        End Get
        Set(value)
            cObservaciones = value
        End Set
    End Property
    Public Property gnCodOrdCom
        Get
            Return nCodOrdCom
        End Get
        Set(value)
            nCodOrdCom = value
        End Set
    End Property
    Public Property gnCodProve
        Get
            Return nCodProve
        End Get
        Set(value)
            nCodProve = value
        End Set
    End Property
    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property
    Public Property g_cMensaje
        Get
            Return _cMensaje
        End Get
        Set(value)
            _cMensaje = value
        End Set
    End Property
    Public Property gcUsuActAud
        Get
            Return cUsuActAud
        End Get
        Set(value)
            cUsuActAud = value
        End Set
    End Property
    Public Property gcUsuRegAud
        Get
            Return cUsuRegAud
        End Get
        Set(value)
            cUsuRegAud = value
        End Set
    End Property
    Public Property gdFecReg
        Get
            Return dFecReg
        End Get
        Set(value)
            dFecReg = value
        End Set
    End Property
    Public Property gnTipMon
        Get
            Return nTipMon
        End Get
        Set(value)
            nTipMon = value
        End Set
    End Property
    Public Property gnImpTotPre
        Get
            Return nImpTotPre
        End Get
        Set(value)
            nImpTotPre = value
        End Set
    End Property
    Public Property gnEstado
        Get
            Return nEstado
        End Get
        Set(value)
            nEstado = value
        End Set
    End Property
End Class
