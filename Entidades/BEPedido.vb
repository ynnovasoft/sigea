﻿Public Class BEPedido
    Private nCodPed As Integer
    Private nCodCli As Integer
    Private nCodCot As Integer
    Private nCodVen As Integer
    Private dFecReg As Date
    Private dFecVen As Date
    Private nTipCam As Double
    Private nCondPag As Integer
    Private nTipMon As Integer
    Private cTipCom As String
    Private nCodSer As Integer
    Private nVenGra As Double
    Private nIGV As Double
    Private nImpTot As Double
    Private cOrdComp As String
    Private nEstado As Integer
    Private nTipAfecIGV As Integer
    Private cTipOpe As String
    Private cUsuRegAud As String
    Private cUsuActAud As String
    Private nCodigoAgencia As Integer
    Private cMensaje As String
    Private cTipDocAdj As String
    Private cNumComp As String
    Private bAplGuia As Boolean
    Private nCodDocRef As Integer
    Private cTipoComRef As String
    Private cNumComRef As String
    Private cCodTipNot As String
    Private cMotNot As String
    Private cObservaciones As String
    Private cNumAdj As String
    Private nMonRet As Double
    Private nImpNetPag As Double
    Private nCodEmp As Integer
    Private cMotAnu As String
    Private nSubTot As Double
    Private nMonDes As Double
    Private nMonAnt As Double
    Private cCodPer As String
    Private cCodGui As String
    Private cModVen As String
    Private bAplDedAnt As Boolean
    Private bVenSer As Boolean
    Private bAplDet As Boolean
    Private nCretoLet As Integer
    Public Property gnCretoLet
        Get
            Return nCretoLet
        End Get
        Set(value)
            nCretoLet = value
        End Set
    End Property
    Public Property gbAplDet
        Get
            Return bAplDet
        End Get
        Set(value)
            bAplDet = value
        End Set
    End Property
    Public Property gbVenSer
        Get
            Return bVenSer
        End Get
        Set(value)
            bVenSer = value
        End Set
    End Property
    Public Property gnMonAnt
        Get
            Return nMonAnt
        End Get
        Set(value)
            nMonAnt = value
        End Set
    End Property
    Public Property gbAplDedAnt
        Get
            Return bAplDedAnt
        End Get
        Set(value)
            bAplDedAnt = value
        End Set
    End Property
    Public Property gcModVen
        Get
            Return cModVen
        End Get
        Set(value)
            cModVen = value
        End Set
    End Property
    Public Property gcCodGui
        Get
            Return cCodGui
        End Get
        Set(value)
            cCodGui = value
        End Set
    End Property
    Public Property gcCodPer
        Get
            Return cCodPer
        End Get
        Set(value)
            cCodPer = value
        End Set
    End Property
    Public Property gnSubTot
        Get
            Return nSubTot
        End Get
        Set(value)
            nSubTot = value
        End Set
    End Property
    Public Property gnMonDes
        Get
            Return nMonDes
        End Get
        Set(value)
            nMonDes = value
        End Set
    End Property
    Public Property gcMotAnu
        Get
            Return cMotAnu
        End Get
        Set(value)
            cMotAnu = value
        End Set
    End Property
    Public Property gnCodVen
        Get
            Return nCodVen
        End Get
        Set(value)
            nCodVen = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gnMonRet
        Get
            Return nMonRet
        End Get
        Set(value)
            nMonRet = value
        End Set
    End Property
    Public Property gnImpNetPag
        Get
            Return nImpNetPag
        End Get
        Set(value)
            nImpNetPag = value
        End Set
    End Property
    Public Property gcNumAdj
        Get
            Return cNumAdj
        End Get
        Set(value)
            cNumAdj = value
        End Set
    End Property
    Public Property gnCodDocRef
        Get
            Return nCodDocRef
        End Get
        Set(value)
            nCodDocRef = value
        End Set
    End Property
    Public Property gcTipoComRef
        Get
            Return cTipoComRef
        End Get
        Set(value)
            cTipoComRef = value
        End Set
    End Property
    Public Property gcNumComRef
        Get
            Return cNumComRef
        End Get
        Set(value)
            cNumComRef = value
        End Set
    End Property
    Public Property gcCodTipNot
        Get
            Return cCodTipNot
        End Get
        Set(value)
            cCodTipNot = value
        End Set
    End Property
    Public Property gcObservaciones
        Get
            Return cObservaciones
        End Get
        Set(value)
            cObservaciones = value
        End Set
    End Property
    Public Property gcMotNot
        Get
            Return cMotNot
        End Get
        Set(value)
            cMotNot = value
        End Set
    End Property
    Public Property gbAplGuia
        Get
            Return bAplGuia
        End Get
        Set(value)
            bAplGuia = value
        End Set
    End Property
    Public Property gcNumComp
        Get
            Return cNumComp
        End Get
        Set(value)
            cNumComp = value
        End Set
    End Property
    Public Property gcTipDocAdj
        Get
            Return cTipDocAdj
        End Get
        Set(value)
            cTipDocAdj = value
        End Set
    End Property
    Public Property gnCodCot
        Get
            Return nCodCot
        End Get
        Set(value)
            nCodCot = value
        End Set
    End Property
    Public Property gcMensaje
        Get
            Return cMensaje
        End Get
        Set(value)
            cMensaje = value
        End Set
    End Property
    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property
    Public Property gcUsuActAud
        Get
            Return cUsuActAud
        End Get
        Set(value)
            cUsuActAud = value
        End Set
    End Property
    Public Property gcUsuRegAud
        Get
            Return cUsuRegAud
        End Get
        Set(value)
            cUsuRegAud = value
        End Set
    End Property
    Public Property gnTipAfecIGV
        Get
            Return nTipAfecIGV
        End Get
        Set(value)
            nTipAfecIGV = value
        End Set
    End Property
    Public Property gcTipOpe
        Get
            Return cTipOpe
        End Get
        Set(value)
            cTipOpe = value
        End Set
    End Property

    Public Property gnCodPed
        Get
            Return nCodPed
        End Get
        Set(value)
            nCodPed = value
        End Set
    End Property
    Public Property gnCodCli
        Get
            Return nCodCli
        End Get
        Set(value)
            nCodCli = value
        End Set
    End Property
    Public Property gdFecReg
        Get
            Return dFecReg
        End Get
        Set(value)
            dFecReg = value
        End Set
    End Property
    Public Property gdFecVen
        Get
            Return dFecVen
        End Get
        Set(value)
            dFecVen = value
        End Set
    End Property
    Public Property gnTipCam
        Get
            Return nTipCam
        End Get
        Set(value)
            nTipCam = value
        End Set
    End Property
    Public Property gnCondPag
        Get
            Return nCondPag
        End Get
        Set(value)
            nCondPag = value
        End Set
    End Property
    Public Property gnTipMon
        Get
            Return nTipMon
        End Get
        Set(value)
            nTipMon = value
        End Set
    End Property
    Public Property gcTipCom
        Get
            Return cTipCom
        End Get
        Set(value)
            cTipCom = value
        End Set
    End Property
    Public Property gnCodSer
        Get
            Return nCodSer
        End Get
        Set(value)
            nCodSer = value
        End Set
    End Property
    Public Property gnVenGra
        Get
            Return nVenGra
        End Get
        Set(value)
            nVenGra = value
        End Set
    End Property
    Public Property gnIGV
        Get
            Return nIGV
        End Get
        Set(value)
            nIGV = value
        End Set
    End Property
    Public Property gnImpTot
        Get
            Return nImpTot
        End Get
        Set(value)
            nImpTot = value
        End Set
    End Property
    Public Property gcOrdComp
        Get
            Return cOrdComp
        End Get
        Set(value)
            cOrdComp = value
        End Set
    End Property
    Public Property gnEstado
        Get
            Return nEstado
        End Get
        Set(value)
            nEstado = value
        End Set
    End Property

End Class
