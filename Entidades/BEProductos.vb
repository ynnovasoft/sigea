﻿Public Class BEProductos
    Private nCodProd As Integer
    Private cCodCat As String
    Private cDescripcion As String
    Private nStock As Integer
    Private cMarca As String
    Private cUndMed As String
    Private cSerie As String
    Private nStockMinimo As Integer
    Private nValUni As Double
    Private nPreUni As Double
    Private nPreComPEN As Double
    Private nPreComUSD As Double
    Private bModDes As Boolean
    Private cCodOriCom As String
    Private nMonDesUni As Double
    Private nMonDesMay As Double
    Private nCodEmp As Integer
    Private cCodFam As String
    Private nCodSubFam As Integer
    Public Property gnCodSubFam
        Get
            Return nCodSubFam
        End Get
        Set(value)
            nCodSubFam = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gcCodFam
        Get
            Return cCodFam
        End Get
        Set(value)
            cCodFam = value
        End Set
    End Property
    Public Property gnMonDesUni
        Get
            Return nMonDesUni
        End Get
        Set(value)
            nMonDesUni = value
        End Set
    End Property

    Public Property gnMonDesMay
        Get
            Return nMonDesMay
        End Get
        Set(value)
            nMonDesMay = value
        End Set
    End Property
    Public Property gcCodOriCom
        Get
            Return cCodOriCom
        End Get
        Set(value)
            cCodOriCom = value
        End Set
    End Property

    Public Property gbModDes
        Get
            Return bModDes
        End Get
        Set(value)
            bModDes = value
        End Set
    End Property

    Public Property gnPreComPEN
        Get
            Return nPreComPEN
        End Get
        Set(value)
            nPreComPEN = value
        End Set
    End Property
    Public Property gnPreComUSD
        Get
            Return nPreComUSD
        End Get
        Set(value)
            nPreComUSD = value
        End Set
    End Property
    Public Property gnValUni
        Get
            Return nValUni
        End Get
        Set(value)
            nValUni = value
        End Set
    End Property
    Public Property gnPreUni
        Get
            Return nPreUni
        End Get
        Set(value)
            nPreUni = value
        End Set
    End Property
    Public Property gnStockMinimo
        Get
            Return nStockMinimo
        End Get
        Set(value)
            nStockMinimo = value
        End Set
    End Property
    Public Property gcSerie
        Get
            Return cSerie
        End Get
        Set(value)
            cSerie = value
        End Set
    End Property
    Public Property gcMarca
        Get
            Return cMarca
        End Get
        Set(value)
            cMarca = value
        End Set
    End Property
    Public Property gcUndMed
        Get
            Return cUndMed
        End Get
        Set(value)
            cUndMed = value
        End Set
    End Property
    Public Property gnStock
        Get
            Return nStock
        End Get
        Set(value)
            nStock = value
        End Set
    End Property
    Public Property gnCodProd
        Get
            Return nCodProd
        End Get
        Set(value)
            nCodProd = value
        End Set
    End Property
    Public Property gcCodCat
        Get
            Return cCodCat
        End Get
        Set(value)
            cCodCat = value
        End Set
    End Property
    Public Property gcDescripcion
        Get
            Return cDescripcion
        End Get
        Set(value)
            cDescripcion = value
        End Set
    End Property


End Class
