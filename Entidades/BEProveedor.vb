﻿Public Class BEProveedor
    Private nCodProve As Integer
    Private cTipDocProv As String
    Private cNumDoc As String
    Private cRazSoc As String
    Private cDirProv As String
    Private nTipPer As Integer
    Private cApePat As String
    Private cApeMat As String
    Private cNombres As String
    Private nCodDep As String
    Private nCodProv As String
    Private nCodDis As String
    Private cCorreo As String
    Private cContactos As String
    Public Property gcDirProv
        Get
            Return cDirProv
        End Get
        Set(value)
            cDirProv = value
        End Set
    End Property
    Public Property gcRazSoc
        Get
            Return cRazSoc
        End Get
        Set(value)
            cRazSoc = value
        End Set
    End Property
    Public Property gcNumDoc
        Get
            Return cNumDoc
        End Get
        Set(value)
            cNumDoc = value
        End Set
    End Property
    Public Property gcTipDocProv
        Get
            Return cTipDocProv
        End Get
        Set(value)
            cTipDocProv = value
        End Set
    End Property
    Public Property gnCodProve
        Get
            Return nCodProve
        End Get
        Set(value)
            nCodProve = value
        End Set
    End Property

    Public Property gcContactos
        Get
            Return cContactos
        End Get
        Set(value)
            cContactos = value
        End Set
    End Property
    Public Property gcCorreo
        Get
            Return cCorreo
        End Get
        Set(value)
            cCorreo = value
        End Set
    End Property
    Public Property gnCodDis
        Get
            Return nCodDis
        End Get
        Set(value)
            nCodDis = value
        End Set
    End Property
    Public Property gnCodProv
        Get
            Return nCodProv
        End Get
        Set(value)
            nCodProv = value
        End Set
    End Property
    Public Property gnCodDep
        Get
            Return nCodDep
        End Get
        Set(value)
            nCodDep = value
        End Set
    End Property
    Public Property gcNombres
        Get
            Return cNombres
        End Get
        Set(value)
            cNombres = value
        End Set
    End Property
    Public Property gcApeMat
        Get
            Return cApeMat
        End Get
        Set(value)
            cApeMat = value
        End Set
    End Property
    Public Property gcApePat
        Get
            Return cApePat
        End Get
        Set(value)
            cApePat = value
        End Set
    End Property
    Public Property gnTipPer
        Get
            Return nTipPer
        End Get
        Set(value)
            nTipPer = value
        End Set
    End Property
End Class
