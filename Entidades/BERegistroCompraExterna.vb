﻿Public Class BERegistroCompraExterna
    Private nCodCom As Integer
    Private nCodProve As Integer
    Private nCodigoAgencia As Integer
    Private dFecReg As Date
    Private nTipCam As Double
    Private cTipCom As String
    Private nTipMon As Integer
    Private cNumComRef As String
    Private nVenGra As Double
    Private nIGV As Double
    Private nImpTot As Double
    Private nEstado As Integer
    Private cObservaciones As String
    Private cUsuRegAud As String
    Private cUsuActAud As String
    Private dFecEmi As Date
    Private nCodEmp As Integer
    Private cNumCom As String
    Public Property gcNumCom
        Get
            Return cNumCom
        End Get
        Set(value)
            cNumCom = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gdFecEmi
        Get
            Return dFecEmi
        End Get
        Set(value)
            dFecEmi = value
        End Set
    End Property
    Public Property gcObservaciones
        Get
            Return cObservaciones
        End Get
        Set(value)
            cObservaciones = value
        End Set
    End Property

    Public Property gcNumComRef
        Get
            Return cNumComRef
        End Get
        Set(value)
            cNumComRef = value
        End Set
    End Property
    Public Property gnCodCom
        Get
            Return nCodCom
        End Get
        Set(value)
            nCodCom = value
        End Set
    End Property
    Public Property gnCodProve
        Get
            Return nCodProve
        End Get
        Set(value)
            nCodProve = value
        End Set
    End Property
    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property
    Public Property gcUsuActAud
        Get
            Return cUsuActAud
        End Get
        Set(value)
            cUsuActAud = value
        End Set
    End Property
    Public Property gcUsuRegAud
        Get
            Return cUsuRegAud
        End Get
        Set(value)
            cUsuRegAud = value
        End Set
    End Property
    Public Property gdFecReg
        Get
            Return dFecReg
        End Get
        Set(value)
            dFecReg = value
        End Set
    End Property
    Public Property gnTipCam
        Get
            Return nTipCam
        End Get
        Set(value)
            nTipCam = value
        End Set
    End Property
    Public Property gnTipMon
        Get
            Return nTipMon
        End Get
        Set(value)
            nTipMon = value
        End Set
    End Property
    Public Property gcTipCom
        Get
            Return cTipCom
        End Get
        Set(value)
            cTipCom = value
        End Set
    End Property
    Public Property gnVenGra
        Get
            Return nVenGra
        End Get
        Set(value)
            nVenGra = value
        End Set
    End Property
    Public Property gnIGV
        Get
            Return nIGV
        End Get
        Set(value)
            nIGV = value
        End Set
    End Property
    Public Property gnImpTot
        Get
            Return nImpTot
        End Get
        Set(value)
            nImpTot = value
        End Set
    End Property
    Public Property gnEstado
        Get
            Return nEstado
        End Get
        Set(value)
            nEstado = value
        End Set
    End Property
End Class
