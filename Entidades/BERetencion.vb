﻿Public Class BERetencion
    Private nCodRet As Integer
    Private nCodCli As Integer
    Private nCodigoAgencia As Integer
    Private dFecReg As Date
    Private cNumRet As String
    Private nImpTot As Double
    Private nMonRet As Double
    Private nImpNetPag As Double
    Private nEstado As Integer
    Private cUsuRegAud As String
    Private cUsuActAud As String
    Private dFecEmi As Date
    Private nTipMon As Integer
    Private nCodEmp As Integer
    Private cMotAnu As String
    Public Property gcMotAnu
        Get
            Return cMotAnu
        End Get
        Set(value)
            cMotAnu = value
        End Set
    End Property
    Public Property gnCodEmp
        Get
            Return nCodEmp
        End Get
        Set(value)
            nCodEmp = value
        End Set
    End Property
    Public Property gnTipMon
        Get
            Return nTipMon
        End Get
        Set(value)
            nTipMon = value
        End Set
    End Property
    Public Property gdFecEmi
        Get
            Return dFecEmi
        End Get
        Set(value)
            dFecEmi = value
        End Set
    End Property
    Public Property gcUsuRegAud
        Get
            Return cUsuRegAud
        End Get
        Set(value)
            cUsuRegAud = value
        End Set
    End Property
    Public Property gcUsuActAud
        Get
            Return cUsuActAud
        End Get
        Set(value)
            cUsuActAud = value
        End Set
    End Property
    Public Property gnEstado
        Get
            Return nEstado
        End Get
        Set(value)
            nEstado = value
        End Set
    End Property
    Public Property gnImpNetPag
        Get
            Return nImpNetPag
        End Get
        Set(value)
            nImpNetPag = value
        End Set
    End Property
    Public Property gnMonRet
        Get
            Return nMonRet
        End Get
        Set(value)
            nMonRet = value
        End Set
    End Property
    Public Property gnImpTot
        Get
            Return nImpTot
        End Get
        Set(value)
            nImpTot = value
        End Set
    End Property
    Public Property gdFecReg
        Get
            Return dFecReg
        End Get
        Set(value)
            dFecReg = value
        End Set
    End Property
    Public Property gcNumRet
        Get
            Return cNumRet
        End Get
        Set(value)
            cNumRet = value
        End Set
    End Property
    Public Property gnCodCli
        Get
            Return nCodCli
        End Get
        Set(value)
            nCodCli = value
        End Set
    End Property
    Public Property gnCodigoAgencia
        Get
            Return nCodigoAgencia
        End Get
        Set(value)
            nCodigoAgencia = value
        End Set
    End Property

    Public Property gnCodRet
        Get
            Return nCodRet
        End Get
        Set(value)
            nCodRet = value
        End Set
    End Property
End Class
