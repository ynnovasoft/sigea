﻿Public Class BEUsuario
    Private nCodUsuario As Integer
    Private cApePat As String
    Private cApeMat As String
    Private cNombres As String
    Private cCodUsuario As String
    Private cPassword As String
    Private bActivo As Boolean
    Private bVendedor As Boolean
    Private bPersonal As Boolean
    Private bUsuario As Boolean
    Private bActPre As Boolean
    Private bGenExcCli As Boolean
    Public Property gbGenExcCli
        Get
            Return bGenExcCli
        End Get
        Set(value)
            bGenExcCli = value
        End Set
    End Property
    Public Property gbPersonal
        Get
            Return bPersonal
        End Get
        Set(value)
            bPersonal = value
        End Set
    End Property
    Public Property gbUsuario
        Get
            Return bUsuario
        End Get
        Set(value)
            bUsuario = value
        End Set
    End Property
    Public Property gbActPre
        Get
            Return bActPre
        End Get
        Set(value)
            bActPre = value
        End Set
    End Property
    Public Property gnCodUsuario
        Get
            Return nCodUsuario
        End Get
        Set(value)
            nCodUsuario = value
        End Set
    End Property
    Public Property gcApePat
        Get
            Return cApePat
        End Get
        Set(value)
            cApePat = value
        End Set
    End Property
    Public Property gcApeMat
        Get
            Return cApeMat
        End Get
        Set(value)
            cApeMat = value
        End Set
    End Property
    Public Property gcNombres
        Get
            Return cNombres
        End Get
        Set(value)
            cNombres = value
        End Set
    End Property
    Public Property gcCodUsuario
        Get
            Return cCodUsuario
        End Get
        Set(value)
            cCodUsuario = value
        End Set
    End Property
    Public Property gcPassword
        Get
            Return cPassword
        End Get
        Set(value)
            cPassword = value
        End Set
    End Property
    Public Property gbActivo
        Get
            Return bActivo
        End Get
        Set(value)
            bActivo = value
        End Set
    End Property
    Public Property gbVendedor
        Get
            Return bVendedor
        End Get
        Set(value)
            bVendedor = value
        End Set
    End Property
End Class
