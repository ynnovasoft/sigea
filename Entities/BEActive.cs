﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class BEActive
    {
        public int nCodAct { get; set; }
        public int nTipAct { get; set; }
        public int nConAct { get; set; }
        public string cDescripcion { get; set; }
        public bool bEstado { get; set; }
    }
}
