﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class BECustomer
    {
        public int nCodCli { get; set; }
        public string cTipDocCli { get; set; }
        public string cNumDoc { get; set; }
        public string cRazSoc { get; set; }
        public string cDirCli { get; set; }
        public int nTipPer { get; set; }
        public string cApePat { get; set; }
        public string cApeMat { get; set; }
        public string cNombres { get; set; }
        public string cCodUbiDep { get; set; }
        public string cCodUbiProv { get; set; }
        public string cCodUbiDis { get; set; }
        public string cCorreo { get; set; }
        public string cContactos { get; set; }
        public bool bAgeRet { get; set; }
        public string cDepartamento { get; set; }
        public string cProvincia { get; set; }
        public string cDistrito { get; set; }
        public int nTipCli { get; set; }
        public int nCodVen { get; set; }
    }
}
