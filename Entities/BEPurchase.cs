﻿using System;


namespace Entities
{
    public class BEPurchase
    {
        public int nCodCom { get; set; }
        public int nCodProve { get; set; }
        public int nCodigoAgencia { get; set; }
        public DateTime dFecReg { get; set; }
        public DateTime dFecVen { get; set; }
        public DateTime dFecEmi { get; set; }
        public double nTipCam { get; set; }
        public int nCondPag { get; set; }
        public string cTipCom { get; set; }
        public int nTipMon { get; set; }
        public string cNumComRef { get; set; }
        public string cNumGuiRef { get; set; }
        public double nVenGra { get; set; }
        public double nIGV { get; set; }
        public double nImpTot { get; set; }
        public int nEstado { get; set; }
        public string cTipMov { get; set; }
        public string cObservaciones { get; set; }
        public string cUsuRegAud { get; set; }
        public string cUsuActAud { get; set; }
        public string _cMensaje { get; set; }
        public int nCodOrdCom { get; set; }
        public string cTipDocAdj { get; set; }
        public bool bIngPar { get; set; }
        public int nCodAgeIng { get; set; }
        public int nCodEmp { get; set; }
        public string cNumOrd { get; set; }
        public string cMotAnu { get; set; }
        public string cNumCom { get; set; }
        public string cCodPer { get; set; }
        public bool bServicio { get; set; }
    }
}
