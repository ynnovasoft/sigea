﻿namespace Entities
{
  public  class BESerie
    {
        public int nCodSer { get; set; }
        public string  cTipCom { get; set; }
        public int nCodigoAgencia { get; set; }
        public string  cSerie { get; set; }
        public string  cNumCor { get; set; }
        public string  cObservaciones { get; set; }
        public int nCodEmp { get; set; }
    }
}
