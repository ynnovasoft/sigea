﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class BEStore
    {
        public int nCodalm { get; set; }
        public int nCodigoAgencia { get; set; }
        public int nCodAgeDes { get; set; }
        public int nCodCliProv { get; set; }
        public string cMotTrasl { get; set; }
        public DateTime dFecReg { get; set; }
        public int nTipMov { get; set; }
        public double nTipCam { get; set; }
        public int nTipMon { get; set; }
        public double nVenGra { get; set; }
        public double nIGV { get; set; }
        public double nImpTot { get; set; }
        public string cTipDocRef { get; set; }
        public int nCodigoDocRef { get; set; }
        public int nEstado { get; set; }
        public string cUsuRegAud { get; set; }
        public string cUsuActAud { get; set; }
        public string cMensaje { get; set; }
        public string NumDocRef { get; set; }
        public string AgenciaOrigen { get; set; }
        public string AgenciaDestino { get; set; }
        public string cObservaciones { get; set; }
        public int nCodAgeOriIng { get; set; }
        public string cNumAlm { get; set; }
        public int nCodSer { get; set; }
        public int nCodEmp { get; set; }
        public double nSubTot { get; set; }
        public double nMonDes { get; set; }
        public int nCodEmpDes { get; set; }
        public string EmpresaOrigen { get; set; }
        public string cCodPer { get; set; }
        public bool bServicio { get; set; }
    }
}
