﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class BETipoCambio
    {
        public double precioCompra { get; set; }
        public double precioVenta { get; set; }
        public string moneda { get; set; }
        public string fecha { get; set; }
    }
}
