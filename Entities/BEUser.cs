﻿namespace Entities
{
   public class BEUser
    {
        public int nCodUsuario { get; set; }
        public string cApePat { get; set; }
        public string cApeMat { get; set; }
        public string cNombres { get; set; }
        public string cCodUsuario { get; set; }
        public string cPassword { get; set; }
        public bool bActivo { get; set; }
        public bool bVendedor { get; set; }
        public bool bPersonal { get; set; }
        public bool bUsuario { get; set; }
        public bool bActPre { get; set; }
        public bool bGenExcCli { get; set; }
        public bool bAjuInv { get; set; }
        public bool bPagSuc { get; set; }
    }
}
