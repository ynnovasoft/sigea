﻿Imports Entities
Imports Data
Public Class BLAlmacen
    Public Function Listar(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                                 ByVal nCodigoAgencia As Integer, ByVal nTipoBusqueda As Integer,
                                 ByVal TipMov As Integer, ByVal CodigoEmpresa As Integer, ByVal Servicio As Boolean) As DataTable
        Dim objDatos As New DAStore
        Return objDatos.Listar(dFecIni, dFecFin, Valor, nCodigoAgencia, nTipoBusqueda, TipMov, CodigoEmpresa, Servicio)
    End Function
    Public Function ListarMovimientos(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Codigo As Integer,
                                      ByVal Agencia As Integer, ByVal nTipoMovimiento As Integer,
                                      ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DAStore
        Return objDatos.ListarMovimientos(dFecIni, dFecFin, Codigo, Agencia, nTipoMovimiento, CodigoEmpresa)
    End Function
    Public Function ConsultaStockRegistradoProducto(ByVal Codigo As Integer, ByVal Movimiento As Integer,
                                                    ByVal Agencia As Integer) As DataTable
        Dim objDatos As New DAStore
        Return objDatos.ConsultaStockRegistradoProducto(Codigo, Movimiento, Agencia)
    End Function
    Public Function ListarTraslados(ByVal Valor As String, ByVal nTipoBusqueda As Integer, ByVal nCodigoAgencia As Integer,
                                ByVal MotivoTraslado As String, ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DAStore
        Return objDatos.ListarTraslados(Valor, nTipoBusqueda, nCodigoAgencia, MotivoTraslado, CodigoEmpresa)
    End Function
    Public Function ListarStockAnterior(ByVal dFecIni As Date, ByVal Codigo As Integer, ByVal Agencia As Integer,
                                        ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DAStore
        Return objDatos.ListarStockAnterior(dFecIni, Codigo, Agencia, CodigoEmpresa)
    End Function
    Public Function RecuperarDetalle(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DAStore
        Return objDatos.RecuperarDetalle(Codigo)
    End Function
    Function Registrar(ByRef oDato As BEStore, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAStore
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Function Anular(ByVal oDato As BEStore, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAStore
        Return objDatos.Anular(oDato, cMensaje)
    End Function
    Function Facturar(ByRef oDato As BEStore, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAStore
        Return objDatos.Facturar(oDato, cMensaje)
    End Function
    Function AgregarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                ByVal Cantidad As Decimal, ByVal Precio As Double,
                             ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAStore
        Return objDatos.AgregarProducto(Codigo, CodigoProducto, Cantidad, Precio, cMensaje)
    End Function
    Function EliminarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer, ByVal EliminaItem As Boolean,
                                       ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAStore
        Return objDatos.EliminarProducto(Codigo, CodigoProducto, EliminaItem, cMensaje)
    End Function
    Function ModificarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                ByVal Cantidad As Decimal, ByVal CantidadAnt As Decimal,
                               ByVal Precio As Double, ByVal Descripcion As String, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAStore
        Return objDatos.ModificarProducto(Codigo, CodigoProducto, Cantidad, CantidadAnt, Precio, Descripcion, cMensaje)
    End Function
End Class
