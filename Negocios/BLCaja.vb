﻿Imports Data
Imports Entidades
Public Class BLCaja

    Function Registrar(ByRef oDato As BECaja, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DACash
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Function RegistrarOperaciones(ByRef oDato As BECaja, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DACash
        Return objDatos.RegistrarOperaciones(oDato, cMensaje)
    End Function
End Class
