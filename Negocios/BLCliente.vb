﻿Imports Entities
Imports Data
Public Class BLCliente
    Public Function Obtener(ByVal Valor As String, ByVal nTipoBusqueda As Integer) As DataTable
        Dim objDatos As New DAClient
        Return objDatos.Obtener(Valor, nTipoBusqueda)
    End Function
    Public Function Listar(ByVal Valor As String, ByVal nTipoBusqueda As Integer) As DataTable
        Dim objDatos As New DAClient
        Return objDatos.Listar(Valor, nTipoBusqueda)
    End Function
    Function Grabar(ByRef oDato As BECustomer, ByVal bNuevo As Boolean, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAClient
        Return objDatos.Grabar(oDato, bNuevo, cMensaje)
    End Function
    Function Eliminar(ByVal Codigo As Integer, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAClient
        Return objDatos.Eliminar(Codigo, cMensaje)
    End Function
End Class
