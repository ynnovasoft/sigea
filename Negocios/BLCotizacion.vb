﻿Imports Entidades
Imports Data
Public Class BLCotizacion
    Public Function Listar(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                                 ByVal nCodigoAgencia As Integer, ByVal nTipoBusqueda As Integer,
                                 ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DAQuotation
        Return objDatos.Listar(dFecIni, dFecFin, Valor, nCodigoAgencia, nTipoBusqueda, CodigoEmpresa)
    End Function
    Public Function RecuperarDetalle(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DAQuotation
        Return objDatos.RecuperarDetalle(Codigo)
    End Function
    Function Registrar(ByRef oDato As BECotizacion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAQuotation
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Function Anular(ByVal oDato As BECotizacion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAQuotation
        Return objDatos.Anular(oDato, cMensaje)
    End Function
    Function Editar(ByVal oDato As BECotizacion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAQuotation
        Return objDatos.Editar(oDato, cMensaje)
    End Function
    Function Facturar(ByVal oDato As BECotizacion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAQuotation
        Return objDatos.Facturar(oDato, cMensaje)
    End Function
    Function Clonar(ByVal oDato As BECotizacion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAQuotation
        Return objDatos.Clonar(oDato, cMensaje)
    End Function

    Function AgregarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                ByVal Cantidad As Decimal, ByVal Precio As Decimal,
                             ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAQuotation
        Return objDatos.AgregarProducto(Codigo, CodigoProducto, Cantidad, Precio, cMensaje)
    End Function
    Function EliminarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                       ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAQuotation
        Return objDatos.EliminarProducto(Codigo, CodigoProducto, cMensaje)
    End Function
    Function ModificarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                ByVal Cantidad As Decimal, ByVal Precio As Decimal, ByVal PorDes As Decimal,
                              ByVal Descripcion As String, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAQuotation
        Return objDatos.ModificarProducto(Codigo, CodigoProducto, Cantidad, Precio, PorDes, Descripcion, cMensaje)
    End Function

End Class
