﻿Imports Data
Imports DASunat.ModeloEntidades
Public Class BLFacturacion
    Public Function ObtenerDatosGeneralesPdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosGeneralesPdf(Codigo)
    End Function
    Public Function ObtenerDatosGeneralesGuiaPdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosGeneralesGuiaPdf(Codigo)
    End Function
    Public Function ObtenerDatosDetallePdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosDetallePdf(Codigo)
    End Function
    Public Function ObtenerDatosDetalleGuiaPdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosDetalleGuiaPdf(Codigo)
    End Function
    Public Function ObtenerDatosAdicionalPdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosAdicionalPdf(Codigo)
    End Function
    Public Function ObtenerDatosCuotasPdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosCuotasPdf(Codigo)
    End Function
    Public Function ObtenerGuiasPdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerGuiasPdf(Codigo)
    End Function
    Public Function ObtenerComunicacionBajaComprobante(ByVal Codigo As Integer) As ComprobanteBaja
        Dim objDatos As New DABilling
        Return objDatos.ObtenerComunicacionBajaComprobante(Codigo)
    End Function
    Public Function ObtenerComunicacionBajaGuia(ByVal Codigo As Integer) As ComprobanteBaja
        Dim objDatos As New DABilling
        Return objDatos.ObtenerComunicacionBajaGuia(Codigo)
    End Function
    Public Function ObtenerCabeceraSunat(ByVal Codigo As Integer) As Comprobante
        Dim objDatos As New DABilling
        Return objDatos.ObtenerCabeceraSunat(Codigo)
    End Function
    Public Function ObtenerGuiasFactura(ByVal Codigo As Integer) As List(Of ListaGuia)
        Dim objDatos As New DABilling
        Return objDatos.ObtenerGuiasFactura(Codigo)
    End Function
    Public Function ObtenerDetalleSunat(ByVal Codigo As Integer) As List(Of Detalle)
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDetalleSunat(Codigo)
    End Function
    Function ActualizarRespuestaSunat(ByVal Codigo As Integer, ByVal Estado As Integer, ByVal Observaciones As String,
                                     ByVal CodigoQR As String, ByVal CodigoHas As String, ByVal TipoComprobante As Integer,
                                      ByRef cMensaje As String) As Boolean
        Dim objDatos As New DABilling
        Return objDatos.ActualizarRespuestaSunat(Codigo, Estado, Observaciones, CodigoQR, CodigoHas, TipoComprobante, cMensaje)
    End Function
    Function RegistrarTicketRespuestaBajaSunat(ByVal Codigo As Integer, ByVal NumTicket As String, ByVal NombreXml As String,
                                           ByVal TipoDocumento As Integer, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DABilling
        Return objDatos.RegistrarTicketRespuestaBajaSunat(Codigo, NumTicket, NombreXml, TipoDocumento, cMensaje)
    End Function
    Public Function ObtenerDatosGeneralesPdfCotizacion(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosGeneralesPdfCotizacion(Codigo)
    End Function
    Public Function ObtenerDatosDetallePdfCotizacion(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosDetallePdfCotizacion(Codigo)
    End Function
    Function EnviaXmlPdfElectronico(ByVal Codigo As Integer, ByVal RutaPDF As String,
                                     ByVal RutaXML As String, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DABilling
        Return objDatos.EnviaXmlPdfElectronico(Codigo, RutaPDF, RutaXML, cMensaje)
    End Function
    Public Function ObtenerAnticipos(ByVal Codigo As Integer) As List(Of Anticipo)
        Dim objDatos As New DABilling
        Return objDatos.ObtenerAnticipos(Codigo)
    End Function
    Public Function ObtenerCuotas(ByVal Codigo As Integer) As List(Of Cuotas)
        Dim objDatos As New DABilling
        Return objDatos.ObtenerCuotas(Codigo)
    End Function
    Public Function ObtenerDatosDetalleEstadoCuentaPdf(ByVal Codigo As Integer, ByVal Moneda As Integer, ByVal CodEmp As Integer,
                                                       ByVal Modalidad As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosDetalleEstadoCuentaPdf(Codigo, Moneda, CodEmp, Modalidad)
    End Function
    Public Function ObtenerDatosGeneralesEstadoCuentaPdf(ByVal Codigo As Integer, ByVal CodEmp As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosGeneralesEstadoCuentaPdf(Codigo, CodEmp)
    End Function
    Public Function ObtenerDatosTotalesEstadoCuentaPdf(ByVal Codigo As Integer, ByVal Moneda As Integer, ByVal CodEmp As Integer,
                                                       ByVal Modalidad As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosTotalesEstadoCuentaPdf(Codigo, Moneda, CodEmp, Modalidad)
    End Function
    Public Function ObtenerCabeceraGuiaSunat(ByVal Codigo As Integer) As Guia
        Dim objDatos As New DABilling
        Return objDatos.ObtenerCabeceraGuiaSunat(Codigo)
    End Function
    Public Function ObtenerDetalleGuiaSunat(ByVal Codigo As Integer) As List(Of DetalleGuia)
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDetalleGuiaSunat(Codigo)
    End Function
    Public Function ObtenerDatosGeneralesOrdenCompraLocalPdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosGeneralesOrdenCompraLocalPdf(Codigo)
    End Function
    Public Function ObtenerDatosDetalleOrdenCompraLocalPdf(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosDetalleOrdenCompraLocalPdf(Codigo)
    End Function
    Function RegistraNumeroTicketEnvioSunat(ByVal Codigo As Integer, ByVal numTicket As String, ByVal TipoDocumento As Integer,
                                            ByRef cMensaje As String) As Boolean
        Dim objDatos As New DABilling
        Return objDatos.RegistraNumeroTicketEnvioSunat(Codigo, numTicket, TipoDocumento, cMensaje)
    End Function
    Public Function ObtenerDatosGeneralesPdfAlmacen(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosGeneralesPdfAlmacen(Codigo)
    End Function
    Public Function ObtenerDatosDetallePdfAlmacen(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DABilling
        Return objDatos.ObtenerDatosDetallePdfAlmacen(Codigo)
    End Function
End Class
