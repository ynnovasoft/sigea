﻿Imports Data
Imports Entidades
Public Class BLGasto
    Function Registrar(ByVal oDato As BEGasto, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DASpent
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Public Function Listar(ByVal FechaIni As Date, ByVal FechaFin As Date, ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DASpent
        Return objDatos.Listar(FechaIni, FechaFin, CodigoEmpresa)
    End Function
    Public Function Eliminar(ByVal Codigo As Integer, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DASpent
        Return objDatos.Eliminar(Codigo, cMensaje)
    End Function
End Class
