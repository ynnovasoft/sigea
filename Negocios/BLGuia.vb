﻿Imports Data
Imports Entidades
Public Class BLGuia
    Function Registrar(ByRef oDato As BEGuia, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAGuide
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Function Anular(ByVal oDato As BEGuia, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAGuide
        Return objDatos.Anular(oDato, cMensaje)
    End Function
    Function Facturar(ByRef oDato As BEGuia, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAGuide
        Return objDatos.Facturar(oDato, cMensaje)
    End Function
    Public Function Listar(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                                 ByVal nCodigoAgencia As Integer, ByVal nTipoBusqueda As Integer,
                                 ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DAGuide
        Return objDatos.Listar(dFecIni, dFecFin, Valor, nCodigoAgencia, nTipoBusqueda, CodigoEmpresa)
    End Function
    Function AgregarProducto(ByVal CodigoGuia As Integer, ByVal CodigoProducto As Integer,
                               ByVal Cantidad As Decimal,
                             ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAGuide
        Return objDatos.AgregarProducto(CodigoGuia, CodigoProducto, Cantidad, cMensaje)
    End Function
    Function EliminarProducto(ByVal CodigoGuia As Integer, ByVal CodigoProducto As Integer,
                               ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAGuide
        Return objDatos.EliminarProducto(CodigoGuia, CodigoProducto, cMensaje)
    End Function
    Function ModificarProducto(ByVal CodigoGuia As Integer, ByVal CodigoProducto As Integer,
                                ByVal Cantidad As Decimal, ByVal CantidadAnt As Decimal, ByVal Descripcion As String,
                               ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAGuide
        Return objDatos.ModificarProducto(CodigoGuia, CodigoProducto, Cantidad, CantidadAnt, Descripcion, cMensaje)
    End Function
    Public Function RecuperarDetalle(ByVal CodigoGuia As Integer) As DataTable
        Dim objDatos As New DAGuide
        Return objDatos.RecuperarDetalle(CodigoGuia)
    End Function
End Class
