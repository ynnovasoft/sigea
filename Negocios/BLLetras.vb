﻿Imports Data
Imports Entidades
Public Class BLLetras
    Public Function RecuperarDetalle(ByVal Codigo As Integer, ByVal EstaTipoBusquedado As Integer) As DataTable
        Dim objDatos As New DALetters
        Return objDatos.RecuperarDetalle(Codigo, EstaTipoBusquedado)
    End Function
    Function Registrar(ByRef oDato As BELetras, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DALetters
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Function RegistrarDetalle(ByVal oDato As BELetras, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DALetters
        Return objDatos.RegistrarDetalle(oDato, cMensaje)
    End Function
    Public Function Listar(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                           ByVal nTipoBusqueda As Integer, ByVal Estado As Integer,
                            ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DALetters
        Return objDatos.Listar(dFecIni, dFecFin, Valor, nTipoBusqueda, Estado, CodigoEmpresa)
    End Function

    Function Anular(ByVal oDato As BELetras, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DALetters
        Return objDatos.Anular(oDato, cMensaje)
    End Function
    Function Gestionar(ByRef oDato As BELetras, ByVal Operacion As Integer, ByVal MedioPago As Integer,
                        ByVal Moneda As Integer, ByVal CodBanco As Integer, ByVal TipoCambio As Double,
                       ByRef NumOpe As String, ByVal CodigoEmpresa As Integer,
                       ByRef cMensaje As String) As Boolean
        Dim objDatos As New DALetters
        Return objDatos.Gestionar(oDato, Operacion, MedioPago, Moneda, CodBanco, TipoCambio, NumOpe,
                                  CodigoEmpresa, cMensaje)
    End Function
End Class
