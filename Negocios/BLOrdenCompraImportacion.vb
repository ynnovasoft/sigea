﻿Imports Data
Imports Entidades
Public Class BLOrdenCompraImportacion
    Function Registrar(ByRef oDato As BEOrdenCompraImportacion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAImportPurchaseOrder
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Public Function Listar(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                               ByVal nTipoBusqueda As Integer, ByVal nEstado As Integer,
                          ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DAImportPurchaseOrder
        Return objDatos.Listar(dFecIni, dFecFin, Valor, nTipoBusqueda, nEstado, CodigoEmpresa)
    End Function
    Public Function RecuperarDetalle(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DAImportPurchaseOrder
        Return objDatos.RecuperarDetalle(Codigo)
    End Function
    Function AgregarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                              ByVal Cantidad As Decimal, ByVal Precio As Double,
                           ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAImportPurchaseOrder
        Return objDatos.AgregarProducto(Codigo, CodigoProducto, Cantidad, Precio, cMensaje)
    End Function
    Function EliminarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                       ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAImportPurchaseOrder
        Return objDatos.EliminarProducto(Codigo, CodigoProducto, cMensaje)
    End Function
    Function ModificarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                ByVal Cantidad As Decimal, ByVal CantidadAnt As Decimal,
                               ByVal Precio As Double, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAImportPurchaseOrder
        Return objDatos.ModificarProducto(Codigo, CodigoProducto, Cantidad, CantidadAnt, Precio, cMensaje)
    End Function
End Class
