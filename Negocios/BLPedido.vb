﻿Imports Data
Imports Entidades
Public Class BLPedido
    Function Registrar(ByRef oDato As BEPedido, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Function Anular(ByVal oDato As BEPedido, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.Anular(oDato, cMensaje)
    End Function
    Function Facturar(ByRef oDato As BEPedido, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.Facturar(oDato, cMensaje)
    End Function
    Function Clonar(ByRef oDato As BEPedido, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.Clonar(oDato, cMensaje)
    End Function
    Function AgregarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                ByVal Cantidad As Decimal, ByVal Precio As Double,
                             ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.AgregarProducto(Codigo, CodigoProducto, Cantidad, Precio, cMensaje)
    End Function
    Function EliminarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                       ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.EliminarProducto(Codigo, CodigoProducto, cMensaje)
    End Function
    Function ModificarProducto(ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                ByVal Cantidad As Decimal, ByVal CantidadAnt As Decimal,
                               ByVal Precio As Double, ByVal PorDes As Double, ByVal Descripcion As String,
                               ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.ModificarProducto(Codigo, CodigoProducto, Cantidad, CantidadAnt, Precio, PorDes, Descripcion, cMensaje)
    End Function
    Public Function RecuperarDetalle(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DAOrder
        Return objDatos.RecuperarDetalle(Codigo)
    End Function
    Public Function ListarCreditos(ByVal Valor As String, ByVal nTipoBusqueda As Integer, ByVal Pagado As Boolean,
                                   ByVal CodigoEmpresa As Integer, ByVal CodigoAgencia As Integer) As DataTable
        Dim objDatos As New DAOrder
        Return objDatos.ListarCreditos(Valor, nTipoBusqueda, Pagado, CodigoEmpresa, CodigoAgencia)
    End Function
    Public Function ListarComprobantesFacturacion(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                                ByVal nTipoBusqueda As Integer, ByVal CodigoEmpresa As Integer,
                                                  ByVal CodigoAgencia As Integer) As DataTable
        Dim objDatos As New DAOrder
        Return objDatos.ListarComprobantesFacturacion(dFecIni, dFecFin, Valor, nTipoBusqueda, CodigoEmpresa, CodigoAgencia)
    End Function
    Function AgregarProductoNotas(ByVal Codigo As Integer, ByVal Precio As Double, ByVal Descripcion As String,
                                  ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.AgregarProductoNotas(Codigo, Precio, Descripcion, cMensaje)
    End Function
    Public Function ObtenerGuias(ByVal Codigo As Integer, ByVal Tipo As Integer, ByVal Guias As String,
                                 ByVal TipoDocumentoRef As String) As DataTable
        Dim objDatos As New DAOrder
        Return objDatos.ObtenerGuias(Codigo, Tipo, Guias, TipoDocumentoRef)
    End Function
    Public Function ObtenerLetras(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DAOrder
        Return objDatos.ObtenerLetras(Codigo)
    End Function
    Function ModificarProductoAnticipo(ByVal CodigoAnticipo As Integer, ByVal Codigo As Integer, ByVal CodigoProducto As Integer,
                                       ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.ModificarProductoAnticipo(CodigoAnticipo, Codigo, CodigoProducto, cMensaje)
    End Function
    Public Function ListarCuotas(ByVal Codigo As Integer) As DataTable
        Dim objDatos As New DAOrder
        Return objDatos.ListarCuotas(Codigo)
    End Function
    Function RegistrarCuota(ByVal Codigo As Integer, ByVal CodigoCuota As String, ByVal Monto As Double, ByVal Fecha As Date,
                            ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.RegistrarCuota(Codigo, CodigoCuota, Monto, Fecha, cMensaje)
    End Function
    Function EliminarCuota(ByRef CodigoCuota As Integer, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.EliminarCuota(CodigoCuota, cMensaje)
    End Function
    Function ActualizarBaja(ByVal oDato As BEPedido, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.ActualizarBaja(oDato, cMensaje)
    End Function
    Function AplicaRetencion(ByVal Codigo As Integer, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAOrder
        Return objDatos.AplicaRetencion(Codigo, cMensaje)
    End Function
End Class
