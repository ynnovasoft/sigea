﻿Imports Data
Imports Entidades
Public Class BLProveedor
    Public Function Obtener(ByVal Valor As String, ByVal nTipoBusqueda As Integer, ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DASupplier
        Return objDatos.Obtener(Valor, nTipoBusqueda)
    End Function
    Public Function Listar(ByVal Valor As String, ByVal nTipoBusqueda As Integer) As DataTable
        Dim objDatos As New DASupplier
        Return objDatos.Listar(Valor, nTipoBusqueda)
    End Function
    Function Grabar(ByRef oDato As BEProveedor, ByVal bNuevo As Boolean, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DASupplier
        Return objDatos.Grabar(oDato, bNuevo, cMensaje)
    End Function
    Function Eliminar(ByRef oDato As BEProveedor, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DASupplier
        Return objDatos.Eliminar(oDato, cMensaje)
    End Function
End Class
