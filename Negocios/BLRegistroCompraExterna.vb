﻿Imports Data
Imports Entidades
Public Class BLRegistroCompraExterna
    Function Registrar(ByRef oDato As BERegistroCompraExterna, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAExternalPurchase
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Function Anular(ByVal oDato As BERegistroCompraExterna, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DAExternalPurchase
        Return objDatos.Anular(oDato, cMensaje)
    End Function
    Public Function Listar(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                            ByVal nTipoBusqueda As Integer, ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DAExternalPurchase
        Return objDatos.Listar(dFecIni, dFecFin, Valor, nTipoBusqueda, CodigoEmpresa)
    End Function
End Class
