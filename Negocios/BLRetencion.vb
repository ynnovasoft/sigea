﻿Imports Data
Imports Entidades
Public Class BLRetencion

    Public Function RecuperarDetalle(ByVal Codigo As Integer, ByVal EstaTipoBusquedado As Integer, ByVal Empresa As Integer) As DataTable
        Dim objDatos As New DARetention
        Return objDatos.RecuperarDetalle(Codigo, EstaTipoBusquedado, Empresa)
    End Function
    Function Registrar(ByRef oDato As BERetencion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DARetention
        Return objDatos.Registrar(oDato, cMensaje)
    End Function
    Function RegistrarDetalle(ByRef oDato As BERetencion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DARetention
        Return objDatos.RegistrarDetalle(oDato, cMensaje)
    End Function
    Public Function Listar(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                           ByVal nTipoBusqueda As Integer, ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DARetention
        Return objDatos.Listar(dFecIni, dFecFin, Valor, nTipoBusqueda, CodigoEmpresa)
    End Function
    Public Function ListarComprobantes(ByVal dFecIni As Date, ByVal dFecFin As Date, ByVal Valor As String,
                          ByVal nTipoBusqueda As Integer, ByVal nEstado As Integer, ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DARetention
        Return objDatos.ListarComprobantes(dFecIni, dFecFin, Valor, nTipoBusqueda, nEstado, CodigoEmpresa)
    End Function
    Function Anular(ByVal oDato As BERetencion, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DARetention
        Return objDatos.Anular(oDato, cMensaje)
    End Function
End Class
