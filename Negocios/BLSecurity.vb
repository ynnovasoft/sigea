﻿Imports Data
Public Class BLSecurity
    Function RecuperaDatosLogueo(ByVal cUsuario As String, ByVal CodigoAgencia As Integer, ByVal CodigoEmpresa As Integer,
                                 ByVal CodigoPersonal As String) As DataTable
        Dim objDatos As New DASecurity
        Return objDatos.RecuperaDatosLogueo(cUsuario, CodigoAgencia, CodigoEmpresa, CodigoPersonal)
    End Function
    Function RecuperaMenu(ByVal CodigoUsuario As Integer, ByVal CodigoAgencia As Integer, ByVal CodigoEmpresa As Integer) As DataTable
        Dim objDatos As New DASecurity
        Return objDatos.RecuperaMenu(CodigoUsuario, CodigoAgencia, CodigoEmpresa)
    End Function
    Function RegistraPermisos(ByVal Dato As DataTable, ByVal CodigoUsuario As Integer, ByVal CodigoAgencia As Integer,
                              ByVal CodigoEmpresa As Integer, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DASecurity
        Return objDatos.RegistraPermisos(Dato, CodigoUsuario, CodigoAgencia, CodigoEmpresa, cMensaje)
    End Function
    Function ValidaIngresoSistema(ByVal cUsuario As String, ByVal cPassword As String, ByVal CodigoAgencia As Integer,
                                  ByVal CodigoEmpresa As Integer, ByRef Respuesta As Integer, ByRef cMensaje As String) As Boolean
        Dim objDatos As New DASecurity
        Return objDatos.ValidaIngresoSistema(cUsuario, cPassword, CodigoAgencia, CodigoEmpresa, Respuesta, cMensaje)
    End Function
End Class
