﻿Imports Entities
Imports DASunat.ConsultaDatosSunat
Module ConsultaDatosGeneralesSunat

    Public Function ConsultarCliente(ByRef Cliente As BECustomer, ByVal Documento As String, ByVal TipoDocumento As Integer) As String
        Dim Mensaje As String = ""
        Dim Obj As New ConsultaDatosSunat
        If TipoDocumento = 1 Then
            Mensaje = Obj.ConsultarDNI(Cliente, Documento)
        ElseIf TipoDocumento = 6 Then
            Mensaje = Obj.ConsultarRUC(Cliente, Documento)
        End If
        Return Mensaje
    End Function
    Public Function ConsultarTipoCambio(ByRef Datos As Entities.BETipoCambio, ByVal Fecha As String) As String
        Dim Mensaje As String = ""
        Dim Obj As New ConsultaDatosSunat
        Mensaje = Obj.ConsultarTipoCambio(Datos, Fecha)
        Return Mensaje
    End Function
End Module
