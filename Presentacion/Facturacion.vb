﻿Imports System.Configuration
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports Negocios
Imports DASunat.ModeloEntidades
Imports DASunat.ServicioComprobantes
Module Facturacion
    Public Sub ModeloImpresionPdf(ByVal Codigo As Integer, ByVal TipoComprobante As String, ByVal Ruta As String)
        Dim ModeloPdf As String = ConfigurationManager.AppSettings("ModeloPdf")
        If ModeloPdf = "1" Then
            If TipoComprobante <> "99" Then
                Call CrearPdf_TA4_Modelo_1(Codigo, TipoComprobante)
                Process.Start(Ruta)
            Else
                FrmReportes.ImpresionTicket(Codigo)
                FrmReportes = Nothing
            End If
        ElseIf ModeloPdf = "2" Then 'EMPRESA EPNISAC Y FLUIDAC
            Call CrearPdf_TA4_Modelo_2(Codigo, TipoComprobante)
            Process.Start(Ruta)
        ElseIf ModeloPdf = "3" Then 'EMPRESA FERRETERIA MELISSA Y GROUP TOOLS
            If MDIPrincipal.CodigoEmpresa = 1 Then
                If TipoComprobante <> "99" Then
                    Call CrearPdf_TA4_FERINDMELISSA(Codigo, TipoComprobante)
                    Process.Start(Ruta)
                Else
                    FrmReportes.ImpresionTicket(Codigo)
                    FrmReportes = Nothing
                End If
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                Call CrearPdf_TA4_GROUPTOOLS(Codigo, TipoComprobante)
                Process.Start(Ruta)
            End If
        ElseIf ModeloPdf = "4" Then 'EMPRESA MAQUIHERRAMIENTAS Y CFC ABASTACIMIENTO
            If TipoComprobante <> "99" Then
                Call CrearPdf_TA4_MAQHER(Codigo, TipoComprobante)
                Process.Start(Ruta)
            Else
                FrmReportes.ImpresionTicket(Codigo)
                FrmReportes = Nothing
            End If
        ElseIf ModeloPdf = "5" Then 'EMPRESA PAVICH
            Call CrearPdf_TA4_Modelo_1(Codigo, TipoComprobante)
            Process.Start(Ruta)
        End If
    End Sub
    Public Sub ModeloImpresionGuiaPdf(ByVal Codigo As Integer)
        Dim ModeloPdf As String = ConfigurationManager.AppSettings("ModeloPdfGuia")
        If ModeloPdf = "1" Then
            Call CrearPdf_TA4_Guia_Modelo_1(Codigo)
        ElseIf ModeloPdf = "2" Then
            Call CrearPdf_TA4_Guia_Modelo_2(Codigo)
        ElseIf ModeloPdf = "3" Then 'EMPRESA FERRETERIA MELISSA Y GROUP TOOLS
            If MDIPrincipal.CodigoEmpresa = 1 Then
                Call CrearPdf_TA4_Guia_FERINDMELISSA(Codigo)
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                Call CrearPdf_TA4_GUIA_GROUPTOOLS(Codigo)
            End If
        ElseIf ModeloPdf = "4" Then 'EMPRESA MAQUIHERRAMIENTAS Y CFC ABASTACIMIENTO
            Call CrearPdf_TA4_Guia_MAQHER(Codigo)
        ElseIf ModeloPdf = "5" Then 'EMPRESA PAVICH
            Call CrearPdf_TA4_Guia_Modelo_1(Codigo)
        End If
    End Sub
    Public Function GenerarComprobanteElectronico(ByVal Codigo As Integer, ByVal TipoComprobante As String) As String
        Dim BLFacturacion As New BLFacturacion
        Dim BECabecera As Comprobante
        Dim BEDetalle As New List(Of Detalle)
        Dim BEGuia As New List(Of ListaGuia)
        Dim BEAnticipo As New List(Of Anticipo)
        Dim BECuotas As New List(Of Cuotas)
        BECabecera = BLFacturacion.ObtenerCabeceraSunat(Codigo)
        BEDetalle = BLFacturacion.ObtenerDetalleSunat(Codigo)
        BEGuia = BLFacturacion.ObtenerGuiasFactura(Codigo)
        BEAnticipo = BLFacturacion.ObtenerAnticipos(Codigo)
        BECuotas = BLFacturacion.ObtenerCuotas(Codigo)

        Dim objComprobante As Comprobante = New Comprobante()
        Dim objdetalle As Detalle = New Detalle()

        objComprobante.NumeroDocumentoEmisor = BECabecera.NumeroDocumentoEmisor
        objComprobante.TipoDocumentoEmisor = BECabecera.TipoDocumentoEmisor
        objComprobante.RazonSocialEmisor = BECabecera.RazonSocialEmisor
        objComprobante.NombreComercialEmisor = BECabecera.NombreComercialEmisor
        objComprobante.DireccionEmisor = BECabecera.DomicilioFiscalEmisor
        objComprobante.CodigoFiscalEmisor = BECabecera.CodigoFiscalEmisor

        objComprobante.SerieNumeroComprobante = BECabecera.SerieNumeroComprobante
        objComprobante.FechaEmision = BECabecera.FechaEmision
        objComprobante.HoraEmision = BECabecera.HoraEmision
        objComprobante.FechaVencimiento = BECabecera.FechaVencimiento
        objComprobante.CodigoTipoOperacion = BECabecera.CodigoTipoOperacion
        objComprobante.CodigoTipoComprobante = BECabecera.CodigoTipoComprobante
        objComprobante.MontoEnLetras = BECabecera.MontoEnLetras
        objComprobante.CodigoTipoMoneda = BECabecera.CodigoTipoMoneda
        objComprobante.TipoDocumentoAdquirente = BECabecera.TipoDocumentoAdquirente
        objComprobante.DocumentoAdquirente = BECabecera.DocumentoAdquirente
        objComprobante.RazonSocialAdquirente = BECabecera.RazonSocialAdquirente
        objComprobante.DireccionAdquirente = BECabecera.DireccionAdquirente

        objComprobante.NumeroOrdenCompra = BECabecera.NumeroOrdenCompra
        objComprobante.CodigoTipoNota = BECabecera.CodigoTipoNota
        objComprobante.MotivoNota = BECabecera.MotivoNota
        objComprobante.TipoDocumentoAfectado = BECabecera.TipoDocumentoAfectado
        objComprobante.SerieNumeroDocAfectado = BECabecera.SerieNumeroDocAfectado

        objComprobante.TipoTributo = BECabecera.TipoTributo
        objComprobante.TipoCodigoTributo = BECabecera.TipoCodigoTributo
        objComprobante.NombreTributo = BECabecera.NombreTributo
        objComprobante.CondicionPago = BECabecera.CondicionPago

        objComprobante.MontoTotalImpuestos = BECabecera.MontoTotalImpuestos
        objComprobante.TotalOperacionesGravadas = BECabecera.TotalOperacionesGravadas
        objComprobante.SumatoriaIGV = BECabecera.SumatoriaIGV
        objComprobante.TotalValorVenta = BECabecera.TotalValorVenta
        objComprobante.TotalPrecioVenta = BECabecera.TotalPrecioVenta
        objComprobante.ImporteTotalComprobante = BECabecera.ImporteTotalComprobante
        objComprobante.TotalAnticipos = BECabecera.TotalAnticipos
        objComprobante.ImporteNetoPago = BECabecera.ImporteNetoPago
        objComprobante.MontoNetoPagoDocAfectado = BECabecera.MontoNetoPagoDocAfectado

        Dim con As Integer = 0
        For Each oproducto In BEDetalle
            con += 1
            objdetalle = New Detalle()
            objdetalle.NumeroItem = con
            objdetalle.UnidadMedida = oproducto.UnidadMedida
            objdetalle.Cantidad = oproducto.Cantidad
            objdetalle.CodigoTipoMoneda = BECabecera.CodigoTipoMoneda
            objdetalle.ValorVentaItem = oproducto.ValorVentaItem
            objdetalle.ValorUnitarioxItem = oproducto.ValorUnitarioxItem
            objdetalle.CodigoTipoPrecio = oproducto.CodigoTipoPrecio
            objdetalle.MontoTributoItem = oproducto.MontoTributoItem
            objdetalle.PrecioUnitario = oproducto.PrecioUnitario
            objdetalle.MontoOperacion = oproducto.MontoOperacion
            objdetalle.DescripcionProducto = oproducto.DescripcionProducto
            objdetalle.CodigoProducto = oproducto.CodigoProducto
            objdetalle.IndicadorDescuento = oproducto.IndicadorDescuento
            objdetalle.CodigoDescuento = oproducto.CodigoDescuento
            objdetalle.FactorPorcDescuento = oproducto.FactorPorcDescuento
            objdetalle.MontoDescuento = oproducto.MontoDescuento
            objdetalle.BaseImponibleDescuento = oproducto.BaseImponibleDescuento

            objdetalle.CodigoAfectacionIgv = oproducto.CodigoAfectacionIgv
            objdetalle.PorcentajeImpuestos = oproducto.PorcentajeImpuestos
            objdetalle.CategoriaImpuestos = oproducto.CategoriaImpuestos
            objdetalle.CodigoTributo = oproducto.CodigoTributo
            objdetalle.NombreTributo = oproducto.NombreTributo
            objComprobante.Detalle.Add(objdetalle)
        Next

        'Solo cuando es tipo de comprobante 07 NOTA CREDITO y el tipo de operación sea CORRECCION DEL MONTO O VENCIMIENTO DE LAS CUOTAS
        If BECabecera.CodigoTipoNota = "13" Then
            BECuotas = BLFacturacion.ObtenerCuotas(BECabecera.CodigoDocumentoAfectado)
        End If

        Dim objGuia As New List(Of ListaGuia)
        For Each oGuia In BEGuia
            Dim Guia As New ListaGuia()
            Guia.NumeroGuiaRemision = oGuia.NumeroGuiaRemision
            Guia.CodigoGuiaRemision = oGuia.CodigoGuiaRemision
            objGuia.Add(Guia)
        Next

        Dim objAnticipo As New List(Of Anticipo)
        For Each oAnticipo In BEAnticipo
            Dim Anticipo As New Anticipo()
            Anticipo.NumeroComprobanteAnticipo = oAnticipo.NumeroComprobanteAnticipo
            Anticipo.CodigoTipoMoneda = oAnticipo.CodigoTipoMoneda
            Anticipo.ImporteTotalAnticipo = oAnticipo.ImporteTotalAnticipo
            objAnticipo.Add(Anticipo)
        Next

        Dim objCuotas As New List(Of Cuotas)
        For Each oCuotas In BECuotas
            Dim Cuotas As New Cuotas()
            Cuotas.CodigoCuota = oCuotas.CodigoCuota
            Cuotas.MontoCuota = oCuotas.MontoCuota
            Cuotas.FechaVencimiento = oCuotas.FechaVencimiento
            objCuotas.Add(Cuotas)
        Next
        Dim osunat As New FacturacionSunat
        Dim Mensaje As String = ""
        Mensaje = osunat.EnviarComprobante(objComprobante, objGuia, objAnticipo, objCuotas, Codigo)
        Return Mensaje
    End Function
    Public Function GenerarGuiaElectronica(ByVal Codigo As Integer, ByRef cNumTickSun As String) As String
        Dim Obj As New BLFacturacion
        Dim BECabecera As Guia
        Dim BEDetalle As New List(Of DetalleGuia)

        BECabecera = Obj.ObtenerCabeceraGuiaSunat(Codigo)
        BEDetalle = Obj.ObtenerDetalleGuiaSunat(Codigo)

        Dim objComprobante As Guia = New Guia()
        Dim objdetalle As DetalleGuia = New DetalleGuia()
        objComprobante.SerieNumeroGuia = BECabecera.SerieNumeroGuia
        objComprobante.FechaEmision = BECabecera.FechaEmision
        objComprobante.HoraEmision = BECabecera.HoraEmision
        objComprobante.CodigoTipoGuia = BECabecera.CodigoTipoGuia
        objComprobante.NumeroDocumentoEmisor = BECabecera.NumeroDocumentoEmisor
        objComprobante.TipoDocumentoEmisor = BECabecera.TipoDocumentoEmisor
        objComprobante.RazonSocialEmisor = BECabecera.RazonSocialEmisor
        objComprobante.DocumentoAdquirente = BECabecera.DocumentoAdquirente
        objComprobante.TipoDocumentoAdquirente = BECabecera.TipoDocumentoAdquirente
        objComprobante.RazonSocialAdquirente = BECabecera.RazonSocialAdquirente
        objComprobante.MotivoTraslado = BECabecera.MotivoTraslado
        objComprobante.IndicacodorTransbordo = BECabecera.IndicacodorTransbordo
        objComprobante.PesoBrutoBienes = BECabecera.PesoBrutoBienes
        objComprobante.ModalidadTraslado = BECabecera.ModalidadTraslado
        objComprobante.FechaInicioTraslado = BECabecera.FechaInicioTraslado
        objComprobante.UbigeoLlegada = BECabecera.UbigeoLlegada
        objComprobante.DireccionLlegada = BECabecera.DireccionLlegada
        objComprobante.CodigoEstablecimientoLlegada = BECabecera.CodigoEstablecimientoLlegada
        objComprobante.UbigeoPartida = BECabecera.UbigeoPartida
        objComprobante.DireccionPartida = BECabecera.DireccionPartida
        objComprobante.CodigoEstablecimientoPartida = BECabecera.CodigoEstablecimientoPartida
        objComprobante.TipoDocumentoAdjunto = BECabecera.TipoDocumentoAdjunto
        objComprobante.NumDocumentoAdjunto = BECabecera.NumDocumentoAdjunto
        objComprobante.DesCompAdjDocumentoAdjunto = BECabecera.DesCompAdjDocumentoAdjunto
        Dim con As Integer = 0
        For Each oproducto In BEDetalle
            con += 1
            objdetalle = New DetalleGuia()
            objdetalle.NumeroItem = con
            objdetalle.UnidadMedida = oproducto.UnidadMedida
            objdetalle.Cantidad = oproducto.Cantidad
            objdetalle.DescripcionProducto = oproducto.DescripcionProducto
            objdetalle.CodigoProducto = oproducto.CodigoProducto
            objComprobante.Detalle.Add(objdetalle)
        Next
        Dim osunat As New FacturacionSunat
        Dim Mensaje As String = ""
        Mensaje = osunat.EnviarGuia(objComprobante, Codigo, cNumTickSun)
        Return Mensaje
    End Function
    Public Function ConsultaValidezComprobante(ByVal TipoComprobate As String, ByVal Serie As String,
                                               ByVal NumeroComprobate As Integer, ByVal FechaEmision As Date,
                                               ByVal Monto As Decimal, ByRef response As BEResponseValidezComprobante) As String
        Dim Mensaje As String = ""
        Dim osunat As New FacturacionSunat
        Mensaje = osunat.ConsultaValidezComprobante(TipoComprobate, Serie, NumeroComprobate, FechaEmision, Monto, response)
        Return Mensaje
    End Function
    Public Function ConsultarEstadoEnvioGuia(ByVal tipoguia As String, ByVal serienumero As String, ByVal numTicket As String,
                                             ByVal Codigo As Integer) As String
        Dim Mensaje As String = ""
        Dim osunat As New FacturacionSunat
        Mensaje = osunat.ConsultarEstadoEnvioGuia(tipoguia, serienumero, numTicket, Codigo)
        Return Mensaje
    End Function
    Public Function ConsultarCDR(ByVal Codigo As Integer, ByVal ruc As String, ByVal tipo As String, ByVal serie As String, ByVal numero As String) As String
        Dim osunat As New FacturacionSunat
        Dim Mensaje As String = ""
        Mensaje = osunat.ConsultarCDR(Codigo, ruc, tipo, serie, numero)
        Return Mensaje
    End Function
    Public Function ActualizarEstadoSunatComprobante(ByVal Codigo As Integer, ByVal numeroticket As String, ByVal NombreXmlBaja As String, ByRef CodigoRespuesta As Integer) As String
        Dim osunat As New FacturacionSunat
        Dim Mensaje As String = ""
        Mensaje = osunat.ActualizarEstadoSunatComprobante(Codigo, numeroticket, NombreXmlBaja, CodigoRespuesta)
        Return Mensaje
    End Function
    Function ComunicacionBajaComprobante(ByVal Codigo As Integer, ByVal MotivoAnu As String, ByRef CodigoRespuesta As Integer, ByVal TipoComprobante As String) As String
        Dim objbaja As ComprobanteBaja
        Dim BLFacturacion As New BLFacturacion
        objbaja = BLFacturacion.ObtenerComunicacionBajaComprobante(Codigo)
        objbaja.MotivoBaja = MotivoAnu
        Dim numerocomprobante As String = objbaja.SerieDocumento + "-" + objbaja.NumCorreDocu
        Dim osunat As New FacturacionSunat
        Dim Mensaje As String = ""
        Mensaje = osunat.EnviarComunicacionBajaComprobante(Codigo, numerocomprobante, objbaja, CodigoRespuesta, TipoComprobante)
        Return Mensaje
    End Function
    Private Function PrepararDatosQR(ByVal ruc As String, ByVal tipodoc As String,
                                     ByVal serienumero As String, ByVal igv As String,
                                     ByVal total As String, ByVal fecha As String,
                                     ByVal codigohash As String) As String

        Dim pContenido As String = ""
        pContenido = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}", ruc, tipodoc,
                                   serienumero, igv, igv, total, fecha, codigohash)
        Return pContenido
    End Function
    Private Function PrepararDatosQRGuia(ByVal codigoqr As String) As String

        Dim pContenido As String = ""
        pContenido = String.Format("{0}", codigoqr)
        Return pContenido
    End Function
    Sub PintarCuadrado(ByVal NumGrosor As Double, ByVal Color As BaseColor,
                       ByVal X1 As Integer, ByVal Y1 As Integer,
                       ByVal X2 As Integer, ByVal Y2 As Integer,
                       ByVal PdfWriter As PdfWriter, ByVal PdfDoc As Document)
        Dim Linea As PdfContentByte
        Linea = PdfWriter.DirectContent
        Linea.SetLineWidth(NumGrosor)
        Linea.SetColorStroke(Color)
        Linea.MoveTo(X1, Y1)
        Linea.LineTo(X2, Y1)
        Linea.Stroke()
        Linea.MoveTo(X2, Y1)
        Linea.LineTo(X2, Y2)
        Linea.Stroke()
        Linea.MoveTo(X2, Y2)
        Linea.LineTo(X1, Y2)
        Linea.Stroke()
        Linea.MoveTo(X1, Y2)
        Linea.LineTo(X1, Y1)
        Linea.Stroke()
    End Sub
    Sub CrearPdf_TA4_Modelo_1(ByVal Codigo As Integer, ByVal TipoComprobante As String)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        Dim Adicional As New DataTable
        Dim Cuotas As New DataTable
        Dim Guia As New DataTable

        General = Obj.ObtenerDatosGeneralesPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdf(Codigo)
        Adicional = Obj.ObtenerDatosAdicionalPdf(Codigo)
        Cuotas = Obj.ObtenerDatosCuotasPdf(Codigo)
        Guia = Obj.ObtenerGuiasPdf(Codigo)
        Dim Guias As String = ""

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 20.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorFondo As New BaseColor(16, 67, 91)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

        Dim iLine As Integer
        Dim iFila As Integer

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(4)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 6.5F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(15, 720)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General) & " ELECTRÓNICA", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Fecha: " + ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 415, 740, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCabeceraTitulo As Single() = New Single() {8.0F, 8.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase("Datos del emisor", Font9NB))
        Col.Border = 0
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Adquiriente / Usuario", Font9NB))
        Col.Border = 0
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCabeceraTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {2.4F, 5.6F, 2.4F, 5.6F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase("RUC:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Identificacion", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Nombre:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Nro Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionEmisor", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Nombre:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Sucursal:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Sucursal", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalleTitulo As Single() = New Single() {1.8F, 7.2F, 1.5F, 1.5F, 2.1F, 1.9F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Descripción", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Unidad", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Valor Unitario", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Valor Venta", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalle As Single() = New Single() {1.8F, 7.2F, 1.5F, 1.5F, 2.1F, 1.9F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)

        If ObtenerDato("CodigoEstado", General) = 3 Then
            Dim RutaImagenAnulado As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaAnulado")
            Dim RutaImagenAnuladoBMP As iTextSharp.text.Image
            RutaImagenAnuladoBMP = iTextSharp.text.Image.GetInstance(RutaImagenAnulado)
            RutaImagenAnuladoBMP.ScaleAbsoluteHeight(700.0F)
            RutaImagenAnuladoBMP.ScaleAbsoluteWidth(600.0F)
            RutaImagenAnuladoBMP.SpacingBefore = 20.0F
            RutaImagenAnuladoBMP.SpacingAfter = 10.0F
            RutaImagenAnuladoBMP.SetAbsolutePosition(30, 0)
            PdfDoc.Add(RutaImagenAnuladoBMP)
        End If

        For i = 0 To Detalle.Rows.Count - 1

            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorUnitario", Detalle, i), 6), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorVenta", Detalle, i), 2), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)
        Next

        PdfDoc.Add(TablaDetalle)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaTotalTitulo"
        Dim TablaTotalTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsTotalTitulo As Single() = New Single() {8.0F, 3.0F, 5.0F}
        TablaTotalTitulo.WidthPercentage = 95
        TablaTotalTitulo.SetWidths(WidthsTotalTitulo)

        Col = New PdfPCell(New Phrase("Datos de Relación / Afectación", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Totales del documento", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)

        PdfDoc.Add(TablaTotalTitulo)
#End Region
#Region "TablaTotal"
        Dim TablaTotal As PdfPTable = New PdfPTable(5)
        Dim WidthsTotal As Single() = New Single() {2.8F, 5.2F, 3.0F, 3.0F, 2.0F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        For i = 0 To Guia.Rows.Count - 1
            If i = 0 Then
                Guias = ObtenerDato("NumeroGuiaRemision", Guia, i)
            Else
                Guias = Guias + ", " + ObtenerDato("NumeroGuiaRemision", Guia, i)
            End If
        Next

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("Docuemento afectado:", Font8NB))
            Col.BackgroundColor = ColorFondo
        Else
            Col = New PdfPCell(New Phrase("Guias de remisión:", Font8NB))
            Col.BackgroundColor = ColorFondo
        End If
        Col.Border = 0
        TablaTotal.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("SerieNumeroDocAfectado", General), Font8))
        Else
            Col = New PdfPCell(New Phrase(Guias, Font8))
        End If
        Col.Border = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Sub Total:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("Tipo Operación:", Font8NB))
            Col.BackgroundColor = ColorFondo
        Else
            Col = New PdfPCell(New Phrase("Ordenes de compra:", Font8NB))
            Col.BackgroundColor = ColorFondo
        End If
        Col.Border = 0
        TablaTotal.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoNotaCredito", General), Font8))
        Else
            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroOrdenCompra", General), Font8))
        End If
        Col.Border = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Descuentos:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("Motivo:", Font8NB))
            Col.BackgroundColor = ColorFondo
        Else
            Col = New PdfPCell(New Phrase("", Font8NB))
            Col.BackgroundColor = ColorFondo
        End If
        Col.Border = 0
        TablaTotal.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("Motivo", General), Font8))
        Else
            Col = New PdfPCell(New Phrase("", Font8))
        End If
        Col.Border = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Ventas Gravadas:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total IGV 18%:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Importe Total de Venta:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)
        PdfDoc.Add(TablaTotal)
#End Region
#Region "TablaAdicionalTitulo"
        Dim TablaAdicionalTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsAdicionalTitulo As Single() = New Single() {8.0F, 8.0F}
        TablaAdicionalTitulo.WidthPercentage = 95
        TablaAdicionalTitulo.SetWidths(WidthsAdicionalTitulo)

        Col = New PdfPCell(New Phrase("Información adicional", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaAdicionalTitulo.AddCell(Col)
        TablaAdicionalTitulo.AddCell(CVacio)

        PdfDoc.Add(TablaAdicionalTitulo)
#End Region
#Region "TablaAdicional"
        Dim TablaAdicional As PdfPTable = New PdfPTable(3)
        Dim WidthsAdicional As Single() = New Single() {2.6F, 5.4F, 8.0F}
        TablaAdicional.WidthPercentage = 95
        TablaAdicional.SetWidths(WidthsAdicional)
        For i = 0 To Adicional.Rows.Count - 1
            If ObtenerDato("Valor", Adicional, i) <> "" Then
                Col = New PdfPCell(New Phrase(ObtenerDato("Titulo", Adicional, i), Font8NB))
                Col.BackgroundColor = ColorFondo
                Col.Border = 0
                TablaAdicional.AddCell(Col)
                Col = New PdfPCell(New Phrase(ObtenerDato("Valor", Adicional, i), Font8))
                Col.Border = 0
                TablaAdicional.AddCell(Col)
                TablaAdicional.AddCell(CVacio)
            End If
        Next
        PdfDoc.Add(TablaAdicional)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaRetencionDetraccionTitulo"
        Dim TablaRetencionDetraccionTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaRetencionDetraccionTitulo As Single() = New Single() {8.0F, 8.0F}
        TablaRetencionDetraccionTitulo.WidthPercentage = 95
        TablaRetencionDetraccionTitulo.SetWidths(WidthsTablaRetencionDetraccionTitulo)
        Dim MontoRetencion As Double = ObtenerDato("MontoRetencion", General)
        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("Información de " + MensajeRetencionDetraccion, Font9NB))
            Col.Border = 0
            Col.BackgroundColor = ColorFondo
            Col.HorizontalAlignment = 1
            TablaRetencionDetraccionTitulo.AddCell(Col)
            TablaRetencionDetraccionTitulo.AddCell(CVacio)
            PdfDoc.Add(TablaRetencionDetraccionTitulo)
        End If

#End Region
#Region "TablaRetencionDetraccion"
        Dim TablaRetencionDetraccion As PdfPTable = New PdfPTable(4)
        Dim WidthsTablaRetencionDetraccion As Single() = New Single() {2.5F, 2.5F, 3.0F, 8.0F}
        TablaRetencionDetraccion.WidthPercentage = 95
        TablaRetencionDetraccion.SetWidths(WidthsTablaRetencionDetraccion)
        MontoRetencion = ObtenerDato("MontoRetencion", General)
        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "detracción"
            Else
                MensajeRetencionDetraccion = "retención"
            End If
            Col = New PdfPCell(New Phrase("Base imponible  de la " + MensajeRetencionDetraccion, Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaRetencionDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase("Porcentaje de " + MensajeRetencionDetraccion, Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaRetencionDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase("Monto de la " + MensajeRetencionDetraccion, Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaRetencionDetraccion.AddCell(Col)
            TablaRetencionDetraccion.AddCell(CVacio)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaRetencionDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(CDbl(IIf(AplicaDetraccion = True, MDIPrincipal.PorcentajeDetraccion, MDIPrincipal.PorcentajeRetencion)) * 100, 2) + "%", Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaRetencionDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", General), 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaRetencionDetraccion.AddCell(Col)
            TablaRetencionDetraccion.AddCell(CVacio)
            PdfDoc.Add(TablaRetencionDetraccion)
            PdfDoc.Add(New Paragraph(" "))
        End If
#End Region
#Region "TablaCuotasTitulo"
        Dim TablaCuotasTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCuotasTitulo As Single() = New Single() {8.0F, 8.0F}
        TablaCuotasTitulo.WidthPercentage = 95
        TablaCuotasTitulo.SetWidths(WidthsCuotasTitulo)
        Dim ContadorCuota As Integer = 0
        ContadorCuota = Cuotas.Rows.Count
        If ContadorCuota > 0 Then
            Col = New PdfPCell(New Phrase("Información de Pago", Font9NB))
            Col.Border = 0
            Col.BackgroundColor = ColorFondo
            Col.HorizontalAlignment = 1
            TablaCuotasTitulo.AddCell(Col)
            TablaCuotasTitulo.AddCell(CVacio)
            PdfDoc.Add(TablaCuotasTitulo)
        End If

#End Region
#Region "TablaCuotas"
        Dim TablaCuotas As PdfPTable = New PdfPTable(4)
        Dim WidthsCuotas As Single() = New Single() {2.5F, 2.5F, 3.0F, 8.0F}
        TablaCuotas.WidthPercentage = 95
        TablaCuotas.SetWidths(WidthsCuotas)
        Dim ContadorCuotas As Integer = 0
        ContadorCuotas = Cuotas.Rows.Count
        If ContadorCuotas > 0 Then
            Col = New PdfPCell(New Phrase("Importe Neto Pago:", Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", General), 2), Font8))
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            TablaCuotas.AddCell(CVacio)
            TablaCuotas.AddCell(CVacio)

            Col = New PdfPCell(New Phrase("N° Cuota", Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            Col = New PdfPCell(New Phrase("Monto Cuota", Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            Col = New PdfPCell(New Phrase("Fecha Vencimiento", Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            TablaCuotas.AddCell(CVacio)

            For i = 0 To Cuotas.Rows.Count - 1
                Col = New PdfPCell(New Phrase(ObtenerDato("cCodCuo", Cuotas, i), Font8))
                Col.Border = 0
                Col.HorizontalAlignment = 1
                TablaCuotas.AddCell(Col)
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nMonCuo", Cuotas, i), 2), Font8))
                Col.Border = 0
                Col.HorizontalAlignment = 1
                TablaCuotas.AddCell(Col)
                Col = New PdfPCell(New Phrase(ObtenerDato("dFecVen", Cuotas, i), Font8))
                Col.Border = 0
                Col.HorizontalAlignment = 1
                TablaCuotas.AddCell(Col)
                TablaCuotas.AddCell(CVacio)
            Next
            PdfDoc.Add(TablaCuotas)
            PdfDoc.Add(New Paragraph(" "))
        End If
#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(2)
        Dim WidthsMontoLetras As Single() = New Single() {12.0F, 4.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font8N))
        Col.Border = 0
        TablaMontoLetras.AddCell(Col)
        TablaMontoLetras.AddCell(CVacio)
        PdfDoc.Add(TablaMontoLetras)

        For iFila = 1 To 40
            PdfDoc.Add(New Paragraph(" "))
            iLine = PdfWriter.GetVerticalPosition(True)
            If iLine < 90 Then
                Exit For
            End If
        Next
#End Region
#Region "Firma"
        Dim Qr As String
        Dim Helpers As New Helpers
        Qr = PrepararDatosQR(ObtenerDato("NumRuc", General), ObtenerDato("TipCom", General), ObtenerDato("Serie", General), ObtenerDato("TotalImpuestos", General),
                        ObtenerDato("ImporteTotal", General), ObtenerDato("FechaEmision", General), ObtenerDato("CodigoHas", General))

        Dim TbFirma As PdfPTable = New PdfPTable(2)
        Dim WidthsFirma As Single() = New Single() {5.0F, 11.0F}
        TbFirma.WidthPercentage = 95
        TbFirma.SetWidths(WidthsFirma)
        Dim QrImage As iTextSharp.text.Image
        QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
        QrImage.ScaleToFit(90.0F, 120.0F)
        QrImage.SpacingBefore = 20.0F
        QrImage.SpacingAfter = 10.0F
        QrImage.SetAbsolutePosition(30, 40)
        PdfDoc.Add(QrImage)

        TbFirma.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Autorizado mediante SOL para Emisión de Comprobantes Electrónicos(SEE-DSC)", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbFirma.AddCell(Col)

        TbFirma.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Puede consultar este comprobante en:https://ww1.sunat.gob.pe/ol-ti-itconsvalicpe/ConsValiCpe.htm", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbFirma.AddCell(Col)

        TbFirma.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("CodigoHas", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbFirma.AddCell(Col)
        PdfDoc.Add(TbFirma)
        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_Modelo_2(ByVal Codigo As Integer, ByVal TipoComprobante As String)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        Dim Cuotas As New DataTable
        Dim Guia As New DataTable
        General = Obj.ObtenerDatosGeneralesPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdf(Codigo)
        Cuotas = Obj.ObtenerDatosCuotasPdf(Codigo)
        Guia = Obj.ObtenerGuiasPdf(Codigo)
        Dim Guias As String = ""

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 20.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorFondo As New BaseColor(41, 182, 246)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7B As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.WHITE))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(4)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 6.5F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(18, 720)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        If TipoComprobante = "99" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General), Font8N))
        Else
            Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General) & " ELECTRÓNICA", Font8N))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Fecha: " + ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 415, 740, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCabeceraTitulo As Single() = New Single() {10.0F, 6.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase("Adquiriente / Cliente", Font9NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Datos Comprobante", Font9NB))
        Col.Border = 0
        Col.BorderWidthLeft = 1
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {2.4F, 7.6F, 2.5F, 3.5F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        For i = 0 To Guia.Rows.Count - 1
            If i = 0 Then
                Guias = ObtenerDato("NumeroGuiaRemision", Guia, i)
            Else
                Guias = Guias + ", " + ObtenerDato("NumeroGuiaRemision", Guia, i)
            End If
        Next

        Col = New PdfPCell(New Phrase("Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Identificacion", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Sucursal:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Sucursal", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Nro Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Moneda:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("MONEDA", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cliente:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("Doc.Afectado:", Font8NB))
        Else
            Col = New PdfPCell(New Phrase("O/C:", Font8NB))
        End If
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("SerieNumeroDocAfectado", General), Font7))
        Else
            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroOrdenCompra", General), Font7))
        End If
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("Tipo Operación:", Font8NB))
        Else
            Col = New PdfPCell(New Phrase("Guia(S):", Font8NB))
        End If
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoNotaCredito", General), Font7))
        Else
            Col = New PdfPCell(New Phrase(Guias, Font7))
        End If
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("Motivo:", Font8NB))
        Else
            Col = New PdfPCell(New Phrase("Condición Pago:", Font8NB))
        End If
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("Motivo", General), Font7))
        Else
            Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font7))
        End If

        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Vendedor:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("VENDEDOR", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalleTitulo As Single() = New Single() {2.0F, 7.0F, 1.5F, 1.5F, 2.1F, 1.9F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Descripción", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Unidad", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        If TipoComprobante = "99" Then
            Col = New PdfPCell(New Phrase("Precio Unitario", Font8NB))
        Else
            Col = New PdfPCell(New Phrase("Valor Unitario", Font8NB))
        End If
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        If TipoComprobante = "99" Then
            Col = New PdfPCell(New Phrase("Sub Total", Font8NB))
        Else
            Col = New PdfPCell(New Phrase("Valor Venta", Font8NB))
        End If

        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalle As Single() = New Single() {2.0F, 7.0F, 1.5F, 1.5F, 2.1F, 1.9F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)

        For i = 0 To Detalle.Rows.Count - 1
            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthLeft = 0.5
            Col.BorderColorLeft = ColorFondo
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)


            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If TipoComprobante = "99" Then
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Precio", Detalle, i), 6), Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorUnitario", Detalle, i), 6), Font7))
            End If
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            If TipoComprobante = "99" Then
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", Detalle, i), 2), Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorVenta", Detalle, i), 2), Font7))
            End If
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)
        Next


        Col = New PdfPCell(New Phrase(" ", Font7B))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7B))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7B))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7B))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)


        Col = New PdfPCell(New Phrase(" ", Font7B))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)


        Col = New PdfPCell(New Phrase(" ", Font7B))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)
        PdfDoc.Add(TablaDetalle)

#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(1)
        Dim WidthsMontoLetras As Single() = New Single() {16.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BackgroundColor = ColorFondo
        TablaMontoLetras.AddCell(Col)
        TablaMontoLetras.AddCell(CVacio)
        PdfDoc.Add(TablaMontoLetras)

        PdfDoc.Add(New Paragraph(" "))
        Dim Line = PdfWriter.GetVerticalPosition(True)
        If Line <= 175 Then
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
        End If
#End Region
#Region "TablaTotalTitulo"
        Dim TablaTotalTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsTotalTitulo As Single() = New Single() {8.0F, 3.0F, 5.0F}
        TablaTotalTitulo.WidthPercentage = 95
        TablaTotalTitulo.SetWidths(WidthsTotalTitulo)

        Col = New PdfPCell(New Phrase(ObtenerDato("Banco", General), Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTALES DEL COMPROBANTE", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)


        Col = New PdfPCell(New Phrase(" ", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaTotalTitulo.AddCell(Col)

        PdfDoc.Add(TablaTotalTitulo)
#End Region
#Region "TablaTotal"

        Dim TablaTotal As PdfPTable = New PdfPTable(6)
        Dim WidthsTotal As Single() = New Single() {2.0F, 3.0F, 3.0F, 3.0F, 3.0F, 2.0F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        Col = New PdfPCell(New Phrase("MONEDA", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° CTA CORRIENTE", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° CCI", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Sub Total:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        If TipoComprobante = "99" Then
            Col = New PdfPCell(New Phrase(FormatNumber(cdbl(ObtenerDato("SubTotal", General)) + cdbl(obtenerDato("TotalImpuestos", General)), 2), Font8))
        Else
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        End If
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("SOLES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCIS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Total Descuentos:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)



        Col = New PdfPCell(New Phrase("DOLARES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCD", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCID", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Total Ventas Gravadas:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        If TipoComprobante = "99" Then
            Col = New PdfPCell(New Phrase(FormatNumber(CDbl(ObtenerDato("TotalValorVenta", General)) + CDbl(ObtenerDato("TotalImpuestos", General)), 2), Font8))
        Else
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font8))
        End If
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)


        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        If TipoComprobante = "99" Then
            Col = New PdfPCell(New Phrase(" ", Font8NB))
        Else
            Col = New PdfPCell(New Phrase("Total IGV 18%:", Font8NB))
        End If

        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        If TipoComprobante = "99" Then
            Col = New PdfPCell(New Phrase(FormatNumber(0, 2), Font8))
        Else
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        End If
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Importe Total de Venta:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        PdfDoc.Add(TablaTotal)

#End Region
#Region "TablaRetencionDetraccionTitulo"
        Dim TablaRetencionDetraccionTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaRetencionDetraccionTitulo As Single() = New Single() {8.0F, 8.0F}
        TablaRetencionDetraccionTitulo.WidthPercentage = 95
        TablaRetencionDetraccionTitulo.SetWidths(WidthsTablaRetencionDetraccionTitulo)
        Dim MontoRetencion As Double = ObtenerDato("MontoRetencion", General)
        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("Información de " + MensajeRetencionDetraccion, Font9NB))
            Col.Border = 0
            Col.BackgroundColor = ColorFondo
            Col.HorizontalAlignment = 1
            TablaRetencionDetraccionTitulo.AddCell(Col)
            TablaRetencionDetraccionTitulo.AddCell(CVacio)
            PdfDoc.Add(TablaRetencionDetraccionTitulo)
        End If

#End Region
#Region "TablaRetencionDetraccion"
        Dim TablaRetencionDetraccion As PdfPTable = New PdfPTable(4)
        Dim WidthsTablaRetencionDetraccion As Single() = New Single() {2.5F, 2.5F, 3.0F, 8.0F}
        TablaRetencionDetraccion.WidthPercentage = 95
        TablaRetencionDetraccion.SetWidths(WidthsTablaRetencionDetraccion)
        MontoRetencion = ObtenerDato("MontoRetencion", General)
        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "detracción"
            Else
                MensajeRetencionDetraccion = "retención"
            End If

            Col = New PdfPCell(New Phrase("Base imponible  de la " + MensajeRetencionDetraccion, Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaRetencionDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase("Porcentaje de " + MensajeRetencionDetraccion, Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaRetencionDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase("Monto de la " + MensajeRetencionDetraccion, Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaRetencionDetraccion.AddCell(Col)
            TablaRetencionDetraccion.AddCell(CVacio)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaRetencionDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(CDbl(IIf(AplicaDetraccion = True, MDIPrincipal.PorcentajeDetraccion, MDIPrincipal.PorcentajeRetencion)) * 100, 2) + "%", Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaRetencionDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", General), 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaRetencionDetraccion.AddCell(Col)
            TablaRetencionDetraccion.AddCell(CVacio)
            PdfDoc.Add(TablaRetencionDetraccion)
        End If

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCuotasTitulo"
        Dim TablaCuotasTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCuotasTitulo As Single() = New Single() {8.0F, 8.0F}
        TablaCuotasTitulo.WidthPercentage = 95
        TablaCuotasTitulo.SetWidths(WidthsCuotasTitulo)
        Dim ContadorCuota As Integer = 0
        ContadorCuota = Cuotas.Rows.Count
        If ContadorCuota > 0 Then
            Col = New PdfPCell(New Phrase("Información de Pago", Font9NB))
            Col.Border = 0
            Col.BackgroundColor = ColorFondo
            Col.HorizontalAlignment = 1
            TablaCuotasTitulo.AddCell(Col)
            TablaCuotasTitulo.AddCell(CVacio)
            PdfDoc.Add(TablaCuotasTitulo)
        End If

#End Region
#Region "TablaCuotas"
        Dim TablaCuotas As PdfPTable = New PdfPTable(4)
        Dim WidthsCuotas As Single() = New Single() {2.5F, 2.5F, 3.0F, 8.0F}
        TablaCuotas.WidthPercentage = 95
        TablaCuotas.SetWidths(WidthsCuotas)
        Dim ContadorCuotas As Integer = 0
        ContadorCuotas = Cuotas.Rows.Count
        If ContadorCuotas > 0 Then
            Col = New PdfPCell(New Phrase("Importe Neto Pago:", Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", General), 2), Font8))
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            TablaCuotas.AddCell(CVacio)
            TablaCuotas.AddCell(CVacio)

            Col = New PdfPCell(New Phrase("N° Cuota", Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            Col = New PdfPCell(New Phrase("Monto Cuota", Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            Col = New PdfPCell(New Phrase("Fecha Vencimiento", Font8NB))
            Col.BackgroundColor = ColorFondo
            Col.Border = 0
            TablaCuotas.AddCell(Col)
            TablaCuotas.AddCell(CVacio)

            For i = 0 To Cuotas.Rows.Count - 1
                Col = New PdfPCell(New Phrase(ObtenerDato("cCodCuo", Cuotas, i), Font8))
                Col.Border = 0
                Col.HorizontalAlignment = 1
                TablaCuotas.AddCell(Col)
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nMonCuo", Cuotas, i), 2), Font8))
                Col.Border = 0
                Col.HorizontalAlignment = 1
                TablaCuotas.AddCell(Col)
                Col = New PdfPCell(New Phrase(ObtenerDato("dFecVen", Cuotas, i), Font8))
                Col.Border = 0
                Col.HorizontalAlignment = 1
                TablaCuotas.AddCell(Col)
                TablaCuotas.AddCell(CVacio)
            Next
            PdfDoc.Add(TablaCuotas)
            PdfDoc.Add(New Paragraph(" "))
        End If
        For Fila = 1 To 48
            Dim Linea = PdfWriter.GetVerticalPosition(True)
            If Linea <= 90 Then
                Exit For
            End If
            PdfDoc.Add(New Paragraph(" "))
        Next
#End Region
        If TipoComprobante = "99" Then
            PdfDoc.Close()
        Else
#Region "CodigoHas"
            Dim Qr As String
            Dim Helpers As New Helpers
            Qr = PrepararDatosQR(ObtenerDato("NumRuc", General), ObtenerDato("TipCom", General), ObtenerDato("Serie", General), ObtenerDato("TotalImpuestos", General),
                        ObtenerDato("ImporteTotal", General), ObtenerDato("FechaEmision", General), ObtenerDato("CodigoHas", General))

            Dim TbCodigoHas As PdfPTable = New PdfPTable(2)
            Dim WidthsCodigoHas As Single() = New Single() {5.0F, 11.0F}
            TbCodigoHas.WidthPercentage = 95
            TbCodigoHas.SetWidths(WidthsCodigoHas)

            Dim QrImage As iTextSharp.text.Image
            QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
            QrImage.ScaleToFit(70.0F, 70.0F)
            QrImage.SpacingBefore = 20.0F
            QrImage.SpacingAfter = 10.0F
            QrImage.SetAbsolutePosition(25, 35)
            PdfDoc.Add(QrImage)


            TbCodigoHas.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("Autorizado mediante SOL para Emisión de Comprobantes Electrónicos(SEE-DSC)", Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TbCodigoHas.AddCell(Col)

            TbCodigoHas.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("Puede consultar este comprobante en:https://ww1.sunat.gob.pe/ol-ti-itconsvalicpe/ConsValiCpe.htm", Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TbCodigoHas.AddCell(Col)

            TbCodigoHas.AddCell(CVacio)
            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoHas", General), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TbCodigoHas.AddCell(Col)

            PdfDoc.Add(TbCodigoHas)

            PdfDoc.Close()
#End Region
        End If

    End Sub
    Sub CrearCotizacion_Pdf_TA4_Modelo_1(ByVal Codigo As Integer)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        General = Obj.ObtenerDatosGeneralesPdfCotizacion(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdfCotizacion(Codigo)
        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        Dim RutaCotizacion As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion1")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion2")
        End If
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 10.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaCotizacion + NombrePdf, FileMode.Create))
        Dim ColorFondo As New BaseColor(16, 67, 91)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(4)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 6.5F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(15, 720)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("COTIZACIÓN", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Fecha: " + ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 415, 740, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCabeceraTitulo As Single() = New Single() {10.0F, 6.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase("Adquiriente / Cliente", Font9NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Datos Cotización", Font9NB))
        Col.Border = 0
        Col.BorderWidthLeft = 1
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {2.4F, 7.6F, 2.5F, 3.5F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase("Nro Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Moneda:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("MONEDA", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cliente:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)


        Col = New PdfPCell(New Phrase("F.Vencimiento:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)


        Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Vendedor:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("VENDEDOR", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Condición Pago:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Tiempo Entrega:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("cTieEnt", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaMenajeCuerpo"
        Dim TablaMenajeCuerpo As PdfPTable = New PdfPTable(1)
        Dim WidthsMenajeCuerpo As Single() = New Single() {16.0F}
        TablaMenajeCuerpo.WidthPercentage = 95
        TablaMenajeCuerpo.SetWidths(WidthsMenajeCuerpo)

        Col = New PdfPCell(New Phrase(ObtenerDato("MensajeCuerpo", General), Font8))
        Col.Border = 0
        TablaMenajeCuerpo.AddCell(Col)
        PdfDoc.Add(TablaMenajeCuerpo)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(7)
        Dim WidthsDetalleTitulo As Single() = New Single() {2.0F, 6.3F, 1.5F, 1.5F, 1.8F, 1.7F, 1.2F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Descripción", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Unidad", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Valor Unitario", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Valor Venta", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Desc.(%)", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(7)
        Dim WidthsDetalle As Single() = New Single() {2.0F, 6.3F, 1.5F, 1.5F, 1.8F, 1.7F, 1.2F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)

        For i = 0 To Detalle.Rows.Count - 1
            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthLeft = 0.5
            Col.BorderColorLeft = ColorFondo
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorUnitario", Detalle, i), 6), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorVenta", Detalle, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("PorcentajeDesc", Detalle, i), 5), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)
        Next


        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)


        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)


        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)

        PdfDoc.Add(TablaDetalle)
#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(1)
        Dim WidthsMontoLetras As Single() = New Single() {16.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BackgroundColor = ColorFondo
        TablaMontoLetras.AddCell(Col)
        TablaMontoLetras.AddCell(CVacio)
        PdfDoc.Add(TablaMontoLetras)
        PdfDoc.Add(New Paragraph(" "))
        Dim Line = PdfWriter.GetVerticalPosition(True)
        If Line <= 175 Then
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
        End If
#End Region
#Region "TablaTotalTitulo"
        Dim TablaTotalTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsTotalTitulo As Single() = New Single() {8.0F, 3.0F, 5.0F}
        TablaTotalTitulo.WidthPercentage = 95
        TablaTotalTitulo.SetWidths(WidthsTotalTitulo)


        Col = New PdfPCell(New Phrase(ObtenerDato("Banco", General), Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTALES DEL COMPROBANTE", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaTotalTitulo.AddCell(Col)

        PdfDoc.Add(TablaTotalTitulo)
#End Region
#Region "TablaTotal"

        Dim TablaTotal As PdfPTable = New PdfPTable(6)
        Dim WidthsTotal As Single() = New Single() {1.4F, 2.6F, 4.0F, 3.0F, 3.0F, 2.0F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        Col = New PdfPCell(New Phrase("MONEDA", Font7NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase("N° CTA CORRIENTE", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase("N° CTA INTERBANCARIO", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Sub Total:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("SOLES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCIS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Descuentos:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("DOLARES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCD", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCID", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Ventas Gravadas:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total IGV 18%:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)


        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Importe Total de Venta:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        PdfDoc.Add(TablaTotal)

#End Region
#Region "TablaFinal"
        Dim TablaFinal As PdfPTable = New PdfPTable(2)
        Dim WidthsFinal As Single() = New Single() {8.0F, 8.0F}
        TablaFinal.WidthPercentage = 95
        TablaFinal.SetWidths(WidthsFinal)

        Col = New PdfPCell(New Phrase("OBSERVACIONES ADICIONALES", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaFinal.AddCell(Col)
        TablaFinal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase(ObtenerDato("cObservaciones", General), Font8))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.HorizontalAlignment = 0
        TablaFinal.AddCell(Col)
        TablaFinal.AddCell(CVacio)

        PdfDoc.Add(TablaFinal)
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "Tabla"
        Dim Tabla As PdfPTable = New PdfPTable(1)
        Dim WidthsTabla As Single() = New Single() {16.0F}
        Tabla.WidthPercentage = 95
        Tabla.SetWidths(WidthsTabla)

        Col = New PdfPCell(New Phrase(ObtenerDato("MensajeCuerpo", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        Tabla.AddCell(Col)
        PdfDoc.Add(Tabla)
        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearCotizacion_Pdf_TA4_Modelo_2(ByVal Codigo As Integer)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        General = Obj.ObtenerDatosGeneralesPdfCotizacion(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdfCotizacion(Codigo)
        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        Dim RutaCotizacion As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion1")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion2")
        End If
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 10.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaCotizacion + NombrePdf, FileMode.Create))
        Dim ColorFondo As New BaseColor(41, 182, 246)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(4)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 6.5F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(15, 720)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("COTIZACIÓN", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Fecha: " + ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 415, 740, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCabeceraTitulo As Single() = New Single() {10.0F, 6.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase("Adquiriente / Cliente", Font9NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Datos Cotización", Font9NB))
        Col.Border = 0
        Col.BorderWidthLeft = 1
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {2.4F, 7.6F, 2.5F, 3.5F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase("Nro Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Moneda:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("MONEDA", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cliente:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)


        Col = New PdfPCell(New Phrase("F.Vencimiento:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)


        Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Vendedor:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("VENDEDOR", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Condición Pago:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Tiempo Entrega:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("cTieEnt", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaMenajeCuerpo"
        Dim TablaMenajeCuerpo As PdfPTable = New PdfPTable(1)
        Dim WidthsMenajeCuerpo As Single() = New Single() {16.0F}
        TablaMenajeCuerpo.WidthPercentage = 95
        TablaMenajeCuerpo.SetWidths(WidthsMenajeCuerpo)

        Col = New PdfPCell(New Phrase(ObtenerDato("MensajeCuerpo", General), Font8))
        Col.Border = 0
        TablaMenajeCuerpo.AddCell(Col)
        PdfDoc.Add(TablaMenajeCuerpo)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(8)
        Dim WidthsDetalleTitulo As Single() = New Single() {2.0F, 4.6F, 1.5F, 1.5F, 1.8F, 1.7F, 1.2F, 1.7F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Descripción", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Unidad", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Valor Unitario", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Valor Venta", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Desc.(%)", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)


        Col = New PdfPCell(New Phrase(IIf(MDIPrincipal.CodigoPrecioLista = 0, "Valor Venta Neto", "Sub Total Neto"), Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(8)
        Dim WidthsDetalle As Single() = New Single() {2.0F, 4.6F, 1.5F, 1.5F, 1.8F, 1.7F, 1.2F, 1.7F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)

        For i = 0 To Detalle.Rows.Count - 1
            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthLeft = 0.5
            Col.BorderColorLeft = ColorFondo
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)


            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorUnitario", Detalle, i), 6), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorVenta", Detalle, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("PorcentajeDesc", Detalle, i), 5), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotalNeto", Detalle, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)
        Next


        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)


        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)

        PdfDoc.Add(TablaDetalle)
#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(1)
        Dim WidthsMontoLetras As Single() = New Single() {16.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BackgroundColor = ColorFondo
        TablaMontoLetras.AddCell(Col)
        TablaMontoLetras.AddCell(CVacio)
        PdfDoc.Add(TablaMontoLetras)
        PdfDoc.Add(New Paragraph(" "))
        Dim Line = PdfWriter.GetVerticalPosition(True)
        If Line <= 175 Then
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
            PdfDoc.Add(New Paragraph(" "))
        End If
#End Region
#Region "TablaTotalTitulo"
        Dim TablaTotalTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsTotalTitulo As Single() = New Single() {8.0F, 3.0F, 5.0F}
        TablaTotalTitulo.WidthPercentage = 95
        TablaTotalTitulo.SetWidths(WidthsTotalTitulo)


        Col = New PdfPCell(New Phrase(ObtenerDato("Banco", General), Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTALES DEL COMPROBANTE", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaTotalTitulo.AddCell(Col)

        PdfDoc.Add(TablaTotalTitulo)
#End Region
#Region "TablaTotal"

        Dim TablaTotal As PdfPTable = New PdfPTable(6)
        Dim WidthsTotal As Single() = New Single() {1.4F, 2.6F, 4.0F, 3.0F, 3.0F, 2.0F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        Col = New PdfPCell(New Phrase("MONEDA", Font7NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase("N° CTA CORRIENTE", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase("N° CTA INTERBANCARIO", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Sub Total:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("SOLES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCIS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Descuentos:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("DOLARES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCD", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCID", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Ventas Gravadas:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total IGV 18%:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)


        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Importe Total de Venta:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        PdfDoc.Add(TablaTotal)

#End Region
#Region "TablaFinal"
        Dim TablaFinal As PdfPTable = New PdfPTable(2)
        Dim WidthsFinal As Single() = New Single() {8.0F, 8.0F}
        TablaFinal.WidthPercentage = 95
        TablaFinal.SetWidths(WidthsFinal)

        Col = New PdfPCell(New Phrase("OBSERVACIONES ADICIONALES", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaFinal.AddCell(Col)
        TablaFinal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase(ObtenerDato("cObservaciones", General), Font8))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.HorizontalAlignment = 0
        TablaFinal.AddCell(Col)
        TablaFinal.AddCell(CVacio)

        PdfDoc.Add(TablaFinal)
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "Tabla"
        Dim Tabla As PdfPTable = New PdfPTable(1)
        Dim WidthsTabla As Single() = New Single() {16.0F}
        Tabla.WidthPercentage = 95
        Tabla.SetWidths(WidthsTabla)

        Col = New PdfPCell(New Phrase(ObtenerDato("MensajeCuerpo", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        Tabla.AddCell(Col)
        PdfDoc.Add(Tabla)
        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_EECC_Modelo_1(ByVal Codigo As Integer, ByVal RazonSocial As String, ByVal Modalidad As Integer)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim DetalleSoles As New DataTable
        Dim DetalleDolares As New DataTable
        Dim TotalSoles As New DataTable
        Dim TotalDolares As New DataTable

        General = Obj.ObtenerDatosGeneralesEstadoCuentaPdf(Codigo, MDIPrincipal.CodigoEmpresa)
        DetalleSoles = Obj.ObtenerDatosDetalleEstadoCuentaPdf(Codigo, 1, MDIPrincipal.CodigoEmpresa, Modalidad)
        DetalleDolares = Obj.ObtenerDatosDetalleEstadoCuentaPdf(Codigo, 2, MDIPrincipal.CodigoEmpresa, Modalidad)
        TotalSoles = Obj.ObtenerDatosTotalesEstadoCuentaPdf(Codigo, 1, MDIPrincipal.CodigoEmpresa, Modalidad)
        TotalDolares = Obj.ObtenerDatosTotalesEstadoCuentaPdf(Codigo, 2, MDIPrincipal.CodigoEmpresa, Modalidad)

        Dim NombrePdf As String
        NombrePdf = "EE-CC-" + RazonSocial + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaEstadoCuenta As String = ConfigurationManager.AppSettings("RutaEstadoCuenta")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 20.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaEstadoCuenta + NombrePdf, FileMode.Create))
        Dim ColorFondo As New BaseColor(16, 67, 91)
        Dim ColorFondoTotales As New BaseColor(252, 243, 207)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7B As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.WHITE))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font10NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell


#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(4)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 6.5F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(18, 720)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("ESTADO DE CUENTA", Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("DEUDA PENDIENTE AL: " + ObtenerDato("FechaEmision", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Soles: " + FormatNumber(ObtenerDato("DeudaPendiente", TotalSoles), 2), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Dolares: " + FormatNumber(ObtenerDato("DeudaPendiente", TotalDolares), 2), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 415, 740, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(1)
        Dim WidthsCabeceraTitulo As Single() = New Single() {16.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase("Adquiriente / Cliente", Font9NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(2)
        Dim WidthsCabecera As Single() = New Single() {2.5F, 13.5F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase("Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Identificacion", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Nro Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cliente:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaMensaje"
        Dim TablaMensaje As PdfPTable = New PdfPTable(1)
        Dim WidthsTablaMensaje As Single() = New Single() {16.0F}
        TablaMensaje.WidthPercentage = 95
        TablaMensaje.SetWidths(WidthsTablaMensaje)

        Col = New PdfPCell(New Phrase("Estimado cliente a continuación se detalla la deuda pendiente que mantiene con nosotros, cabe resaltar que los montos se encuentran sujetos en base a la fecha de emisión de este Estado de Cuenta : Fecha Emisión: " + ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        TablaMensaje.AddCell(Col)
        PdfDoc.Add(TablaMensaje)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleSoles"
        Dim TablaDetalleSoles As PdfPTable = New PdfPTable(1)
        Dim WidthsTablaDetalleSoles As Single() = New Single() {16.0F}
        TablaDetalleSoles.WidthPercentage = 95
        TablaDetalleSoles.SetWidths(WidthsTablaDetalleSoles)

        Col = New PdfPCell(New Phrase("MONTOS EN MONEDA SOLES", Font10NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleSoles.AddCell(Col)
        PdfDoc.Add(TablaDetalleSoles)
#End Region
#Region "TablaDetalleModalidadSoles"
        Dim TablaDetalleModalidadSoles As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaDetalleModalidadSoles As Single() = New Single() {10.3F, 5.7F}
        TablaDetalleModalidadSoles.WidthPercentage = 95
        TablaDetalleModalidadSoles.SetWidths(WidthsTablaDetalleModalidadSoles)

        Col = New PdfPCell(New Phrase("INFORMACIÓN GENERAL", Font9NB))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleModalidadSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("INFORMACIÓN DE PAGO", Font9NB))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorBordeCelda
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleModalidadSoles.AddCell(Col)

        PdfDoc.Add(TablaDetalleModalidadSoles)
#End Region
#Region "TablaDetalleTituloSoles"
        Dim TablaDetalleTituloSoles As PdfPTable = New PdfPTable(9)
        Dim WidthsTablaDetalleTituloSoles As Single() = New Single() {1.6F, 1.6F, 1.8F, 2.0F, 1.3F, 2.0F, 1.9F, 1.8F, 2.0F}
        TablaDetalleTituloSoles.WidthPercentage = 95
        TablaDetalleTituloSoles.SetWidths(WidthsTablaDetalleTituloSoles)

        Col = New PdfPCell(New Phrase("Fecha Emisión", Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Fecha Vencimiento", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° Comprobante", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Total", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Retención", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Neto Pago", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Deuda Facturada", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Amortizado", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Deuda Pendiente", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        PdfDoc.Add(TablaDetalleTituloSoles)
#End Region
#Region "TablaDetalleComprobanteSoles"
        Dim TablaDetalleComprobanteSoles As PdfPTable = New PdfPTable(9)
        Dim WidthsTablaDetalleComprobanteSoles As Single() = New Single() {1.6F, 1.6F, 1.8F, 2.0F, 1.3F, 2.0F, 1.9F, 1.8F, 2.0F}
        TablaDetalleComprobanteSoles.WidthPercentage = 95
        TablaDetalleComprobanteSoles.SetWidths(WidthsTablaDetalleComprobanteSoles)
        For i = 0 To DetalleSoles.Rows.Count - 1
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaRegistro", DetalleSoles, i), Font7))
            Col.Border = 0
            Col.BorderWidthLeft = 0.5
            Col.BorderColorLeft = ColorFondo
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", DetalleSoles, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", DetalleSoles, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoTotalPago", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoDeuda", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoAmortizado", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("DeudaPendiente", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)
        Next
        PdfDoc.Add(TablaDetalleComprobanteSoles)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleDolares"
        Dim TablaDetalleDolares As PdfPTable = New PdfPTable(1)
        Dim WidthsTablaDetalleDolares As Single() = New Single() {16.0F}
        TablaDetalleDolares.WidthPercentage = 95
        TablaDetalleDolares.SetWidths(WidthsTablaDetalleDolares)

        Col = New PdfPCell(New Phrase("MONTOS EN MONEDA DOLARES", Font10NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleDolares.AddCell(Col)
        PdfDoc.Add(TablaDetalleDolares)
#End Region
#Region "TablaDetalleModalidadDolares"
        Dim TablaDetalleModalidadDolares As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaDetalleModalidadDolares As Single() = New Single() {10.3F, 5.7F}
        TablaDetalleModalidadDolares.WidthPercentage = 95
        TablaDetalleModalidadDolares.SetWidths(WidthsTablaDetalleModalidadDolares)

        Col = New PdfPCell(New Phrase("INFORMACIÓN GENERAL", Font9NB))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleModalidadDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("INFORMACIÓN DE PAGO", Font9NB))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorBordeCelda
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleModalidadDolares.AddCell(Col)

        PdfDoc.Add(TablaDetalleModalidadDolares)
#End Region
#Region "TablaDetalleTituloDolares"
        Dim TablaDetalleTituloDolares As PdfPTable = New PdfPTable(9)
        Dim WidthsTablaDetalleTituloDolares As Single() = New Single() {1.6F, 1.6F, 1.8F, 2.0F, 1.3F, 2.0F, 1.9F, 1.8F, 2.0F}
        TablaDetalleTituloDolares.WidthPercentage = 95
        TablaDetalleTituloDolares.SetWidths(WidthsTablaDetalleTituloDolares)

        Col = New PdfPCell(New Phrase("Fecha Emisión", Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Fecha Vencimiento", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° Comprobante", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Total", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Retención", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Neto Pago", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Deuda Facturada", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Amortizado", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Deuda Pendiente", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        PdfDoc.Add(TablaDetalleTituloDolares)
#End Region
#Region "TablaDetalleComprobanteDolares"
        Dim TablaDetalleComprobanteDolares As PdfPTable = New PdfPTable(9)
        Dim WidthsTablaDetalleComprobanteDolares As Single() = New Single() {1.6F, 1.6F, 1.8F, 2.0F, 1.3F, 2.0F, 1.9F, 1.8F, 2.0F}
        TablaDetalleComprobanteDolares.WidthPercentage = 95
        TablaDetalleComprobanteDolares.SetWidths(WidthsTablaDetalleComprobanteDolares)
        For i = 0 To DetalleDolares.Rows.Count - 1
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaRegistro", DetalleDolares, i), Font7))
            Col.Border = 0
            Col.BorderWidthLeft = 0.5
            Col.BorderColorLeft = ColorFondo
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", DetalleDolares, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", DetalleDolares, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoTotalPago", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoDeuda", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoAmortizado", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("DeudaPendiente", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)
        Next
        PdfDoc.Add(TablaDetalleComprobanteDolares)

        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "TablaTotalTitulo"
        Dim TablaTotalTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsTotalTitulo As Single() = New Single() {8.0F, 0.5F, 7.5F}
        TablaTotalTitulo.WidthPercentage = 95
        TablaTotalTitulo.SetWidths(WidthsTotalTitulo)

        Col = New PdfPCell(New Phrase("TOTALES EN SOLES", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTALES EN DOLARES", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)
        PdfDoc.Add(TablaTotalTitulo)
#End Region
#Region "TablaTotal"

        Dim TablaTotal As PdfPTable = New PdfPTable(5)
        Dim WidthsTotal As Single() = New Single() {4.5F, 3.5F, 0.5F, 4.0F, 3.5F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        Col = New PdfPCell(New Phrase("Importe Total:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoTotalPago", TotalSoles), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Importe Total:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoTotalPago", TotalDolares), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Retención:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", TotalSoles), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Importe Retención:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", TotalDolares), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Neto Pago:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", TotalSoles), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Importe Neto Pago:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", TotalDolares), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("Total Deuda Facturada:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoDeuda", TotalSoles), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Total Deuda Facturada:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoDeuda", TotalDolares), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)


        Col = New PdfPCell(New Phrase("Total Amortizado:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoAmortizado", TotalSoles), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Total Amortizado:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoAmortizado", TotalDolares), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("Deuda Pendiente:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("DeudaPendiente", TotalSoles), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Deuda Pendiente:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("DeudaPendiente", TotalDolares), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        PdfDoc.Add(TablaTotal)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCuentasTitulo"

        Dim TablaCuentasTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsTablaCuentasTitulo As Single() = New Single() {3.0F, 5.0F, 8.0F}
        TablaCuentasTitulo.WidthPercentage = 95
        TablaCuentasTitulo.SetWidths(WidthsTablaCuentasTitulo)

        Col = New PdfPCell(New Phrase("Banco: ", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaCuentasTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("Banco", General), Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentasTitulo.AddCell(Col)
        TablaCuentasTitulo.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Depositar a nombre de: ", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaCuentasTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentasTitulo.AddCell(Col)
        TablaCuentasTitulo.AddCell(CVacio)

        PdfDoc.Add(TablaCuentasTitulo)
#End Region
#Region "TablaCuentas"

        Dim TablaCuentas As PdfPTable = New PdfPTable(4)
        Dim WidthsTablaCuentas As Single() = New Single() {2.0F, 3.0F, 3.0F, 8.0F}
        TablaCuentas.WidthPercentage = 95
        TablaCuentas.SetWidths(WidthsTablaCuentas)

        Col = New PdfPCell(New Phrase("MONEDA", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° CTA CORRIENTE", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° CCI", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)
        TablaCuentas.AddCell(CVacio)


        Col = New PdfPCell(New Phrase("SOLES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCIS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)
        TablaCuentas.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("DOLARES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCD", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCID", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)
        TablaCuentas.AddCell(CVacio)

        PdfDoc.Add(TablaCuentas)
        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_EECC_Modelo_2(ByVal Codigo As Integer, ByVal RazonSocial As String, ByVal Modalidad As Integer)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim DetalleSoles As New DataTable
        Dim DetalleDolares As New DataTable
        Dim TotalSoles As New DataTable
        Dim TotalDolares As New DataTable

        General = Obj.ObtenerDatosGeneralesEstadoCuentaPdf(Codigo, MDIPrincipal.CodigoEmpresa)
        DetalleSoles = Obj.ObtenerDatosDetalleEstadoCuentaPdf(Codigo, 1, MDIPrincipal.CodigoEmpresa, Modalidad)
        DetalleDolares = Obj.ObtenerDatosDetalleEstadoCuentaPdf(Codigo, 2, MDIPrincipal.CodigoEmpresa, Modalidad)
        TotalSoles = Obj.ObtenerDatosTotalesEstadoCuentaPdf(Codigo, 1, MDIPrincipal.CodigoEmpresa, Modalidad)
        TotalDolares = Obj.ObtenerDatosTotalesEstadoCuentaPdf(Codigo, 2, MDIPrincipal.CodigoEmpresa, Modalidad)

        Dim NombrePdf As String
        NombrePdf = "EE-CC-" + RazonSocial + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaEstadoCuenta As String = ConfigurationManager.AppSettings("RutaEstadoCuenta")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 20.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaEstadoCuenta + NombrePdf, FileMode.Create))
        Dim ColorFondo As New BaseColor(41, 182, 246)
        Dim ColorFondoTotales As New BaseColor(252, 243, 207)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7B As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.WHITE))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font10NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(4)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 6.5F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(18, 720)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("ESTADO DE CUENTA", Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("DEUDA PENDIENTE AL: " + ObtenerDato("FechaEmision", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Soles: " + FormatNumber(ObtenerDato("DeudaPendiente", TotalSoles), 2), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Dolares: " + FormatNumber(ObtenerDato("DeudaPendiente", TotalDolares), 2), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 415, 740, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(1)
        Dim WidthsCabeceraTitulo As Single() = New Single() {16.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase("Adquiriente / Cliente", Font9NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(2)
        Dim WidthsCabecera As Single() = New Single() {2.5F, 13.5F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase("Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Identificacion", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Nro Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cliente:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.BackgroundColor = ColorBordeCelda
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaMensaje"
        Dim TablaMensaje As PdfPTable = New PdfPTable(1)
        Dim WidthsTablaMensaje As Single() = New Single() {16.0F}
        TablaMensaje.WidthPercentage = 95
        TablaMensaje.SetWidths(WidthsTablaMensaje)

        Col = New PdfPCell(New Phrase("Estimado cliente a continuación se detalla la deuda pendiente que mantiene con nosotros, cabe resaltar que los montos se encuentran sujetos en base a la fecha de emisión de este Estado de Cuenta : Fecha Emisión: " + ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        TablaMensaje.AddCell(Col)
        PdfDoc.Add(TablaMensaje)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleSoles"
        Dim TablaDetalleSoles As PdfPTable = New PdfPTable(1)
        Dim WidthsTablaDetalleSoles As Single() = New Single() {16.0F}
        TablaDetalleSoles.WidthPercentage = 95
        TablaDetalleSoles.SetWidths(WidthsTablaDetalleSoles)

        Col = New PdfPCell(New Phrase("MONTOS EN MONEDA SOLES", Font10NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleSoles.AddCell(Col)
        PdfDoc.Add(TablaDetalleSoles)
#End Region
#Region "TablaDetalleModalidadSoles"
        Dim TablaDetalleModalidadSoles As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaDetalleModalidadSoles As Single() = New Single() {10.3F, 5.7F}
        TablaDetalleModalidadSoles.WidthPercentage = 95
        TablaDetalleModalidadSoles.SetWidths(WidthsTablaDetalleModalidadSoles)

        Col = New PdfPCell(New Phrase("INFORMACIÓN GENERAL", Font9NB))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleModalidadSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("INFORMACIÓN DE PAGO", Font9NB))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorBordeCelda
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleModalidadSoles.AddCell(Col)

        PdfDoc.Add(TablaDetalleModalidadSoles)
#End Region
#Region "TablaDetalleTituloSoles"
        Dim TablaDetalleTituloSoles As PdfPTable = New PdfPTable(9)
        Dim WidthsTablaDetalleTituloSoles As Single() = New Single() {1.6F, 1.6F, 1.8F, 2.0F, 1.3F, 2.0F, 1.9F, 1.8F, 2.0F}
        TablaDetalleTituloSoles.WidthPercentage = 95
        TablaDetalleTituloSoles.SetWidths(WidthsTablaDetalleTituloSoles)

        Col = New PdfPCell(New Phrase("Fecha Emisión", Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Fecha Vencimiento", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° Comprobante", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Total", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Retención", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Neto Pago", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Deuda Facturada", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Amortizado", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        Col = New PdfPCell(New Phrase("Deuda Pendiente", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloSoles.AddCell(Col)

        PdfDoc.Add(TablaDetalleTituloSoles)
#End Region
#Region "TablaDetalleComprobanteSoles"
        Dim TablaDetalleComprobanteSoles As PdfPTable = New PdfPTable(9)
        Dim WidthsTablaDetalleComprobanteSoles As Single() = New Single() {1.6F, 1.6F, 1.8F, 2.0F, 1.3F, 2.0F, 1.9F, 1.8F, 2.0F}
        TablaDetalleComprobanteSoles.WidthPercentage = 95
        TablaDetalleComprobanteSoles.SetWidths(WidthsTablaDetalleComprobanteSoles)
        For i = 0 To DetalleSoles.Rows.Count - 1
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaRegistro", DetalleSoles, i), Font7))
            Col.Border = 0
            Col.BorderWidthLeft = 0.5
            Col.BorderColorLeft = ColorFondo
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", DetalleSoles, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", DetalleSoles, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoTotalPago", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoDeuda", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoAmortizado", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("DeudaPendiente", DetalleSoles, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteSoles.AddCell(Col)
        Next
        PdfDoc.Add(TablaDetalleComprobanteSoles)

        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleDolares"
        Dim TablaDetalleDolares As PdfPTable = New PdfPTable(1)
        Dim WidthsTablaDetalleDolares As Single() = New Single() {16.0F}
        TablaDetalleDolares.WidthPercentage = 95
        TablaDetalleDolares.SetWidths(WidthsTablaDetalleDolares)

        Col = New PdfPCell(New Phrase("MONTOS EN MONEDA DOLARES", Font10NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleDolares.AddCell(Col)
        PdfDoc.Add(TablaDetalleDolares)
#End Region
#Region "TablaDetalleModalidadDolares"
        Dim TablaDetalleModalidadDolares As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaDetalleModalidadDolares As Single() = New Single() {10.3F, 5.7F}
        TablaDetalleModalidadDolares.WidthPercentage = 95
        TablaDetalleModalidadDolares.SetWidths(WidthsTablaDetalleModalidadDolares)

        Col = New PdfPCell(New Phrase("INFORMACIÓN GENERAL", Font9NB))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleModalidadDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("INFORMACIÓN DE PAGO", Font9NB))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorBordeCelda
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleModalidadDolares.AddCell(Col)

        PdfDoc.Add(TablaDetalleModalidadDolares)
#End Region
#Region "TablaDetalleTituloDolares"
        Dim TablaDetalleTituloDolares As PdfPTable = New PdfPTable(9)
        Dim WidthsTablaDetalleTituloDolares As Single() = New Single() {1.6F, 1.6F, 1.8F, 2.0F, 1.3F, 2.0F, 1.9F, 1.8F, 2.0F}
        TablaDetalleTituloDolares.WidthPercentage = 95
        TablaDetalleTituloDolares.SetWidths(WidthsTablaDetalleTituloDolares)

        Col = New PdfPCell(New Phrase("Fecha Emisión", Font8NB))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Fecha Vencimiento", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° Comprobante", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Total", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Retención", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Neto Pago", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Deuda Facturada", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Monto Amortizado", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorBordeCelda
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        Col = New PdfPCell(New Phrase("Deuda Pendiente", Font8NB))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalleTituloDolares.AddCell(Col)

        PdfDoc.Add(TablaDetalleTituloDolares)
#End Region
#Region "TablaDetalleComprobanteDolares"
        Dim TablaDetalleComprobanteDolares As PdfPTable = New PdfPTable(9)
        Dim WidthsTablaDetalleComprobanteDolares As Single() = New Single() {1.6F, 1.6F, 1.8F, 2.0F, 1.3F, 2.0F, 1.9F, 1.8F, 2.0F}
        TablaDetalleComprobanteDolares.WidthPercentage = 95
        TablaDetalleComprobanteDolares.SetWidths(WidthsTablaDetalleComprobanteDolares)
        For i = 0 To DetalleDolares.Rows.Count - 1
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaRegistro", DetalleDolares, i), Font7))
            Col.Border = 0
            Col.BorderWidthLeft = 0.5
            Col.BorderColorLeft = ColorFondo
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", DetalleDolares, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", DetalleDolares, i), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoTotalPago", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoDeuda", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoAmortizado", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("DeudaPendiente", DetalleDolares, i), 2), Font7))
            Col.Border = 0
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalleComprobanteDolares.AddCell(Col)
        Next
        PdfDoc.Add(TablaDetalleComprobanteDolares)

        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "TablaTotalTitulo"
        Dim TablaTotalTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsTotalTitulo As Single() = New Single() {8.0F, 0.5F, 7.5F}
        TablaTotalTitulo.WidthPercentage = 95
        TablaTotalTitulo.SetWidths(WidthsTotalTitulo)

        Col = New PdfPCell(New Phrase("TOTALES EN SOLES", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)
        TablaTotalTitulo.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTALES EN DOLARES", Font9NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotalTitulo.AddCell(Col)
        PdfDoc.Add(TablaTotalTitulo)
#End Region
#Region "TablaTotal"

        Dim TablaTotal As PdfPTable = New PdfPTable(5)
        Dim WidthsTotal As Single() = New Single() {4.5F, 3.5F, 0.5F, 4.0F, 3.5F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        Col = New PdfPCell(New Phrase("Importe Total:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoTotalPago", TotalSoles), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Importe Total:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoTotalPago", TotalDolares), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Retención:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", TotalSoles), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Importe Retención:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", TotalDolares), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("Importe Neto Pago:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", TotalSoles), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Importe Neto Pago:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", TotalDolares), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("Total Deuda Facturada:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoDeuda", TotalSoles), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Total Deuda Facturada:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoDeuda", TotalDolares), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)


        Col = New PdfPCell(New Phrase("Total Amortizado:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoAmortizado", TotalSoles), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Total Amortizado:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoAmortizado", TotalDolares), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase("Deuda Pendiente:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("DeudaPendiente", TotalSoles), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Deuda Pendiente:", Font8NB))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        Col.BackgroundColor = ColorFondo
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("DeudaPendiente", TotalDolares), 2), Font8N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoTotales
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        PdfDoc.Add(TablaTotal)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCuentasTitulo"

        Dim TablaCuentasTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsTablaCuentasTitulo As Single() = New Single() {3.0F, 5.0F, 8.0F}
        TablaCuentasTitulo.WidthPercentage = 95
        TablaCuentasTitulo.SetWidths(WidthsTablaCuentasTitulo)

        Col = New PdfPCell(New Phrase("Banco: ", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaCuentasTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("Banco", General), Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentasTitulo.AddCell(Col)
        TablaCuentasTitulo.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Depositar a nombre de: ", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 2
        TablaCuentasTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentasTitulo.AddCell(Col)
        TablaCuentasTitulo.AddCell(CVacio)

        PdfDoc.Add(TablaCuentasTitulo)
#End Region
#Region "TablaCuentas"

        Dim TablaCuentas As PdfPTable = New PdfPTable(4)
        Dim WidthsTablaCuentas As Single() = New Single() {2.0F, 3.0F, 3.0F, 8.0F}
        TablaCuentas.WidthPercentage = 95
        TablaCuentas.SetWidths(WidthsTablaCuentas)

        Col = New PdfPCell(New Phrase("MONEDA", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° CTA CORRIENTE", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° CCI", Font7NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)
        TablaCuentas.AddCell(CVacio)


        Col = New PdfPCell(New Phrase("SOLES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCIS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)
        TablaCuentas.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("DOLARES", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCD", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CCID", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaCuentas.AddCell(Col)
        TablaCuentas.AddCell(CVacio)

        PdfDoc.Add(TablaCuentas)
        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_Guia_Modelo_1(ByVal Codigo As Integer)
        Dim General As New DataTable
        Dim Detalle As New DataTable

        Dim Obj As New BLFacturacion
        General = Obj.ObtenerDatosGeneralesGuiaPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetalleGuiaPdf(Codigo)

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 20.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorFondo As New BaseColor(16, 67, 91)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell
        Dim iLine As Integer
        Dim iFila As Integer

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(4)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 6.5F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(15, 720)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("TipoGuia", General) & " ELECTRÓNICA", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroGuia", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Fecha: " + ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 415, 740, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCabeceraTitulo As Single() = New Single() {8.0F, 8.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase("Datos del emisor", Font9NB))
        Col.Border = 0
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Adquiriente / Receptor", Font9NB))
        Col.Border = 0
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCabeceraTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {2.4F, 5.6F, 2.4F, 5.6F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase("RUC:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Identificacion", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Razon Social:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Nro Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Sucursal:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Sucursal", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Razon Social:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionEmisor", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalleTitulo As Single() = New Single() {0.5F, 1.5F, 8.5F, 1.5F, 2.0F, 2.0F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("N°", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Descripción", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Unidad Medida", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Serie Producto", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalle As Single() = New Single() {0.5F, 1.5F, 8.5F, 1.5F, 2.0F, 2.0F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)


        For i = 0 To Detalle.Rows.Count - 1

            Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("SerieProducto", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)
        Next

        PdfDoc.Add(TablaDetalle)
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraEnvioTitulo"
        Dim TablaCabeceraEnvioTitulo As PdfPTable = New PdfPTable(1)
        Dim WidthsCabeceraEnvioTitulo As Single() = New Single() {16.0F}
        TablaCabeceraEnvioTitulo.WidthPercentage = 95
        TablaCabeceraEnvioTitulo.SetWidths(WidthsCabeceraEnvioTitulo)

        Col = New PdfPCell(New Phrase("Datos / detalle  del envío", Font9NB))
        Col.Border = 0
        Col.BorderColorRight = ColorBordeCelda
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvioTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraEnvioTitulo)
#End Region
#Region "TablaCabeceraEnvio"
        Dim TablaCabeceraEnvio As PdfPTable = New PdfPTable(4)
        Dim WidthsCabeceraEnvio As Single() = New Single() {2.4F, 5.6F, 2.4F, 5.6F}
        TablaCabeceraEnvio.WidthPercentage = 95
        TablaCabeceraEnvio.SetWidths(WidthsCabeceraEnvio)

        Col = New PdfPCell(New Phrase("Dirección Partida:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionPartida", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección Llegada:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionLlegada", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Fecha Traslado:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("FechaTraslado", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Motivo Traslado:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("MotivoTraslado", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Tipo Documento Relacionado:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° Documento Relacionado:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Comprobante", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Orden Compra:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("cOrdComp", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabeceraEnvio.AddCell(Col)

        PdfDoc.Add(TablaCabeceraEnvio)
        PdfDoc.Add(New Paragraph(" "))

        For iFila = 1 To 40
            PdfDoc.Add(New Paragraph(" "))
            iLine = PdfWriter.GetVerticalPosition(True)
            If iLine < 90 Then
                Exit For
            End If
        Next
#End Region
#Region "Firma"
        Dim Qr As String
        Dim Helpers As New Helpers
        Qr = PrepararDatosQRGuia(ObtenerDato("CodigoQR", General))

        Dim TbFirma As PdfPTable = New PdfPTable(2)
        Dim WidthsFirma As Single() = New Single() {5.0F, 11.0F}
        TbFirma.WidthPercentage = 95
        TbFirma.SetWidths(WidthsFirma)
        Dim QrImage As iTextSharp.text.Image
        QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
        QrImage.ScaleToFit(90.0F, 120.0F)
        QrImage.SpacingBefore = 20.0F
        QrImage.SpacingAfter = 10.0F
        QrImage.SetAbsolutePosition(30, 40)
        PdfDoc.Add(QrImage)

        TbFirma.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Puede consultar este comprobante en: " & ObtenerDato("CodigoQR", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbFirma.AddCell(Col)

        TbFirma.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Código Hash: " & ObtenerDato("CodigoHas", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbFirma.AddCell(Col)

        PdfDoc.Add(TbFirma)
        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_Guia_Modelo_2(ByVal Codigo As Integer)

        Dim General As New DataTable
        Dim Detalle As New DataTable

        Dim Obj As New BLFacturacion
        General = Obj.ObtenerDatosGeneralesGuiaPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetalleGuiaPdf(Codigo)

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 20.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorFondo As New BaseColor(41, 182, 246)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7B As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.WHITE))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell
        Dim iLine As Integer
        Dim iFila As Integer

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(4)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 6.5F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(15, 720)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("TipoGuia", General) & " ELECTRÓNICA", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroGuia", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Fecha: " + ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 415, 740, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCabeceraTitulo As Single() = New Single() {8.0F, 8.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase("Datos del emisor", Font9NB))
        Col.Border = 0
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Adquiriente / Receptor", Font9NB))
        Col.Border = 0
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCabeceraTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {2.4F, 5.6F, 2.4F, 5.6F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase("RUC:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Identificacion", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Razon Social:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Nro Identificación:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Sucursal:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Sucursal", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Razon Social:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionEmisor", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalleTitulo As Single() = New Single() {0.5F, 1.5F, 8.5F, 1.5F, 2.0F, 2.0F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("N°", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Descripción", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Unidad Medida", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("Serie Producto", Font8NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBordeCelda
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalle As Single() = New Single() {0.5F, 1.5F, 8.5F, 1.5F, 2.0F, 2.0F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)


        For i = 0 To Detalle.Rows.Count - 1

            Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("SerieProducto", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)
        Next

        PdfDoc.Add(TablaDetalle)
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraEnvioTitulo"
        Dim TablaCabeceraEnvioTitulo As PdfPTable = New PdfPTable(1)
        Dim WidthsCabeceraEnvioTitulo As Single() = New Single() {16.0F}
        TablaCabeceraEnvioTitulo.WidthPercentage = 95
        TablaCabeceraEnvioTitulo.SetWidths(WidthsCabeceraEnvioTitulo)

        Col = New PdfPCell(New Phrase("Datos / detalle  del envío", Font9NB))
        Col.Border = 0
        Col.BorderColorRight = ColorBordeCelda
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvioTitulo.AddCell(Col)

        PdfDoc.Add(TablaCabeceraEnvioTitulo)
#End Region
#Region "TablaCabeceraEnvio"
        Dim TablaCabeceraEnvio As PdfPTable = New PdfPTable(4)
        Dim WidthsCabeceraEnvio As Single() = New Single() {2.4F, 5.6F, 2.4F, 5.6F}
        TablaCabeceraEnvio.WidthPercentage = 95
        TablaCabeceraEnvio.SetWidths(WidthsCabeceraEnvio)

        Col = New PdfPCell(New Phrase("Dirección Partida:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionPartida", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Dirección Llegada:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionLlegada", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Fecha Traslado:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("FechaTraslado", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Motivo Traslado:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("MotivoTraslado", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Tipo Documento Relacionado:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("N° Documento Relacionado:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Comprobante", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase("Orden Compra:", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("cOrdComp", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        Col.BorderColorRight = ColorBordeCelda
        Col.BorderWidthRight = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.BorderColorLeft = ColorBordeCelda
        Col.BorderWidthLeft = 0.5
        TablaCabeceraEnvio.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBordeCelda
        TablaCabeceraEnvio.AddCell(Col)

        PdfDoc.Add(TablaCabeceraEnvio)
        PdfDoc.Add(New Paragraph(" "))

        For iFila = 1 To 40
            PdfDoc.Add(New Paragraph(" "))
            iLine = PdfWriter.GetVerticalPosition(True)
            If iLine < 90 Then
                Exit For
            End If
        Next
#End Region
#Region "Firma"
        Dim Qr As String
        Dim Helpers As New Helpers
        Qr = PrepararDatosQRGuia(ObtenerDato("CodigoQR", General))

        Dim TbFirma As PdfPTable = New PdfPTable(2)
        Dim WidthsFirma As Single() = New Single() {5.0F, 11.0F}
        TbFirma.WidthPercentage = 95
        TbFirma.SetWidths(WidthsFirma)
        Dim QrImage As iTextSharp.text.Image
        QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
        QrImage.ScaleToFit(90.0F, 120.0F)
        QrImage.SpacingBefore = 20.0F
        QrImage.SpacingAfter = 10.0F
        QrImage.SetAbsolutePosition(30, 40)
        PdfDoc.Add(QrImage)

        TbFirma.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Puede consultar este comprobante en: " & ObtenerDato("CodigoQR", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbFirma.AddCell(Col)

        TbFirma.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Código Hash: " & ObtenerDato("CodigoHas", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbFirma.AddCell(Col)

        PdfDoc.Add(TbFirma)
        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_FERINDMELISSA(ByVal Codigo As Integer, ByVal TipoComprobante As String)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        Dim Cuotas As New DataTable
        Dim Guia As New DataTable

        General = Obj.ObtenerDatosGeneralesPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdf(Codigo)
        Cuotas = Obj.ObtenerDatosCuotasPdf(Codigo)
        Guia = Obj.ObtenerGuiasPdf(Codigo)
        Dim Guias As String = ""

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 40.0F, 10.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorCeleste As New BaseColor(33, 150, 243)
        Dim Color As New BaseColor(13, 71, 161)
        Dim ColorFondo As New BaseColor(0, 0, 0)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8C As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorCeleste))
        Dim Font8A As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, Color))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font11N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(3)
        Dim WidthsInformacion As Single() = New Single() {10.0F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)

        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(320.0F, 350.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(25, 733)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General) & " ELECTRÓNICA", Font9N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 400, 745, 570, 810, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "TablaInformacionCabecera"
        Dim TablaInformacionCabecera As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaInformacionCabecera As Single() = New Single() {8.8F, 7.2F}
        TablaInformacionCabecera.WidthPercentage = 95
        TablaInformacionCabecera.SetWidths(WidthsTablaInformacionCabecera)
        Dim RutaMarca As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca")
        Dim RutaMarcaBMP As iTextSharp.text.Image
        RutaMarcaBMP = iTextSharp.text.Image.GetInstance(RutaMarca)
        RutaMarcaBMP.ScaleToFit(239.0F, 252)
        RutaMarcaBMP.SpacingBefore = 20.0F
        RutaMarcaBMP.SpacingAfter = 10.0F
        RutaMarcaBMP.SetAbsolutePosition(331, 659)
        PdfDoc.Add(RutaMarcaBMP)

        Col = New PdfPCell(New Phrase("BROCAS HSS - MACHOS HSS - TARRAJAS -CHUCK - FRESAS HSS", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("CUCHILLAS - MICRÓMETROS - CALIBRADORES - COMPARARADORES", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("ATENDEMOS PEDIDOS A PROVINCIAS", Font8C))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionEmisor", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        PdfDoc.Add(TablaInformacionCabecera)
        Call PintarCuadrado(1, ColorFondo, 26, 690, 314, 690.3, PdfWriter, PdfDoc)

#End Region
#Region "TablaInformacionCabeceraContacto"
        Dim TablaInformacionCabeceraContacto As PdfPTable = New PdfPTable(5)
        Dim WidthsTablaInformacionCabeceraContacto As Single() = New Single() {0.3F, 3.2F, 0.3F, 5.0F, 7.5F}
        TablaInformacionCabeceraContacto.WidthPercentage = 95
        TablaInformacionCabeceraContacto.SetWidths(WidthsTablaInformacionCabeceraContacto)

        Dim RutaTelefono As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaTelefono")
        Dim RutaTelefonoBMP As iTextSharp.text.Image
        RutaTelefonoBMP = iTextSharp.text.Image.GetInstance(RutaTelefono)
        RutaTelefonoBMP.ScaleToFit(10.0F, 10.0F)
        RutaTelefonoBMP.SpacingBefore = 20.0F
        RutaTelefonoBMP.SpacingAfter = 10.0F
        RutaTelefonoBMP.SetAbsolutePosition(25, 678)
        PdfDoc.Add(RutaTelefonoBMP)

        Dim RutaWatzap As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaWatzap")
        Dim RutaWatzapBMP As iTextSharp.text.Image
        RutaWatzapBMP = iTextSharp.text.Image.GetInstance(RutaWatzap)
        RutaWatzapBMP.ScaleToFit(10.0F, 10.0F)
        RutaWatzapBMP.SpacingBefore = 20.0F
        RutaWatzapBMP.SpacingAfter = 10.0F
        RutaWatzapBMP.SetAbsolutePosition(25, 667)
        PdfDoc.Add(RutaWatzapBMP)

        Dim RutaCorreo As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaCorreo")
        Dim RutaCorreoBMP As iTextSharp.text.Image
        RutaCorreoBMP = iTextSharp.text.Image.GetInstance(RutaCorreo)
        RutaCorreoBMP.ScaleToFit(12.0F, 12.0F)
        RutaCorreoBMP.SpacingBefore = 20.0F
        RutaCorreoBMP.SpacingAfter = 10.0F
        RutaCorreoBMP.SetAbsolutePosition(140, 678)
        PdfDoc.Add(RutaCorreoBMP)

        Dim RutaCorreo2 As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaCorreo")
        Dim RutaCorreo2BMP As iTextSharp.text.Image
        RutaCorreo2BMP = iTextSharp.text.Image.GetInstance(RutaCorreo2)
        RutaCorreo2BMP.ScaleToFit(12.0F, 12.0F)
        RutaCorreo2BMP.SpacingBefore = 20.0F
        RutaCorreo2BMP.SpacingAfter = 10.0F
        RutaCorreo2BMP.SetAbsolutePosition(140, 667)
        PdfDoc.Add(RutaCorreo2BMP)


        TablaInformacionCabeceraContacto.AddCell(CVacio)
        If MDIPrincipal.CodigoAgencia = 17 Then
            Col = New PdfPCell(New Phrase("941482857", Font8))
        Else
            Col = New PdfPCell(New Phrase("981573044", Font8))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)

        TablaInformacionCabeceraContacto.AddCell(CVacio)
        If MDIPrincipal.CodigoAgencia = 17 Then
            Col = New PdfPCell(New Phrase("981458735", Font8))
        Else
            Col = New PdfPCell(New Phrase("981573044 / 941482857", Font8))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("ferindmelissa@gmail.com", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)

        PdfDoc.Add(TablaInformacionCabeceraContacto)
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {1.7F, 10.4F, 1.6F, 2.3F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase(" CLIENTE:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("RUC/DNI:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" DIRECCIÓN:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("TELEFONO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("TelefonoAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)

        Call PintarCuadrado(1, ColorFondo, 25, 569, 570, 658, PdfWriter, PdfDoc)
#End Region
#Region "TablaCabeceraDatos"
        Dim TablaCabeceraDatos As PdfPTable = New PdfPTable(6)
        Dim WidthsTablaCabeceraDatos As Single() = New Single() {2.8F, 2.7F, 2.5F, 4.1F, 1.6F, 2.3F}
        TablaCabeceraDatos.WidthPercentage = 95
        TablaCabeceraDatos.SetWidths(WidthsTablaCabeceraDatos)

        For i = 0 To Guia.Rows.Count - 1
            If i = 0 Then
                Guias = ObtenerDato("NumeroGuiaRemision", Guia, i)
            Else
                Guias = Guias + ", " + ObtenerDato("NumeroGuiaRemision", Guia, i)
            End If
        Next

        Col = New PdfPCell(New Phrase(" FECHA EMISIÓN:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("EMAIL:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("EmailAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("MONEDA:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("MONEDA", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)

        Col = New PdfPCell(New Phrase(" FECHA VENCIMIENTO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("CONDICIÓN PAGO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("OC/PEDIDO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroOrdenCompra", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)

        Col = New PdfPCell(New Phrase(" GUIA DE REMISIÓN:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(Guias, Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        TablaCabeceraDatos.AddCell(CVacio)
        TablaCabeceraDatos.AddCell(CVacio)
        TablaCabeceraDatos.AddCell(CVacio)
        TablaCabeceraDatos.AddCell(CVacio)

        PdfDoc.Add(TablaCabeceraDatos)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(7)
        Dim WidthsDetalleTitulo As Single() = New Single() {0.8F, 1.5F, 1.8F, 7.0F, 1.5F, 1.8F, 1.6F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("Item", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("U/Medida", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Descripción de Productos", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 0
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Precio Unitario", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Sub Total", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(7)
        Dim WidthsDetalle As Single() = New Single() {0.8F, 1.5F, 1.8F, 7.0F, 1.5F, 1.8F, 1.6F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)
        Dim RutaLogoAgua As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogoAgua")
        Dim RutaLogoAguaBMP As iTextSharp.text.Image
        RutaLogoAguaBMP = iTextSharp.text.Image.GetInstance(RutaLogoAgua)
        RutaLogoAguaBMP.ScaleToFit(330.0F, 340.0F)
        RutaLogoAguaBMP.SpacingBefore = 20.0F
        RutaLogoAguaBMP.SpacingAfter = 10.0F
        RutaLogoAguaBMP.SetAbsolutePosition(125, 387)
        PdfDoc.Add(RutaLogoAguaBMP)

        For i = 0 To 22
            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaDetalle.AddCell(Col)


            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Precio", Detalle, i), 6), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", Detalle, i), 2), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)
        Next
        PdfDoc.Add(TablaDetalle)
#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(1)
        Dim WidthsMontoLetras As Single() = New Single() {16.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font8))
        Col.Border = 0
        Col.BorderWidthTop = 1
        Col.BorderColorTop = ColorFondo
        TablaMontoLetras.AddCell(Col)
        TablaMontoLetras.AddCell(CVacio)
        PdfDoc.Add(TablaMontoLetras)
        PdfDoc.Add(New Paragraph(" "))
        For i = 1 To 34
            Dim Line = PdfWriter.GetVerticalPosition(True)
            If Line <= 260 Then
                Exit For
            End If
            PdfDoc.Add(New Paragraph(" "))
        Next

#End Region
#Region "TablaTotal"
        Dim TablaTotal As PdfPTable = New PdfPTable(7)
        Dim WidthsTotal As Single() = New Single() {3.0F, 3.0F, 7.0F, 0.2F, 3.0F, 0.2F, 1.8F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)
        Dim RutaYape As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaYape")
        Dim RutaYapeBMP As iTextSharp.text.Image
        RutaYapeBMP = iTextSharp.text.Image.GetInstance(RutaYape)
        RutaYapeBMP.ScaleToFit(85.0F, 85.0F)
        RutaYapeBMP.SpacingBefore = 20.0F
        RutaYapeBMP.SpacingAfter = 10.0F
        RutaYapeBMP.SetAbsolutePosition(25, 171)
        PdfDoc.Add(RutaYapeBMP)

        Dim RutaQrYape As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaQrYape")
        Dim RutaQrYapeBMP As iTextSharp.text.Image
        RutaQrYapeBMP = iTextSharp.text.Image.GetInstance(RutaQrYape)
        RutaQrYapeBMP.ScaleToFit(90.0F, 90.0F)
        RutaQrYapeBMP.SpacingBefore = 20.0F
        RutaQrYapeBMP.SpacingAfter = 10.0F
        RutaQrYapeBMP.SetAbsolutePosition(111, 169)
        PdfDoc.Add(RutaQrYapeBMP)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font9N))
        Col.Border = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Sub Total:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("BCP CORREINTE S/.:" + ObtenerDato("CCS", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Descuentos:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("BCP CORRIENTE $:" + ObtenerDato("CCD", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Ventas Gravadas:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("BCP INTERBANCARIO S/.:" + ObtenerDato("CCIS", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Total IGV 18%:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("BCP INTERBANCARIO $:" + ObtenerDato("CCID", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Importe Total de Venta:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        PdfDoc.Add(TablaTotal)

        Call PintarCuadrado(1, ColorFondo, 420, 169, 570, 258, PdfWriter, PdfDoc)
        Call PintarCuadrado(1, ColorFondo, 202, 169, 415, 258, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "CodigoHas"
        Dim TbCodigoHas As PdfPTable = New PdfPTable(2)
        Dim WidthsCodigoHas As Single() = New Single() {5.0F, 11.0F}
        TbCodigoHas.WidthPercentage = 95
        TbCodigoHas.SetWidths(WidthsCodigoHas)
        Dim RutaFace As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaFace")
        Dim RutaFaceBMP As iTextSharp.text.Image
        RutaFaceBMP = iTextSharp.text.Image.GetInstance(RutaFace)
        RutaFaceBMP.ScaleToFit(80.0F, 100.0F)
        RutaFaceBMP.SpacingBefore = 20.0F
        RutaFaceBMP.SpacingAfter = 10.0F
        RutaFaceBMP.SetAbsolutePosition(20, 106)
        PdfDoc.Add(RutaFaceBMP)

        TbCodigoHas.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Autorizado mediante SOL para Emisión de Comprobantes Electrónicos(SEE-DSC)", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbCodigoHas.AddCell(Col)

        TbCodigoHas.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Puede consultar este comprobante en:https://ww1.sunat.gob.pe/ol-ti-itconsvalicpe/ConsValiCpe.htm", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbCodigoHas.AddCell(Col)

        TbCodigoHas.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("CodigoHas", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbCodigoHas.AddCell(Col)

        PdfDoc.Add(TbCodigoHas)
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "PiePagina"
        Dim TbPiePagina As PdfPTable = New PdfPTable(4)
        Dim WidthsPiePagina As Single() = New Single() {1.2F, 1.5F, 10.3F, 3.0F}
        TbPiePagina.WidthPercentage = 95
        TbPiePagina.SetWidths(WidthsPiePagina)
        Dim RutaPiePaginaImagen As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPiePaginaImagen")
        Dim RutaPiePaginaImagenBMP As iTextSharp.text.Image
        RutaPiePaginaImagenBMP = iTextSharp.text.Image.GetInstance(RutaPiePaginaImagen)
        RutaPiePaginaImagenBMP.ScaleToFit(300.0F, 320.0F)
        RutaPiePaginaImagenBMP.SpacingBefore = 20.0F
        RutaPiePaginaImagenBMP.SpacingAfter = 10.0F
        RutaPiePaginaImagenBMP.SetAbsolutePosition(139, 16)
        PdfDoc.Add(RutaPiePaginaImagenBMP)

        Dim Qr As String
        Dim Helpers As New Helpers
        Qr = PrepararDatosQR(ObtenerDato("NumRuc", General), ObtenerDato("TipCom", General), ObtenerDato("Serie", General), ObtenerDato("TotalImpuestos", General),
                        ObtenerDato("ImporteTotal", General), ObtenerDato("FechaEmision", General), ObtenerDato("CodigoHas", General))

        Dim QrImage As iTextSharp.text.Image
        QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
        QrImage.ScaleToFit(90.0F, 120.0F)
        QrImage.SpacingBefore = 20.0F
        QrImage.SpacingAfter = 10.0F
        QrImage.SetAbsolutePosition(480, 30)
        PdfDoc.Add(QrImage)


        Col = New PdfPCell(New Phrase("Usuario: ", Font7N))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CodUsuario", General), Font7))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        TbPiePagina.AddCell(CVacio)
        TbPiePagina.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Hora: ", Font7N))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("Hora", General), Font7))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        TbPiePagina.AddCell(CVacio)
        TbPiePagina.AddCell(CVacio)

        PdfDoc.Add(TbPiePagina)

        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_GROUPTOOLS(ByVal Codigo As Integer, ByVal TipoComprobante As String)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        Dim Cuotas As New DataTable
        Dim Guia As New DataTable

        General = Obj.ObtenerDatosGeneralesPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdf(Codigo)
        Cuotas = Obj.ObtenerDatosCuotasPdf(Codigo)
        Guia = Obj.ObtenerGuiasPdf(Codigo)
        Dim Guias As String = ""

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 5.0F, 5.0F, 20.0F, 5.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorAzul As New BaseColor(3, 62, 105)
        Dim ColorBlanco = BaseColor.WHITE
        Dim ColorPlomoAcero As New BaseColor(69, 90, 100)
        Dim ColorLetra As New BaseColor(55, 71, 79)
        Dim ColorFondoDetalle As New BaseColor(224, 224, 224)

        Dim Font7_H As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.NORMAL, BaseColor.BLACK))
        Dim Font7_H_P As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.NORMAL, ColorLetra))
        Dim Font8_C As New Font(FontFactory.GetFont(FontFactory.COURIER, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8_H As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8N_C As New Font(FontFactory.GetFont(FontFactory.COURIER_BOLD, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8NN As New Font(FontFactory.GetFont(FontFactory.COURIER, 8, Font.BOLD, BaseColor.BLACK))
        Dim Font9N_C As New Font(FontFactory.GetFont(FontFactory.COURIER, 9, Font.BOLD, BaseColor.BLACK))
        Dim Font9N_C_B As New Font(FontFactory.GetFont(FontFactory.COURIER, 9, Font.BOLD, BaseColor.WHITE))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK))
        Dim Font8N_R As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD, BaseColor.RED))
        Dim Font9_H_P As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, ColorLetra))
        Dim Font9_H_B As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.WHITE))
        Dim Font10NN As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD, BaseColor.BLACK))
        Dim Font12NNB As New Font(FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, Font.BOLD, BaseColor.WHITE))
        Dim Font13NN As New Font(FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 13, Font.BOLD, BaseColor.BLACK))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

        Dim Line As Integer = 0
        Dim CantidadItems As Integer = Detalle.Rows.Count - 1
        Dim ii As Integer = 0
        Dim Contador As Integer = 0
        For ContadorPagina = 0 To 10
            If ContadorPagina > 0 Then
                If ii <= CantidadItems Then
                    PdfDoc.NewPage()
                    Contador = 0
                Else
                    Exit For
                End If
            End If
#Region "TablaInformacion"
            Dim TbInformacion As PdfPTable = New PdfPTable(3)
            Dim WidthsInformacion As Single() = New Single() {11.8F, 0.2F, 4.0F}
            TbInformacion.WidthPercentage = 95
            TbInformacion.SetWidths(WidthsInformacion)

            Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
            Dim LogoBMP As iTextSharp.text.Image
            LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
            LogoBMP.ScaleToFit(400.0F, 400.0F)
            LogoBMP.SpacingBefore = 20.0F
            LogoBMP.SpacingAfter = 10.0F
            LogoBMP.SetAbsolutePosition(20, 737)
            PdfDoc.Add(LogoBMP)

            TbInformacion.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font12NNB))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.Padding = 5
            TbInformacion.AddCell(Col)

            TbInformacion.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.Padding = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            If TipoComprobante = "99" Then
                Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General) & " ", Font10NN))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General) & " ELECTRÓNICA", Font10NN))
            End If
            Col.Border = 0
            Col.Padding = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.HorizontalAlignment = 1
            TbInformacion.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7_H))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font9N))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.HorizontalAlignment = 1
            TbInformacion.AddCell(Col)


            Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font8_H))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font9N))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)

            PdfDoc.Add(TbInformacion)

            TbInformacion = New PdfPTable(4)
            WidthsInformacion = New Single() {7.0F, 4.8F, 0.2F, 4.0F}
            TbInformacion.WidthPercentage = 95
            TbInformacion.SetWidths(WidthsInformacion)

            Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font8_H))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("ATENDEMOS ENVIOS A PROVINCIA", Font8N_R))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font13NN))
            Col.Border = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font9N))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.TOP_BORDER
            TbInformacion.AddCell(Col)

            PdfDoc.Add(TbInformacion)
#End Region
#Region "TablaCabeceraTitulo"
            Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(5)
            Dim WidthsCabeceraTitulo As Single() = New Single() {11.0F, 0.1F, 2.4F, 0.2F, 2.3F}
            TablaCabeceraTitulo.WidthPercentage = 95
            TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

            Col = New PdfPCell(New Phrase("", Font8N_C))
            Col.Border = 0
            TablaCabeceraTitulo.AddCell(Col)
            TablaCabeceraTitulo.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font8_C))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            PdfDoc.Add(TablaCabeceraTitulo)

            Col = New PdfPCell(New Phrase("DATOS DEL CLIENTE", Font9N_C_B))
            Col.Border = 0
            Col.Padding = 5
            Col.BackgroundColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            TablaCabeceraTitulo.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("Fecha Emisión", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.VerticalAlignment = 0
            TablaCabeceraTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabeceraTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaEmision", General) + " - " + ObtenerDato("Hora", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
            Dim TablaCabecera As PdfPTable = New PdfPTable(7)
            Dim WidthsCabecera As Single() = New Single() {2.3F, 0.2F, 8.5F, 0.1F, 2.4F, 0.2F, 2.3F}
            TablaCabecera.WidthPercentage = 95
            TablaCabecera.SetWidths(WidthsCabecera)

            For i = 0 To Guia.Rows.Count - 1
                If i = 0 Then
                    Guias = ObtenerDato("NumeroGuiaRemision", Guia, i)
                Else
                    Guias = Guias + ", " + ObtenerDato("NumeroGuiaRemision", Guia, i)
                End If
            Next

            Col = New PdfPCell(New Phrase("Razon Social", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase("Tip.Doc.Afectado", Font8N_C))
            Else
                Col = New PdfPCell(New Phrase("Guia", Font8N_C))
            End If
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobanteAfectado", General), Font7_H_P))
            Else
                Col = New PdfPCell(New Phrase(Guias, Font7_H_P))
            End If
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)

            Col = New PdfPCell(New Phrase("RUC", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase("N° Doc.Afectado", Font8N_C))
            Else
                Col = New PdfPCell(New Phrase("Orden Compra", Font8N_C))
            End If
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase(ObtenerDato("SerieNumeroDocAfectado", General), Font7_H_P))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("NumeroOrdenCompra", General), Font7_H_P))
            End If
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)

            Col = New PdfPCell(New Phrase("Dirección", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("Tipo Moneda", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("MONEDA", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)

            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase("Tipo Operación", Font8N_C))
            Else
                Col = New PdfPCell(New Phrase("Condición Pago", Font8N_C))
            End If
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase(ObtenerDato("CodigoNotaCredito", General), Font7_H_P))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font7_H_P))
            End If
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase("F.Doc.Afectado", Font8N_C))
            Else
                Col = New PdfPCell(New Phrase("F. Vencimiento", Font8N_C))
            End If
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase(ObtenerDato("FechaComprobanteAfecta", General), Font7_H_P))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", General), Font7_H_P))
            End If
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)

            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase("Motivo", Font8N_C))
            Else
                Col = New PdfPCell(New Phrase("Correo", Font8N_C))
            End If
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            Col.PaddingBottom = 5
            TablaCabecera.AddCell(Col)
            If TipoComprobante = "07" Or TipoComprobante = "08" Then
                Col = New PdfPCell(New Phrase(ObtenerDato("Motivo", General), Font7_H_P))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("EmailAdquiriente", General), Font7_H_P))
            End If
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)

            Col = New PdfPCell(New Phrase("Vendedor", Font8N_C))
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            Col.PaddingBottom = 5
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("VENDEDOR", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)

            PdfDoc.Add(TablaCabecera)
#End Region
#Region "TablaDetalleTitulo"
            Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(7)
            Dim WidthsDetalleTitulo As Single() = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 1.9F}
            TablaDetalleTitulo.WidthPercentage = 95
            TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

            Col = New PdfPCell(New Phrase("Item", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("Código", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("Cant.", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("U.M", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("Descripción", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 0
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("P.Unit", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 2
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("Sub Total", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 2
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)

            PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
            Dim TablaDetalle As PdfPTable = New PdfPTable(8)
            Dim WidthsDetalle As Single() = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 0.1F, 1.8F}
            TablaDetalle.WidthPercentage = 95
            TablaDetalle.SetWidths(WidthsDetalle)
            TablaDetalle.FooterRows = 20
            TablaDetalle.HeaderRows = 30
            Do While Contador < 30
                Line = PdfWriter.GetVerticalPosition(True)
                If Line > 249 Then
                    TablaDetalle = New PdfPTable(8)
                    WidthsDetalle = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 0.1F, 1.8F}
                    TablaDetalle.WidthPercentage = 95
                    TablaDetalle.SetWidths(WidthsDetalle)
                    If ii <= Detalle.Rows.Count - 1 Then
                        Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("CodUnidadMedida", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Precio", Detalle, ii), 6), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 2
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase("", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorAzul
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", Detalle, ii), 2), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 2
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)
                        PdfDoc.Add(TablaDetalle)
                        Contador = Contador + 1
                        ii = ii + 1
                    Else
                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase("", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorAzul
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 2
                        TablaDetalle.AddCell(Col)
                        PdfDoc.Add(TablaDetalle)
                        Contador = Contador + 1
                    End If
                Else
                    Contador = 30
                    Exit Do
                End If
            Loop
#End Region
#Region "TablaMontoLetras"
            Dim TablaMontoLetras As PdfPTable = New PdfPTable(2)
            Dim WidthsMontoLetras As Single() = New Single() {2.5F, 13.5F}
            TablaMontoLetras.WidthPercentage = 95
            TablaMontoLetras.SetWidths(WidthsMontoLetras)

            Col = New PdfPCell(New Phrase("", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.TOP_BORDER
            Col.BorderColor = ColorAzul
            TablaMontoLetras.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.TOP_BORDER
            Col.BorderColor = ColorAzul
            TablaMontoLetras.AddCell(Col)

            Col = New PdfPCell(New Phrase("Monto en letras", Font8N_C))
            Col.Border = 0
            Col.PaddingTop = 7
            Col.PaddingBottom = 7
            TablaMontoLetras.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingTop = 7
            Col.PaddingBottom = 7
            TablaMontoLetras.AddCell(Col)

            PdfDoc.Add(TablaMontoLetras)
#End Region
#Region "TablaTotalTitulo"
            Dim TablaTotalTitulo As PdfPTable = New PdfPTable(3)
            Dim WidthsTotalTitulo As Single() = New Single() {10.0F, 1.0F, 5.0F}
            TablaTotalTitulo.WidthPercentage = 95
            TablaTotalTitulo.SetWidths(WidthsTotalTitulo)

            Col = New PdfPCell(New Phrase("CUENTAS BANCARIAS", Font9N_C_B))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            Col.Padding = 5
            Col.BackgroundColor = ColorAzul
            TablaTotalTitulo.AddCell(Col)
            TablaTotalTitulo.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("MONTOS TOTALES", Font9N_C_B))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            Col.Padding = 5
            Col.BackgroundColor = ColorPlomoAcero
            TablaTotalTitulo.AddCell(Col)

            PdfDoc.Add(TablaTotalTitulo)

#End Region
#Region "TablaTotalTituloCuentas"
            Dim TablaTotalTituloCuentas As PdfPTable = New PdfPTable(6)
            Dim WidthsTotalTituloCuentas As Single() = New Single() {3.0F, 7.0F, 1.0F, 3.0F, 0.2F, 1.8F}
            TablaTotalTituloCuentas.WidthPercentage = 95
            TablaTotalTituloCuentas.SetWidths(WidthsTotalTituloCuentas)

            Col = New PdfPCell(New Phrase("BANCO", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("NÚMEROS DE CUENTA", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C_B))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("SUB TOTAL", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font9_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("N° C/C s/:" + ObtenerDato("CCS", General), Font9_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font8NN))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("TOTAL DESCUENTOS", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font9_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("N° CCI s/:" + ObtenerDato("CCIS", General), Font9_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("VENTAS GRAVADAS", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font9_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("N° C/C $:" + ObtenerDato("CCD", General), Font9_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("TOTAL IGV 18%", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font9_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("N° CCI $:" + ObtenerDato("CCID", General), Font9_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("IMPORTE TOTAL", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9_H_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font9_H_B))
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C_B))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9_H_B))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)


            PdfDoc.Add(TablaTotalTituloCuentas)
            PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "CodigoHas"
            Dim TbCodigoHas As PdfPTable = New PdfPTable(3)
            Dim WidthsCodigoHas As Single() = New Single() {1.5F, 9.5F, 5.0F}
            TbCodigoHas.WidthPercentage = 95
            TbCodigoHas.SetWidths(WidthsCodigoHas)


            TbCodigoHas.AddCell(CVacio)
            Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Col.Border = 0
            TbCodigoHas.AddCell(Col)
            TbCodigoHas.AddCell(CVacio)

            TbCodigoHas.AddCell(CVacio)
            Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Col.Border = 0
            TbCodigoHas.AddCell(Col)
            TbCodigoHas.AddCell(CVacio)

            TbCodigoHas.AddCell(CVacio)
            If TipoComprobante = "99" Then
                Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Else
                Col = New PdfPCell(New Phrase("Autorizado mediante SOL para Emisión de Comprobantes Electrónicos(SEE-DSC)", Font7_H_P))
            End If
            Col.Border = 0
            TbCodigoHas.AddCell(Col)
            TbCodigoHas.AddCell(CVacio)

            TbCodigoHas.AddCell(CVacio)
            If TipoComprobante = "99" Then
                Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Else
                Col = New PdfPCell(New Phrase("Puede consultar este comprobante en:https://ww1.sunat.gob.pe/ol-ti-itconsvalicpe/ConsValiCpe.htm", Font7_H_P))
            End If
            Col.Border = 0
            TbCodigoHas.AddCell(Col)
            TbCodigoHas.AddCell(CVacio)

            TbCodigoHas.AddCell(CVacio)
            If TipoComprobante = "99" Then
                Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("CodigoHas", General), Font7_H_P))
            End If
            Col.Border = 0
            TbCodigoHas.AddCell(Col)
            TbCodigoHas.AddCell(CVacio)

            PdfDoc.Add(TbCodigoHas)
#End Region
#Region "PiePagina"
            Dim MarcaBMP As iTextSharp.text.Image

            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaBancoBcp"))
            MarcaBMP.ScaleToFit(70.0F, 70.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(35, 117)
            PdfDoc.Add(MarcaBMP)
            If TipoComprobante <> "99" Then
                Dim Qr As String
                Dim Helpers As New Helpers
                Qr = PrepararDatosQR(ObtenerDato("NumRucSolo", General), ObtenerDato("TipCom", General), ObtenerDato("Serie", General), ObtenerDato("TotalImpuestos", General),
                                ObtenerDato("ImporteTotal", General), ObtenerDato("FechaEmision", General), ObtenerDato("CodigoHas", General))

                Dim QrImage As iTextSharp.text.Image
                QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
                QrImage.ScaleToFit(45.0F, 45.0F)
                QrImage.SpacingBefore = 20.0F
                QrImage.SpacingAfter = 10.0F
                QrImage.SetAbsolutePosition(20, 50)
                PdfDoc.Add(QrImage)
            End If
            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaLogoAgua"))
            MarcaBMP.ScaleToFit(570, 570)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(10, 360)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca1"))
            MarcaBMP.ScaleToFit(50.0F, 50.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(17, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca2"))
            MarcaBMP.ScaleToFit(90.0F, 90.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(75, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca3"))
            MarcaBMP.ScaleToFit(50.0F, 50.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(175, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca4"))
            MarcaBMP.ScaleToFit(100.0F, 100.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(230, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca5"))
            MarcaBMP.ScaleToFit(70.0F, 70.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(343, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca6"))
            MarcaBMP.ScaleToFit(80.0F, 80.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(417, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = iTextSharp.text.Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca7"))
            MarcaBMP.ScaleToFit(80.0F, 80.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(500, 8)
            PdfDoc.Add(MarcaBMP)
#End Region
        Next
        PdfDoc.Close()
    End Sub
    Sub CrearPdf_TA4_MAQHER(ByVal Codigo As Integer, ByVal TipoComprobante As String)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        Dim Cuotas As New DataTable
        Dim Guia As New DataTable
        General = Obj.ObtenerDatosGeneralesPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdf(Codigo)
        Guia = Obj.ObtenerGuiasPdf(Codigo)
        Cuotas = Obj.ObtenerDatosCuotasPdf(Codigo)
        Dim CodigoTipoOperacionNota As String = ObtenerDato("CodigoTipoNotaCredito", General)
        If CodigoTipoOperacionNota = "13" And (TipoComprobante = "07" Or TipoComprobante = "08") Then
            Cuotas = Obj.ObtenerDatosCuotasPdf(ObtenerDato("CodigoDocumentoAfecta", General))
        End If
        Dim Guias As String = ""
        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 10.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorCeleste As New BaseColor(33, 150, 243)
        Dim Color As New BaseColor(13, 71, 161)
        Dim ColorFondo As New BaseColor(0, 0, 0)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font6 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NR As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.RED))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8C As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorCeleste))
        Dim Font8A As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, Color))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font11N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(5)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 0.5F, 7.0F, 0.5F, 4.5F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(160.0F, 160.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(20, 700)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Sucursal: " + ObtenerDato("DireccionEmisor", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Telf: " + ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("ELECTRÓNICA", Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("www.mqaperu.com.pe", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("N° " + ObtenerDato("NumeroComprobante", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(0.5, ColorFondo, 425, 715, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(3)
        Dim WidthsCabecera As Single() = New Single() {2.5F, 0.2F, 13.3F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase(" Cliente:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" Dirección:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" RUC:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)
        PdfDoc.Add(New Paragraph(" "))

        Call PintarCuadrado(0.5, ColorFondo, 25, 668, 570, 710, PdfWriter, PdfDoc)
#End Region
#Region "TablaCabeceraDatos"
        Dim TablaCabeceraDatos As PdfPTable = New PdfPTable(5)
        Dim WidthsTablaCabeceraDatos As Single() = New Single() {3.0F, 2.2F, 3.0F, 4.4F, 3.4F}
        TablaCabeceraDatos.WidthPercentage = 95
        TablaCabeceraDatos.SetWidths(WidthsTablaCabeceraDatos)

        For i = 0 To Guia.Rows.Count - 1
            If i = 0 Then
                Guias = ObtenerDato("NumeroGuiaRemision", Guia, i)
            Else
                Guias = Guias + ", " + ObtenerDato("NumeroGuiaRemision", Guia, i)
            End If
        Next
        Col = New PdfPCell(New Phrase("FECHA EMISION", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("TIPO OPERACIÓN", Font7N))
        Else
            Col = New PdfPCell(New Phrase("FEC. VENCIMIENTO", Font7N))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("N° COMP.MODIFICA", Font7N))
        Else
            Col = New PdfPCell(New Phrase("ORD.COMPRA/PEDIDO", Font7N))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("TIPO OPERACIÓN", Font7N))
        Else
            Col = New PdfPCell(New Phrase("GUIA REMISIÓN", Font7N))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("FEC.COMP.MODIFICA", Font7N))
        Else
            Col = New PdfPCell(New Phrase("COND.PAGO", Font7N))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("FechaEmision", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobanteAfectado", General), Font7))
        Else
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", General), Font7))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("SerieNumeroDocAfectado", General), Font7))
        Else
            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroOrdenCompra", General), Font7))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoNotaCredito", General), Font6))
        Else
            Col = New PdfPCell(New Phrase(Guias, Font6))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaComprobanteAfecta", General), Font7))
        Else
            Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font7))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)

        PdfDoc.Add(TablaCabeceraDatos)

        Call PintarCuadrado(0.5, ColorFondo, 25, 629, 570, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 202, 629, 202, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 304, 629, 304, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 25, 620, 570, 629, PdfWriter, PdfDoc)
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(5)
        Dim WidthsDetalleTitulo As Single() = New Single() {1.5F, 1.5F, 9.6F, 1.8F, 1.6F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("CANTIDAD", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("U.M", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("DESCRIPCIÓN", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("VALOR UNIT.", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("VALOR VENTA", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
        Call PintarCuadrado(0.5, ColorFondo, 25, 290, 570, 629, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 75, 290, 75, 629, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 126, 290, 126, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 454, 290, 454, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 516, 290, 516, 629, PdfWriter, PdfDoc)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(5)
        Dim WidthsDetalle As Single() = New Single() {1.5F, 1.5F, 9.6F, 1.8F, 1.6F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)

        For i = 0 To 29
            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font6))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorUnitario", Detalle, i), 6), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorVenta", Detalle, i), 2), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)
        Next
        PdfDoc.Add(TablaDetalle)
#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(1)
        Dim WidthsMontoLetras As Single() = New Single() {16.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font7N))
        Col.Border = 0
        TablaMontoLetras.AddCell(Col)
        PdfDoc.Add(TablaMontoLetras)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaTotal"
        Dim TablaTotal As PdfPTable = New PdfPTable(7)
        Dim WidthsTotal As Single() = New Single() {2.5F, 2.5F, 2.7F, 3.0F, 3.0F, 0.5F, 1.8F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        Dim ContadorCuota As Integer = 0
        ContadorCuota = Cuotas.Rows.Count

        If ContadorCuota > 0 Then
            Col = New PdfPCell(New Phrase("Importe neto pago:", Font8N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            If CodigoTipoOperacionNota = "13" And (TipoComprobante = "07" Or TipoComprobante = "08") Then
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteNetoPagoAfectado", General), 2), Font8))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", General), 2), Font8))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
        Else
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
        End If
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("SUB TOTAL", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        If ContadorCuota > 0 Then
            Col = New PdfPCell(New Phrase("N° Cuota", Font8N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            Col = New PdfPCell(New Phrase("Monto Cuota", Font8N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            Col = New PdfPCell(New Phrase("Fecha Vencimiento", Font8N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
        Else
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
        End If
        TablaTotal.AddCell(CVacio)
        If CDbl(ObtenerDato("TotalAnticipos", General)) > 0 Then
            Col = New PdfPCell(New Phrase("TOTAL ANTICIPOS", Font7N))
        Else
            Col = New PdfPCell(New Phrase("TOTAL DESCUENTO", Font7N))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        If CDbl(ObtenerDato("TotalAnticipos", General)) > 0 Then
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalAnticipos", General), 2), Font8))
        Else
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font8))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        If ContadorCuota > 0 Then
            Col = New PdfPCell(New Phrase(ObtenerDato("cCodCuo", Cuotas, 0), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nMonCuo", Cuotas, 0), 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("dFecVen", Cuotas, 0), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
        Else
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
        End If
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTAL VENTAS GRAVADAS", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        If ContadorCuota > 1 Then
            Col = New PdfPCell(New Phrase(ObtenerDato("cCodCuo", Cuotas, 1), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nMonCuo", Cuotas, 1), 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("dFecVen", Cuotas, 1), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
        Else
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
        End If
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTAL IGV 18%", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        If ContadorCuota > 2 Then
            Col = New PdfPCell(New Phrase(ObtenerDato("cCodCuo", Cuotas, 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nMonCuo", Cuotas, 2), 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("dFecVen", Cuotas, 2), Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaTotal.AddCell(Col)
        Else
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
            TablaTotal.AddCell(CVacio)
        End If
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("IMPORTE TOTAL VENTA", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)

        PdfDoc.Add(TablaTotal)
        If ContadorCuota > 0 Then
            Call PintarCuadrado(0.5, ColorFondo, 25, 200, 290, 263, PdfWriter, PdfDoc)
            Call PintarCuadrado(0.5, ColorFondo, 25, 212, 290, 250, PdfWriter, PdfDoc)
            Call PintarCuadrado(0.5, ColorFondo, 25, 235, 290, 224, PdfWriter, PdfDoc)
            Call PintarCuadrado(0.5, ColorFondo, 104, 200, 190, 250, PdfWriter, PdfDoc)
        End If
        Call PintarCuadrado(0.5, ColorFondo, 390, 200, 570, 263, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 390, 212, 570, 250, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 390, 235, 570, 224, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 514, 200, 514, 263, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "CodigoTitulo"
        Dim TbCodigoTitulo As PdfPTable = New PdfPTable(5)
        Dim WidthsCodigoTitulo As Single() = New Single() {2.5F, 5.2F, 0.3F, 6.2F, 1.8F}
        TbCodigoTitulo.WidthPercentage = 95
        TbCodigoTitulo.SetWidths(WidthsCodigoTitulo)
        Dim MontoRetencion As Double = ObtenerDato("MontoRetencion", General)
        Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)

        Col = New PdfPCell(New Phrase("Depositar a nombre de: ", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoTitulo.AddCell(Col)
        TbCodigoTitulo.AddCell(CVacio)
        If TipoComprobante = "07" Or TipoComprobante = "08" Then
            Col = New PdfPCell(New Phrase("Motivo: " + ObtenerDato("Motivo", General), Font7N))
            Col.Border = 0
            TbCodigoTitulo.AddCell(Col)
        Else
            If MontoRetencion > 0 Then
                Dim MensajeRetencionDetraccion As String = ""
                If AplicaDetraccion = True Then
                    MensajeRetencionDetraccion = "Detracción"
                Else
                    MensajeRetencionDetraccion = "Retención"
                End If
                Col = New PdfPCell(New Phrase("Información de " + MensajeRetencionDetraccion, Font8N))
                Col.Border = 0
                Col.HorizontalAlignment = 1
                TbCodigoTitulo.AddCell(Col)
            Else
                TbCodigoTitulo.AddCell(CVacio)
            End If
        End If
        TbCodigoTitulo.AddCell(CVacio)
        PdfDoc.Add(TbCodigoTitulo)

#End Region
#Region "CodigoCuenta"
        Dim TbCodigoCuenta As PdfPTable = New PdfPTable(9)
        Dim WidthsCodigoCuenta As Single() = New Single() {1.0F, 1.2F, 2.5F, 3.0F, 0.3F, 2.2F, 2.0, 2.0F, 1.8F}
        TbCodigoCuenta.WidthPercentage = 95
        TbCodigoCuenta.SetWidths(WidthsCodigoCuenta)

        Col = New PdfPCell(New Phrase("BANCO", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("MONEDA", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("N.CTA CORRIENTE", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("CCI", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)

        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase("Base imponible", Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase("Porcentaje de ", Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase("Monto de ", Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        TbCodigoCuenta.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("S/.", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCS", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCIS", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)

        If MontoRetencion > 0 Then
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("la " + MensajeRetencionDetraccion, Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("la " + MensajeRetencionDetraccion, Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("la " + MensajeRetencionDetraccion, Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        TbCodigoCuenta.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("BCP", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("USD.", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCD", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCID", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)

        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase(FormatNumber(CDbl(IIf(AplicaDetraccion = True, MDIPrincipal.PorcentajeDetraccion, MDIPrincipal.PorcentajeRetencion)) * 100, 2) + "%", Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", General), 2), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        TbCodigoCuenta.AddCell(CVacio)
        PdfDoc.Add(TbCodigoCuenta)

        If MontoRetencion > 0 Then
            Call PintarCuadrado(0.5, ColorFondo, 297, 136, 510, 185, PdfWriter, PdfDoc)
            Call PintarCuadrado(0.5, ColorFondo, 297, 148, 510, 170, PdfWriter, PdfDoc)
            Call PintarCuadrado(0.5, ColorFondo, 372, 136, 441, 170, PdfWriter, PdfDoc)
        End If
        Call PintarCuadrado(0.5, ColorFondo, 25, 136, 290, 185, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 25, 160, 290, 170, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 58, 136, 290, 146, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 58, 136, 100, 170, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 178, 136, 178, 170, PdfWriter, PdfDoc)
#End Region
#Region "InfoDetraccion"
        Dim TbInfoDetraccion As PdfPTable = New PdfPTable(2)
        Dim WidthsInfoDetraccion As Single() = New Single() {9.0F, 7.0F}
        TbInfoDetraccion.WidthPercentage = 95
        TbInfoDetraccion.SetWidths(WidthsInfoDetraccion)

        If MontoRetencion > 0 And AplicaDetraccion = True Then
            Col = New PdfPCell(New Phrase("OPERACIÓN SUJETA AL SISTEMA DE PAGOS DE OBLIGACIONES TRIBUTARIAS", Font7NR))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInfoDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase("Cuenta de Detracción:00-006-091504", Font8N))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TbInfoDetraccion.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInfoDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TbInfoDetraccion.AddCell(Col)
        Else
            Col = New PdfPCell(New Phrase(" ", Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInfoDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TbInfoDetraccion.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInfoDetraccion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TbInfoDetraccion.AddCell(Col)
        End If
        PdfDoc.Add(TbInfoDetraccion)
#End Region
#Region "CodigoHas"
        Dim TbCodigoHas As PdfPTable = New PdfPTable(2)
        Dim WidthsCodigoHas As Single() = New Single() {12.0F, 4.0F}
        TbCodigoHas.WidthPercentage = 95
        TbCodigoHas.SetWidths(WidthsCodigoHas)

        TbCodigoHas.AddCell(CVacio)
        TbCodigoHas.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Autorizado mediante SOL para Emisión de Comprobantes Electrónicos(SEE-DSC)", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoHas.AddCell(Col)
        Col = New PdfPCell(New Phrase("Vendedor: " + ObtenerDato("VENDEDOR", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbCodigoHas.AddCell(Col)


        Col = New PdfPCell(New Phrase("Puede consultar este comprobante en:https://ww1.sunat.gob.pe/ol-ti-itconsvalicpe/ConsValiCpe.htm", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoHas.AddCell(Col)
        TbCodigoHas.AddCell(CVacio)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoHas.AddCell(Col)
        TbCodigoHas.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("UNA VEZ SALIDA LA MERCADERIA NO HAY LUGAR A RECLAMO NI DEVOLUCIÓN ", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbCodigoHas.AddCell(Col)
        TbCodigoHas.AddCell(CVacio)
        PdfDoc.Add(TbCodigoHas)
        Call PintarCuadrado(0.5, ColorFondo, 25, 80, 570, 80, PdfWriter, PdfDoc)
#End Region
#Region "PiePagina"
        Dim TbPiePagina As PdfPTable = New PdfPTable(4)
        Dim WidthsPiePagina As Single() = New Single() {1.2F, 1.5F, 10.3F, 3.0F}
        TbPiePagina.WidthPercentage = 95
        TbPiePagina.SetWidths(WidthsPiePagina)
        Dim RutaPiePaginaImagen As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPiePaginaImagen")
        Dim RutaPiePaginaImagenBMP As iTextSharp.text.Image
        RutaPiePaginaImagenBMP = iTextSharp.text.Image.GetInstance(RutaPiePaginaImagen)
        RutaPiePaginaImagenBMP.ScaleToFit(500.0F, 520.0F)
        RutaPiePaginaImagenBMP.SpacingBefore = 20.0F
        RutaPiePaginaImagenBMP.SpacingAfter = 10.0F
        RutaPiePaginaImagenBMP.SetAbsolutePosition(50, 14)
        PdfDoc.Add(RutaPiePaginaImagenBMP)

        Dim Qr As String
        Dim Helpers As New Helpers
        Qr = PrepararDatosQR(ObtenerDato("NumRuc", General), ObtenerDato("TipCom", General), ObtenerDato("Serie", General), ObtenerDato("TotalImpuestos", General),
                        ObtenerDato("ImporteTotal", General), ObtenerDato("FechaEmision", General), ObtenerDato("CodigoHas", General))

        Dim QrImage As iTextSharp.text.Image
        QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
        QrImage.ScaleToFit(50.0F, 50.0F)
        QrImage.SpacingBefore = 20.0F
        QrImage.SpacingAfter = 10.0F
        QrImage.SetAbsolutePosition(518, 136)
        PdfDoc.Add(QrImage)

        PdfDoc.Add(TbPiePagina)

        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_GUIA_GROUPTOOLS(ByVal Codigo As Integer)
        Dim General As New DataTable
        Dim Detalle As New DataTable

        Dim Obj As New BLFacturacion
        General = Obj.ObtenerDatosGeneralesGuiaPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetalleGuiaPdf(Codigo)

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 5.0F, 5.0F, 20.0F, 5.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim Color As New BaseColor(0, 102, 255)
        Dim ColorFondo As New BaseColor(176, 190, 197)

        Dim ColorAzul As New BaseColor(3, 62, 105)
        Dim ColorBlanco = BaseColor.WHITE
        Dim ColorPlomoAcero As New BaseColor(69, 90, 100)
        Dim ColorLetra As New BaseColor(55, 71, 79)
        Dim ColorFondoDetalle As New BaseColor(224, 224, 224)

        Dim Font7_H As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.NORMAL, BaseColor.BLACK))
        Dim Font7_H_P As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.NORMAL, ColorLetra))
        Dim Font8_C As New Font(FontFactory.GetFont(FontFactory.COURIER, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8_H As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8N_C As New Font(FontFactory.GetFont(FontFactory.COURIER_BOLD, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8NN As New Font(FontFactory.GetFont(FontFactory.COURIER, 8, Font.BOLD, BaseColor.BLACK))
        Dim Font9N_C As New Font(FontFactory.GetFont(FontFactory.COURIER, 9, Font.BOLD, BaseColor.BLACK))
        Dim Font9N_C_B As New Font(FontFactory.GetFont(FontFactory.COURIER, 9, Font.BOLD, BaseColor.WHITE))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK))
        Dim Font8N_R As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD, BaseColor.RED))
        Dim Font9_H_P As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, ColorLetra))
        Dim Font9_H_B As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.WHITE))
        Dim Font10NN As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD, BaseColor.BLACK))
        Dim Font12NNB As New Font(FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, Font.BOLD, BaseColor.WHITE))
        Dim Font13NN As New Font(FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 13, Font.BOLD, BaseColor.BLACK))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

        Dim Line As Integer = 0
        Dim CantidadItems As Integer = Detalle.Rows.Count - 1
        Dim ii As Integer = 0
        Dim Contador As Integer = 0
        For ContadorPagina = 0 To 10
            If ContadorPagina > 0 Then
                If ii <= CantidadItems Then
                    PdfDoc.NewPage()
                    Contador = 0
                Else
                    Exit For
                End If
            End If
#Region "TablaInformacion"
            Dim TbInformacion As PdfPTable = New PdfPTable(3)
            Dim WidthsInformacion As Single() = New Single() {11.8F, 0.2F, 4.0F}
            TbInformacion.WidthPercentage = 95
            TbInformacion.SetWidths(WidthsInformacion)

            Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
            Dim LogoBMP As iTextSharp.text.Image
            LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
            LogoBMP.ScaleToFit(400, 400)
            LogoBMP.SpacingBefore = 20.0F
            LogoBMP.SpacingAfter = 10.0F
            LogoBMP.SetAbsolutePosition(20, 737)
            PdfDoc.Add(LogoBMP)

            TbInformacion.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font12NNB))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.Padding = 5
            TbInformacion.AddCell(Col)

            TbInformacion.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.Padding = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("TipoGuia", General) & " ELECTRÓNICA", Font10NN))
            Col.Border = 0
            Col.Padding = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.HorizontalAlignment = 1
            TbInformacion.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7_H))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroGuia", General), Font9N))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.HorizontalAlignment = 1
            TbInformacion.AddCell(Col)


            Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font8_H))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font9N))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)

            PdfDoc.Add(TbInformacion)

            TbInformacion = New PdfPTable(4)
            WidthsInformacion = New Single() {7.0F, 4.8F, 0.2F, 4.0F}
            TbInformacion.WidthPercentage = 95
            TbInformacion.SetWidths(WidthsInformacion)

            Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font8_H))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("ATENDEMOS ENVIOS A PROVINCIA", Font8N_R))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font13NN))
            Col.Border = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font9N))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.TOP_BORDER
            TbInformacion.AddCell(Col)

            PdfDoc.Add(TbInformacion)
#End Region
#Region "TablaCabeceraTitulo"
            Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(1)
            Dim WidthsCabeceraTitulo As Single() = New Single() {16.6F}
            TablaCabeceraTitulo.WidthPercentage = 95
            TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

            Col = New PdfPCell(New Phrase("DATOS DEL INICIO DE TRASLADO", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.Padding = 5
            TablaCabeceraTitulo.AddCell(Col)
            PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
            Dim TablaCabecera As PdfPTable = New PdfPTable(4)
            Dim WidthsCabecera As Single() = New Single() {4.0F, 4.0F, 4.0F, 4.0F}
            TablaCabecera.WidthPercentage = 95
            TablaCabecera.SetWidths(WidthsCabecera)

            Col = New PdfPCell(New Phrase("Fecha Emisión", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.HorizontalAlignment = 1
            Col.BorderColor = ColorPlomoAcero
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase("Fecha Traslado", Font8N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase("Motivo de Traslado", Font8N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase("Modalidad de Transporte", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaCabecera.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("FechaEmision", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.HorizontalAlignment = 1
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 5
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaTraslado", General), Font7_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("MotivoTraslado", General), Font7_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("cModalidadTraslado", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaCabecera.AddCell(Col)

            PdfDoc.Add(TablaCabecera)

#End Region
#Region "TablaCabeceraTituloDestino"
            Dim TablaCabeceraTituloDestino As PdfPTable = New PdfPTable(1)
            Dim WidthsCabeceraTituloDestino As Single() = New Single() {16.6F}
            TablaCabeceraTituloDestino.WidthPercentage = 95
            TablaCabeceraTituloDestino.SetWidths(WidthsCabeceraTituloDestino)

            Col = New PdfPCell(New Phrase("DATOS DEL DESTINATARIO", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.Padding = 5
            TablaCabeceraTituloDestino.AddCell(Col)

            PdfDoc.Add(TablaCabeceraTituloDestino)
#End Region
#Region "TablaCabeceraDestino"
            Dim TablaCabeceraDestino As PdfPTable = New PdfPTable(2)
            Dim WidthsCabeceraDestino As Single() = New Single() {8.0F, 8.0F}
            TablaCabeceraDestino.WidthPercentage = 95
            TablaCabeceraDestino.SetWidths(WidthsCabeceraDestino)

            Col = New PdfPCell(New Phrase("Razón Social del Destinatario", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraDestino.AddCell(Col)
            Col = New PdfPCell(New Phrase("Documento de identidad", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraDestino.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 8
            TablaCabeceraDestino.AddCell(Col)
            Col = New PdfPCell(New Phrase("RUC - " + ObtenerDato("DocumentoAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 8
            TablaCabeceraDestino.AddCell(Col)

            Col = New PdfPCell(New Phrase("Dirección de Partida", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraDestino.AddCell(Col)
            Col = New PdfPCell(New Phrase("Dirección de Llegada", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraDestino.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("DireccionPartida", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 8
            TablaCabeceraDestino.AddCell(Col)
            Col = New PdfPCell(New Phrase("RUC - " + ObtenerDato("DireccionAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 8
            TablaCabeceraDestino.AddCell(Col)
            PdfDoc.Add(TablaCabeceraDestino)

#End Region
#Region "TablaCabeceraTituloTrasnportista"
            Dim TablaCabeceraTituloTrasnportista As PdfPTable = New PdfPTable(1)
            Dim WidthsCabeceraTituloTrasnportista As Single() = New Single() {16.6F}
            TablaCabeceraTituloTrasnportista.WidthPercentage = 95
            TablaCabeceraTituloTrasnportista.SetWidths(WidthsCabeceraTituloTrasnportista)

            Col = New PdfPCell(New Phrase("DATOS DEL TRANSPORTISTA", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.Padding = 5
            TablaCabeceraTituloTrasnportista.AddCell(Col)
            PdfDoc.Add(TablaCabeceraTituloTrasnportista)
#End Region
#Region "TablaCabeceraTrasnportista"
            Dim TablaCabeceraTrasnportista As PdfPTable = New PdfPTable(4)
            Dim WidthsCabeceraTrasnportista As Single() = New Single() {8.0F, 3.0F, 2.5F, 2.5F}
            TablaCabeceraTrasnportista.WidthPercentage = 95
            TablaCabeceraTrasnportista.SetWidths(WidthsCabeceraTrasnportista)

            Col = New PdfPCell(New Phrase("Razón Social del Transportista", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase("Documento de Identidad", Font8N_C))
            Col.Border = 0
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase("Placa de Vehículo", Font8N_C))
            Col.Border = 0
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase("Marca", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTrasnportista.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialTransportista", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 8
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquirienteTransportista", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingBottom = 8
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("Placa", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingBottom = 8
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("Marca", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 8
            TablaCabeceraTrasnportista.AddCell(Col)

            Col = New PdfPCell(New Phrase("Nombre del Conductor", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase("Documento de Identidad", Font8N_C))
            Col.Border = 0
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase("Licencia", Font8N_C))
            Col.Border = 0
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTrasnportista.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Conductor", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 8
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquirienteConductor", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingBottom = 8
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("LicenciaConductor", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingBottom = 8
            TablaCabeceraTrasnportista.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8NN))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.PaddingBottom = 8
            TablaCabeceraTrasnportista.AddCell(Col)

            PdfDoc.Add(TablaCabeceraTrasnportista)

#End Region
#Region "TablaDetalleTitulo"
            Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(7)
            Dim WidthsDetalleTitulo As Single() = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 1.9F}
            TablaDetalleTitulo.WidthPercentage = 95
            TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

            Col = New PdfPCell(New Phrase("ITEM", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("CÓDIGO", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("CANT.", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("U.M", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("DESCRIPCIÓN", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 0
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("PESO UNIT.", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 2
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("P.TOTAL", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 2
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)

            PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
            Dim TablaDetalle As PdfPTable = New PdfPTable(8)
            Dim WidthsDetalle As Single() = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 0.1F, 1.8F}
            TablaDetalle.WidthPercentage = 95
            TablaDetalle.SetWidths(WidthsDetalle)
            TablaDetalle.FooterRows = 20
            TablaDetalle.HeaderRows = 30
            Do While Contador < 30
                Line = PdfWriter.GetVerticalPosition(True)
                If Line > 135 Then
                    TablaDetalle = New PdfPTable(8)
                    WidthsDetalle = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 0.1F, 1.8F}
                    TablaDetalle.WidthPercentage = 95
                    TablaDetalle.SetWidths(WidthsDetalle)
                    If ii <= Detalle.Rows.Count - 1 Then
                        Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("CodUnidadMedida", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("PesoUnitario", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.HorizontalAlignment = 2
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase("", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("PesoTotal", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.HorizontalAlignment = 2
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)
                        PdfDoc.Add(TablaDetalle)
                        Contador = Contador + 1
                        ii = ii + 1
                    Else
                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase("", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorPlomoAcero
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 2
                        TablaDetalle.AddCell(Col)
                        PdfDoc.Add(TablaDetalle)
                        Contador = Contador + 1
                    End If
                Else
                    Contador = 30
                    Exit Do
                End If
            Loop
#End Region
#Region "TablaReferencia"
            Dim TablaReferencia As PdfPTable = New PdfPTable(3)
            Dim WidthsReferencia As Single() = New Single() {7.8F, 0.2F, 8.0F}
            TablaReferencia.WidthPercentage = 95
            TablaReferencia.SetWidths(WidthsReferencia)

            Col = New PdfPCell(New Phrase("DOCUMENTOS DE REFERENCIA", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)
            Col = New PdfPCell(New Phrase("OBSERVACIONES", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Comprobante", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("Observaciones", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)

            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.Padding = 5
            TablaReferencia.AddCell(Col)

            PdfDoc.Add(TablaReferencia)
#End Region
#Region "CodigoHas"
            Dim MarcaBMP As iTextSharp.text.Image

            Dim TbCodigoHas As PdfPTable = New PdfPTable(3)
            Dim WidthsCodigoHas As Single() = New Single() {2.5F, 5.5F, 8.0F}
            TbCodigoHas.WidthPercentage = 95
            TbCodigoHas.SetWidths(WidthsCodigoHas)

            Dim Qr As String
            Dim Helpers As New Helpers
            Qr = PrepararDatosQRGuia(ObtenerDato("CodigoQR", General))

            Dim QrImage As Image
            QrImage = Image.GetInstance(Helpers.CreateQR(Qr))
            QrImage.ScaleToFit(65.0F, 65.0F)
            QrImage.SpacingBefore = 20.0F
            QrImage.SpacingAfter = 10.0F
            QrImage.SetAbsolutePosition(23, 17)
            PdfDoc.Add(QrImage)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaLogoAgua"))
            MarcaBMP.ScaleToFit(570, 570)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(10, 260)
            PdfDoc.Add(MarcaBMP)

            Col = New PdfPCell(New Phrase("", Font8NN))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.PaddingTop = 17
            Col.BorderColor = ColorPlomoAcero
            TbCodigoHas.AddCell(Col)
            Col = New PdfPCell(New Phrase("Puede consultar este comprobante en: " & ObtenerDato("CodigoQR", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingTop = 17
            TbCodigoHas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.PaddingTop = 17
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TbCodigoHas.AddCell(Col)

            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.PaddingBottom = 17
            Col.BorderColor = ColorPlomoAcero
            TbCodigoHas.AddCell(Col)
            Col = New PdfPCell(New Phrase("Código Hash: " & ObtenerDato("CodigoHas", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingBottom = 17
            TbCodigoHas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TbCodigoHas.AddCell(Col)

            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TbCodigoHas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            TbCodigoHas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TbCodigoHas.AddCell(Col)

            PdfDoc.Add(TbCodigoHas)
#End Region
        Next
        PdfDoc.Close()
    End Sub
    Sub CrearPdf_TA4_Guia_FERINDMELISSA(ByVal Codigo As Integer)
        Dim General As New DataTable
        Dim Detalle As New DataTable

        Dim Obj As New BLFacturacion
        General = Obj.ObtenerDatosGeneralesGuiaPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetalleGuiaPdf(Codigo)

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 40.0F, 10.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorCeleste As New BaseColor(33, 150, 243)
        Dim Color As New BaseColor(13, 71, 161)
        Dim ColorFondo As New BaseColor(0, 0, 0)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8C As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorCeleste))
        Dim Font8A As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, Color))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font11N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(3)
        Dim WidthsInformacion As Single() = New Single() {10.0F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)

        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(320.0F, 350.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(25, 733)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("TipoGuia", General) & " ELECTRÓNICA", Font9N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroGuia", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 400, 745, 570, 810, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "TablaInformacionCabecera"
        Dim TablaInformacionCabecera As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaInformacionCabecera As Single() = New Single() {8.8F, 7.2F}
        TablaInformacionCabecera.WidthPercentage = 95
        TablaInformacionCabecera.SetWidths(WidthsTablaInformacionCabecera)
        Dim RutaMarca As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca")
        Dim RutaMarcaBMP As iTextSharp.text.Image
        RutaMarcaBMP = iTextSharp.text.Image.GetInstance(RutaMarca)
        RutaMarcaBMP.ScaleToFit(239.0F, 252)
        RutaMarcaBMP.SpacingBefore = 20.0F
        RutaMarcaBMP.SpacingAfter = 10.0F
        RutaMarcaBMP.SetAbsolutePosition(331, 659)
        PdfDoc.Add(RutaMarcaBMP)

        Col = New PdfPCell(New Phrase("BROCAS HSS - MACHOS HSS - TARRAJAS -CHUCK - FRESAS HSS", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("CUCHILLAS - MICRÓMETROS - CALIBRADORES - COMPARARADORES", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("ATENDEMOS PEDIDOS A PROVINCIAS", Font8C))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        PdfDoc.Add(TablaInformacionCabecera)
        Call PintarCuadrado(1, ColorFondo, 26, 690, 314, 690.3, PdfWriter, PdfDoc)

#End Region
#Region "TablaInformacionCabeceraContacto"
        Dim TablaInformacionCabeceraContacto As PdfPTable = New PdfPTable(5)
        Dim WidthsTablaInformacionCabeceraContacto As Single() = New Single() {0.3F, 3.2F, 0.3F, 5.0F, 7.5F}
        TablaInformacionCabeceraContacto.WidthPercentage = 95
        TablaInformacionCabeceraContacto.SetWidths(WidthsTablaInformacionCabeceraContacto)

        Dim RutaTelefono As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaTelefono")
        Dim RutaTelefonoBMP As iTextSharp.text.Image
        RutaTelefonoBMP = iTextSharp.text.Image.GetInstance(RutaTelefono)
        RutaTelefonoBMP.ScaleToFit(10.0F, 10.0F)
        RutaTelefonoBMP.SpacingBefore = 20.0F
        RutaTelefonoBMP.SpacingAfter = 10.0F
        RutaTelefonoBMP.SetAbsolutePosition(25, 678)
        PdfDoc.Add(RutaTelefonoBMP)

        Dim RutaWatzap As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaWatzap")
        Dim RutaWatzapBMP As iTextSharp.text.Image
        RutaWatzapBMP = iTextSharp.text.Image.GetInstance(RutaWatzap)
        RutaWatzapBMP.ScaleToFit(10.0F, 10.0F)
        RutaWatzapBMP.SpacingBefore = 20.0F
        RutaWatzapBMP.SpacingAfter = 10.0F
        RutaWatzapBMP.SetAbsolutePosition(25, 667)
        PdfDoc.Add(RutaWatzapBMP)

        Dim RutaCorreo As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaCorreo")
        Dim RutaCorreoBMP As iTextSharp.text.Image
        RutaCorreoBMP = iTextSharp.text.Image.GetInstance(RutaCorreo)
        RutaCorreoBMP.ScaleToFit(12.0F, 12.0F)
        RutaCorreoBMP.SpacingBefore = 20.0F
        RutaCorreoBMP.SpacingAfter = 10.0F
        RutaCorreoBMP.SetAbsolutePosition(140, 678)
        PdfDoc.Add(RutaCorreoBMP)

        Dim RutaCorreo2 As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaCorreo")
        Dim RutaCorreo2BMP As iTextSharp.text.Image
        RutaCorreo2BMP = iTextSharp.text.Image.GetInstance(RutaCorreo2)
        RutaCorreo2BMP.ScaleToFit(12.0F, 12.0F)
        RutaCorreo2BMP.SpacingBefore = 20.0F
        RutaCorreo2BMP.SpacingAfter = 10.0F
        RutaCorreo2BMP.SetAbsolutePosition(140, 667)
        PdfDoc.Add(RutaCorreo2BMP)

        TablaInformacionCabeceraContacto.AddCell(CVacio)
        If MDIPrincipal.CodigoAgencia = 17 Then
            Col = New PdfPCell(New Phrase("941482857", Font8))
        Else
            Col = New PdfPCell(New Phrase("981573044", Font8))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)

        TablaInformacionCabeceraContacto.AddCell(CVacio)
        If MDIPrincipal.CodigoAgencia = 17 Then
            Col = New PdfPCell(New Phrase("981458735", Font8))
        Else
            Col = New PdfPCell(New Phrase("981573044 / 941482857", Font8))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("ferindmelissa@gmail.com", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)

        PdfDoc.Add(TablaInformacionCabeceraContacto)
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {1.7F, 10.4F, 1.6F, 2.3F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase(" CLIENTE:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("RUC/DNI:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" DIRECCIÓN:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("TELEFONO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("TelefonoAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)

        Call PintarCuadrado(1, ColorFondo, 25, 569, 570, 658, PdfWriter, PdfDoc)
#End Region
#Region "TablaCabeceraDatos"
        Dim TablaCabeceraDatos As PdfPTable = New PdfPTable(4)
        Dim WidthsTablaCabeceraDatos As Single() = New Single() {2.8F, 2.7F, 3.0F, 7.5F}
        TablaCabeceraDatos.WidthPercentage = 95
        TablaCabeceraDatos.SetWidths(WidthsTablaCabeceraDatos)

        Col = New PdfPCell(New Phrase(" FECHA TRASLADO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("FechaTraslado", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)

        Col = New PdfPCell(New Phrase("MOTIVO TRASLADO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("MotivoTraslado", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        PdfDoc.Add(TablaCabeceraDatos)


        Dim TablaCabeceraDatosEnvio As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaCabeceraDatosEnvio As Single() = New Single() {3.0F, 13.0F}
        TablaCabeceraDatosEnvio.WidthPercentage = 95
        TablaCabeceraDatosEnvio.SetWidths(WidthsTablaCabeceraDatosEnvio)

        Col = New PdfPCell(New Phrase(" DIRECCIÓN PARTIDA:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatosEnvio.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionPartida", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatosEnvio.AddCell(Col)
        Col = New PdfPCell(New Phrase(" DIRECCIÓN LLEGADA:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatosEnvio.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionLlegada", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatosEnvio.AddCell(Col)

        PdfDoc.Add(TablaCabeceraDatosEnvio)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalleTitulo As Single() = New Single() {0.8F, 1.5F, 1.8F, 8.4F, 1.5F, 2.0F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("Item", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("U/Medida", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Descripción de Productos", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 0
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Serie", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalle As Single() = New Single() {0.8F, 1.5F, 1.8F, 8.4F, 1.5F, 2.0F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)
        Dim RutaLogoAgua As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogoAgua")
        Dim RutaLogoAguaBMP As iTextSharp.text.Image
        RutaLogoAguaBMP = iTextSharp.text.Image.GetInstance(RutaLogoAgua)
        RutaLogoAguaBMP.ScaleToFit(330.0F, 340.0F)
        RutaLogoAguaBMP.SpacingBefore = 20.0F
        RutaLogoAguaBMP.SpacingAfter = 10.0F
        RutaLogoAguaBMP.SetAbsolutePosition(125, 387)
        PdfDoc.Add(RutaLogoAguaBMP)

        For i = 0 To 33
            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaDetalle.AddCell(Col)


            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("SerieProducto", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

        Next
        PdfDoc.Add(TablaDetalle)
        For i = 1 To 34
            Dim Line = PdfWriter.GetVerticalPosition(True)
            If Line <= 260 Then
                Exit For
            End If
            PdfDoc.Add(New Paragraph(" "))
        Next
#End Region
#Region "CodigoHas"
        Dim TbCodigoHas As PdfPTable = New PdfPTable(2)
        Dim WidthsCodigoHas As Single() = New Single() {5.0F, 11.0F}
        TbCodigoHas.WidthPercentage = 95
        TbCodigoHas.SetWidths(WidthsCodigoHas)
        Dim RutaFace As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaFace")
        Dim RutaFaceBMP As iTextSharp.text.Image
        RutaFaceBMP = iTextSharp.text.Image.GetInstance(RutaFace)
        RutaFaceBMP.ScaleToFit(80.0F, 100.0F)
        RutaFaceBMP.SpacingBefore = 20.0F
        RutaFaceBMP.SpacingAfter = 10.0F
        RutaFaceBMP.SetAbsolutePosition(20, 106)
        PdfDoc.Add(RutaFaceBMP)

        TbCodigoHas.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Puede consultar este comprobante en: " & ObtenerDato("CodigoQR", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbCodigoHas.AddCell(Col)

        TbCodigoHas.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Código Hash: " & ObtenerDato("CodigoHas", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbCodigoHas.AddCell(Col)


        PdfDoc.Add(TbCodigoHas)
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "PiePagina"
        Dim TbPiePagina As PdfPTable = New PdfPTable(4)
        Dim WidthsPiePagina As Single() = New Single() {1.2F, 1.5F, 10.3F, 3.0F}
        TbPiePagina.WidthPercentage = 95
        TbPiePagina.SetWidths(WidthsPiePagina)
        Dim RutaPiePaginaImagen As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPiePaginaImagen")
        Dim RutaPiePaginaImagenBMP As iTextSharp.text.Image
        RutaPiePaginaImagenBMP = iTextSharp.text.Image.GetInstance(RutaPiePaginaImagen)
        RutaPiePaginaImagenBMP.ScaleToFit(300.0F, 320.0F)
        RutaPiePaginaImagenBMP.SpacingBefore = 20.0F
        RutaPiePaginaImagenBMP.SpacingAfter = 10.0F
        RutaPiePaginaImagenBMP.SetAbsolutePosition(139, 16)
        PdfDoc.Add(RutaPiePaginaImagenBMP)

        Dim Qr As String
        Dim Helpers As New Helpers
        Qr = PrepararDatosQRGuia(ObtenerDato("CodigoQR", General))

        Dim QrImage As iTextSharp.text.Image
        QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
        QrImage.ScaleToFit(90.0F, 120.0F)
        QrImage.SpacingBefore = 20.0F
        QrImage.SpacingAfter = 10.0F
        QrImage.SetAbsolutePosition(480, 30)
        PdfDoc.Add(QrImage)


        Col = New PdfPCell(New Phrase("Usuario: ", Font7N))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("UsuarioRegistrador", General), Font7))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        TbPiePagina.AddCell(CVacio)
        TbPiePagina.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Hora: ", Font7N))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("Hora", General), Font7))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        TbPiePagina.AddCell(CVacio)
        TbPiePagina.AddCell(CVacio)

        PdfDoc.Add(TbPiePagina)

        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_Guia_MAQHER(ByVal Codigo As Integer)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        General = Obj.ObtenerDatosGeneralesGuiaPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetalleGuiaPdf(Codigo)


        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 10.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorCeleste As New BaseColor(33, 150, 243)
        Dim Color As New BaseColor(13, 71, 161)
        Dim ColorFondo As New BaseColor(0, 0, 0)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font6 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8C As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorCeleste))
        Dim Font8A As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, Color))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font11N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(5)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 0.5F, 7.0F, 0.5F, 4.5F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(160.0F, 160.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(20, 700)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Sucursal: " + ObtenerDato("DireccionEmisor", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobante", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Telf: " + ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("ELECTRÓNICA", Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("www.mqaperu.com.pe", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("N° " + ObtenerDato("NumeroComprobante", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(0.5, ColorFondo, 425, 715, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(3)
        Dim WidthsCabecera As Single() = New Single() {2.5F, 0.2F, 13.3F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase(" Cliente:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" Dirección:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" RUC:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)
        PdfDoc.Add(New Paragraph(" "))

        Call PintarCuadrado(0.5, ColorFondo, 25, 668, 570, 710, PdfWriter, PdfDoc)
#End Region
#Region "TablaCabeceraDatos"
        Dim TablaCabeceraDatos As PdfPTable = New PdfPTable(5)
        Dim WidthsTablaCabeceraDatos As Single() = New Single() {3.0F, 2.2F, 3.0F, 4.4F, 3.4F}
        TablaCabeceraDatos.WidthPercentage = 95
        TablaCabeceraDatos.SetWidths(WidthsTablaCabeceraDatos)

        'For i = 0 To Guia.Rows.Count - 1
        '    If i = 0 Then
        '        Guias = ObtenerDato("NumeroGuiaRemision", Guia, i)
        '    Else
        '        Guias = Guias + ", " + ObtenerDato("NumeroGuiaRemision", Guia, i)
        '    End If
        'Next
        Col = New PdfPCell(New Phrase("FECHA EMISION", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        'If TipoComprobante = "07" Or TipoComprobante = "08" Then
        '    Col = New PdfPCell(New Phrase("TIP.COMP.MODIFICA", Font7N))
        'Else
        '    Col = New PdfPCell(New Phrase("FEC. VENCIMIENTO", Font7N))
        'End If
        'Col.Border = 0
        'Col.HorizontalAlignment = 1
        'TablaCabeceraDatos.AddCell(Col)
        'If TipoComprobante = "07" Or TipoComprobante = "08" Then
        '    Col = New PdfPCell(New Phrase("N° COMP.MODIFICA", Font7N))
        'Else
        '    Col = New PdfPCell(New Phrase("ORD.COMPRA/PEDIDO", Font7N))
        'End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        'If TipoComprobante = "07" Or TipoComprobante = "08" Then
        '    Col = New PdfPCell(New Phrase("MOTIVO", Font7N))
        'Else
        '    Col = New PdfPCell(New Phrase("GUIA REMISIÓN", Font7N))
        'End If
        'Col.Border = 0
        'Col.HorizontalAlignment = 1
        'TablaCabeceraDatos.AddCell(Col)
        'If TipoComprobante = "07" Or TipoComprobante = "08" Then
        '    Col = New PdfPCell(New Phrase("FEC.COMP.MODIFICA", Font7N))
        'Else
        '    Col = New PdfPCell(New Phrase("COND.PAGO", Font7N))
        'End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("FechaEmision", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        'If TipoComprobante = "07" Or TipoComprobante = "08" Then
        '    Col = New PdfPCell(New Phrase(ObtenerDato("TipoComprobanteAfectado", General), Font7))
        'Else
        '    Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", General), Font7))
        'End If
        'Col.Border = 0
        'Col.HorizontalAlignment = 1
        'TablaCabeceraDatos.AddCell(Col)
        'If TipoComprobante = "07" Or TipoComprobante = "08" Then
        '    Col = New PdfPCell(New Phrase(ObtenerDato("SerieNumeroDocAfectado", General), Font7))
        'Else
        '    Col = New PdfPCell(New Phrase(ObtenerDato("NumeroOrdenCompra", General), Font7))
        'End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)
        'If TipoComprobante = "07" Or TipoComprobante = "08" Then
        '    Col = New PdfPCell(New Phrase(ObtenerDato("CodigoNotaCredito", General), Font7))
        'Else
        '    Col = New PdfPCell(New Phrase(Guias, Font7))
        'End If
        'Col.Border = 0
        'Col.HorizontalAlignment = 1
        'TablaCabeceraDatos.AddCell(Col)
        'If TipoComprobante = "07" Or TipoComprobante = "08" Then
        '    Col = New PdfPCell(New Phrase(ObtenerDato("FechaComprobanteAfecta", General), Font7))
        'Else
        '    Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font7))
        'End If
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaCabeceraDatos.AddCell(Col)

        PdfDoc.Add(TablaCabeceraDatos)

        Call PintarCuadrado(0.5, ColorFondo, 25, 629, 570, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 202, 629, 202, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 304, 629, 304, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 25, 620, 570, 629, PdfWriter, PdfDoc)
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(5)
        Dim WidthsDetalleTitulo As Single() = New Single() {1.5F, 1.5F, 9.6F, 1.8F, 1.6F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("CANTIDAD", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("U.M", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("DESCRIPCIÓN", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("VALOR UNIT.", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("VALOR VENTA", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
        Call PintarCuadrado(0.5, ColorFondo, 25, 300, 570, 629, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 75, 300, 75, 629, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 126, 300, 126, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 454, 300, 454, 653, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 516, 300, 516, 629, PdfWriter, PdfDoc)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(5)
        Dim WidthsDetalle As Single() = New Single() {1.5F, 1.5F, 9.6F, 1.8F, 1.6F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)

        For i = 0 To 28
            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font6))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Precio", Detalle, i), 6), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", Detalle, i), 2), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)
        Next
        PdfDoc.Add(TablaDetalle)
#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(1)
        Dim WidthsMontoLetras As Single() = New Single() {16.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font7N))
        Col.Border = 0
        TablaMontoLetras.AddCell(Col)
        TablaMontoLetras.AddCell(CVacio)
        PdfDoc.Add(TablaMontoLetras)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaTotal"
        Dim TablaTotal As PdfPTable = New PdfPTable(7)
        Dim WidthsTotal As Single() = New Single() {2.5F, 2.5F, 2.7F, 3.0F, 3.0F, 0.5F, 1.8F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        'Dim ContadorCuota As Integer = 0
        'ContadorCuota = Cuotas.Rows.Count

        'If ContadorCuota > 0 Then
        '    Col = New PdfPCell(New Phrase("Importe neto pago:", Font8N))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoNetoPago", General), 2), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        'Else
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        'End If
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("SUB TOTAL", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        'If ContadorCuota > 0 Then
        '    Col = New PdfPCell(New Phrase("N° Cuota", Font8N))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase("Monto Cuota", Font8N))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase("Fecha Vencimiento", Font8N))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        'Else
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        'End If
        TablaTotal.AddCell(CVacio)
        If CDbl(ObtenerDato("TotalAnticipos", General)) > 0 Then
            Col = New PdfPCell(New Phrase("TOTAL ANTICIPOS", Font7N))
        Else
            Col = New PdfPCell(New Phrase("TOTAL DESCUENTO", Font7N))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        If CDbl(ObtenerDato("TotalAnticipos", General)) > 0 Then
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalAnticipos", General), 2), Font8))
        Else
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font8))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        'If ContadorCuota > 0 Then
        '    Col = New PdfPCell(New Phrase(ObtenerDato("cCodCuo", Cuotas, 0), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nMonCuo", Cuotas, 0), 2), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase(ObtenerDato("dFecVen", Cuotas, 0), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        'Else
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        'End If
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTAL VENTAS GRAVADAS", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        'If ContadorCuota > 1 Then
        '    Col = New PdfPCell(New Phrase(ObtenerDato("cCodCuo", Cuotas, 1), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nMonCuo", Cuotas, 1), 2), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase(ObtenerDato("dFecVen", Cuotas, 1), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        'Else
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        'End If
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTAL IGV 18%", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        'If ContadorCuota > 2 Then
        '    Col = New PdfPCell(New Phrase(ObtenerDato("cCodCuo", Cuotas, 2), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nMonCuo", Cuotas, 2), 2), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        '    Col = New PdfPCell(New Phrase(ObtenerDato("dFecVen", Cuotas, 2), Font8))
        '    Col.Border = 0
        '    Col.HorizontalAlignment = 0
        '    TablaTotal.AddCell(Col)
        'Else
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        '    TablaTotal.AddCell(CVacio)
        'End If
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("IMPORTE TOTAL VENTA", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)


        Col = New PdfPCell(New Phrase("Cuentas Bancarias", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)

        PdfDoc.Add(TablaTotal)
        'If ContadorCuota > 0 Then
        '    Call PintarCuadrado(0.5, ColorFondo, 25, 210, 290, 273, PdfWriter, PdfDoc)
        '    Call PintarCuadrado(0.5, ColorFondo, 25, 224, 290, 260, PdfWriter, PdfDoc)
        '    Call PintarCuadrado(0.5, ColorFondo, 25, 235, 290, 248, PdfWriter, PdfDoc)
        '    Call PintarCuadrado(0.5, ColorFondo, 104, 210, 190, 260, PdfWriter, PdfDoc)
        'End If
        Call PintarCuadrado(0.5, ColorFondo, 390, 210, 570, 273, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 390, 224, 570, 260, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 390, 235, 570, 248, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 514, 210, 514, 273, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "CodigoTitulo"
        Dim TbCodigoTitulo As PdfPTable = New PdfPTable(5)
        Dim WidthsCodigoTitulo As Single() = New Single() {2.5F, 5.2F, 0.3F, 6.2F, 1.8F}
        TbCodigoTitulo.WidthPercentage = 95
        TbCodigoTitulo.SetWidths(WidthsCodigoTitulo)
        Dim MontoRetencion As Double = ObtenerDato("MontoRetencion", General)

        Col = New PdfPCell(New Phrase("Depositar a nombre de: ", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoTitulo.AddCell(Col)
        TbCodigoTitulo.AddCell(CVacio)
        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("Información de " + MensajeRetencionDetraccion, Font8N))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbCodigoTitulo.AddCell(Col)
        Else
            TbCodigoTitulo.AddCell(CVacio)
        End If
        TbCodigoTitulo.AddCell(CVacio)
        PdfDoc.Add(TbCodigoTitulo)

#End Region
#Region "CodigoCuenta"
        Dim TbCodigoCuenta As PdfPTable = New PdfPTable(9)
        Dim WidthsCodigoCuenta As Single() = New Single() {1.0F, 1.2F, 2.5F, 3.0F, 0.3F, 2.2F, 2.0, 2.0F, 1.8F}
        TbCodigoCuenta.WidthPercentage = 95
        TbCodigoCuenta.SetWidths(WidthsCodigoCuenta)
        MontoRetencion = ObtenerDato("MontoRetencion", General)

        Col = New PdfPCell(New Phrase("BANCO", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("MONEDA", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("N.CTA CORRIENTE", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("CCI", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)

        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase("Base imponible", Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase("Porcentaje de ", Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase("Monto de ", Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        TbCodigoCuenta.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("S/.", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCS", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCIS", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)

        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("la " + MensajeRetencionDetraccion, Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("la " + MensajeRetencionDetraccion, Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Dim MensajeRetencionDetraccion As String = ""
            If AplicaDetraccion = True Then
                MensajeRetencionDetraccion = "Detracción"
            Else
                MensajeRetencionDetraccion = "Retención"
            End If
            Col = New PdfPCell(New Phrase("la " + MensajeRetencionDetraccion, Font7N))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        TbCodigoCuenta.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("BCP", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("USD.", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCD", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCID", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)

        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Dim AplicaDetraccion As Boolean = ObtenerDato("bAplDet", General)
            Col = New PdfPCell(New Phrase(FormatNumber(CDbl(IIf(AplicaDetraccion = True, MDIPrincipal.PorcentajeDetraccion, MDIPrincipal.PorcentajeRetencion)) * 100, 2) + "%", Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        If MontoRetencion > 0 Then
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("MontoRetencion", General), 2), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbCodigoCuenta.AddCell(Col)
        Else
            TbCodigoCuenta.AddCell(CVacio)
        End If
        TbCodigoCuenta.AddCell(CVacio)
        PdfDoc.Add(TbCodigoCuenta)

        PdfDoc.Add(New Paragraph(" "))

        If MontoRetencion > 0 Then
            Call PintarCuadrado(0.5, ColorFondo, 297, 130, 510, 185, PdfWriter, PdfDoc)
            Call PintarCuadrado(0.5, ColorFondo, 297, 148, 510, 170, PdfWriter, PdfDoc)
            Call PintarCuadrado(0.5, ColorFondo, 372, 130, 441, 170, PdfWriter, PdfDoc)
        End If
        Call PintarCuadrado(0.5, ColorFondo, 25, 130, 290, 185, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 25, 160, 290, 170, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 58, 130, 290, 146, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 58, 130, 100, 170, PdfWriter, PdfDoc)
        Call PintarCuadrado(0.5, ColorFondo, 178, 130, 178, 170, PdfWriter, PdfDoc)
#End Region
#Region "CodigoHas"
        Dim TbCodigoHas As PdfPTable = New PdfPTable(2)
        Dim WidthsCodigoHas As Single() = New Single() {12.0F, 4.0F}
        TbCodigoHas.WidthPercentage = 95
        TbCodigoHas.SetWidths(WidthsCodigoHas)

        Col = New PdfPCell(New Phrase("Autorizado mediante SOL para Emisión de Comprobantes Electrónicos(SEE-DSC)", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoHas.AddCell(Col)
        Col = New PdfPCell(New Phrase("Vendedor: " + ObtenerDato("VENDEDOR", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbCodigoHas.AddCell(Col)


        Col = New PdfPCell(New Phrase("Puede consultar este comprobante en: " & ObtenerDato("CodigoQR", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoHas.AddCell(Col)
        TbCodigoHas.AddCell(CVacio)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoHas.AddCell(Col)
        TbCodigoHas.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("UNA VEZ SALIDA LA MERCADERIA NO HAY LUGAR A RECLAMO NI DEVOLUCIÓN ", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbCodigoHas.AddCell(Col)
        TbCodigoHas.AddCell(CVacio)
        PdfDoc.Add(TbCodigoHas)
        Call PintarCuadrado(0.5, ColorFondo, 25, 90, 570, 90, PdfWriter, PdfDoc)
#End Region
#Region "PiePagina"
        Dim TbPiePagina As PdfPTable = New PdfPTable(4)
        Dim WidthsPiePagina As Single() = New Single() {1.2F, 1.5F, 10.3F, 3.0F}
        TbPiePagina.WidthPercentage = 95
        TbPiePagina.SetWidths(WidthsPiePagina)
        Dim RutaPiePaginaImagen As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPiePaginaImagen")
        Dim RutaPiePaginaImagenBMP As iTextSharp.text.Image
        RutaPiePaginaImagenBMP = iTextSharp.text.Image.GetInstance(RutaPiePaginaImagen)
        RutaPiePaginaImagenBMP.ScaleToFit(500.0F, 520.0F)
        RutaPiePaginaImagenBMP.SpacingBefore = 20.0F
        RutaPiePaginaImagenBMP.SpacingAfter = 10.0F
        RutaPiePaginaImagenBMP.SetAbsolutePosition(50, 14)
        PdfDoc.Add(RutaPiePaginaImagenBMP)

        Dim Qr As String
        Dim Helpers As New Helpers
        Qr = PrepararDatosQRGuia(ObtenerDato("CodigoQR", General))

        Dim QrImage As iTextSharp.text.Image
        QrImage = iTextSharp.text.Image.GetInstance(Helpers.CreateQR(Qr))
        QrImage.ScaleToFit(50.0F, 50.0F)
        QrImage.SpacingBefore = 20.0F
        QrImage.SpacingAfter = 10.0F
        QrImage.SetAbsolutePosition(518, 126)
        PdfDoc.Add(QrImage)

        PdfDoc.Add(TbPiePagina)

        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearCotizacion_Pdf_TA4_FERINDMELISSA(ByVal Codigo As Integer)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        General = Obj.ObtenerDatosGeneralesPdfCotizacion(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdfCotizacion(Codigo)
        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        Dim RutaCotizacion As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion1")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion2")
        End If
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 40.0F, 10.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaCotizacion + NombrePdf, FileMode.Create))
        Dim ColorCeleste As New BaseColor(33, 150, 243)
        Dim Color As New BaseColor(13, 71, 161)
        Dim ColorFondo As New BaseColor(0, 0, 0)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font6 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8C As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorCeleste))
        Dim Font8A As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, Color))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font11N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(3)
        Dim WidthsInformacion As Single() = New Single() {10.0F, 1.0F, 5.0F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(320.0F, 350.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(25, 733)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("COTIZACIÓN", Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(1, ColorFondo, 406, 745, 570, 810, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "TablaInformacionCabecera"
        Dim TablaInformacionCabecera As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaInformacionCabecera As Single() = New Single() {8.8F, 7.2F}
        TablaInformacionCabecera.WidthPercentage = 95
        TablaInformacionCabecera.SetWidths(WidthsTablaInformacionCabecera)
        Dim RutaMarca As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca")
        Dim RutaMarcaBMP As iTextSharp.text.Image
        RutaMarcaBMP = iTextSharp.text.Image.GetInstance(RutaMarca)
        RutaMarcaBMP.ScaleToFit(239.0F, 252)
        RutaMarcaBMP.SpacingBefore = 20.0F
        RutaMarcaBMP.SpacingAfter = 10.0F
        RutaMarcaBMP.SetAbsolutePosition(331, 659)
        PdfDoc.Add(RutaMarcaBMP)

        Col = New PdfPCell(New Phrase("BROCAS HSS - MACHOS HSS - TARRAJAS -CHUCK - FRESAS HSS", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("CUCHILLAS - MICRÓMETROS - CALIBRADORES - COMPARARADORES", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("ATENDEMOS PEDIDOS A PROVINCIAS", Font8C))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionEmisor", General), Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabecera.AddCell(Col)
        TablaInformacionCabecera.AddCell(CVacio)

        PdfDoc.Add(TablaInformacionCabecera)
        Call PintarCuadrado(1, ColorFondo, 26, 690, 314, 690.3, PdfWriter, PdfDoc)

#End Region
#Region "TablaInformacionCabeceraContacto"
        Dim TablaInformacionCabeceraContacto As PdfPTable = New PdfPTable(5)
        Dim WidthsTablaInformacionCabeceraContacto As Single() = New Single() {0.3F, 3.2F, 0.3F, 5.0F, 7.5F}
        TablaInformacionCabeceraContacto.WidthPercentage = 95
        TablaInformacionCabeceraContacto.SetWidths(WidthsTablaInformacionCabeceraContacto)

        Dim RutaTelefono As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaTelefono")
        Dim RutaTelefonoBMP As iTextSharp.text.Image
        RutaTelefonoBMP = iTextSharp.text.Image.GetInstance(RutaTelefono)
        RutaTelefonoBMP.ScaleToFit(10.0F, 10.0F)
        RutaTelefonoBMP.SpacingBefore = 20.0F
        RutaTelefonoBMP.SpacingAfter = 10.0F
        RutaTelefonoBMP.SetAbsolutePosition(25, 678)
        PdfDoc.Add(RutaTelefonoBMP)

        Dim RutaWatzap As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaWatzap")
        Dim RutaWatzapBMP As iTextSharp.text.Image
        RutaWatzapBMP = iTextSharp.text.Image.GetInstance(RutaWatzap)
        RutaWatzapBMP.ScaleToFit(10.0F, 10.0F)
        RutaWatzapBMP.SpacingBefore = 20.0F
        RutaWatzapBMP.SpacingAfter = 10.0F
        RutaWatzapBMP.SetAbsolutePosition(25, 667)
        PdfDoc.Add(RutaWatzapBMP)

        Dim RutaCorreo As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaCorreo")
        Dim RutaCorreoBMP As iTextSharp.text.Image
        RutaCorreoBMP = iTextSharp.text.Image.GetInstance(RutaCorreo)
        RutaCorreoBMP.ScaleToFit(12.0F, 12.0F)
        RutaCorreoBMP.SpacingBefore = 20.0F
        RutaCorreoBMP.SpacingAfter = 10.0F
        RutaCorreoBMP.SetAbsolutePosition(140, 678)
        PdfDoc.Add(RutaCorreoBMP)

        Dim RutaCorreo2 As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaCorreo")
        Dim RutaCorreo2BMP As iTextSharp.text.Image
        RutaCorreo2BMP = iTextSharp.text.Image.GetInstance(RutaCorreo2)
        RutaCorreo2BMP.ScaleToFit(12.0F, 12.0F)
        RutaCorreo2BMP.SpacingBefore = 20.0F
        RutaCorreo2BMP.SpacingAfter = 10.0F
        RutaCorreo2BMP.SetAbsolutePosition(140, 667)
        PdfDoc.Add(RutaCorreo2BMP)

        TablaInformacionCabeceraContacto.AddCell(CVacio)
        If MDIPrincipal.CodigoAgencia = 17 Then
            Col = New PdfPCell(New Phrase("941482857", Font8))
        Else
            Col = New PdfPCell(New Phrase("981573044", Font8))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)

        TablaInformacionCabeceraContacto.AddCell(CVacio)
        If MDIPrincipal.CodigoAgencia = 17 Then
            Col = New PdfPCell(New Phrase("981458735", Font8))
        Else
            Col = New PdfPCell(New Phrase("981573044 / 941482857", Font8))
        End If
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("ferindmelissa@gmail.com", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaInformacionCabeceraContacto.AddCell(Col)
        TablaInformacionCabeceraContacto.AddCell(CVacio)

        PdfDoc.Add(TablaInformacionCabeceraContacto)
        PdfDoc.Add(New Paragraph(" "))

#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {1.7F, 10.4F, 1.6F, 2.3F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase(" CLIENTE:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("RUC/DNI:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" DIRECCIÓN:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("TELEFONO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("TelefonoAdquiriente", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)

        Call PintarCuadrado(1, ColorFondo, 25, 566, 570, 658, PdfWriter, PdfDoc)
#End Region
#Region "TablaCabeceraDatos"
        Dim TablaCabeceraDatos As PdfPTable = New PdfPTable(6)
        Dim WidthsTablaCabeceraDatos As Single() = New Single() {2.8F, 2.7F, 2.5F, 4.1F, 1.6F, 2.3F}
        TablaCabeceraDatos.WidthPercentage = 95
        TablaCabeceraDatos.SetWidths(WidthsTablaCabeceraDatos)

        Col = New PdfPCell(New Phrase(" FECHA EMISIÓN:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("VENDEDOR:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("VENDEDOR", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("MONEDA:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("MONEDA", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)

        Col = New PdfPCell(New Phrase(" FECHA VENCIMIENTO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("CONDICIÓN PAGO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("OC/PEDIDO:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatos.AddCell(Col)
        PdfDoc.Add(TablaCabeceraDatos)
#End Region
#Region "TablaCabeceraDatos"
        Dim TablaCabeceraDatosFinal As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaCabeceraDatosFinals As Single() = New Single() {2.8F, 13.2F}
        TablaCabeceraDatosFinal.WidthPercentage = 95
        TablaCabeceraDatosFinal.SetWidths(WidthsTablaCabeceraDatosFinals)

        Col = New PdfPCell(New Phrase(" TIEMPO ENTREGA:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatosFinal.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("cTieEnt", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaCabeceraDatosFinal.AddCell(Col)
        TablaCabeceraDatosFinal.AddCell(CVacio)

        PdfDoc.Add(TablaCabeceraDatosFinal)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(7)
        Dim WidthsDetalleTitulo As Single() = New Single() {0.8F, 1.5F, 1.8F, 7.0F, 1.5F, 1.8F, 1.6F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("Item", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("U/Medida", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Código", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Descripción de Productos", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 0
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Cantidad", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Precio Unitario", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase("Sub Total", Font8NB))
        Col.Border = 0
        Col.BackgroundColor = ColorCeleste
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(7)
        Dim WidthsDetalle As Single() = New Single() {0.8F, 1.5F, 1.8F, 7.0F, 1.5F, 1.8F, 1.6F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)
        Dim RutaLogoAgua As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogoAgua")
        Dim RutaLogoAguaBMP As iTextSharp.text.Image
        RutaLogoAguaBMP = iTextSharp.text.Image.GetInstance(RutaLogoAgua)
        RutaLogoAguaBMP.ScaleToFit(330.0F, 340.0F)
        RutaLogoAguaBMP.SpacingBefore = 20.0F
        RutaLogoAguaBMP.SpacingAfter = 10.0F
        RutaLogoAguaBMP.SetAbsolutePosition(125, 387)
        PdfDoc.Add(RutaLogoAguaBMP)

        For i = 0 To 22
            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font6))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TablaDetalle.AddCell(Col)


            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Precio", Detalle, i), 6), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)

            If i > Detalle.Rows.Count - 1 Then
                Col = New PdfPCell(New Phrase(" ", Font7))
            Else
                Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", Detalle, i), 2), Font7))
            End If
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)
        Next
        PdfDoc.Add(TablaDetalle)
#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(1)
        Dim WidthsMontoLetras As Single() = New Single() {16.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font8))
        Col.Border = 0
        Col.BorderWidthTop = 1
        Col.BorderColorTop = ColorFondo
        TablaMontoLetras.AddCell(Col)
        TablaMontoLetras.AddCell(CVacio)
        PdfDoc.Add(TablaMontoLetras)
        PdfDoc.Add(New Paragraph(" "))
        For i = 1 To 34
            Dim Line = PdfWriter.GetVerticalPosition(True)
            If Line <= 260 Then
                Exit For
            End If
            PdfDoc.Add(New Paragraph(" "))
        Next

#End Region
#Region "TablaTotal"
        Dim TablaTotal As PdfPTable = New PdfPTable(7)
        Dim WidthsTotal As Single() = New Single() {3.0F, 3.0F, 7.0F, 0.2F, 3.0F, 0.2F, 1.8F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)
        Dim RutaYape As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaYape")
        Dim RutaYapeBMP As iTextSharp.text.Image
        RutaYapeBMP = iTextSharp.text.Image.GetInstance(RutaYape)
        RutaYapeBMP.ScaleToFit(85.0F, 85.0F)
        RutaYapeBMP.SpacingBefore = 20.0F
        RutaYapeBMP.SpacingAfter = 10.0F
        RutaYapeBMP.SetAbsolutePosition(25, 171)
        PdfDoc.Add(RutaYapeBMP)

        Dim RutaQrYape As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaQrYape")
        Dim RutaQrYapeBMP As iTextSharp.text.Image
        RutaQrYapeBMP = iTextSharp.text.Image.GetInstance(RutaQrYape)
        RutaQrYapeBMP.ScaleToFit(90.0F, 90.0F)
        RutaQrYapeBMP.SpacingBefore = 20.0F
        RutaQrYapeBMP.SpacingAfter = 10.0F
        RutaQrYapeBMP.SetAbsolutePosition(111, 169)
        PdfDoc.Add(RutaQrYapeBMP)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font9N))
        Col.Border = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Sub Total:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("BCP CORREINTE S/.:" + ObtenerDato("CCS", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Descuentos:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("BCP CORRIENTE $:" + ObtenerDato("CCD", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Total Ventas Gravadas:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("BCP INTERBANCARIO S/.:" + ObtenerDato("CCIS", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Total IGV 18%:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        TablaTotal.AddCell(CVacio)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("BCP INTERBANCARIO $:" + ObtenerDato("CCID", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Importe Total de Venta:", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        PdfDoc.Add(TablaTotal)

        Call PintarCuadrado(1, ColorFondo, 420, 169, 570, 258, PdfWriter, PdfDoc)
        Call PintarCuadrado(1, ColorFondo, 202, 169, 415, 258, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "CodigoHas"
        Dim TbCodigoHas As PdfPTable = New PdfPTable(2)
        Dim WidthsCodigoHas As Single() = New Single() {5.0F, 11.0F}
        TbCodigoHas.WidthPercentage = 95
        TbCodigoHas.SetWidths(WidthsCodigoHas)
        Dim RutaFace As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaFace")
        Dim RutaFaceBMP As iTextSharp.text.Image
        RutaFaceBMP = iTextSharp.text.Image.GetInstance(RutaFace)
        RutaFaceBMP.ScaleToFit(80.0F, 100.0F)
        RutaFaceBMP.SpacingBefore = 20.0F
        RutaFaceBMP.SpacingAfter = 10.0F
        RutaFaceBMP.SetAbsolutePosition(20, 106)
        PdfDoc.Add(RutaFaceBMP)

        TbCodigoHas.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("cObservaciones", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbCodigoHas.AddCell(Col)

        PdfDoc.Add(TbCodigoHas)

        For i = 1 To 34
            Dim Line = PdfWriter.GetVerticalPosition(True)
            If Line <= 60 Then
                Exit For
            End If
            PdfDoc.Add(New Paragraph(" "))
        Next
#End Region
#Region "PiePagina"
        Dim TbPiePagina As PdfPTable = New PdfPTable(4)
        Dim WidthsPiePagina As Single() = New Single() {1.2F, 1.5F, 10.3F, 3.0F}
        TbPiePagina.WidthPercentage = 95
        TbPiePagina.SetWidths(WidthsPiePagina)
        Dim RutaPiePaginaImagen As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPiePaginaImagen")
        Dim RutaPiePaginaImagenBMP As iTextSharp.text.Image
        RutaPiePaginaImagenBMP = iTextSharp.text.Image.GetInstance(RutaPiePaginaImagen)
        RutaPiePaginaImagenBMP.ScaleToFit(300.0F, 320.0F)
        RutaPiePaginaImagenBMP.SpacingBefore = 20.0F
        RutaPiePaginaImagenBMP.SpacingAfter = 10.0F
        RutaPiePaginaImagenBMP.SetAbsolutePosition(133, 16)
        PdfDoc.Add(RutaPiePaginaImagenBMP)

        Dim RutaPiePaginaImagen2 As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPiePaginaImagen2")
        Dim RutaPiePaginaImagen2BMP As iTextSharp.text.Image
        RutaPiePaginaImagen2BMP = iTextSharp.text.Image.GetInstance(RutaPiePaginaImagen2)
        RutaPiePaginaImagen2BMP.ScaleToFit(110.0F, 120.0F)
        RutaPiePaginaImagen2BMP.SpacingBefore = 20.0F
        RutaPiePaginaImagen2BMP.SpacingAfter = 10.0F
        RutaPiePaginaImagen2BMP.SetAbsolutePosition(460, 30)
        PdfDoc.Add(RutaPiePaginaImagen2BMP)


        Col = New PdfPCell(New Phrase("Usuario: ", Font7N))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CodUsuario", General), Font7))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        TbPiePagina.AddCell(CVacio)
        TbPiePagina.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Hora: ", Font7N))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("Hora", General), Font7))
        Col.Border = 0
        TbPiePagina.AddCell(Col)
        TbPiePagina.AddCell(CVacio)
        TbPiePagina.AddCell(CVacio)

        PdfDoc.Add(TbPiePagina)

        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearCotizacionPdf_TA4_GROUPTOOLS(ByVal Codigo As Integer)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable

        General = Obj.ObtenerDatosGeneralesPdfCotizacion(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdfCotizacion(Codigo)

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        Dim RutaCotizacion As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion1")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion2")
        End If
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 5.0F, 5.0F, 20.0F, 5.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaCotizacion + NombrePdf, FileMode.Create))
        Dim ColorAzul As New BaseColor(3, 62, 105)
        Dim ColorBlanco = BaseColor.WHITE
        Dim ColorPlomoAcero As New BaseColor(69, 90, 100)
        Dim ColorLetra As New BaseColor(55, 71, 79)
        Dim ColorFondoDetalle As New BaseColor(224, 224, 224)

        Dim Font7_H As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.NORMAL, BaseColor.BLACK))
        Dim Font7_H_P As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.NORMAL, ColorLetra))
        Dim Font8_C As New Font(FontFactory.GetFont(FontFactory.COURIER, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8_H As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8N_C As New Font(FontFactory.GetFont(FontFactory.COURIER_BOLD, 8, Font.NORMAL, BaseColor.BLACK))
        Dim Font8NN As New Font(FontFactory.GetFont(FontFactory.COURIER, 8, Font.BOLD, BaseColor.BLACK))
        Dim Font9N_C As New Font(FontFactory.GetFont(FontFactory.COURIER, 9, Font.BOLD, BaseColor.BLACK))
        Dim Font9N_C_B As New Font(FontFactory.GetFont(FontFactory.COURIER, 9, Font.BOLD, BaseColor.WHITE))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK))
        Dim Font8N_R As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD, BaseColor.RED))
        Dim Font9_H_P As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.NORMAL, ColorLetra))
        Dim Font9_H_B As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.WHITE))
        Dim Font10NN As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD, BaseColor.BLACK))
        Dim Font12NNB As New Font(FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, Font.BOLD, BaseColor.WHITE))
        Dim Font13NN As New Font(FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 13, Font.BOLD, BaseColor.BLACK))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

        Dim Line As Integer = 0
        Dim CantidadItems As Integer = Detalle.Rows.Count - 1
        Dim ii As Integer = 0
        Dim Contador As Integer = 0
        For ContadorPagina = 0 To 10
            If ContadorPagina > 0 Then
                If ii <= CantidadItems Then
                    PdfDoc.NewPage()
                    Contador = 0
                Else
                    Exit For
                End If
            End If
#Region "TablaInformacion"
            Dim TbInformacion As PdfPTable = New PdfPTable(3)
            Dim WidthsInformacion As Single() = New Single() {11.8F, 0.2F, 4.0F}
            TbInformacion.WidthPercentage = 95
            TbInformacion.SetWidths(WidthsInformacion)

            Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
            Dim LogoBMP As iTextSharp.text.Image
            LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
            LogoBMP.ScaleToFit(400.0F, 400.0F)
            LogoBMP.SpacingBefore = 20.0F
            LogoBMP.SpacingAfter = 10.0F
            LogoBMP.SetAbsolutePosition(20, 737)
            PdfDoc.Add(LogoBMP)

            TbInformacion.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font12NNB))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.Padding = 5
            TbInformacion.AddCell(Col)

            TbInformacion.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.Padding = 15
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("COTIZACIÓN", Font10NN))
            Col.Border = 0
            Col.Padding = 15
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.HorizontalAlignment = 1
            TbInformacion.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font7_H))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font9N))
            Col.Border = 0
            Col.PaddingTop = 10
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.HorizontalAlignment = 1
            TbInformacion.AddCell(Col)


            Col = New PdfPCell(New Phrase(ObtenerDato("Contactos", General), Font8_H))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font13NN))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font9N))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.RIGHT_BORDER
            TbInformacion.AddCell(Col)

            PdfDoc.Add(TbInformacion)

            TbInformacion = New PdfPTable(4)
            WidthsInformacion = New Single() {7.0F, 4.8F, 0.2F, 4.0F}
            TbInformacion.WidthPercentage = 95
            TbInformacion.SetWidths(WidthsInformacion)

            Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font8_H))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase("ATENDEMOS ENVIOS A PROVINCIA", Font8N_R))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font13NN))
            Col.Border = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font9N))
            Col.Border = 0
            Col.BorderColor = ColorAzul
            Col.Border = PdfPCell.TOP_BORDER
            TbInformacion.AddCell(Col)

            PdfDoc.Add(TbInformacion)
#End Region
#Region "TablaCabeceraTitulo"
            Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(5)
            Dim WidthsCabeceraTitulo As Single() = New Single() {11.0F, 0.1F, 2.1F, 0.3F, 2.5F}
            TablaCabeceraTitulo.WidthPercentage = 95
            TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

            Col = New PdfPCell(New Phrase("", Font8N_C))
            Col.Border = 0
            TablaCabeceraTitulo.AddCell(Col)
            TablaCabeceraTitulo.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font8_C))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            PdfDoc.Add(TablaCabeceraTitulo)

            Col = New PdfPCell(New Phrase("DATOS DEL CLIENTE", Font9N_C_B))
            Col.Border = 0
            Col.Padding = 5
            Col.BackgroundColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            TablaCabeceraTitulo.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("F. Emisión", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.VerticalAlignment = 0
            TablaCabeceraTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            TablaCabeceraTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaEmision", General), Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.RIGHT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaCabeceraTitulo.AddCell(Col)
            PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
            Dim TablaCabecera As PdfPTable = New PdfPTable(7)
            Dim WidthsCabecera As Single() = New Single() {2.0F, 0.3F, 8.7F, 0.1F, 2.1F, 0.3F, 2.5F}
            TablaCabecera.WidthPercentage = 95
            TablaCabecera.SetWidths(WidthsCabecera)

            Col = New PdfPCell(New Phrase("Razon Social", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocialAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)

            Col = New PdfPCell(New Phrase("Cond. Pago", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("CONDPAGO", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)

            Col = New PdfPCell(New Phrase("RUC", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("Tipo Moneda", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("MONEDA", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)

            Col = New PdfPCell(New Phrase("Dirección", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("DireccionAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("F. Vencimiento", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("FechaVencimiento", General), Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)


            Col = New PdfPCell(New Phrase("Correo", Font8N_C))
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            Col.PaddingBottom = 5
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("EmailAdquiriente", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("Vendedor", Font8N_C))
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.LEFT_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font8N_C))
            Col.Border = 0
            Col.PaddingBottom = 5
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("VENDEDOR", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.RIGHT_BORDER
            TablaCabecera.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            TablaCabecera.AddCell(CVacio)
            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font8N_C))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)
            Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Col.Border = 0
            Col.BorderColor = ColorPlomoAcero
            Col.Border = PdfPCell.TOP_BORDER
            TablaCabecera.AddCell(Col)

            PdfDoc.Add(TablaCabecera)
#End Region
#Region "TablaDetalleTitulo"
            Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(7)
            Dim WidthsDetalleTitulo As Single() = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 1.9F}
            TablaDetalleTitulo.WidthPercentage = 95
            TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

            Col = New PdfPCell(New Phrase("Item", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("Código", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("Cant.", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("U.M", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("Descripción", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 0
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("P.Unit", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 2
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)
            Col = New PdfPCell(New Phrase("Sub Total", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 2
            Col.PaddingBottom = 5
            TablaDetalleTitulo.AddCell(Col)

            PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
            Dim TablaDetalle As PdfPTable = New PdfPTable(8)
            Dim WidthsDetalle As Single() = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 0.1F, 1.8F}
            TablaDetalle.WidthPercentage = 95
            TablaDetalle.SetWidths(WidthsDetalle)
            TablaDetalle.FooterRows = 20
            TablaDetalle.HeaderRows = 30
            Do While Contador < 30
                Line = PdfWriter.GetVerticalPosition(True)
                If Line > 249 Then
                    TablaDetalle = New PdfPTable(8)
                    WidthsDetalle = New Single() {0.8F, 1.8F, 1.2F, 1.0F, 7.5F, 1.8F, 0.1F, 1.8F}
                    TablaDetalle.WidthPercentage = 95
                    TablaDetalle.SetWidths(WidthsDetalle)
                    If ii <= Detalle.Rows.Count - 1 Then
                        Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("CodUnidadMedida", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 1
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, ii), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Precio", Detalle, ii), 6), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 2
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase("", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorAzul
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", Detalle, ii), 2), Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.HorizontalAlignment = 2
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)
                        PdfDoc.Add(TablaDetalle)
                        Contador = Contador + 1
                        ii = ii + 1
                    Else
                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.LEFT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 1
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase("", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorAzul
                        TablaDetalle.AddCell(Col)

                        Col = New PdfPCell(New Phrase(" ", Font7_H_P))
                        Col.Border = 0
                        Col.Border = PdfPCell.RIGHT_BORDER
                        Col.BorderColor = ColorAzul
                        Col.PaddingBottom = 5
                        Col.PaddingTop = 5
                        Col.HorizontalAlignment = 2
                        TablaDetalle.AddCell(Col)
                        PdfDoc.Add(TablaDetalle)
                        Contador = Contador + 1
                    End If
                Else
                    Contador = 30
                    Exit Do
                End If
            Loop
#End Region
#Region "TablaMontoLetras"
            Dim TablaMontoLetras As PdfPTable = New PdfPTable(2)
            Dim WidthsMontoLetras As Single() = New Single() {2.5F, 13.5F}
            TablaMontoLetras.WidthPercentage = 95
            TablaMontoLetras.SetWidths(WidthsMontoLetras)

            Col = New PdfPCell(New Phrase("", Font8N_C))
            Col.Border = 0
            Col.Border = PdfPCell.TOP_BORDER
            Col.BorderColor = ColorAzul
            TablaMontoLetras.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.TOP_BORDER
            Col.BorderColor = ColorAzul
            TablaMontoLetras.AddCell(Col)

            Col = New PdfPCell(New Phrase("Monto en letras", Font8N_C))
            Col.Border = 0
            Col.PaddingTop = 7
            Col.PaddingBottom = 7
            TablaMontoLetras.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font7_H_P))
            Col.Border = 0
            Col.PaddingTop = 7
            Col.PaddingBottom = 7
            TablaMontoLetras.AddCell(Col)

            PdfDoc.Add(TablaMontoLetras)
#End Region
#Region "TablaTotalTitulo"
            Dim TablaTotalTitulo As PdfPTable = New PdfPTable(3)
            Dim WidthsTotalTitulo As Single() = New Single() {10.0F, 1.0F, 5.0F}
            TablaTotalTitulo.WidthPercentage = 95
            TablaTotalTitulo.SetWidths(WidthsTotalTitulo)

            Col = New PdfPCell(New Phrase("CUENTAS BANCARIAS", Font9N_C_B))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            Col.Padding = 5
            Col.BackgroundColor = ColorAzul
            TablaTotalTitulo.AddCell(Col)
            TablaTotalTitulo.AddCell(CVacio)
            Col = New PdfPCell(New Phrase("MONTOS TOTALES", Font9N_C_B))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            Col.Padding = 5
            Col.BackgroundColor = ColorPlomoAcero
            TablaTotalTitulo.AddCell(Col)

            PdfDoc.Add(TablaTotalTitulo)

#End Region
#Region "TablaTotalTituloCuentas"
            Dim TablaTotalTituloCuentas As PdfPTable = New PdfPTable(6)
            Dim WidthsTotalTituloCuentas As Single() = New Single() {3.0F, 7.0F, 1.0F, 3.0F, 0.2F, 1.8F}
            TablaTotalTituloCuentas.WidthPercentage = 95
            TablaTotalTituloCuentas.SetWidths(WidthsTotalTituloCuentas)

            Col = New PdfPCell(New Phrase("BANCO", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("NÚMEROS DE CUENTA", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C_B))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("SUB TOTAL", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font9_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("N° C/C s/:" + ObtenerDato("CCS", General), Font9_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font8NN))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("TOTAL DESCUENTOS", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Descuentos", General), 2), Font9_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("N° CCI s/:" + ObtenerDato("CCIS", General), Font9_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("VENTAS GRAVADAS", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalValorVenta", General), 2), Font9_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("N° C/C $:" + ObtenerDato("CCD", General), Font9_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("TOTAL IGV 18%", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9N_C))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font9_H_P))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("N° CCI $:" + ObtenerDato("CCID", General), Font9_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.LEFT_BORDER
            Col.BorderColor = ColorPlomoAcero
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("IMPORTE TOTAL", Font9N_C_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(":", Font9_H_B))
            Col.Border = 0
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 1
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font9_H_B))
            Col.Border = 0
            Col.PaddingBottom = 5
            Col.BackgroundColor = ColorAzul
            Col.HorizontalAlignment = 2
            TablaTotalTituloCuentas.AddCell(Col)

            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font7_H_P))
            Col.Border = 0
            Col.Border = PdfPCell.BOTTOM_BORDER
            Col.BorderColor = ColorPlomoAcero
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C_B))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9N_C))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)
            Col = New PdfPCell(New Phrase("", Font9_H_B))
            Col.Border = 0
            TablaTotalTituloCuentas.AddCell(Col)


            PdfDoc.Add(TablaTotalTituloCuentas)
            PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "CodigoHas"
            Dim TbCodigoHas As PdfPTable = New PdfPTable(1)
            Dim WidthsCodigoHas As Single() = New Single() {16.0F}
            TbCodigoHas.WidthPercentage = 95
            TbCodigoHas.SetWidths(WidthsCodigoHas)

            Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Col.Border = 0
            TbCodigoHas.AddCell(Col)

            Col = New PdfPCell(New Phrase(" ", Font7_H_P))
            Col.Border = 0
            TbCodigoHas.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("MensajeCuerpo", General), Font9_H_P))
            Col.Border = 0
            TbCodigoHas.AddCell(Col)

            PdfDoc.Add(TbCodigoHas)
#End Region
#Region "PiePagina"
            Dim MarcaBMP As Image

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaBancoBcp"))
            MarcaBMP.ScaleToFit(70.0F, 70.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(35, 117)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaLogoAgua"))
            MarcaBMP.ScaleToFit(570, 570)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(10, 360)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca1"))
            MarcaBMP.ScaleToFit(50.0F, 50.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(17, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca2"))
            MarcaBMP.ScaleToFit(90.0F, 90.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(75, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca3"))
            MarcaBMP.ScaleToFit(50.0F, 50.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(175, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca4"))
            MarcaBMP.ScaleToFit(100.0F, 100.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(230, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca5"))
            MarcaBMP.ScaleToFit(70.0F, 70.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(343, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca6"))
            MarcaBMP.ScaleToFit(80.0F, 80.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(417, 8)
            PdfDoc.Add(MarcaBMP)

            MarcaBMP = Image.GetInstance(RutaEmpresa + ConfigurationManager.AppSettings("RutaMarca7"))
            MarcaBMP.ScaleToFit(80.0F, 80.0F)
            MarcaBMP.SpacingBefore = 20.0F
            MarcaBMP.SpacingAfter = 10.0F
            MarcaBMP.SetAbsolutePosition(500, 8)
            PdfDoc.Add(MarcaBMP)
#End Region
        Next
        PdfDoc.Close()
    End Sub
    Sub CrearCotizacion_Pdf_TA4_MAQHER(ByVal Codigo As Integer)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        General = Obj.ObtenerDatosGeneralesPdfCotizacion(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdfCotizacion(Codigo)
        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        Dim RutaCotizacion As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion1")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion2")
        End If
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 10.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaCotizacion + NombrePdf, FileMode.Create))
        Dim ColorFondoBase As New BaseColor(200, 230, 201)
        Dim ColorBlanco As New BaseColor(255, 255, 255)
        Dim ColorFondo As New BaseColor(0, 0, 0)
        Dim ColorLetra As New BaseColor(0, 0, 0)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim ColorBordeCelda As New BaseColor(255, 255, 255)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font8NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font9 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL, ColorLetra))
        Dim Font9N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font9NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font11N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.BOLD, ColorLetra))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, ColorLetra))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaInformacion"
        Dim TbInformacion As PdfPTable = New PdfPTable(5)
        Dim WidthsInformacion As Single() = New Single() {4.5F, 0.5F, 7.0F, 0.5F, 4.5F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(160.0F, 160.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(20, 700)
        PdfDoc.Add(LogoBMP)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Direccion", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumRuc", General), Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Sucursal: " + ObtenerDato("DireccionEmisor", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("Telf: " + ObtenerDato("Contactos", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("COTIZACIÓN", Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("Correo", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)

        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("www.mqaperu.com.pe", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("NumeroComprobante", General), Font11N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)

        PdfDoc.Add(TbInformacion)
        Call PintarCuadrado(0.5, ColorFondo, 425, 715, 570, 820, PdfWriter, PdfDoc)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(4)
        Dim WidthsCabecera As Single() = New Single() {1.8F, 8.6F, 1.6F, 4.0F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase(" Emisión", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(": " + ObtenerDato("FechaEmision", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("Cond. Venta", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(": " + ObtenerDato("CONDPAGO", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" Cliente", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(": " + ObtenerDato("RazonSocialAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("Vendedor", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(": " + ObtenerDato("VENDEDOR", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" RUC o DNI", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(": " + ObtenerDato("DocumentoAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabecera.AddCell(Col)
        PdfDoc.Add(TablaCabecera)
#End Region
#Region "TablaCabeceraAd"
        Dim TablaCabeceraAd As PdfPTable = New PdfPTable(2)
        Dim WidthsTablaCabeceraAd As Single() = New Single() {1.8F, 14.2F}
        TablaCabeceraAd.WidthPercentage = 95
        TablaCabeceraAd.SetWidths(WidthsTablaCabeceraAd)

        Col = New PdfPCell(New Phrase(" Dirección", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabeceraAd.AddCell(Col)
        Col = New PdfPCell(New Phrase(": " + ObtenerDato("DireccionAdquiriente", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaCabeceraAd.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8N))
        Col.Border = 0
        Col.Padding = 1
        Col.BackgroundColor = ColorBlanco
        Col.HorizontalAlignment = 0
        TablaCabeceraAd.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.HorizontalAlignment = 0
        TablaCabeceraAd.AddCell(Col)

        PdfDoc.Add(TablaCabeceraAd)
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(7)
        Dim WidthsDetalleTitulo As Single() = New Single() {0.9F, 1.5F, 7.1F, 1.2F, 1.5F, 1.9F, 1.9F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("ITEM", Font7N))
        Col.Border = 0
        Col.Padding = 7
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("CODIGO", Font7N))
        Col.Border = 0
        Col.Padding = 7
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("ARTÍCULO", Font7N))
        Col.Border = 0
        Col.Padding = 7
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("CANT.", Font7N))
        Col.Border = 0
        Col.Padding = 7
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("U.M", Font7N))
        Col.Border = 0
        Col.Padding = 7
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("VALOR UNIT.", Font7N))
        Col.Border = 0
        Col.Padding = 7
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("VALOR VENTA", Font7N))
        Col.Border = 0
        Col.Padding = 7
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(7)
        Dim WidthsDetalle As Single() = New Single() {0.9F, 1.5F, 7.1F, 1.2F, 1.5F, 1.9F, 1.9F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)
        Dim cantidad As Integer = Detalle.Rows.Count - 1

        For i = 0 To Detalle.Rows.Count - 1
            Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, i), Font7))
            Col.Border = 0
            Col.Padding = 6
            Col.BorderWidthLeft = 0.5
            Col.BorderColorLeft = ColorFondo
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)


            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            Col.Border = 0
            Col.Padding = 6
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            Col.Padding = 6
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            Col.Border = 0
            Col.Padding = 6
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Unidadmedida", Detalle, i), Font7))
            Col.Border = 0
            Col.Padding = 6
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)


            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorUnitario", Detalle, i), 6), Font7))
            Col.Border = 0
            Col.Padding = 6
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)


            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ValorVenta", Detalle, i), 2), Font7))
            Col.Border = 0
            Col.Padding = 6
            Col.BorderWidthRight = 0.5
            Col.BorderColorRight = ColorFondo
            Col.HorizontalAlignment = 2
            TablaDetalle.AddCell(Col)
        Next
        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 1
        TablaDetalle.AddCell(Col)


        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)


        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaDetalle.AddCell(Col)

        PdfDoc.Add(TablaDetalle)
#End Region
#Region "TablaMontoLetras"
        Dim TablaMontoLetras As PdfPTable = New PdfPTable(1)
        Dim WidthsMontoLetras As Single() = New Single() {16.0F}
        TablaMontoLetras.WidthPercentage = 95
        TablaMontoLetras.SetWidths(WidthsMontoLetras)
        Col = New PdfPCell(New Phrase(ObtenerDato("MontoLetras", General), Font7N))
        Col.Border = 0
        Col.Padding = 6
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo

        TablaMontoLetras.AddCell(Col)
        TablaMontoLetras.AddCell(CVacio)
        PdfDoc.Add(TablaMontoLetras)

#End Region
#Region "TablaTotal"
        Dim TablaTotal As PdfPTable = New PdfPTable(6)
        Dim WidthsTotal As Single() = New Single() {0.8F, 9.4F, 0.5F, 3.0F, 0.5F, 1.8F}
        TablaTotal.WidthPercentage = 95
        TablaTotal.SetWidths(WidthsTotal)

        Col = New PdfPCell(New Phrase(" *", Font11N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(" PRECIOS NO INCLUYEN IGV", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("SUB TOTAL", Font7N))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        Col.BackgroundColor = ColorFondoBase
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.HorizontalAlignment = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(" *", Font11N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(" PLAZO DE ENTREGA: " + ObtenerDato("cTieEnt", General), Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TOTAL IGV 18%", Font7N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("TotalImpuestos", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        Col = New PdfPCell(New Phrase(" *", Font11N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(" LUGAR DE ENTREGA: EN SU ALMACEN", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font7N))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font8))
        Col.Border = 0
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)


        Col = New PdfPCell(New Phrase(" *", Font11N))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(" VALIDEZ DE OFERTA: 15 DÍAS", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        TablaTotal.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("IMPORTE TOTAL VENTA", Font7N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BackgroundColor = ColorFondoBase
        Col.HorizontalAlignment = 0
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(IIf(ObtenerDato("MONEDA", General) = "SOLES", "(S/)", "($.)"), Font7N))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.HorizontalAlignment = 1
        TablaTotal.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("ImporteTotal", General), 2), Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 2
        TablaTotal.AddCell(Col)

        PdfDoc.Add(TablaTotal)
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "CodigoTitulo"
        Dim TbCodigoTitulo As PdfPTable = New PdfPTable(3)
        Dim WidthsCodigoTitulo As Single() = New Single() {2.5F, 5.2F, 8.3F}
        TbCodigoTitulo.WidthPercentage = 95
        TbCodigoTitulo.SetWidths(WidthsCodigoTitulo)

        Col = New PdfPCell(New Phrase("Cuentas Bancarias:", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbCodigoTitulo.AddCell(Col)
        TbCodigoTitulo.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("Depositar a nombre de: ", Font7))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoTitulo.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font7N))
        Col.Border = 0
        Col.BorderWidthTop = 0.5
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoTitulo.AddCell(Col)
        TbCodigoTitulo.AddCell(CVacio)
        PdfDoc.Add(TbCodigoTitulo)
#End Region
#Region "CodigoCuenta"
        Dim TbCodigoCuenta As PdfPTable = New PdfPTable(5)
        Dim WidthsCodigoCuenta As Single() = New Single() {1.0F, 1.2F, 2.5F, 3.0F, 8.3F}
        TbCodigoCuenta.WidthPercentage = 95
        TbCodigoCuenta.SetWidths(WidthsCodigoCuenta)

        Col = New PdfPCell(New Phrase("BANCO", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("MONEDA", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("N.CTA CORRIENTE", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("CCI", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("", Font8))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("S/.", Font8))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCIS", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)

        Col = New PdfPCell(New Phrase("BCP", Font7))
        Col.Border = 0
        Col.BorderWidthLeft = 0.5
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase("USD.", Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCD", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CCID", General), Font7))
        Col.Border = 0
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthRight = 0.5
        Col.BorderColorRight = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoCuenta.AddCell(Col)
        TbCodigoCuenta.AddCell(CVacio)
        PdfDoc.Add(TbCodigoCuenta)


#End Region
#Region "CodigoHas"
        Dim TbCodigoHas As PdfPTable = New PdfPTable(1)
        Dim WidthsCodigoHas As Single() = New Single() {16.0F}
        TbCodigoHas.WidthPercentage = 95
        TbCodigoHas.SetWidths(WidthsCodigoHas)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.Padding = 1
        Col.BorderWidthBottom = 0.5
        Col.BorderColorBottom = ColorFondo
        Col.HorizontalAlignment = 0
        TbCodigoHas.AddCell(Col)

        Col = New PdfPCell(New Phrase("UNA VEZ SALIDA LA MERCADERIA NO HAY LUGAR A RECLAMO NI DEVOLUCIÓN ", Font7))
        Col.Border = 0
        Col.Padding = 10
        Col.HorizontalAlignment = 1
        TbCodigoHas.AddCell(Col)
        PdfDoc.Add(TbCodigoHas)
#End Region
#Region "PiePagina"
        Dim TbPiePagina As PdfPTable = New PdfPTable(4)
        Dim WidthsPiePagina As Single() = New Single() {1.2F, 1.5F, 10.3F, 3.0F}
        TbPiePagina.WidthPercentage = 95
        TbPiePagina.SetWidths(WidthsPiePagina)
        Dim RutaPiePaginaImagen As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPiePaginaImagen")
        Dim RutaPiePaginaImagenBMP As iTextSharp.text.Image
        RutaPiePaginaImagenBMP = iTextSharp.text.Image.GetInstance(RutaPiePaginaImagen)
        RutaPiePaginaImagenBMP.ScaleToFit(500.0F, 520.0F)
        RutaPiePaginaImagenBMP.SpacingBefore = 20.0F
        RutaPiePaginaImagenBMP.SpacingAfter = 10.0F
        RutaPiePaginaImagenBMP.SetAbsolutePosition(50, 14)
        PdfDoc.Add(RutaPiePaginaImagenBMP)

        PdfDoc.Add(TbPiePagina)

        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_TA4_OrdenCompra_Local_EPNISAC(ByVal Codigo As Integer)

        Dim General As New DataTable
        Dim Detalle As New DataTable

        Dim Obj As New BLFacturacion
        General = Obj.ObtenerDatosGeneralesOrdenCompraLocalPdf(Codigo)
        Detalle = Obj.ObtenerDatosDetalleOrdenCompraLocalPdf(Codigo)

        Dim NombrePdf As String
        NombrePdf = ObtenerDato("Nombre", General) + ".pdf"
        Dim RutaEmpresa As String = ""
        Dim RutaOrdenCompraLocal As String = ConfigurationManager.AppSettings("RutaOrdenCompraLocal")
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim PdfDoc As New Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 30.0F, 20.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaOrdenCompraLocal + NombrePdf, FileMode.Create))
        Dim ColorNegro As New BaseColor(0, 0, 0)
        Dim ColorFondo As New BaseColor(41, 182, 246)
        Dim ColorBlanco As New BaseColor(255, 255, 255)
        Dim ColorCeldaDetalle As New BaseColor(229, 231, 233)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL, ColorNegro))
        Dim Font7NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.WHITE))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, ColorNegro))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, ColorNegro))
        Dim Font10NB As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE))

        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(""))
        CVacio.Border = 0
        PdfDoc.Open()

        Dim Col As PdfPCell

#Region "TablaLogo"
        Dim TablaLogo As PdfPTable = New PdfPTable(2)
        Dim WidthsLogo As Single() = New Single() {4.5F, 11.5F}
        TablaLogo.WidthPercentage = 95
        TablaLogo.SetWidths(WidthsLogo)
        Dim LogoUrl As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaLogo")
        Dim LogoBMP As iTextSharp.text.Image
        LogoBMP = iTextSharp.text.Image.GetInstance(LogoUrl)
        LogoBMP.ScaleToFit(150.0F, 180.0F)
        LogoBMP.SpacingBefore = 20.0F
        LogoBMP.SpacingAfter = 10.0F
        LogoBMP.SetAbsolutePosition(20, 720)
        PdfDoc.Add(LogoBMP)

        PdfDoc.Add(TablaLogo)

        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaCabeceraTitulo"
        Dim TablaCabeceraTitulo As PdfPTable = New PdfPTable(2)
        Dim WidthsCabeceraTitulo As Single() = New Single() {12.0F, 4.0F}
        TablaCabeceraTitulo.WidthPercentage = 95
        TablaCabeceraTitulo.SetWidths(WidthsCabeceraTitulo)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("ORDEN DE COMPRA N°: " & ObtenerDato("NumeroOrden", General), Font10NB))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("FECHA: " & ObtenerDato("FechaEmision", General), Font10NB))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        Col.BackgroundColor = ColorFondo
        TablaCabeceraTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.BackgroundColor = ColorFondo
        Col.HorizontalAlignment = 1
        TablaCabeceraTitulo.AddCell(Col)


        PdfDoc.Add(TablaCabeceraTitulo)
#End Region
#Region "TablaCabecera"
        Dim TablaCabecera As PdfPTable = New PdfPTable(2)
        Dim WidthsCabecera As Single() = New Single() {12.0F, 4.0F}
        TablaCabecera.WidthPercentage = 95
        TablaCabecera.SetWidths(WidthsCabecera)

        Col = New PdfPCell(New Phrase("PROVEEDOR", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("MONEDA", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DocumentoProveedor", General) & " - " & ObtenerDato("Proveedor", General), Font8))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("Moneda", General), Font8))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("DireccionProveedor", General), Font8))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font8))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase("CONTACTOS", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase("CONDICIÓN DE PAGO", Font8N))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("CorreoProveedor", General) & " - " & ObtenerDato("ContactosProveedor", General), Font8))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("CondicionPago", General), Font8))
        Col.Border = 0
        Col.BackgroundColor = ColorBlanco
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.PaddingBottom = 8
        TablaCabecera.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        TablaCabecera.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font7))
        Col.Border = 0
        Col.PaddingBottom = -7
        TablaCabecera.AddCell(Col)

        PdfDoc.Add(TablaCabecera)
#End Region
#Region "TablaDetalleTitulo"
        Dim TablaDetalleTitulo As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalleTitulo As Single() = New Single() {0.5F, 1.8F, 8.7F, 1.5F, 1.5F, 2.0F}
        TablaDetalleTitulo.WidthPercentage = 95
        TablaDetalleTitulo.SetWidths(WidthsDetalleTitulo)

        Col = New PdfPCell(New Phrase("N°", Font7NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBlanco
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("CÓDIGO", Font7NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBlanco
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("DESCRIPCIÓN", Font7NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBlanco
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("CANTIDAD", Font7NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBlanco
        Col.HorizontalAlignment = 1
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("PRECIO", Font7NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBlanco
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        Col = New PdfPCell(New Phrase("SUB TOTAL", Font7NB))
        Col.Border = 5
        Col.BackgroundColor = ColorFondo
        Col.BorderColor = ColorBlanco
        Col.HorizontalAlignment = 2
        TablaDetalleTitulo.AddCell(Col)

        PdfDoc.Add(TablaDetalleTitulo)
#End Region
#Region "TablaDetalle"
        Dim TablaDetalle As PdfPTable = New PdfPTable(6)
        Dim WidthsDetalle As Single() = New Single() {0.5F, 1.8F, 8.7F, 1.5F, 1.5F, 2.0F}
        TablaDetalle.WidthPercentage = 95
        TablaDetalle.SetWidths(WidthsDetalle)

        For i = 0 To Detalle.Rows.Count - 1

            Col = New PdfPCell(New Phrase(ObtenerDato("nNumOrd", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoProducto", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            Col.HorizontalAlignment = 1
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(ObtenerDato("Cantidad", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nValUni", Detalle, i), 6), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)

            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("nValVent", Detalle, i), 2), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            If i = Int(i / 2) * 2 Then
                Col.BackgroundColor = BaseColor.WHITE
            Else
                Col.BackgroundColor = ColorCeldaDetalle
            End If
            TablaDetalle.AddCell(Col)
        Next

        PdfDoc.Add(TablaDetalle)
        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))
#End Region
#Region "TablaTotales"
        Dim TablaTotales As PdfPTable = New PdfPTable(4)
        Dim WidthsTotales As Single() = New Single() {8.0F, 2.2F, 1.5F, 2.3F}
        TablaTotales.WidthPercentage = 95
        TablaTotales.SetWidths(WidthsTotales)

        Col = New PdfPCell(New Phrase("OBSERVACIONES", Font8N))
        Col.Border = 0
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthTop = 1
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaTotales.AddCell(Col)
        Col = New PdfPCell(New Phrase("SUB TOTAL", Font8N))
        Col.Border = 0
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthTop = 1
        Col.HorizontalAlignment = 1
        Col.PaddingBottom = 10
        Col.PaddingTop = 10
        TablaTotales.AddCell(Col)
        Col = New PdfPCell(New Phrase("IGV", Font8N))
        Col.Border = 0
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthTop = 1
        Col.HorizontalAlignment = 1
        Col.PaddingBottom = 10
        Col.PaddingTop = 10
        TablaTotales.AddCell(Col)
        Col = New PdfPCell(New Phrase("IMPORTE TOTAL", Font8N))
        Col.Border = 0
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.BorderColorTop = ColorFondo
        Col.BorderWidthTop = 1
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        Col.HorizontalAlignment = 2
        Col.PaddingBottom = 10
        Col.PaddingTop = 10
        TablaTotales.AddCell(Col)

        Col = New PdfPCell(New Phrase(ObtenerDato("Observaciones", General), Font7))
        Col.Border = 0
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaTotales.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("SubTotal", General), 2), Font8))
        Col.Border = 0
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.HorizontalAlignment = 1
        Col.PaddingBottom = 10
        Col.PaddingTop = 10
        TablaTotales.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Igv", General), 2), Font8))
        Col.Border = 0
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.HorizontalAlignment = 1
        Col.PaddingBottom = 10
        Col.PaddingTop = 10
        TablaTotales.AddCell(Col)
        Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Total", General), 2), Font8))
        Col.Border = 0
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        Col.HorizontalAlignment = 2
        Col.PaddingBottom = 10
        Col.PaddingTop = 10
        TablaTotales.AddCell(Col)


        PdfDoc.Add(TablaTotales)
#End Region
#Region "TablaDatos"
        Dim TablaDatos As PdfPTable = New PdfPTable(2)
        Dim WidthsDatos As Single() = New Single() {1.5F, 15.0F}
        TablaDatos.WidthPercentage = 95
        TablaDatos.SetWidths(WidthsDatos)

        Col = New PdfPCell(New Phrase("NOTA:", Font8N))
        Col.Border = 0
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("LA MERCADERÍA SE RECIBIRA CON GUÍA Y FACTURA - INDICAR O/C", Font8N))
        Col.Border = 0
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        TablaDatos.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8N))
        Col.Border = 0
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("Consignar los siguientes datos en la Guia y Factura Ruc: " & ObtenerDato("RucEmpresa", General) & " Razon Social: " & ObtenerDato("Empresa", General), Font8N))
        Col.Border = 0
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        TablaDatos.AddCell(Col)

        Col = New PdfPCell(New Phrase(" ", Font8N))
        Col.Border = 0
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.BorderColorLeft = ColorFondo
        Col.BorderWidthLeft = 1
        TablaDatos.AddCell(Col)
        Col = New PdfPCell(New Phrase("Domicilio Fiscal: " & ObtenerDato("DireccionEmpresa", General), Font8N))
        Col.Border = 0
        Col.BorderColorBottom = ColorFondo
        Col.BorderWidthBottom = 1
        Col.BorderColorRight = ColorFondo
        Col.BorderWidthRight = 1
        Col.PaddingBottom = 8
        TablaDatos.AddCell(Col)

        PdfDoc.Add(TablaDatos)
        PdfDoc.Close()
#End Region
    End Sub
    Sub CrearPdf_Ticket_Almacen(ByVal Codigo As Integer, ByVal Nombre As String)
        Dim Obj As New BLFacturacion
        Dim General As New DataTable
        Dim Detalle As New DataTable
        General = Obj.ObtenerDatosGeneralesPdfAlmacen(Codigo)
        Detalle = Obj.ObtenerDatosDetallePdfAlmacen(Codigo)

        Dim NombrePdf As String
        NombrePdf = Nombre + ".pdf"
        Dim RutaImpresionAlmacen As String = ""
        Dim RutaPdf As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaPdf = ConfigurationManager.AppSettings("RutaImpresionAlmacen1")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaPdf = ConfigurationManager.AppSettings("RutaImpresionAlmacen2")
        End If
        Dim PdfDoc As New Document(PageSize.A4, 0.0F, 0.0F, 10.0F, 0.0F)
        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(PdfDoc, New FileStream(RutaPdf + NombrePdf, FileMode.Create))
        Dim ColorLetraNegro As New BaseColor(0, 0, 0)
        Dim Font7 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.NORMAL, ColorLetraNegro))
        Dim Font7N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.BOLD, ColorLetraNegro))
        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, ColorLetraNegro))
        Dim Font8N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD, ColorLetraNegro))
        Dim Font10N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD, ColorLetraNegro))
        Dim Font12N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD, ColorLetraNegro))
        Dim Font12 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.NORMAL, ColorLetraNegro))
        Dim Font18N As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 18, Font.BOLD, ColorLetraNegro))

        Dim Contador As Integer
        Dim ContadorProductos As Integer
        Dim CVacio As PdfPCell = New PdfPCell(New Phrase(" "))
        CVacio.Border = 0
        PdfDoc.Open()
        Dim Col As PdfPCell
        Dim TbInformacion As PdfPTable
        Dim WidthsInformacion As Single()

#Region "Cuerpo"
        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("RazonSocial", General), Font18N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)

        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)

        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(ObtenerDato("TipoMovimiento", General), Font12N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)

        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)


        TbInformacion = New PdfPTable(5)
        WidthsInformacion = New Single() {5.05F, 2.0F, 0.2F, 3.7F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("N° MOVIMIENTO", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("cNumAlm", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("TIPO OPERACION", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("cMotTrasl", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("PUNTO SALIDA", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("AgenciaSalida", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("PUNTO INGRESO", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("AgenciaIngreso", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)

        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)


        TbInformacion = New PdfPTable(5)
        WidthsInformacion = New Single() {5.05F, 2.0F, 0.2F, 3.7F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("COMENTARIOS", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("cObservaciones", General), Font7))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)

        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)


        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("DETALLE DEL MOVIMIENTO", Font10N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)

        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)



        TbInformacion = New PdfPTable(7)
        WidthsInformacion = New Single() {5.05F, 0.5F, 1.4F, 2.6F, 0.6F, 0.8F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("N°", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase("Código", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase("Descripcion", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase("U/M", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase("Cant", Font7N))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)


        TbInformacion = New PdfPTable(7)
        WidthsInformacion = New Single() {5.05F, 0.5F, 1.4F, 2.6F, 0.6F, 0.8F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        Contador = Detalle.Rows.Count
        For i = 0 To Detalle.Rows.Count - 1
            TbInformacion.AddCell(CVacio)
            Col = New PdfPCell(New Phrase(ObtenerDato("item", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("CodigoCatalogo", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("Descripcion", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 0
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(ObtenerDato("UnidadMedida", Detalle, i), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 1
            TbInformacion.AddCell(Col)
            Col = New PdfPCell(New Phrase(FormatNumber(ObtenerDato("Cantidad", Detalle, i), 0), Font7))
            Col.Border = 0
            Col.HorizontalAlignment = 2
            TbInformacion.AddCell(Col)
            TbInformacion.AddCell(CVacio)
            ContadorProductos = ContadorProductos + ObtenerDato("Cantidad", Detalle, i)
        Next
        PdfDoc.Add(TbInformacion)

        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)




        TbInformacion = New PdfPTable(5)
        WidthsInformacion = New Single() {5.05F, 3.0F, 0.2F, 2.7F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("N° TOTAL ITEM ", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(Contador, Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("N° TOTAL PRODUCTOS", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ContadorProductos, Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)


        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)


        TbInformacion = New PdfPTable(5)
        WidthsInformacion = New Single() {5.05F, 3.0F, 0.2F, 2.7F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("USUARIO", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("Usuario", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("PERSONAL", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("Personal", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("FECHA", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("FechaEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("HORA", Font8N))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(":", Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(ObtenerDato("HoraEmision", General), Font8))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)


        TbInformacion = New PdfPTable(3)
        WidthsInformacion = New Single() {5.05F, 5.9F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)


        PdfDoc.Add(New Paragraph(" "))
        PdfDoc.Add(New Paragraph(" "))

        TbInformacion = New PdfPTable(7)
        WidthsInformacion = New Single() {5.05F, 0.59F, 0.59F, 3.54F, 0.59F, 0.59F, 5.05F}
        TbInformacion.WidthPercentage = 95
        TbInformacion.SetWidths(WidthsInformacion)
        TbInformacion.AddCell(CVacio)
        Col = New PdfPCell(New Phrase(" ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 0
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        Col.BorderWidthBottom = 0.5F
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 1
        TbInformacion.AddCell(Col)
        Col = New PdfPCell(New Phrase(" ", Font12))
        Col.Border = 0
        Col.HorizontalAlignment = 2
        TbInformacion.AddCell(Col)
        TbInformacion.AddCell(CVacio)
        PdfDoc.Add(TbInformacion)

        PdfDoc.Close()
#End Region
    End Sub
End Module
