﻿Imports Negocios
Imports Entities
Imports Business
Imports System.Windows.Forms
Imports System.Drawing.Imaging
Public Class FrmActivos
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Dim BEActivos As New BEActive
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion
    Dim Accion As Boolean = False
    Private Sub FrmActivos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call InterfaceNuevo()
            Call MostrarDatosActivos()
        End If
    End Sub
    Sub InicioActivos(ByVal oBEActivos As BEActive, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEActivos = oBEActivos
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosActivos()
        bNuevo = False
        txtDescripcion.Tag = BEActivos.nCodAct
        txtDescripcion.Text = BEActivos.cDescripcion
        cmbTipoActivo.SelectedValue = BEActivos.nTipAct
        cmbCondicion.SelectedValue = BEActivos.nConAct
    End Sub
    Sub Limpiar()
        bNuevo = True
        txtDescripcion.Tag = 0
        txtDescripcion.Clear()
    End Sub
    Sub InterfaceNuevo()
        bNuevo = True
        txtDescripcion.Enabled = True
        cmbTipoActivo.Enabled = True
        cmbCondicion.Enabled = True
        btnGrabar.Enabled = True
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("440,450")
        Call CargaCombo(CreaDatoCombos(dt, 440), cmbTipoActivo)
        Call CargaCombo(CreaDatoCombos(dt, 450), cmbCondicion)
    End Sub

    Private Sub txtDescripcion_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcion.TextChanged

    End Sub

    Private Sub txtDescripcion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If bNuevo = False Then
            Call ModificarActivo()
        Else
            Call InsertarActivo()
        End If

    End Sub
    Sub ModificarActivo()
        Dim oDatos As New BEActive
        cMensaje = ""
        oDatos.nCodAct = txtDescripcion.Tag
        oDatos.nConAct = cmbCondicion.SelectedValue
        oDatos.nTipAct = cmbTipoActivo.SelectedValue
        oDatos.cDescripcion = txtDescripcion.Text
        Dim obj As New BLActive
        If obj.Grabar(oDatos, bNuevo, 0, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("El activo se actualizó correctamente", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub InsertarActivo()
        Dim oDatos As New BEActive
        cMensaje = ""
        oDatos.nConAct = cmbCondicion.SelectedValue
        oDatos.nTipAct = cmbTipoActivo.SelectedValue
        oDatos.cDescripcion = txtDescripcion.Text
        Dim obj As New BLActive
        If obj.Grabar(oDatos, bNuevo, MDIPrincipal.CodigoEmpresa, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("El activo se registró correctamente", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub cmbTipoActivo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoActivo.SelectedIndexChanged

    End Sub

    Private Sub FrmActivos_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        End If
    End Sub

    Private Sub cmbCondicion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCondicion.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoActivo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoActivo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbCondicion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbCondicion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class