﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmActualizacionPrecio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbMarca = New System.Windows.Forms.ComboBox()
        Me.txtCadCot = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.Label()
        Me.txtPreListPEN = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPreCompPEN = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtPreCompUSD = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtStockMinimo = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPreListUSD = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDescuentoUnidad = New System.Windows.Forms.TextBox()
        Me.txtDescuentoMayor = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tlsTitulo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(576, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(179, 22)
        Me.lblTitulo.Text = "ACTUALIZACÓN DE PRECIOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(302, 37)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(83, 14)
        Me.Label17.TabIndex = 125
        Me.Label17.Text = "Cód.Catálogo:"
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(463, 199)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(101, 26)
        Me.btnGrabar.TabIndex = 6
        Me.btnGrabar.Text = "Grabar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 14)
        Me.Label1.TabIndex = 135
        Me.Label1.Text = "Descripción:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 14)
        Me.Label3.TabIndex = 137
        Me.Label3.Text = "Marca:"
        '
        'cmbMarca
        '
        Me.cmbMarca.BackColor = System.Drawing.Color.White
        Me.cmbMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarca.Enabled = False
        Me.cmbMarca.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMarca.FormattingEnabled = True
        Me.cmbMarca.Location = New System.Drawing.Point(138, 30)
        Me.cmbMarca.Name = "cmbMarca"
        Me.cmbMarca.Size = New System.Drawing.Size(143, 22)
        Me.cmbMarca.TabIndex = 0
        '
        'txtCadCot
        '
        Me.txtCadCot.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtCadCot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCadCot.Location = New System.Drawing.Point(393, 30)
        Me.txtCadCot.Name = "txtCadCot"
        Me.txtCadCot.Size = New System.Drawing.Size(171, 22)
        Me.txtCadCot.TabIndex = 148
        Me.txtCadCot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.Location = New System.Drawing.Point(138, 56)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(426, 22)
        Me.txtDescripcion.TabIndex = 149
        Me.txtDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPreListPEN
        '
        Me.txtPreListPEN.Location = New System.Drawing.Point(138, 133)
        Me.txtPreListPEN.Name = "txtPreListPEN"
        Me.txtPreListPEN.Size = New System.Drawing.Size(101, 22)
        Me.txtPreListPEN.TabIndex = 1
        Me.txtPreListPEN.Text = "0.000000"
        Me.txtPreListPEN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 141)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 14)
        Me.Label7.TabIndex = 151
        Me.Label7.Text = "Valor Unitario:"
        '
        'txtPreCompPEN
        '
        Me.txtPreCompPEN.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtPreCompPEN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPreCompPEN.Location = New System.Drawing.Point(138, 83)
        Me.txtPreCompPEN.Name = "txtPreCompPEN"
        Me.txtPreCompPEN.Size = New System.Drawing.Size(101, 22)
        Me.txtPreCompPEN.TabIndex = 155
        Me.txtPreCompPEN.Text = "0.000000"
        Me.txtPreCompPEN.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 91)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(115, 14)
        Me.Label8.TabIndex = 154
        Me.Label8.Text = "Precio Compra PEN:"
        '
        'txtPreCompUSD
        '
        Me.txtPreCompUSD.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtPreCompUSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPreCompUSD.Location = New System.Drawing.Point(138, 108)
        Me.txtPreCompUSD.Name = "txtPreCompUSD"
        Me.txtPreCompUSD.Size = New System.Drawing.Size(101, 22)
        Me.txtPreCompUSD.TabIndex = 160
        Me.txtPreCompUSD.Text = "0.000000"
        Me.txtPreCompUSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 116)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(116, 14)
        Me.Label4.TabIndex = 159
        Me.Label4.Text = "Precio Compra USD:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(370, 141)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 14)
        Me.Label2.TabIndex = 162
        Me.Label2.Text = "Stock Mínimo:"
        '
        'txtStockMinimo
        '
        Me.txtStockMinimo.Location = New System.Drawing.Point(463, 133)
        Me.txtStockMinimo.Name = "txtStockMinimo"
        Me.txtStockMinimo.Size = New System.Drawing.Size(101, 22)
        Me.txtStockMinimo.TabIndex = 5
        Me.txtStockMinimo.Text = "0"
        Me.txtStockMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 166)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 14)
        Me.Label5.TabIndex = 158
        Me.Label5.Text = "Precio Unitario:"
        '
        'txtPreListUSD
        '
        Me.txtPreListUSD.Location = New System.Drawing.Point(138, 158)
        Me.txtPreListUSD.Name = "txtPreListUSD"
        Me.txtPreListUSD.Size = New System.Drawing.Size(101, 22)
        Me.txtPreListUSD.TabIndex = 2
        Me.txtPreListUSD.Text = "0.000000"
        Me.txtPreListUSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(337, 116)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(116, 14)
        Me.Label6.TabIndex = 166
        Me.Label6.Text = "Descuento x Mayor:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(332, 91)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(121, 14)
        Me.Label9.TabIndex = 165
        Me.Label9.Text = "Descuento x Unidad:"
        '
        'txtDescuentoUnidad
        '
        Me.txtDescuentoUnidad.Location = New System.Drawing.Point(463, 83)
        Me.txtDescuentoUnidad.Name = "txtDescuentoUnidad"
        Me.txtDescuentoUnidad.Size = New System.Drawing.Size(82, 22)
        Me.txtDescuentoUnidad.TabIndex = 3
        Me.txtDescuentoUnidad.Text = "0.00000"
        Me.txtDescuentoUnidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDescuentoMayor
        '
        Me.txtDescuentoMayor.Location = New System.Drawing.Point(463, 108)
        Me.txtDescuentoMayor.Name = "txtDescuentoMayor"
        Me.txtDescuentoMayor.Size = New System.Drawing.Size(82, 22)
        Me.txtDescuentoMayor.TabIndex = 4
        Me.txtDescuentoMayor.Text = "0.00000"
        Me.txtDescuentoMayor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(547, 111)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(19, 14)
        Me.Label10.TabIndex = 167
        Me.Label10.Text = "%"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(547, 87)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(19, 14)
        Me.Label11.TabIndex = 168
        Me.Label11.Text = "%"
        '
        'FrmActualizacionPrecio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(576, 234)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.txtDescuentoMayor)
        Me.Controls.Add(Me.txtCadCot)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDescuentoUnidad)
        Me.Controls.Add(Me.cmbMarca)
        Me.Controls.Add(Me.txtStockMinimo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.txtPreCompUSD)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Controls.Add(Me.txtPreListPEN)
        Me.Controls.Add(Me.txtPreCompPEN)
        Me.Controls.Add(Me.txtPreListUSD)
        Me.Controls.Add(Me.Label8)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmActualizacionPrecio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnMover As Panel
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents Label17 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents cmbMarca As ComboBox
    Friend WithEvents txtCadCot As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtPreListPEN As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPreCompPEN As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtPreCompUSD As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtStockMinimo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtPreListUSD As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtDescuentoUnidad As TextBox
    Friend WithEvents txtDescuentoMayor As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
End Class
