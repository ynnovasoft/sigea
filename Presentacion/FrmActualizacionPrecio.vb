﻿Imports Negocios
Imports Entidades
Imports Business
Imports System.Windows.Forms
Public Class FrmActualizacionPrecio
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim Accion As Boolean = False
    Private Sub FrmActualizacionPrecio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
    End Sub
    Sub Inicio(ByVal nCodProd As Integer, ByVal cMarca As String, ByVal cCodCat As String, ByVal cDescripcion As String,
              ByVal nPreComPEN As Decimal, ByVal nPreComUSD As Decimal, ByVal nValUni As Decimal, ByVal nPreUni As Decimal,
               ByVal nStockMinimo As Integer, ByVal nDescuentoUnidad As Decimal, ByVal nDescuentoMayor As Decimal, ByRef bAccion As Boolean)
        txtCadCot.Tag = nCodProd
        cmbMarca.SelectedValue = cMarca
        txtCadCot.Text = cCodCat
        txtDescripcion.Text = cDescripcion
        txtPreCompPEN.Text = FormatNumber(nPreComPEN, 6)
        txtPreCompUSD.Text = FormatNumber(nPreComUSD, 6)
        txtPreListPEN.Text = FormatNumber(nValUni, 6)
        txtPreListUSD.Text = FormatNumber(nPreUni, 6)
        txtStockMinimo.Text = FormatNumber(nStockMinimo, 2)
        txtDescuentoMayor.Text = FormatNumber(nDescuentoMayor, 5)
        txtDescuentoUnidad.Text = FormatNumber(nDescuentoUnidad, 5)
        If MDIPrincipal.CodigoPrecioLista = 0 Then
            txtPreListPEN.Enabled = True
            txtPreListUSD.Enabled = False
        Else
            txtPreListPEN.Enabled = False
            txtPreListUSD.Enabled = True
        End If
        If MDIPrincipal.ActualizaPrecio = True Then
            ModalidadVista(True)
        Else
            ModalidadVista(False)
        End If
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub ModalidadVista(ByVal Valor As Boolean)
        If Valor = True Then
            If MDIPrincipal.CodigoPrecioLista = 0 Then
                txtPreListPEN.Enabled = Valor
                txtPreListUSD.Enabled = Not Valor
            Else
                txtPreListPEN.Enabled = Not Valor
                txtPreListUSD.Enabled = Valor
            End If
        Else
            txtPreListPEN.Enabled = Valor
            txtPreListUSD.Enabled = Valor
        End If
        txtStockMinimo.Enabled = Valor
        txtDescuentoMayor.Enabled = Valor
        txtDescuentoUnidad.Enabled = Valor
        btnGrabar.Enabled = Valor
    End Sub

    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("130")
        Call CargaCombo(CreaDatoCombos(dt, 130), cmbMarca)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Call ModificarProducto()
    End Sub
    Sub ModificarProducto()
        Dim oDatos As New BEProductos
        Dim BLProductos As New BLProducts
        cMensaje = ""
        oDatos.gnCodProd = txtCadCot.Tag
        oDatos.gnValUni = txtPreListPEN.Text
        oDatos.gnPreUni = txtPreListUSD.Text
        oDatos.gnStockMinimo = txtStockMinimo.Text
        oDatos.gnMonDesUni = txtDescuentoUnidad.Text
        oDatos.gnMonDesMay = txtDescuentoMayor.Text
        If BLProductos.GrabarProductoPrecio(oDatos, MDIPrincipal.CodigoEmpresa, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se actualizó correctamente los precios", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub txtPreListPEN_GotFocus(sender As Object, e As EventArgs) Handles txtPreListPEN.GotFocus
        Call ValidaFormatoMonedaGot(txtPreListPEN, 6)
    End Sub

    Private Sub txtPreListPEN_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPreListPEN.KeyPress
        Call ValidaDecimales(e, txtPreListPEN)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtPreListPEN_LostFocus(sender As Object, e As EventArgs) Handles txtPreListPEN.LostFocus
        Call ValidaFormatoMonedaLost(txtPreListPEN, 6)
    End Sub

    Private Sub txtPreListPEN_TextChanged(sender As Object, e As EventArgs) Handles txtPreListPEN.TextChanged

    End Sub

    Private Sub txtPreListUSD_GotFocus(sender As Object, e As EventArgs) Handles txtPreListUSD.GotFocus
        Call ValidaFormatoMonedaGot(txtPreListUSD, 6)
    End Sub

    Private Sub txtPreListUSD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPreListUSD.KeyPress
        Call ValidaDecimales(e, txtPreListUSD)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtPreListUSD_LostFocus(sender As Object, e As EventArgs) Handles txtPreListUSD.LostFocus
        Call ValidaFormatoMonedaLost(txtPreListUSD, 6)
    End Sub

    Private Sub txtPreListUSD_TextChanged(sender As Object, e As EventArgs) Handles txtPreListUSD.TextChanged

    End Sub

    Private Sub txtStockMinimo_GotFocus(sender As Object, e As EventArgs) Handles txtStockMinimo.GotFocus
        Call ValidaFormatoMonedaGot(txtStockMinimo, 0)
    End Sub

    Private Sub txtStockMinimo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtStockMinimo.KeyPress
        Call ValidaDecimales(e, txtPreListUSD)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtStockMinimo_LostFocus(sender As Object, e As EventArgs) Handles txtStockMinimo.LostFocus
        Call ValidaFormatoMonedaLost(txtStockMinimo, 0)
    End Sub

    Private Sub cmbMarca_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMarca.SelectedIndexChanged

    End Sub

    Private Sub txtStockMinimo_TextChanged(sender As Object, e As EventArgs) Handles txtStockMinimo.TextChanged

    End Sub

    Private Sub cmbMarca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMarca.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtPreListPEN_Leave(sender As Object, e As EventArgs) Handles txtPreListPEN.Leave
        txtPreListUSD.Text = FormatNumber(IIf(txtPreListPEN.Text = "", 0, txtPreListPEN.Text) * 1.18, 6)
    End Sub

    Private Sub txtPreListUSD_Leave(sender As Object, e As EventArgs) Handles txtPreListUSD.Leave
        txtPreListPEN.Text = FormatNumber(IIf(txtPreListUSD.Text = "", 0, txtPreListUSD.Text) / 1.18, 6)
    End Sub

    Private Sub txtDescuentoUnidad_TextChanged(sender As Object, e As EventArgs) Handles txtDescuentoUnidad.TextChanged

    End Sub

    Private Sub FrmActualizacionPrecio_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub txtDescuentoMayor_TextChanged(sender As Object, e As EventArgs) Handles txtDescuentoMayor.TextChanged

    End Sub

    Private Sub txtDescuentoUnidad_GotFocus(sender As Object, e As EventArgs) Handles txtDescuentoUnidad.GotFocus
        Call ValidaFormatoMonedaGot(txtDescuentoUnidad, 5)
    End Sub

    Private Sub txtDescuentoMayor_GotFocus(sender As Object, e As EventArgs) Handles txtDescuentoMayor.GotFocus
        Call ValidaFormatoMonedaGot(txtDescuentoMayor, 5)
    End Sub

    Private Sub txtDescuentoUnidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescuentoUnidad.KeyPress
        Call ValidaDecimales(e, txtDescuentoUnidad)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtDescuentoMayor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescuentoMayor.KeyPress
        Call ValidaDecimales(e, txtDescuentoMayor)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtDescuentoUnidad_LostFocus(sender As Object, e As EventArgs) Handles txtDescuentoUnidad.LostFocus
        Call ValidaFormatoMonedaLost(txtDescuentoUnidad, 5)
    End Sub

    Private Sub txtDescuentoMayor_LostFocus(sender As Object, e As EventArgs) Handles txtDescuentoMayor.LostFocus
        Call ValidaFormatoMonedaLost(txtDescuentoMayor, 5)
    End Sub
End Class