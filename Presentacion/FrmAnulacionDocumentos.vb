﻿Imports Negocios
Imports Business
Imports Entidades
Imports Entities
Public Class FrmAnulacionDocumentos
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Dim FechaRegistro As Date
    Private Sub FrmAnulacionDocumentos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaCombos()
        Call Limpiar()
        Call InterfaceEntrada()
        btnExaminar.Focus()
    End Sub
    Sub Limpiar()
        txtNumDoc.Text = ""
        txtObservaciones.Text = ""
        txtNumDoc.Tag = 0
    End Sub
    Sub InterfaceEntrada()
        cmbTipComprobante.Enabled = True
        btnAnular.Enabled = False
        txtObservaciones.Enabled = False
        btnExaminar.Enabled = True
    End Sub
    Sub CargaCombos()
        Dim dt = New DataTable()
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(30, "20,50")
        Call CargaCombo(dt, cmbTipComprobante)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        If txtObservaciones.Text.Trim = "" Then
            MsgBox("Debe registrar una observación o motivo de anulación", MsgBoxStyle.Exclamation, "MENSAJE")
            txtObservaciones.Focus()
            Exit Sub
        End If
        If cmbTipComprobante.SelectedValue = "20" Then
            Dim Obj As New BLCommons
            Obj.ValidaEstadoMesContable(txtNumDoc.Tag, FechaRegistro, 8, cMensaje)
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If

            Dim oDatos As New BERetencion
            Dim BLGuia As New BLRetencion
            cMensaje = ""
            oDatos.gnCodRet = txtNumDoc.Tag
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcMotAnu = txtObservaciones.Text

            If BLGuia.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("Se anuló correctamente la retención", MsgBoxStyle.Information, "MENSAJE")
                Call Limpiar()
                Call InterfaceEntrada()
                btnExaminar.Focus()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        ElseIf cmbTipComprobante.SelectedValue = "50" Then
            Dim Obj As New BLCommons
            Obj.ValidaEstadoMesContable(txtNumDoc.Tag, FechaRegistro, 7, cMensaje)
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If

            Dim oDatos As New BELetras
            Dim BLLetras As New BLLetras
            cMensaje = ""
            oDatos.gnCodLet = txtNumDoc.Tag
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcMotAnu = txtObservaciones.Text

            If BLLetras.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("Se anuló correctamente la letra", MsgBoxStyle.Information, "MENSAJE")
                Call Limpiar()
                Call InterfaceEntrada()
                btnExaminar.Focus()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub cmbTipComprobante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipComprobante.SelectedIndexChanged

    End Sub

    Private Sub btnExaminar_Click(sender As Object, e As EventArgs) Handles btnExaminar.Click
        Dim Codigo As Integer = 0
        Dim NumDoc As String = ""
        If cmbTipComprobante.SelectedValue = "20" Then
            FrmConsultaRetencion.InicioAnulacion(Codigo, NumDoc, FechaRegistro)
            FrmConsultaRetencion = Nothing
            If Codigo <> 0 Then
                txtNumDoc.Tag = Codigo
                txtNumDoc.Text = NumDoc
                btnAnular.Enabled = True
                txtObservaciones.Enabled = True
                txtObservaciones.Focus()
            End If
        ElseIf cmbTipComprobante.SelectedValue = "50" Then
            FrmConsultaLetras.InicioAnulacion(Codigo, NumDoc, FechaRegistro)
            FrmConsultaLetras = Nothing
            If Codigo <> 0 Then
                txtNumDoc.Tag = Codigo
                txtNumDoc.Text = NumDoc
                btnAnular.Enabled = True
                txtObservaciones.Enabled = True
                txtObservaciones.Focus()
            End If
        End If
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub cmbTipComprobante_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipComprobante.SelectionChangeCommitted
        txtNumDoc.Tag = 0
        txtNumDoc.Text = ""
        btnAnular.Enabled = False
        btnExaminar.Focus()
    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipComprobante.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnExaminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnExaminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class