﻿Imports Negocios
Imports Business
Imports Entidades
Imports System.Configuration
Imports System.Windows.Forms
Imports System.Drawing.Imaging
Imports iTextSharp.text.pdf
Public Class FrmAperturaCierreContable
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Private Sub FrmAperturaCierreContable_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaAños()
    End Sub

    Sub CargaAños()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarAñoContable(MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbAño)
    End Sub
    Sub ListarMesesContables()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMesContable(cmbAño.SelectedValue, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
        If dtgListado.Rows.Count > 0 Then
            btnGrabar.Enabled = True
        Else
            btnGrabar.Enabled = False
        End If
    End Sub
    Private Sub cmbAño_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAño.SelectedIndexChanged

    End Sub

    Private Sub FrmAperturaCierreContable_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub cmbAño_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbAño.SelectedValueChanged
        If (TypeOf cmbAño.SelectedValue IsNot DataRowView) Then
            Call ListarMesesContables()
        End If
    End Sub

    Private Sub cmbAño_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbAño.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Dim MensajePregunta = ""
        If dtgListado.CurrentRow.Cells("bEstado").Value = True Then
            MensajePregunta = "¿Esta seguro que desea: CERRAR el mes de: " + dtgListado.CurrentRow.Cells("cMes").Value + " ?"
        Else
            MensajePregunta = "¿Esta seguro que desea: APERTURAR el mes de: " + dtgListado.CurrentRow.Cells("cMes").Value + " ?"
        End If
        If MsgBox(MensajePregunta, MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
            cMensaje = ""
            Dim Obj As New BLCommons
            If Obj.AperturaCierreContable(MDIPrincipal.CodigoEmpresa, cmbAño.SelectedValue, dtgListado.CurrentRow.Cells("nMesCon").Value,
                          dtgListado.CurrentRow.Cells("bEstado").Value, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Call ListarMesesContables()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown

    End Sub

    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs) Handles dtgListado.SelectionChanged
        If dtgListado.Rows.Count > 0 Then
            btnGrabar.Enabled = True
        Else
            btnGrabar.Enabled = False
        End If
    End Sub
End Class