﻿Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Public Class FrmAperturaCierreDia
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Private Sub FrmAperturaCierreDia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        btnVerInforme.Enabled = False
        dtpFechaRegistro.Enabled = True
        txtTipoCambioC.Enabled = True
        txtTipoCambioV.Enabled = True
        btnGrabar.Select()
    End Sub
    Sub ConsultaTipoCambio()
        Dim Cliente As New BECustomer
        Dim Datos As New Entities.BETipoCambio
        cMensaje = ConsultarTipoCambio(Datos, Format(CDate(dtpFechaRegistro.Value), "yyyy-MM-dd"))
        If cMensaje = "" Then
            txtTipoCambioV.Text = FormatNumber(Datos.precioVenta, 4)
            txtTipoCambioC.Text = FormatNumber(Datos.precioCompra, 4)
        Else
            txtTipoCambioV.Text = "0.0000"
            txtTipoCambioC.Text = "0.0000"
        End If
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("320")
        Call CargaCombo(CreaDatoCombos(dt, 320), cmbTipOpe)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged
        Call ConsultaTipoCambio()
    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Function ValidaDatos() As Boolean
        ValidaDatos = True
        If CDbl(IIf(txtTipoCambioV.Text = "", 0, txtTipoCambioV.Text)) = 0 Then
            MsgBox("Debe registrar el tipo de cambio de venta ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtTipoCambioV.Focus()
            Exit Function
        End If
        If CDbl(IIf(txtTipoCambioC.Text = "", 0, txtTipoCambioC.Text)) = 0 Then
            MsgBox("Debe registrar el tipo de cambio de compra ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtTipoCambioC.Focus()
            Exit Function
        End If
    End Function
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If cmbTipOpe.SelectedValue = 1 Then
            If ValidaDatos() = False Then
                Exit Sub
            End If
        End If
        Dim oDatos As New BECaja
        cMensaje = ""
        oDatos.gdFecha = dtpFechaRegistro.Value
        oDatos.gnTipCamVen = txtTipoCambioV.Text
        oDatos.gnTipCamCom = txtTipoCambioC.Text
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gbAperturado = IIf(cmbTipOpe.SelectedValue = 1, True, False)
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        Dim BLCaja As New BLCaja
        If BLCaja.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se registró correctamente la operación", MsgBoxStyle.Information, "MENSAJE")
            Call MDIPrincipal.MDIPrincipal_Load(sender, e)
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub txtTipoCambioC_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambioC.TextChanged

    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTipoCambioV_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambioV.TextChanged

    End Sub

    Private Sub txtTipoCambioC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambioC.KeyPress
        Call ValidaDecimales(e, txtTipoCambioC)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTipoCambioV_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambioV.KeyPress
        Call ValidaDecimales(e, txtTipoCambioV)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTipoCambioC_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambioC.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambioC, 4)
    End Sub

    Private Sub txtTipoCambioC_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambioC.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambioC, 4)
    End Sub

    Private Sub txtTipoCambioV_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambioV.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambioV, 4)
    End Sub

    Private Sub cmbTipOpe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipOpe.SelectedIndexChanged

    End Sub

    Private Sub txtTipoCambioV_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambioV.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambioV, 4)
    End Sub

    Private Sub cmbTipOpe_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipOpe.SelectionChangeCommitted

    End Sub

    Private Sub btnVerInforme_Click(sender As Object, e As EventArgs) Handles btnVerInforme.Click
        FrmReportes.ReporteOperacionesCaja("", 0, MDIPrincipal.FechaSistema, "CIERRE Y LIQUIDACIÓN DE DÍA")
        FrmReportes = Nothing
    End Sub

    Private Sub cmbTipOpe_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipOpe.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnVerInforme_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnVerInforme.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub FrmAperturaCierreDia_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        End If
    End Sub

    Private Sub cmbTipOpe_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbTipOpe.SelectedValueChanged
        If (TypeOf cmbTipOpe.SelectedValue IsNot DataRowView) Then
            txtTipoCambioV.Text = "0.0000"
            txtTipoCambioC.Text = "0.0000"
            If cmbTipOpe.SelectedValue = 1 Then
                btnVerInforme.Enabled = False
                dtpFechaRegistro.Enabled = True
                txtTipoCambioC.Enabled = True
                txtTipoCambioV.Enabled = True
                dtpFechaRegistro.Value = Now.Date()
                txtTipoCambioV.Select()
            Else
                btnVerInforme.Enabled = True
                dtpFechaRegistro.Enabled = False
                txtTipoCambioC.Enabled = False
                txtTipoCambioV.Enabled = False
                dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
                btnVerInforme.Select()
            End If
        End If
    End Sub
End Class