﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCierreOperacionesCaja
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.dtpdFecReg = New System.Windows.Forms.Label()
        Me.btnVerInforme = New System.Windows.Forms.Button()
        Me.txtUsuario = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCodUsu = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbTipOpe = New System.Windows.Forms.ComboBox()
        Me.txtAgencia = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMontoApertura = New System.Windows.Forms.TextBox()
        Me.tlsTitulo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(456, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(154, 22)
        Me.lblTitulo.Text = "OPERACIONES DE  CAJA"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(9, 40)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(43, 14)
        Me.Label17.TabIndex = 125
        Me.Label17.Text = "Fecha:"
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(346, 152)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(101, 26)
        Me.btnGrabar.TabIndex = 3
        Me.btnGrabar.Text = "Grabar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'dtpdFecReg
        '
        Me.dtpdFecReg.BackColor = System.Drawing.SystemColors.Info
        Me.dtpdFecReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.dtpdFecReg.Location = New System.Drawing.Point(71, 32)
        Me.dtpdFecReg.Name = "dtpdFecReg"
        Me.dtpdFecReg.Size = New System.Drawing.Size(128, 22)
        Me.dtpdFecReg.TabIndex = 148
        Me.dtpdFecReg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnVerInforme
        '
        Me.btnVerInforme.FlatAppearance.BorderSize = 0
        Me.btnVerInforme.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVerInforme.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVerInforme.Image = Global.Presentacion.My.Resources.Resources.Print_22
        Me.btnVerInforme.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVerInforme.Location = New System.Drawing.Point(12, 152)
        Me.btnVerInforme.Name = "btnVerInforme"
        Me.btnVerInforme.Size = New System.Drawing.Size(109, 26)
        Me.btnVerInforme.TabIndex = 2
        Me.btnVerInforme.Text = "Ver Informe"
        Me.btnVerInforme.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnVerInforme.UseVisualStyleBackColor = True
        '
        'txtUsuario
        '
        Me.txtUsuario.BackColor = System.Drawing.SystemColors.Info
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuario.Location = New System.Drawing.Point(71, 56)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(376, 22)
        Me.txtUsuario.TabIndex = 177
        Me.txtUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 14)
        Me.Label2.TabIndex = 176
        Me.Label2.Text = "Usuario:"
        '
        'txtCodUsu
        '
        Me.txtCodUsu.BackColor = System.Drawing.SystemColors.Info
        Me.txtCodUsu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodUsu.Location = New System.Drawing.Point(304, 32)
        Me.txtCodUsu.Name = "txtCodUsu"
        Me.txtCodUsu.Size = New System.Drawing.Size(143, 22)
        Me.txtCodUsu.TabIndex = 179
        Me.txtCodUsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(226, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 14)
        Me.Label4.TabIndex = 178
        Me.Label4.Text = "Cod.Usuario:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 14)
        Me.Label5.TabIndex = 181
        Me.Label5.Text = "Tip.Ope:"
        '
        'cmbTipOpe
        '
        Me.cmbTipOpe.BackColor = System.Drawing.Color.White
        Me.cmbTipOpe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipOpe.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipOpe.FormattingEnabled = True
        Me.cmbTipOpe.Location = New System.Drawing.Point(71, 80)
        Me.cmbTipOpe.Name = "cmbTipOpe"
        Me.cmbTipOpe.Size = New System.Drawing.Size(128, 22)
        Me.cmbTipOpe.TabIndex = 0
        '
        'txtAgencia
        '
        Me.txtAgencia.BackColor = System.Drawing.SystemColors.Info
        Me.txtAgencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAgencia.Location = New System.Drawing.Point(304, 80)
        Me.txtAgencia.Name = "txtAgencia"
        Me.txtAgencia.Size = New System.Drawing.Size(143, 22)
        Me.txtAgencia.TabIndex = 183
        Me.txtAgencia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(246, 86)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 14)
        Me.Label3.TabIndex = 182
        Me.Label3.Text = "Sucursal:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(203, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 14)
        Me.Label1.TabIndex = 185
        Me.Label1.Text = "Monto Apertura:"
        '
        'txtMontoApertura
        '
        Me.txtMontoApertura.Location = New System.Drawing.Point(304, 104)
        Me.txtMontoApertura.Name = "txtMontoApertura"
        Me.txtMontoApertura.Size = New System.Drawing.Size(143, 22)
        Me.txtMontoApertura.TabIndex = 1
        Me.txtMontoApertura.Text = "0.00"
        Me.txtMontoApertura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'FrmCierreOperacionesCaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(456, 192)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtMontoApertura)
        Me.Controls.Add(Me.txtAgencia)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cmbTipOpe)
        Me.Controls.Add(Me.txtCodUsu)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnVerInforme)
        Me.Controls.Add(Me.dtpdFecReg)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCierreOperacionesCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnMover As Panel
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents Label17 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents dtpdFecReg As System.Windows.Forms.Label
    Friend WithEvents btnVerInforme As Button
    Friend WithEvents txtUsuario As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtCodUsu As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents cmbTipOpe As ComboBox
    Friend WithEvents txtAgencia As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtMontoApertura As TextBox
End Class
