﻿Imports Negocios
Imports Entidades
Imports Business
Public Class FrmCierreOperacionesCaja
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Private Sub FrmCierreOperacionesCaja_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        dtpdFecReg.Text = MDIPrincipal.FechaSistema
        txtCodUsu.Text = MDIPrincipal.CodUsuario
        txtUsuario.Text = MDIPrincipal.Usuario
        txtAgencia.Text = MDIPrincipal.Agencia
        btnVerInforme.Enabled = False
        txtMontoApertura.Enabled = True
        txtMontoApertura.Text = FormatNumber(MDIPrincipal.MontoApertura, 2)
        Call CargaCombos()
        txtMontoApertura.Select()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("320")
        Call CargaCombo(CreaDatoCombos(dt, 320), cmbTipOpe)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Dim oDatos As New BECaja
        cMensaje = ""
        oDatos.gdFecha = MDIPrincipal.FechaSistema
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.gbAperturado = IIf(cmbTipOpe.SelectedValue = 1, True, False)
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.gnMonApe = IIf(txtMontoApertura.Text = "", 0, txtMontoApertura.Text)
        Dim BLCaja As New BLCaja
        If BLCaja.RegistrarOperaciones(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se registró correctamente", MsgBoxStyle.Information, "MENSAJE")
            Call MDIPrincipal.MDIPrincipal_Load(sender, e)
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub


    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub btnVerInforme_Click(sender As Object, e As EventArgs) Handles btnVerInforme.Click
        FrmReportes.ReporteOperacionesCaja(MDIPrincipal.CodUsuario, MDIPrincipal.CodigoAgencia,
                                           MDIPrincipal.FechaSistema, "CIERRE Y ARQUEO CAJA")
        FrmReportes = Nothing
    End Sub

    Private Sub cmbTipOpe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipOpe.SelectedIndexChanged

    End Sub

    Private Sub btnVerInforme_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnVerInforme.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipOpe_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipOpe.SelectionChangeCommitted
        If cmbTipOpe.SelectedValue = 1 Then
            btnVerInforme.Enabled = False
            txtMontoApertura.Enabled = True
            txtMontoApertura.Select()
        Else
            btnVerInforme.Enabled = True
            txtMontoApertura.Enabled = False
            btnVerInforme.Select()
        End If
    End Sub

    Private Sub txtMontoApertura_TextChanged(sender As Object, e As EventArgs) Handles txtMontoApertura.TextChanged

    End Sub

    Private Sub cmbTipOpe_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipOpe.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMontoApertura_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMontoApertura.KeyPress
        Call ValidaDecimales(e, txtMontoApertura)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMontoApertura_GotFocus(sender As Object, e As EventArgs) Handles txtMontoApertura.GotFocus
        Call ValidaFormatoMonedaGot(txtMontoApertura)
    End Sub

    Private Sub txtMontoApertura_LostFocus(sender As Object, e As EventArgs) Handles txtMontoApertura.LostFocus
        Call ValidaFormatoMonedaLost(txtMontoApertura)
    End Sub

    Private Sub FrmCierreOperacionesCaja_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        End If
    End Sub

    Private Sub cmbTipOpe_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbTipOpe.SelectedValueChanged
        If (TypeOf cmbTipOpe.SelectedValue IsNot DataRowView) Then
            If cmbTipOpe.SelectedValue = 1 Then
                btnVerInforme.Enabled = False
                txtMontoApertura.Enabled = True
                txtMontoApertura.SelectAll()
            Else
                btnVerInforme.Enabled = True
                txtMontoApertura.Enabled = False
                btnVerInforme.Select()
            End If
        End If
    End Sub
End Class