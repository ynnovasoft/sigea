﻿Imports Negocios
Imports Entities
Imports Business
Public Class FrmClientes
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Dim BEClientes As New BECustomer
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion
    Dim Accion As Boolean = False
    Private Sub FrmClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call CargaCombos()
            Call CargaDepartamento()
            Call CargaProvincia()
            Call CargaDistrito()
            Call CargaVendedores()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call CargaCombos()
            Call CargaDepartamento()
            Call CargaVendedores()
            Call MostrarDatosCliente()
        End If
    End Sub
    Sub InicioCliente(ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosCliente()
        bNuevo = False
        txtNumDocumento.Tag = BEClientes.nCodCli
        txtNumDocumento.Text = BEClientes.cNumDoc
        txtRazonSocial.Text = BEClientes.cRazSoc
        txtDireccion.Text = BEClientes.cDirCli
        txtCorreo.Text = BEClientes.cCorreo
        txtContactos.Text = BEClientes.cContactos
        txtApelPat.Text = BEClientes.cApePat
        txtApelMat.Text = BEClientes.cApeMat
        txtNombres.Text = BEClientes.cNombres
        chbRetencion.Checked = BEClientes.bAgeRet
        cmbTipoCliente.SelectedValue = BEClientes.nTipCli
        cmbVendedor.SelectedValue = BEClientes.nCodVen
        cmbTipoPersona.SelectedValue = BEClientes.nTipPer
        cmbTipDocumento.SelectedValue = BEClientes.cTipDocCli
        cmbDepartamento.SelectedValue = BEClientes.cCodUbiDep
        Call CargaProvincia()
        cmbProvincia.SelectedValue = BEClientes.cCodUbiProv
        Call CargaDistrito()
        cmbDistrito.SelectedValue = BEClientes.cCodUbiDis

        If cmbTipoPersona.SelectedValue = 1 Then
            txtApelPat.Enabled = True
            txtApelMat.Enabled = True
            txtNombres.Enabled = True
            txtRazonSocial.Enabled = False
        Else
            txtApelPat.Enabled = False
            txtApelMat.Enabled = False
            txtNombres.Enabled = False
            txtRazonSocial.Enabled = True
        End If
        cmbTipoPersona.Select()

    End Sub
    Sub Limpiar()
        bNuevo = True
        chbRetencion.Checked = False
        cmbTipoPersona.SelectedValue = 1
        cmbTipDocumento.SelectedValue = 1
        cmbTipoCliente.SelectedValue = 0
        txtNumDocumento.MaxLength = 8
        txtApelMat.Clear()
        txtApelPat.Clear()
        txtNombres.Clear()
        txtContactos.Clear()
        txtCorreo.Clear()
        txtDireccion.Clear()
        txtRazonSocial.Clear()
        txtNumDocumento.Clear()
    End Sub
    Sub InterfaceNuevo()
        chbRetencion.Enabled = True
        txtRazonSocial.Enabled = False
        txtNumDocumento.Enabled = True
        txtApelMat.Enabled = True
        txtApelPat.Enabled = True
        txtNombres.Enabled = True
        txtContactos.Enabled = True
        txtCorreo.Enabled = True
        txtDireccion.Enabled = True
        btnGrabar.Enabled = True
        cmbTipoPersona.Enabled = True
        cmbTipDocumento.Enabled = True
        cmbDepartamento.Enabled = True
        cmbProvincia.Enabled = True
        cmbDistrito.Enabled = True
        cmbTipoCliente.Enabled = True
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("20,230,460")
        Call CargaCombo(CreaDatoCombos(dt, 20), cmbTipDocumento)
        Call CargaCombo(CreaDatoCombos(dt, 230), cmbTipoPersona)
        Call CargaCombo(CreaDatoCombos(dt, 460), cmbTipoCliente)
    End Sub
    Sub CargaDepartamento()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarDepartamento()
        Call CargaCombo(dt, cmbDepartamento)
    End Sub
    Sub CargaProvincia()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarProvincia(IIf(cmbDepartamento.SelectedValue = Nothing, "", cmbDepartamento.SelectedValue))
        Call CargaCombo(dt, cmbProvincia)
    End Sub
    Sub CargaDistrito()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarDistrito(IIf(cmbProvincia.SelectedValue = Nothing, "", cmbProvincia.SelectedValue))
        Call CargaCombo(dt, cmbDistrito)
    End Sub
    Sub CargaVendedores()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarVendedores()
        Call CargaCombo(dt, cmbVendedor)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If txtNumDocumento.Text.Trim = "" Then
            MsgBox("Debe registrar el número de documento", MsgBoxStyle.Exclamation, "MENSAJE")
            txtNumDocumento.Focus()
            Exit Sub
        End If
        Dim Cantidad As Integer = txtNumDocumento.Text.Trim.Length
        If cmbTipDocumento.SelectedValue = 1 Then
            If Cantidad <> 8 Then
                MsgBox("La cantidad de caracteres deberia ser igual a OCHO(8)", MsgBoxStyle.Exclamation, "MENSAJE")
                txtNumDocumento.Focus()
                Exit Sub
            End If
        ElseIf cmbTipDocumento.SelectedValue = 6 Then
            If Cantidad <> 11 Then
                MsgBox("La cantidad de caracteres deberia ser igual a ONCE(11)", MsgBoxStyle.Exclamation, "MENSAJE")
                txtNumDocumento.Focus()
                Exit Sub
            End If
        End If
        If cmbTipoPersona.SelectedValue = 2 Then
            If txtRazonSocial.Text.Trim = "" Then
                MsgBox("Debe registrar la  razón social del cliente", MsgBoxStyle.Exclamation, "MENSAJE")
                txtRazonSocial.Focus()
                Exit Sub
            End If
        End If

        If bNuevo = False Then
            Call ModificarCliente()

        Else
            Call InsertarCliente()

        End If
    End Sub
    Sub ModificarCliente()
        Dim BLCliente As New BLCliente
        Dim oDatos As New BECustomer
        cMensaje = ""
        oDatos.nCodCli = txtNumDocumento.Tag
        oDatos.cApeMat = txtApelMat.Text
        oDatos.cApePat = txtApelPat.Text
        oDatos.cContactos = txtContactos.Text
        oDatos.cCorreo = txtCorreo.Text
        oDatos.cDirCli = txtDireccion.Text
        oDatos.cNombres = txtNombres.Text
        oDatos.cNumDoc = txtNumDocumento.Text.Trim
        oDatos.cRazSoc = txtRazonSocial.Text
        oDatos.cTipDocCli = cmbTipDocumento.SelectedValue
        oDatos.cCodUbiDep = IIf(cmbDepartamento.SelectedValue = Nothing, "", cmbDepartamento.SelectedValue)
        oDatos.cCodUbiDis = IIf(cmbDistrito.SelectedValue = Nothing, "", cmbDistrito.SelectedValue)
        oDatos.cCodUbiProv = IIf(cmbProvincia.SelectedValue = Nothing, "", cmbProvincia.SelectedValue)
        oDatos.nTipPer = cmbTipoPersona.SelectedValue
        oDatos.nTipPer = cmbTipoPersona.SelectedValue
        oDatos.bAgeRet = chbRetencion.Checked
        oDatos.nTipCli = cmbTipoCliente.SelectedValue
        oDatos.nCodVen = cmbVendedor.SelectedValue

        If BLCliente.Grabar(oDatos, False, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se modificó correctamente el cliente", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub InsertarCliente()
        Dim BLCliente As New BLCliente
        Dim oDatos As New BECustomer
        cMensaje = ""
        oDatos.cApeMat = txtApelMat.Text
        oDatos.cApePat = txtApelPat.Text
        oDatos.cContactos = txtContactos.Text
        oDatos.cCorreo = txtCorreo.Text
        oDatos.cDirCli = txtDireccion.Text
        oDatos.cNombres = txtNombres.Text
        oDatos.cNumDoc = txtNumDocumento.Text.Trim
        oDatos.cRazSoc = txtRazonSocial.Text
        oDatos.cTipDocCli = cmbTipDocumento.SelectedValue
        oDatos.cCodUbiDep = IIf(cmbDepartamento.SelectedValue = Nothing, "", cmbDepartamento.SelectedValue)
        oDatos.cCodUbiDis = IIf(cmbDistrito.SelectedValue = Nothing, "", cmbDistrito.SelectedValue)
        oDatos.cCodUbiProv = IIf(cmbProvincia.SelectedValue = Nothing, "", cmbProvincia.SelectedValue)
        oDatos.nTipPer = cmbTipoPersona.SelectedValue
        oDatos.bAgeRet = chbRetencion.Checked
        oDatos.nTipCli = cmbTipoCliente.SelectedValue
        oDatos.nCodVen = cmbVendedor.SelectedValue

        If BLCliente.Grabar(oDatos, True, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se registró correctamente el cliente", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub cmbTipoPersona_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoPersona.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoPersona_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoPersona.SelectedIndexChanged

    End Sub

    Private Sub cmbTipDocumento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipDocumento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipDocumento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipDocumento.SelectedIndexChanged

    End Sub

    Private Sub txtNumDocumento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumDocumento.KeyPress
        Call ValidaEspacio(e)
        Call ValidaNumeros(e)
        Call ValidaSonidoEnter(e)
        If e.KeyChar = Chr(13) Then
            Dim Cantidad As Integer = txtNumDocumento.Text.Trim.Length
            If cmbTipDocumento.SelectedValue = 1 Then
                If Cantidad <> 8 Then
                    MsgBox("La cantidad de caracteres deberia ser igual a OCHO(8)", MsgBoxStyle.Exclamation, "MENSAJE")
                    txtNumDocumento.Focus()
                    Exit Sub
                End If
            ElseIf cmbTipDocumento.SelectedValue = 6 Then
                If Cantidad <> 11 Then
                    MsgBox("La cantidad de caracteres deberia ser igual a ONCE(11)", MsgBoxStyle.Exclamation, "MENSAJE")
                    txtNumDocumento.Focus()
                    Exit Sub
                End If
            End If
            Dim Cliente As New BECustomer
            cMensaje = ConsultarCliente(Cliente, txtNumDocumento.Text.Trim, cmbTipDocumento.SelectedValue)
            If cMensaje = "" Then
                txtRazonSocial.Text = Cliente.cRazSoc
                txtDireccion.Text = Cliente.cDirCli
                If cmbTipDocumento.SelectedValue = 1 Then
                    txtApelPat.Text = Cliente.cApePat
                    txtApelMat.Text = Cliente.cApeMat
                    txtNombres.Text = Cliente.cNombres
                Else
                    chbRetencion.Checked = Cliente.bAgeRet
                End If
                cmbDepartamento.SelectedValue = Cliente.cCodUbiDep
                Call CargaProvincia()
                cmbProvincia.SelectedValue = Cliente.cCodUbiProv
                Call CargaDistrito()
                cmbDistrito.SelectedValue = Cliente.cCodUbiDis
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtNumDocumento_TextChanged(sender As Object, e As EventArgs) Handles txtNumDocumento.TextChanged

    End Sub

    Private Sub txtApelPat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtApelPat.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtApelPat_TextChanged(sender As Object, e As EventArgs) Handles txtApelPat.TextChanged

    End Sub

    Private Sub txtApelMat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtApelMat.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtApelMat_TextChanged(sender As Object, e As EventArgs) Handles txtApelMat.TextChanged

    End Sub

    Private Sub txtNombres_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombres.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtNombres_TextChanged(sender As Object, e As EventArgs) Handles txtNombres.TextChanged

    End Sub

    Private Sub txtRazonSocial_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRazonSocial.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtRazonSocial_TextChanged(sender As Object, e As EventArgs) Handles txtRazonSocial.TextChanged

    End Sub

    Private Sub txtContactos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtContactos.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtContactos_TextChanged(sender As Object, e As EventArgs) Handles txtContactos.TextChanged

    End Sub

    Private Sub txtDireccion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDireccion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtDireccion_TextChanged(sender As Object, e As EventArgs) Handles txtDireccion.TextChanged

    End Sub

    Private Sub txtCorreo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCorreo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtCorreo_TextChanged(sender As Object, e As EventArgs) Handles txtCorreo.TextChanged

    End Sub

    Private Sub cmbDepartamento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbDepartamento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbDepartamento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDepartamento.SelectedIndexChanged

    End Sub

    Private Sub cmbProvincia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbProvincia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbProvincia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbProvincia.SelectedIndexChanged

    End Sub

    Private Sub cmbDistrito_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbDistrito.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbDistrito_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDistrito.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoPersona_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoPersona.SelectionChangeCommitted

    End Sub

    Private Sub cmbDepartamento_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbDepartamento.SelectionChangeCommitted


    End Sub

    Private Sub cmbProvincia_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbProvincia.SelectionChangeCommitted

    End Sub

    Private Sub chbRetencion_CheckedChanged(sender As Object, e As EventArgs) Handles chbRetencion.CheckedChanged

    End Sub
    Private Sub chbRetencion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbRetencion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipDocumento_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipDocumento.SelectionChangeCommitted

    End Sub

    Private Sub txtApelPat_Leave(sender As Object, e As EventArgs) Handles txtApelPat.Leave

    End Sub

    Private Sub txtApelMat_Leave(sender As Object, e As EventArgs) Handles txtApelMat.Leave

    End Sub

    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click

    End Sub

    Private Sub Label14_Click(sender As Object, e As EventArgs) Handles Label14.Click

    End Sub

    Private Sub txtNombres_Leave(sender As Object, e As EventArgs) Handles txtNombres.Leave

    End Sub

    Private Sub FrmClientes_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        End If
    End Sub

    Private Sub cmbTipoPersona_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbTipoPersona.SelectedValueChanged
        If (TypeOf cmbTipoPersona.SelectedValue IsNot DataRowView) Then
            If cmbTipoPersona.SelectedValue = 1 Then
                txtApelPat.Enabled = True
                txtApelMat.Enabled = True
                txtNombres.Enabled = True
                txtRazonSocial.Enabled = False
                cmbTipDocumento.Enabled = True
                cmbTipDocumento.SelectedValue = 1
                txtNumDocumento.MaxLength = 8
            Else
                txtApelPat.Enabled = False
                txtApelMat.Enabled = False
                txtNombres.Enabled = False
                txtRazonSocial.Enabled = True
                cmbTipDocumento.Enabled = False
                cmbTipDocumento.SelectedValue = 6
                txtNumDocumento.MaxLength = 11
            End If
            If bNuevo = True Then
                txtNumDocumento.Clear()
                txtNombres.Clear()
                txtApelMat.Clear()
                txtApelPat.Clear()
                txtRazonSocial.Clear()
                txtDireccion.Clear()
                txtCorreo.Clear()
                txtContactos.Clear()
            End If
        End If
    End Sub

    Private Sub cmbTipDocumento_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbTipDocumento.SelectedValueChanged
        If (TypeOf cmbTipDocumento.SelectedValue IsNot DataRowView) Then
            If cmbTipDocumento.SelectedValue = "6" Then
                txtNumDocumento.MaxLength = 11
            ElseIf cmbTipDocumento.SelectedValue = "1" Then
                txtNumDocumento.MaxLength = 8
            ElseIf cmbTipDocumento.SelectedValue = "4" Or cmbTipDocumento.SelectedValue = "7" Then
                txtNumDocumento.MaxLength = 12
            End If
            If bNuevo = True Then
                txtNumDocumento.Text = ""
                txtNombres.Clear()
                txtApelMat.Clear()
                txtApelPat.Clear()
                txtRazonSocial.Clear()
            End If
            txtNumDocumento.Focus()
        End If
    End Sub

    Private Sub cmbProvincia_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbProvincia.SelectedValueChanged
        If (TypeOf cmbProvincia.SelectedValue IsNot DataRowView) Then
            Call CargaDistrito()
        End If
    End Sub

    Private Sub cmbDistrito_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbDistrito.SelectionChangeCommitted

    End Sub

    Private Sub cmbDistrito_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbDistrito.SelectedValueChanged

    End Sub

    Private Sub cmbVendedor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbVendedor.SelectedIndexChanged

    End Sub

    Private Sub cmbDepartamento_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbDepartamento.SelectedValueChanged
        If (TypeOf cmbDepartamento.SelectedValue IsNot DataRowView) Then
            Call CargaProvincia()
        End If
    End Sub

    Private Sub cmbVendedor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbVendedor.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class