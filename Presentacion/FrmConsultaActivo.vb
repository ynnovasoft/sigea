﻿Imports Business
Imports Negocios
Public Class FrmConsultaActivo
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim Codigo As Integer = 0
    Dim Tipo As String = ""
    Sub Inicio(ByVal pnCodigo As Integer, ByVal pnTipo As String)
        Codigo = pnCodigo
        Tipo = pnTipo
        Me.ShowDialog()
    End Sub
    Private Sub FrmConsultaActivo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call BuscarProductos()
        txtBuscar.Focus()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call BuscarProductos()
    End Sub
    Sub BuscarProductos()
        Dim dt As New DataTable
        Dim BLActive As New BLActive
        dt = BLActive.Listar(txtBuscar.Text, MDIPrincipal.CodigoAgencia,
                                                     MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            If Tipo = "80" Then
                Call AgregarDetalleCompras(dtgListado.CurrentRow.Cells("nCodAct").Value, 1, 0)
                FrmRegistroComprasServicios.RecuperarDetalle()
            ElseIf Tipo = "05" Then
                Call AgregarDetalleAlmacen(dtgListado.CurrentRow.Cells("nCodAct").Value, 1, 0)
                FrmParteSalidaServicios.RecuperarDetalle()
            ElseIf Tipo = "06" Then
                Call AgregarDetalleAlmacen(dtgListado.CurrentRow.Cells("nCodAct").Value, 1, 0)
                FrmParteIngresoServicios.RecuperarDetalle()
            End If
        End If
    End Sub
    Sub AgregarDetalleCompras(ByVal CodAct As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim Obj As New BLActive
        If Obj.AgregarActivoCompra(Codigo, CodAct, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub AgregarDetalleAlmacen(ByVal CodAct As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim Obj As New BLActive
        If Obj.AgregarActivoAlmacen(Codigo, CodAct, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                If Tipo = "80" Then
                    Call AgregarDetalleCompras(dtgListado.CurrentRow.Cells("nCodAct").Value, 1, 0)
                    FrmRegistroComprasServicios.RecuperarDetalle()
                ElseIf Tipo = "05" Then
                    Call AgregarDetalleAlmacen(dtgListado.CurrentRow.Cells("nCodAct").Value, 1, 0)
                    FrmParteSalidaServicios.RecuperarDetalle()
                ElseIf Tipo = "06" Then
                    Call AgregarDetalleAlmacen(dtgListado.CurrentRow.Cells("nCodAct").Value, 1, 0)
                    FrmParteIngresoServicios.RecuperarDetalle()
                End If
            End If
        End If
    End Sub

    Private Sub FrmConsultaActivo_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class