﻿Imports Negocios
Imports Entities
Imports Business
Public Class FrmConsultaAlmacenIngreso
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim BEAlmacen As New BEStore
    Dim BEClientes As New BECustomer
    Dim MotivoTraslado As String = ""
    Dim CodigoDestinoAgencia As Integer = 0
    Dim CodigoDestinoEmpresa As Integer = 0
    Sub InicioIngreso(ByRef oAlmacen As BEStore, ByRef oClientes As BECustomer)
        CodigoDestinoAgencia = MDIPrincipal.CodigoAgencia
        CodigoDestinoEmpresa = 0
        MotivoTraslado = "04"
        Me.ShowDialog()
        oAlmacen = BEAlmacen
        oClientes = BEClientes
    End Sub
    Sub InicioIngresoEmpreea(ByRef oAlmacen As BEStore, ByRef oClientes As BECustomer)
        CodigoDestinoAgencia = 0
        CodigoDestinoEmpresa = MDIPrincipal.CodigoEmpresa
        MotivoTraslado = "08"
        Me.ShowDialog()
        oAlmacen = BEAlmacen
        oClientes = BEClientes
    End Sub

    Private Sub FrmConsultaAlmacenIngreso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call ListarAlmacen()
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("210")
        Call CargaCombo(CreaDatoCombos(dt, 210), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call ListarAlmacen()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarAlmacen()
    End Sub
    Sub ListarAlmacen()
        Dim dt As DataTable
        Dim BLAlmacen As New BLAlmacen
        dt = BLAlmacen.ListarTraslados(txtBuscar.Text, cmbTipoBusqueda.SelectedValue, MDIPrincipal.CodigoAgencia,
                                       MotivoTraslado, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            BEAlmacen.nCodalm = dtgListado.CurrentRow.Cells("nCodalm").Value
            BEAlmacen.AgenciaOrigen = dtgListado.CurrentRow.Cells("AgenciaOrigen").Value
            BEAlmacen.EmpresaOrigen = dtgListado.CurrentRow.Cells("EmpresaOrigen").Value
            Me.Close()
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                BEAlmacen.nCodalm = dtgListado.CurrentRow.Cells("nCodalm").Value
                BEAlmacen.AgenciaOrigen = dtgListado.CurrentRow.Cells("AgenciaOrigen").Value
                BEAlmacen.EmpresaOrigen = dtgListado.CurrentRow.Cells("EmpresaOrigen").Value
                Me.Close()
            End If
        End If
    End Sub

    Private Sub FrmConsultaAlmacenIngreso_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class