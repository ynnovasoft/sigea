﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaAlmacenServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodalm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumAlm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipMov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMotivoTraslado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AgenciaOrigen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AgenciaDestino = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodigoAgencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodAgeDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodAgeOriIng = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMotTrasl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nVenGra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nIGV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMensaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipDocRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumDocRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cObservaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodigoDocRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodCliProv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDirCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFecIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnRefreescar = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnAnularMov = New System.Windows.Forms.Button()
        Me.btnNuevoMov = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbTipoMovimiento = New System.Windows.Forms.ComboBox()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(386, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL DE MOVIMIENTOS DE ALMACEN SERVICIOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodalm, Me.cNumAlm, Me.dFecReg, Me.cRazSoc, Me.cTipMov, Me.cMotivoTraslado, Me.AgenciaOrigen, Me.AgenciaDestino, Me.cEstado, Me.nCodigoAgencia, Me.nCodAgeDes, Me.nCodAgeOriIng, Me.cMotTrasl, Me.nTipCam, Me.nTipMon, Me.nEstado, Me.nVenGra, Me.nIGV, Me.nImpTot, Me.cMensaje, Me.cTipDocRef, Me.NumDocRef, Me.cObservaciones, Me.nCodigoDocRef, Me.nCodCliProv, Me.cNumDoc, Me.cDirCli, Me.nTipMov})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 68)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1322, 487)
        Me.dtgListado.TabIndex = 1
        '
        'nCodalm
        '
        Me.nCodalm.DataPropertyName = "nCodalm"
        Me.nCodalm.HeaderText = "nCodalm"
        Me.nCodalm.Name = "nCodalm"
        Me.nCodalm.ReadOnly = True
        Me.nCodalm.Visible = False
        '
        'cNumAlm
        '
        Me.cNumAlm.DataPropertyName = "cNumAlm"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumAlm.DefaultCellStyle = DataGridViewCellStyle15
        Me.cNumAlm.HeaderText = "N° Alm"
        Me.cNumAlm.Name = "cNumAlm"
        Me.cNumAlm.ReadOnly = True
        Me.cNumAlm.Width = 110
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle16
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.Width = 80
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle17
        Me.cRazSoc.HeaderText = "Cliente/Proveedor"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 250
        '
        'cTipMov
        '
        Me.cTipMov.DataPropertyName = "cTipMov"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cTipMov.DefaultCellStyle = DataGridViewCellStyle18
        Me.cTipMov.HeaderText = "Tipo Movimiento"
        Me.cTipMov.Name = "cTipMov"
        Me.cTipMov.ReadOnly = True
        Me.cTipMov.Width = 195
        '
        'cMotivoTraslado
        '
        Me.cMotivoTraslado.DataPropertyName = "cMotivoTraslado"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cMotivoTraslado.DefaultCellStyle = DataGridViewCellStyle19
        Me.cMotivoTraslado.HeaderText = "Motivo Traslado"
        Me.cMotivoTraslado.Name = "cMotivoTraslado"
        Me.cMotivoTraslado.ReadOnly = True
        Me.cMotivoTraslado.Width = 290
        '
        'AgenciaOrigen
        '
        Me.AgenciaOrigen.DataPropertyName = "AgenciaOrigen"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.AgenciaOrigen.DefaultCellStyle = DataGridViewCellStyle20
        Me.AgenciaOrigen.HeaderText = "Alm.Origen"
        Me.AgenciaOrigen.Name = "AgenciaOrigen"
        Me.AgenciaOrigen.ReadOnly = True
        Me.AgenciaOrigen.Width = 124
        '
        'AgenciaDestino
        '
        Me.AgenciaDestino.DataPropertyName = "AgenciaDestino"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.AgenciaDestino.DefaultCellStyle = DataGridViewCellStyle21
        Me.AgenciaDestino.HeaderText = "Alm.Destino"
        Me.AgenciaDestino.Name = "AgenciaDestino"
        Me.AgenciaDestino.ReadOnly = True
        Me.AgenciaDestino.Width = 125
        '
        'cEstado
        '
        Me.cEstado.DataPropertyName = "cEstado"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstado.DefaultCellStyle = DataGridViewCellStyle22
        Me.cEstado.HeaderText = "Estado"
        Me.cEstado.Name = "cEstado"
        Me.cEstado.ReadOnly = True
        Me.cEstado.Width = 123
        '
        'nCodigoAgencia
        '
        Me.nCodigoAgencia.DataPropertyName = "nCodigoAgencia"
        Me.nCodigoAgencia.HeaderText = "nCodigoAgencia"
        Me.nCodigoAgencia.Name = "nCodigoAgencia"
        Me.nCodigoAgencia.ReadOnly = True
        Me.nCodigoAgencia.Visible = False
        '
        'nCodAgeDes
        '
        Me.nCodAgeDes.DataPropertyName = "nCodAgeDes"
        Me.nCodAgeDes.HeaderText = "nCodAgeDes"
        Me.nCodAgeDes.Name = "nCodAgeDes"
        Me.nCodAgeDes.ReadOnly = True
        Me.nCodAgeDes.Visible = False
        '
        'nCodAgeOriIng
        '
        Me.nCodAgeOriIng.DataPropertyName = "nCodAgeOriIng"
        Me.nCodAgeOriIng.HeaderText = "nCodAgeOriIng"
        Me.nCodAgeOriIng.Name = "nCodAgeOriIng"
        Me.nCodAgeOriIng.ReadOnly = True
        Me.nCodAgeOriIng.Visible = False
        '
        'cMotTrasl
        '
        Me.cMotTrasl.DataPropertyName = "cMotTrasl"
        Me.cMotTrasl.HeaderText = "cMotTrasl"
        Me.cMotTrasl.Name = "cMotTrasl"
        Me.cMotTrasl.ReadOnly = True
        Me.cMotTrasl.Visible = False
        '
        'nTipCam
        '
        Me.nTipCam.DataPropertyName = "nTipCam"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle23.Format = "N2"
        DataGridViewCellStyle23.NullValue = Nothing
        Me.nTipCam.DefaultCellStyle = DataGridViewCellStyle23
        Me.nTipCam.HeaderText = "nTipCam"
        Me.nTipCam.Name = "nTipCam"
        Me.nTipCam.ReadOnly = True
        Me.nTipCam.Visible = False
        Me.nTipCam.Width = 120
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.Visible = False
        '
        'nEstado
        '
        Me.nEstado.DataPropertyName = "nEstado"
        Me.nEstado.HeaderText = "nEstado"
        Me.nEstado.Name = "nEstado"
        Me.nEstado.ReadOnly = True
        Me.nEstado.Visible = False
        '
        'nVenGra
        '
        Me.nVenGra.DataPropertyName = "nVenGra"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle24.Format = "N2"
        Me.nVenGra.DefaultCellStyle = DataGridViewCellStyle24
        Me.nVenGra.HeaderText = "V.Grabadas"
        Me.nVenGra.Name = "nVenGra"
        Me.nVenGra.ReadOnly = True
        Me.nVenGra.Visible = False
        '
        'nIGV
        '
        Me.nIGV.DataPropertyName = "nIGV"
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle25.Format = "N2"
        Me.nIGV.DefaultCellStyle = DataGridViewCellStyle25
        Me.nIGV.HeaderText = "IGV"
        Me.nIGV.Name = "nIGV"
        Me.nIGV.ReadOnly = True
        Me.nIGV.Visible = False
        '
        'nImpTot
        '
        Me.nImpTot.DataPropertyName = "nImpTot"
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle26.Format = "N2"
        Me.nImpTot.DefaultCellStyle = DataGridViewCellStyle26
        Me.nImpTot.HeaderText = "Imp.Total"
        Me.nImpTot.Name = "nImpTot"
        Me.nImpTot.ReadOnly = True
        Me.nImpTot.Visible = False
        '
        'cMensaje
        '
        Me.cMensaje.DataPropertyName = "cMensaje"
        Me.cMensaje.HeaderText = "cMensaje"
        Me.cMensaje.Name = "cMensaje"
        Me.cMensaje.ReadOnly = True
        Me.cMensaje.Visible = False
        '
        'cTipDocRef
        '
        Me.cTipDocRef.DataPropertyName = "cTipDocRef"
        Me.cTipDocRef.HeaderText = "cTipDocRef"
        Me.cTipDocRef.Name = "cTipDocRef"
        Me.cTipDocRef.ReadOnly = True
        Me.cTipDocRef.Visible = False
        '
        'NumDocRef
        '
        Me.NumDocRef.DataPropertyName = "NumDocRef"
        Me.NumDocRef.HeaderText = "NumDocRef"
        Me.NumDocRef.Name = "NumDocRef"
        Me.NumDocRef.ReadOnly = True
        Me.NumDocRef.Visible = False
        '
        'cObservaciones
        '
        Me.cObservaciones.DataPropertyName = "cObservaciones"
        Me.cObservaciones.HeaderText = "cObservaciones"
        Me.cObservaciones.Name = "cObservaciones"
        Me.cObservaciones.ReadOnly = True
        Me.cObservaciones.Visible = False
        '
        'nCodigoDocRef
        '
        Me.nCodigoDocRef.DataPropertyName = "nCodigoDocRef"
        Me.nCodigoDocRef.HeaderText = "nCodigoDocRef"
        Me.nCodigoDocRef.Name = "nCodigoDocRef"
        Me.nCodigoDocRef.ReadOnly = True
        Me.nCodigoDocRef.Visible = False
        '
        'nCodCliProv
        '
        Me.nCodCliProv.DataPropertyName = "nCodCliProv"
        Me.nCodCliProv.HeaderText = "nCodCliProv"
        Me.nCodCliProv.Name = "nCodCliProv"
        Me.nCodCliProv.ReadOnly = True
        Me.nCodCliProv.Visible = False
        '
        'cNumDoc
        '
        Me.cNumDoc.DataPropertyName = "cDirCli"
        Me.cNumDoc.HeaderText = "cNumDoc"
        Me.cNumDoc.Name = "cNumDoc"
        Me.cNumDoc.ReadOnly = True
        Me.cNumDoc.Visible = False
        '
        'cDirCli
        '
        Me.cDirCli.DataPropertyName = "cDirCli"
        Me.cDirCli.HeaderText = "cDirCli"
        Me.cDirCli.Name = "cDirCli"
        Me.cDirCli.ReadOnly = True
        Me.cDirCli.Visible = False
        '
        'nTipMov
        '
        Me.nTipMov.DataPropertyName = "nTipMov"
        Me.nTipMov.HeaderText = "nTipMov"
        Me.nTipMov.Name = "nTipMov"
        Me.nTipMov.ReadOnly = True
        Me.nTipMov.Visible = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(603, 43)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(218, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Desde:"
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(226, 43)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(178, 22)
        Me.cmbTipoBusqueda.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1067, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(268, 14)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(114, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 14)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta:"
        '
        'dtpFecIni
        '
        Me.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecIni.Location = New System.Drawing.Point(9, 43)
        Me.dtpFecIni.Name = "dtpFecIni"
        Me.dtpFecIni.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecIni.TabIndex = 3
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(117, 43)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecFin.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(223, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'btnRefreescar
        '
        Me.btnRefreescar.FlatAppearance.BorderSize = 0
        Me.btnRefreescar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreescar.Image = Global.Presentacion.My.Resources.Resources.Refrescar_24
        Me.btnRefreescar.Location = New System.Drawing.Point(822, 41)
        Me.btnRefreescar.Name = "btnRefreescar"
        Me.btnRefreescar.Size = New System.Drawing.Size(28, 25)
        Me.btnRefreescar.TabIndex = 2
        Me.btnRefreescar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip.SetToolTip(Me.btnRefreescar, "Actualizar Listado(F5)")
        Me.btnRefreescar.UseVisualStyleBackColor = True
        '
        'btnAnularMov
        '
        Me.btnAnularMov.FlatAppearance.BorderSize = 0
        Me.btnAnularMov.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularMov.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularMov.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnularMov.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnularMov.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnularMov.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnularMov.Location = New System.Drawing.Point(213, 570)
        Me.btnAnularMov.Name = "btnAnularMov"
        Me.btnAnularMov.Size = New System.Drawing.Size(174, 26)
        Me.btnAnularMov.TabIndex = 8
        Me.btnAnularMov.Text = "Anular Movimiento(F4)"
        Me.btnAnularMov.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnularMov.UseVisualStyleBackColor = True
        '
        'btnNuevoMov
        '
        Me.btnNuevoMov.FlatAppearance.BorderSize = 0
        Me.btnNuevoMov.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoMov.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoMov.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoMov.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevoMov.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevoMov.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoMov.Location = New System.Drawing.Point(9, 570)
        Me.btnNuevoMov.Name = "btnNuevoMov"
        Me.btnNuevoMov.Size = New System.Drawing.Size(176, 26)
        Me.btnNuevoMov.TabIndex = 7
        Me.btnNuevoMov.Text = "Nuevo Movimiento(F1)"
        Me.btnNuevoMov.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoMov.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(407, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(101, 14)
        Me.Label5.TabIndex = 134
        Me.Label5.Text = "Tipo Movimiento:"
        '
        'cmbTipoMovimiento
        '
        Me.cmbTipoMovimiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoMovimiento.FormattingEnabled = True
        Me.cmbTipoMovimiento.Location = New System.Drawing.Point(410, 43)
        Me.cmbTipoMovimiento.Name = "cmbTipoMovimiento"
        Me.cmbTipoMovimiento.Size = New System.Drawing.Size(187, 22)
        Me.cmbTipoMovimiento.TabIndex = 6
        '
        'FrmConsultaAlmacenServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cmbTipoMovimiento)
        Me.Controls.Add(Me.btnAnularMov)
        Me.Controls.Add(Me.btnNuevoMov)
        Me.Controls.Add(Me.btnRefreescar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecIni)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaAlmacenServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnRefreescar As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents btnAnularMov As Button
    Friend WithEvents btnNuevoMov As Button
    Friend WithEvents nCodalm As DataGridViewTextBoxColumn
    Friend WithEvents cNumAlm As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents cTipMov As DataGridViewTextBoxColumn
    Friend WithEvents cMotivoTraslado As DataGridViewTextBoxColumn
    Friend WithEvents AgenciaOrigen As DataGridViewTextBoxColumn
    Friend WithEvents AgenciaDestino As DataGridViewTextBoxColumn
    Friend WithEvents cEstado As DataGridViewTextBoxColumn
    Friend WithEvents nCodigoAgencia As DataGridViewTextBoxColumn
    Friend WithEvents nCodAgeDes As DataGridViewTextBoxColumn
    Friend WithEvents nCodAgeOriIng As DataGridViewTextBoxColumn
    Friend WithEvents cMotTrasl As DataGridViewTextBoxColumn
    Friend WithEvents nTipCam As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents nEstado As DataGridViewTextBoxColumn
    Friend WithEvents nVenGra As DataGridViewTextBoxColumn
    Friend WithEvents nIGV As DataGridViewTextBoxColumn
    Friend WithEvents nImpTot As DataGridViewTextBoxColumn
    Friend WithEvents cMensaje As DataGridViewTextBoxColumn
    Friend WithEvents cTipDocRef As DataGridViewTextBoxColumn
    Friend WithEvents NumDocRef As DataGridViewTextBoxColumn
    Friend WithEvents cObservaciones As DataGridViewTextBoxColumn
    Friend WithEvents nCodigoDocRef As DataGridViewTextBoxColumn
    Friend WithEvents nCodCliProv As DataGridViewTextBoxColumn
    Friend WithEvents cNumDoc As DataGridViewTextBoxColumn
    Friend WithEvents cDirCli As DataGridViewTextBoxColumn
    Friend WithEvents nTipMov As DataGridViewTextBoxColumn
    Friend WithEvents Label5 As Label
    Friend WithEvents cmbTipoMovimiento As ComboBox
End Class
