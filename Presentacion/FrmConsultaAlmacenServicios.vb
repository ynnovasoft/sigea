﻿Imports Negocios
Imports Business
Imports Entidades
Imports Entities
Imports System.Windows.Forms
Public Class FrmConsultaAlmacenServicios
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False

    Private Sub FrmConsultaAlmacenServicios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        dtpFecIni.Value = DateAdd(DateInterval.Month, -6, dtpFecIni.Value)
        Call CargaCombos()
        Call CargaMovimientos()
        Call ListarAlmacen()
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("210")
        Call CargaCombo(CreaDatoCombos(dt, 210), cmbTipoBusqueda)
    End Sub
    Sub CargaMovimientos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(30, "05,06")
        Call CargaCombo(dt, cmbTipoMovimiento)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call ListarAlmacen()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarAlmacen()
    End Sub
    Sub ListarAlmacen()
        Dim dt As DataTable
        Dim BLAlmacen As New BLAlmacen
        dt = BLAlmacen.Listar(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text, MDIPrincipal.CodigoAgencia, cmbTipoBusqueda.SelectedValue,
                              IIf(cmbTipoMovimiento.SelectedValue = "05", 1, 2), MDIPrincipal.CodigoEmpresa, True)
        Call LlenaAGridView(dt, dtgListado)
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value)
        Else
            Call ValidaBotones(0)
        End If
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call ListarAlmacen()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call ListarAlmacen()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEAlmacen As New BEStore
            Dim BEClientes As New BECustomer
            BEAlmacen.nCodalm = dtgListado.CurrentRow.Cells("nCodalm").Value
            BEAlmacen.nCodigoAgencia = dtgListado.CurrentRow.Cells("nCodigoAgencia").Value
            BEAlmacen.nCodAgeDes = dtgListado.CurrentRow.Cells("nCodAgeDes").Value
            BEAlmacen.AgenciaOrigen = dtgListado.CurrentRow.Cells("AgenciaOrigen").Value
            BEAlmacen.AgenciaDestino = dtgListado.CurrentRow.Cells("AgenciaDestino").Value
            BEAlmacen.cMotTrasl = dtgListado.CurrentRow.Cells("cMotTrasl").Value
            BEAlmacen.dFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BEAlmacen.nTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
            BEAlmacen.nTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BEAlmacen.nVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
            BEAlmacen.nIGV = dtgListado.CurrentRow.Cells("nIGV").Value
            BEAlmacen.nImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
            BEAlmacen.cMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
            BEAlmacen.nEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BEAlmacen.cTipDocRef = dtgListado.CurrentRow.Cells("cTipDocRef").Value
            BEAlmacen.NumDocRef = dtgListado.CurrentRow.Cells("NumDocRef").Value
            BEAlmacen.NumDocRef = dtgListado.CurrentRow.Cells("NumDocRef").Value
            BEAlmacen.nCodigoDocRef = dtgListado.CurrentRow.Cells("nCodigoDocRef").Value
            BEAlmacen.cObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
            BEAlmacen.cNumAlm = dtgListado.CurrentRow.Cells("cNumAlm").Value
            BEAlmacen.nSubTot = dtgListado.CurrentRow.Cells("nSubTot").Value
            BEAlmacen.nMonDes = dtgListado.CurrentRow.Cells("nMonDes").Value
            BEAlmacen.nCodEmpDes = dtgListado.CurrentRow.Cells("nCodEmpDes").Value
            BEAlmacen.EmpresaOrigen = dtgListado.CurrentRow.Cells("EmpresaOrigen").Value
            BEAlmacen.nTipMov = dtgListado.CurrentRow.Cells("nTipMov").Value
            BEAlmacen.nCodCliProv = dtgListado.CurrentRow.Cells("nCodCliProv").Value

            BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value

            Dim Actualizar As Boolean = False
            If BEAlmacen.nEstado = 1 Then
                If ValidaAperturaDia() = False Then
                    Exit Sub
                End If
            End If
            If cmbTipoMovimiento.SelectedValue = "05" Then
                FrmParteSalidaServicios.InicioAlmacen(BEAlmacen, BEClientes, 2, Actualizar)
                FrmParteSalidaServicios = Nothing
            Else
                FrmParteIngresoServicios.InicioAlmacen(BEAlmacen, BEClientes, 2, Actualizar)
                FrmParteIngresoServicios = Nothing
            End If
            If Actualizar = True Then
                Call ListarAlmacen()
            End If
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEAlmacen As New BEStore
                Dim BEClientes As New BECustomer
                BEAlmacen.nCodalm = dtgListado.CurrentRow.Cells("nCodalm").Value
                BEAlmacen.nCodigoAgencia = dtgListado.CurrentRow.Cells("nCodigoAgencia").Value
                BEAlmacen.nCodAgeDes = dtgListado.CurrentRow.Cells("nCodAgeDes").Value
                BEAlmacen.AgenciaOrigen = dtgListado.CurrentRow.Cells("AgenciaOrigen").Value
                BEAlmacen.AgenciaDestino = dtgListado.CurrentRow.Cells("AgenciaDestino").Value
                BEAlmacen.cMotTrasl = dtgListado.CurrentRow.Cells("cMotTrasl").Value
                BEAlmacen.dFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BEAlmacen.nTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
                BEAlmacen.nTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BEAlmacen.nEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BEAlmacen.nVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
                BEAlmacen.nIGV = dtgListado.CurrentRow.Cells("nIGV").Value
                BEAlmacen.nImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
                BEAlmacen.cMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
                BEAlmacen.cTipDocRef = dtgListado.CurrentRow.Cells("cTipDocRef").Value
                BEAlmacen.NumDocRef = dtgListado.CurrentRow.Cells("NumDocRef").Value
                BEAlmacen.NumDocRef = dtgListado.CurrentRow.Cells("NumDocRef").Value
                BEAlmacen.nCodigoDocRef = dtgListado.CurrentRow.Cells("nCodigoDocRef").Value
                BEAlmacen.cObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
                BEAlmacen.cNumAlm = dtgListado.CurrentRow.Cells("cNumAlm").Value
                BEAlmacen.nSubTot = dtgListado.CurrentRow.Cells("nSubTot").Value
                BEAlmacen.nMonDes = dtgListado.CurrentRow.Cells("nMonDes").Value
                BEAlmacen.nCodEmpDes = dtgListado.CurrentRow.Cells("nCodEmpDes").Value
                BEAlmacen.EmpresaOrigen = dtgListado.CurrentRow.Cells("EmpresaOrigen").Value
                BEAlmacen.nTipMov = dtgListado.CurrentRow.Cells("nTipMov").Value
                BEAlmacen.nCodCliProv = dtgListado.CurrentRow.Cells("nCodCliProv").Value

                BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value

                Dim Actualizar As Boolean = False
                If BEAlmacen.nEstado = 1 Then
                    If ValidaAperturaDia() = False Then
                        Exit Sub
                    End If
                End If
                If cmbTipoMovimiento.SelectedValue = "05" Then
                    FrmParteSalidaServicios.InicioAlmacen(BEAlmacen, BEClientes, 2, Actualizar)
                    FrmParteSalidaServicios = Nothing
                Else
                    FrmParteIngresoServicios.InicioAlmacen(BEAlmacen, BEClientes, 2, Actualizar)
                    FrmParteIngresoServicios = Nothing
                End If
                If Actualizar = True Then
                    Call ListarAlmacen()
                End If

            End If
        End If
    End Sub
    Sub ValidaBotones(ByVal Estado As Integer)
        btnNuevoMov.Enabled = True
        If Estado = 0 Then
            btnAnularMov.Enabled = False
            Exit Sub
        End If

        If Estado = 3 Or Estado = 4 Then
            btnAnularMov.Enabled = False
        Else
            btnAnularMov.Enabled = True
        End If
    End Sub
    Private Sub cmbTipoMovimiento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoMovimiento.SelectedIndexChanged

    End Sub

    Private Sub FrmConsultaAlmacenServicios_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevoMov.Enabled = True And btnNuevoMov.Visible = True Then
            Call btnNuevoMov_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnularMov.Enabled = True And btnAnularMov.Visible = True Then
            Call btnAnularMov_Click(sender, e)
        ElseIf e.KeyCode = Keys.F5 And btnRefreescar.Enabled = True And btnRefreescar.Visible = True Then
            Call btnRefreescar_Click(sender, e)
        End If
    End Sub

    Private Sub btnNuevoMov_Click(sender As Object, e As EventArgs) Handles btnNuevoMov.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If MDIPrincipal.CodigoBaseDatos <> 1 Then
            MsgBox("No se puede realizar ningun tipo de operaciones en una BASE DE DATOS tipificada como: BACKUP, Cierre sesiÓn y vulva a ingresar al sistema indicado la BASE DE DATOS ACTUAL", MsgBoxStyle.Critical, "Aviso")
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        If cmbTipoMovimiento.SelectedValue = "05" Then
            FrmParteSalidaServicios.InicioAlmacen(Nothing, Nothing, 1, Actualizar)
            FrmParteSalidaServicios = Nothing
        Else
            FrmParteIngresoServicios.InicioAlmacen(Nothing, Nothing, 1, Actualizar)
            FrmParteIngresoServicios = Nothing
        End If
        If Actualizar = True Then
            Call ListarAlmacen()
        End If
    End Sub
    Private Sub btnAnularMov_Click(sender As Object, e As EventArgs) Handles btnAnularMov.Click
        Dim oDatos As New BEStore
        If MsgBox("¿Esta seguro que desea anular el parte de " + IIf(cmbTipoMovimiento.SelectedValue = "05", "Salida)", "Ingreso?"), MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
            Dim cMensaje As String = ""
            oDatos.nCodalm = dtgListado.CurrentRow.Cells("nCodalm").Value
            oDatos.cUsuActAud = MDIPrincipal.CodUsuario
            oDatos.cCodPer = MDIPrincipal.CodigoPersonal
            Dim BLAlmacen As New BLAlmacen
            If BLAlmacen.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Call ListarAlmacen()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnRefreescar_Click(sender As Object, e As EventArgs) Handles btnRefreescar.Click
        Call ListarAlmacen()
    End Sub

    Private Sub cmbTipoMovimiento_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoMovimiento.SelectionChangeCommitted
        Call ListarAlmacen()
    End Sub

    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs) Handles dtgListado.SelectionChanged
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value)
        Else
            Call ValidaBotones(0)
        End If
    End Sub
    Private Sub btnAnularMov_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnularMov.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnNuevoMov_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevoMov.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoMovimiento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoMovimiento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class