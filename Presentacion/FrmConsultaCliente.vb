﻿Imports Negocios
Imports Business
Imports Entities
Public Class FrmConsultaCliente
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim oDatos As New BECustomer
    Sub Inicio(ByRef pnoDatos As BECustomer)
        Me.ShowDialog()
        pnoDatos = oDatos
    End Sub
    Private Sub FrmConsultaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call BuscarClientes()
        txtBuscar.Select()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("200")
        Call CargaCombo(CreaDatoCombos(dt, 200), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Select()
        Call BuscarClientes()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call BuscarClientes()
    End Sub
    Sub BuscarClientes()
        Dim dt As DataTable
        Dim BLCliente As New BLCliente
        dt = BLCliente.Listar(txtBuscar.Text, cmbTipoBusqueda.SelectedValue)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            oDatos.nCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
            oDatos.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            oDatos.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
            oDatos.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            oDatos.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value
            oDatos.nCodVen = dtgListado.CurrentRow.Cells("nCodVen").Value
            Me.Close()
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub
    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                oDatos.nCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
                oDatos.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                oDatos.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
                oDatos.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                oDatos.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value
                oDatos.nCodVen = dtgListado.CurrentRow.Cells("nCodVen").Value
                Me.Close()
            End If
        End If
    End Sub
    Private Sub dtgListado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtgListado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub FrmConsultaCliente_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class