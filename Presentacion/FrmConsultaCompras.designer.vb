﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaCompras
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodProve = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumComRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Agencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecEmi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecVen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipMov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipMov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cObservaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumGuiRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nVenGra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nIGV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMensaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodOrdCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumOrd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bIngPar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodAgeIng = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFecIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnRefreescar = New System.Windows.Forms.Button()
        Me.btnAnularCompra = New System.Windows.Forms.Button()
        Me.btnNuevoCompra = New System.Windows.Forms.Button()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(210, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL  DE COMPRAS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodCom, Me.nCodProve, Me.cNumCom, Me.dFecReg, Me.cRazSoc, Me.cNumComRef, Me.CondPag, Me.TipMon, Me.cEstado, Me.Agencia, Me.dFecEmi, Me.dFecVen, Me.nTipCam, Me.nCondPag, Me.nTipMon, Me.nTipCom, Me.cTipMov, Me.TipMov, Me.cObservaciones, Me.cNumGuiRef, Me.nEstado, Me.nVenGra, Me.nIGV, Me.nImpTot, Me.cMensaje, Me.nCodOrdCom, Me.cNumOrd, Me.bIngPar, Me.nCodAgeIng, Me.bServicio})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 66)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1324, 499)
        Me.dtgListado.TabIndex = 1
        '
        'nCodCom
        '
        Me.nCodCom.DataPropertyName = "nCodCom"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodCom.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodCom.HeaderText = "nCodCom"
        Me.nCodCom.Name = "nCodCom"
        Me.nCodCom.ReadOnly = True
        Me.nCodCom.Visible = False
        '
        'nCodProve
        '
        Me.nCodProve.DataPropertyName = "nCodProve"
        Me.nCodProve.HeaderText = "nCodProve"
        Me.nCodProve.Name = "nCodProve"
        Me.nCodProve.ReadOnly = True
        Me.nCodProve.Visible = False
        Me.nCodProve.Width = 110
        '
        'cNumCom
        '
        Me.cNumCom.DataPropertyName = "cNumCom"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumCom.DefaultCellStyle = DataGridViewCellStyle3
        Me.cNumCom.HeaderText = "N° Compra"
        Me.cNumCom.Name = "cNumCom"
        Me.cNumCom.ReadOnly = True
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle4
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.Width = 90
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle5
        Me.cRazSoc.HeaderText = "Proveedor"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 308
        '
        'cNumComRef
        '
        Me.cNumComRef.DataPropertyName = "cNumComRef"
        Me.cNumComRef.HeaderText = "Factura/Boleta"
        Me.cNumComRef.Name = "cNumComRef"
        Me.cNumComRef.ReadOnly = True
        Me.cNumComRef.Width = 150
        '
        'CondPag
        '
        Me.CondPag.DataPropertyName = "CondPag"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.CondPag.DefaultCellStyle = DataGridViewCellStyle6
        Me.CondPag.HeaderText = "Cond.Pago"
        Me.CondPag.Name = "CondPag"
        Me.CondPag.ReadOnly = True
        Me.CondPag.Width = 160
        '
        'TipMon
        '
        Me.TipMon.DataPropertyName = "TipMon"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipMon.DefaultCellStyle = DataGridViewCellStyle7
        Me.TipMon.HeaderText = "Moneda"
        Me.TipMon.Name = "TipMon"
        Me.TipMon.ReadOnly = True
        Me.TipMon.Width = 90
        '
        'cEstado
        '
        Me.cEstado.DataPropertyName = "cEstado"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstado.DefaultCellStyle = DataGridViewCellStyle8
        Me.cEstado.HeaderText = "Estado"
        Me.cEstado.Name = "cEstado"
        Me.cEstado.ReadOnly = True
        Me.cEstado.Width = 110
        '
        'Agencia
        '
        Me.Agencia.DataPropertyName = "Agencia"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Agencia.DefaultCellStyle = DataGridViewCellStyle9
        Me.Agencia.HeaderText = "Agencia Ingreso"
        Me.Agencia.Name = "Agencia"
        Me.Agencia.ReadOnly = True
        Me.Agencia.Visible = False
        Me.Agencia.Width = 150
        '
        'dFecEmi
        '
        Me.dFecEmi.DataPropertyName = "dFecEmi"
        Me.dFecEmi.HeaderText = "dFecEmi"
        Me.dFecEmi.Name = "dFecEmi"
        Me.dFecEmi.ReadOnly = True
        Me.dFecEmi.Visible = False
        '
        'dFecVen
        '
        Me.dFecVen.DataPropertyName = "dFecVen"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.dFecVen.DefaultCellStyle = DataGridViewCellStyle10
        Me.dFecVen.HeaderText = "dFecVen"
        Me.dFecVen.Name = "dFecVen"
        Me.dFecVen.ReadOnly = True
        Me.dFecVen.Visible = False
        '
        'nTipCam
        '
        Me.nTipCam.DataPropertyName = "nTipCam"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.nTipCam.DefaultCellStyle = DataGridViewCellStyle11
        Me.nTipCam.HeaderText = "nTipCam"
        Me.nTipCam.Name = "nTipCam"
        Me.nTipCam.ReadOnly = True
        Me.nTipCam.Visible = False
        Me.nTipCam.Width = 120
        '
        'nCondPag
        '
        Me.nCondPag.DataPropertyName = "nCondPag"
        Me.nCondPag.HeaderText = "nCondPag"
        Me.nCondPag.Name = "nCondPag"
        Me.nCondPag.ReadOnly = True
        Me.nCondPag.Visible = False
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.Visible = False
        '
        'nTipCom
        '
        Me.nTipCom.DataPropertyName = "nTipCom"
        Me.nTipCom.HeaderText = "nTipCom"
        Me.nTipCom.Name = "nTipCom"
        Me.nTipCom.ReadOnly = True
        Me.nTipCom.Visible = False
        '
        'cTipMov
        '
        Me.cTipMov.DataPropertyName = "cTipMov"
        Me.cTipMov.HeaderText = "cTipMov"
        Me.cTipMov.Name = "cTipMov"
        Me.cTipMov.ReadOnly = True
        Me.cTipMov.Visible = False
        '
        'TipMov
        '
        Me.TipMov.DataPropertyName = "TipMov"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipMov.DefaultCellStyle = DataGridViewCellStyle12
        Me.TipMov.HeaderText = "Mot.Traslado"
        Me.TipMov.Name = "TipMov"
        Me.TipMov.ReadOnly = True
        Me.TipMov.Visible = False
        Me.TipMov.Width = 200
        '
        'cObservaciones
        '
        Me.cObservaciones.DataPropertyName = "cObservaciones"
        Me.cObservaciones.HeaderText = "cObservaciones"
        Me.cObservaciones.Name = "cObservaciones"
        Me.cObservaciones.ReadOnly = True
        Me.cObservaciones.Visible = False
        '
        'cNumGuiRef
        '
        Me.cNumGuiRef.DataPropertyName = "cNumGuiRef"
        Me.cNumGuiRef.HeaderText = "cNumGuiRef"
        Me.cNumGuiRef.Name = "cNumGuiRef"
        Me.cNumGuiRef.ReadOnly = True
        Me.cNumGuiRef.Visible = False
        '
        'nEstado
        '
        Me.nEstado.DataPropertyName = "nEstado"
        Me.nEstado.HeaderText = "nEstado"
        Me.nEstado.Name = "nEstado"
        Me.nEstado.ReadOnly = True
        Me.nEstado.Visible = False
        '
        'nVenGra
        '
        Me.nVenGra.DataPropertyName = "nVenGra"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        Me.nVenGra.DefaultCellStyle = DataGridViewCellStyle13
        Me.nVenGra.HeaderText = "V.Grabadas"
        Me.nVenGra.Name = "nVenGra"
        Me.nVenGra.ReadOnly = True
        '
        'nIGV
        '
        Me.nIGV.DataPropertyName = "nIGV"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N2"
        Me.nIGV.DefaultCellStyle = DataGridViewCellStyle14
        Me.nIGV.HeaderText = "IGV"
        Me.nIGV.Name = "nIGV"
        Me.nIGV.ReadOnly = True
        Me.nIGV.Width = 90
        '
        'nImpTot
        '
        Me.nImpTot.DataPropertyName = "nImpTot"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.Format = "N2"
        Me.nImpTot.DefaultCellStyle = DataGridViewCellStyle15
        Me.nImpTot.HeaderText = "Imp.Total"
        Me.nImpTot.Name = "nImpTot"
        Me.nImpTot.ReadOnly = True
        '
        'cMensaje
        '
        Me.cMensaje.DataPropertyName = "cMensaje"
        Me.cMensaje.HeaderText = "cMensaje"
        Me.cMensaje.Name = "cMensaje"
        Me.cMensaje.ReadOnly = True
        Me.cMensaje.Visible = False
        '
        'nCodOrdCom
        '
        Me.nCodOrdCom.DataPropertyName = "nCodOrdCom"
        Me.nCodOrdCom.HeaderText = "nCodOrdCom"
        Me.nCodOrdCom.Name = "nCodOrdCom"
        Me.nCodOrdCom.ReadOnly = True
        Me.nCodOrdCom.Visible = False
        '
        'cNumOrd
        '
        Me.cNumOrd.DataPropertyName = "cNumOrd"
        Me.cNumOrd.HeaderText = "cNumOrd"
        Me.cNumOrd.Name = "cNumOrd"
        Me.cNumOrd.ReadOnly = True
        Me.cNumOrd.Visible = False
        '
        'bIngPar
        '
        Me.bIngPar.DataPropertyName = "bIngPar"
        Me.bIngPar.HeaderText = "bIngPar"
        Me.bIngPar.Name = "bIngPar"
        Me.bIngPar.ReadOnly = True
        Me.bIngPar.Visible = False
        '
        'nCodAgeIng
        '
        Me.nCodAgeIng.DataPropertyName = "nCodAgeIng"
        Me.nCodAgeIng.HeaderText = "nCodAgeIng"
        Me.nCodAgeIng.Name = "nCodAgeIng"
        Me.nCodAgeIng.ReadOnly = True
        Me.nCodAgeIng.Visible = False
        '
        'bServicio
        '
        Me.bServicio.DataPropertyName = "bServicio"
        Me.bServicio.HeaderText = "bServicio"
        Me.bServicio.Name = "bServicio"
        Me.bServicio.ReadOnly = True
        Me.bServicio.Visible = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(366, 43)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(176, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Desde:"
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.BackColor = System.Drawing.Color.White
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(198, 43)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(166, 22)
        Me.cmbTipoBusqueda.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1054, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(268, 14)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(100, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 14)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta:"
        '
        'dtpFecIni
        '
        Me.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecIni.Location = New System.Drawing.Point(9, 43)
        Me.dtpFecIni.Name = "dtpFecIni"
        Me.dtpFecIni.Size = New System.Drawing.Size(91, 22)
        Me.dtpFecIni.TabIndex = 3
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(103, 43)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(91, 22)
        Me.dtpFecFin.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(195, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'btnRefreescar
        '
        Me.btnRefreescar.FlatAppearance.BorderSize = 0
        Me.btnRefreescar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreescar.Image = Global.Presentacion.My.Resources.Resources.Refrescar_24
        Me.btnRefreescar.Location = New System.Drawing.Point(545, 39)
        Me.btnRefreescar.Name = "btnRefreescar"
        Me.btnRefreescar.Size = New System.Drawing.Size(28, 25)
        Me.btnRefreescar.TabIndex = 2
        Me.btnRefreescar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip.SetToolTip(Me.btnRefreescar, "Actualizar Listado <F5>")
        Me.btnRefreescar.UseVisualStyleBackColor = True
        '
        'btnAnularCompra
        '
        Me.btnAnularCompra.FlatAppearance.BorderSize = 0
        Me.btnAnularCompra.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularCompra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularCompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnularCompra.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnularCompra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnularCompra.Location = New System.Drawing.Point(198, 577)
        Me.btnAnularCompra.Name = "btnAnularCompra"
        Me.btnAnularCompra.Size = New System.Drawing.Size(145, 26)
        Me.btnAnularCompra.TabIndex = 136
        Me.btnAnularCompra.Text = "Anular Compra(F4)"
        Me.btnAnularCompra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnularCompra.UseVisualStyleBackColor = True
        '
        'btnNuevoCompra
        '
        Me.btnNuevoCompra.FlatAppearance.BorderSize = 0
        Me.btnNuevoCompra.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoCompra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoCompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoCompra.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevoCompra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoCompra.Location = New System.Drawing.Point(9, 577)
        Me.btnNuevoCompra.Name = "btnNuevoCompra"
        Me.btnNuevoCompra.Size = New System.Drawing.Size(147, 26)
        Me.btnNuevoCompra.TabIndex = 135
        Me.btnNuevoCompra.Text = "Nueva Compra(F1)"
        Me.btnNuevoCompra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoCompra.UseVisualStyleBackColor = True
        '
        'FrmConsultaCompras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnAnularCompra)
        Me.Controls.Add(Me.btnNuevoCompra)
        Me.Controls.Add(Me.btnRefreescar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecIni)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaCompras"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents btnRefreescar As Button
    Friend WithEvents btnAnularCompra As Button
    Friend WithEvents btnNuevoCompra As Button
    Friend WithEvents nCodCom As DataGridViewTextBoxColumn
    Friend WithEvents nCodProve As DataGridViewTextBoxColumn
    Friend WithEvents cNumCom As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents cNumComRef As DataGridViewTextBoxColumn
    Friend WithEvents CondPag As DataGridViewTextBoxColumn
    Friend WithEvents TipMon As DataGridViewTextBoxColumn
    Friend WithEvents cEstado As DataGridViewTextBoxColumn
    Friend WithEvents Agencia As DataGridViewTextBoxColumn
    Friend WithEvents dFecEmi As DataGridViewTextBoxColumn
    Friend WithEvents dFecVen As DataGridViewTextBoxColumn
    Friend WithEvents nTipCam As DataGridViewTextBoxColumn
    Friend WithEvents nCondPag As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents nTipCom As DataGridViewTextBoxColumn
    Friend WithEvents cTipMov As DataGridViewTextBoxColumn
    Friend WithEvents TipMov As DataGridViewTextBoxColumn
    Friend WithEvents cObservaciones As DataGridViewTextBoxColumn
    Friend WithEvents cNumGuiRef As DataGridViewTextBoxColumn
    Friend WithEvents nEstado As DataGridViewTextBoxColumn
    Friend WithEvents nVenGra As DataGridViewTextBoxColumn
    Friend WithEvents nIGV As DataGridViewTextBoxColumn
    Friend WithEvents nImpTot As DataGridViewTextBoxColumn
    Friend WithEvents cMensaje As DataGridViewTextBoxColumn
    Friend WithEvents nCodOrdCom As DataGridViewTextBoxColumn
    Friend WithEvents cNumOrd As DataGridViewTextBoxColumn
    Friend WithEvents bIngPar As DataGridViewTextBoxColumn
    Friend WithEvents nCodAgeIng As DataGridViewTextBoxColumn
    Friend WithEvents bServicio As DataGridViewTextBoxColumn
End Class
