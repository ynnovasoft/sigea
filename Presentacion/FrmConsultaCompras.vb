﻿Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Public Class FrmConsultaCompras
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False

    Private Sub FrmConsultaCompras_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        dtpFecIni.Value = DateAdd(DateInterval.Year, -1, dtpFecIni.Value)
        Call CargaCombos()
        Call ValidaBotones(0)
        Call ListarCompras()
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("240")
        Call CargaCombo(CreaDatoCombos(dt, 240), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call ListarCompras()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarCompras()
    End Sub
    Sub ListarCompras()
        Dim dt As DataTable
        Dim BLRegistroCompra As New BLPurchase
        dt = BLRegistroCompra.Listar(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text, cmbTipoBusqueda.SelectedValue,
                                      MDIPrincipal.CodigoEmpresa, False)
        Call LlenaAGridView(dt, dtgListado)
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value)
        Else
            Call ValidaBotones(0)
        End If
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call ListarCompras()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call ListarCompras()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BERegistroCompra As New BEPurchase
            Dim BEProveedor As New BEProveedor
            BERegistroCompra.nCodCom = dtgListado.CurrentRow.Cells("nCodCom").Value
            BERegistroCompra.nCodProve = dtgListado.CurrentRow.Cells("nCodProve").Value
            BERegistroCompra.dFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BERegistroCompra.dFecEmi = dtgListado.CurrentRow.Cells("dFecEmi").Value
            BERegistroCompra.dFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
            BERegistroCompra.nTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
            BERegistroCompra.nCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
            BERegistroCompra.nTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BERegistroCompra.cTipCom = dtgListado.CurrentRow.Cells("cTipCom").Value
            BERegistroCompra.cTipMov = dtgListado.CurrentRow.Cells("cTipMov").Value
            BERegistroCompra.cObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
            BERegistroCompra.cNumComRef = dtgListado.CurrentRow.Cells("cNumComRef").Value
            BERegistroCompra.cNumGuiRef = dtgListado.CurrentRow.Cells("cNumGuiRef").Value
            BERegistroCompra.nEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BERegistroCompra.nVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
            BERegistroCompra.nIGV = dtgListado.CurrentRow.Cells("nIGV").Value
            BERegistroCompra.nImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
            BERegistroCompra._cMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
            BERegistroCompra.nCodAgeIng = dtgListado.CurrentRow.Cells("nCodAgeIng").Value
            BERegistroCompra.nCodOrdCom = dtgListado.CurrentRow.Cells("nCodOrdCom").Value
            BERegistroCompra.cNumOrd = dtgListado.CurrentRow.Cells("cNumOrd").Value
            BERegistroCompra.bIngPar = dtgListado.CurrentRow.Cells("bIngPar").Value
            BERegistroCompra.cNumCom = dtgListado.CurrentRow.Cells("cNumCom").Value
            BERegistroCompra.bServicio = dtgListado.CurrentRow.Cells("bServicio").Value

            BEProveedor.gcNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEProveedor.gcDirProv = dtgListado.CurrentRow.Cells("cDirProv").Value
            BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value

            Dim Actualizar As Boolean = False
            If BERegistroCompra.nEstado = 1 Then
                If ValidaAperturaDia() = False Then
                    Exit Sub
                End If
            End If
            FrmRegistroCompras.InicioCompras(BERegistroCompra, BEProveedor, 2, Actualizar)
            FrmRegistroCompras = Nothing
            If Actualizar = True Then
                Call ListarCompras()
            End If
        End If

    End Sub
    Sub ValidaBotones(ByVal Estado As Integer)
        btnNuevoCompra.Enabled = True
        If Estado = 0 Then
            btnAnularCompra.Enabled = False
            Exit Sub
        End If

        If Estado = 3 Then
            btnAnularCompra.Enabled = False
        Else
            btnAnularCompra.Enabled = True
        End If
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub tlsTitulo_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tlsTitulo.ItemClicked

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub
    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BERegistroCompra As New BEPurchase
                Dim BEProveedor As New BEProveedor
                BERegistroCompra.nCodCom = dtgListado.CurrentRow.Cells("nCodCom").Value
                BERegistroCompra.nCodProve = dtgListado.CurrentRow.Cells("nCodProve").Value
                BERegistroCompra.dFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BERegistroCompra.dFecEmi = dtgListado.CurrentRow.Cells("dFecEmi").Value
                BERegistroCompra.dFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
                BERegistroCompra.nTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
                BERegistroCompra.nCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
                BERegistroCompra.nTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BERegistroCompra.cTipCom = dtgListado.CurrentRow.Cells("cTipCom").Value
                BERegistroCompra.cTipMov = dtgListado.CurrentRow.Cells("cTipMov").Value
                BERegistroCompra.cObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
                BERegistroCompra.cNumComRef = dtgListado.CurrentRow.Cells("cNumComRef").Value
                BERegistroCompra.cNumGuiRef = dtgListado.CurrentRow.Cells("cNumGuiRef").Value
                BERegistroCompra.nEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BERegistroCompra.nVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
                BERegistroCompra.nIGV = dtgListado.CurrentRow.Cells("nIGV").Value
                BERegistroCompra.nImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
                BERegistroCompra._cMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
                BERegistroCompra.nCodAgeIng = dtgListado.CurrentRow.Cells("nCodAgeIng").Value
                BERegistroCompra.nCodOrdCom = dtgListado.CurrentRow.Cells("nCodOrdCom").Value
                BERegistroCompra.cNumOrd = dtgListado.CurrentRow.Cells("cNumOrd").Value
                BERegistroCompra.bIngPar = dtgListado.CurrentRow.Cells("bIngPar").Value
                BERegistroCompra.cNumCom = dtgListado.CurrentRow.Cells("cNumCom").Value
                BERegistroCompra.bServicio = dtgListado.CurrentRow.Cells("bServicio").Value

                BEProveedor.gcNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEProveedor.gcDirProv = dtgListado.CurrentRow.Cells("cDirProv").Value
                BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value

                Dim Actualizar As Boolean = False
                If BERegistroCompra.nEstado = 1 Then
                    If ValidaAperturaDia() = False Then
                        Exit Sub
                    End If
                End If
                FrmRegistroCompras.InicioCompras(BERegistroCompra, BEProveedor, 2, Actualizar)
                FrmRegistroCompras = Nothing
                If Actualizar = True Then
                    Call ListarCompras()
                End If
            End If
        End If
    End Sub

    Private Sub btnAnularCompra_Click(sender As Object, e As EventArgs) Handles btnAnularCompra.Click
        Dim cMensaje As String = ""
        Dim Obj As New BLCommons
        Obj.ValidaEstadoMesContable(dtgListado.CurrentRow.Cells("nCodCom").Value, dtgListado.CurrentRow.Cells("dFecReg").Value,
                                    4, cMensaje)
        If cMensaje <> "" Then
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If

        Dim oDatos As New BEPurchase
        If MsgBox("¿Esta seguro que desea anular el registro de compra?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then

            oDatos.nCodCom = dtgListado.CurrentRow.Cells("nCodCom").Value
            oDatos.cUsuActAud = MDIPrincipal.CodUsuario
            oDatos.cCodPer = MDIPrincipal.CodigoPersonal
            oDatos.cMotAnu = ""
            Dim BLPurchase As New BLPurchase
            If BLPurchase.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Call ListarCompras()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnNuevoCompra_Click(sender As Object, e As EventArgs) Handles btnNuevoCompra.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If MDIPrincipal.CodigoBaseDatos <> 1 Then
            MsgBox("No se puede realizar ningun tipo de operaciones en una BASE DE DATOS tipificada como: BACKUP, Cierre sesiÓn y vulva a ingresar al sistema indicado la BASE DE DATOS ACTUAL", MsgBoxStyle.Critical, "Aviso")
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        FrmRegistroCompras.InicioCompras(Nothing, Nothing, 1, Actualizar)
        FrmRegistroCompras = Nothing
        If Actualizar = True Then
            Call ListarCompras()
        End If
    End Sub

    Private Sub btnRefreescar_Click(sender As Object, e As EventArgs) Handles btnRefreescar.Click
        Call ListarCompras()
    End Sub

    Private Sub FrmConsultaCompras_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevoCompra.Enabled = True And btnNuevoCompra.Visible = True Then
            Call btnNuevoCompra_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnularCompra.Enabled = True And btnAnularCompra.Visible = True Then
            Call btnAnularCompra_Click(sender, e)
        ElseIf e.KeyCode = Keys.F5 And btnRefreescar.Enabled = True And btnRefreescar.Visible = True Then
            Call btnRefreescar_Click(sender, e)
        End If
    End Sub

    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs) Handles dtgListado.SelectionChanged
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value)
        Else
            Call ValidaBotones(0)
        End If
    End Sub
End Class