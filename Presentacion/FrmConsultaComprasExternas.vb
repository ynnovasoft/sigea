﻿Imports Negocios
Imports Entidades
Imports Business
Public Class FrmConsultaComprasExternas
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim Estado As Integer = 0
    Dim BERegistroCompraExterna As New BERegistroCompraExterna
    Dim BEProveedor As New BEProveedor
    Dim BLProveedor As New BLProveedor
    Sub InicioCompras(ByRef oPedido As BERegistroCompraExterna, ByRef oProveedor As BEProveedor)
        Estado = 0
        Me.ShowDialog()
        oPedido = BERegistroCompraExterna
        oProveedor = BEProveedor
    End Sub
    Private Sub FrmConsultaComprasExternas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpFecIni.Value = DateAdd(DateInterval.Month, -1, dtpFecIni.Value)
        Call CargaCombos()
        Call BuscarCompras()
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("240")
        Call CargaCombo(CreaDatoCombos(dt, 240), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call BuscarCompras()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call BuscarCompras()
    End Sub
    Sub BuscarCompras()
        Dim dt As DataTable
        Dim BLRegistroCompraExterna As New BLRegistroCompraExterna
        dt = BLRegistroCompraExterna.Listar(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text,
                                   cmbTipoBusqueda.SelectedValue, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call BuscarCompras()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call BuscarCompras()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            BERegistroCompraExterna.gnCodCom = dtgListado.CurrentRow.Cells("nCodCom").Value
            BERegistroCompraExterna.gcNumCom = dtgListado.CurrentRow.Cells("cNumCom").Value
            BERegistroCompraExterna.gnCodProve = dtgListado.CurrentRow.Cells("nCodProve").Value
            BERegistroCompraExterna.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BERegistroCompraExterna.gdFecEmi = dtgListado.CurrentRow.Cells("dFecEmi").Value
            BERegistroCompraExterna.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
            BERegistroCompraExterna.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BERegistroCompraExterna.gcTipCom = dtgListado.CurrentRow.Cells("cTipCom").Value
            BERegistroCompraExterna.gcObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
            BERegistroCompraExterna.gcNumComRef = dtgListado.CurrentRow.Cells("cNumComRef").Value
            BERegistroCompraExterna.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BERegistroCompraExterna.gnVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
            BERegistroCompraExterna.gnIGV = dtgListado.CurrentRow.Cells("nIGV").Value
            BERegistroCompraExterna.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value

            BEProveedor.gcNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEProveedor.gcDirProv = dtgListado.CurrentRow.Cells("cDirProv").Value
            BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            Me.Close()
        End If

    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub tlsTitulo_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tlsTitulo.ItemClicked

    End Sub

    Private Sub cmbEstado_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs) Handles dtgListado.SelectionChanged

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                BERegistroCompraExterna.gnCodCom = dtgListado.CurrentRow.Cells("nCodCom").Value
                BERegistroCompraExterna.gcNumCom = dtgListado.CurrentRow.Cells("cNumCom").Value
                BERegistroCompraExterna.gnCodProve = dtgListado.CurrentRow.Cells("nCodProve").Value
                BERegistroCompraExterna.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BERegistroCompraExterna.gdFecEmi = dtgListado.CurrentRow.Cells("dFecEmi").Value
                BERegistroCompraExterna.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
                BERegistroCompraExterna.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BERegistroCompraExterna.gcTipCom = dtgListado.CurrentRow.Cells("cTipCom").Value
                BERegistroCompraExterna.gcObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
                BERegistroCompraExterna.gcNumComRef = dtgListado.CurrentRow.Cells("cNumComRef").Value
                BERegistroCompraExterna.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BERegistroCompraExterna.gnVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
                BERegistroCompraExterna.gnIGV = dtgListado.CurrentRow.Cells("nIGV").Value
                BERegistroCompraExterna.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value

                BEProveedor.gcNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEProveedor.gcDirProv = dtgListado.CurrentRow.Cells("cDirProv").Value
                BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                Me.Close()
            End If
        End If
    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class