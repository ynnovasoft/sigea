﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaCotizacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodCot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumCot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecVen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nVenGra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nIGV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMensaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAgeRet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTieEnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cObservaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFecIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnGenerarComprobante = New System.Windows.Forms.Button()
        Me.btnNuevoCotizacion = New System.Windows.Forms.Button()
        Me.btnAnularCotizacion = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnRefreescar = New System.Windows.Forms.Button()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(234, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL DE COTIZACIONES"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodCot, Me.cNumCot, Me.dFecReg, Me.nCodCli, Me.cRazSoc, Me.dFecVen, Me.nTipCam, Me.nCondPag, Me.CondPag, Me.nTipMon, Me.TipMon, Me.nEstado, Me.cEstado, Me.nVenGra, Me.nIGV, Me.nImpTot, Me.cMensaje, Me.bAgeRet, Me.cTieEnt, Me.cObservaciones})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 66)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1324, 509)
        Me.dtgListado.TabIndex = 1
        '
        'nCodCot
        '
        Me.nCodCot.DataPropertyName = "nCodCot"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodCot.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodCot.HeaderText = "nCodCot"
        Me.nCodCot.Name = "nCodCot"
        Me.nCodCot.ReadOnly = True
        Me.nCodCot.Visible = False
        Me.nCodCot.Width = 90
        '
        'cNumCot
        '
        Me.cNumCot.DataPropertyName = "cNumCot"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumCot.DefaultCellStyle = DataGridViewCellStyle3
        Me.cNumCot.HeaderText = "N° Cot."
        Me.cNumCot.Name = "cNumCot"
        Me.cNumCot.ReadOnly = True
        Me.cNumCot.Width = 90
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle4
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.Width = 90
        '
        'nCodCli
        '
        Me.nCodCli.DataPropertyName = "nCodCli"
        Me.nCodCli.HeaderText = "nCodCli"
        Me.nCodCli.Name = "nCodCli"
        Me.nCodCli.ReadOnly = True
        Me.nCodCli.Visible = False
        Me.nCodCli.Width = 110
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle5
        Me.cRazSoc.HeaderText = "Cliente"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 320
        '
        'dFecVen
        '
        Me.dFecVen.DataPropertyName = "dFecVen"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.NullValue = Nothing
        Me.dFecVen.DefaultCellStyle = DataGridViewCellStyle6
        Me.dFecVen.HeaderText = "dFecVen"
        Me.dFecVen.Name = "dFecVen"
        Me.dFecVen.ReadOnly = True
        Me.dFecVen.Visible = False
        '
        'nTipCam
        '
        Me.nTipCam.DataPropertyName = "nTipCam"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.nTipCam.DefaultCellStyle = DataGridViewCellStyle7
        Me.nTipCam.HeaderText = "nTipCam"
        Me.nTipCam.Name = "nTipCam"
        Me.nTipCam.ReadOnly = True
        Me.nTipCam.Visible = False
        Me.nTipCam.Width = 120
        '
        'nCondPag
        '
        Me.nCondPag.DataPropertyName = "nCondPag"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCondPag.DefaultCellStyle = DataGridViewCellStyle8
        Me.nCondPag.HeaderText = "nCondPag"
        Me.nCondPag.Name = "nCondPag"
        Me.nCondPag.ReadOnly = True
        Me.nCondPag.Visible = False
        Me.nCondPag.Width = 120
        '
        'CondPag
        '
        Me.CondPag.DataPropertyName = "CondPag"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.CondPag.DefaultCellStyle = DataGridViewCellStyle9
        Me.CondPag.HeaderText = "Cond.Pago"
        Me.CondPag.Name = "CondPag"
        Me.CondPag.ReadOnly = True
        Me.CondPag.Width = 190
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.Visible = False
        '
        'TipMon
        '
        Me.TipMon.DataPropertyName = "TipMon"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipMon.DefaultCellStyle = DataGridViewCellStyle10
        Me.TipMon.HeaderText = "Moneda"
        Me.TipMon.Name = "TipMon"
        Me.TipMon.ReadOnly = True
        Me.TipMon.Width = 130
        '
        'nEstado
        '
        Me.nEstado.DataPropertyName = "nEstado"
        Me.nEstado.HeaderText = "nEstado"
        Me.nEstado.Name = "nEstado"
        Me.nEstado.ReadOnly = True
        Me.nEstado.Visible = False
        '
        'cEstado
        '
        Me.cEstado.DataPropertyName = "cEstado"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstado.DefaultCellStyle = DataGridViewCellStyle11
        Me.cEstado.HeaderText = "Estado"
        Me.cEstado.Name = "cEstado"
        Me.cEstado.ReadOnly = True
        Me.cEstado.Width = 130
        '
        'nVenGra
        '
        Me.nVenGra.DataPropertyName = "nVenGra"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N2"
        Me.nVenGra.DefaultCellStyle = DataGridViewCellStyle12
        Me.nVenGra.HeaderText = "V.Grabadas"
        Me.nVenGra.Name = "nVenGra"
        Me.nVenGra.ReadOnly = True
        Me.nVenGra.Width = 120
        '
        'nIGV
        '
        Me.nIGV.DataPropertyName = "nIGV"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        Me.nIGV.DefaultCellStyle = DataGridViewCellStyle13
        Me.nIGV.HeaderText = "IGV"
        Me.nIGV.Name = "nIGV"
        Me.nIGV.ReadOnly = True
        Me.nIGV.Width = 110
        '
        'nImpTot
        '
        Me.nImpTot.DataPropertyName = "nImpTot"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N2"
        Me.nImpTot.DefaultCellStyle = DataGridViewCellStyle14
        Me.nImpTot.HeaderText = "Imp.Total"
        Me.nImpTot.Name = "nImpTot"
        Me.nImpTot.ReadOnly = True
        Me.nImpTot.Width = 120
        '
        'cMensaje
        '
        Me.cMensaje.DataPropertyName = "cMensaje"
        Me.cMensaje.HeaderText = "cMensaje"
        Me.cMensaje.Name = "cMensaje"
        Me.cMensaje.ReadOnly = True
        Me.cMensaje.Visible = False
        '
        'bAgeRet
        '
        Me.bAgeRet.DataPropertyName = "bAgeRet"
        Me.bAgeRet.HeaderText = "bAgeRet"
        Me.bAgeRet.Name = "bAgeRet"
        Me.bAgeRet.ReadOnly = True
        Me.bAgeRet.Visible = False
        '
        'cTieEnt
        '
        Me.cTieEnt.DataPropertyName = "cTieEnt"
        Me.cTieEnt.HeaderText = "cTieEnt"
        Me.cTieEnt.Name = "cTieEnt"
        Me.cTieEnt.ReadOnly = True
        Me.cTieEnt.Visible = False
        '
        'cObservaciones
        '
        Me.cObservaciones.DataPropertyName = "cObservaciones"
        Me.cObservaciones.HeaderText = "cObservaciones"
        Me.cObservaciones.Name = "cObservaciones"
        Me.cObservaciones.ReadOnly = True
        Me.cObservaciones.Visible = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(420, 43)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(199, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Desde:"
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(240, 43)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(178, 22)
        Me.cmbTipoBusqueda.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1055, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(268, 14)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(117, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 14)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta:"
        '
        'dtpFecIni
        '
        Me.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecIni.Location = New System.Drawing.Point(9, 43)
        Me.dtpFecIni.Name = "dtpFecIni"
        Me.dtpFecIni.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecIni.TabIndex = 3
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(120, 43)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecFin.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(237, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'btnGenerarComprobante
        '
        Me.btnGenerarComprobante.FlatAppearance.BorderSize = 0
        Me.btnGenerarComprobante.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarComprobante.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarComprobante.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarComprobante.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarComprobante.Image = Global.Presentacion.My.Resources.Resources.CuentasxPagar_32
        Me.btnGenerarComprobante.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerarComprobante.Location = New System.Drawing.Point(453, 581)
        Me.btnGenerarComprobante.Name = "btnGenerarComprobante"
        Me.btnGenerarComprobante.Size = New System.Drawing.Size(178, 26)
        Me.btnGenerarComprobante.TabIndex = 8
        Me.btnGenerarComprobante.Text = "Generar Comprobante"
        Me.btnGenerarComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerarComprobante.UseVisualStyleBackColor = True
        '
        'btnNuevoCotizacion
        '
        Me.btnNuevoCotizacion.FlatAppearance.BorderSize = 0
        Me.btnNuevoCotizacion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoCotizacion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoCotizacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoCotizacion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevoCotizacion.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevoCotizacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoCotizacion.Location = New System.Drawing.Point(9, 581)
        Me.btnNuevoCotizacion.Name = "btnNuevoCotizacion"
        Me.btnNuevoCotizacion.Size = New System.Drawing.Size(167, 26)
        Me.btnNuevoCotizacion.TabIndex = 6
        Me.btnNuevoCotizacion.Text = "Nuevo Cotización(F1)"
        Me.btnNuevoCotizacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoCotizacion.UseVisualStyleBackColor = True
        '
        'btnAnularCotizacion
        '
        Me.btnAnularCotizacion.FlatAppearance.BorderSize = 0
        Me.btnAnularCotizacion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularCotizacion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularCotizacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnularCotizacion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnularCotizacion.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnularCotizacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnularCotizacion.Location = New System.Drawing.Point(221, 581)
        Me.btnAnularCotizacion.Name = "btnAnularCotizacion"
        Me.btnAnularCotizacion.Size = New System.Drawing.Size(166, 26)
        Me.btnAnularCotizacion.TabIndex = 7
        Me.btnAnularCotizacion.Text = "Anular Cotización(F4)"
        Me.btnAnularCotizacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnularCotizacion.UseVisualStyleBackColor = True
        '
        'btnRefreescar
        '
        Me.btnRefreescar.FlatAppearance.BorderSize = 0
        Me.btnRefreescar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreescar.Image = Global.Presentacion.My.Resources.Resources.Refrescar_24
        Me.btnRefreescar.Location = New System.Drawing.Point(620, 42)
        Me.btnRefreescar.Name = "btnRefreescar"
        Me.btnRefreescar.Size = New System.Drawing.Size(22, 22)
        Me.btnRefreescar.TabIndex = 2
        Me.btnRefreescar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip.SetToolTip(Me.btnRefreescar, "Actualizar Listado(F5)")
        Me.btnRefreescar.UseVisualStyleBackColor = True
        '
        'FrmConsultaCotizacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnRefreescar)
        Me.Controls.Add(Me.btnGenerarComprobante)
        Me.Controls.Add(Me.btnNuevoCotizacion)
        Me.Controls.Add(Me.btnAnularCotizacion)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecIni)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaCotizacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnGenerarComprobante As Button
    Friend WithEvents btnNuevoCotizacion As Button
    Friend WithEvents btnAnularCotizacion As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents btnRefreescar As Button
    Friend WithEvents nCodCot As DataGridViewTextBoxColumn
    Friend WithEvents cNumCot As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents nCodCli As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents dFecVen As DataGridViewTextBoxColumn
    Friend WithEvents nTipCam As DataGridViewTextBoxColumn
    Friend WithEvents nCondPag As DataGridViewTextBoxColumn
    Friend WithEvents CondPag As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents TipMon As DataGridViewTextBoxColumn
    Friend WithEvents nEstado As DataGridViewTextBoxColumn
    Friend WithEvents cEstado As DataGridViewTextBoxColumn
    Friend WithEvents nVenGra As DataGridViewTextBoxColumn
    Friend WithEvents nIGV As DataGridViewTextBoxColumn
    Friend WithEvents nImpTot As DataGridViewTextBoxColumn
    Friend WithEvents cMensaje As DataGridViewTextBoxColumn
    Friend WithEvents bAgeRet As DataGridViewTextBoxColumn
    Friend WithEvents cTieEnt As DataGridViewTextBoxColumn
    Friend WithEvents cObservaciones As DataGridViewTextBoxColumn
End Class
