﻿Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Public Class FrmConsultaCotizacion
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim Estado As Integer = 0
    Private Sub FrmConsultaCotizacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        dtpFecIni.Value = DateAdd(DateInterval.Month, -6, dtpFecIni.Value)
        Call CargaCombos()
        txtBuscar.Select()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("100")
        Call CargaCombo(CreaDatoCombos(dt, 100), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Select()
        Call ListarCotizacion()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarCotizacion()
    End Sub
    Sub ListarCotizacion()
        Dim dt As DataTable
        Dim BLCotizacion As New BLCotizacion
        dt = BLCotizacion.Listar(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text, MDIPrincipal.CodigoAgencia,
                                   cmbTipoBusqueda.SelectedValue, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value)
        Else
            Call ValidaBotones(0)
        End If
    End Sub
    Sub ValidaBotones(ByVal Estado As Integer)
        btnNuevoCotizacion.Enabled = True
        If Estado = 0 Then
            btnAnularCotizacion.Enabled = False
            btnGenerarComprobante.Enabled = False
            Exit Sub
        End If

        If Estado = 3 Or Estado = 4 Then
            btnAnularCotizacion.Enabled = False
        Else
            btnAnularCotizacion.Enabled = True
        End If

        If Estado = 2 Then
            btnGenerarComprobante.Enabled = True
        Else
            btnGenerarComprobante.Enabled = False
        End If
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call ListarCotizacion()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call ListarCotizacion()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEClientes As New BECustomer
            Dim BECotizacion As New BECotizacion
            BECotizacion.gnCodCot = dtgListado.CurrentRow.Cells("nCodCot").Value
            BECotizacion.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
            BECotizacion.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BECotizacion.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
            BECotizacion.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
            BECotizacion.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
            BECotizacion.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BECotizacion.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BECotizacion.gnVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
            BECotizacion.gnIGV = dtgListado.CurrentRow.Cells("nIGV").Value
            BECotizacion.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
            BECotizacion.gcMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
            BECotizacion.gcNumCot = dtgListado.CurrentRow.Cells("cNumCot").Value
            BECotizacion.gnSubTot = dtgListado.CurrentRow.Cells("nSubTot").Value
            BECotizacion.gnMonDes = dtgListado.CurrentRow.Cells("nMonDes").Value
            BECotizacion.gcTieEnt = dtgListado.CurrentRow.Cells("cTieEnt").Value
            BECotizacion.gcObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value

            BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

            Dim Actualizar As Boolean = False
            If BECotizacion.gnEstado = 1 Then
                If ValidaAperturaDia() = False Then
                    Exit Sub
                End If
            End If
            FrmRegistroCotizacion.InicioCotizacion(BECotizacion, BEClientes, 2, Actualizar)
            FrmRegistroCotizacion = Nothing
            If Actualizar = True Then
                Call ListarCotizacion()
            End If
        End If

    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnRefreescar_Click(sender As Object, e As EventArgs) Handles btnRefreescar.Click
        Call ListarCotizacion()
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEClientes As New BECustomer
                Dim BECotizacion As New BECotizacion
                BECotizacion.gnCodCot = dtgListado.CurrentRow.Cells("nCodCot").Value
                BECotizacion.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
                BECotizacion.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BECotizacion.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
                BECotizacion.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
                BECotizacion.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
                BECotizacion.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BECotizacion.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BECotizacion.gnVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
                BECotizacion.gnIGV = dtgListado.CurrentRow.Cells("nIGV").Value
                BECotizacion.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
                BECotizacion.gcMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
                BECotizacion.gcNumCot = dtgListado.CurrentRow.Cells("cNumCot").Value
                BECotizacion.gnSubTot = dtgListado.CurrentRow.Cells("nSubTot").Value
                BECotizacion.gnMonDes = dtgListado.CurrentRow.Cells("nMonDes").Value
                BECotizacion.gcTieEnt = dtgListado.CurrentRow.Cells("cTieEnt").Value
                BECotizacion.gcObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value

                BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

                Dim Actualizar As Boolean = False
                If BECotizacion.gnEstado = 1 Then
                    If ValidaAperturaDia() = False Then
                        Exit Sub
                    End If
                End If
                FrmRegistroCotizacion.InicioCotizacion(BECotizacion, BEClientes, 2, Actualizar)
                FrmRegistroCotizacion = Nothing
                If Actualizar = True Then
                    Call ListarCotizacion()
                End If
            End If
        End If
    End Sub

    Private Sub btnRefreescar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnRefreescar.KeyPress
        ValidaSonidoEnter(e)
    End Sub

    Private Sub btnNuevoCotizacion_Click(sender As Object, e As EventArgs) Handles btnNuevoCotizacion.Click
        Dim Actualizar As Boolean = False
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If MDIPrincipal.CodigoBaseDatos <> 1 Then
            MsgBox("No se puede realizar ningun tipo de operaciones en una BASE DE DATOS tipificada como: BACKUP, Cierre sesiÓn y vulva a ingresar al sistema indicado la BASE DE DATOS ACTUAL", MsgBoxStyle.Critical, "Aviso")
            Exit Sub
        End If
        FrmRegistroCotizacion.InicioCotizacion(Nothing, Nothing, 1, Actualizar)
        FrmRegistroCotizacion = Nothing
        If Actualizar = True Then
            Call ListarCotizacion()
        End If
    End Sub

    Private Sub btnAnularCotizacion_Click(sender As Object, e As EventArgs) Handles btnAnularCotizacion.Click
        Dim cMensaje As String = ""
        Dim oDatos As New BECotizacion
        If MsgBox("¿Esta seguro que desea anular la cotización?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodCot = dtgListado.CurrentRow.Cells("nCodCot").Value
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            Dim BLCotizacion As New BLCotizacion
            If BLCotizacion.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Call ListarCotizacion()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnGenerarComprobante_Click(sender As Object, e As EventArgs) Handles btnGenerarComprobante.Click
        Dim BEClientes As New BECustomer
        Dim BECotizacion As New BECotizacion

        BECotizacion.gnCodCot = dtgListado.CurrentRow.Cells("nCodCot").Value
        BECotizacion.gcNumCot = dtgListado.CurrentRow.Cells("cNumCot").Value
        BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
        BECotizacion.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
        BECotizacion.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
        BECotizacion.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
        BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
        BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
        BECotizacion.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
        BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        FrmTomaPedidos.InicioCotizacion(BECotizacion, BEClientes, 4, Actualizar)
        FrmTomaPedidos = Nothing
        If Actualizar = True Then
            Call ListarCotizacion()
        End If
    End Sub

    Private Sub FrmConsultaCotizacion_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevoCotizacion.Enabled = True And btnNuevoCotizacion.Visible = True Then
            Call btnNuevoCotizacion_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnularCotizacion.Enabled = True And btnAnularCotizacion.Visible = True Then
            Call btnAnularCotizacion_Click(sender, e)
        ElseIf e.KeyCode = Keys.F5 And btnRefreescar.Enabled = True And btnRefreescar.Visible = True Then
            Call btnRefreescar_Click(sender, e)
        End If
    End Sub

    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs) Handles dtgListado.SelectionChanged
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value)
        Else
            Call ValidaBotones(0)
        End If
    End Sub

    Private Sub btnGenerarComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGenerarComprobante.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAnularCotizacion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnularCotizacion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnNuevoCotizacion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevoCotizacion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class