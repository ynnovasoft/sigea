﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaDocumentosCreditoClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodPed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumComp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MONTOTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MONTOPAGADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DEUDAFECHA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecVen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chbPagado = New System.Windows.Forms.CheckBox()
        Me.btnEstadoCuenta = New System.Windows.Forms.Button()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(346, 22)
        Me.lblTitulo.Text = "LISTADO CANCELACIÓN DE COMPROBANTES - CLIENTES"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodCli, Me.nCodPed, Me.cNumComp, Me.dFecReg, Me.cRazSoc, Me.cNumDoc, Me.nTipMon, Me.nTipCam, Me.TipMon, Me.TipCom, Me.CondPag, Me.MONTOTOTAL, Me.MONTOPAGADO, Me.DEUDAFECHA, Me.dFecVen, Me.nCondPag})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 56)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1325, 508)
        Me.dtgListado.TabIndex = 1
        '
        'nCodCli
        '
        Me.nCodCli.DataPropertyName = "nCodCli"
        Me.nCodCli.HeaderText = "nCodCli"
        Me.nCodCli.Name = "nCodCli"
        Me.nCodCli.ReadOnly = True
        Me.nCodCli.Visible = False
        '
        'nCodPed
        '
        Me.nCodPed.DataPropertyName = "nCodPed"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodPed.DefaultCellStyle = DataGridViewCellStyle13
        Me.nCodPed.HeaderText = "N° Pedido"
        Me.nCodPed.Name = "nCodPed"
        Me.nCodPed.ReadOnly = True
        Me.nCodPed.Visible = False
        Me.nCodPed.Width = 90
        '
        'cNumComp
        '
        Me.cNumComp.DataPropertyName = "cNumComp"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumComp.DefaultCellStyle = DataGridViewCellStyle14
        Me.cNumComp.HeaderText = "N° Comp"
        Me.cNumComp.Name = "cNumComp"
        Me.cNumComp.ReadOnly = True
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle15
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.Width = 80
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle16
        Me.cRazSoc.HeaderText = "Cliente"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 448
        '
        'cNumDoc
        '
        Me.cNumDoc.DataPropertyName = "cNumDoc"
        Me.cNumDoc.HeaderText = "cNumDoc"
        Me.cNumDoc.Name = "cNumDoc"
        Me.cNumDoc.ReadOnly = True
        Me.cNumDoc.Visible = False
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.Visible = False
        '
        'nTipCam
        '
        Me.nTipCam.DataPropertyName = "nTipCam"
        Me.nTipCam.HeaderText = "nTipCam"
        Me.nTipCam.Name = "nTipCam"
        Me.nTipCam.ReadOnly = True
        Me.nTipCam.Visible = False
        '
        'TipMon
        '
        Me.TipMon.DataPropertyName = "TipMon"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipMon.DefaultCellStyle = DataGridViewCellStyle17
        Me.TipMon.HeaderText = "Moneda"
        Me.TipMon.Name = "TipMon"
        Me.TipMon.ReadOnly = True
        Me.TipMon.Width = 85
        '
        'TipCom
        '
        Me.TipCom.DataPropertyName = "TipCom"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipCom.DefaultCellStyle = DataGridViewCellStyle18
        Me.TipCom.HeaderText = "Tipo Comprobante"
        Me.TipCom.Name = "TipCom"
        Me.TipCom.ReadOnly = True
        Me.TipCom.Width = 150
        '
        'CondPag
        '
        Me.CondPag.DataPropertyName = "CondPag"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.CondPag.DefaultCellStyle = DataGridViewCellStyle19
        Me.CondPag.HeaderText = "Condición Pago"
        Me.CondPag.Name = "CondPag"
        Me.CondPag.ReadOnly = True
        Me.CondPag.Width = 150
        '
        'MONTOTOTAL
        '
        Me.MONTOTOTAL.DataPropertyName = "MONTOTOTAL"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle20.Format = "N2"
        Me.MONTOTOTAL.DefaultCellStyle = DataGridViewCellStyle20
        Me.MONTOTOTAL.HeaderText = "Imp.Total"
        Me.MONTOTOTAL.Name = "MONTOTOTAL"
        Me.MONTOTOTAL.ReadOnly = True
        Me.MONTOTOTAL.Width = 90
        '
        'MONTOPAGADO
        '
        Me.MONTOPAGADO.DataPropertyName = "MONTOPAGADO"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle21.Format = "N2"
        Me.MONTOPAGADO.DefaultCellStyle = DataGridViewCellStyle21
        Me.MONTOPAGADO.HeaderText = "Monto Pag."
        Me.MONTOPAGADO.Name = "MONTOPAGADO"
        Me.MONTOPAGADO.ReadOnly = True
        '
        'DEUDAFECHA
        '
        Me.DEUDAFECHA.DataPropertyName = "DEUDAFECHA"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle22.Format = "N2"
        Me.DEUDAFECHA.DefaultCellStyle = DataGridViewCellStyle22
        Me.DEUDAFECHA.HeaderText = "Deuda"
        Me.DEUDAFECHA.Name = "DEUDAFECHA"
        Me.DEUDAFECHA.ReadOnly = True
        '
        'dFecVen
        '
        Me.dFecVen.DataPropertyName = "dFecVen"
        Me.dFecVen.HeaderText = "dFecVen"
        Me.dFecVen.Name = "dFecVen"
        Me.dFecVen.ReadOnly = True
        Me.dFecVen.Visible = False
        '
        'nCondPag
        '
        Me.nCondPag.DataPropertyName = "nCondPag"
        Me.nCondPag.HeaderText = "nCondPag"
        Me.nCondPag.Name = "nCondPag"
        Me.nCondPag.ReadOnly = True
        Me.nCondPag.Visible = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(242, 32)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(254, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.BackColor = System.Drawing.Color.White
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(83, 32)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(157, 22)
        Me.cmbTipoBusqueda.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1067, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(268, 14)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'chbPagado
        '
        Me.chbPagado.AutoSize = True
        Me.chbPagado.Location = New System.Drawing.Point(505, 34)
        Me.chbPagado.Name = "chbPagado"
        Me.chbPagado.Size = New System.Drawing.Size(169, 18)
        Me.chbPagado.TabIndex = 3
        Me.chbPagado.Text = "Comprobantes Cancelados"
        Me.chbPagado.UseVisualStyleBackColor = True
        '
        'btnEstadoCuenta
        '
        Me.btnEstadoCuenta.FlatAppearance.BorderSize = 0
        Me.btnEstadoCuenta.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEstadoCuenta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEstadoCuenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEstadoCuenta.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEstadoCuenta.Image = Global.Presentacion.My.Resources.Resources.EECC
        Me.btnEstadoCuenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEstadoCuenta.Location = New System.Drawing.Point(9, 575)
        Me.btnEstadoCuenta.Name = "btnEstadoCuenta"
        Me.btnEstadoCuenta.Size = New System.Drawing.Size(143, 26)
        Me.btnEstadoCuenta.TabIndex = 4
        Me.btnEstadoCuenta.Text = "Estado Cuenta"
        Me.btnEstadoCuenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEstadoCuenta.UseVisualStyleBackColor = True
        '
        'FrmConsultaDocumentosCreditoClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnEstadoCuenta)
        Me.Controls.Add(Me.chbPagado)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaDocumentosCreditoClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chbPagado As CheckBox
    Friend WithEvents nCodCli As DataGridViewTextBoxColumn
    Friend WithEvents nCodPed As DataGridViewTextBoxColumn
    Friend WithEvents cNumComp As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents cNumDoc As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents nTipCam As DataGridViewTextBoxColumn
    Friend WithEvents TipMon As DataGridViewTextBoxColumn
    Friend WithEvents TipCom As DataGridViewTextBoxColumn
    Friend WithEvents CondPag As DataGridViewTextBoxColumn
    Friend WithEvents MONTOTOTAL As DataGridViewTextBoxColumn
    Friend WithEvents MONTOPAGADO As DataGridViewTextBoxColumn
    Friend WithEvents DEUDAFECHA As DataGridViewTextBoxColumn
    Friend WithEvents dFecVen As DataGridViewTextBoxColumn
    Friend WithEvents nCondPag As DataGridViewTextBoxColumn
    Friend WithEvents btnEstadoCuenta As Button
End Class
