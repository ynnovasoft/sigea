﻿Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Imports System.Configuration
Public Class FrmConsultaDocumentosCreditoClientes
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Private Sub FrmConsultaDocumentosCreditoClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call Listar()
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("90")
        Call CargaCombo(CreaDatoCombos(dt, 90), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call Listar()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call Listar()
    End Sub
    Sub Listar()
        Dim dt As DataTable
        Dim BLPedido As New BLPedido
        dt = BLPedido.ListarCreditos(txtBuscar.Text, cmbTipoBusqueda.SelectedValue, chbPagado.Checked,
                                         MDIPrincipal.CodigoEmpresa, IIf(MDIPrincipal.PagoSucursal = True, 0, MDIPrincipal.CodigoAgencia))
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs)
        Call Listar()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs)
        Call Listar()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEPedido As New BEPedido
            Dim BEClientes As New BECustomer
            BEPedido.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
            BEPedido.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BEPedido.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
            BEPedido.gcOrdComp = dtgListado.CurrentRow.Cells("TipMon").Value
            BEPedido.gnImpTot = dtgListado.CurrentRow.Cells("MONTOTOTAL").Value
            BEPedido.gcNumComp = dtgListado.CurrentRow.Cells("cNumComp").Value
            BEPedido.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BEPedido.gcMensaje = dtgListado.CurrentRow.Cells("CondPag").Value
            BEPedido.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value

            BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            BEClientes.nCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value

            Dim Actualizar As Boolean = False

            If ValidaAperturaDia() = False Then
                Exit Sub
            End If
            If ValidaAperturaCaja() = False Then
                Exit Sub
            End If
            FrmRegistroCobroClientes.InicioCancelacion(BEPedido, BEClientes, 2, Actualizar)
            FrmRegistroCobroClientes = Nothing

            If Actualizar = True Then
                Call Listar()
            End If
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub tlsTitulo_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tlsTitulo.ItemClicked

    End Sub

    Private Sub cmbEstado_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbEstado_SelectionChangeCommitted(sender As Object, e As EventArgs)
        Call Listar()
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbPagado_CheckedChanged(sender As Object, e As EventArgs) Handles chbPagado.CheckedChanged
        Call Listar()
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEPedido As New BEPedido
                Dim BEClientes As New BECustomer
                BEPedido.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
                BEPedido.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BEPedido.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
                BEPedido.gcOrdComp = dtgListado.CurrentRow.Cells("TipMon").Value
                BEPedido.gnImpTot = dtgListado.CurrentRow.Cells("MONTOTOTAL").Value
                BEPedido.gcNumComp = dtgListado.CurrentRow.Cells("cNumComp").Value
                BEPedido.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BEPedido.gcMensaje = dtgListado.CurrentRow.Cells("CondPag").Value
                BEPedido.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value

                BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                BEClientes.nCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
                Dim Actualizar As Boolean = False

                If ValidaAperturaDia() = False Then
                    Exit Sub
                End If
                If ValidaAperturaCaja() = False Then
                    Exit Sub
                End If
                FrmRegistroCobroClientes.InicioCancelacion(BEPedido, BEClientes, 2, Actualizar)
                FrmRegistroCobroClientes = Nothing

                If Actualizar = True Then
                    Call Listar()
                End If
            End If
        End If
    End Sub

    Private Sub chbPagado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbPagado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEstadoCuenta_Click(sender As Object, e As EventArgs) Handles btnEstadoCuenta.Click
        Dim Modalidad As Integer = 0
        Dim Result As DialogResult = MsgBox("Seleccione la modalidad de Estado de Cuenta: " & vbNewLine & " " & vbNewLine & "(SI)=Deuda vencida pendiente" & vbNewLine & "(NO)=Deuda total pendiente", MessageBoxIcon.Information + vbYesNoCancel, "MENSAJE")
        If Result = vbYes Then
            Modalidad = 1
        ElseIf Result = vbNo Then
            Modalidad = 2
        Else
            Exit Sub
        End If
        Dim ModeloEstadoCuenta As String = ConfigurationManager.AppSettings("ModeloEstadoCuenta")
        Dim RutaEstadoCuenta As String = ConfigurationManager.AppSettings("RutaEstadoCuenta")
        RutaEstadoCuenta = RutaEstadoCuenta + "EE-CC-" + dtgListado.CurrentRow.Cells("cRazSoc").Value + ".pdf"
        If ModeloEstadoCuenta = "1" Then
            Call CrearPdf_TA4_EECC_Modelo_1(dtgListado.CurrentRow.Cells("nCodCli").Value, dtgListado.CurrentRow.Cells("cRazSoc").Value, Modalidad)
            Process.Start(RutaEstadoCuenta)
        ElseIf ModeloEstadoCuenta = "2" Then
            Call CrearPdf_TA4_EECC_Modelo_2(dtgListado.CurrentRow.Cells("nCodCli").Value, dtgListado.CurrentRow.Cells("cRazSoc").Value, Modalidad)
            Process.Start(RutaEstadoCuenta)
        ElseIf ModeloEstadoCuenta = "3" Then

        ElseIf ModeloEstadoCuenta = "4" Then

        End If
    End Sub

    Private Sub FrmConsultaDocumentosCreditoClientes_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub btnEstadoCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEstadoCuenta.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class