﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaDocumentosCreditoProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chbPagado = New System.Windows.Forms.CheckBox()
        Me.nCodCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumComRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MONTOTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MONTOPAGADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DEUDAFECHA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1244, 29)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(220, 26)
        Me.lblTitulo.Text = "LISTADO  CRÉDITO PROVEEDORES"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodCom, Me.cNumCom, Me.cRazSoc, Me.cNumDoc, Me.dFecReg, Me.nTipMon, Me.nTipCam, Me.TipMon, Me.TipCom, Me.cNumComRef, Me.CondPag, Me.MONTOTOTAL, Me.MONTOPAGADO, Me.DEUDAFECHA})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 61)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1226, 416)
        Me.dtgListado.TabIndex = 2
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(217, 36)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(188, 23)
        Me.txtBuscar.TabIndex = 1
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.BackColor = System.Drawing.Color.White
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(83, 36)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(133, 24)
        Me.cmbTipoBusqueda.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(946, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(285, 16)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 16)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'chbPagado
        '
        Me.chbPagado.AutoSize = True
        Me.chbPagado.Location = New System.Drawing.Point(411, 38)
        Me.chbPagado.Name = "chbPagado"
        Me.chbPagado.Size = New System.Drawing.Size(179, 20)
        Me.chbPagado.TabIndex = 129
        Me.chbPagado.Text = "Comprobantes Cancelados"
        Me.chbPagado.UseVisualStyleBackColor = True
        '
        'nCodCom
        '
        Me.nCodCom.DataPropertyName = "nCodCom"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodCom.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodCom.HeaderText = "nCodCom"
        Me.nCodCom.Name = "nCodCom"
        Me.nCodCom.ReadOnly = True
        Me.nCodCom.Visible = False
        '
        'cNumCom
        '
        Me.cNumCom.DataPropertyName = "cNumCom"
        Me.cNumCom.HeaderText = "N° Compra"
        Me.cNumCom.Name = "cNumCom"
        Me.cNumCom.ReadOnly = True
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle3
        Me.cRazSoc.HeaderText = "Cliente/Proveedor"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 300
        '
        'cNumDoc
        '
        Me.cNumDoc.DataPropertyName = "cNumDoc"
        Me.cNumDoc.HeaderText = "cNumDoc"
        Me.cNumDoc.Name = "cNumDoc"
        Me.cNumDoc.ReadOnly = True
        Me.cNumDoc.Visible = False
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle4
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.Width = 80
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.Visible = False
        '
        'nTipCam
        '
        Me.nTipCam.DataPropertyName = "nTipCam"
        Me.nTipCam.HeaderText = "nTipCam"
        Me.nTipCam.Name = "nTipCam"
        Me.nTipCam.ReadOnly = True
        Me.nTipCam.Visible = False
        '
        'TipMon
        '
        Me.TipMon.DataPropertyName = "TipMon"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipMon.DefaultCellStyle = DataGridViewCellStyle5
        Me.TipMon.HeaderText = "Moneda"
        Me.TipMon.Name = "TipMon"
        Me.TipMon.ReadOnly = True
        Me.TipMon.Width = 90
        '
        'TipCom
        '
        Me.TipCom.DataPropertyName = "TipCom"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipCom.DefaultCellStyle = DataGridViewCellStyle6
        Me.TipCom.HeaderText = "Tip.Comp"
        Me.TipCom.Name = "TipCom"
        Me.TipCom.ReadOnly = True
        Me.TipCom.Width = 110
        '
        'cNumComRef
        '
        Me.cNumComRef.DataPropertyName = "cNumComRef"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumComRef.DefaultCellStyle = DataGridViewCellStyle7
        Me.cNumComRef.HeaderText = "N° Factura"
        Me.cNumComRef.Name = "cNumComRef"
        Me.cNumComRef.ReadOnly = True
        '
        'CondPag
        '
        Me.CondPag.DataPropertyName = "CondPag"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.CondPag.DefaultCellStyle = DataGridViewCellStyle8
        Me.CondPag.HeaderText = "Cond.Pag"
        Me.CondPag.Name = "CondPag"
        Me.CondPag.ReadOnly = True
        Me.CondPag.Width = 130
        '
        'MONTOTOTAL
        '
        Me.MONTOTOTAL.DataPropertyName = "MONTOTOTAL"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.MONTOTOTAL.DefaultCellStyle = DataGridViewCellStyle9
        Me.MONTOTOTAL.HeaderText = "Imp.Total"
        Me.MONTOTOTAL.Name = "MONTOTOTAL"
        Me.MONTOTOTAL.ReadOnly = True
        Me.MONTOTOTAL.Width = 96
        '
        'MONTOPAGADO
        '
        Me.MONTOPAGADO.DataPropertyName = "MONTOPAGADO"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.MONTOPAGADO.DefaultCellStyle = DataGridViewCellStyle10
        Me.MONTOPAGADO.HeaderText = "Monto Pag."
        Me.MONTOPAGADO.Name = "MONTOPAGADO"
        Me.MONTOPAGADO.ReadOnly = True
        '
        'DEUDAFECHA
        '
        Me.DEUDAFECHA.DataPropertyName = "DEUDAFECHA"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        Me.DEUDAFECHA.DefaultCellStyle = DataGridViewCellStyle11
        Me.DEUDAFECHA.HeaderText = "Deuda"
        Me.DEUDAFECHA.Name = "DEUDAFECHA"
        Me.DEUDAFECHA.ReadOnly = True
        Me.DEUDAFECHA.Width = 97
        '
        'FrmConsultaDocumentosCreditoProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1244, 486)
        Me.ControlBox = False
        Me.Controls.Add(Me.chbPagado)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaDocumentosCreditoProveedor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chbPagado As CheckBox
    Friend WithEvents nCodCom As DataGridViewTextBoxColumn
    Friend WithEvents cNumCom As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents cNumDoc As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents nTipCam As DataGridViewTextBoxColumn
    Friend WithEvents TipMon As DataGridViewTextBoxColumn
    Friend WithEvents TipCom As DataGridViewTextBoxColumn
    Friend WithEvents cNumComRef As DataGridViewTextBoxColumn
    Friend WithEvents CondPag As DataGridViewTextBoxColumn
    Friend WithEvents MONTOTOTAL As DataGridViewTextBoxColumn
    Friend WithEvents MONTOPAGADO As DataGridViewTextBoxColumn
    Friend WithEvents DEUDAFECHA As DataGridViewTextBoxColumn
End Class
