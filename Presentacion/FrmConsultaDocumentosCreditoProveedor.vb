﻿Imports Negocios
Imports Entities
Imports Entidades
Imports Business
Imports System.Windows.Forms
Public Class FrmConsultaDocumentosCreditoProveedor
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim BEPurchase As New BEPurchase
    Dim BEProveedor As New BEProveedor
    Dim Estado As Integer = 0
    Sub Inicio(ByRef oRegistroCompra As BEPurchase, ByRef oProveedor As BEProveedor)
        Me.ShowDialog()
        oRegistroCompra = BEPurchase
        oProveedor = BEProveedor
    End Sub

    Private Sub FrmConsultaDocumentosCreditoProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call Listar()
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(240, "1,2,3")
        Call CargaCombo(dt, cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call Listar()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call Listar()
    End Sub
    Sub Listar()
        Dim dt As DataTable
        Dim BLPurchase As New BLPurchase
        dt = BLPurchase.ListarCreditos(txtBuscar.Text,
                                   cmbTipoBusqueda.SelectedValue, chbPagado.Checked, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs)
        Call Listar()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs)
        Call Listar()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            BEPurchase.nCodCom = dtgListado.CurrentRow.Cells("nCodCom").Value
            BEPurchase.cNumCom = dtgListado.CurrentRow.Cells("cNumCom").Value
            BEPurchase.dFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BEPurchase.dFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
            BEPurchase.cObservaciones = dtgListado.CurrentRow.Cells("TipMon").Value
            BEPurchase.nImpTot = dtgListado.CurrentRow.Cells("MONTOTOTAL").Value
            BEPurchase.nTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BEPurchase._cMensaje = dtgListado.CurrentRow.Cells("CondPag").Value

            BEProveedor.gcNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            Me.Close()
        End If

    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub tlsTitulo_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tlsTitulo.ItemClicked

    End Sub

    Private Sub cmbEstado_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbEstado_SelectionChangeCommitted(sender As Object, e As EventArgs)
        Call Listar()
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbPagado_CheckedChanged(sender As Object, e As EventArgs) Handles chbPagado.CheckedChanged
        Call Listar()
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                BEPurchase.nCodCom = dtgListado.CurrentRow.Cells("nCodCom").Value
                BEPurchase.cNumCom = dtgListado.CurrentRow.Cells("cNumCom").Value
                BEPurchase.dFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BEPurchase.dFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
                BEPurchase.cObservaciones = dtgListado.CurrentRow.Cells("TipMon").Value
                BEPurchase.nImpTot = dtgListado.CurrentRow.Cells("MONTOTOTAL").Value
                BEPurchase.nTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BEPurchase._cMensaje = dtgListado.CurrentRow.Cells("CondPag").Value

                BEProveedor.gcNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                Me.Close()
            End If
        End If
    End Sub

    Private Sub chbPagado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbPagado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub FrmConsultaDocumentosCreditoProveedor_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class