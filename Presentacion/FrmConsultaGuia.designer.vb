﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaGuia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFecIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnNuevoGuia = New System.Windows.Forms.Button()
        Me.btnAnularGuia = New System.Windows.Forms.Button()
        Me.btnGenerarComprobante = New System.Windows.Forms.Button()
        Me.btnRefreescar = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblMensajeEstadoSunat = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnActEstado = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnEnviarSunat = New System.Windows.Forms.Button()
        Me.Seleccionar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.nCodGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MotTrasl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstadoSistema = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstadoSunat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bOfLine = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.nCodCot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDirCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMotTrasl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDirPart = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodDirCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMensajeEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Almacen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipDocAdj = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDocAdj = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipoCompAdj = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAgeRet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bParcial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumGuiaCor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cObsSut = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumTickSun = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bElectronico = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecTras = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cModTras = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(282, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL  DE GUIAS ELECTRÓNICAS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Seleccionar, Me.nCodGuia, Me.cNumGuia, Me.dFecReg, Me.cRazSoc, Me.TipGuia, Me.MotTrasl, Me.cEstadoSistema, Me.cEstadoSunat, Me.bOfLine, Me.nCodCot, Me.nCodCli, Me.cNumDoc, Me.cDirCli, Me.nEstado, Me.cTipGuia, Me.nCodSer, Me.cMotTrasl, Me.cDirPart, Me.nCodDirCli, Me.cMensajeEstado, Me.Almacen, Me.cTipDocAdj, Me.cNumDocAdj, Me.cTipoCompAdj, Me.nTipMon, Me.bAgeRet, Me.bParcial, Me.cNumGuiaCor, Me.cObsSut, Me.cNumTickSun, Me.bElectronico, Me.dFecTras, Me.cModTras})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 68)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1230, 489)
        Me.dtgListado.TabIndex = 1
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(399, 45)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(194, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Desde:"
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(234, 45)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(163, 22)
        Me.cmbTipoBusqueda.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(117, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 14)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta:"
        '
        'dtpFecIni
        '
        Me.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecIni.Location = New System.Drawing.Point(9, 45)
        Me.dtpFecIni.Name = "dtpFecIni"
        Me.dtpFecIni.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecIni.TabIndex = 3
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(120, 45)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecFin.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(231, 30)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'btnNuevoGuia
        '
        Me.btnNuevoGuia.FlatAppearance.BorderSize = 0
        Me.btnNuevoGuia.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoGuia.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoGuia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoGuia.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevoGuia.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevoGuia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoGuia.Location = New System.Drawing.Point(9, 573)
        Me.btnNuevoGuia.Name = "btnNuevoGuia"
        Me.btnNuevoGuia.Size = New System.Drawing.Size(130, 26)
        Me.btnNuevoGuia.TabIndex = 6
        Me.btnNuevoGuia.Text = "Nueva Guia(F1)"
        Me.btnNuevoGuia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoGuia.UseVisualStyleBackColor = True
        '
        'btnAnularGuia
        '
        Me.btnAnularGuia.FlatAppearance.BorderSize = 0
        Me.btnAnularGuia.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularGuia.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularGuia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnularGuia.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnularGuia.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnularGuia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnularGuia.Location = New System.Drawing.Point(160, 573)
        Me.btnAnularGuia.Name = "btnAnularGuia"
        Me.btnAnularGuia.Size = New System.Drawing.Size(129, 26)
        Me.btnAnularGuia.TabIndex = 7
        Me.btnAnularGuia.Text = "Anular Guia(F4)"
        Me.btnAnularGuia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnularGuia.UseVisualStyleBackColor = True
        '
        'btnGenerarComprobante
        '
        Me.btnGenerarComprobante.FlatAppearance.BorderSize = 0
        Me.btnGenerarComprobante.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarComprobante.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarComprobante.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarComprobante.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarComprobante.Image = Global.Presentacion.My.Resources.Resources.CuentasxPagar_32
        Me.btnGenerarComprobante.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerarComprobante.Location = New System.Drawing.Point(317, 573)
        Me.btnGenerarComprobante.Name = "btnGenerarComprobante"
        Me.btnGenerarComprobante.Size = New System.Drawing.Size(179, 26)
        Me.btnGenerarComprobante.TabIndex = 8
        Me.btnGenerarComprobante.Text = "Generar Comprobante"
        Me.btnGenerarComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerarComprobante.UseVisualStyleBackColor = True
        '
        'btnRefreescar
        '
        Me.btnRefreescar.FlatAppearance.BorderSize = 0
        Me.btnRefreescar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreescar.Image = Global.Presentacion.My.Resources.Resources.Refrescar_24
        Me.btnRefreescar.Location = New System.Drawing.Point(595, 44)
        Me.btnRefreescar.Name = "btnRefreescar"
        Me.btnRefreescar.Size = New System.Drawing.Size(22, 22)
        Me.btnRefreescar.TabIndex = 2
        Me.btnRefreescar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip.SetToolTip(Me.btnRefreescar, "Actualizar Listado <F5>")
        Me.btnRefreescar.UseVisualStyleBackColor = True
        '
        'lblMensajeEstadoSunat
        '
        Me.lblMensajeEstadoSunat.BackColor = System.Drawing.Color.White
        Me.lblMensajeEstadoSunat.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeEstadoSunat.Location = New System.Drawing.Point(664, 28)
        Me.lblMensajeEstadoSunat.Name = "lblMensajeEstadoSunat"
        Me.lblMensajeEstadoSunat.Size = New System.Drawing.Size(667, 35)
        Me.lblMensajeEstadoSunat.TabIndex = 129
        Me.lblMensajeEstadoSunat.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnActEstado)
        Me.GroupBox2.Controls.Add(Me.btnImprimir)
        Me.GroupBox2.Controls.Add(Me.btnEnviarSunat)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.Location = New System.Drawing.Point(1244, 61)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(90, 496)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "SUNAT"
        '
        'btnActEstado
        '
        Me.btnActEstado.FlatAppearance.BorderSize = 0
        Me.btnActEstado.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnActEstado.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnActEstado.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActEstado.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActEstado.Image = Global.Presentacion.My.Resources.Resources.ActualizarSunat_24
        Me.btnActEstado.Location = New System.Drawing.Point(5, 167)
        Me.btnActEstado.Name = "btnActEstado"
        Me.btnActEstado.Size = New System.Drawing.Size(80, 65)
        Me.btnActEstado.TabIndex = 12
        Me.btnActEstado.Text = "Consultar Estado"
        Me.btnActEstado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnActEstado.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.FlatAppearance.BorderSize = 0
        Me.btnImprimir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnImprimir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Image = Global.Presentacion.My.Resources.Resources.Print_22
        Me.btnImprimir.Location = New System.Drawing.Point(5, 100)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(80, 46)
        Me.btnImprimir.TabIndex = 11
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnEnviarSunat
        '
        Me.btnEnviarSunat.FlatAppearance.BorderSize = 0
        Me.btnEnviarSunat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEnviarSunat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEnviarSunat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnviarSunat.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarSunat.Image = Global.Presentacion.My.Resources.Resources.Sunat_22
        Me.btnEnviarSunat.Location = New System.Drawing.Point(5, 20)
        Me.btnEnviarSunat.Name = "btnEnviarSunat"
        Me.btnEnviarSunat.Size = New System.Drawing.Size(80, 56)
        Me.btnEnviarSunat.TabIndex = 10
        Me.btnEnviarSunat.Text = "Enviar Sunat"
        Me.btnEnviarSunat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnEnviarSunat.UseVisualStyleBackColor = True
        '
        'Seleccionar
        '
        Me.Seleccionar.HeaderText = "Sel"
        Me.Seleccionar.Name = "Seleccionar"
        Me.Seleccionar.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Seleccionar.Width = 25
        '
        'nCodGuia
        '
        Me.nCodGuia.DataPropertyName = "nCodGuia"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodGuia.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodGuia.HeaderText = "Cod.Guia"
        Me.nCodGuia.Name = "nCodGuia"
        Me.nCodGuia.Visible = False
        '
        'cNumGuia
        '
        Me.cNumGuia.DataPropertyName = "cNumGuia"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumGuia.DefaultCellStyle = DataGridViewCellStyle3
        Me.cNumGuia.HeaderText = "N° Guia"
        Me.cNumGuia.Name = "cNumGuia"
        Me.cNumGuia.ReadOnly = True
        Me.cNumGuia.Width = 125
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle4
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle5
        Me.cRazSoc.HeaderText = "Cliente"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 280
        '
        'TipGuia
        '
        Me.TipGuia.DataPropertyName = "TipGuia"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipGuia.DefaultCellStyle = DataGridViewCellStyle6
        Me.TipGuia.HeaderText = "Tipo Guia"
        Me.TipGuia.Name = "TipGuia"
        Me.TipGuia.ReadOnly = True
        Me.TipGuia.Width = 179
        '
        'MotTrasl
        '
        Me.MotTrasl.DataPropertyName = "MotTrasl"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.MotTrasl.DefaultCellStyle = DataGridViewCellStyle7
        Me.MotTrasl.HeaderText = "Mot.Traslado"
        Me.MotTrasl.Name = "MotTrasl"
        Me.MotTrasl.ReadOnly = True
        Me.MotTrasl.Width = 200
        '
        'cEstadoSistema
        '
        Me.cEstadoSistema.DataPropertyName = "cEstadoSistema"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstadoSistema.DefaultCellStyle = DataGridViewCellStyle8
        Me.cEstadoSistema.HeaderText = "Estado Sistema"
        Me.cEstadoSistema.Name = "cEstadoSistema"
        Me.cEstadoSistema.ReadOnly = True
        Me.cEstadoSistema.Width = 125
        '
        'cEstadoSunat
        '
        Me.cEstadoSunat.DataPropertyName = "cEstadoSunat"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstadoSunat.DefaultCellStyle = DataGridViewCellStyle9
        Me.cEstadoSunat.HeaderText = "Estado Sunat"
        Me.cEstadoSunat.Name = "cEstadoSunat"
        Me.cEstadoSunat.ReadOnly = True
        Me.cEstadoSunat.Width = 125
        '
        'bOfLine
        '
        Me.bOfLine.DataPropertyName = "bOfLine"
        Me.bOfLine.HeaderText = "OffLine"
        Me.bOfLine.Name = "bOfLine"
        Me.bOfLine.ReadOnly = True
        Me.bOfLine.Width = 50
        '
        'nCodCot
        '
        Me.nCodCot.DataPropertyName = "nCodCot"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodCot.DefaultCellStyle = DataGridViewCellStyle10
        Me.nCodCot.HeaderText = "nCodCot"
        Me.nCodCot.Name = "nCodCot"
        Me.nCodCot.Visible = False
        Me.nCodCot.Width = 90
        '
        'nCodCli
        '
        Me.nCodCli.DataPropertyName = "nCodCli"
        Me.nCodCli.HeaderText = "nCodCli"
        Me.nCodCli.Name = "nCodCli"
        Me.nCodCli.Visible = False
        Me.nCodCli.Width = 110
        '
        'cNumDoc
        '
        Me.cNumDoc.DataPropertyName = "cNumDoc"
        Me.cNumDoc.HeaderText = "cNumDoc"
        Me.cNumDoc.Name = "cNumDoc"
        Me.cNumDoc.Visible = False
        '
        'cDirCli
        '
        Me.cDirCli.DataPropertyName = "cDirCli"
        Me.cDirCli.HeaderText = "cDirCli"
        Me.cDirCli.Name = "cDirCli"
        Me.cDirCli.Visible = False
        '
        'nEstado
        '
        Me.nEstado.DataPropertyName = "nEstado"
        Me.nEstado.HeaderText = "nEstado"
        Me.nEstado.Name = "nEstado"
        Me.nEstado.Visible = False
        '
        'cTipGuia
        '
        Me.cTipGuia.DataPropertyName = "cTipGuia"
        Me.cTipGuia.HeaderText = "cTipGuia"
        Me.cTipGuia.Name = "cTipGuia"
        Me.cTipGuia.Visible = False
        '
        'nCodSer
        '
        Me.nCodSer.DataPropertyName = "nCodSer"
        Me.nCodSer.HeaderText = "nCodSer"
        Me.nCodSer.Name = "nCodSer"
        Me.nCodSer.Visible = False
        '
        'cMotTrasl
        '
        Me.cMotTrasl.DataPropertyName = "cMotTrasl"
        Me.cMotTrasl.HeaderText = "cMotTrasl"
        Me.cMotTrasl.Name = "cMotTrasl"
        Me.cMotTrasl.Visible = False
        '
        'cDirPart
        '
        Me.cDirPart.DataPropertyName = "cDirPart"
        Me.cDirPart.HeaderText = "cDirPart"
        Me.cDirPart.Name = "cDirPart"
        Me.cDirPart.Visible = False
        '
        'nCodDirCli
        '
        Me.nCodDirCli.DataPropertyName = "nCodDirCli"
        Me.nCodDirCli.HeaderText = "nCodDirCli"
        Me.nCodDirCli.Name = "nCodDirCli"
        Me.nCodDirCli.Visible = False
        '
        'cMensajeEstado
        '
        Me.cMensajeEstado.DataPropertyName = "cMensajeEstado"
        Me.cMensajeEstado.HeaderText = "cMensajeEstado"
        Me.cMensajeEstado.Name = "cMensajeEstado"
        Me.cMensajeEstado.Visible = False
        '
        'Almacen
        '
        Me.Almacen.DataPropertyName = "Almacen"
        Me.Almacen.HeaderText = "Almacen"
        Me.Almacen.Name = "Almacen"
        Me.Almacen.Visible = False
        '
        'cTipDocAdj
        '
        Me.cTipDocAdj.DataPropertyName = "cTipDocAdj"
        Me.cTipDocAdj.HeaderText = "cTipDocAdj"
        Me.cTipDocAdj.Name = "cTipDocAdj"
        Me.cTipDocAdj.Visible = False
        '
        'cNumDocAdj
        '
        Me.cNumDocAdj.DataPropertyName = "cNumDocAdj"
        Me.cNumDocAdj.HeaderText = "cNumDocAdj"
        Me.cNumDocAdj.Name = "cNumDocAdj"
        Me.cNumDocAdj.Visible = False
        '
        'cTipoCompAdj
        '
        Me.cTipoCompAdj.DataPropertyName = "cTipoCompAdj"
        Me.cTipoCompAdj.HeaderText = "cTipoCompAdj"
        Me.cTipoCompAdj.Name = "cTipoCompAdj"
        Me.cTipoCompAdj.Visible = False
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.Visible = False
        '
        'bAgeRet
        '
        Me.bAgeRet.DataPropertyName = "bAgeRet"
        Me.bAgeRet.HeaderText = "bAgeRet"
        Me.bAgeRet.Name = "bAgeRet"
        Me.bAgeRet.Visible = False
        '
        'bParcial
        '
        Me.bParcial.DataPropertyName = "bParcial"
        Me.bParcial.HeaderText = "bParcial"
        Me.bParcial.Name = "bParcial"
        Me.bParcial.Visible = False
        '
        'cNumGuiaCor
        '
        Me.cNumGuiaCor.DataPropertyName = "cNumGuiaCor"
        Me.cNumGuiaCor.HeaderText = "cNumGuiaCor"
        Me.cNumGuiaCor.Name = "cNumGuiaCor"
        Me.cNumGuiaCor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cNumGuiaCor.Visible = False
        '
        'cObsSut
        '
        Me.cObsSut.DataPropertyName = "cObsSut"
        Me.cObsSut.HeaderText = "cObsSut"
        Me.cObsSut.Name = "cObsSut"
        Me.cObsSut.Visible = False
        '
        'cNumTickSun
        '
        Me.cNumTickSun.DataPropertyName = "cNumTickSun"
        Me.cNumTickSun.HeaderText = "cNumTickSun"
        Me.cNumTickSun.Name = "cNumTickSun"
        Me.cNumTickSun.Visible = False
        '
        'bElectronico
        '
        Me.bElectronico.DataPropertyName = "bElectronico"
        Me.bElectronico.HeaderText = "bElectronico"
        Me.bElectronico.Name = "bElectronico"
        Me.bElectronico.Visible = False
        '
        'dFecTras
        '
        Me.dFecTras.DataPropertyName = "dFecTras"
        Me.dFecTras.HeaderText = "dFecTras"
        Me.dFecTras.Name = "dFecTras"
        Me.dFecTras.Visible = False
        '
        'cModTras
        '
        Me.cModTras.DataPropertyName = "cModTras"
        Me.cModTras.HeaderText = "cModTras"
        Me.cModTras.Name = "cModTras"
        Me.cModTras.Visible = False
        '
        'FrmConsultaGuia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblMensajeEstadoSunat)
        Me.Controls.Add(Me.btnRefreescar)
        Me.Controls.Add(Me.btnGenerarComprobante)
        Me.Controls.Add(Me.btnAnularGuia)
        Me.Controls.Add(Me.btnNuevoGuia)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecIni)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaGuia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnNuevoGuia As Button
    Friend WithEvents btnAnularGuia As Button
    Friend WithEvents btnGenerarComprobante As Button
    Friend WithEvents btnRefreescar As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents lblMensajeEstadoSunat As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnImprimir As Button
    Friend WithEvents btnEnviarSunat As Button
    Friend WithEvents btnActEstado As Button
    Friend WithEvents Seleccionar As DataGridViewCheckBoxColumn
    Friend WithEvents nCodGuia As DataGridViewTextBoxColumn
    Friend WithEvents cNumGuia As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents TipGuia As DataGridViewTextBoxColumn
    Friend WithEvents MotTrasl As DataGridViewTextBoxColumn
    Friend WithEvents cEstadoSistema As DataGridViewTextBoxColumn
    Friend WithEvents cEstadoSunat As DataGridViewTextBoxColumn
    Friend WithEvents bOfLine As DataGridViewCheckBoxColumn
    Friend WithEvents nCodCot As DataGridViewTextBoxColumn
    Friend WithEvents nCodCli As DataGridViewTextBoxColumn
    Friend WithEvents cNumDoc As DataGridViewTextBoxColumn
    Friend WithEvents cDirCli As DataGridViewTextBoxColumn
    Friend WithEvents nEstado As DataGridViewTextBoxColumn
    Friend WithEvents cTipGuia As DataGridViewTextBoxColumn
    Friend WithEvents nCodSer As DataGridViewTextBoxColumn
    Friend WithEvents cMotTrasl As DataGridViewTextBoxColumn
    Friend WithEvents cDirPart As DataGridViewTextBoxColumn
    Friend WithEvents nCodDirCli As DataGridViewTextBoxColumn
    Friend WithEvents cMensajeEstado As DataGridViewTextBoxColumn
    Friend WithEvents Almacen As DataGridViewTextBoxColumn
    Friend WithEvents cTipDocAdj As DataGridViewTextBoxColumn
    Friend WithEvents cNumDocAdj As DataGridViewTextBoxColumn
    Friend WithEvents cTipoCompAdj As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents bAgeRet As DataGridViewTextBoxColumn
    Friend WithEvents bParcial As DataGridViewTextBoxColumn
    Friend WithEvents cNumGuiaCor As DataGridViewTextBoxColumn
    Friend WithEvents cObsSut As DataGridViewTextBoxColumn
    Friend WithEvents cNumTickSun As DataGridViewTextBoxColumn
    Friend WithEvents bElectronico As DataGridViewTextBoxColumn
    Friend WithEvents dFecTras As DataGridViewTextBoxColumn
    Friend WithEvents cModTras As DataGridViewTextBoxColumn
End Class
