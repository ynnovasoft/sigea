﻿Imports System.Configuration
Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Public Class FrmConsultaGuia
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String
    Private Sub FrmConsultaGuia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        dtpFecIni.Value = DateAdd(DateInterval.Month, -6, dtpFecIni.Value)
        Call CargaCombos()
        txtBuscar.Select()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("170,180")
        Call CargaCombo(CreaDatoCombos(dt, 170), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted

    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarGuias()
    End Sub
    Sub ListarGuias()
        Dim dt As DataTable
        Dim BLGuia As New BLGuia
        dt = BLGuia.Listar(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text, MDIPrincipal.CodigoAgencia,
                                   cmbTipoBusqueda.SelectedValue, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
        dtgListado.Columns("cEstadoSistema").DefaultCellStyle.BackColor = Color.LightCyan
        dtgListado.Columns("cEstadoSunat").DefaultCellStyle.BackColor = Color.GreenYellow
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value, dtgListado.CurrentRow.Cells("cEstadoSunat").Value,
                               dtgListado.CurrentRow.Cells("bElectronico").Value, dtgListado.CurrentRow.Cells("bOfLine").Value)
            lblMensajeEstadoSunat.Text = dtgListado.CurrentRow.Cells("cObsSut").Value
        Else
            Call ValidaBotones(0, "", False, False)
            lblMensajeEstadoSunat.Text = ""
        End If
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call ListarGuias()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call ListarGuias()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEGuia As New BEGuia
            Dim BEClientes As New BECustomer
            BEGuia.gnCodGuia = dtgListado.CurrentRow.Cells("nCodGuia").Value
            BEGuia.gnCodCot = dtgListado.CurrentRow.Cells("nCodCot").Value
            BEGuia.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
            BEGuia.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BEGuia.gdFecTras = dtgListado.CurrentRow.Cells("dFecTras").Value
            BEGuia.gcNumGuia = dtgListado.CurrentRow.Cells("cNumGuia").Value
            BEGuia.gcTipGuia = dtgListado.CurrentRow.Cells("cTipGuia").Value
            BEGuia.gnCodSer = dtgListado.CurrentRow.Cells("nCodSer").Value
            BEGuia.gcMotTrasl = dtgListado.CurrentRow.Cells("cMotTrasl").Value
            BEGuia.gAlmacen = dtgListado.CurrentRow.Cells("Almacen").Value
            BEGuia.gcDirPart = dtgListado.CurrentRow.Cells("cDirPart").Value
            BEGuia.gnCodDirCli = dtgListado.CurrentRow.Cells("nCodDirCli").Value
            BEGuia.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BEGuia.gcMensaje = dtgListado.CurrentRow.Cells("cMensajeEstado").Value
            BEGuia.gcTipDocAdj = dtgListado.CurrentRow.Cells("cTipDocAdj").Value
            BEGuia.gcNumDocAdj = dtgListado.CurrentRow.Cells("cNumDocAdj").Value
            BEGuia.gcTipoCompAdj = dtgListado.CurrentRow.Cells("cTipoCompAdj").Value
            BEGuia.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BEGuia.gbParcial = dtgListado.CurrentRow.Cells("bParcial").Value
            BEGuia.gcModTras = dtgListado.CurrentRow.Cells("cModTras").Value

            BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

            Dim Actualizar As Boolean = False
            If BEGuia.gnEstado = 1 Then
                If ValidaAperturaDia() = False Then
                    Exit Sub
                End If
            End If
            FrmRegistroGuias.InicioGuia(BEGuia, BEClientes, 2, Actualizar)
            FrmRegistroGuias = Nothing
            If Actualizar = True Then
                Call ListarGuias()
            End If
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub
    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEGuia As New BEGuia
                Dim BEClientes As New BECustomer
                BEGuia.gnCodGuia = dtgListado.CurrentRow.Cells("nCodGuia").Value
                BEGuia.gnCodCot = dtgListado.CurrentRow.Cells("nCodCot").Value
                BEGuia.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
                BEGuia.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BEGuia.gdFecTras = dtgListado.CurrentRow.Cells("dFecTras").Value
                BEGuia.gcNumGuia = dtgListado.CurrentRow.Cells("cNumGuia").Value
                BEGuia.gcTipGuia = dtgListado.CurrentRow.Cells("cTipGuia").Value
                BEGuia.gnCodSer = dtgListado.CurrentRow.Cells("nCodSer").Value
                BEGuia.gcMotTrasl = dtgListado.CurrentRow.Cells("cMotTrasl").Value
                BEGuia.gAlmacen = dtgListado.CurrentRow.Cells("Almacen").Value
                BEGuia.gcDirPart = dtgListado.CurrentRow.Cells("cDirPart").Value
                BEGuia.gnCodDirCli = dtgListado.CurrentRow.Cells("nCodDirCli").Value
                BEGuia.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BEGuia.gcMensaje = dtgListado.CurrentRow.Cells("cMensajeEstado").Value
                BEGuia.gcTipDocAdj = dtgListado.CurrentRow.Cells("cTipDocAdj").Value
                BEGuia.gcNumDocAdj = dtgListado.CurrentRow.Cells("cNumDocAdj").Value
                BEGuia.gcTipoCompAdj = dtgListado.CurrentRow.Cells("cTipoCompAdj").Value
                BEGuia.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BEGuia.gbParcial = dtgListado.CurrentRow.Cells("bParcial").Value
                BEGuia.gcModTras = dtgListado.CurrentRow.Cells("cModTras").Value

                BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

                Dim Actualizar As Boolean = False
                If BEGuia.gnEstado = 1 Then
                    If ValidaAperturaDia() = False Then
                        Exit Sub
                    End If
                End If
                FrmRegistroGuias.InicioGuia(BEGuia, BEClientes, 2, Actualizar)
                FrmRegistroGuias = Nothing
                If Actualizar = True Then
                    Call ListarGuias()
                End If
            End If
        End If
    End Sub
    Sub ValidaBotones(ByVal Estado As Integer, ByVal EstadoSunat As String, ByVal Electronico As Boolean, ByVal bOffLine As Boolean)
        btnNuevoGuia.Enabled = True
        If Estado = 0 Then
            btnAnularGuia.Enabled = False
            btnGenerarComprobante.Enabled = False
            btnImprimir.Enabled = False
            btnEnviarSunat.Enabled = False
            btnActEstado.Enabled = False
            Exit Sub
        End If

        If Estado = 3 Then
            btnAnularGuia.Enabled = False
        Else
            btnAnularGuia.Enabled = True
        End If

        If Estado = 2 Then
            btnGenerarComprobante.Enabled = True
        Else
            btnGenerarComprobante.Enabled = False
        End If

        If EstadoSunat = "PENDIENTE" And Electronico Then
            btnEnviarSunat.Enabled = True
        Else
            btnEnviarSunat.Enabled = False
        End If

        If Estado = 2 Or Estado = 3 Or Estado = 4 Then
            If EstadoSunat = "ACEPTADO" Then
                btnImprimir.Enabled = True
            ElseIf EstadoSunat = "PENDIENTE" And Electronico = False Then
                btnImprimir.Enabled = True
            ElseIf EstadoSunat = "PENDIENTE" And Electronico = True Then
                If bOffLine = True Then
                    btnImprimir.Enabled = True
                Else
                    btnImprimir.Enabled = False
                End If
            ElseIf EstadoSunat = "ENVIADO" Then
                If bOffLine = True Then
                    btnImprimir.Enabled = True
                Else
                    btnImprimir.Enabled = False
                End If
            Else
                btnImprimir.Enabled = False
            End If
        Else
            btnImprimir.Enabled = False
        End If
        If EstadoSunat = "ENVIADO" And Electronico = True Then
            btnActEstado.Enabled = True
        Else
            btnActEstado.Enabled = False
        End If
    End Sub
    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs) Handles dtgListado.SelectionChanged
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value, dtgListado.CurrentRow.Cells("cEstadoSunat").Value,
                               dtgListado.CurrentRow.Cells("bElectronico").Value, dtgListado.CurrentRow.Cells("bOfLine").Value)
            lblMensajeEstadoSunat.Text = dtgListado.CurrentRow.Cells("cObsSut").Value
        Else
            Call ValidaBotones(0, "", False, False)
            lblMensajeEstadoSunat.Text = ""
        End If
    End Sub

    Private Sub btnNuevoGuia_Click(sender As Object, e As EventArgs) Handles btnNuevoGuia.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If MDIPrincipal.CodigoBaseDatos <> 1 Then
            MsgBox("No se puede realizar ningun tipo de operaciones en una BASE DE DATOS tipificada como: BACKUP, Cierre sesiÓn y vulva a ingresar al sistema indicado la BASE DE DATOS ACTUAL", MsgBoxStyle.Critical, "Aviso")
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        FrmRegistroGuias.InicioFacturacion(Nothing, Nothing, 1, Actualizar)
        FrmRegistroGuias = Nothing
        If Actualizar = True Then
            Call ListarGuias()
        End If
    End Sub

    Private Sub btnAnularGuia_Click(sender As Object, e As EventArgs) Handles btnAnularGuia.Click
        Dim Obj As New BLCommons
        Obj.ValidaEstadoMesContable(dtgListado.CurrentRow.Cells("nCodGuia").Value, dtgListado.CurrentRow.Cells("dFecReg").Value,
                                    1, cMensaje)
        If cMensaje <> "" Then
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If

        If dtgListado.Rows.Count > 0 Then
            Dim MotivoAnulacion As String = ""
            If MsgBox("¿Esta seguro que desea anular la guia?", MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
                If dtgListado.CurrentRow.Cells("nEstado").Value <> 1 Then
                    MotivoAnulacion = InputBox("Ingrese el motivo de la anulación de la guia", "ANULACIÓN GUIA")
                    If MotivoAnulacion.Trim = "" Then
                        MsgBox("Debe registrar el motivo de la anulación de la guia", MsgBoxStyle.Exclamation, "MENSAJE")
                        Exit Sub
                    End If
                End If
                Dim oDatos As New BEGuia
                oDatos.gnCodGuia = dtgListado.CurrentRow.Cells("nCodGuia").Value
                oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
                oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
                oDatos.gcMotAnu = MotivoAnulacion
                cMensaje = ""
                Dim BLGuia As New BLGuia
                If BLGuia.Anular(oDatos, cMensaje) = True Then
                    If cMensaje <> "" Then
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        Exit Sub
                    End If
                    Call ListarGuias()
                Else
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                End If
            End If
        End If
    End Sub

    Private Sub btnRefreescar_Click(sender As Object, e As EventArgs) Handles btnRefreescar.Click
        Call ListarGuias()
    End Sub

    Private Sub btnGenerarComprobante_Click(sender As Object, e As EventArgs) Handles btnGenerarComprobante.Click
        Dim BEGuiaLista As New List(Of BEGuia)
        Dim BEClientes As New BECustomer

        For i = 0 To dtgListado.Rows.Count - 1
            If dtgListado.Rows(i).Cells("Seleccionar").Value = True Then
                BEClientes.nCodCli = dtgListado.Rows(i).Cells("nCodCli").Value
                BEClientes.cNumDoc = dtgListado.Rows(i).Cells("cNumDoc").Value
                BEClientes.cDirCli = dtgListado.Rows(i).Cells("cDirCli").Value
                BEClientes.cRazSoc = dtgListado.Rows(i).Cells("cRazSoc").Value
                BEClientes.bAgeRet = dtgListado.Rows(i).Cells("bAgeRet").Value
                BEClientes.nTipPer = dtgListado.Rows(i).Cells("nTipMon").Value

                Dim oDato As New BEGuia
                oDato.gnCodGuia = dtgListado.Rows(i).Cells("nCodGuia").Value
                oDato.gcNumGuia = dtgListado.Rows(i).Cells("cNumGuiaCor").Value
                BEGuiaLista.Add(oDato)
            End If
        Next
        If BEGuiaLista.Count > 0 Then
            Dim Actualizar As Boolean = False
            If ValidaAperturaDia() = False Then
                Exit Sub
            End If
            If ValidaAperturaCaja() = False Then
                Exit Sub
            End If
            FrmTomaPedidos.InicioGuia(BEGuiaLista, BEClientes, 3, Actualizar)
            FrmTomaPedidos = Nothing
            If Actualizar = True Then
                Call ListarGuias()
            End If
        Else
            MsgBox("Debe seleccionar al menos una guia de remisión para generar el comprobante", MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If

    End Sub

    Private Sub FrmConsultaGuia_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevoGuia.Enabled = True And btnNuevoGuia.Visible = True Then
            Call btnNuevoGuia_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnularGuia.Enabled = True And btnAnularGuia.Visible = True Then
            Call btnAnularGuia_Click(sender, e)
        ElseIf e.KeyCode = Keys.F5 And btnRefreescar.Enabled = True And btnRefreescar.Visible = True Then
            Call btnRefreescar_Click(sender, e)
        End If
    End Sub

    Private Sub btnNuevoGuia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevoGuia.KeyPress
        ValidaSonidoEnter(e)
    End Sub

    Private Sub btnRefreescar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnRefreescar.KeyPress
        ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAnularGuia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnularGuia.KeyPress
        ValidaSonidoEnter(e)
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        If dtgListado.CurrentRow.Cells("bElectronico").Value = True Then
            Dim RutaEmpresa As String = ""
            If MDIPrincipal.CodigoEmpresa = 1 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            End If
            Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
            RutaPdf = RutaPdf + dtgListado.CurrentRow.Cells("cTipGuia").Value + "-" + dtgListado.CurrentRow.Cells("cNumGuia").Value + ".pdf"
            Call ModeloImpresionGuiaPdf(dtgListado.CurrentRow.Cells("nCodGuia").Value)
            Process.Start(RutaPdf)
        Else
            FrmReportes.ImpresionGuia(dtgListado.CurrentRow.Cells("nCodGuia").Value)
            FrmReportes = Nothing
        End If
    End Sub
    Private Sub btnEnviarSunat_Click(sender As Object, e As EventArgs) Handles btnEnviarSunat.Click
        cMensaje = ""
        If MsgBox("¿Esta seguro que desea enviar a la sunat la guia seleccionada?", MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
            Dim cNumTickSun As String = ""
            cMensaje = GenerarGuiaElectronica(dtgListado.CurrentRow.Cells("nCodGuia").Value, cNumTickSun)
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                If dtgListado.CurrentRow.Cells("bOfLine").Value = False Then
                    If MsgBox("¿Actualmente existen intermitencias en la plataforma de SUNAT, y no se pueden enviar los comprobantes:¿Desea enviar de Manera OffLine?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                        EnvioComprobantesOffLine(dtgListado.CurrentRow.Cells("nCodGuia").Value, dtgListado.CurrentRow.Cells("cTipGuia").Value)
                    End If
                End If
            Else
                cMensaje = ConsultarEstadoEnvioGuia(dtgListado.CurrentRow.Cells("cTipGuia").Value, dtgListado.CurrentRow.Cells("cNumGuia").Value,
                                                cNumTickSun, dtgListado.CurrentRow.Cells("nCodGuia").Value)
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    If dtgListado.CurrentRow.Cells("bOfLine").Value = False Then
                        If MsgBox("¿Actualmente existen intermitencias en la plataforma de SUNAT, y no se pueden enviar los comprobantes:¿Desea enviar de Manera OffLine?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                            EnvioComprobantesOffLine(dtgListado.CurrentRow.Cells("nCodGuia").Value, dtgListado.CurrentRow.Cells("cTipGuia").Value)
                        End If
                    End If
                End If
            End If
            Call ListarGuias()
        End If
    End Sub
    Sub EnvioComprobantesOffLine(ByVal Codigo As Integer, ByVal TipoComprobante As Integer)
        Dim Obj As New BLCommons
        cMensaje = ""
        If Obj.EnvioComprobantesOffLine(Codigo, TipoComprobante, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnGenerarComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGenerarComprobante.KeyPress
        ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEnviarSunat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEnviarSunat.KeyPress
        ValidaSonidoEnter(e)
    End Sub

    Private Sub lblMensajeEstadoSunat_Click(sender As Object, e As EventArgs) Handles lblMensajeEstadoSunat.Click

    End Sub

    Private Sub btnActEstado_Click(sender As Object, e As EventArgs) Handles btnActEstado.Click
        cMensaje = ""
        cMensaje = ConsultarEstadoEnvioGuia(dtgListado.CurrentRow.Cells("cTipGuia").Value, dtgListado.CurrentRow.Cells("cNumGuia").Value,
                                                dtgListado.CurrentRow.Cells("cNumTickSun").Value, dtgListado.CurrentRow.Cells("nCodGuia").Value)
        If cMensaje <> "" Then
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            If dtgListado.CurrentRow.Cells("bOfLine").Value = False Then
                If MsgBox("¿Actualmente existen intermitencias en la plataforma de SUNAT, y no se pueden enviar los comprobantes:¿Desea enviar de Manera OffLine?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                    EnvioComprobantesOffLine(dtgListado.CurrentRow.Cells("nCodGuia").Value, dtgListado.CurrentRow.Cells("cTipGuia").Value)
                End If
            End If
        End If
        Call ListarGuias()
    End Sub

    Private Sub btnImprimir_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnImprimir.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnActEstado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnActEstado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedValueChanged
        If (TypeOf cmbTipoBusqueda.SelectedValue IsNot DataRowView) Then
            txtBuscar.Clear()
            txtBuscar.Select()
            Call ListarGuias()
        End If
    End Sub
End Class