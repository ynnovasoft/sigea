﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaImpresionComprobantes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodPed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumComp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstadoSistema = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstadoSunat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bOfLine = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.cNumTickSun = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ruc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.serie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.num = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cObsSut = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNomXmlBaj = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAplDedAnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonAnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMensajeEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodCot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecVen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDirCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cOrdComp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipAfecIGV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipOpe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nVenGra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nIGV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipDocAdj = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAplGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodDocRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipoComRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumComRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodTipNot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMotNot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumAdj = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonRet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpNetPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAgeRet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodVen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nSubTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodGui = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAplicaGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bVenSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAplDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCretoLet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFecIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblMensajeEstadoSunat = New System.Windows.Forms.Label()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnValidarEstadoSunat = New System.Windows.Forms.Button()
        Me.btnActualizarBaja = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnActEstado = New System.Windows.Forms.Button()
        Me.btnDarBaja = New System.Windows.Forms.Button()
        Me.btnEnviarSunat = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnRefreescar = New System.Windows.Forms.Button()
        Me.btnGenerarDebito = New System.Windows.Forms.Button()
        Me.btnGenerarNota = New System.Windows.Forms.Button()
        Me.btnGenerarGuia = New System.Windows.Forms.Button()
        Me.btnNuevoComprobante = New System.Windows.Forms.Button()
        Me.btnAnularComprobante = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlsTitulo.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodPed, Me.cNumComp, Me.dFecReg, Me.cRazSoc, Me.TipCom, Me.CondPag, Me.cMoneda, Me.nImpTot, Me.cEstadoSistema, Me.cEstadoSunat, Me.bOfLine, Me.cNumTickSun, Me.ruc, Me.serie, Me.num, Me.cObsSut, Me.cNomXmlBaj, Me.bAplDedAnt, Me.nMonAnt, Me.cMensajeEstado, Me.nCodCot, Me.nEstado, Me.dFecVen, Me.cNumDoc, Me.cDirCli, Me.nTipCam, Me.nCondPag, Me.nTipMon, Me.cTipCom, Me.nCodSer, Me.cOrdComp, Me.nTipAfecIGV, Me.cTipOpe, Me.nVenGra, Me.nIGV, Me.cTipDocAdj, Me.bAplGuia, Me.nCodDocRef, Me.cTipoComRef, Me.cNumComRef, Me.cCodTipNot, Me.cMotNot, Me.cNumAdj, Me.nMonRet, Me.nImpNetPag, Me.bAgeRet, Me.nCodVen, Me.nSubTot, Me.nMonDes, Me.cCodGui, Me.bAplicaGuia, Me.bVenSer, Me.bAplDet, Me.nCretoLet})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 67)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1229, 495)
        Me.dtgListado.TabIndex = 1
        '
        'nCodPed
        '
        Me.nCodPed.DataPropertyName = "nCodPed"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodPed.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodPed.HeaderText = "nCodPed"
        Me.nCodPed.Name = "nCodPed"
        Me.nCodPed.ReadOnly = True
        Me.nCodPed.Visible = False
        '
        'cNumComp
        '
        Me.cNumComp.DataPropertyName = "cNumComp"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumComp.DefaultCellStyle = DataGridViewCellStyle3
        Me.cNumComp.HeaderText = "N° Comprobante"
        Me.cNumComp.Name = "cNumComp"
        Me.cNumComp.ReadOnly = True
        Me.cNumComp.Width = 127
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle4
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.Width = 80
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle5
        Me.cRazSoc.HeaderText = "Cliente"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 285
        '
        'TipCom
        '
        Me.TipCom.DataPropertyName = "TipCom"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipCom.DefaultCellStyle = DataGridViewCellStyle6
        Me.TipCom.HeaderText = "Tipo Comprobante"
        Me.TipCom.Name = "TipCom"
        Me.TipCom.ReadOnly = True
        Me.TipCom.Width = 135
        '
        'CondPag
        '
        Me.CondPag.DataPropertyName = "CondPag"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.CondPag.DefaultCellStyle = DataGridViewCellStyle7
        Me.CondPag.HeaderText = "Condición Pago"
        Me.CondPag.Name = "CondPag"
        Me.CondPag.ReadOnly = True
        Me.CondPag.Width = 136
        '
        'cMoneda
        '
        Me.cMoneda.DataPropertyName = "cMoneda"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cMoneda.DefaultCellStyle = DataGridViewCellStyle8
        Me.cMoneda.HeaderText = "Moneda"
        Me.cMoneda.Name = "cMoneda"
        Me.cMoneda.ReadOnly = True
        Me.cMoneda.Width = 70
        '
        'nImpTot
        '
        Me.nImpTot.DataPropertyName = "nImpTot"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.nImpTot.DefaultCellStyle = DataGridViewCellStyle9
        Me.nImpTot.HeaderText = "Imp.Total"
        Me.nImpTot.Name = "nImpTot"
        Me.nImpTot.ReadOnly = True
        Me.nImpTot.Width = 90
        '
        'cEstadoSistema
        '
        Me.cEstadoSistema.DataPropertyName = "cEstadoSistema"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstadoSistema.DefaultCellStyle = DataGridViewCellStyle10
        Me.cEstadoSistema.HeaderText = "Estado Sistema"
        Me.cEstadoSistema.Name = "cEstadoSistema"
        Me.cEstadoSistema.ReadOnly = True
        Me.cEstadoSistema.Width = 125
        '
        'cEstadoSunat
        '
        Me.cEstadoSunat.DataPropertyName = "cEstadoSunat"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstadoSunat.DefaultCellStyle = DataGridViewCellStyle11
        Me.cEstadoSunat.HeaderText = "Estado Sunat"
        Me.cEstadoSunat.Name = "cEstadoSunat"
        Me.cEstadoSunat.ReadOnly = True
        Me.cEstadoSunat.Width = 110
        '
        'bOfLine
        '
        Me.bOfLine.DataPropertyName = "bOfLine"
        Me.bOfLine.HeaderText = "OffLine"
        Me.bOfLine.Name = "bOfLine"
        Me.bOfLine.ReadOnly = True
        Me.bOfLine.Width = 50
        '
        'cNumTickSun
        '
        Me.cNumTickSun.DataPropertyName = "cNumTickSun"
        Me.cNumTickSun.HeaderText = "cNumTickSun"
        Me.cNumTickSun.Name = "cNumTickSun"
        Me.cNumTickSun.ReadOnly = True
        Me.cNumTickSun.Visible = False
        '
        'ruc
        '
        Me.ruc.DataPropertyName = "ruc"
        Me.ruc.HeaderText = "ruc"
        Me.ruc.Name = "ruc"
        Me.ruc.ReadOnly = True
        Me.ruc.Visible = False
        '
        'serie
        '
        Me.serie.DataPropertyName = "serie"
        Me.serie.HeaderText = "serie"
        Me.serie.Name = "serie"
        Me.serie.ReadOnly = True
        Me.serie.Visible = False
        '
        'num
        '
        Me.num.DataPropertyName = "num"
        Me.num.HeaderText = "num"
        Me.num.Name = "num"
        Me.num.ReadOnly = True
        Me.num.Visible = False
        '
        'cObsSut
        '
        Me.cObsSut.DataPropertyName = "cObsSut"
        Me.cObsSut.HeaderText = "cObsSut"
        Me.cObsSut.Name = "cObsSut"
        Me.cObsSut.ReadOnly = True
        Me.cObsSut.Visible = False
        '
        'cNomXmlBaj
        '
        Me.cNomXmlBaj.DataPropertyName = "cNomXmlBaj"
        Me.cNomXmlBaj.HeaderText = "cNomXmlBaj"
        Me.cNomXmlBaj.Name = "cNomXmlBaj"
        Me.cNomXmlBaj.ReadOnly = True
        Me.cNomXmlBaj.Visible = False
        '
        'bAplDedAnt
        '
        Me.bAplDedAnt.DataPropertyName = "bAplDedAnt"
        Me.bAplDedAnt.HeaderText = "bAplDedAnt"
        Me.bAplDedAnt.Name = "bAplDedAnt"
        Me.bAplDedAnt.ReadOnly = True
        Me.bAplDedAnt.Visible = False
        '
        'nMonAnt
        '
        Me.nMonAnt.DataPropertyName = "nMonAnt"
        Me.nMonAnt.HeaderText = "nMonAnt"
        Me.nMonAnt.Name = "nMonAnt"
        Me.nMonAnt.ReadOnly = True
        Me.nMonAnt.Visible = False
        '
        'cMensajeEstado
        '
        Me.cMensajeEstado.DataPropertyName = "cMensajeEstado"
        Me.cMensajeEstado.HeaderText = "cMensajeEstado"
        Me.cMensajeEstado.Name = "cMensajeEstado"
        Me.cMensajeEstado.ReadOnly = True
        Me.cMensajeEstado.Visible = False
        '
        'nCodCot
        '
        Me.nCodCot.DataPropertyName = "nCodCot"
        Me.nCodCot.HeaderText = "nCodCot"
        Me.nCodCot.Name = "nCodCot"
        Me.nCodCot.ReadOnly = True
        Me.nCodCot.Visible = False
        '
        'nEstado
        '
        Me.nEstado.DataPropertyName = "nEstado"
        Me.nEstado.HeaderText = "nEstado"
        Me.nEstado.Name = "nEstado"
        Me.nEstado.ReadOnly = True
        Me.nEstado.Visible = False
        '
        'dFecVen
        '
        Me.dFecVen.DataPropertyName = "dFecVen"
        Me.dFecVen.HeaderText = "dFecVen"
        Me.dFecVen.Name = "dFecVen"
        Me.dFecVen.ReadOnly = True
        Me.dFecVen.Visible = False
        '
        'cNumDoc
        '
        Me.cNumDoc.DataPropertyName = "cNumDoc"
        Me.cNumDoc.HeaderText = "cNumDoc"
        Me.cNumDoc.Name = "cNumDoc"
        Me.cNumDoc.ReadOnly = True
        Me.cNumDoc.Visible = False
        '
        'cDirCli
        '
        Me.cDirCli.DataPropertyName = "cDirCli"
        Me.cDirCli.HeaderText = "cDirCli"
        Me.cDirCli.Name = "cDirCli"
        Me.cDirCli.ReadOnly = True
        Me.cDirCli.Visible = False
        '
        'nTipCam
        '
        Me.nTipCam.DataPropertyName = "nTipCam"
        Me.nTipCam.HeaderText = "nTipCam"
        Me.nTipCam.Name = "nTipCam"
        Me.nTipCam.ReadOnly = True
        Me.nTipCam.Visible = False
        '
        'nCondPag
        '
        Me.nCondPag.DataPropertyName = "nCondPag"
        Me.nCondPag.HeaderText = "nCondPag"
        Me.nCondPag.Name = "nCondPag"
        Me.nCondPag.ReadOnly = True
        Me.nCondPag.Visible = False
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.Visible = False
        '
        'cTipCom
        '
        Me.cTipCom.DataPropertyName = "cTipCom"
        Me.cTipCom.HeaderText = "cTipCom"
        Me.cTipCom.Name = "cTipCom"
        Me.cTipCom.ReadOnly = True
        Me.cTipCom.Visible = False
        '
        'nCodSer
        '
        Me.nCodSer.DataPropertyName = "nCodSer"
        Me.nCodSer.HeaderText = "nCodSer"
        Me.nCodSer.Name = "nCodSer"
        Me.nCodSer.ReadOnly = True
        Me.nCodSer.Visible = False
        '
        'cOrdComp
        '
        Me.cOrdComp.DataPropertyName = "cOrdComp"
        Me.cOrdComp.HeaderText = "cOrdComp"
        Me.cOrdComp.Name = "cOrdComp"
        Me.cOrdComp.ReadOnly = True
        Me.cOrdComp.Visible = False
        '
        'nTipAfecIGV
        '
        Me.nTipAfecIGV.DataPropertyName = "nTipAfecIGV"
        Me.nTipAfecIGV.HeaderText = "nTipAfecIGV"
        Me.nTipAfecIGV.Name = "nTipAfecIGV"
        Me.nTipAfecIGV.ReadOnly = True
        Me.nTipAfecIGV.Visible = False
        '
        'cTipOpe
        '
        Me.cTipOpe.DataPropertyName = "cTipOpe"
        Me.cTipOpe.HeaderText = "cTipOpe"
        Me.cTipOpe.Name = "cTipOpe"
        Me.cTipOpe.ReadOnly = True
        Me.cTipOpe.Visible = False
        '
        'nVenGra
        '
        Me.nVenGra.DataPropertyName = "nVenGra"
        Me.nVenGra.HeaderText = "nVenGra"
        Me.nVenGra.Name = "nVenGra"
        Me.nVenGra.ReadOnly = True
        Me.nVenGra.Visible = False
        '
        'nIGV
        '
        Me.nIGV.DataPropertyName = "nIGV"
        Me.nIGV.HeaderText = "nIGV"
        Me.nIGV.Name = "nIGV"
        Me.nIGV.ReadOnly = True
        Me.nIGV.Visible = False
        '
        'cTipDocAdj
        '
        Me.cTipDocAdj.DataPropertyName = "cTipDocAdj"
        Me.cTipDocAdj.HeaderText = "cTipDocAdj"
        Me.cTipDocAdj.Name = "cTipDocAdj"
        Me.cTipDocAdj.ReadOnly = True
        Me.cTipDocAdj.Visible = False
        '
        'bAplGuia
        '
        Me.bAplGuia.DataPropertyName = "bAplGuia"
        Me.bAplGuia.HeaderText = "bAplGuia"
        Me.bAplGuia.Name = "bAplGuia"
        Me.bAplGuia.ReadOnly = True
        Me.bAplGuia.Visible = False
        '
        'nCodDocRef
        '
        Me.nCodDocRef.DataPropertyName = "nCodDocRef"
        Me.nCodDocRef.HeaderText = "nCodDocRef"
        Me.nCodDocRef.Name = "nCodDocRef"
        Me.nCodDocRef.ReadOnly = True
        Me.nCodDocRef.Visible = False
        '
        'cTipoComRef
        '
        Me.cTipoComRef.DataPropertyName = "cTipoComRef"
        Me.cTipoComRef.HeaderText = "cTipoComRef"
        Me.cTipoComRef.Name = "cTipoComRef"
        Me.cTipoComRef.ReadOnly = True
        Me.cTipoComRef.Visible = False
        '
        'cNumComRef
        '
        Me.cNumComRef.DataPropertyName = "cNumComRef"
        Me.cNumComRef.HeaderText = "cNumComRef"
        Me.cNumComRef.Name = "cNumComRef"
        Me.cNumComRef.ReadOnly = True
        Me.cNumComRef.Visible = False
        '
        'cCodTipNot
        '
        Me.cCodTipNot.DataPropertyName = "cCodTipNot"
        Me.cCodTipNot.HeaderText = "cCodTipNot"
        Me.cCodTipNot.Name = "cCodTipNot"
        Me.cCodTipNot.ReadOnly = True
        Me.cCodTipNot.Visible = False
        '
        'cMotNot
        '
        Me.cMotNot.DataPropertyName = "cMotNot"
        Me.cMotNot.HeaderText = "cMotNot"
        Me.cMotNot.Name = "cMotNot"
        Me.cMotNot.ReadOnly = True
        Me.cMotNot.Visible = False
        '
        'cNumAdj
        '
        Me.cNumAdj.DataPropertyName = "cNumAdj"
        Me.cNumAdj.HeaderText = "cNumAdj"
        Me.cNumAdj.Name = "cNumAdj"
        Me.cNumAdj.ReadOnly = True
        Me.cNumAdj.Visible = False
        '
        'nMonRet
        '
        Me.nMonRet.DataPropertyName = "nMonRet"
        Me.nMonRet.HeaderText = "nMonRet"
        Me.nMonRet.Name = "nMonRet"
        Me.nMonRet.ReadOnly = True
        Me.nMonRet.Visible = False
        '
        'nImpNetPag
        '
        Me.nImpNetPag.DataPropertyName = "nImpNetPag"
        Me.nImpNetPag.HeaderText = "nImpNetPag"
        Me.nImpNetPag.Name = "nImpNetPag"
        Me.nImpNetPag.ReadOnly = True
        Me.nImpNetPag.Visible = False
        '
        'bAgeRet
        '
        Me.bAgeRet.DataPropertyName = "bAgeRet"
        Me.bAgeRet.HeaderText = "bAgeRet"
        Me.bAgeRet.Name = "bAgeRet"
        Me.bAgeRet.ReadOnly = True
        Me.bAgeRet.Visible = False
        '
        'nCodVen
        '
        Me.nCodVen.DataPropertyName = "nCodVen"
        Me.nCodVen.HeaderText = "nCodVen"
        Me.nCodVen.Name = "nCodVen"
        Me.nCodVen.ReadOnly = True
        Me.nCodVen.Visible = False
        '
        'nSubTot
        '
        Me.nSubTot.DataPropertyName = "nSubTot"
        Me.nSubTot.HeaderText = "nSubTot"
        Me.nSubTot.Name = "nSubTot"
        Me.nSubTot.ReadOnly = True
        Me.nSubTot.Visible = False
        '
        'nMonDes
        '
        Me.nMonDes.DataPropertyName = "nMonDes"
        Me.nMonDes.HeaderText = "nMonDes"
        Me.nMonDes.Name = "nMonDes"
        Me.nMonDes.ReadOnly = True
        Me.nMonDes.Visible = False
        '
        'cCodGui
        '
        Me.cCodGui.DataPropertyName = "cCodGui"
        Me.cCodGui.HeaderText = "cCodGui"
        Me.cCodGui.Name = "cCodGui"
        Me.cCodGui.ReadOnly = True
        Me.cCodGui.Visible = False
        '
        'bAplicaGuia
        '
        Me.bAplicaGuia.DataPropertyName = "bAplicaGuia"
        Me.bAplicaGuia.HeaderText = "bAplicaGuia"
        Me.bAplicaGuia.Name = "bAplicaGuia"
        Me.bAplicaGuia.ReadOnly = True
        Me.bAplicaGuia.Visible = False
        '
        'bVenSer
        '
        Me.bVenSer.DataPropertyName = "bVenSer"
        Me.bVenSer.HeaderText = "bVenSer"
        Me.bVenSer.Name = "bVenSer"
        Me.bVenSer.ReadOnly = True
        Me.bVenSer.Visible = False
        '
        'bAplDet
        '
        Me.bAplDet.DataPropertyName = "bAplDet"
        Me.bAplDet.HeaderText = "bAplDet"
        Me.bAplDet.Name = "bAplDet"
        Me.bAplDet.ReadOnly = True
        Me.bAplDet.Visible = False
        '
        'nCretoLet
        '
        Me.nCretoLet.DataPropertyName = "nCretoLet"
        Me.nCretoLet.HeaderText = "nCretoLet"
        Me.nCretoLet.Name = "nCretoLet"
        Me.nCretoLet.ReadOnly = True
        Me.nCretoLet.Visible = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(414, 43)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(183, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Desde:"
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(234, 43)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(178, 22)
        Me.cmbTipoBusqueda.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(117, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 14)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta:"
        '
        'dtpFecIni
        '
        Me.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecIni.Location = New System.Drawing.Point(9, 43)
        Me.dtpFecIni.Name = "dtpFecIni"
        Me.dtpFecIni.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecIni.TabIndex = 3
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(120, 43)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecFin.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(231, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'lblMensajeEstadoSunat
        '
        Me.lblMensajeEstadoSunat.BackColor = System.Drawing.Color.White
        Me.lblMensajeEstadoSunat.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeEstadoSunat.Location = New System.Drawing.Point(664, 28)
        Me.lblMensajeEstadoSunat.Name = "lblMensajeEstadoSunat"
        Me.lblMensajeEstadoSunat.Size = New System.Drawing.Size(667, 35)
        Me.lblMensajeEstadoSunat.TabIndex = 128
        Me.lblMensajeEstadoSunat.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(341, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL DE COMPROBANTES ELECTRONICOS"
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnValidarEstadoSunat)
        Me.GroupBox2.Controls.Add(Me.btnActualizarBaja)
        Me.GroupBox2.Controls.Add(Me.btnImprimir)
        Me.GroupBox2.Controls.Add(Me.btnActEstado)
        Me.GroupBox2.Controls.Add(Me.btnDarBaja)
        Me.GroupBox2.Controls.Add(Me.btnEnviarSunat)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.Location = New System.Drawing.Point(1242, 60)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(90, 502)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "SUNAT"
        '
        'btnValidarEstadoSunat
        '
        Me.btnValidarEstadoSunat.FlatAppearance.BorderSize = 0
        Me.btnValidarEstadoSunat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnValidarEstadoSunat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnValidarEstadoSunat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnValidarEstadoSunat.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValidarEstadoSunat.Image = Global.Presentacion.My.Resources.Resources.ValidarSunat_32
        Me.btnValidarEstadoSunat.Location = New System.Drawing.Point(5, 323)
        Me.btnValidarEstadoSunat.Name = "btnValidarEstadoSunat"
        Me.btnValidarEstadoSunat.Size = New System.Drawing.Size(80, 82)
        Me.btnValidarEstadoSunat.TabIndex = 17
        Me.btnValidarEstadoSunat.Text = "Validar Estado Sunat"
        Me.btnValidarEstadoSunat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnValidarEstadoSunat.UseVisualStyleBackColor = True
        '
        'btnActualizarBaja
        '
        Me.btnActualizarBaja.FlatAppearance.BorderSize = 0
        Me.btnActualizarBaja.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnActualizarBaja.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnActualizarBaja.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizarBaja.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizarBaja.Image = Global.Presentacion.My.Resources.Resources.Baja_32
        Me.btnActualizarBaja.Location = New System.Drawing.Point(5, 430)
        Me.btnActualizarBaja.Name = "btnActualizarBaja"
        Me.btnActualizarBaja.Size = New System.Drawing.Size(80, 52)
        Me.btnActualizarBaja.TabIndex = 16
        Me.btnActualizarBaja.Text = "Act. Baja"
        Me.btnActualizarBaja.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnActualizarBaja.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.FlatAppearance.BorderSize = 0
        Me.btnImprimir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnImprimir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Image = Global.Presentacion.My.Resources.Resources.Print_22
        Me.btnImprimir.Location = New System.Drawing.Point(5, 170)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(80, 42)
        Me.btnImprimir.TabIndex = 14
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnActEstado
        '
        Me.btnActEstado.FlatAppearance.BorderSize = 0
        Me.btnActEstado.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnActEstado.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnActEstado.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActEstado.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActEstado.Image = Global.Presentacion.My.Resources.Resources.ActualizarSunat_24
        Me.btnActEstado.Location = New System.Drawing.Point(5, 236)
        Me.btnActEstado.Name = "btnActEstado"
        Me.btnActEstado.Size = New System.Drawing.Size(80, 61)
        Me.btnActEstado.TabIndex = 15
        Me.btnActEstado.Text = "Actualizar Estado"
        Me.btnActEstado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnActEstado.UseVisualStyleBackColor = True
        '
        'btnDarBaja
        '
        Me.btnDarBaja.FlatAppearance.BorderSize = 0
        Me.btnDarBaja.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnDarBaja.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnDarBaja.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDarBaja.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDarBaja.Image = Global.Presentacion.My.Resources.Resources.Baja_24
        Me.btnDarBaja.Location = New System.Drawing.Point(5, 104)
        Me.btnDarBaja.Name = "btnDarBaja"
        Me.btnDarBaja.Size = New System.Drawing.Size(80, 42)
        Me.btnDarBaja.TabIndex = 13
        Me.btnDarBaja.Text = "Dar Baja"
        Me.btnDarBaja.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnDarBaja.UseVisualStyleBackColor = True
        '
        'btnEnviarSunat
        '
        Me.btnEnviarSunat.FlatAppearance.BorderSize = 0
        Me.btnEnviarSunat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEnviarSunat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEnviarSunat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnviarSunat.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarSunat.Image = Global.Presentacion.My.Resources.Resources.Sunat_22
        Me.btnEnviarSunat.Location = New System.Drawing.Point(5, 18)
        Me.btnEnviarSunat.Name = "btnEnviarSunat"
        Me.btnEnviarSunat.Size = New System.Drawing.Size(80, 62)
        Me.btnEnviarSunat.TabIndex = 12
        Me.btnEnviarSunat.Text = "Enviar Sunat"
        Me.btnEnviarSunat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnEnviarSunat.UseVisualStyleBackColor = True
        '
        'btnRefreescar
        '
        Me.btnRefreescar.FlatAppearance.BorderSize = 0
        Me.btnRefreescar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreescar.Image = Global.Presentacion.My.Resources.Resources.Refrescar_24
        Me.btnRefreescar.Location = New System.Drawing.Point(598, 42)
        Me.btnRefreescar.Name = "btnRefreescar"
        Me.btnRefreescar.Size = New System.Drawing.Size(22, 22)
        Me.btnRefreescar.TabIndex = 2
        Me.btnRefreescar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip.SetToolTip(Me.btnRefreescar, "Actualizar Listado(F5)")
        Me.btnRefreescar.UseVisualStyleBackColor = True
        '
        'btnGenerarDebito
        '
        Me.btnGenerarDebito.FlatAppearance.BorderSize = 0
        Me.btnGenerarDebito.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarDebito.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarDebito.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarDebito.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarDebito.Image = Global.Presentacion.My.Resources.Resources.CuentasxCobrar_32
        Me.btnGenerarDebito.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerarDebito.Location = New System.Drawing.Point(513, 576)
        Me.btnGenerarDebito.Name = "btnGenerarDebito"
        Me.btnGenerarDebito.Size = New System.Drawing.Size(122, 26)
        Me.btnGenerarDebito.TabIndex = 10
        Me.btnGenerarDebito.Text = "Nota Débito"
        Me.btnGenerarDebito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerarDebito.UseVisualStyleBackColor = True
        '
        'btnGenerarNota
        '
        Me.btnGenerarNota.FlatAppearance.BorderSize = 0
        Me.btnGenerarNota.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarNota.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarNota.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarNota.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarNota.Image = Global.Presentacion.My.Resources.Resources.CuentasxPagar_32
        Me.btnGenerarNota.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerarNota.Location = New System.Drawing.Point(372, 576)
        Me.btnGenerarNota.Name = "btnGenerarNota"
        Me.btnGenerarNota.Size = New System.Drawing.Size(124, 26)
        Me.btnGenerarNota.TabIndex = 9
        Me.btnGenerarNota.Text = "Nota Crédito"
        Me.btnGenerarNota.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerarNota.UseVisualStyleBackColor = True
        '
        'btnGenerarGuia
        '
        Me.btnGenerarGuia.FlatAppearance.BorderSize = 0
        Me.btnGenerarGuia.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarGuia.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarGuia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarGuia.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarGuia.Image = Global.Presentacion.My.Resources.Resources.Title_20
        Me.btnGenerarGuia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerarGuia.Location = New System.Drawing.Point(243, 576)
        Me.btnGenerarGuia.Name = "btnGenerarGuia"
        Me.btnGenerarGuia.Size = New System.Drawing.Size(111, 26)
        Me.btnGenerarGuia.TabIndex = 8
        Me.btnGenerarGuia.Text = "Generar Guía"
        Me.btnGenerarGuia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerarGuia.UseVisualStyleBackColor = True
        '
        'btnNuevoComprobante
        '
        Me.btnNuevoComprobante.FlatAppearance.BorderSize = 0
        Me.btnNuevoComprobante.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoComprobante.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoComprobante.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoComprobante.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevoComprobante.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevoComprobante.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoComprobante.Location = New System.Drawing.Point(9, 576)
        Me.btnNuevoComprobante.Name = "btnNuevoComprobante"
        Me.btnNuevoComprobante.Size = New System.Drawing.Size(100, 26)
        Me.btnNuevoComprobante.TabIndex = 6
        Me.btnNuevoComprobante.Text = "Nuevo(F1)"
        Me.btnNuevoComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoComprobante.UseVisualStyleBackColor = True
        '
        'btnAnularComprobante
        '
        Me.btnAnularComprobante.FlatAppearance.BorderSize = 0
        Me.btnAnularComprobante.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularComprobante.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularComprobante.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnularComprobante.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnularComprobante.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnularComprobante.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnularComprobante.Location = New System.Drawing.Point(127, 576)
        Me.btnAnularComprobante.Name = "btnAnularComprobante"
        Me.btnAnularComprobante.Size = New System.Drawing.Size(99, 26)
        Me.btnAnularComprobante.TabIndex = 7
        Me.btnAnularComprobante.Text = "Anular(F4)"
        Me.btnAnularComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnularComprobante.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'FrmConsultaImpresionComprobantes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnGenerarDebito)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnGenerarNota)
        Me.Controls.Add(Me.btnGenerarGuia)
        Me.Controls.Add(Me.btnNuevoComprobante)
        Me.Controls.Add(Me.btnAnularComprobante)
        Me.Controls.Add(Me.lblMensajeEstadoSunat)
        Me.Controls.Add(Me.btnRefreescar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecIni)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaImpresionComprobantes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnRefreescar As Button
    Friend WithEvents lblMensajeEstadoSunat As Label
    Friend WithEvents pnMover As Panel
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnAnularComprobante As Button
    Friend WithEvents btnNuevoComprobante As Button
    Friend WithEvents btnGenerarGuia As Button
    Friend WithEvents btnGenerarNota As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnEnviarSunat As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents btnImprimir As Button
    Friend WithEvents btnActEstado As Button
    Friend WithEvents btnDarBaja As Button
    Friend WithEvents btnGenerarDebito As Button
    Friend WithEvents btnActualizarBaja As Button
    Friend WithEvents btnValidarEstadoSunat As Button
    Friend WithEvents nCodPed As DataGridViewTextBoxColumn
    Friend WithEvents cNumComp As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents TipCom As DataGridViewTextBoxColumn
    Friend WithEvents CondPag As DataGridViewTextBoxColumn
    Friend WithEvents cMoneda As DataGridViewTextBoxColumn
    Friend WithEvents nImpTot As DataGridViewTextBoxColumn
    Friend WithEvents cEstadoSistema As DataGridViewTextBoxColumn
    Friend WithEvents cEstadoSunat As DataGridViewTextBoxColumn
    Friend WithEvents bOfLine As DataGridViewCheckBoxColumn
    Friend WithEvents cNumTickSun As DataGridViewTextBoxColumn
    Friend WithEvents ruc As DataGridViewTextBoxColumn
    Friend WithEvents serie As DataGridViewTextBoxColumn
    Friend WithEvents num As DataGridViewTextBoxColumn
    Friend WithEvents cObsSut As DataGridViewTextBoxColumn
    Friend WithEvents cNomXmlBaj As DataGridViewTextBoxColumn
    Friend WithEvents bAplDedAnt As DataGridViewTextBoxColumn
    Friend WithEvents nMonAnt As DataGridViewTextBoxColumn
    Friend WithEvents cMensajeEstado As DataGridViewTextBoxColumn
    Friend WithEvents nCodCot As DataGridViewTextBoxColumn
    Friend WithEvents nEstado As DataGridViewTextBoxColumn
    Friend WithEvents dFecVen As DataGridViewTextBoxColumn
    Friend WithEvents cNumDoc As DataGridViewTextBoxColumn
    Friend WithEvents cDirCli As DataGridViewTextBoxColumn
    Friend WithEvents nTipCam As DataGridViewTextBoxColumn
    Friend WithEvents nCondPag As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents cTipCom As DataGridViewTextBoxColumn
    Friend WithEvents nCodSer As DataGridViewTextBoxColumn
    Friend WithEvents cOrdComp As DataGridViewTextBoxColumn
    Friend WithEvents nTipAfecIGV As DataGridViewTextBoxColumn
    Friend WithEvents cTipOpe As DataGridViewTextBoxColumn
    Friend WithEvents nVenGra As DataGridViewTextBoxColumn
    Friend WithEvents nIGV As DataGridViewTextBoxColumn
    Friend WithEvents cTipDocAdj As DataGridViewTextBoxColumn
    Friend WithEvents bAplGuia As DataGridViewTextBoxColumn
    Friend WithEvents nCodDocRef As DataGridViewTextBoxColumn
    Friend WithEvents cTipoComRef As DataGridViewTextBoxColumn
    Friend WithEvents cNumComRef As DataGridViewTextBoxColumn
    Friend WithEvents cCodTipNot As DataGridViewTextBoxColumn
    Friend WithEvents cMotNot As DataGridViewTextBoxColumn
    Friend WithEvents cNumAdj As DataGridViewTextBoxColumn
    Friend WithEvents nMonRet As DataGridViewTextBoxColumn
    Friend WithEvents nImpNetPag As DataGridViewTextBoxColumn
    Friend WithEvents bAgeRet As DataGridViewTextBoxColumn
    Friend WithEvents nCodVen As DataGridViewTextBoxColumn
    Friend WithEvents nSubTot As DataGridViewTextBoxColumn
    Friend WithEvents nMonDes As DataGridViewTextBoxColumn
    Friend WithEvents cCodGui As DataGridViewTextBoxColumn
    Friend WithEvents bAplicaGuia As DataGridViewTextBoxColumn
    Friend WithEvents bVenSer As DataGridViewTextBoxColumn
    Friend WithEvents bAplDet As DataGridViewTextBoxColumn
    Friend WithEvents nCretoLet As DataGridViewTextBoxColumn
End Class
