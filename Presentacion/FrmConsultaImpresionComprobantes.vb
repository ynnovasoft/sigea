﻿Imports System.Configuration
Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Imports DASunat.ServicioComprobantes
Public Class FrmConsultaImpresionComprobantes
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String
    Private Sub FrmConsultaImpresionComprobantes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        dtpFecIni.Value = DateAdd(DateInterval.Month, -6, dtpFecIni.Value)
        Call CargaCombos()
        txtBuscar.Select()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("90")
        Call CargaCombo(CreaDatoCombos(dt, 90), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub
    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted

    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarComprobantes()
    End Sub
    Sub ListarComprobantes()
        Dim dt As DataTable
        Dim BLPedido As New BLPedido
        dt = BLPedido.ListarComprobantesFacturacion(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text,
                                   cmbTipoBusqueda.SelectedValue, MDIPrincipal.CodigoEmpresa, MDIPrincipal.CodigoAgencia)
        Call LlenaAGridView(dt, dtgListado)
        dtgListado.Columns("cEstadoSistema").DefaultCellStyle.BackColor = Color.LightCyan
        dtgListado.Columns("cEstadoSunat").DefaultCellStyle.BackColor = Color.GreenYellow
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value, dtgListado.CurrentRow.Cells("cTipCom").Value,
                               dtgListado.CurrentRow.Cells("bAplicaGuia").Value, dtgListado.CurrentRow.Cells("cEstadoSunat").Value,
                               dtgListado.CurrentRow.Cells("bOfLine").Value)
            lblMensajeEstadoSunat.Text = dtgListado.CurrentRow.Cells("cObsSut").Value
        Else
            Call ValidaBotones(0, "", False, "", False)
            lblMensajeEstadoSunat.Text = ""
        End If
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call ListarComprobantes()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call ListarComprobantes()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub cmbEstado_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnRefreescar_Click(sender As Object, e As EventArgs) Handles btnRefreescar.Click
        Call ListarComprobantes()
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEClientes As New BECustomer
                Dim BEPedido As New BEPedido
                BEPedido.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
                BEPedido.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
                BEPedido.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BEPedido.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
                BEPedido.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
                BEPedido.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
                BEPedido.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BEPedido.gcTipCom = dtgListado.CurrentRow.Cells("cTipCom").Value
                BEPedido.gnCodSer = dtgListado.CurrentRow.Cells("nCodSer").Value
                BEPedido.gcOrdComp = dtgListado.CurrentRow.Cells("cOrdComp").Value
                BEPedido.gnTipAfecIGV = dtgListado.CurrentRow.Cells("nTipAfecIGV").Value
                BEPedido.gcTipOpe = dtgListado.CurrentRow.Cells("cTipOpe").Value
                BEPedido.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BEPedido.gnVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
                BEPedido.gnIGV = dtgListado.CurrentRow.Cells("nIGV").Value
                BEPedido.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
                BEPedido.gcMensaje = dtgListado.CurrentRow.Cells("cMensajeEstado").Value
                BEPedido.gnCodCot = dtgListado.CurrentRow.Cells("nCodCot").Value
                BEPedido.gcTipDocAdj = dtgListado.CurrentRow.Cells("cTipDocAdj").Value
                BEPedido.gcNumComp = dtgListado.CurrentRow.Cells("cNumComp").Value
                BEPedido.gbAplGuia = dtgListado.CurrentRow.Cells("bAplGuia").Value
                BEPedido.gnCodVen = dtgListado.CurrentRow.Cells("nCodVen").Value

                BEPedido.gnCodDocRef = dtgListado.CurrentRow.Cells("nCodDocRef").Value
                BEPedido.gcTipoComRef = dtgListado.CurrentRow.Cells("cTipoComRef").Value
                BEPedido.gcNumComRef = dtgListado.CurrentRow.Cells("cNumComRef").Value
                BEPedido.gcCodTipNot = dtgListado.CurrentRow.Cells("cCodTipNot").Value
                BEPedido.gcMotNot = dtgListado.CurrentRow.Cells("cMotNot").Value
                BEPedido.gcNumAdj = dtgListado.CurrentRow.Cells("cNumAdj").Value
                BEPedido.gnMonRet = dtgListado.CurrentRow.Cells("nMonRet").Value
                BEPedido.gnImpNetPag = dtgListado.CurrentRow.Cells("nImpNetPag").Value
                BEPedido.gnSubTot = dtgListado.CurrentRow.Cells("nSubTot").Value
                BEPedido.gnMonDes = dtgListado.CurrentRow.Cells("nMonDes").Value
                BEPedido.gcCodGui = dtgListado.CurrentRow.Cells("cCodGui").Value
                BEPedido.gbAplDedAnt = dtgListado.CurrentRow.Cells("bAplDedAnt").Value
                BEPedido.gnMonAnt = dtgListado.CurrentRow.Cells("nMonAnt").Value
                BEPedido.gbVenSer = dtgListado.CurrentRow.Cells("bVenSer").Value
                BEPedido.gbAplDet = dtgListado.CurrentRow.Cells("bAplDet").Value
                BEPedido.gnCretoLet = dtgListado.CurrentRow.Cells("nCretoLet").Value

                BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

                Dim Actualizar As Boolean = False
                If BEPedido.gcTipCom = "01" Or BEPedido.gcTipCom = "03" Or BEPedido.gcTipCom = "99" Then
                    If BEPedido.gnEstado = 1 Then
                        If ValidaAperturaDia() = False Then
                            Exit Sub
                        End If
                        If ValidaAperturaCaja() = False Then
                            Exit Sub
                        End If
                    End If
                    FrmTomaPedidos.InicioFacturacion(BEPedido, BEClientes, 2, Actualizar)
                    FrmTomaPedidos = Nothing
                ElseIf BEPedido.gcTipCom = "07" Then
                    If BEPedido.gnEstado = 1 Then
                        If ValidaAperturaDia() = False Then
                            Exit Sub
                        End If
                        If ValidaAperturaCaja() = False Then
                            Exit Sub
                        End If
                    End If
                    FrmRegistroNotaCredito.InicioFacturacion(BEPedido, BEClientes, 2, Actualizar)
                    FrmRegistroNotaCredito = Nothing
                ElseIf BEPedido.gcTipCom = "08" Then
                    If BEPedido.gnEstado = 1 Then
                        If ValidaAperturaDia() = False Then
                            Exit Sub
                        End If
                        If ValidaAperturaCaja() = False Then
                            Exit Sub
                        End If
                    End If
                    FrmRegistroNotaDebito.InicioFacturacion(BEPedido, BEClientes, 2, Actualizar)
                    FrmRegistroNotaDebito = Nothing
                End If
                If Actualizar = True Then
                    Call ListarComprobantes()
                End If
            End If
        End If
    End Sub

    Private Sub btnRefreescar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnRefreescar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub lblMensajeEstadoSunat_Click(sender As Object, e As EventArgs) Handles lblMensajeEstadoSunat.Click

    End Sub

    Private Sub btnNuevoComprobante_Click(sender As Object, e As EventArgs) Handles btnNuevoComprobante.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        If MDIPrincipal.CodigoBaseDatos <> 1 Then
            MsgBox("No se puede realizar ningun tipo de operaciones en una BASE DE DATOS tipificada como: BACKUP, Cierre sesiÓn y vulva a ingresar al sistema indicado la BASE DE DATOS ACTUAL", MsgBoxStyle.Critical, "Aviso")
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        FrmTomaPedidos.InicioFacturacion(Nothing, Nothing, 1, Actualizar)
        FrmTomaPedidos = Nothing
        If Actualizar = True Then
            Call ListarComprobantes()
        End If
    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEClientes As New BECustomer
            Dim BEPedido As New BEPedido
            BEPedido.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
            BEPedido.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
            BEPedido.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BEPedido.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
            BEPedido.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
            BEPedido.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
            BEPedido.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BEPedido.gcTipCom = dtgListado.CurrentRow.Cells("cTipCom").Value
            BEPedido.gnCodSer = dtgListado.CurrentRow.Cells("nCodSer").Value
            BEPedido.gcOrdComp = dtgListado.CurrentRow.Cells("cOrdComp").Value
            BEPedido.gnTipAfecIGV = dtgListado.CurrentRow.Cells("nTipAfecIGV").Value
            BEPedido.gcTipOpe = dtgListado.CurrentRow.Cells("cTipOpe").Value
            BEPedido.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BEPedido.gnVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
            BEPedido.gnIGV = dtgListado.CurrentRow.Cells("nIGV").Value
            BEPedido.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
            BEPedido.gcMensaje = dtgListado.CurrentRow.Cells("cMensajeEstado").Value
            BEPedido.gnCodCot = dtgListado.CurrentRow.Cells("nCodCot").Value
            BEPedido.gcTipDocAdj = dtgListado.CurrentRow.Cells("cTipDocAdj").Value
            BEPedido.gcNumComp = dtgListado.CurrentRow.Cells("cNumComp").Value
            BEPedido.gbAplGuia = dtgListado.CurrentRow.Cells("bAplGuia").Value
            BEPedido.gnCodVen = dtgListado.CurrentRow.Cells("nCodVen").Value

            BEPedido.gnCodDocRef = dtgListado.CurrentRow.Cells("nCodDocRef").Value
            BEPedido.gcTipoComRef = dtgListado.CurrentRow.Cells("cTipoComRef").Value
            BEPedido.gcNumComRef = dtgListado.CurrentRow.Cells("cNumComRef").Value
            BEPedido.gcCodTipNot = dtgListado.CurrentRow.Cells("cCodTipNot").Value
            BEPedido.gcMotNot = dtgListado.CurrentRow.Cells("cMotNot").Value
            BEPedido.gcNumAdj = dtgListado.CurrentRow.Cells("cNumAdj").Value
            BEPedido.gnMonRet = dtgListado.CurrentRow.Cells("nMonRet").Value
            BEPedido.gnImpNetPag = dtgListado.CurrentRow.Cells("nImpNetPag").Value
            BEPedido.gnSubTot = dtgListado.CurrentRow.Cells("nSubTot").Value
            BEPedido.gnMonDes = dtgListado.CurrentRow.Cells("nMonDes").Value
            BEPedido.gcCodGui = dtgListado.CurrentRow.Cells("cCodGui").Value
            BEPedido.gbAplDedAnt = dtgListado.CurrentRow.Cells("bAplDedAnt").Value
            BEPedido.gnMonAnt = dtgListado.CurrentRow.Cells("nMonAnt").Value
            BEPedido.gbVenSer = dtgListado.CurrentRow.Cells("bVenSer").Value
            BEPedido.gbAplDet = dtgListado.CurrentRow.Cells("bAplDet").Value
            BEPedido.gnCretoLet = dtgListado.CurrentRow.Cells("nCretoLet").Value

            BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

            Dim Actualizar As Boolean = False
            If BEPedido.gcTipCom = "01" Or BEPedido.gcTipCom = "03" Or BEPedido.gcTipCom = "99" Then
                If BEPedido.gnEstado = 1 Then
                    If ValidaAperturaDia() = False Then
                        Exit Sub
                    End If
                    If ValidaAperturaCaja() = False Then
                        Exit Sub
                    End If
                End If
                FrmTomaPedidos.InicioFacturacion(BEPedido, BEClientes, 2, Actualizar)
                FrmTomaPedidos = Nothing
            ElseIf BEPedido.gcTipCom = "07" Then
                If BEPedido.gnEstado = 1 Then
                    If ValidaAperturaDia() = False Then
                        Exit Sub
                    End If
                    If ValidaAperturaCaja() = False Then
                        Exit Sub
                    End If
                End If
                FrmRegistroNotaCredito.InicioFacturacion(BEPedido, BEClientes, 2, Actualizar)
                FrmRegistroNotaCredito = Nothing
            ElseIf BEPedido.gcTipCom = "08" Then
                If BEPedido.gnEstado = 1 Then
                    If ValidaAperturaDia() = False Then
                        Exit Sub
                    End If
                    If ValidaAperturaCaja() = False Then
                        Exit Sub
                    End If
                End If
                FrmRegistroNotaDebito.InicioFacturacion(BEPedido, BEClientes, 2, Actualizar)
                FrmRegistroNotaDebito = Nothing
            End If
            If Actualizar = True Then
                Call ListarComprobantes()
            End If
        End If
    End Sub

    Private Sub btnAnularComprobante_Click(sender As Object, e As EventArgs) Handles btnAnularComprobante.Click
        Dim Obj As New BLCommons
        Obj.ValidaEstadoMesContable(dtgListado.CurrentRow.Cells("nCodPed").Value, dtgListado.CurrentRow.Cells("dFecReg").Value,
                                    2, cMensaje)
        If cMensaje <> "" Then
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If

        Dim MotivoAnulacion As String = ""
        Dim MensajePregunta = ""
        If dtgListado.CurrentRow.Cells("nEstado").Value = 1 Then
            MensajePregunta = "¿Esta seguro que desea anular el pedido?"
        Else
            MensajePregunta = "¿Esta seguro que desea anular el comprobante?"
        End If
        If MsgBox(MensajePregunta, MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
            If dtgListado.CurrentRow.Cells("nEstado").Value <> 1 Then
                MotivoAnulacion = InputBox("Ingrese el motivo de la anulación del comprobante", "ANULACIÓN COMPROBANTE")
                If MotivoAnulacion.Trim = "" Then
                    MsgBox("Debe registrar el motivo de la anulación del comprobante", MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
            End If
            Dim oDatos As New BEPedido
            oDatos.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            oDatos.gcMotAnu = MotivoAnulacion
            cMensaje = ""
            Dim BLPedido As New BLPedido
            If BLPedido.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Call ListarComprobantes()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub
    Private Sub FrmConsultaImpresionComprobantes_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevoComprobante.Enabled = True And btnNuevoComprobante.Visible = True Then
            Call btnNuevoComprobante_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnularComprobante.Enabled = True And btnAnularComprobante.Visible = True Then
            Call btnAnularComprobante_Click(sender, e)
        ElseIf e.KeyCode = Keys.F5 And btnRefreescar.Enabled = True And btnRefreescar.Visible = True Then
            Call btnRefreescar_Click(sender, e)
        End If
    End Sub
    Sub ValidaBotones(ByVal Estado As Integer, ByVal TipoComprobante As String, ByVal Aplicaguia As Boolean,
                      ByVal EstadoSunat As String, ByVal bOffLine As Boolean)
        btnNuevoComprobante.Enabled = True
        If Estado = 0 Then
            btnAnularComprobante.Enabled = False
            btnGenerarGuia.Enabled = False
            btnGenerarNota.Enabled = False
            btnGenerarDebito.Enabled = False
            btnEnviarSunat.Enabled = False
            btnDarBaja.Enabled = False
            btnActEstado.Enabled = False
            btnImprimir.Enabled = False
            btnActualizarBaja.Enabled = False
            btnValidarEstadoSunat.Enabled = False
            Exit Sub
        End If

        If Estado = 3 Or Estado = 4 Then
            btnAnularComprobante.Enabled = False
        Else
            btnAnularComprobante.Enabled = True
        End If

        If Aplicaguia = True And Estado = 2 Then
            btnGenerarGuia.Enabled = True
        Else
            btnGenerarGuia.Enabled = False
        End If

        If Estado = 2 And EstadoSunat = "ACEPTADO" And (TipoComprobante <> "99" And TipoComprobante <> "07" And TipoComprobante <> "08") Then
            btnGenerarNota.Enabled = True
        Else
            btnGenerarNota.Enabled = False
        End If

        If Estado = 2 And EstadoSunat = "ACEPTADO" And (TipoComprobante <> "99" And TipoComprobante <> "07" And TipoComprobante <> "08") Then
            btnGenerarDebito.Enabled = True
        Else
            btnGenerarDebito.Enabled = False
        End If

        If Estado = 2 And EstadoSunat = "PENDIENTE" And TipoComprobante <> "99" Then
            btnEnviarSunat.Enabled = True
        Else
            btnEnviarSunat.Enabled = False
        End If

        If EstadoSunat = "ACEPTADO" And Estado <> 4 Then
            btnDarBaja.Enabled = True
        Else
            btnDarBaja.Enabled = False
        End If

        If (EstadoSunat = "EN PROCESO" Or EstadoSunat = "PENDIENTE") And TipoComprobante <> "99" Then
            btnActEstado.Enabled = True
        Else
            btnActEstado.Enabled = False
        End If

        If Estado = 2 Or Estado = 3 Or Estado = 4 Then
            If TipoComprobante = "99" Then
                btnImprimir.Enabled = True
            ElseIf TipoComprobante <> "99" And EstadoSunat = "ACEPTADO" Then
                btnImprimir.Enabled = True
            ElseIf TipoComprobante <> "99" And EstadoSunat = "BAJA" Then
                btnImprimir.Enabled = True
            ElseIf TipoComprobante <> "99" And EstadoSunat = "PENDIENTE" Then
                If bOffLine = True Then
                    btnImprimir.Enabled = True
                Else
                    btnImprimir.Enabled = False
                End If
            Else
                btnImprimir.Enabled = False
            End If
        Else
            btnImprimir.Enabled = False
        End If

        If EstadoSunat = "EN PROCESO" Then
            btnActualizarBaja.Enabled = True
        Else
            btnActualizarBaja.Enabled = False
        End If
        If TipoComprobante <> "99" Then
            If Estado <> 1 And EstadoSunat <> "" Then
                btnValidarEstadoSunat.Enabled = True
            Else
                btnValidarEstadoSunat.Enabled = False
            End If
        Else
            btnValidarEstadoSunat.Enabled = False
        End If
    End Sub
    Private Sub btnEnviarSunat_Click(sender As Object, e As EventArgs) Handles btnEnviarSunat.Click
        cMensaje = ""
        If MsgBox("¿Esta seguro que desea enviar a la sunat el comprobante seleccionado?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = GenerarComprobanteElectronico(dtgListado.CurrentRow.Cells("nCodPed").Value, dtgListado.CurrentRow.Cells("cTipCom").Value)
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                If dtgListado.CurrentRow.Cells("bOfLine").Value = False Then
                    If MsgBox("¿Actualmente existen intermitencias en la plataforma de SUNAT, y no se pueden enviar los comprobantes:¿Desea enviar de Manera OffLine?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                        EnvioComprobantesOffLine(dtgListado.CurrentRow.Cells("nCodPed").Value, dtgListado.CurrentRow.Cells("cTipCom").Value)
                    End If
                End If
            End If
            Call ListarComprobantes()
        End If
    End Sub
    Sub EnvioComprobantesOffLine(ByVal Codigo As Integer, ByVal TipoComprobante As Integer)
        Dim Obj As New BLCommons
        cMensaje = ""
        If Obj.EnvioComprobantesOffLine(Codigo, TipoComprobante, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnDarBaja_Click(sender As Object, e As EventArgs) Handles btnDarBaja.Click
        If MsgBox("¿Esta seguro que desea dar de BAJA el comprobante seleccionado?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            Dim oGuias As New DataTable
            Dim Obj As New BLPedido
            oGuias = Obj.ObtenerGuias(dtgListado.CurrentRow.Cells("nCodPed").Value, 0, "", dtgListado.CurrentRow.Cells("cTipCom").Value)
            If oGuias.Rows.Count > 0 Then
                MsgBox("El comprobante que se intenta dar de baja tiene relacionado  guias de remisión, es necesario anular primero la guia", MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Dim MotivoAnulacion As String
            MotivoAnulacion = InputBox("Ingrese el motivo de la anulación para dar de baja", "COMUNICACION BAJA")
            If MotivoAnulacion.Trim = "" Then
                MsgBox("Debe registrar el motivo de la anulación", MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Dim CodigoRespuesta As Integer = 0
            cMensaje = ""
            cMensaje = ComunicacionBajaComprobante(dtgListado.CurrentRow.Cells("nCodPed").Value, MotivoAnulacion, CodigoRespuesta,
                                                   dtgListado.CurrentRow.Cells("cTipCom").Value)
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            Else
                If CodigoRespuesta = 1 Then
                    MsgBox("Se anuló(Baja) correctamente el comprobante en la plataforma de SUNAT", MsgBoxStyle.Information, "MENSAJE")
                    Dim oDatos As New BEPedido
                    oDatos.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
                    oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
                    oDatos.gcMotAnu = MotivoAnulacion
                    oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
                    cMensaje = ""
                    If Obj.Anular(oDatos, cMensaje) = True Then
                        If cMensaje <> "" Then
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        Else
                            MsgBox("Se anuló correctamente el comprobante en el sistema", MsgBoxStyle.Information, "MENSAJE")
                        End If
                    Else
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    End If
                End If
            End If
            Call ListarComprobantes()
        End If
    End Sub
    Private Sub btnActEstado_Click(sender As Object, e As EventArgs) Handles btnActEstado.Click
        If dtgListado.CurrentRow.Cells("cEstadoSunat").Value = "EN PROCESO" Then
            Dim CodigoRespuesta As Integer = 0
            If MsgBox("¿Esta seguro que desea actualizar el estado del comprobante?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                cMensaje = ""
                cMensaje = ActualizarEstadoSunatComprobante(dtgListado.CurrentRow.Cells("nCodPed").Value, dtgListado.CurrentRow.Cells("cNumTickSun").Value,
                                                 dtgListado.CurrentRow.Cells("cNomXmlBaj").Value, CodigoRespuesta)
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Else
                    If CodigoRespuesta = 1 Then
                        MsgBox("Se anuló(Baja) correctamente el comprobante en la plataforma de SUNAT", MsgBoxStyle.Information, "MENSAJE")
                        Dim oDatos As New BEPedido
                        Dim BLPedido As New BLPedido
                        cMensaje = ""
                        oDatos.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
                        oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
                        oDatos.gcMotAnu = ""
                        oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
                        If BLPedido.Anular(oDatos, cMensaje) = True Then
                            If cMensaje <> "" Then
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                            Else
                                MsgBox("Se anuló(Baja) correctamente el comprobante en el sistema", MsgBoxStyle.Information, "MENSAJE")
                            End If
                        Else
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        End If
                    End If
                End If
                Call ListarComprobantes()
            End If
        ElseIf dtgListado.CurrentRow.Cells("cEstadoSunat").Value = "PENDIENTE" Then
            If MsgBox("¿Esta seguro que desea descargar el CDR del comprobante?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                cMensaje = ""
                cMensaje = ConsultarCDR(dtgListado.CurrentRow.Cells("nCodPed").Value, dtgListado.CurrentRow.Cells("ruc").Value,
                                        dtgListado.CurrentRow.Cells("cTipCom").Value, dtgListado.CurrentRow.Cells("serie").Value,
                                        dtgListado.CurrentRow.Cells("num").Value)
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                End If
                Call ListarComprobantes()
            End If
        End If
    End Sub
    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        Dim RutaEmpresa As String = ""
        If MDIPrincipal.CodigoEmpresa = 1 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
        ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
            RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
        End If
        Dim RutaPdf As String = RutaEmpresa + ConfigurationManager.AppSettings("RutaPdf")
        RutaPdf = RutaPdf + dtgListado.CurrentRow.Cells("cTipCom").Value + "-" + dtgListado.CurrentRow.Cells("cNumComp").Value + ".pdf"
        Call ModeloImpresionPdf(dtgListado.CurrentRow.Cells("nCodPed").Value, dtgListado.CurrentRow.Cells("cTipCom").Value, RutaPdf)
    End Sub
    Private Sub btnGenerarGuia_Click(sender As Object, e As EventArgs) Handles btnGenerarGuia.Click
        Dim BEClientes As New BECustomer
        Dim BEPedido As New BEPedido
        BEPedido.gcNumComp = dtgListado.CurrentRow.Cells("cNumComp").Value
        BEPedido.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
        BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
        BEPedido.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
        BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
        BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
        BEPedido.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
        BEPedido.gcTipCom = dtgListado.CurrentRow.Cells("cTipCom").Value

        If ValidaAperturaDia() = False Then
            Exit Sub
        End If

        Dim Actualizar As Boolean = False
        FrmRegistroGuias.InicioFacturacion(BEPedido, BEClientes, 3, Actualizar)
        FrmRegistroGuias = Nothing
        If Actualizar = True Then
            Call ListarComprobantes()
        End If

    End Sub
    Private Sub btnGenerarNota_Click(sender As Object, e As EventArgs) Handles btnGenerarNota.Click
        Dim BEClientes As New BECustomer
        Dim BEPedido As New BEPedido
        BEPedido.gcNumComp = dtgListado.CurrentRow.Cells("cNumComp").Value
        BEPedido.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
        BEPedido.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
        BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
        BEPedido.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
        BEPedido.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        FrmRegistroNotaCredito.InicioFacturacion(BEPedido, BEClientes, 3, Actualizar)
        FrmRegistroNotaCredito = Nothing
        If Actualizar = True Then
            Call ListarComprobantes()
        End If
    End Sub
    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs) Handles dtgListado.SelectionChanged
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value, dtgListado.CurrentRow.Cells("cTipCom").Value,
                               dtgListado.CurrentRow.Cells("bAplicaGuia").Value, dtgListado.CurrentRow.Cells("cEstadoSunat").Value,
                                dtgListado.CurrentRow.Cells("bOfLine").Value)
            lblMensajeEstadoSunat.Text = dtgListado.CurrentRow.Cells("cObsSut").Value
        Else
            Call ValidaBotones(0, "", False, "", False)
            lblMensajeEstadoSunat.Text = ""
        End If
    End Sub
    Private Sub btnNuevoComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevoComprobante.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnAnularComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnularComprobante.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnGenerarGuia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGenerarGuia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnGenerarNota_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGenerarNota.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnEnviarSunat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEnviarSunat.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnDarBaja_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnDarBaja.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnActEstado_ImeModeChanged(sender As Object, e As EventArgs) Handles btnActEstado.ImeModeChanged
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnImprimir_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnImprimir.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnGenerarDebito_Click(sender As Object, e As EventArgs) Handles btnGenerarDebito.Click
        Dim BEClientes As New BECustomer
        Dim BEPedido As New BEPedido
        BEPedido.gcNumComp = dtgListado.CurrentRow.Cells("cNumComp").Value
        BEPedido.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
        BEPedido.gnCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
        BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
        BEPedido.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
        BEPedido.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value

        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If

        Dim Actualizar As Boolean = False
        FrmRegistroNotaDebito.InicioFacturacion(BEPedido, BEClientes, 3, Actualizar)
        FrmRegistroNotaDebito = Nothing
        If Actualizar = True Then
            Call ListarComprobantes()
        End If
    End Sub
    Private Sub btnActualizarBaja_Click(sender As Object, e As EventArgs) Handles btnActualizarBaja.Click
        If dtgListado.Rows.Count > 0 Then
            If MsgBox("¿Esta seguro que desea actualizar la BAJA del comprobante seleccionado?", MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
                Dim oDatos As New BEPedido
                oDatos.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
                oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
                oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
                Dim BLPedido As New BLPedido
                cMensaje = ""
                If BLPedido.ActualizarBaja(oDatos, cMensaje) = True Then
                    If cMensaje <> "" Then
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        Exit Sub
                    End If
                    oDatos.gnCodPed = dtgListado.CurrentRow.Cells("nCodPed").Value
                    oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
                    oDatos.gcMotAnu = ""
                    oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
                    cMensaje = ""
                    If BLPedido.Anular(oDatos, cMensaje) = True Then
                        If cMensaje <> "" Then
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        Else
                            MsgBox("Se anuló correctamente el comprobante en el sistema", MsgBoxStyle.Information, "MENSAJE")
                        End If
                    Else
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    End If
                    Call ListarComprobantes()
                Else
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                End If
            End If
        End If
    End Sub

    Private Sub btnGenerarDebito_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGenerarDebito.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnValidarEstadoSunat_Click(sender As Object, e As EventArgs) Handles btnValidarEstadoSunat.Click
        Dim response As New BEResponseValidezComprobante
        cMensaje = ""
        cMensaje = ConsultaValidezComprobante(dtgListado.CurrentRow.Cells("cTipCom").Value, dtgListado.CurrentRow.Cells("Serie").Value,
                                             dtgListado.CurrentRow.Cells("num").Value, dtgListado.CurrentRow.Cells("dFecReg").Value,
                                              dtgListado.CurrentRow.Cells("nImpTot").Value, response)
        If cMensaje = "" Then
            FrmConsultaValidacionComprobante.Inicio(response)
            FrmConsultaValidacionComprobante = Nothing
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnActualizarBaja_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnActualizarBaja.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnValidarEstadoSunat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnValidarEstadoSunat.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedValueChanged
        If (TypeOf cmbTipoBusqueda.SelectedValue IsNot DataRowView) Then
            txtBuscar.Clear()
            txtBuscar.Select()
            Call ListarComprobantes()
        End If
    End Sub
End Class