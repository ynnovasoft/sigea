﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaInventario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodCat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStock = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nKardex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStockFisico = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMarca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nAgregar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnStockRetenido = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.chbDescuadre = New System.Windows.Forms.CheckBox()
        Me.dtgListadoDescuadre = New System.Windows.Forms.DataGridView()
        Me.nQuitar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.nCodProdu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcionu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStocku = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nKardexu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStockFisicou = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nDiferencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnGenerarCuadre = New System.Windows.Forms.Button()
        Me.btnMovimiento = New System.Windows.Forms.Button()
        Me.btnUnificarCodigo = New System.Windows.Forms.Button()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgListadoDescuadre, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(223, 22)
        Me.lblTitulo.Text = "GESTIÓN Y CUADRE DE INVENTARIO"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodProd, Me.cCodCat, Me.cDescripcion, Me.nStock, Me.nKardex, Me.nStockFisico, Me.cMarca, Me.nAgregar})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 56)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.RowHeadersWidth = 20
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dtgListado.Size = New System.Drawing.Size(844, 505)
        Me.dtgListado.TabIndex = 1
        '
        'nCodProd
        '
        Me.nCodProd.DataPropertyName = "nCodProd"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodProd.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodProd.HeaderText = "N° Prod."
        Me.nCodProd.Name = "nCodProd"
        Me.nCodProd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCodProd.Visible = False
        Me.nCodProd.Width = 90
        '
        'cCodCat
        '
        Me.cCodCat.DataPropertyName = "cCodCat"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cCodCat.DefaultCellStyle = DataGridViewCellStyle3
        Me.cCodCat.HeaderText = "Cod.Catálogo"
        Me.cCodCat.Name = "cCodCat"
        Me.cCodCat.ReadOnly = True
        Me.cCodCat.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cCodCat.Width = 110
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cDescripcion.DefaultCellStyle = DataGridViewCellStyle4
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.ReadOnly = True
        Me.cDescripcion.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cDescripcion.Width = 300
        '
        'nStock
        '
        Me.nStock.DataPropertyName = "nStock"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.nStock.DefaultCellStyle = DataGridViewCellStyle5
        Me.nStock.HeaderText = "Stock Sistema"
        Me.nStock.Name = "nStock"
        Me.nStock.ReadOnly = True
        Me.nStock.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nStock.Width = 120
        '
        'nKardex
        '
        Me.nKardex.DataPropertyName = "nKardex"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Format = "N2"
        Me.nKardex.DefaultCellStyle = DataGridViewCellStyle6
        Me.nKardex.HeaderText = "Stock Kardex"
        Me.nKardex.Name = "nKardex"
        Me.nKardex.ReadOnly = True
        Me.nKardex.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nKardex.Width = 110
        '
        'nStockFisico
        '
        Me.nStockFisico.DataPropertyName = "nStockFisico"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.Format = "N2"
        Me.nStockFisico.DefaultCellStyle = DataGridViewCellStyle7
        Me.nStockFisico.HeaderText = "Stock Físico"
        Me.nStockFisico.Name = "nStockFisico"
        Me.nStockFisico.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nStockFisico.Width = 110
        '
        'cMarca
        '
        Me.cMarca.DataPropertyName = "cMarca"
        Me.cMarca.HeaderText = "cMarca"
        Me.cMarca.Name = "cMarca"
        Me.cMarca.ReadOnly = True
        Me.cMarca.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cMarca.Visible = False
        '
        'nAgregar
        '
        Me.nAgregar.HeaderText = "Acción"
        Me.nAgregar.Name = "nAgregar"
        Me.nAgregar.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nAgregar.Text = "Agregar"
        Me.nAgregar.UseColumnTextForButtonValue = True
        Me.nAgregar.Width = 70
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(214, 32)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(260, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.BackColor = System.Drawing.Color.White
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(78, 32)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(135, 22)
        Me.cmbTipoBusqueda.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'btnStockRetenido
        '
        Me.btnStockRetenido.FlatAppearance.BorderSize = 0
        Me.btnStockRetenido.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnStockRetenido.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnStockRetenido.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStockRetenido.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStockRetenido.Image = Global.Presentacion.My.Resources.Resources.Productos32
        Me.btnStockRetenido.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStockRetenido.Location = New System.Drawing.Point(9, 578)
        Me.btnStockRetenido.Name = "btnStockRetenido"
        Me.btnStockRetenido.Size = New System.Drawing.Size(143, 26)
        Me.btnStockRetenido.TabIndex = 5
        Me.btnStockRetenido.Text = "Stock Retenido"
        Me.btnStockRetenido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnStockRetenido.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'chbDescuadre
        '
        Me.chbDescuadre.AutoSize = True
        Me.chbDescuadre.Location = New System.Drawing.Point(635, 32)
        Me.chbDescuadre.Name = "chbDescuadre"
        Me.chbDescuadre.Size = New System.Drawing.Size(209, 18)
        Me.chbDescuadre.TabIndex = 3
        Me.chbDescuadre.Text = "Mostrar Productos con descuadre"
        Me.chbDescuadre.UseVisualStyleBackColor = True
        '
        'dtgListadoDescuadre
        '
        Me.dtgListadoDescuadre.AllowUserToAddRows = False
        Me.dtgListadoDescuadre.AllowUserToDeleteRows = False
        Me.dtgListadoDescuadre.AllowUserToResizeColumns = False
        Me.dtgListadoDescuadre.AllowUserToResizeRows = False
        Me.dtgListadoDescuadre.BackgroundColor = System.Drawing.Color.White
        Me.dtgListadoDescuadre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListadoDescuadre.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dtgListadoDescuadre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListadoDescuadre.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nQuitar, Me.nCodProdu, Me.cDescripcionu, Me.nStocku, Me.nKardexu, Me.nStockFisicou, Me.nDiferencia})
        Me.dtgListadoDescuadre.EnableHeadersVisualStyles = False
        Me.dtgListadoDescuadre.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListadoDescuadre.Location = New System.Drawing.Point(860, 56)
        Me.dtgListadoDescuadre.Name = "dtgListadoDescuadre"
        Me.dtgListadoDescuadre.ReadOnly = True
        Me.dtgListadoDescuadre.RowHeadersVisible = False
        Me.dtgListadoDescuadre.RowHeadersWidth = 20
        Me.dtgListadoDescuadre.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dtgListadoDescuadre.Size = New System.Drawing.Size(471, 505)
        Me.dtgListadoDescuadre.TabIndex = 4
        '
        'nQuitar
        '
        Me.nQuitar.HeaderText = "Acción"
        Me.nQuitar.Name = "nQuitar"
        Me.nQuitar.ReadOnly = True
        Me.nQuitar.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nQuitar.Text = "Quitar"
        Me.nQuitar.UseColumnTextForButtonValue = True
        Me.nQuitar.Width = 70
        '
        'nCodProdu
        '
        Me.nCodProdu.DataPropertyName = "nCodProd"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodProdu.DefaultCellStyle = DataGridViewCellStyle9
        Me.nCodProdu.HeaderText = "N° Prod."
        Me.nCodProdu.Name = "nCodProdu"
        Me.nCodProdu.ReadOnly = True
        Me.nCodProdu.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCodProdu.Visible = False
        Me.nCodProdu.Width = 90
        '
        'cDescripcionu
        '
        Me.cDescripcionu.DataPropertyName = "cDescripcion"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cDescripcionu.DefaultCellStyle = DataGridViewCellStyle10
        Me.cDescripcionu.HeaderText = "Descripción"
        Me.cDescripcionu.Name = "cDescripcionu"
        Me.cDescripcionu.ReadOnly = True
        Me.cDescripcionu.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cDescripcionu.Width = 300
        '
        'nStocku
        '
        Me.nStocku.DataPropertyName = "nStock"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.nStocku.DefaultCellStyle = DataGridViewCellStyle11
        Me.nStocku.HeaderText = "Stock Sistema"
        Me.nStocku.Name = "nStocku"
        Me.nStocku.ReadOnly = True
        Me.nStocku.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nStocku.Width = 120
        '
        'nKardexu
        '
        Me.nKardexu.DataPropertyName = "nKardex"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Format = "N2"
        Me.nKardexu.DefaultCellStyle = DataGridViewCellStyle12
        Me.nKardexu.HeaderText = "Stock Kardex"
        Me.nKardexu.Name = "nKardexu"
        Me.nKardexu.ReadOnly = True
        Me.nKardexu.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nKardexu.Width = 110
        '
        'nStockFisicou
        '
        Me.nStockFisicou.DataPropertyName = "nStockFisico"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.Format = "N2"
        Me.nStockFisicou.DefaultCellStyle = DataGridViewCellStyle13
        Me.nStockFisicou.HeaderText = "Stock Físico"
        Me.nStockFisicou.Name = "nStockFisicou"
        Me.nStockFisicou.ReadOnly = True
        Me.nStockFisicou.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nStockFisicou.Width = 110
        '
        'nDiferencia
        '
        Me.nDiferencia.DataPropertyName = "nDiferencia"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.Format = "N2"
        Me.nDiferencia.DefaultCellStyle = DataGridViewCellStyle14
        Me.nDiferencia.HeaderText = "Diferencia"
        Me.nDiferencia.Name = "nDiferencia"
        Me.nDiferencia.ReadOnly = True
        Me.nDiferencia.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nDiferencia.Width = 90
        '
        'btnGenerarCuadre
        '
        Me.btnGenerarCuadre.FlatAppearance.BorderSize = 0
        Me.btnGenerarCuadre.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarCuadre.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarCuadre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarCuadre.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarCuadre.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGenerarCuadre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerarCuadre.Location = New System.Drawing.Point(1199, 577)
        Me.btnGenerarCuadre.Name = "btnGenerarCuadre"
        Me.btnGenerarCuadre.Size = New System.Drawing.Size(132, 26)
        Me.btnGenerarCuadre.TabIndex = 9
        Me.btnGenerarCuadre.Text = "Generar Cuadre"
        Me.btnGenerarCuadre.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerarCuadre.UseVisualStyleBackColor = True
        '
        'btnMovimiento
        '
        Me.btnMovimiento.FlatAppearance.BorderSize = 0
        Me.btnMovimiento.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnMovimiento.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnMovimiento.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMovimiento.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMovimiento.Image = Global.Presentacion.My.Resources.Resources.Title_20
        Me.btnMovimiento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMovimiento.Location = New System.Drawing.Point(168, 579)
        Me.btnMovimiento.Name = "btnMovimiento"
        Me.btnMovimiento.Size = New System.Drawing.Size(154, 23)
        Me.btnMovimiento.TabIndex = 6
        Me.btnMovimiento.Text = "Movimiento General"
        Me.btnMovimiento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMovimiento.UseVisualStyleBackColor = True
        '
        'btnUnificarCodigo
        '
        Me.btnUnificarCodigo.FlatAppearance.BorderSize = 0
        Me.btnUnificarCodigo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnUnificarCodigo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnUnificarCodigo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnificarCodigo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnificarCodigo.Image = Global.Presentacion.My.Resources.Resources.Unificar_24
        Me.btnUnificarCodigo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUnificarCodigo.Location = New System.Drawing.Point(1048, 577)
        Me.btnUnificarCodigo.Name = "btnUnificarCodigo"
        Me.btnUnificarCodigo.Size = New System.Drawing.Size(135, 26)
        Me.btnUnificarCodigo.TabIndex = 8
        Me.btnUnificarCodigo.Text = "Unificar Códigos"
        Me.btnUnificarCodigo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUnificarCodigo.UseVisualStyleBackColor = True
        '
        'FrmConsultaInventario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnUnificarCodigo)
        Me.Controls.Add(Me.btnMovimiento)
        Me.Controls.Add(Me.btnGenerarCuadre)
        Me.Controls.Add(Me.dtgListadoDescuadre)
        Me.Controls.Add(Me.chbDescuadre)
        Me.Controls.Add(Me.btnStockRetenido)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaInventario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgListadoDescuadre, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents btnStockRetenido As Button
    Friend WithEvents chbDescuadre As CheckBox
    Friend WithEvents nCodProd As DataGridViewTextBoxColumn
    Friend WithEvents cCodCat As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents nStock As DataGridViewTextBoxColumn
    Friend WithEvents nKardex As DataGridViewTextBoxColumn
    Friend WithEvents nStockFisico As DataGridViewTextBoxColumn
    Friend WithEvents cMarca As DataGridViewTextBoxColumn
    Friend WithEvents nAgregar As DataGridViewButtonColumn
    Friend WithEvents dtgListadoDescuadre As DataGridView
    Friend WithEvents btnGenerarCuadre As Button
    Friend WithEvents nQuitar As DataGridViewButtonColumn
    Friend WithEvents nCodProdu As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcionu As DataGridViewTextBoxColumn
    Friend WithEvents nStocku As DataGridViewTextBoxColumn
    Friend WithEvents nKardexu As DataGridViewTextBoxColumn
    Friend WithEvents nStockFisicou As DataGridViewTextBoxColumn
    Friend WithEvents nDiferencia As DataGridViewTextBoxColumn
    Friend WithEvents btnMovimiento As Button
    Friend WithEvents btnUnificarCodigo As Button
End Class
