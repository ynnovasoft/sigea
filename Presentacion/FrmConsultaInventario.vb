﻿Imports Negocios
Imports System.Configuration
Imports Business
Public Class FrmConsultaInventario
    Dim eX, eY As Integer
    Dim cMensaje As String = ""
    Dim Arrastre As Boolean = False
    Private Sub FrmConsultaInventario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call BuscarProductos()
        Call BuscarProductosDescuadre()
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("110,130")
        Call CargaCombo(CreaDatoCombos(dt, 110), cmbTipoBusqueda)
        cmbTipoBusqueda.SelectedValue = Convert.ToInt32(ConfigurationManager.AppSettings("OrdenProductos"))
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call BuscarProductos()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call BuscarProductos()
    End Sub
    Sub BuscarProductos()
        Dim dt As DataTable
        Dim BLProductos As New BLProducts
        dt = BLProductos.ListarInventarioProductos(txtBuscar.Text, cmbTipoBusqueda.SelectedValue, chbDescuadre.Checked,
                                                    MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa, MDIPrincipal.CodUsuario)
        Call LlenaAGridView(dt, dtgListado)
        If chbDescuadre.Checked = True Then
            dtgListado.Columns("nStockFisico").ReadOnly = True
        Else
            dtgListado.Columns("nStockFisico").ReadOnly = False
        End If
    End Sub
    Sub BuscarProductosDescuadre()
        Dim dt As DataTable
        Dim BLProductos As New BLProducts
        dt = BLProductos.ListarInventarioDescuadre(MDIPrincipal.CodUsuario, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListadoDescuadre)
        If dtgListadoDescuadre.Rows.Count > 0 Then
            btnGenerarCuadre.Enabled = True
            If dtgListadoDescuadre.Rows.Count > 1 Then
                btnUnificarCodigo.Enabled = True
            Else
                btnUnificarCodigo.Enabled = False
            End If
        Else
            btnGenerarCuadre.Enabled = False
            btnUnificarCodigo.Enabled = False
        End If
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        If dtgListado.Rows.Count > 0 Then
            Dim senderGrid = DirectCast(sender, DataGridView)
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                If dtgListado.Columns(e.ColumnIndex).Name = "nAgregar" Then
                    cMensaje = ""
                    Dim Diferencia As Double = 0
                    Diferencia = dtgListado.CurrentRow.Cells("nStockFisico").Value - dtgListado.CurrentRow.Cells("nKardex").Value
                    If Diferencia <> Convert.ToDouble(0.00) Then
                        Dim BLProductos As New BLProducts
                        If BLProductos.AgregarCuadreInventario(dtgListado.CurrentRow.Cells("nCodProd").Value, MDIPrincipal.CodUsuario,
                                                                MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa,
                                                               dtgListado.CurrentRow.Cells("nStockFisico").Value,
                                                               Diferencia, cMensaje) = True Then
                            If cMensaje <> "" Then
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                                Exit Sub
                            End If
                            Call BuscarProductos()
                            Call BuscarProductosDescuadre()
                        Else
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub FrmConsultaInventario_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub btnStockRetenido_Click(sender As Object, e As EventArgs) Handles btnStockRetenido.Click
        FrmStockPendienteProducto.Inicio(dtgListado.CurrentRow.Cells("nCodProd").Value, dtgListado.CurrentRow.Cells("cMarca").Value,
                                      dtgListado.CurrentRow.Cells("cCodCat").Value, dtgListado.CurrentRow.Cells("cDescripcion").Value)
        FrmStockPendienteProducto = Nothing
    End Sub

    Private Sub btnStockRetenido_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnStockRetenido.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbDescuadre_CheckedChanged(sender As Object, e As EventArgs) Handles chbDescuadre.CheckedChanged
        If chbDescuadre.Checked = True Then
            dtgListado.Columns("nStockFisico").ReadOnly = True
        Else
            dtgListado.Columns("nStockFisico").ReadOnly = False
        End If
        Call BuscarProductos()
    End Sub

    Private Sub btnGenerarCuadre_Click(sender As Object, e As EventArgs) Handles btnGenerarCuadre.Click
        If MsgBox("¿Esta seguro que desea generar el cuadre de productos?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            Dim BLProductos As New BLProducts
            If BLProductos.GenerarCuadre(MDIPrincipal.CodUsuario, MDIPrincipal.CodigoAgencia,
                                         MDIPrincipal.CodigoEmpresa, MDIPrincipal.CodigoPersonal,
                                         MDIPrincipal.NumeroRuc, MDIPrincipal.FechaSistema, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("Se registró el Parte de salida e ingreso y cuadre de inventario solicitado", MsgBoxStyle.Information, "MENSAJE")
                Call BuscarProductos()
                Call BuscarProductosDescuadre()

            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub chbDescuadre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbDescuadre.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListadoDescuadre_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListadoDescuadre.CellContentClick
        If dtgListadoDescuadre.Rows.Count > 0 Then
            Dim senderGrid = DirectCast(sender, DataGridView)
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                If dtgListadoDescuadre.Columns(e.ColumnIndex).Name = "nQuitar" Then
                    cMensaje = ""
                    Dim BLProductos As New BLProducts
                    If BLProductos.QuitarCuadreInventario(dtgListadoDescuadre.CurrentRow.Cells("nCodProdu").Value, MDIPrincipal.CodUsuario,
                                                            MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa, cMensaje) = True Then
                        If cMensaje <> "" Then
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                            Exit Sub
                        End If
                        Call BuscarProductos()
                        Call BuscarProductosDescuadre()
                    Else
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btnGenerarCuadre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGenerarCuadre.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListadoDescuadre_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListadoDescuadre.DataError

    End Sub

    Private Sub btnMovimiento_Click(sender As Object, e As EventArgs) Handles btnMovimiento.Click
        FrmConsultaMovimientosProducto.Inicio(dtgListado.CurrentRow.Cells("nCodProd").Value,
                                         dtgListado.CurrentRow.Cells("cCodCat").Value,
                                          dtgListado.CurrentRow.Cells("cDescripcion").Value)
        FrmConsultaMovimientosProducto = Nothing
    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub btnUnificarCodigo_Click(sender As Object, e As EventArgs) Handles btnUnificarCodigo.Click
        If MsgBox("¿Esta seguro que desea unificar los productos?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            Dim BLProductos As New BLProducts
            If BLProductos.UnificarProductos(MDIPrincipal.CodUsuario, MDIPrincipal.CodigoAgencia,
                                         MDIPrincipal.CodigoEmpresa, dtgListadoDescuadre.CurrentRow.Cells("nCodProdu").Value,
                                             cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("Se registró la unificación de productos", MsgBoxStyle.Information, "MENSAJE")
                Call BuscarProductos()
                Call BuscarProductosDescuadre()

            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnMovimiento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnMovimiento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnUnificarCodigo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnUnificarCodigo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class