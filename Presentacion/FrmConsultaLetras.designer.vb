﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaLetras
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFecIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nCodLet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumLet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecGir = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecVen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cAgencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumLetRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bParcial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1087, 29)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(145, 26)
        Me.lblTitulo.Text = "CONSULTA DE LETRAS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 9.75!)
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodLet, Me.cNumLet, Me.dFecReg, Me.dFecGir, Me.dFecVen, Me.cAgencia, Me.cRazSoc, Me.cEstado, Me.nImpTot, Me.nEstado, Me.nMonto, Me.cNumLetRef, Me.nTipMon, Me.bParcial})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 74)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1069, 395)
        Me.dtgListado.TabIndex = 5
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(419, 49)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(165, 23)
        Me.txtBuscar.TabIndex = 3
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 16)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Desde:"
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(240, 49)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(178, 24)
        Me.cmbTipoBusqueda.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(112, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 16)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta:"
        '
        'dtpFecIni
        '
        Me.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecIni.Location = New System.Drawing.Point(9, 49)
        Me.dtpFecIni.Name = "dtpFecIni"
        Me.dtpFecIni.Size = New System.Drawing.Size(103, 23)
        Me.dtpFecIni.TabIndex = 0
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(115, 49)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(103, 23)
        Me.dtpFecFin.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(237, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 16)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(796, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(285, 16)
        Me.Label2.TabIndex = 128
        Me.Label2.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'nCodLet
        '
        Me.nCodLet.DataPropertyName = "nCodLet"
        Me.nCodLet.HeaderText = "nCodLet"
        Me.nCodLet.Name = "nCodLet"
        Me.nCodLet.ReadOnly = True
        Me.nCodLet.Visible = False
        '
        'cNumLet
        '
        Me.cNumLet.DataPropertyName = "cNumLet"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumLet.DefaultCellStyle = DataGridViewCellStyle11
        Me.cNumLet.HeaderText = "N° Letra"
        Me.cNumLet.Name = "cNumLet"
        Me.cNumLet.ReadOnly = True
        Me.cNumLet.Width = 130
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle12
        Me.dFecReg.HeaderText = "Fecha Reg"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        '
        'dFecGir
        '
        Me.dFecGir.DataPropertyName = "dFecGir"
        DataGridViewCellStyle13.Format = "d"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.dFecGir.DefaultCellStyle = DataGridViewCellStyle13
        Me.dFecGir.HeaderText = "Fecha Giro"
        Me.dFecGir.Name = "dFecGir"
        Me.dFecGir.ReadOnly = True
        '
        'dFecVen
        '
        Me.dFecVen.DataPropertyName = "dFecVen"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.dFecVen.DefaultCellStyle = DataGridViewCellStyle14
        Me.dFecVen.HeaderText = "Fec.Venc"
        Me.dFecVen.Name = "dFecVen"
        Me.dFecVen.ReadOnly = True
        Me.dFecVen.Width = 90
        '
        'cAgencia
        '
        Me.cAgencia.DataPropertyName = "cAgencia"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cAgencia.DefaultCellStyle = DataGridViewCellStyle15
        Me.cAgencia.HeaderText = "Sucursal"
        Me.cAgencia.Name = "cAgencia"
        Me.cAgencia.ReadOnly = True
        Me.cAgencia.Width = 124
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle16
        Me.cRazSoc.HeaderText = "Cliente"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 250
        '
        'cEstado
        '
        Me.cEstado.DataPropertyName = "cEstado"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstado.DefaultCellStyle = DataGridViewCellStyle17
        Me.cEstado.HeaderText = "Estado"
        Me.cEstado.Name = "cEstado"
        Me.cEstado.ReadOnly = True
        Me.cEstado.Width = 150
        '
        'nImpTot
        '
        Me.nImpTot.DataPropertyName = "nImpTot"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.Format = "N2"
        Me.nImpTot.DefaultCellStyle = DataGridViewCellStyle18
        Me.nImpTot.HeaderText = "Imp.Total"
        Me.nImpTot.Name = "nImpTot"
        Me.nImpTot.ReadOnly = True
        '
        'nEstado
        '
        Me.nEstado.DataPropertyName = "nEstado"
        Me.nEstado.HeaderText = "nEstado"
        Me.nEstado.Name = "nEstado"
        Me.nEstado.ReadOnly = True
        Me.nEstado.Visible = False
        '
        'nMonto
        '
        Me.nMonto.DataPropertyName = "nMonto"
        Me.nMonto.HeaderText = "nMonto"
        Me.nMonto.Name = "nMonto"
        Me.nMonto.ReadOnly = True
        Me.nMonto.Visible = False
        '
        'cNumLetRef
        '
        Me.cNumLetRef.DataPropertyName = "cNumLetRef"
        Me.cNumLetRef.HeaderText = "cNumLetRef"
        Me.cNumLetRef.Name = "cNumLetRef"
        Me.cNumLetRef.ReadOnly = True
        Me.cNumLetRef.Visible = False
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.Visible = False
        '
        'bParcial
        '
        Me.bParcial.DataPropertyName = "bParcial"
        Me.bParcial.HeaderText = "bParcial"
        Me.bParcial.Name = "bParcial"
        Me.bParcial.ReadOnly = True
        Me.bParcial.Visible = False
        '
        'FrmConsultaLetras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1087, 479)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecIni)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaLetras"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents Label2 As Label
    Friend WithEvents nCodLet As DataGridViewTextBoxColumn
    Friend WithEvents cNumLet As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents dFecGir As DataGridViewTextBoxColumn
    Friend WithEvents dFecVen As DataGridViewTextBoxColumn
    Friend WithEvents cAgencia As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents cEstado As DataGridViewTextBoxColumn
    Friend WithEvents nImpTot As DataGridViewTextBoxColumn
    Friend WithEvents nEstado As DataGridViewTextBoxColumn
    Friend WithEvents nMonto As DataGridViewTextBoxColumn
    Friend WithEvents cNumLetRef As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents bParcial As DataGridViewTextBoxColumn
End Class
