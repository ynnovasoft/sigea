﻿Imports Negocios
Imports Business
Imports Entidades
Imports Entities
Public Class FrmConsultaLetras
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim BEClientes As New BECustomer
    Dim BELetras As New BELetras
    Dim Estado As Integer = 0
    Private Sub FrmConsultaLetras_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpFecIni.Value = DateAdd(DateInterval.Month, -6, dtpFecIni.Value)
        Call CargaCombos()
        Call Buscar()
        txtBuscar.Focus()
    End Sub
    Sub InicioLetras(ByRef oRet As BELetras, ByRef oCli As BECustomer)
        Estado = 0
        Me.ShowDialog()
        oRet = BELetras
        oCli = BEClientes
    End Sub
    Sub InicioAnulacion(ByRef pnCodigo As Integer, ByRef pnNumDoc As String, ByRef pdFecReg As Date)
        Estado = 1
        Me.ShowDialog()
        pnCodigo = BELetras.gnCodLet
        pnNumDoc = BELetras.gcNumLet
        pdFecReg = BELetras.gdFecReg
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("290")
        Call CargaCombo(CreaDatoCombos(dt, 290), cmbTipoBusqueda)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call Buscar()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call Buscar()
    End Sub
    Sub Buscar()
        Dim dt As DataTable
        Dim BLLetras As New BLLetras
        dt = BLLetras.Listar(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text,
                                  cmbTipoBusqueda.SelectedValue, Estado, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call Buscar()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call Buscar()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub
    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            BELetras.gnCodLet = dtgListado.CurrentRow.Cells("nCodLet").Value
            BELetras.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BELetras.gdFecGir = dtgListado.CurrentRow.Cells("dFecGir").Value
            BELetras.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
            BELetras.gnMonto = dtgListado.CurrentRow.Cells("nMonto").Value
            BELetras.gcNumLet = dtgListado.CurrentRow.Cells("cNumLet").Value
            BELetras.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BELetras.gcNumLetRef = dtgListado.CurrentRow.Cells("cNumLetRef").Value
            BELetras.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BELetras.gbParcial = dtgListado.CurrentRow.Cells("bParcial").Value
            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            Me.Close()
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                BELetras.gnCodLet = dtgListado.CurrentRow.Cells("nCodLet").Value
                BELetras.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BELetras.gdFecGir = dtgListado.CurrentRow.Cells("dFecGir").Value
                BELetras.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
                BELetras.gnMonto = dtgListado.CurrentRow.Cells("nMonto").Value
                BELetras.gcNumLet = dtgListado.CurrentRow.Cells("cNumLet").Value
                BELetras.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BELetras.gcNumLetRef = dtgListado.CurrentRow.Cells("cNumLetRef").Value
                BELetras.gbParcial = dtgListado.CurrentRow.Cells("bParcial").Value

                BELetras.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                Me.Close()
            End If
        End If
    End Sub
End Class