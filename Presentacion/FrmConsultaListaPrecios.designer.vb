﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaListaPrecios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblCantidadProductos = New System.Windows.Forms.Label()
        Me.btnProcesarStock = New System.Windows.Forms.Button()
        Me.btnMovimiento = New System.Windows.Forms.Button()
        Me.btnStockRetenido = New System.Windows.Forms.Button()
        Me.btnActualizarPrecio = New System.Windows.Forms.Button()
        Me.btnAnularProducto = New System.Windows.Forms.Button()
        Me.btnNuevoProducto = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbFamilia = New System.Windows.Forms.ComboBox()
        Me.nCodProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodCat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cFamilia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Marca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUndMed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStock = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ver = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.cMarca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreComPEN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreComUSD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cSerie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bModDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStockMinimo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodOriCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonDesUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonDesMay = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodFam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodEmp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(227, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL  DE  PRODUCTOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodProd, Me.cCodCat, Me.cFamilia, Me.Marca, Me.cDescripcion, Me.cUndMed, Me.nValUni, Me.nPreUni, Me.nStock, Me.Ver, Me.cMarca, Me.nPreComPEN, Me.nPreComUSD, Me.cSerie, Me.bModDes, Me.nStockMinimo, Me.cCodOriCom, Me.nMonDesUni, Me.nMonDesMay, Me.cCodFam, Me.nCodEmp})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 70)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.RowHeadersWidth = 20
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1324, 492)
        Me.dtgListado.TabIndex = 1
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(350, 45)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(276, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.BackColor = System.Drawing.Color.White
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(176, 45)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(172, 22)
        Me.cmbTipoBusqueda.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(173, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label2.Location = New System.Drawing.Point(1083, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(253, 14)
        Me.Label2.TabIndex = 136
        Me.Label2.Text = "Doble Click / ENTER para editar  un producto"
        '
        'lblCantidadProductos
        '
        Me.lblCantidadProductos.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblCantidadProductos.Location = New System.Drawing.Point(1078, 577)
        Me.lblCantidadProductos.Name = "lblCantidadProductos"
        Me.lblCantidadProductos.Size = New System.Drawing.Size(255, 19)
        Me.lblCantidadProductos.TabIndex = 138
        Me.lblCantidadProductos.Text = "Mensaje"
        Me.lblCantidadProductos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnProcesarStock
        '
        Me.btnProcesarStock.FlatAppearance.BorderSize = 0
        Me.btnProcesarStock.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnProcesarStock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnProcesarStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcesarStock.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcesarStock.Image = Global.Presentacion.My.Resources.Resources.Stock
        Me.btnProcesarStock.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnProcesarStock.Location = New System.Drawing.Point(748, 574)
        Me.btnProcesarStock.Name = "btnProcesarStock"
        Me.btnProcesarStock.Size = New System.Drawing.Size(155, 26)
        Me.btnProcesarStock.TabIndex = 9
        Me.btnProcesarStock.Text = "Re-Procesar Stock"
        Me.btnProcesarStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnProcesarStock.UseVisualStyleBackColor = True
        '
        'btnMovimiento
        '
        Me.btnMovimiento.FlatAppearance.BorderSize = 0
        Me.btnMovimiento.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnMovimiento.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnMovimiento.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMovimiento.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMovimiento.Image = Global.Presentacion.My.Resources.Resources.Title_20
        Me.btnMovimiento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMovimiento.Location = New System.Drawing.Point(575, 574)
        Me.btnMovimiento.Name = "btnMovimiento"
        Me.btnMovimiento.Size = New System.Drawing.Size(153, 26)
        Me.btnMovimiento.TabIndex = 8
        Me.btnMovimiento.Text = "Movimiento General"
        Me.btnMovimiento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMovimiento.UseVisualStyleBackColor = True
        '
        'btnStockRetenido
        '
        Me.btnStockRetenido.FlatAppearance.BorderSize = 0
        Me.btnStockRetenido.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnStockRetenido.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnStockRetenido.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStockRetenido.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStockRetenido.Image = Global.Presentacion.My.Resources.Resources.Productos32
        Me.btnStockRetenido.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStockRetenido.Location = New System.Drawing.Point(411, 575)
        Me.btnStockRetenido.Name = "btnStockRetenido"
        Me.btnStockRetenido.Size = New System.Drawing.Size(142, 26)
        Me.btnStockRetenido.TabIndex = 7
        Me.btnStockRetenido.Text = "Stock Retenido"
        Me.btnStockRetenido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnStockRetenido.UseVisualStyleBackColor = True
        '
        'btnActualizarPrecio
        '
        Me.btnActualizarPrecio.FlatAppearance.BorderSize = 0
        Me.btnActualizarPrecio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnActualizarPrecio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnActualizarPrecio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizarPrecio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizarPrecio.Image = Global.Presentacion.My.Resources.Resources.Refrescar_24
        Me.btnActualizarPrecio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnActualizarPrecio.Location = New System.Drawing.Point(253, 575)
        Me.btnActualizarPrecio.Name = "btnActualizarPrecio"
        Me.btnActualizarPrecio.Size = New System.Drawing.Size(137, 26)
        Me.btnActualizarPrecio.TabIndex = 6
        Me.btnActualizarPrecio.Text = "Actualizar Precio"
        Me.btnActualizarPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnActualizarPrecio.UseVisualStyleBackColor = True
        '
        'btnAnularProducto
        '
        Me.btnAnularProducto.FlatAppearance.BorderSize = 0
        Me.btnAnularProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnularProducto.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnularProducto.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnularProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnularProducto.Location = New System.Drawing.Point(128, 575)
        Me.btnAnularProducto.Name = "btnAnularProducto"
        Me.btnAnularProducto.Size = New System.Drawing.Size(106, 26)
        Me.btnAnularProducto.TabIndex = 5
        Me.btnAnularProducto.Text = "Eliminar(F4)"
        Me.btnAnularProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnularProducto.UseVisualStyleBackColor = True
        '
        'btnNuevoProducto
        '
        Me.btnNuevoProducto.FlatAppearance.BorderSize = 0
        Me.btnNuevoProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoProducto.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevoProducto.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevoProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoProducto.Location = New System.Drawing.Point(10, 575)
        Me.btnNuevoProducto.Name = "btnNuevoProducto"
        Me.btnNuevoProducto.Size = New System.Drawing.Size(102, 26)
        Me.btnNuevoProducto.TabIndex = 4
        Me.btnNuevoProducto.Text = "Nuevo(F1)"
        Me.btnNuevoProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoProducto.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 140
        Me.Label1.Text = "Familia:"
        '
        'cmbFamilia
        '
        Me.cmbFamilia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFamilia.FormattingEnabled = True
        Me.cmbFamilia.Location = New System.Drawing.Point(10, 45)
        Me.cmbFamilia.Name = "cmbFamilia"
        Me.cmbFamilia.Size = New System.Drawing.Size(162, 22)
        Me.cmbFamilia.TabIndex = 2
        '
        'nCodProd
        '
        Me.nCodProd.DataPropertyName = "nCodProd"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodProd.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodProd.HeaderText = "N° Prod."
        Me.nCodProd.Name = "nCodProd"
        Me.nCodProd.ReadOnly = True
        Me.nCodProd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCodProd.Visible = False
        Me.nCodProd.Width = 90
        '
        'cCodCat
        '
        Me.cCodCat.DataPropertyName = "cCodCat"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cCodCat.DefaultCellStyle = DataGridViewCellStyle3
        Me.cCodCat.HeaderText = "Código Catálogo"
        Me.cCodCat.Name = "cCodCat"
        Me.cCodCat.ReadOnly = True
        Me.cCodCat.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cCodCat.Width = 112
        '
        'cFamilia
        '
        Me.cFamilia.DataPropertyName = "cFamilia"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cFamilia.DefaultCellStyle = DataGridViewCellStyle4
        Me.cFamilia.HeaderText = "Familia"
        Me.cFamilia.Name = "cFamilia"
        Me.cFamilia.ReadOnly = True
        Me.cFamilia.Width = 150
        '
        'Marca
        '
        Me.Marca.DataPropertyName = "Marca"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Marca.DefaultCellStyle = DataGridViewCellStyle5
        Me.Marca.HeaderText = "Tipo Marca"
        Me.Marca.Name = "Marca"
        Me.Marca.ReadOnly = True
        Me.Marca.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Marca.Width = 150
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cDescripcion.DefaultCellStyle = DataGridViewCellStyle6
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.ReadOnly = True
        Me.cDescripcion.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cDescripcion.Width = 597
        '
        'cUndMed
        '
        Me.cUndMed.DataPropertyName = "cUndMed"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cUndMed.DefaultCellStyle = DataGridViewCellStyle7
        Me.cUndMed.HeaderText = "U/M"
        Me.cUndMed.Name = "cUndMed"
        Me.cUndMed.ReadOnly = True
        Me.cUndMed.Width = 45
        '
        'nValUni
        '
        Me.nValUni.DataPropertyName = "nValUni"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.nValUni.DefaultCellStyle = DataGridViewCellStyle8
        Me.nValUni.HeaderText = "Precio Venta"
        Me.nValUni.Name = "nValUni"
        Me.nValUni.ReadOnly = True
        Me.nValUni.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nValUni.Visible = False
        Me.nValUni.Width = 80
        '
        'nPreUni
        '
        Me.nPreUni.DataPropertyName = "nPreUni"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.nPreUni.DefaultCellStyle = DataGridViewCellStyle9
        Me.nPreUni.HeaderText = "Precio Venta"
        Me.nPreUni.Name = "nPreUni"
        Me.nPreUni.ReadOnly = True
        Me.nPreUni.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nPreUni.Width = 80
        '
        'nStock
        '
        Me.nStock.DataPropertyName = "nStock"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.nStock.DefaultCellStyle = DataGridViewCellStyle10
        Me.nStock.HeaderText = "Stock"
        Me.nStock.Name = "nStock"
        Me.nStock.ReadOnly = True
        Me.nStock.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nStock.Width = 98
        '
        'Ver
        '
        Me.Ver.HeaderText = "Acción"
        Me.Ver.Name = "Ver"
        Me.Ver.ReadOnly = True
        Me.Ver.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Ver.Text = "Ver Stock"
        Me.Ver.UseColumnTextForButtonValue = True
        Me.Ver.Width = 70
        '
        'cMarca
        '
        Me.cMarca.DataPropertyName = "cMarca"
        Me.cMarca.HeaderText = "cMarca"
        Me.cMarca.Name = "cMarca"
        Me.cMarca.ReadOnly = True
        Me.cMarca.Visible = False
        '
        'nPreComPEN
        '
        Me.nPreComPEN.DataPropertyName = "nPreComPEN"
        Me.nPreComPEN.HeaderText = "nPreComPEN"
        Me.nPreComPEN.Name = "nPreComPEN"
        Me.nPreComPEN.ReadOnly = True
        Me.nPreComPEN.Visible = False
        '
        'nPreComUSD
        '
        Me.nPreComUSD.DataPropertyName = "nPreComUSD"
        Me.nPreComUSD.HeaderText = "nPreComUSD"
        Me.nPreComUSD.Name = "nPreComUSD"
        Me.nPreComUSD.ReadOnly = True
        Me.nPreComUSD.Visible = False
        '
        'cSerie
        '
        Me.cSerie.DataPropertyName = "cSerie"
        Me.cSerie.HeaderText = "cSerie"
        Me.cSerie.Name = "cSerie"
        Me.cSerie.ReadOnly = True
        Me.cSerie.Visible = False
        '
        'bModDes
        '
        Me.bModDes.DataPropertyName = "bModDes"
        Me.bModDes.HeaderText = "bModDes"
        Me.bModDes.Name = "bModDes"
        Me.bModDes.ReadOnly = True
        Me.bModDes.Visible = False
        '
        'nStockMinimo
        '
        Me.nStockMinimo.DataPropertyName = "nStockMinimo"
        Me.nStockMinimo.HeaderText = "nStockMinimo"
        Me.nStockMinimo.Name = "nStockMinimo"
        Me.nStockMinimo.ReadOnly = True
        Me.nStockMinimo.Visible = False
        '
        'cCodOriCom
        '
        Me.cCodOriCom.DataPropertyName = "cCodOriCom"
        Me.cCodOriCom.HeaderText = "cCodOriCom"
        Me.cCodOriCom.Name = "cCodOriCom"
        Me.cCodOriCom.ReadOnly = True
        Me.cCodOriCom.Visible = False
        '
        'nMonDesUni
        '
        Me.nMonDesUni.DataPropertyName = "nMonDesUni"
        Me.nMonDesUni.HeaderText = "nMonDesUni"
        Me.nMonDesUni.Name = "nMonDesUni"
        Me.nMonDesUni.ReadOnly = True
        Me.nMonDesUni.Visible = False
        '
        'nMonDesMay
        '
        Me.nMonDesMay.DataPropertyName = "nMonDesMay"
        Me.nMonDesMay.HeaderText = "nMonDesMay"
        Me.nMonDesMay.Name = "nMonDesMay"
        Me.nMonDesMay.ReadOnly = True
        Me.nMonDesMay.Visible = False
        '
        'cCodFam
        '
        Me.cCodFam.DataPropertyName = "cCodFam"
        Me.cCodFam.HeaderText = "cCodFam"
        Me.cCodFam.Name = "cCodFam"
        Me.cCodFam.ReadOnly = True
        Me.cCodFam.Visible = False
        '
        'nCodEmp
        '
        Me.nCodEmp.DataPropertyName = "nCodEmp"
        Me.nCodEmp.HeaderText = "nCodEmp"
        Me.nCodEmp.Name = "nCodEmp"
        Me.nCodEmp.ReadOnly = True
        Me.nCodEmp.Visible = False
        '
        'FrmConsultaListaPrecios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbFamilia)
        Me.Controls.Add(Me.btnProcesarStock)
        Me.Controls.Add(Me.btnMovimiento)
        Me.Controls.Add(Me.btnStockRetenido)
        Me.Controls.Add(Me.btnActualizarPrecio)
        Me.Controls.Add(Me.btnAnularProducto)
        Me.Controls.Add(Me.lblCantidadProductos)
        Me.Controls.Add(Me.btnNuevoProducto)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaListaPrecios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnNuevoProducto As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents lblCantidadProductos As Label
    Friend WithEvents btnAnularProducto As Button
    Friend WithEvents btnActualizarPrecio As Button
    Friend WithEvents btnStockRetenido As Button
    Friend WithEvents btnMovimiento As Button
    Friend WithEvents btnProcesarStock As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbFamilia As ComboBox
    Friend WithEvents nCodProd As DataGridViewTextBoxColumn
    Friend WithEvents cCodCat As DataGridViewTextBoxColumn
    Friend WithEvents cFamilia As DataGridViewTextBoxColumn
    Friend WithEvents Marca As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents cUndMed As DataGridViewTextBoxColumn
    Friend WithEvents nValUni As DataGridViewTextBoxColumn
    Friend WithEvents nPreUni As DataGridViewTextBoxColumn
    Friend WithEvents nStock As DataGridViewTextBoxColumn
    Friend WithEvents Ver As DataGridViewButtonColumn
    Friend WithEvents cMarca As DataGridViewTextBoxColumn
    Friend WithEvents nPreComPEN As DataGridViewTextBoxColumn
    Friend WithEvents nPreComUSD As DataGridViewTextBoxColumn
    Friend WithEvents cSerie As DataGridViewTextBoxColumn
    Friend WithEvents bModDes As DataGridViewTextBoxColumn
    Friend WithEvents nStockMinimo As DataGridViewTextBoxColumn
    Friend WithEvents cCodOriCom As DataGridViewTextBoxColumn
    Friend WithEvents nMonDesUni As DataGridViewTextBoxColumn
    Friend WithEvents nMonDesMay As DataGridViewTextBoxColumn
    Friend WithEvents cCodFam As DataGridViewTextBoxColumn
    Friend WithEvents nCodEmp As DataGridViewTextBoxColumn
End Class
