﻿Imports System.Configuration
Imports Entidades
Imports Business
Imports System.Globalization

Public Class FrmConsultaListaPrecios
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Private Sub FrmConsultaListaPrecios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargaFamilia()
        Call BuscarProductos()
        txtBuscar.Select()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("110,130")
        Call CargaCombo(CreaDatoCombos(dt, 110), cmbTipoBusqueda)
        cmbTipoBusqueda.SelectedValue = Convert.ToInt32(ConfigurationManager.AppSettings("OrdenProductos"))
    End Sub
    Sub CargaFamilia()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarFamilia(0)
        Call CargaCombo(dt, cmbFamilia)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Select()
        Call BuscarProductos()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call BuscarProductos()
    End Sub
    Sub BuscarProductos()
        Dim dt As DataTable
        Dim Obj As New BLProducts
        dt = Obj.ListarProductos(txtBuscar.Text, cmbTipoBusqueda.SelectedValue, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa,
                                 cmbFamilia.SelectedValue)
        Call LlenaAGridView(dt, dtgListado)
        If MDIPrincipal.CodigoPrecioLista = 1 Then
            dtgListado.Columns("nValUni").Visible = False
            dtgListado.Columns("nPreUni").Visible = True
        Else
            dtgListado.Columns("nValUni").Visible = True
            dtgListado.Columns("nPreUni").Visible = False
        End If

        lblCantidadProductos.Text = "Se encontrarón un total de " + FormatNumber(dtgListado.Rows.Count, 0) + " productos"
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        If dtgListado.Rows.Count > 0 Then
            Dim senderGrid = DirectCast(sender, DataGridView)
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                If dtgListado.Columns(e.ColumnIndex).Name = "Ver" Then
                    FrmListarStock.Inicio(dtgListado.CurrentRow.Cells("nCodProd").Value)
                    FrmListarStock = Nothing
                End If
            End If
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMarca_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEProductos As New BEProductos
                BEProductos.gnCodProd = dtgListado.CurrentRow.Cells("nCodProd").Value
                BEProductos.gcCodCat = dtgListado.CurrentRow.Cells("cCodCat").Value
                BEProductos.gcMarca = dtgListado.CurrentRow.Cells("cMarca").Value
                BEProductos.gcSerie = dtgListado.CurrentRow.Cells("cSerie").Value
                BEProductos.gcDescripcion = dtgListado.CurrentRow.Cells("cDescripcion").Value
                BEProductos.gcUndMed = dtgListado.CurrentRow.Cells("cUndMed").Value
                BEProductos.gbModDes = dtgListado.CurrentRow.Cells("bModDes").Value
                BEProductos.gcCodOriCom = dtgListado.CurrentRow.Cells("cCodOriCom").Value
                BEProductos.gnCodEmp = dtgListado.CurrentRow.Cells("nCodEmp").Value
                BEProductos.gcCodFam = dtgListado.CurrentRow.Cells("cCodFam").Value
                Dim Actualizar As Boolean = False
                FrmProductos.InicioProductos(BEProductos, 2, Actualizar)
                FrmProductos = Nothing
                If Actualizar = True Then
                    Call BuscarProductos()
                End If
            End If
        End If
    End Sub

    Private Sub cmbMarca_SelectionChangeCommitted(sender As Object, e As EventArgs)
        Call BuscarProductos()
    End Sub

    Private Sub btnNuevoProducto_Click(sender As Object, e As EventArgs) Handles btnNuevoProducto.Click
        Dim Actualizar As Boolean = False
        FrmProductos.InicioProductos(Nothing, 1, Actualizar)
        FrmProductos = Nothing
        If Actualizar = True Then
            Call BuscarProductos()
        End If
    End Sub

    Private Sub btnAnularProducto_Click(sender As Object, e As EventArgs) Handles btnAnularProducto.Click
        Dim oDatos As New BEProductos
        Dim cMensaje As String = ""
        If MsgBox("¿Esta seguro que desea eliminar el producto?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodProd = dtgListado.CurrentRow.Cells("nCodProd").Value
            Dim obj As New BLProducts
            If obj.EliminarProducto(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("El producto se eliminó correctamente", MsgBoxStyle.Information, "MENSAJE")
                Call BuscarProductos()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEProductos As New BEProductos
            BEProductos.gnCodProd = dtgListado.CurrentRow.Cells("nCodProd").Value
            BEProductos.gcCodCat = dtgListado.CurrentRow.Cells("cCodCat").Value
            BEProductos.gcMarca = dtgListado.CurrentRow.Cells("cMarca").Value
            BEProductos.gcSerie = dtgListado.CurrentRow.Cells("cSerie").Value
            BEProductos.gcDescripcion = dtgListado.CurrentRow.Cells("cDescripcion").Value
            BEProductos.gcUndMed = dtgListado.CurrentRow.Cells("cUndMed").Value
            BEProductos.gbModDes = dtgListado.CurrentRow.Cells("bModDes").Value
            BEProductos.gcCodOriCom = dtgListado.CurrentRow.Cells("cCodOriCom").Value
            BEProductos.gnCodEmp = dtgListado.CurrentRow.Cells("nCodEmp").Value
            BEProductos.gcCodFam = dtgListado.CurrentRow.Cells("cCodFam").Value
            Dim Actualizar As Boolean = False
            FrmProductos.InicioProductos(BEProductos, 2, Actualizar)
            FrmProductos = Nothing
            If Actualizar = True Then
                Call BuscarProductos()
            End If
        End If
    End Sub

    Private Sub FrmConsultaListaPrecios_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevoProducto.Enabled = True And btnNuevoProducto.Visible = True Then
            Call btnNuevoProducto_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnularProducto.Enabled = True And btnAnularProducto.Visible = True Then
            Call btnAnularProducto_Click(sender, e)
        End If
    End Sub

    Private Sub btnNuevoProducto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevoProducto.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnActualizarPrecio_Click(sender As Object, e As EventArgs) Handles btnActualizarPrecio.Click

        Dim Actualizar As Boolean = False
        FrmActualizacionPrecio.Inicio(dtgListado.CurrentRow.Cells("nCodProd").Value, dtgListado.CurrentRow.Cells("cMarca").Value,
                                      dtgListado.CurrentRow.Cells("cCodCat").Value, dtgListado.CurrentRow.Cells("cDescripcion").Value,
                                      dtgListado.CurrentRow.Cells("nPreComPEN").Value, dtgListado.CurrentRow.Cells("nPreComUSD").Value,
                                      dtgListado.CurrentRow.Cells("nValUni").Value, dtgListado.CurrentRow.Cells("nPreUni").Value,
                                      dtgListado.CurrentRow.Cells("nStockMinimo").Value, dtgListado.CurrentRow.Cells("nMonDesUni").Value,
                                      dtgListado.CurrentRow.Cells("nMonDesMay").Value, Actualizar)
        FrmActualizacionPrecio = Nothing
        If Actualizar = True Then
            Call BuscarProductos()
        End If

    End Sub

    Private Sub btnAnularProducto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnularProducto.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnStockRetenido_Click(sender As Object, e As EventArgs) Handles btnStockRetenido.Click
        FrmStockPendienteProducto.Inicio(dtgListado.CurrentRow.Cells("nCodProd").Value, dtgListado.CurrentRow.Cells("cMarca").Value,
                                      dtgListado.CurrentRow.Cells("cCodCat").Value, dtgListado.CurrentRow.Cells("cDescripcion").Value)
        FrmStockPendienteProducto = Nothing
    End Sub

    Private Sub btnActualizarPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnActualizarPrecio.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnStockRetenido_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnStockRetenido.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnMovimiento_Click(sender As Object, e As EventArgs) Handles btnMovimiento.Click
        FrmConsultaMovimientosProducto.Inicio(dtgListado.CurrentRow.Cells("nCodProd").Value,
                                          dtgListado.CurrentRow.Cells("cCodCat").Value,
                                           dtgListado.CurrentRow.Cells("cDescripcion").Value)
        FrmConsultaMovimientosProducto = Nothing
    End Sub

    Private Sub btnProcesarStock_Click(sender As Object, e As EventArgs) Handles btnProcesarStock.Click
        Dim cMensaje As String = ""
        If MsgBox("¿Esta seguro que desea Re-Procesar el stock?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            Dim obj As New BLProducts
            If obj.ReprocesarStock(dtgListado.CurrentRow.Cells("nCodProd").Value, MDIPrincipal.CodigoAgencia,
                                   MDIPrincipal.CodigoEmpresa, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("Se realizó el Re-Proceso correctamente", MsgBoxStyle.Information, "MENSAJE")
                Call BuscarProductos()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnMovimiento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnMovimiento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnProcesarStock_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnProcesarStock.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbFamilia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFamilia.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedValueChanged

    End Sub

    Private Sub cmbFamilia_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbFamilia.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Select()
        Call BuscarProductos()
    End Sub

    Private Sub cmbFamilia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbFamilia.KeyPress
        ValidaSonidoEnter(e)
    End Sub
End Class