﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaMovimientosServicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.cNumAlm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AgenciaSalida = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AgenciaIngreso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMotivoTraslado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocumentoRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Movimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodalm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTipoMovimiento = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFecIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtProducto = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.cmbAgencia = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtStockActual = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtStockAnterior = New System.Windows.Forms.Label()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(325, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL DE MOVIMIENTOS POR SERVICIO"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cNumAlm, Me.dFecReg, Me.AgenciaSalida, Me.AgenciaIngreso, Me.cMotivoTraslado, Me.TipoComprobante, Me.DocumentoRef, Me.cRazSoc, Me.Movimiento, Me.PrecioS, Me.PrecioD, Me.nCodalm, Me.nTipMov})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.Location = New System.Drawing.Point(9, 88)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.RowHeadersWidth = 20
        Me.dtgListado.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1323, 514)
        Me.dtgListado.TabIndex = 0
        '
        'cNumAlm
        '
        Me.cNumAlm.DataPropertyName = "cNumAlm"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumAlm.DefaultCellStyle = DataGridViewCellStyle12
        Me.cNumAlm.HeaderText = "N° Almacen"
        Me.cNumAlm.Name = "cNumAlm"
        Me.cNumAlm.ReadOnly = True
        Me.cNumAlm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cNumAlm.Width = 110
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle13
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dFecReg.Width = 80
        '
        'AgenciaSalida
        '
        Me.AgenciaSalida.DataPropertyName = "AgenciaSalida"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.AgenciaSalida.DefaultCellStyle = DataGridViewCellStyle14
        Me.AgenciaSalida.HeaderText = "Sucursal Salida"
        Me.AgenciaSalida.Name = "AgenciaSalida"
        Me.AgenciaSalida.ReadOnly = True
        Me.AgenciaSalida.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AgenciaSalida.Width = 120
        '
        'AgenciaIngreso
        '
        Me.AgenciaIngreso.DataPropertyName = "AgenciaIngreso"
        Me.AgenciaIngreso.HeaderText = "Sucursal Ingreso"
        Me.AgenciaIngreso.Name = "AgenciaIngreso"
        Me.AgenciaIngreso.ReadOnly = True
        Me.AgenciaIngreso.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AgenciaIngreso.Width = 120
        '
        'cMotivoTraslado
        '
        Me.cMotivoTraslado.DataPropertyName = "cMotivoTraslado"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cMotivoTraslado.DefaultCellStyle = DataGridViewCellStyle15
        Me.cMotivoTraslado.HeaderText = "Tipo Operación"
        Me.cMotivoTraslado.Name = "cMotivoTraslado"
        Me.cMotivoTraslado.ReadOnly = True
        Me.cMotivoTraslado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cMotivoTraslado.Width = 130
        '
        'TipoComprobante
        '
        Me.TipoComprobante.DataPropertyName = "TipoComprobante"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipoComprobante.DefaultCellStyle = DataGridViewCellStyle16
        Me.TipoComprobante.HeaderText = "Tipo Documento"
        Me.TipoComprobante.Name = "TipoComprobante"
        Me.TipoComprobante.ReadOnly = True
        Me.TipoComprobante.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TipoComprobante.Width = 120
        '
        'DocumentoRef
        '
        Me.DocumentoRef.DataPropertyName = "DocumentoRef"
        Me.DocumentoRef.HeaderText = "N° Documento"
        Me.DocumentoRef.Name = "DocumentoRef"
        Me.DocumentoRef.ReadOnly = True
        Me.DocumentoRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DocumentoRef.Width = 120
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle17
        Me.cRazSoc.HeaderText = "Cliente / Proveedor"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cRazSoc.Width = 300
        '
        'Movimiento
        '
        Me.Movimiento.DataPropertyName = "Movimiento"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle18.Format = "N2"
        DataGridViewCellStyle18.NullValue = Nothing
        Me.Movimiento.DefaultCellStyle = DataGridViewCellStyle18
        Me.Movimiento.HeaderText = "Cantidad"
        Me.Movimiento.Name = "Movimiento"
        Me.Movimiento.ReadOnly = True
        Me.Movimiento.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Movimiento.Width = 70
        '
        'PrecioS
        '
        Me.PrecioS.DataPropertyName = "PrecioS"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle19.Format = "N6"
        Me.PrecioS.DefaultCellStyle = DataGridViewCellStyle19
        Me.PrecioS.HeaderText = "Precio.Unit(S/.)"
        Me.PrecioS.Name = "PrecioS"
        Me.PrecioS.ReadOnly = True
        Me.PrecioS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PrecioS.Width = 120
        '
        'PrecioD
        '
        Me.PrecioD.DataPropertyName = "PrecioD"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle20.Format = "N6"
        Me.PrecioD.DefaultCellStyle = DataGridViewCellStyle20
        Me.PrecioD.HeaderText = "Precio.Unit($.)"
        Me.PrecioD.Name = "PrecioD"
        Me.PrecioD.ReadOnly = True
        Me.PrecioD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PrecioD.Width = 120
        '
        'nCodalm
        '
        Me.nCodalm.DataPropertyName = "nCodalm"
        Me.nCodalm.HeaderText = "nCodalm"
        Me.nCodalm.Name = "nCodalm"
        Me.nCodalm.ReadOnly = True
        Me.nCodalm.Visible = False
        '
        'nTipMov
        '
        Me.nTipMov.DataPropertyName = "nTipMov"
        Me.nTipMov.HeaderText = "nTipMov"
        Me.nTipMov.Name = "nTipMov"
        Me.nTipMov.ReadOnly = True
        Me.nTipMov.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Desde:"
        '
        'cmbTipoMovimiento
        '
        Me.cmbTipoMovimiento.BackColor = System.Drawing.Color.White
        Me.cmbTipoMovimiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoMovimiento.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoMovimiento.FormattingEnabled = True
        Me.cmbTipoMovimiento.Location = New System.Drawing.Point(403, 42)
        Me.cmbTipoMovimiento.Name = "cmbTipoMovimiento"
        Me.cmbTipoMovimiento.Size = New System.Drawing.Size(178, 22)
        Me.cmbTipoMovimiento.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(729, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(216, 14)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Doble Click / ENTER para ver el detalle"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(112, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 14)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta:"
        '
        'dtpFecIni
        '
        Me.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecIni.Location = New System.Drawing.Point(9, 43)
        Me.dtpFecIni.Name = "dtpFecIni"
        Me.dtpFecIni.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecIni.TabIndex = 1
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(115, 43)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(103, 22)
        Me.dtpFecFin.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(400, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Movimiento:"
        '
        'txtProducto
        '
        Me.txtProducto.BackColor = System.Drawing.SystemColors.Info
        Me.txtProducto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtProducto.Location = New System.Drawing.Point(9, 66)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(572, 20)
        Me.txtProducto.TabIndex = 129
        Me.txtProducto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Red
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Location = New System.Drawing.Point(63, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 10)
        Me.Label5.TabIndex = 130
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(11, 21)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 14)
        Me.Label6.TabIndex = 131
        Me.Label6.Text = "Salidas"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(4, 39)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 14)
        Me.Label7.TabIndex = 133
        Me.Label7.Text = "Ingresos"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Blue
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Location = New System.Drawing.Point(63, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(24, 10)
        Me.Label8.TabIndex = 134
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(1239, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(93, 62)
        Me.GroupBox1.TabIndex = 135
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Leyenda"
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'cmbAgencia
        '
        Me.cmbAgencia.BackColor = System.Drawing.Color.White
        Me.cmbAgencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAgencia.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbAgencia.FormattingEnabled = True
        Me.cmbAgencia.Location = New System.Drawing.Point(221, 42)
        Me.cmbAgencia.Name = "cmbAgencia"
        Me.cmbAgencia.Size = New System.Drawing.Size(176, 22)
        Me.cmbAgencia.TabIndex = 3
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(218, 28)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(55, 14)
        Me.Label17.TabIndex = 139
        Me.Label17.Text = "Sucursal:"
        '
        'txtStockActual
        '
        Me.txtStockActual.BackColor = System.Drawing.SystemColors.Info
        Me.txtStockActual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtStockActual.Location = New System.Drawing.Point(1100, 63)
        Me.txtStockActual.Name = "txtStockActual"
        Me.txtStockActual.Size = New System.Drawing.Size(73, 20)
        Me.txtStockActual.TabIndex = 140
        Me.txtStockActual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(1018, 69)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 14)
        Me.Label11.TabIndex = 141
        Me.Label11.Text = "Stock Actual:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(1007, 46)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(90, 14)
        Me.Label12.TabIndex = 143
        Me.Label12.Text = "Stock Anterior:"
        '
        'txtStockAnterior
        '
        Me.txtStockAnterior.BackColor = System.Drawing.SystemColors.Info
        Me.txtStockAnterior.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtStockAnterior.Location = New System.Drawing.Point(1100, 40)
        Me.txtStockAnterior.Name = "txtStockAnterior"
        Me.txtStockAnterior.Size = New System.Drawing.Size(73, 20)
        Me.txtStockAnterior.TabIndex = 142
        Me.txtStockAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmConsultaMovimientosServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtStockAnterior)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtStockActual)
        Me.Controls.Add(Me.cmbAgencia)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtProducto)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecIni)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbTipoMovimiento)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaMovimientosServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoMovimiento As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtProducto As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cmbAgencia As ComboBox
    Friend WithEvents Label17 As Label
    Friend WithEvents cNumAlm As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents AgenciaSalida As DataGridViewTextBoxColumn
    Friend WithEvents AgenciaIngreso As DataGridViewTextBoxColumn
    Friend WithEvents cMotivoTraslado As DataGridViewTextBoxColumn
    Friend WithEvents TipoComprobante As DataGridViewTextBoxColumn
    Friend WithEvents DocumentoRef As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents Movimiento As DataGridViewTextBoxColumn
    Friend WithEvents PrecioS As DataGridViewTextBoxColumn
    Friend WithEvents PrecioD As DataGridViewTextBoxColumn
    Friend WithEvents nCodalm As DataGridViewTextBoxColumn
    Friend WithEvents nTipMov As DataGridViewTextBoxColumn
    Friend WithEvents txtStockActual As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtStockAnterior As Label
End Class
