﻿Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Imports System.Windows.Forms
Imports System.Configuration
Public Class FrmConsultaMovimientosServicio
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim AccionBuscar As Boolean = False

    Private Sub FrmConsultaMovimientosServicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call RecuperaStockAnterior()
        Call Buscar()
        cmbTipoMovimiento.Focus()
    End Sub
    Sub Inicio(ByVal Codigo As Integer, ByVal Descripcion As String)
        txtProducto.Tag = Codigo
        txtProducto.Text = Descripcion
        Call CargaCombos()
        Call CargarAgencia()
        AccionBuscar = True
        Me.ShowDialog()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMovimientos()
        Call CargaCombo(dt, cmbTipoMovimiento)
    End Sub
    Sub CargarAgencia()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarAgencias(True, MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbAgencia)
        cmbAgencia.SelectedValue = MDIPrincipal.CodigoAgencia
    End Sub
    Sub RecuperaStockAnterior()
        Dim dt As DataTable
        Dim BLAlmacen As New BLAlmacen
        dt = BLAlmacen.ListarStockAnterior(dtpFecIni.Value, txtProducto.Tag, cmbAgencia.SelectedValue, MDIPrincipal.CodigoEmpresa)
        If dt.Rows.Count > 0 Then
            txtStockAnterior.Text = FormatNumber(ObtenerDato("StockAnterior", dt), 2)
        Else
            txtStockAnterior.Text = "0.00"
        End If
    End Sub
    Sub Buscar()
        Dim dt As DataTable
        Dim BLAlmacen As New BLAlmacen
        dt = BLAlmacen.ListarMovimientos(dtpFecIni.Value, dtpFecFin.Value, txtProducto.Tag, cmbAgencia.SelectedValue,
                                         cmbTipoMovimiento.SelectedValue, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
        Dim StockAnterior As Double = txtStockAnterior.Text
        If dtgListado.Rows.Count > 0 Then
            Dim Contador As Double = 0
            For i = 0 To dtgListado.Rows.Count - 1
                If dtgListado.Rows(i).Cells("nTipMov").Value = "1" Then
                    dtgListado.Rows(i).DefaultCellStyle.ForeColor = Color.Red
                Else
                    dtgListado.Rows(i).DefaultCellStyle.ForeColor = Color.Blue
                End If
                Contador = Contador + dtgListado.Rows(i).Cells("Movimiento").Value
            Next
            txtStockActual.Text = FormatNumber(StockAnterior + Contador, 2)
        Else
            txtStockActual.Text = FormatNumber(StockAnterior, 2)
        End If
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call RecuperaStockAnterior()
        Call Buscar()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call RecuperaStockAnterior()
        Call Buscar()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEAlmacen As New BEStore
            Dim BEClientes As New BECustomer
            BEAlmacen.nCodalm = dtgListado.CurrentRow.Cells("nCodalm").Value
            BEAlmacen.nCodigoAgencia = dtgListado.CurrentRow.Cells("nCodigoAgencia").Value
            BEAlmacen.nCodAgeDes = dtgListado.CurrentRow.Cells("nCodAgeDes").Value
            BEAlmacen.AgenciaOrigen = dtgListado.CurrentRow.Cells("AgenciaSalida").Value
            BEAlmacen.AgenciaDestino = dtgListado.CurrentRow.Cells("AgenciaIngreso").Value
            BEAlmacen.cMotTrasl = dtgListado.CurrentRow.Cells("cMotTrasl").Value
            BEAlmacen.dFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BEAlmacen.nTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
            BEAlmacen.nTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BEAlmacen.nVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
            BEAlmacen.nIGV = dtgListado.CurrentRow.Cells("nIGV").Value
            BEAlmacen.nImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
            BEAlmacen.cMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
            BEAlmacen.nEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BEAlmacen.cTipDocRef = dtgListado.CurrentRow.Cells("cTipDocRef").Value
            BEAlmacen.NumDocRef = dtgListado.CurrentRow.Cells("NumDocRef").Value
            BEAlmacen.NumDocRef = dtgListado.CurrentRow.Cells("NumDocRef").Value
            BEAlmacen.nCodigoDocRef = dtgListado.CurrentRow.Cells("nCodigoDocRef").Value
            BEAlmacen.cObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
            BEAlmacen.cNumAlm = dtgListado.CurrentRow.Cells("cNumAlm").Value
            BEAlmacen.nSubTot = dtgListado.CurrentRow.Cells("nSubTot").Value
            BEAlmacen.nMonDes = dtgListado.CurrentRow.Cells("nMonDes").Value
            BEAlmacen.nCodEmpDes = dtgListado.CurrentRow.Cells("nCodEmpDes").Value
            BEAlmacen.EmpresaOrigen = dtgListado.CurrentRow.Cells("EmpresaOrigen").Value
            BEAlmacen.nTipMov = dtgListado.CurrentRow.Cells("nTipMov").Value
            BEAlmacen.nCodCliProv = dtgListado.CurrentRow.Cells("nCodCliProv").Value

            BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value

            If BEAlmacen.nTipMov = 1 Then
                FrmParteSalida.InicioAlmacen(BEAlmacen, BEClientes, 2, False)
                FrmParteSalida = Nothing
            Else
                FrmParteIngreso.InicioAlmacen(BEAlmacen, BEClientes, 2, False)
                FrmParteIngreso = Nothing
            End If
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoMovimiento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoMovimiento.SelectedIndexChanged

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEAlmacen As New BEStore
                Dim BEClientes As New BECustomer
                BEAlmacen.nCodalm = dtgListado.CurrentRow.Cells("nCodalm").Value
                BEAlmacen.nCodigoAgencia = dtgListado.CurrentRow.Cells("nCodigoAgencia").Value
                BEAlmacen.nCodAgeDes = dtgListado.CurrentRow.Cells("nCodAgeDes").Value
                BEAlmacen.AgenciaOrigen = dtgListado.CurrentRow.Cells("AgenciaSalida").Value
                BEAlmacen.AgenciaDestino = dtgListado.CurrentRow.Cells("AgenciaIngreso").Value
                BEAlmacen.cMotTrasl = dtgListado.CurrentRow.Cells("cMotTrasl").Value
                BEAlmacen.dFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BEAlmacen.nTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
                BEAlmacen.nTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BEAlmacen.nEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BEAlmacen.nVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
                BEAlmacen.nIGV = dtgListado.CurrentRow.Cells("nIGV").Value
                BEAlmacen.nImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
                BEAlmacen.cMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
                BEAlmacen.cTipDocRef = dtgListado.CurrentRow.Cells("cTipDocRef").Value
                BEAlmacen.NumDocRef = dtgListado.CurrentRow.Cells("NumDocRef").Value
                BEAlmacen.NumDocRef = dtgListado.CurrentRow.Cells("NumDocRef").Value
                BEAlmacen.nCodigoDocRef = dtgListado.CurrentRow.Cells("nCodigoDocRef").Value
                BEAlmacen.cObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
                BEAlmacen.cNumAlm = dtgListado.CurrentRow.Cells("cNumAlm").Value
                BEAlmacen.nSubTot = dtgListado.CurrentRow.Cells("nSubTot").Value
                BEAlmacen.nMonDes = dtgListado.CurrentRow.Cells("nMonDes").Value
                BEAlmacen.nCodEmpDes = dtgListado.CurrentRow.Cells("nCodEmpDes").Value
                BEAlmacen.EmpresaOrigen = dtgListado.CurrentRow.Cells("EmpresaOrigen").Value
                BEAlmacen.nTipMov = dtgListado.CurrentRow.Cells("nTipMov").Value
                BEAlmacen.nCodCliProv = dtgListado.CurrentRow.Cells("nCodCliProv").Value

                BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value

                If BEAlmacen.nTipMov = 1 Then
                    FrmParteSalida.InicioAlmacen(BEAlmacen, BEClientes, 2, False)
                    FrmParteSalida = Nothing
                Else
                    FrmParteIngreso.InicioAlmacen(BEAlmacen, BEClientes, 2, False)
                    FrmParteIngreso = Nothing
                End If
            End If
        End If
    End Sub
    Private Sub cmbTipoMovimiento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoMovimiento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbAgencia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAgencia.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoMovimiento_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbTipoMovimiento.SelectedValueChanged
        If (TypeOf cmbTipoMovimiento.SelectedValue IsNot DataRowView) Then
            If AccionBuscar = True Then
                Call RecuperaStockAnterior()
                Call Buscar()
            End If
        End If
    End Sub
    Private Sub cmbAgencia_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbAgencia.SelectedValueChanged
        If (TypeOf cmbAgencia.SelectedValue IsNot DataRowView) Then
            If AccionBuscar = True Then
                Call RecuperaStockAnterior()
                Call Buscar()
            End If
        End If
    End Sub

    Private Sub cmbAgencia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbAgencia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub FrmConsultaMovimientosServicio_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class