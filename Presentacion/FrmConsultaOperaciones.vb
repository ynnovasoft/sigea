﻿Imports Negocios
Imports Business
Imports Entidades
Imports System.Windows.Forms
Public Class FrmConsultaOperaciones
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim dt As DataTable
    Dim BLComunes As New BLCommons
    Dim BLCaja As New BLCaja
    Dim cMensaje As String = ""

    Private Sub FrmConsultaOperaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaAgencias()
        Call CargaUsuarios()
    End Sub
    Sub CargaAgencias()
        Try
            dt = BLComunes.MostrarAgenciasCaja(dtpFecha.Value, MDIPrincipal.CodigoEmpresa)
            Call CargaCombo(dt, cmbAgencia)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            dt = Nothing
        End Try
    End Sub
    Sub CargaUsuarios()
        Try
            dt = BLComunes.MostrarUsuariosCaja(dtpFecha.Value, cmbAgencia.SelectedValue, MDIPrincipal.CodigoEmpresa)
            Call CargaCombo(dt, cmbUsuarios)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            dt = Nothing
        End Try
    End Sub
    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub


    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub btnVerInforme_Click(sender As Object, e As EventArgs) Handles btnVerInforme.Click
        FrmReportes.ReporteOperacionesCaja(IIf(cmbUsuarios.SelectedValue = "0", "", cmbUsuarios.SelectedValue),
                                           cmbAgencia.SelectedValue, dtpFecha.Text, "CONSULTA DE CIERRE OPERACIONES")
        FrmReportes = Nothing
    End Sub

    Private Sub cmbAgencia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAgencia.SelectedIndexChanged

    End Sub

    Private Sub btnVerInforme_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnVerInforme.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        Call CargaAgencias()
    End Sub

    Private Sub cmbAgencia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbAgencia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbUsuarios.SelectedIndexChanged

    End Sub

    Private Sub dtpFecha_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecha.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbUsuarios_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbUsuarios.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbAgencia_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbAgencia.SelectionChangeCommitted
        Call CargaUsuarios()
    End Sub
End Class