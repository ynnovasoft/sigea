﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaOrdenCompraImportacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodOrdCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodProve = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumOrd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpTotPre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cObservaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Agencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMensaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFecIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecFin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbEstado = New System.Windows.Forms.ComboBox()
        Me.btnRefreescar = New System.Windows.Forms.Button()
        Me.btnAnularCompra = New System.Windows.Forms.Button()
        Me.btnNuevoCompra = New System.Windows.Forms.Button()
        Me.btnGenerarCompra = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(351, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL ORDENES DE COMPRA IMPORTACIÓN"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodOrdCom, Me.nCodProve, Me.cNumOrd, Me.dFecReg, Me.cRazSoc, Me.TipMon, Me.cEstado, Me.nImpTotPre, Me.nTipMon, Me.cObservaciones, Me.nEstado, Me.Agencia, Me.cMensaje})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 67)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1324, 499)
        Me.dtgListado.TabIndex = 1
        '
        'nCodOrdCom
        '
        Me.nCodOrdCom.DataPropertyName = "nCodOrdCom"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodOrdCom.DefaultCellStyle = DataGridViewCellStyle10
        Me.nCodOrdCom.HeaderText = "nCodOrdCom"
        Me.nCodOrdCom.Name = "nCodOrdCom"
        Me.nCodOrdCom.ReadOnly = True
        Me.nCodOrdCom.Visible = False
        '
        'nCodProve
        '
        Me.nCodProve.DataPropertyName = "nCodProve"
        Me.nCodProve.HeaderText = "nCodProve"
        Me.nCodProve.Name = "nCodProve"
        Me.nCodProve.ReadOnly = True
        Me.nCodProve.Visible = False
        Me.nCodProve.Width = 110
        '
        'cNumOrd
        '
        Me.cNumOrd.DataPropertyName = "cNumOrd"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumOrd.DefaultCellStyle = DataGridViewCellStyle11
        Me.cNumOrd.HeaderText = "N° Orden"
        Me.cNumOrd.Name = "cNumOrd"
        Me.cNumOrd.ReadOnly = True
        Me.cNumOrd.Width = 120
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle12
        Me.dFecReg.HeaderText = "Fecha"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle13
        Me.cRazSoc.HeaderText = "Proveedor"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 350
        '
        'TipMon
        '
        Me.TipMon.DataPropertyName = "TipMon"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipMon.DefaultCellStyle = DataGridViewCellStyle14
        Me.TipMon.HeaderText = "Moneda"
        Me.TipMon.Name = "TipMon"
        Me.TipMon.ReadOnly = True
        '
        'cEstado
        '
        Me.cEstado.DataPropertyName = "cEstado"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cEstado.DefaultCellStyle = DataGridViewCellStyle15
        Me.cEstado.HeaderText = "Estado"
        Me.cEstado.Name = "cEstado"
        Me.cEstado.ReadOnly = True
        Me.cEstado.Width = 130
        '
        'nImpTotPre
        '
        Me.nImpTotPre.DataPropertyName = "nImpTotPre"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Format = "N2"
        Me.nImpTotPre.DefaultCellStyle = DataGridViewCellStyle16
        Me.nImpTotPre.HeaderText = "Importe Total"
        Me.nImpTotPre.Name = "nImpTotPre"
        Me.nImpTotPre.ReadOnly = True
        Me.nImpTotPre.Width = 110
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.Visible = False
        '
        'cObservaciones
        '
        Me.cObservaciones.DataPropertyName = "cObservaciones"
        Me.cObservaciones.HeaderText = "cObservaciones"
        Me.cObservaciones.Name = "cObservaciones"
        Me.cObservaciones.ReadOnly = True
        Me.cObservaciones.Visible = False
        '
        'nEstado
        '
        Me.nEstado.DataPropertyName = "nEstado"
        Me.nEstado.HeaderText = "nEstado"
        Me.nEstado.Name = "nEstado"
        Me.nEstado.ReadOnly = True
        Me.nEstado.Visible = False
        '
        'Agencia
        '
        Me.Agencia.DataPropertyName = "Agencia"
        Me.Agencia.HeaderText = "Agencia"
        Me.Agencia.Name = "Agencia"
        Me.Agencia.ReadOnly = True
        Me.Agencia.Visible = False
        '
        'cMensaje
        '
        Me.cMensaje.DataPropertyName = "cMensaje"
        Me.cMensaje.HeaderText = "cMensaje"
        Me.cMensaje.Name = "cMensaje"
        Me.cMensaje.ReadOnly = True
        Me.cMensaje.Visible = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(548, 43)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(199, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 121
        Me.Label1.Text = "Desde:"
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.BackColor = System.Drawing.Color.White
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(198, 43)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(170, 22)
        Me.cmbTipoBusqueda.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1053, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(268, 14)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(100, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 14)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta:"
        '
        'dtpFecIni
        '
        Me.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecIni.Location = New System.Drawing.Point(9, 43)
        Me.dtpFecIni.Name = "dtpFecIni"
        Me.dtpFecIni.Size = New System.Drawing.Size(91, 22)
        Me.dtpFecIni.TabIndex = 3
        '
        'dtpFecFin
        '
        Me.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecFin.Location = New System.Drawing.Point(103, 43)
        Me.dtpFecFin.Name = "dtpFecFin"
        Me.dtpFecFin.Size = New System.Drawing.Size(91, 22)
        Me.dtpFecFin.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(195, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(370, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 14)
        Me.Label5.TabIndex = 131
        Me.Label5.Text = "Estado:"
        '
        'cmbEstado
        '
        Me.cmbEstado.BackColor = System.Drawing.Color.White
        Me.cmbEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEstado.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbEstado.FormattingEnabled = True
        Me.cmbEstado.Location = New System.Drawing.Point(373, 43)
        Me.cmbEstado.Name = "cmbEstado"
        Me.cmbEstado.Size = New System.Drawing.Size(169, 22)
        Me.cmbEstado.TabIndex = 6
        '
        'btnRefreescar
        '
        Me.btnRefreescar.FlatAppearance.BorderSize = 0
        Me.btnRefreescar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreescar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreescar.Image = Global.Presentacion.My.Resources.Resources.Refrescar_24
        Me.btnRefreescar.Location = New System.Drawing.Point(749, 41)
        Me.btnRefreescar.Name = "btnRefreescar"
        Me.btnRefreescar.Size = New System.Drawing.Size(23, 25)
        Me.btnRefreescar.TabIndex = 2
        Me.btnRefreescar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip.SetToolTip(Me.btnRefreescar, "Actualizar Listado(F5)")
        Me.btnRefreescar.UseVisualStyleBackColor = True
        '
        'btnAnularCompra
        '
        Me.btnAnularCompra.FlatAppearance.BorderSize = 0
        Me.btnAnularCompra.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularCompra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularCompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnularCompra.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnularCompra.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnularCompra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnularCompra.Location = New System.Drawing.Point(228, 577)
        Me.btnAnularCompra.Name = "btnAnularCompra"
        Me.btnAnularCompra.Size = New System.Drawing.Size(190, 26)
        Me.btnAnularCompra.TabIndex = 8
        Me.btnAnularCompra.Text = "Anular Orden Compra(F4)"
        Me.btnAnularCompra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnularCompra.UseVisualStyleBackColor = True
        '
        'btnNuevoCompra
        '
        Me.btnNuevoCompra.FlatAppearance.BorderSize = 0
        Me.btnNuevoCompra.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoCompra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoCompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoCompra.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevoCompra.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevoCompra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoCompra.Location = New System.Drawing.Point(9, 577)
        Me.btnNuevoCompra.Name = "btnNuevoCompra"
        Me.btnNuevoCompra.Size = New System.Drawing.Size(196, 26)
        Me.btnNuevoCompra.TabIndex = 7
        Me.btnNuevoCompra.Text = "Nuevo Orden  Compra(F1)"
        Me.btnNuevoCompra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoCompra.UseVisualStyleBackColor = True
        '
        'btnGenerarCompra
        '
        Me.btnGenerarCompra.FlatAppearance.BorderSize = 0
        Me.btnGenerarCompra.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarCompra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGenerarCompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarCompra.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarCompra.Image = Global.Presentacion.My.Resources.Resources.CuentasxPagar_32
        Me.btnGenerarCompra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerarCompra.Location = New System.Drawing.Point(436, 577)
        Me.btnGenerarCompra.Name = "btnGenerarCompra"
        Me.btnGenerarCompra.Size = New System.Drawing.Size(140, 26)
        Me.btnGenerarCompra.TabIndex = 9
        Me.btnGenerarCompra.Text = "Generar Compra"
        Me.btnGenerarCompra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerarCompra.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.FlatAppearance.BorderSize = 0
        Me.btnImprimir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnImprimir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Image = Global.Presentacion.My.Resources.Resources.Print_22
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(595, 577)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(87, 26)
        Me.btnImprimir.TabIndex = 10
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'FrmConsultaOrdenCompraImportacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnGenerarCompra)
        Me.Controls.Add(Me.btnAnularCompra)
        Me.Controls.Add(Me.btnNuevoCompra)
        Me.Controls.Add(Me.btnRefreescar)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cmbEstado)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpFecFin)
        Me.Controls.Add(Me.dtpFecIni)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaOrdenCompraImportacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFecIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As Label
    Friend WithEvents cmbEstado As ComboBox
    Friend WithEvents btnRefreescar As Button
    Friend WithEvents btnAnularCompra As Button
    Friend WithEvents btnNuevoCompra As Button
    Friend WithEvents btnGenerarCompra As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents btnImprimir As Button
    Friend WithEvents nCodOrdCom As DataGridViewTextBoxColumn
    Friend WithEvents nCodProve As DataGridViewTextBoxColumn
    Friend WithEvents cNumOrd As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents TipMon As DataGridViewTextBoxColumn
    Friend WithEvents cEstado As DataGridViewTextBoxColumn
    Friend WithEvents nImpTotPre As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents cObservaciones As DataGridViewTextBoxColumn
    Friend WithEvents nEstado As DataGridViewTextBoxColumn
    Friend WithEvents Agencia As DataGridViewTextBoxColumn
    Friend WithEvents cMensaje As DataGridViewTextBoxColumn
End Class
