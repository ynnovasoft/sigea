﻿Imports Negocios
Imports Entidades
Imports Business
Imports System.Configuration
Public Class FrmConsultaOrdenCompras
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Private Sub FrmConsultaOrdenCompras_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        dtpFecIni.Value = DateAdd(DateInterval.Year, -1, dtpFecIni.Value)
        Call CargaCombos()
        Call ValidaBotones(0)
        Call ListarOrdenesCompra(cmbEstado.SelectedValue)
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("190,180")
        Call CargaCombo(CreaDatoCombos(dt, 190), cmbTipoBusqueda)
        Call CargaCombo(CreaDatoCombos(dt, 180), cmbEstado)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call ListarOrdenesCompra(cmbEstado.SelectedValue)
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarOrdenesCompra(cmbEstado.SelectedValue)
    End Sub
    Sub ListarOrdenesCompra(ByVal nEstado As Integer)
        Dim dt As New DataTable
        Dim BLOrdenCompra As New BLOrdenCompra
        dt = BLOrdenCompra.Listar(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text,
                                       cmbTipoBusqueda.SelectedValue, nEstado, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value)
        Else
            Call ValidaBotones(0)
        End If
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call ListarOrdenesCompra(cmbEstado.SelectedValue)
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call ListarOrdenesCompra(cmbEstado.SelectedValue)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEOrdenCompra As New BEOrdenCompra
            Dim BEProveedor As New BEProveedor
            BEOrdenCompra.gnCodOrdCom = dtgListado.CurrentRow.Cells("nCodOrdCom").Value
            BEOrdenCompra.gnCodProve = dtgListado.CurrentRow.Cells("nCodProve").Value
            BEOrdenCompra.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BEOrdenCompra.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
            BEOrdenCompra.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
            BEOrdenCompra.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
            BEOrdenCompra.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
            BEOrdenCompra.gcObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
            BEOrdenCompra.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
            BEOrdenCompra.gnVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
            BEOrdenCompra.gnIGV = dtgListado.CurrentRow.Cells("nIGV").Value
            BEOrdenCompra.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
            BEOrdenCompra.g_cMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
            BEOrdenCompra.gcTipMov = dtgListado.CurrentRow.Cells("cTipMov").Value
            BEOrdenCompra.gcNumOrd = dtgListado.CurrentRow.Cells("cNumOrd").Value

            BEProveedor.gcNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEProveedor.gcDirProv = dtgListado.CurrentRow.Cells("cDirProv").Value
            BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value

            Dim Actualizar As Boolean = False
            If BEOrdenCompra.gnEstado = 1 Then
                If ValidaAperturaDia() = False Then
                    Exit Sub
                End If
            End If
            FrmRegistroOrdenCompra.InicioOrdenCompra(BEOrdenCompra, BEProveedor, 2, Actualizar)
            FrmRegistroOrdenCompra = Nothing
            If Actualizar = True Then
                Call ListarOrdenesCompra(cmbEstado.SelectedValue)
            End If
        End If
    End Sub
    Sub ValidaBotones(ByVal Estado As Integer)
        btnNuevoCompra.Enabled = True
        If Estado = 0 Then
            btnAnularCompra.Enabled = False
            btnGenerarCompra.Enabled = False
            btnImprimir.Enabled = False
            Exit Sub
        End If

        If Estado = 3 Or Estado = 4 Then
            btnAnularCompra.Enabled = False
        Else
            btnAnularCompra.Enabled = True
        End If

        If Estado = 2 Then
            btnGenerarCompra.Enabled = True
        Else
            btnGenerarCompra.Enabled = False
        End If

        If Estado = 2 Or Estado = 4 Then
            btnImprimir.Enabled = True
        Else
            btnImprimir.Enabled = False
        End If
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub tlsTitulo_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tlsTitulo.ItemClicked

    End Sub

    Private Sub cmbEstado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbEstado.SelectedIndexChanged

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbEstado_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbEstado.SelectionChangeCommitted
        Call ListarOrdenesCompra(cmbEstado.SelectedValue)
    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbEstado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbEstado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEOrdenCompra As New BEOrdenCompra
                Dim BEProveedor As New BEProveedor
                BEOrdenCompra.gnCodOrdCom = dtgListado.CurrentRow.Cells("nCodOrdCom").Value
                BEOrdenCompra.gnCodProve = dtgListado.CurrentRow.Cells("nCodProve").Value
                BEOrdenCompra.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BEOrdenCompra.gdFecVen = dtgListado.CurrentRow.Cells("dFecVen").Value
                BEOrdenCompra.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
                BEOrdenCompra.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
                BEOrdenCompra.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
                BEOrdenCompra.gcObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
                BEOrdenCompra.gnEstado = dtgListado.CurrentRow.Cells("nEstado").Value
                BEOrdenCompra.gnVenGra = dtgListado.CurrentRow.Cells("nVenGra").Value
                BEOrdenCompra.gnIGV = dtgListado.CurrentRow.Cells("nIGV").Value
                BEOrdenCompra.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
                BEOrdenCompra.g_cMensaje = dtgListado.CurrentRow.Cells("cMensaje").Value
                BEOrdenCompra.gcTipMov = dtgListado.CurrentRow.Cells("cTipMov").Value
                BEOrdenCompra.gcNumOrd = dtgListado.CurrentRow.Cells("cNumOrd").Value

                BEProveedor.gcNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEProveedor.gcDirProv = dtgListado.CurrentRow.Cells("cDirProv").Value
                BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value

                Dim Actualizar As Boolean = False
                If BEOrdenCompra.gnEstado = 1 Then
                    If ValidaAperturaDia() = False Then
                        Exit Sub
                    End If
                End If
                FrmRegistroOrdenCompra.InicioOrdenCompra(BEOrdenCompra, BEProveedor, 2, Actualizar)
                FrmRegistroOrdenCompra = Nothing
                If Actualizar = True Then
                    Call ListarOrdenesCompra(cmbEstado.SelectedValue)
                End If
            End If
        End If
    End Sub

    Private Sub btnRefreescar_Click(sender As Object, e As EventArgs) Handles btnRefreescar.Click
        Call ListarOrdenesCompra(cmbEstado.SelectedValue)
    End Sub

    Private Sub btnNuevoCompra_Click(sender As Object, e As EventArgs) Handles btnNuevoCompra.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If MDIPrincipal.CodigoBaseDatos <> 1 Then
            MsgBox("No se puede realizar ningun tipo de operaciones en una BASE DE DATOS tipificada como: BACKUP, Cierre sesiÓn y vulva a ingresar al sistema indicado la BASE DE DATOS ACTUAL", MsgBoxStyle.Critical, "Aviso")
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        FrmRegistroOrdenCompra.InicioOrdenCompra(Nothing, Nothing, 1, Actualizar)
        FrmRegistroOrdenCompra = Nothing
        If Actualizar = True Then
            Call ListarOrdenesCompra(cmbEstado.SelectedValue)
        End If
    End Sub

    Private Sub btnAnularCompra_Click(sender As Object, e As EventArgs) Handles btnAnularCompra.Click
        Dim oDatos As New BEOrdenCompra
        If MsgBox("¿Esta seguro que desea anular la orden de compra?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            Dim cMensaje As String = ""
            oDatos.gnCodOrdCom = dtgListado.CurrentRow.Cells("nCodOrdCom").Value
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            Dim BLOrdenCompra As New BLOrdenCompra
            If BLOrdenCompra.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Call ListarOrdenesCompra(cmbEstado.SelectedValue)
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnGenerarCompra_Click(sender As Object, e As EventArgs) Handles btnGenerarCompra.Click
        Dim BEProveedor As New BEProveedor
        Dim BEOrdenCompra As New BEOrdenCompra

        BEOrdenCompra.gnCodProve = dtgListado.CurrentRow.Cells("nCodProve").Value
        BEOrdenCompra.gnCodOrdCom = dtgListado.CurrentRow.Cells("nCodOrdCom").Value
        BEOrdenCompra.gcNumOrd = dtgListado.CurrentRow.Cells("cNumOrd").Value
        BEOrdenCompra.gcObservaciones = dtgListado.CurrentRow.Cells("cObservaciones").Value
        BEOrdenCompra.gnTipCam = dtgListado.CurrentRow.Cells("nTipCam").Value
        BEProveedor.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
        BEOrdenCompra.gnCondPag = dtgListado.CurrentRow.Cells("nCondPag").Value
        BEOrdenCompra.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value
        BEOrdenCompra.gcTipMov = dtgListado.CurrentRow.Cells("cTipMov").Value

        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        FrmRegistroCompras.InicioOrdenCompra(BEOrdenCompra, BEProveedor, 3, Actualizar)
        FrmRegistroCompras = Nothing
        If Actualizar = True Then
            Call ListarOrdenesCompra(cmbEstado.SelectedValue)
        End If
    End Sub

    Private Sub FrmConsultaOrdenCompras_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevoCompra.Enabled = True And btnNuevoCompra.Visible = True Then
            Call btnNuevoCompra_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnularCompra.Enabled = True And btnAnularCompra.Visible = True Then
            Call btnAnularCompra_Click(sender, e)
        ElseIf e.KeyCode = Keys.F5 And btnRefreescar.Enabled = True And btnRefreescar.Visible = True Then
            Call btnRefreescar_Click(sender, e)
        End If
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click

        Dim RutaOrdenCompra As String = ConfigurationManager.AppSettings("RutaOrdenCompraLocal")
        RutaOrdenCompra = RutaOrdenCompra + "ORDEN COMPRA N° " + dtgListado.CurrentRow.Cells("cNumOrd").Value.ToString.Trim + " - " + dtgListado.CurrentRow.Cells("cRazSoc").Value.ToString.Trim + ".pdf"

        Dim ModeloOrdenCompraLocal As String = ConfigurationManager.AppSettings("ModeloOrdenCompraLocal")
        If ModeloOrdenCompraLocal = "0" Then
            FrmReportes.ImpresionOrdenCompra(dtgListado.CurrentRow.Cells("nCodOrdCom").Value)
            FrmReportes = Nothing
        ElseIf ModeloOrdenCompraLocal = "2" Then
            Call CrearPdf_TA4_OrdenCompra_Local_EPNISAC(dtgListado.CurrentRow.Cells("nCodOrdCom").Value)
            Process.Start(RutaOrdenCompra)
        End If

    End Sub

    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs) Handles dtgListado.SelectionChanged
        If dtgListado.Rows.Count > 0 Then
            Call ValidaBotones(dtgListado.CurrentRow.Cells("nEstado").Value)
        Else
            Call ValidaBotones(0)
        End If
    End Sub
End Class