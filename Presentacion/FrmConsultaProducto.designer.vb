﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbFamilia = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodCat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cFamilia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Marca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUndMed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPrecioCompraSoles = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPrecioCompraDolares = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStock = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMarca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreComPEN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreComUSD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cSerie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bModDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStockMinimo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodOriCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonDesUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonDesMay = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodFam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodEmp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1272, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(145, 22)
        Me.lblTitulo.Text = "AGREGAR PRODUCTOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        Me.btnCerrar.ToolTipText = "Cerrar <ESC>"
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(338, 46)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(276, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(175, 46)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(162, 22)
        Me.cmbTipoBusqueda.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(172, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 14)
        Me.Label1.TabIndex = 129
        Me.Label1.Text = "Familia:"
        '
        'cmbFamilia
        '
        Me.cmbFamilia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFamilia.FormattingEnabled = True
        Me.cmbFamilia.Location = New System.Drawing.Point(9, 46)
        Me.cmbFamilia.Name = "cmbFamilia"
        Me.cmbFamilia.Size = New System.Drawing.Size(162, 22)
        Me.cmbFamilia.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label2.Location = New System.Drawing.Point(1005, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(263, 14)
        Me.Label2.TabIndex = 137
        Me.Label2.Text = "Doble Click / ENTER para agregar  un producto"
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodProd, Me.cCodCat, Me.cFamilia, Me.Marca, Me.cDescripcion, Me.cUndMed, Me.nPrecioCompraSoles, Me.nPrecioCompraDolares, Me.nValUni, Me.nPreUni, Me.nStock, Me.cMarca, Me.nPreComPEN, Me.nPreComUSD, Me.cSerie, Me.bModDes, Me.nStockMinimo, Me.cCodOriCom, Me.nMonDesUni, Me.nMonDesMay, Me.cCodFam, Me.nCodEmp})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 71)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.RowHeadersWidth = 20
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1254, 531)
        Me.dtgListado.TabIndex = 138
        '
        'nCodProd
        '
        Me.nCodProd.DataPropertyName = "nCodProd"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodProd.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodProd.HeaderText = "N° Prod."
        Me.nCodProd.Name = "nCodProd"
        Me.nCodProd.ReadOnly = True
        Me.nCodProd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCodProd.Visible = False
        Me.nCodProd.Width = 90
        '
        'cCodCat
        '
        Me.cCodCat.DataPropertyName = "cCodCat"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cCodCat.DefaultCellStyle = DataGridViewCellStyle3
        Me.cCodCat.HeaderText = "Código Catálogo"
        Me.cCodCat.Name = "cCodCat"
        Me.cCodCat.ReadOnly = True
        Me.cCodCat.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cCodCat.Width = 112
        '
        'cFamilia
        '
        Me.cFamilia.DataPropertyName = "cFamilia"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cFamilia.DefaultCellStyle = DataGridViewCellStyle4
        Me.cFamilia.HeaderText = "Familia"
        Me.cFamilia.Name = "cFamilia"
        Me.cFamilia.ReadOnly = True
        Me.cFamilia.Width = 150
        '
        'Marca
        '
        Me.Marca.DataPropertyName = "Marca"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Marca.DefaultCellStyle = DataGridViewCellStyle5
        Me.Marca.HeaderText = "Tipo Marca"
        Me.Marca.Name = "Marca"
        Me.Marca.ReadOnly = True
        Me.Marca.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Marca.Width = 150
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cDescripcion.DefaultCellStyle = DataGridViewCellStyle6
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.ReadOnly = True
        Me.cDescripcion.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cDescripcion.Width = 517
        '
        'cUndMed
        '
        Me.cUndMed.DataPropertyName = "cUndMed"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cUndMed.DefaultCellStyle = DataGridViewCellStyle7
        Me.cUndMed.HeaderText = "U/M"
        Me.cUndMed.Name = "cUndMed"
        Me.cUndMed.ReadOnly = True
        Me.cUndMed.Width = 45
        '
        'nPrecioCompraSoles
        '
        Me.nPrecioCompraSoles.DataPropertyName = "nPrecioCompraSoles"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        DataGridViewCellStyle8.Format = "N2"
        Me.nPrecioCompraSoles.DefaultCellStyle = DataGridViewCellStyle8
        Me.nPrecioCompraSoles.HeaderText = "Precio Compra"
        Me.nPrecioCompraSoles.Name = "nPrecioCompraSoles"
        Me.nPrecioCompraSoles.ReadOnly = True
        Me.nPrecioCompraSoles.Width = 80
        '
        'nPrecioCompraDolares
        '
        Me.nPrecioCompraDolares.DataPropertyName = "nPrecioCompraDolares"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        DataGridViewCellStyle9.Format = "N2"
        Me.nPrecioCompraDolares.DefaultCellStyle = DataGridViewCellStyle9
        Me.nPrecioCompraDolares.HeaderText = "Precio Compra"
        Me.nPrecioCompraDolares.Name = "nPrecioCompraDolares"
        Me.nPrecioCompraDolares.ReadOnly = True
        Me.nPrecioCompraDolares.Visible = False
        Me.nPrecioCompraDolares.Width = 80
        '
        'nValUni
        '
        Me.nValUni.DataPropertyName = "nValUni"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.nValUni.DefaultCellStyle = DataGridViewCellStyle10
        Me.nValUni.HeaderText = "Valor Unitario"
        Me.nValUni.Name = "nValUni"
        Me.nValUni.ReadOnly = True
        Me.nValUni.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nValUni.Visible = False
        Me.nValUni.Width = 80
        '
        'nPreUni
        '
        Me.nPreUni.DataPropertyName = "nPreUni"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.nPreUni.DefaultCellStyle = DataGridViewCellStyle11
        Me.nPreUni.HeaderText = "Precio Unitario"
        Me.nPreUni.Name = "nPreUni"
        Me.nPreUni.ReadOnly = True
        Me.nPreUni.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nPreUni.Width = 80
        '
        'nStock
        '
        Me.nStock.DataPropertyName = "nStock"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.nStock.DefaultCellStyle = DataGridViewCellStyle12
        Me.nStock.HeaderText = "Stock"
        Me.nStock.Name = "nStock"
        Me.nStock.ReadOnly = True
        Me.nStock.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nStock.Width = 98
        '
        'cMarca
        '
        Me.cMarca.DataPropertyName = "cMarca"
        Me.cMarca.HeaderText = "cMarca"
        Me.cMarca.Name = "cMarca"
        Me.cMarca.ReadOnly = True
        Me.cMarca.Visible = False
        '
        'nPreComPEN
        '
        Me.nPreComPEN.DataPropertyName = "nPreComPEN"
        Me.nPreComPEN.HeaderText = "nPreComPEN"
        Me.nPreComPEN.Name = "nPreComPEN"
        Me.nPreComPEN.ReadOnly = True
        Me.nPreComPEN.Visible = False
        '
        'nPreComUSD
        '
        Me.nPreComUSD.DataPropertyName = "nPreComUSD"
        Me.nPreComUSD.HeaderText = "nPreComUSD"
        Me.nPreComUSD.Name = "nPreComUSD"
        Me.nPreComUSD.ReadOnly = True
        Me.nPreComUSD.Visible = False
        '
        'cSerie
        '
        Me.cSerie.DataPropertyName = "cSerie"
        Me.cSerie.HeaderText = "cSerie"
        Me.cSerie.Name = "cSerie"
        Me.cSerie.ReadOnly = True
        Me.cSerie.Visible = False
        '
        'bModDes
        '
        Me.bModDes.DataPropertyName = "bModDes"
        Me.bModDes.HeaderText = "bModDes"
        Me.bModDes.Name = "bModDes"
        Me.bModDes.ReadOnly = True
        Me.bModDes.Visible = False
        '
        'nStockMinimo
        '
        Me.nStockMinimo.DataPropertyName = "nStockMinimo"
        Me.nStockMinimo.HeaderText = "nStockMinimo"
        Me.nStockMinimo.Name = "nStockMinimo"
        Me.nStockMinimo.ReadOnly = True
        Me.nStockMinimo.Visible = False
        '
        'cCodOriCom
        '
        Me.cCodOriCom.DataPropertyName = "cCodOriCom"
        Me.cCodOriCom.HeaderText = "cCodOriCom"
        Me.cCodOriCom.Name = "cCodOriCom"
        Me.cCodOriCom.ReadOnly = True
        Me.cCodOriCom.Visible = False
        '
        'nMonDesUni
        '
        Me.nMonDesUni.DataPropertyName = "nMonDesUni"
        Me.nMonDesUni.HeaderText = "nMonDesUni"
        Me.nMonDesUni.Name = "nMonDesUni"
        Me.nMonDesUni.ReadOnly = True
        Me.nMonDesUni.Visible = False
        '
        'nMonDesMay
        '
        Me.nMonDesMay.DataPropertyName = "nMonDesMay"
        Me.nMonDesMay.HeaderText = "nMonDesMay"
        Me.nMonDesMay.Name = "nMonDesMay"
        Me.nMonDesMay.ReadOnly = True
        Me.nMonDesMay.Visible = False
        '
        'cCodFam
        '
        Me.cCodFam.DataPropertyName = "cCodFam"
        Me.cCodFam.HeaderText = "cCodFam"
        Me.cCodFam.Name = "cCodFam"
        Me.cCodFam.ReadOnly = True
        Me.cCodFam.Visible = False
        '
        'nCodEmp
        '
        Me.nCodEmp.DataPropertyName = "nCodEmp"
        Me.nCodEmp.HeaderText = "nCodEmp"
        Me.nCodEmp.Name = "nCodEmp"
        Me.nCodEmp.ReadOnly = True
        Me.nCodEmp.Visible = False
        '
        'FrmConsultaProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1272, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbFamilia)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaProducto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbFamilia As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents dtgListado As DataGridView
    Friend WithEvents nCodProd As DataGridViewTextBoxColumn
    Friend WithEvents cCodCat As DataGridViewTextBoxColumn
    Friend WithEvents cFamilia As DataGridViewTextBoxColumn
    Friend WithEvents Marca As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents cUndMed As DataGridViewTextBoxColumn
    Friend WithEvents nPrecioCompraSoles As DataGridViewTextBoxColumn
    Friend WithEvents nPrecioCompraDolares As DataGridViewTextBoxColumn
    Friend WithEvents nValUni As DataGridViewTextBoxColumn
    Friend WithEvents nPreUni As DataGridViewTextBoxColumn
    Friend WithEvents nStock As DataGridViewTextBoxColumn
    Friend WithEvents cMarca As DataGridViewTextBoxColumn
    Friend WithEvents nPreComPEN As DataGridViewTextBoxColumn
    Friend WithEvents nPreComUSD As DataGridViewTextBoxColumn
    Friend WithEvents cSerie As DataGridViewTextBoxColumn
    Friend WithEvents bModDes As DataGridViewTextBoxColumn
    Friend WithEvents nStockMinimo As DataGridViewTextBoxColumn
    Friend WithEvents cCodOriCom As DataGridViewTextBoxColumn
    Friend WithEvents nMonDesUni As DataGridViewTextBoxColumn
    Friend WithEvents nMonDesMay As DataGridViewTextBoxColumn
    Friend WithEvents cCodFam As DataGridViewTextBoxColumn
    Friend WithEvents nCodEmp As DataGridViewTextBoxColumn
End Class
