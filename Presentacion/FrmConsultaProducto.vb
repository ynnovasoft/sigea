﻿Imports Negocios
Imports Business
Imports System.Configuration
Public Class FrmConsultaProducto
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim CodProd As Integer = 0
    Dim cMensaje As String = ""
    Dim Codigo As Integer = 0
    Dim Tipo As String = ""
    Sub Inicio(ByVal pnCodigo As Integer, ByVal pnTipo As String)
        Codigo = pnCodigo
        Tipo = pnTipo
        Me.ShowDialog()
    End Sub
    Private Sub FrmConsultaProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargaFamilia()
        Call BuscarProductos()
        txtBuscar.Select()
    End Sub
    Sub CargaCombos()
        Dim BLComunes As New BLCommons
        Dim dt As New DataTable
        dt = BLComunes.MostrarMaestro("110")
        Call CargaCombo(CreaDatoCombos(dt, 110), cmbTipoBusqueda)
        cmbTipoBusqueda.SelectedValue = Convert.ToInt32(ConfigurationManager.AppSettings("OrdenProductos"))
    End Sub
    Sub CargaFamilia()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarFamilia(0)
        Call CargaCombo(dt, cmbFamilia)
    End Sub
    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Select()
        Call BuscarProductos()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call BuscarProductos()
    End Sub
    Sub BuscarProductos()
        Dim dt As New DataTable
        Dim BLProductos As New BLProducts
        dt = BLProductos.ListarProductos(txtBuscar.Text, cmbTipoBusqueda.SelectedValue,
                                             MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa, cmbFamilia.SelectedValue)
        Call LlenaAGridView(dt, dtgListado)
        If MDIPrincipal.CodigoPrecioLista = 1 Then
            dtgListado.Columns("nValUni").Visible = False
            dtgListado.Columns("nPreUni").Visible = True
        Else
            dtgListado.Columns("nValUni").Visible = True
            dtgListado.Columns("nPreUni").Visible = False
        End If
        If MDIPrincipal.CodigoMonedaPrecio = 1 Then
            dtgListado.Columns("nPrecioCompraSoles").Visible = True
            dtgListado.Columns("nPrecioCompraDolares").Visible = False
        Else
            dtgListado.Columns("nPrecioCompraSoles").Visible = False
            dtgListado.Columns("nPrecioCompraDolares").Visible = True
        End If
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Sub AgregarProductoDetalleCotizacion(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim Obj As New BLCotizacion
        If Obj.AgregarProducto(Codigo, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Public Sub AgregarProductoDetallePedido(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim Obj As New BLPedido
        If Obj.AgregarProducto(Codigo, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub AgregarProductoDetalleCompras(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim Obj As New BLPurchase
        If Obj.AgregarProducto(Codigo, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub AgregarProductoDetalleOrdenCompra(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim Obj As New BLOrdenCompra
        If Obj.AgregarProducto(Codigo, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub AgregarProductoDetalleAlmacen(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim Obj As New BLAlmacen
        If Obj.AgregarProducto(Codigo, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub AgregarProductoDetalleGuia(ByVal CodProd As Integer, ByVal Cantidad As Decimal)
        cMensaje = ""
        Dim Obj As New BLGuia
        If Obj.AgregarProducto(Codigo, CodProd, Cantidad, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub AgregarProductoDetalleOrdenCompraImportacion(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim Obj As New BLOrdenCompraImportacion
        If Obj.AgregarProducto(Codigo, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbFamilia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFamilia.SelectedIndexChanged

    End Sub

    Private Sub FrmConsultaProducto_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub cmbFamilia_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbFamilia.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Select()
        Call BuscarProductos()
    End Sub

    Private Sub cmbFamilia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbFamilia.KeyPress
        ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                If Tipo = "02" Then
                    Call AgregarProductoDetalleCotizacion(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                    FrmRegistroCotizacion.RecuperarDetalle()
                ElseIf Tipo = "01" Or Tipo = "03" Or Tipo = "99" Then
                    Call AgregarProductoDetallePedido(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                    FrmTomaPedidos.RecuperarDetalle()
                ElseIf Tipo = "80" Then
                    Call AgregarProductoDetalleCompras(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                    FrmRegistroCompras.RecuperarDetalle()
                ElseIf Tipo = "70" Then
                    Call AgregarProductoDetalleOrdenCompra(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                    FrmRegistroOrdenCompra.RecuperarDetalle()
                ElseIf Tipo = "09" Then
                    Call AgregarProductoDetalleGuia(dtgListado.CurrentRow.Cells("nCodProd").Value, 1)
                    FrmRegistroGuias.RecuperarDetalle()
                ElseIf Tipo = "05" Then
                    Call AgregarProductoDetalleAlmacen(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                    FrmParteSalida.RecuperarDetalle()
                ElseIf Tipo = "06" Then
                    Call AgregarProductoDetalleAlmacen(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                    FrmParteIngreso.RecuperarDetalle()
                ElseIf Tipo = "07" Then
                    Call AgregarProductoDetallePedido(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                    FrmRegistroNotaCredito.RecuperarDetalle()
                End If
            End If
        End If
    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            If Tipo = "02" Then
                Call AgregarProductoDetalleCotizacion(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                FrmRegistroCotizacion.RecuperarDetalle()
            ElseIf Tipo = "01" Or Tipo = "03" Or Tipo = "99" Then
                Call AgregarProductoDetallePedido(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                FrmTomaPedidos.RecuperarDetalle()
            ElseIf Tipo = "80" Then
                Call AgregarProductoDetalleCompras(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                FrmRegistroCompras.RecuperarDetalle()
            ElseIf Tipo = "70" Then
                Call AgregarProductoDetalleOrdenCompra(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                FrmRegistroOrdenCompra.RecuperarDetalle()
            ElseIf Tipo = "09" Then
                Call AgregarProductoDetalleGuia(dtgListado.CurrentRow.Cells("nCodProd").Value, 1)
                FrmRegistroGuias.RecuperarDetalle()
            ElseIf Tipo = "05" Then
                Call AgregarProductoDetalleAlmacen(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                FrmParteSalida.RecuperarDetalle()
            ElseIf Tipo = "06" Then
                Call AgregarProductoDetalleAlmacen(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                FrmParteIngreso.RecuperarDetalle()
            ElseIf Tipo = "07" Then
                Call AgregarProductoDetallePedido(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                FrmRegistroNotaCredito.RecuperarDetalle()
            ElseIf Tipo = "100" Then
                Call AgregarProductoDetalleOrdenCompraImportacion(dtgListado.CurrentRow.Cells("nCodProd").Value, 1, 0)
                FrmRegistroOrdenCompraImportacion.RecuperarDetalle()
            End If
        End If
    End Sub
End Class