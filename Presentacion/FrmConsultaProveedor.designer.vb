﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodProve = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.LightSkyBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.LightSkyBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(728, 29)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.LightSkyBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.Black
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(188, 26)
        Me.lblTitulo.Text = "CONSULTA DE PROVEEDORES"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.PowderBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodProve, Me.cNumDoc, Me.cRazSoc})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.Location = New System.Drawing.Point(9, 61)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(710, 350)
        Me.dtgListado.TabIndex = 2
        '
        'nCodProve
        '
        Me.nCodProve.DataPropertyName = "nCodProve"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodProve.DefaultCellStyle = DataGridViewCellStyle2
        Me.nCodProve.HeaderText = "nCodProve"
        Me.nCodProve.Name = "nCodProve"
        Me.nCodProve.ReadOnly = True
        Me.nCodProve.Visible = False
        Me.nCodProve.Width = 90
        '
        'cNumDoc
        '
        Me.cNumDoc.DataPropertyName = "cNumDoc"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumDoc.DefaultCellStyle = DataGridViewCellStyle3
        Me.cNumDoc.HeaderText = "N° Documento"
        Me.cNumDoc.Name = "cNumDoc"
        Me.cNumDoc.ReadOnly = True
        Me.cNumDoc.Width = 210
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cRazSoc.DefaultCellStyle = DataGridViewCellStyle4
        Me.cRazSoc.HeaderText = "Razon Social"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cRazSoc.Width = 480
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(249, 36)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(156, 23)
        Me.txtBuscar.TabIndex = 1
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(84, 36)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(163, 24)
        Me.cmbTipoBusqueda.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(417, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(285, 16)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 16)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'FrmConsultaProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(728, 420)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaProveedor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents nCodProve As DataGridViewTextBoxColumn
    Friend WithEvents cNumDoc As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
End Class
