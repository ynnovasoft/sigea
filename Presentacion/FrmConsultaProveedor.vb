﻿Imports Negocios
Imports Business
Imports Entidades
Imports System.Windows.Forms
Public Class FrmConsultaProveedor
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim dt As DataTable
    Dim BLComunes As New BLCommons
    Dim BLProveedor As New BLProveedor
    Dim oDatos As New BEProveedor
    Sub Inicio(ByRef pnoDatos As BEProveedor)

        Me.ShowDialog()
        pnoDatos = oDatos
    End Sub

    Private Sub FrmConsultaProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaCombos()
        Call BuscarProductos()
        txtBuscar.Focus()
    End Sub
    Sub CargaCombos()
        Try
            dt = BLComunes.MostrarMaestro("200")
            Call CargaCombo(CreaDatoCombos(dt, 200), cmbTipoBusqueda)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            dt = Nothing
        End Try
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call BuscarProductos()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call BuscarProductos()
    End Sub
    Sub BuscarProductos()
        Try
            dt = BLProveedor.Listar(txtBuscar.Text, cmbTipoBusqueda.SelectedValue)
            Call LlenaAGridView(dt, dtgListado)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            dt = Nothing
        End Try
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            oDatos.gnCodProv = dtgListado.CurrentRow.Cells("nCodProve").Value
            oDatos.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            Me.Close()
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                oDatos.gnCodProv = dtgListado.CurrentRow.Cells("nCodProve").Value
                oDatos.gcRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                Me.Close()
            End If
        End If
    End Sub

    Private Sub dtgListado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtgListado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class