﻿Imports Negocios
Imports Business
Imports Entidades
Imports Entities
Public Class FrmConsultaRetencion
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim dt As DataTable
    Dim BLComunes As New BLCommons
    Dim BLRetencion As New BLRetencion
    Dim BEClientes As New BECustomer
    Dim BERetencion As New BERetencion

    Private Sub FrmConsultaRetencion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpFecIni.Value = DateAdd(DateInterval.Month, -1, dtpFecIni.Value)
        Call CargaCombos()
        Call Buscar()
        txtBuscar.Focus()
    End Sub
    Sub InicioRetencion(ByRef oRet As BERetencion, ByRef oCli As BECustomer)
        Me.ShowDialog()
        oRet = BERetencion
        oCli = BEClientes
    End Sub
    Sub InicioAnulacion(ByRef pnCodigo As Integer, ByRef pnNumDoc As String, ByRef pdFecReg As Date)
        Me.ShowDialog()
        pnCodigo = BERetencion.gnCodRet
        pnNumDoc = BERetencion.gnCodRet
        pdFecReg = BERetencion.gdFecReg
    End Sub
    Sub CargaCombos()
        Try
            dt = BLComunes.MostrarMaestro("220")
            Call CargaCombo(CreaDatoCombos(dt, 220), cmbTipoBusqueda)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            dt = Nothing
        End Try
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call Buscar()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call Buscar()
    End Sub
    Sub Buscar()
        Try
            dt = BLRetencion.Listar(dtpFecIni.Value, dtpFecFin.Value, txtBuscar.Text,
                                  cmbTipoBusqueda.SelectedValue, MDIPrincipal.CodigoEmpresa)
            Call LlenaAGridView(dt, dtgListado)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            dt = Nothing
        End Try
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call Buscar()
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call Buscar()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            BERetencion.gnCodRet = dtgListado.CurrentRow.Cells("nCodRet").Value
            BERetencion.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
            BERetencion.gdFecEmi = dtgListado.CurrentRow.Cells("dFecEmi").Value
            BERetencion.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
            BERetencion.gnMonRet = dtgListado.CurrentRow.Cells("nMonRet").Value
            BERetencion.gnImpNetPag = dtgListado.CurrentRow.Cells("nImpNetPag").Value
            BERetencion.gcNumRet = dtgListado.CurrentRow.Cells("cNumRet").Value
            BERetencion.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value

            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            Me.Close()
        End If
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                BERetencion.gnCodRet = dtgListado.CurrentRow.Cells("nCodRet").Value
                BERetencion.gdFecReg = dtgListado.CurrentRow.Cells("dFecReg").Value
                BERetencion.gdFecEmi = dtgListado.CurrentRow.Cells("dFecEmi").Value
                BERetencion.gnImpTot = dtgListado.CurrentRow.Cells("nImpTot").Value
                BERetencion.gnMonRet = dtgListado.CurrentRow.Cells("nMonRet").Value
                BERetencion.gnImpNetPag = dtgListado.CurrentRow.Cells("nImpNetPag").Value
                BERetencion.gcNumRet = dtgListado.CurrentRow.Cells("cNumRet").Value
                BERetencion.gnTipMon = dtgListado.CurrentRow.Cells("nTipMon").Value

                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                Me.Close()
            End If
        End If
    End Sub
End Class