﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaValidacionComprobante
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.lblEstadoDomicilio = New System.Windows.Forms.Label()
        Me.lblEstadoContribuyente = New System.Windows.Forms.Label()
        Me.lblEstadoComprobante = New System.Windows.Forms.Label()
        Me.tlsTitulo.SuspendLayout()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(377, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(272, 22)
        Me.lblTitulo.Text = "DETALLES DE COMPROBANTE ELECTRONCO "
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 119)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(267, 14)
        Me.Label1.TabIndex = 135
        Me.Label1.Text = "Estado del contribuyente a la fecha de emisión:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 42)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(280, 14)
        Me.Label4.TabIndex = 149
        Me.Label4.Text = "Estado del comprobante a la fecha de la consulta:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(11, 199)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(248, 14)
        Me.Label9.TabIndex = 157
        Me.Label9.Text = "Condición de domicilio a la fecha de emisión:"
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'lblEstadoDomicilio
        '
        Me.lblEstadoDomicilio.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblEstadoDomicilio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEstadoDomicilio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoDomicilio.ForeColor = System.Drawing.Color.Black
        Me.lblEstadoDomicilio.Location = New System.Drawing.Point(14, 222)
        Me.lblEstadoDomicilio.Name = "lblEstadoDomicilio"
        Me.lblEstadoDomicilio.Size = New System.Drawing.Size(350, 22)
        Me.lblEstadoDomicilio.TabIndex = 158
        Me.lblEstadoDomicilio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblEstadoContribuyente
        '
        Me.lblEstadoContribuyente.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblEstadoContribuyente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEstadoContribuyente.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoContribuyente.ForeColor = System.Drawing.Color.Black
        Me.lblEstadoContribuyente.Location = New System.Drawing.Point(14, 141)
        Me.lblEstadoContribuyente.Name = "lblEstadoContribuyente"
        Me.lblEstadoContribuyente.Size = New System.Drawing.Size(350, 22)
        Me.lblEstadoContribuyente.TabIndex = 159
        Me.lblEstadoContribuyente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblEstadoComprobante
        '
        Me.lblEstadoComprobante.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblEstadoComprobante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEstadoComprobante.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoComprobante.ForeColor = System.Drawing.Color.Black
        Me.lblEstadoComprobante.Location = New System.Drawing.Point(14, 64)
        Me.lblEstadoComprobante.Name = "lblEstadoComprobante"
        Me.lblEstadoComprobante.Size = New System.Drawing.Size(350, 22)
        Me.lblEstadoComprobante.TabIndex = 160
        Me.lblEstadoComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmConsultaValidacionComprobante
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(377, 258)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblEstadoComprobante)
        Me.Controls.Add(Me.lblEstadoContribuyente)
        Me.Controls.Add(Me.lblEstadoDomicilio)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaValidacionComprobante"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnMover As Panel
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblEstadoDomicilio As Label
    Friend WithEvents lblEstadoContribuyente As Label
    Friend WithEvents lblEstadoComprobante As Label
End Class
