﻿Imports Negocios
Imports Entidades
Imports System.Windows.Forms
Imports System.Configuration
Imports DASunat.ServicioComprobantes
Public Class FrmConsultaValidacionComprobante
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Private Sub FrmConsultaValidacionComprobante_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True

    End Sub
    Sub Inicio(ByVal Dato As BEResponseValidezComprobante)
        If Dato.data.estadoCp = 0 Then
            lblEstadoComprobante.Text = "NO EXISTE (Comprobante no informado)"
        ElseIf Dato.data.estadoCp = 1 Then
            lblEstadoComprobante.Text = "ACEPTADO: (Comprobante aceptado)"
        ElseIf Dato.data.estadoCp = 2 Then
            lblEstadoComprobante.Text = "ANULADO: (Comunicado en una baja)"
        ElseIf Dato.data.estadoCp = 3 Then
            lblEstadoComprobante.Text = "AUTORIZADO: (con autorización de imprenta)"
        ElseIf Dato.data.estadoCp = 4 Then
            lblEstadoComprobante.Text = "NO AUTORIZADO: (no autorizado por imprenta)"
        End If
        If Dato.data.estadoRuc = "00" Then
            lblEstadoContribuyente.Text = "ACTIVO"
        ElseIf Dato.data.estadoRuc = "01" Then
            lblEstadoContribuyente.Text = "BAJA PROVISIONAL"
        ElseIf Dato.data.estadoRuc = "02" Then
            lblEstadoContribuyente.Text = "BAJA PROV. POR OFICIO"
        ElseIf Dato.data.estadoRuc = "03" Then
            lblEstadoContribuyente.Text = "SUSPENSION TEMPORAL"
        ElseIf Dato.data.estadoRuc = "10" Then
            lblEstadoContribuyente.Text = "BAJA DEFINITIVA"
        ElseIf Dato.data.estadoRuc = "11" Then
            lblEstadoContribuyente.Text = "BAJA DE OFICIO"
        ElseIf Dato.data.estadoRuc = "22" Then
            lblEstadoContribuyente.Text = "INHABILITADO-VENT.UNICA"
        Else
            lblEstadoContribuyente.Text = "--"
        End If
        If Dato.data.condDomiRuc = "00" Then
            lblEstadoDomicilio.Text = "HABIDO"
        ElseIf Dato.data.condDomiRuc = "09" Then
            lblEstadoDomicilio.Text = "PENDIENTE"
        ElseIf Dato.data.condDomiRuc = "11" Then
            lblEstadoDomicilio.Text = "POR VERIFICAR"
        ElseIf Dato.data.condDomiRuc = "12" Then
            lblEstadoDomicilio.Text = "NO HABIDO"
        ElseIf Dato.data.condDomiRuc = "20" Then
            lblEstadoDomicilio.Text = "NO HALLADO"
        Else
            lblEstadoDomicilio.Text = "--"
        End If
        Me.ShowDialog()
    End Sub
    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub FrmConsultaValidacionComprobante_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class