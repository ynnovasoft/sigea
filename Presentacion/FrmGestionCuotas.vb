﻿Imports Negocios
Imports Entidades
Imports System.Windows.Forms
Public Class FrmGestionCuotas
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim CodigoPedido As Integer = 0
    Dim Vista As Boolean = False
    Private Sub FrmGestionCuotas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        If Vista = True Then
            txtMonto.Enabled = True
            dtpFecVen.Enabled = True
            btnGrabar.Enabled = True
            dtgListado.Columns("Eliminar").Visible = True
            dtgListado.Columns("dFecVen").Width = 140
        Else
            txtMonto.Enabled = False
            dtpFecVen.Enabled = False
            btnGrabar.Enabled = False
            dtgListado.Columns("Eliminar").Visible = False
            dtgListado.Columns("dFecVen").Width = 240
        End If
        Call ListarCuotas()
    End Sub
    Sub Inicio(ByVal nCodigo As Integer, ByVal bVista As Boolean)
        CodigoPedido = nCodigo
        Vista = bVista

        Me.ShowDialog()
    End Sub
    Sub ListarCuotas()
        Dim dt As DataTable
        Dim Obj As New BLPedido
        dt = Obj.ListarCuotas(CodigoPedido)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub dtpFecVen_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecVen.ValueChanged

    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub dtpFecVen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecVen.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_TextChanged(sender As Object, e As EventArgs) Handles txtMonto.TextChanged

    End Sub
    Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress
        Call ValidaDecimales(e, txtMonto)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_GotFocus(sender As Object, e As EventArgs) Handles txtMonto.GotFocus
        Call ValidaFormatoMonedaGot(txtMonto)
    End Sub

    Private Sub txtMonto_LostFocus(sender As Object, e As EventArgs) Handles txtMonto.LostFocus
        Call ValidaFormatoMonedaLost(txtMonto)
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        cMensaje = ""
        If dtgListado.Rows.Count > 0 Then
            Dim senderGrid = DirectCast(sender, DataGridView)
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                If dtgListado.Columns(e.ColumnIndex).Name = "Eliminar" Then
                    If MsgBox("¿Esta seguro que desea eliminar la cuota?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                        Dim BLPedido As New BLPedido
                        If BLPedido.EliminarCuota(dtgListado.CurrentRow.Cells("nCodPedCuo").Value, cMensaje) = True Then
                            If cMensaje <> "" Then
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                                Exit Sub
                            End If
                            Call ListarCuotas()
                            txtMonto.Focus()
                        Else
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        cMensaje = ""
        Dim Obj As New BLPedido
        If Obj.RegistrarCuota(CodigoPedido, "Cuota00" & dtgListado.Rows.Count + 1, txtMonto.Text, dtpFecVen.Value, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Call ListarCuotas()
            txtMonto.Text = "0.00"
            txtMonto.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub FrmGestionCuotas_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class