﻿Imports Negocios
Imports Business
Imports Entidades
Imports System.Windows.Forms
Public Class FrmGestionLetras
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Dim bRespuesta As Boolean = False
    Dim CodigoLetra As Integer = 0
    Dim Moneda As Integer = 1
    Private Sub FrmGestionLetras_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaCombos()
        Call CargaOperacion()
        Call CargaMedioPago()

        txtMonto.Enabled = False
        dtpFecVen.Enabled = False
        cmbMedioPago.Enabled = False
        btnGrabar.Enabled = False
        cmbBanco.Enabled = False
        cmbMoneda.Enabled = False
        txtNumOpe.Enabled = False
        txtMontoPago.Enabled = False
        txtTipoCambio.Enabled = False
        dtpFecVen.Value = MDIPrincipal.FechaSistema
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        cmbMoneda.SelectedValue = Moneda
        If cmbMoneda.SelectedValue = 1 Then
            Label16.Text = "Monto Dolares:"
        Else
            Label16.Text = "Monto Soles:"
        End If

    End Sub
    Sub Inicio(ByVal Monto As Double, ByRef pnRespuesta As Boolean, ByVal pnCodigoLetra As Integer, ByVal nMoneda As Integer)
        CodigoLetra = pnCodigoLetra
        txtMontoTotal.Text = FormatNumber(Monto, 2)
        Moneda = nMoneda
        Me.ShowDialog()
        pnRespuesta = bRespuesta
    End Sub
    Sub CargaMedioPago()
        Dim BLComunes As New BLCommons
        Dim dt As New DataTable
        dt = BLComunes.MostrarMaestroUnico(80, "1,2,3,4,5,10")
        Call CargaCombo(dt, cmbMedioPago)

    End Sub
    Sub CargaOperacion()
        Dim BLComunes As New BLCommons
        Dim dt As New DataTable
        dt = BLComunes.MostrarMaestroUnico(300, "2,4,5")
        Call CargaCombo(dt, cmbTipOpe)
        cmbTipOpe.SelectedValue = 0

    End Sub
    Sub CargaCombos()
        Dim BLComunes As New BLCommons
        Dim dt As New DataTable
        dt = BLComunes.MostrarMaestro("60,340")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
        Label16.Text = "Monto Dolares:"
        Call CargaCombo(CreaDatoCombos(dt, 340), cmbBanco)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 6))
    End Sub

    Private Sub dtpFecVen_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecVen.ValueChanged

    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub cmbTipOpe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipOpe.SelectedIndexChanged

    End Sub

    Private Sub dtpFecVen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecVen.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_TextChanged(sender As Object, e As EventArgs) Handles txtMonto.TextChanged

    End Sub

    Private Sub cmbTipOpe_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipOpe.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress
        Call ValidaDecimales(e, txtMonto)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_GotFocus(sender As Object, e As EventArgs) Handles txtMonto.GotFocus
        Call ValidaFormatoMonedaGot(txtMonto)
    End Sub

    Private Sub txtMonto_LostFocus(sender As Object, e As EventArgs) Handles txtMonto.LostFocus
        Call ValidaFormatoMonedaLost(txtMonto)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Dim oDatos As New BELetras
        cMensaje = ""
        oDatos.gnCodLet = CodigoLetra
        oDatos.gdFecReg = MDIPrincipal.FechaSistema
        oDatos.gdFecGir = MDIPrincipal.FechaSistema
        oDatos.gdFecVen = dtpFecVen.Value
        oDatos.gnMonto = txtMontoPago.Text
        oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gnCodLetRef = CodigoLetra
        oDatos.gnMonRen = txtMonto.Text
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        Dim BLLetras As New BLLetras
        If BLLetras.Gestionar(oDatos, cmbTipOpe.SelectedValue, cmbMedioPago.SelectedValue, cmbMoneda.SelectedValue,
                                  cmbBanco.SelectedValue, txtTipoCambio.Text,
                                  txtNumOpe.Text, MDIPrincipal.CodigoEmpresa, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            If cmbTipOpe.SelectedValue = 4 Then
                MsgBox("Se Canceló la letra correctamente", MsgBoxStyle.Information, "MENSAJE")
            ElseIf cmbTipOpe.SelectedValue = 2 Then
                MsgBox("Se protestó la letra correctamente", MsgBoxStyle.Information, "MENSAJE")
            ElseIf cmbTipOpe.SelectedValue = 5 Then
                MsgBox("Se Registró la letra N° " + oDatos.gcNumLet, MsgBoxStyle.Information, "MENSAJE")
            End If
            bRespuesta = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub cmbMedioPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMedioPago.SelectedIndexChanged

    End Sub

    Private Sub cmbTipOpe_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipOpe.SelectionChangeCommitted
        btnGrabar.Enabled = True
        txtMonto.Text = "0.00"
        cmbMedioPago.SelectedValue = 1
        cmbBanco.SelectedValue = 0
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        cmbMoneda.SelectedValue = Moneda
        If cmbTipOpe.SelectedValue = 2 Then
            txtMonto.Enabled = False
            dtpFecVen.Enabled = True
            cmbMedioPago.Enabled = False
            cmbBanco.Enabled = False
            cmbMoneda.Enabled = False
            txtNumOpe.Enabled = False
            txtMontoPago.Enabled = False
            txtTipoCambio.Enabled = False
            txtMontoPago.Text = FormatNumber(txtMontoTotal.Text, 2)
            dtpFecVen.Focus()
        ElseIf cmbTipOpe.SelectedValue = 4 Then
            txtMonto.Enabled = False
            dtpFecVen.Enabled = False
            cmbMedioPago.Enabled = True
            cmbBanco.Enabled = False
            cmbMoneda.Enabled = True
            txtNumOpe.Enabled = True
            txtMontoPago.Enabled = True
            txtTipoCambio.Enabled = True
            txtMontoPago.Text = FormatNumber(CDbl(txtMontoTotal.Text) + CDbl(txtMonto.Text), 2)
            cmbMedioPago.Focus()
        ElseIf cmbTipOpe.SelectedValue = 5 Then
            txtMonto.Enabled = True
            dtpFecVen.Enabled = True
            cmbMedioPago.Enabled = True
            cmbBanco.Enabled = False
            cmbMoneda.Enabled = True
            txtNumOpe.Enabled = True
            txtMontoPago.Enabled = True
            txtTipoCambio.Enabled = True
            txtMonto.Text = FormatNumber(txtMontoTotal.Text, 2)
            txtMontoPago.Text = "0.00"
            dtpFecVen.Focus()
        End If
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / txtTipoCambio.Text, 2)
            Label16.Text = "Monto Dolares:"
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * txtTipoCambio.Text, 2)
            Label16.Text = "Monto Soles:"
        End If
    End Sub

    Private Sub cmbBanco_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBanco.SelectedIndexChanged

    End Sub

    Private Sub cmbMedioPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMedioPago.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtNumOpe_TextChanged(sender As Object, e As EventArgs) Handles txtNumOpe.TextChanged

    End Sub

    Private Sub cmbBanco_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbBanco.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub txtNumOpe_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumOpe.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub

    Private Sub txtMontoPago_TextChanged(sender As Object, e As EventArgs) Handles txtMontoPago.TextChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMontoPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMontoPago.KeyPress
        Call ValidaDecimales(e, txtMontoPago)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMontoPago_GotFocus(sender As Object, e As EventArgs) Handles txtMontoPago.GotFocus
        Call ValidaFormatoMonedaGot(txtMontoPago)
    End Sub

    Private Sub lblTexto_Click(sender As Object, e As EventArgs) Handles lblTexto.Click

    End Sub

    Private Sub txtMontoPago_LostFocus(sender As Object, e As EventArgs) Handles txtMontoPago.LostFocus
        Call ValidaFormatoMonedaLost(txtMontoPago)
    End Sub

    Private Sub txtMonto_Leave(sender As Object, e As EventArgs) Handles txtMonto.Leave
        If cmbTipOpe.SelectedValue <> 2 Then
            txtMontoPago.Text = FormatNumber(CDbl(txtMontoTotal.Text) - CDbl(txtMonto.Text), 2)
        End If
    End Sub

    Private Sub cmbMedioPago_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMedioPago.SelectionChangeCommitted
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        txtNumOpe.Text = ""
        If cmbMedioPago.SelectedValue = 4 Or cmbMedioPago.SelectedValue = 5 Then
            cmbBanco.SelectedValue = 1
            cmbBanco.Enabled = True
            cmbBanco.Focus()
        Else
            cmbBanco.SelectedValue = 0
            cmbBanco.Enabled = False
            txtNumOpe.Focus()
        End If
    End Sub

    Private Sub txtTipoCambio_Leave(sender As Object, e As EventArgs) Handles txtTipoCambio.Leave
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / txtTipoCambio.Text, 2)
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * txtTipoCambio.Text, 2)
        End If
    End Sub

    Private Sub cmbMoneda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMoneda.SelectionChangeCommitted
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / txtTipoCambio.Text, 2)
            Label16.Text = "Monto Dolares:"
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * txtTipoCambio.Text, 2)
            Label16.Text = "Monto Soles:"
        End If
    End Sub

    Private Sub txtMontoPago_Leave(sender As Object, e As EventArgs) Handles txtMontoPago.Leave
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / txtTipoCambio.Text, 2)
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * txtTipoCambio.Text, 2)
        End If
    End Sub
End Class