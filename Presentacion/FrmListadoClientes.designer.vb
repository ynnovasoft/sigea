﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmListadoClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.cmbTipoBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.btnExportarExcel = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.nCodCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipPer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipDocCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDirCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cVendedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCorreo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cContactos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipPer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipDocCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cApePat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cApeMat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNombres = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodUbiDep = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodUbiProv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodUbiDis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAgeRet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCli = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodVen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(205, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL  DE CLIENTES"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(174, 44)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(230, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbTipoBusqueda
        '
        Me.cmbTipoBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoBusqueda.FormattingEnabled = True
        Me.cmbTipoBusqueda.Location = New System.Drawing.Point(9, 44)
        Me.cmbTipoBusqueda.Name = "cmbTipoBusqueda"
        Me.cmbTipoBusqueda.Size = New System.Drawing.Size(163, 22)
        Me.cmbTipoBusqueda.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Buscar por:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(1094, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(240, 14)
        Me.Label5.TabIndex = 145
        Me.Label5.Text = "Doble Click / ENTER para editar un registro"
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodCli, Me.cNumDoc, Me.cRazSoc, Me.TipPer, Me.TipDocCli, Me.cDirCli, Me.cVendedor, Me.cCorreo, Me.cContactos, Me.nTipPer, Me.cTipDocCli, Me.cApePat, Me.cApeMat, Me.cNombres, Me.cCodUbiDep, Me.cCodUbiProv, Me.cCodUbiDis, Me.bAgeRet, Me.nTipCli, Me.nCodVen})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 68)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.RowHeadersWidth = 20
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1322, 494)
        Me.dtgListado.TabIndex = 1
        '
        'btnExportarExcel
        '
        Me.btnExportarExcel.FlatAppearance.BorderSize = 0
        Me.btnExportarExcel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnExportarExcel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnExportarExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportarExcel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExportarExcel.Image = Global.Presentacion.My.Resources.Resources.Excel_24
        Me.btnExportarExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExportarExcel.Location = New System.Drawing.Point(1203, 573)
        Me.btnExportarExcel.Name = "btnExportarExcel"
        Me.btnExportarExcel.Size = New System.Drawing.Size(128, 26)
        Me.btnExportarExcel.TabIndex = 5
        Me.btnExportarExcel.Text = "Exportar Excel"
        Me.btnExportarExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExportarExcel.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.FlatAppearance.BorderSize = 0
        Me.btnAnular.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnular.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnular.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnular.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnular.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnular.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnular.Location = New System.Drawing.Point(130, 573)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(106, 26)
        Me.btnAnular.TabIndex = 4
        Me.btnAnular.Text = "Eliminar(F4)"
        Me.btnAnular.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatAppearance.BorderSize = 0
        Me.btnNuevo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(9, 573)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(99, 26)
        Me.btnNuevo.TabIndex = 3
        Me.btnNuevo.Text = "Nuevo(F1)"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'nCodCli
        '
        Me.nCodCli.DataPropertyName = "nCodCli"
        Me.nCodCli.HeaderText = "nCodCli"
        Me.nCodCli.Name = "nCodCli"
        Me.nCodCli.ReadOnly = True
        Me.nCodCli.Visible = False
        '
        'cNumDoc
        '
        Me.cNumDoc.DataPropertyName = "cNumDoc"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumDoc.DefaultCellStyle = DataGridViewCellStyle2
        Me.cNumDoc.HeaderText = "N° Documento"
        Me.cNumDoc.Name = "cNumDoc"
        Me.cNumDoc.ReadOnly = True
        Me.cNumDoc.Width = 130
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        Me.cRazSoc.HeaderText = "Razon Social"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 318
        '
        'TipPer
        '
        Me.TipPer.DataPropertyName = "TipPer"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipPer.DefaultCellStyle = DataGridViewCellStyle3
        Me.TipPer.HeaderText = "Tip Persona"
        Me.TipPer.Name = "TipPer"
        Me.TipPer.ReadOnly = True
        Me.TipPer.Width = 150
        '
        'TipDocCli
        '
        Me.TipDocCli.DataPropertyName = "TipDocCli"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipDocCli.DefaultCellStyle = DataGridViewCellStyle4
        Me.TipDocCli.HeaderText = "Tip Documento"
        Me.TipDocCli.Name = "TipDocCli"
        Me.TipDocCli.ReadOnly = True
        Me.TipDocCli.Width = 150
        '
        'cDirCli
        '
        Me.cDirCli.DataPropertyName = "cDirCli"
        Me.cDirCli.HeaderText = "Dirección Fiscal"
        Me.cDirCli.Name = "cDirCli"
        Me.cDirCli.ReadOnly = True
        Me.cDirCli.Width = 350
        '
        'cVendedor
        '
        Me.cVendedor.DataPropertyName = "cVendedor"
        Me.cVendedor.HeaderText = "Vendedor"
        Me.cVendedor.Name = "cVendedor"
        Me.cVendedor.ReadOnly = True
        Me.cVendedor.Width = 200
        '
        'cCorreo
        '
        Me.cCorreo.DataPropertyName = "cCorreo"
        Me.cCorreo.HeaderText = "cCorreo"
        Me.cCorreo.Name = "cCorreo"
        Me.cCorreo.ReadOnly = True
        Me.cCorreo.Visible = False
        '
        'cContactos
        '
        Me.cContactos.DataPropertyName = "cContactos"
        Me.cContactos.HeaderText = "cContactos"
        Me.cContactos.Name = "cContactos"
        Me.cContactos.ReadOnly = True
        Me.cContactos.Visible = False
        '
        'nTipPer
        '
        Me.nTipPer.DataPropertyName = "nTipPer"
        Me.nTipPer.HeaderText = "nTipPer"
        Me.nTipPer.Name = "nTipPer"
        Me.nTipPer.ReadOnly = True
        Me.nTipPer.Visible = False
        '
        'cTipDocCli
        '
        Me.cTipDocCli.DataPropertyName = "cTipDocCli"
        Me.cTipDocCli.HeaderText = "cTipDocCli"
        Me.cTipDocCli.Name = "cTipDocCli"
        Me.cTipDocCli.ReadOnly = True
        Me.cTipDocCli.Visible = False
        '
        'cApePat
        '
        Me.cApePat.DataPropertyName = "cApePat"
        Me.cApePat.HeaderText = "cApePat"
        Me.cApePat.Name = "cApePat"
        Me.cApePat.ReadOnly = True
        Me.cApePat.Visible = False
        '
        'cApeMat
        '
        Me.cApeMat.DataPropertyName = "cApeMat"
        Me.cApeMat.HeaderText = "cApeMat"
        Me.cApeMat.Name = "cApeMat"
        Me.cApeMat.ReadOnly = True
        Me.cApeMat.Visible = False
        '
        'cNombres
        '
        Me.cNombres.DataPropertyName = "cNombres"
        Me.cNombres.HeaderText = "cNombres"
        Me.cNombres.Name = "cNombres"
        Me.cNombres.ReadOnly = True
        Me.cNombres.Visible = False
        '
        'cCodUbiDep
        '
        Me.cCodUbiDep.DataPropertyName = "cCodUbiDep"
        Me.cCodUbiDep.HeaderText = "nCodDep"
        Me.cCodUbiDep.Name = "cCodUbiDep"
        Me.cCodUbiDep.ReadOnly = True
        Me.cCodUbiDep.Visible = False
        '
        'cCodUbiProv
        '
        Me.cCodUbiProv.DataPropertyName = "cCodUbiProv"
        Me.cCodUbiProv.HeaderText = "nCodProv"
        Me.cCodUbiProv.Name = "cCodUbiProv"
        Me.cCodUbiProv.ReadOnly = True
        Me.cCodUbiProv.Visible = False
        '
        'cCodUbiDis
        '
        Me.cCodUbiDis.DataPropertyName = "cCodUbiDis"
        Me.cCodUbiDis.HeaderText = "nCodDis"
        Me.cCodUbiDis.Name = "cCodUbiDis"
        Me.cCodUbiDis.ReadOnly = True
        Me.cCodUbiDis.Visible = False
        '
        'bAgeRet
        '
        Me.bAgeRet.DataPropertyName = "bAgeRet"
        Me.bAgeRet.HeaderText = "bAgeRet"
        Me.bAgeRet.Name = "bAgeRet"
        Me.bAgeRet.ReadOnly = True
        Me.bAgeRet.Visible = False
        '
        'nTipCli
        '
        Me.nTipCli.DataPropertyName = "nTipCli"
        Me.nTipCli.HeaderText = "nTipCli"
        Me.nTipCli.Name = "nTipCli"
        Me.nTipCli.ReadOnly = True
        Me.nTipCli.Visible = False
        '
        'nCodVen
        '
        Me.nCodVen.DataPropertyName = "nCodVen"
        Me.nCodVen.HeaderText = "nCodVen"
        Me.nCodVen.Name = "nCodVen"
        Me.nCodVen.ReadOnly = True
        Me.nCodVen.Visible = False
        '
        'FrmListadoClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnExportarExcel)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbTipoBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmListadoClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoBusqueda As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As Button
    Friend WithEvents btnAnular As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents Label5 As Label
    Friend WithEvents dtgListado As DataGridView
    Friend WithEvents btnExportarExcel As Button
    Friend WithEvents nCodCli As DataGridViewTextBoxColumn
    Friend WithEvents cNumDoc As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents TipPer As DataGridViewTextBoxColumn
    Friend WithEvents TipDocCli As DataGridViewTextBoxColumn
    Friend WithEvents cDirCli As DataGridViewTextBoxColumn
    Friend WithEvents cVendedor As DataGridViewTextBoxColumn
    Friend WithEvents cCorreo As DataGridViewTextBoxColumn
    Friend WithEvents cContactos As DataGridViewTextBoxColumn
    Friend WithEvents nTipPer As DataGridViewTextBoxColumn
    Friend WithEvents cTipDocCli As DataGridViewTextBoxColumn
    Friend WithEvents cApePat As DataGridViewTextBoxColumn
    Friend WithEvents cApeMat As DataGridViewTextBoxColumn
    Friend WithEvents cNombres As DataGridViewTextBoxColumn
    Friend WithEvents cCodUbiDep As DataGridViewTextBoxColumn
    Friend WithEvents cCodUbiProv As DataGridViewTextBoxColumn
    Friend WithEvents cCodUbiDis As DataGridViewTextBoxColumn
    Friend WithEvents bAgeRet As DataGridViewTextBoxColumn
    Friend WithEvents nTipCli As DataGridViewTextBoxColumn
    Friend WithEvents nCodVen As DataGridViewTextBoxColumn
End Class
