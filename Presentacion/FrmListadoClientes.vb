﻿Imports Negocios
Imports Business
Imports Entities
Imports ClosedXML.Excel

Public Class FrmListadoClientes
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Private Sub FrmListadoClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call ListarClientes()
        If MDIPrincipal.GeneraExcelClientes = True Then
            btnExportarExcel.Enabled = True
        Else
            btnExportarExcel.Enabled = False
        End If
        txtBuscar.Select()
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("200")
        Call CargaCombo(CreaDatoCombos(dt, 200), cmbTipoBusqueda)
    End Sub
    Sub ListarClientes()
        Dim BLCliente As New BLCliente
        Dim dt As New DataTable
        dt = BLCliente.Listar(txtBuscar.Text, cmbTipoBusqueda.SelectedValue)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub cmbTipoBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Select()
        Call ListarClientes()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarClientes()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub
    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub dtgListado_SelectionChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub FrmListadoClientes_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevo.Enabled = True And btnNuevo.Visible = True Then
            Call btnNuevo_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnular.Enabled = True And btnAnular.Visible = True Then
            Call btnAnular_Click(sender, e)
        End If
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEClientes As New BECustomer
                BEClientes.nCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
                BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
                BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
                BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
                BEClientes.cCorreo = dtgListado.CurrentRow.Cells("cCorreo").Value
                BEClientes.cContactos = dtgListado.CurrentRow.Cells("cContactos").Value
                BEClientes.cApePat = dtgListado.CurrentRow.Cells("cApePat").Value
                BEClientes.cApeMat = dtgListado.CurrentRow.Cells("cApeMat").Value
                BEClientes.cNombres = dtgListado.CurrentRow.Cells("cNombres").Value
                BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

                BEClientes.nTipPer = dtgListado.CurrentRow.Cells("nTipPer").Value
                BEClientes.cTipDocCli = dtgListado.CurrentRow.Cells("cTipDocCli").Value
                BEClientes.cCodUbiDep = dtgListado.CurrentRow.Cells("cCodUbiDep").Value
                BEClientes.cCodUbiProv = dtgListado.CurrentRow.Cells("cCodUbiProv").Value
                BEClientes.cCodUbiDis = dtgListado.CurrentRow.Cells("cCodUbiDis").Value
                BEClientes.nTipCli = dtgListado.CurrentRow.Cells("nTipCli").Value
                BEClientes.nCodVen = dtgListado.CurrentRow.Cells("nCodVen").Value

                Dim Actualizar As Boolean = False
                FrmClientes.InicioCliente(BEClientes, 2, Actualizar)
                FrmClientes = Nothing
                If Actualizar = True Then
                    Call ListarClientes()
                End If
            End If
        End If
    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEClientes As New BECustomer

            BEClientes.nCodCli = dtgListado.CurrentRow.Cells("nCodCli").Value
            BEClientes.cNumDoc = dtgListado.CurrentRow.Cells("cNumDoc").Value
            BEClientes.cRazSoc = dtgListado.CurrentRow.Cells("cRazSoc").Value
            BEClientes.cDirCli = dtgListado.CurrentRow.Cells("cDirCli").Value
            BEClientes.cCorreo = dtgListado.CurrentRow.Cells("cCorreo").Value
            BEClientes.cContactos = dtgListado.CurrentRow.Cells("cContactos").Value
            BEClientes.cApePat = dtgListado.CurrentRow.Cells("cApePat").Value
            BEClientes.cApeMat = dtgListado.CurrentRow.Cells("cApeMat").Value
            BEClientes.cNombres = dtgListado.CurrentRow.Cells("cNombres").Value
            BEClientes.bAgeRet = dtgListado.CurrentRow.Cells("bAgeRet").Value

            BEClientes.nTipPer = dtgListado.CurrentRow.Cells("nTipPer").Value
            BEClientes.cTipDocCli = dtgListado.CurrentRow.Cells("cTipDocCli").Value
            BEClientes.cCodUbiDep = dtgListado.CurrentRow.Cells("cCodUbiDep").Value
            BEClientes.cCodUbiProv = dtgListado.CurrentRow.Cells("cCodUbiProv").Value
            BEClientes.cCodUbiDis = dtgListado.CurrentRow.Cells("cCodUbiDis").Value
            BEClientes.nTipCli = dtgListado.CurrentRow.Cells("nTipCli").Value
            BEClientes.nCodVen = dtgListado.CurrentRow.Cells("nCodVen").Value


            Dim Actualizar As Boolean = False
            FrmClientes.InicioCliente(BEClientes, 2, Actualizar)
            FrmClientes = Nothing
            If Actualizar = True Then
                Call ListarClientes()
            End If
        End If
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Dim Actualizar As Boolean = False
        FrmClientes.InicioCliente(Nothing, 1, Actualizar)
        FrmClientes = Nothing
        If Actualizar = True Then
            Call ListarClientes()
        End If
    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        If MsgBox("¿Esta seguro que desea eliminar el cliente?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            Dim BLCliente As New BLCliente
            If BLCliente.Eliminar(dtgListado.CurrentRow.Cells("nCodCli").Value, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("Se eliminó correctamente el cliente", MsgBoxStyle.Information, "MENSAJE")
                Call ListarClientes()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnExportarExcel_Click(sender As Object, e As EventArgs) Handles btnExportarExcel.Click

        Dim workbook = New XLWorkbook()
        Dim worksheet = workbook.Worksheets.Add("CLIENTES")

        worksheet.Cell(1, 1).Value = "N° DOCUMENTO"
        worksheet.Cell(1, 1).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
        worksheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

        worksheet.Cell(1, 2).Value = "RAZON SOCIAL"
        worksheet.Cell(1, 2).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
        worksheet.Cell(1, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

        worksheet.Cell(1, 3).Value = "TIPO PERSONA"
        worksheet.Cell(1, 3).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
        worksheet.Cell(1, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

        worksheet.Cell(1, 4).Value = "TIPO DOCUMENTO"
        worksheet.Cell(1, 4).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
        worksheet.Cell(1, 4).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

        worksheet.Cell(1, 5).Value = "DIRECCIÓN"
        worksheet.Cell(1, 5).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
        worksheet.Cell(1, 5).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

        worksheet.Cell(1, 6).Value = "VENDEDOR"
        worksheet.Cell(1, 6).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
        worksheet.Cell(1, 6).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

        worksheet.Cell(1, 7).Value = "CORREO"
        worksheet.Cell(1, 7).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
        worksheet.Cell(1, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

        worksheet.Cell(1, 8).Value = "CONTACTOS"
        worksheet.Cell(1, 8).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
        worksheet.Cell(1, 8).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

        Dim rango = worksheet.Range("A1:H1")
        rango.Style.Font.FontSize = 12
        rango.Style.Font.Bold = True
        rango.Style.Font.FontColor = XLColor.White
        rango.Style.Fill.BackgroundColor = XLColor.DodgerBlue

        Dim Fila As Integer = 1
        For i As Integer = 1 To dtgListado.Rows.Count - 1
            Fila = Fila + 1

            worksheet.Cell(Fila, 1).Style.NumberFormat.Format = "@"
            worksheet.Cell(Fila, 1).Value = dtgListado.Rows(i).Cells("cNumDoc").Value
            worksheet.Cell(Fila, 1).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            worksheet.Cell(Fila, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

            worksheet.Cell(Fila, 2).Value = dtgListado.Rows(i).Cells("cRazSoc").Value
            worksheet.Cell(Fila, 2).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            worksheet.Cell(Fila, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

            worksheet.Cell(Fila, 3).Value = dtgListado.Rows(i).Cells("TipPer").Value
            worksheet.Cell(Fila, 3).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            worksheet.Cell(Fila, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

            worksheet.Cell(Fila, 4).Value = dtgListado.Rows(i).Cells("TipDocCli").Value
            worksheet.Cell(Fila, 4).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            worksheet.Cell(Fila, 4).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

            worksheet.Cell(Fila, 5).Value = dtgListado.Rows(i).Cells("cDirCli").Value
            worksheet.Cell(Fila, 5).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            worksheet.Cell(Fila, 5).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

            worksheet.Cell(Fila, 6).Value = dtgListado.Rows(i).Cells("cVendedor").Value
            worksheet.Cell(Fila, 6).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            worksheet.Cell(Fila, 6).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

            worksheet.Cell(Fila, 7).Value = dtgListado.Rows(i).Cells("cCorreo").Value
            worksheet.Cell(Fila, 7).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            worksheet.Cell(Fila, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

            worksheet.Cell(Fila, 8).Style.NumberFormat.Format = "@"
            worksheet.Cell(Fila, 8).Value = dtgListado.Rows(i).Cells("cContactos").Value
            worksheet.Cell(Fila, 8).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            worksheet.Cell(Fila, 8).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left
        Next
        worksheet.Columns(1, 7).AdjustToContents()

        Dim Dialog As New SaveFileDialog
        Dialog.DefaultExt = "xlsx"
        Dialog.FileName = "LISTADO CARTERA DE CLIENTES"
        Dialog.Filter = "Archivos de Excel (*.xlsx)|*.xlsx"
        If Dialog.ShowDialog = DialogResult.OK Then
            workbook.SaveAs(Dialog.FileName)
        End If
    End Sub

    Private Sub btnAnular_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnular.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnExportarExcel_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnExportarExcel.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class