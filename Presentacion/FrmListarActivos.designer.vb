﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmListarActivos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodAct = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipoActivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCondicionActivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nConAct = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipAct = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nStock = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblCantidadProductos = New System.Windows.Forms.Label()
        Me.btnAnularProducto = New System.Windows.Forms.Button()
        Me.btnNuevoProducto = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.btnMovimiento = New System.Windows.Forms.Button()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1343, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(206, 22)
        Me.lblTitulo.Text = "LISTADO GENERAL  DE  ACTIVOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodAct, Me.cDescripcion, Me.cTipoActivo, Me.cCondicionActivo, Me.nConAct, Me.nTipAct, Me.nStock})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 53)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.RowHeadersWidth = 20
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(1324, 504)
        Me.dtgListado.TabIndex = 1
        '
        'nCodAct
        '
        Me.nCodAct.DataPropertyName = "nCodAct"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nCodAct.DefaultCellStyle = DataGridViewCellStyle14
        Me.nCodAct.HeaderText = "nCodAct"
        Me.nCodAct.Name = "nCodAct"
        Me.nCodAct.ReadOnly = True
        Me.nCodAct.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCodAct.Visible = False
        Me.nCodAct.Width = 90
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cDescripcion.DefaultCellStyle = DataGridViewCellStyle15
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.ReadOnly = True
        Me.cDescripcion.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cDescripcion.Width = 460
        '
        'cTipoActivo
        '
        Me.cTipoActivo.DataPropertyName = "cTipoActivo"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cTipoActivo.DefaultCellStyle = DataGridViewCellStyle16
        Me.cTipoActivo.HeaderText = "Tipo Activo"
        Me.cTipoActivo.Name = "cTipoActivo"
        Me.cTipoActivo.ReadOnly = True
        Me.cTipoActivo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cTipoActivo.Width = 180
        '
        'cCondicionActivo
        '
        Me.cCondicionActivo.DataPropertyName = "cCondicionActivo"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cCondicionActivo.DefaultCellStyle = DataGridViewCellStyle17
        Me.cCondicionActivo.HeaderText = "Condición Activo"
        Me.cCondicionActivo.Name = "cCondicionActivo"
        Me.cCondicionActivo.ReadOnly = True
        Me.cCondicionActivo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cCondicionActivo.Width = 150
        '
        'nConAct
        '
        Me.nConAct.DataPropertyName = "nConAct"
        Me.nConAct.HeaderText = "nConAct"
        Me.nConAct.Name = "nConAct"
        Me.nConAct.ReadOnly = True
        Me.nConAct.Visible = False
        '
        'nTipAct
        '
        Me.nTipAct.DataPropertyName = "nTipAct"
        Me.nTipAct.HeaderText = "nTipAct"
        Me.nTipAct.Name = "nTipAct"
        Me.nTipAct.ReadOnly = True
        Me.nTipAct.Visible = False
        '
        'nStock
        '
        Me.nStock.DataPropertyName = "nStock"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle18.Format = "N2"
        DataGridViewCellStyle18.NullValue = Nothing
        Me.nStock.DefaultCellStyle = DataGridViewCellStyle18
        Me.nStock.HeaderText = "Stock"
        Me.nStock.Name = "nStock"
        Me.nStock.ReadOnly = True
        Me.nStock.Width = 120
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(209, 32)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(255, 22)
        Me.txtBuscar.TabIndex = 0
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(184, 14)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Ingrese la descripción del activo:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1090, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(235, 14)
        Me.Label2.TabIndex = 136
        Me.Label2.Text = "Doble Click / ENTER para editar  un activo"
        '
        'lblCantidadProductos
        '
        Me.lblCantidadProductos.Location = New System.Drawing.Point(1019, 573)
        Me.lblCantidadProductos.Name = "lblCantidadProductos"
        Me.lblCantidadProductos.Size = New System.Drawing.Size(314, 19)
        Me.lblCantidadProductos.TabIndex = 138
        Me.lblCantidadProductos.Text = "Mensaje"
        Me.lblCantidadProductos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAnularProducto
        '
        Me.btnAnularProducto.FlatAppearance.BorderSize = 0
        Me.btnAnularProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnularProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnularProducto.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnularProducto.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnularProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnularProducto.Location = New System.Drawing.Point(137, 575)
        Me.btnAnularProducto.Name = "btnAnularProducto"
        Me.btnAnularProducto.Size = New System.Drawing.Size(106, 26)
        Me.btnAnularProducto.TabIndex = 3
        Me.btnAnularProducto.Text = "Eliminar(F4)"
        Me.btnAnularProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnularProducto.UseVisualStyleBackColor = True
        '
        'btnNuevoProducto
        '
        Me.btnNuevoProducto.FlatAppearance.BorderSize = 0
        Me.btnNuevoProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevoProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoProducto.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevoProducto.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevoProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoProducto.Location = New System.Drawing.Point(10, 575)
        Me.btnNuevoProducto.Name = "btnNuevoProducto"
        Me.btnNuevoProducto.Size = New System.Drawing.Size(100, 26)
        Me.btnNuevoProducto.TabIndex = 2
        Me.btnNuevoProducto.Text = "Nuevo(F1)"
        Me.btnNuevoProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoProducto.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'btnMovimiento
        '
        Me.btnMovimiento.FlatAppearance.BorderSize = 0
        Me.btnMovimiento.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnMovimiento.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnMovimiento.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMovimiento.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMovimiento.Image = Global.Presentacion.My.Resources.Resources.Title_20
        Me.btnMovimiento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMovimiento.Location = New System.Drawing.Point(280, 575)
        Me.btnMovimiento.Name = "btnMovimiento"
        Me.btnMovimiento.Size = New System.Drawing.Size(153, 26)
        Me.btnMovimiento.TabIndex = 4
        Me.btnMovimiento.Text = "Movimiento General"
        Me.btnMovimiento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMovimiento.UseVisualStyleBackColor = True
        '
        'FrmListarActivos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1343, 613)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnMovimiento)
        Me.Controls.Add(Me.btnAnularProducto)
        Me.Controls.Add(Me.lblCantidadProductos)
        Me.Controls.Add(Me.btnNuevoProducto)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmListarActivos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnNuevoProducto As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents lblCantidadProductos As Label
    Friend WithEvents btnAnularProducto As Button
    Friend WithEvents nCodAct As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents cTipoActivo As DataGridViewTextBoxColumn
    Friend WithEvents cCondicionActivo As DataGridViewTextBoxColumn
    Friend WithEvents nConAct As DataGridViewTextBoxColumn
    Friend WithEvents nTipAct As DataGridViewTextBoxColumn
    Friend WithEvents nStock As DataGridViewTextBoxColumn
    Friend WithEvents btnMovimiento As Button
End Class
