﻿Imports Business
Imports Entities
Imports System.Windows.Forms
Public Class FrmListarActivos
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Private Sub FrmListarActivos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call Listar()
        txtBuscar.Focus()
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call Listar()
    End Sub
    Sub Listar()
        Dim dt As DataTable
        Dim Obj As New BLActive
        dt = Obj.Listar(txtBuscar.Text, MDIPrincipal.CodigoAgencia,
                                                     MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
        lblCantidadProductos.Text = "Se encontrarón un total de " + FormatNumber(dtgListado.Rows.Count, 0) + " activos"
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                Dim BEActivos As New BEActive
                BEActivos.nCodAct = dtgListado.CurrentRow.Cells("nCodAct").Value
                BEActivos.cDescripcion = dtgListado.CurrentRow.Cells("cDescripcion").Value
                BEActivos.nTipAct = dtgListado.CurrentRow.Cells("nTipAct").Value
                BEActivos.nConAct = dtgListado.CurrentRow.Cells("nConAct").Value
                Dim Actualizar As Boolean = False
                FrmActivos.InicioActivos(BEActivos, 2, Actualizar)
                FrmActivos = Nothing
                If Actualizar = True Then
                    Call Listar()
                End If
            End If
        End If
    End Sub

    Private Sub btnNuevoProducto_Click(sender As Object, e As EventArgs) Handles btnNuevoProducto.Click
        If MDIPrincipal.CodigoBaseDatos <> 1 Then
            MsgBox("No se puede realizar ningun tipo de operaciones en una BASE DE DATOS tipificada como: BACKUP, Cierre sesiÓn y vulva a ingresar al sistema indicado la BASE DE DATOS ACTUAL", MsgBoxStyle.Critical, "Aviso")
            Exit Sub
        End If
        Dim Actualizar As Boolean = False
        FrmActivos.InicioActivos(Nothing, 1, Actualizar)
        FrmActivos = Nothing
        If Actualizar = True Then
            Call Listar()
        End If
    End Sub

    Private Sub btnAnularProducto_Click(sender As Object, e As EventArgs) Handles btnAnularProducto.Click
        Dim oDatos As New BEActive
        Dim cMensaje As String = ""
        If MsgBox("¿Esta seguro que desea eliminar el activo?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.nCodAct = dtgListado.CurrentRow.Cells("nCodAct").Value
            Dim Obj As New BLActive
            If Obj.Eliminar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("El activo se eliminó correctamente", MsgBoxStyle.Information, "MENSAJE")
                Call Listar()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            Dim BEActivos As New BEActive
            BEActivos.nCodAct = dtgListado.CurrentRow.Cells("nCodAct").Value
            BEActivos.cDescripcion = dtgListado.CurrentRow.Cells("cDescripcion").Value
            BEActivos.nTipAct = dtgListado.CurrentRow.Cells("nTipAct").Value
            BEActivos.nConAct = dtgListado.CurrentRow.Cells("nConAct").Value
            Dim Actualizar As Boolean = False
            FrmActivos.InicioActivos(BEActivos, 2, Actualizar)
            FrmActivos = Nothing
            If Actualizar = True Then
                Call Listar()
            End If
        End If
    End Sub
    Private Sub FrmListarActivos_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevoProducto.Enabled = True And btnNuevoProducto.Visible = True Then
            Call btnNuevoProducto_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnularProducto.Enabled = True And btnAnularProducto.Visible = True Then
            Call btnAnularProducto_Click(sender, e)
        End If
    End Sub

    Private Sub btnNuevoProducto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevoProducto.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnMovimiento_Click(sender As Object, e As EventArgs) Handles btnMovimiento.Click
        FrmConsultaMovimientosServicio.Inicio(dtgListado.CurrentRow.Cells("nCodAct").Value,
                                          dtgListado.CurrentRow.Cells("cDescripcion").Value)
        FrmConsultaMovimientosServicio = Nothing
    End Sub

    Private Sub btnAnularProducto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnularProducto.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class