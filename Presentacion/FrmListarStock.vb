﻿Imports Business

Public Class FrmListarStock
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim CodigoProducto As Integer = 0
    Private Sub FrmListarStock_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call Listar()
    End Sub
    Sub Inicio(ByVal pnCodProd As Integer)
        CodigoProducto = pnCodProd
        Me.ShowDialog()
    End Sub
    Sub Listar()
        Dim dt As DataTable
        Dim Obj As New BLProducts
        dt = Obj.ListarStockAlamcen(CodigoProducto, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 5, MousePosition.Y - Location.Y - eY - 5))
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub FrmListarStock_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class