﻿Imports Business
Public Class FrmLogin
    Private Sub FrmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaBaseDatos()
        Call CargaEmpresas()
        Call CargaAgencias()
        Call CargaPersonal()
        txtUser.Text = "Usuario"
        txtUser.ForeColor = Color.DarkGray
        txtUser.CharacterCasing = CharacterCasing.Normal
        txtPassword.Text = "Contraseña"
        txtPassword.ForeColor = Color.DarkGray
        txtUser.Select()
    End Sub
    Sub CargaBaseDatos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarBaseDatos()
        Call CargaCombo(dt, cmbBaseDatos)
    End Sub
    Sub CargaEmpresas()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarEmpresas(False)
        Call CargaCombo(dt, cmbEmpresa)
    End Sub
    Sub CargaAgencias()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarAgencias(False, cmbEmpresa.SelectedValue)
        Call CargaCombo(dt, cmbAgencias)
    End Sub
    Sub CargaPersonal()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarPersonal()
        Call CargaCombo(dt, cmbPersonal)
    End Sub
    Function ValidaDatos() As Boolean
        ValidaDatos = True
        If Len(Trim(txtUser.Text)) = 0 Then
            MsgBox("Debe ingresar el usuario", MsgBoxStyle.Exclamation, "MENSAJE")
            txtUser.Focus()
            txtUser.SelectAll()
            ValidaDatos = False
            Exit Function
        End If

        If Len(Trim(txtPassword.Text)) = 0 Then
            MsgBox("Debe ingresar la contraseña", MsgBoxStyle.Exclamation, "MENSAJE")
            txtPassword.Focus()
            txtPassword.SelectAll()
            ValidaDatos = False
            Exit Function
        End If
    End Function
    Function Ingresar() As Boolean
        Dim cMensaje As String = ""
        Dim Respuesta As Integer = 0
        Ingresar = True
        Dim Obj As New BLSecurity
        If Obj.ValidaIngresoSistema(txtUser.Text, txtPassword.Text, cmbAgencias.SelectedValue, cmbEmpresa.SelectedValue, Respuesta, cMensaje) = True Then
            Select Case Respuesta
                Case 0
                    MDIPrincipal.CodUsuario = txtUser.Text
                    MDIPrincipal.CodigoAgencia = cmbAgencias.SelectedValue
                    MDIPrincipal.CodigoEmpresa = cmbEmpresa.SelectedValue
                    MDIPrincipal.CodigoPersonal = cmbPersonal.SelectedValue
                    MDIPrincipal.MensajeCorte = cMensaje
                    MDIPrincipal.CodigoBaseDatos = cmbBaseDatos.SelectedValue
                    Ingresar = True
                Case 1
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    txtUser.Focus()
                    txtUser.SelectAll()
                    Ingresar = False
                    Exit Function
                Case 2
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    txtPassword.Focus()
                    txtPassword.SelectAll()
                    Ingresar = False
                    Exit Function
                Case 3
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    MDIPrincipal.CodUsuario = txtUser.Text
                    MDIPrincipal.CodigoAgencia = cmbAgencias.SelectedValue
                    MDIPrincipal.CodigoEmpresa = cmbEmpresa.SelectedValue
                    MDIPrincipal.CodigoPersonal = cmbPersonal.SelectedValue
                    MDIPrincipal.MensajeCorte = cMensaje
                    MDIPrincipal.CodigoBaseDatos = cmbBaseDatos.SelectedValue
                    Ingresar = True
                Case 4
                    MsgBox(cMensaje, MsgBoxStyle.Critical, "MENSAJE")
                    cmdOk.Select()
                    Ingresar = False
                    Exit Function
            End Select
        End If
    End Function

    Private Sub txtUser_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUser.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtPassword_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub cmdOk_Click(sender As System.Object, e As System.EventArgs) Handles cmdOk.Click
        If ValidaDatos() = False Then
            Exit Sub
        End If
        If Ingresar() = False Then
            Exit Sub
        End If
        MDIPrincipal.Show()
        Me.Dispose(False)
    End Sub
    Private Sub txtUser_TextChanged(sender As Object, e As EventArgs) Handles txtUser.TextChanged

    End Sub

    Private Sub txtPassword_TextChanged(sender As Object, e As EventArgs) Handles txtPassword.TextChanged

    End Sub

    Private Sub cmdOk_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmdOk.KeyPress

    End Sub

    Private Sub FrmLogin_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub cmbAgencias_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub
    Private Sub btnSalir_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub ptbCerrar_Click(sender As Object, e As EventArgs) Handles ptbCerrar.Click
        Me.Close()
    End Sub

    Private Sub cmbAgencias_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles cmbAgencias.SelectedIndexChanged

    End Sub

    Private Sub cmbAgencias_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbAgencias.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbEmpresa.SelectedIndexChanged

    End Sub

    Private Sub cmbEmpresa_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbEmpresa.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbEmpresa_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbEmpresa.SelectionChangeCommitted
        Call CargaAgencias()
    End Sub

    Private Sub txtUser_GotFocus(sender As Object, e As EventArgs) Handles txtUser.GotFocus
        If txtUser.Text = "Usuario" Then
            txtUser.Text = ""
            txtUser.ForeColor = Color.Black
        End If
    End Sub

    Private Sub txtUser_LostFocus(sender As Object, e As EventArgs) Handles txtUser.LostFocus
        txtUser.CharacterCasing = CharacterCasing.Normal
        If txtUser.Text = "" Then
            txtUser.CharacterCasing = CharacterCasing.Normal
            txtUser.Text = "Usuario"
            txtUser.ForeColor = Color.DarkGray
        Else
            txtUser.ForeColor = Color.Black
            txtUser.CharacterCasing = CharacterCasing.Upper
        End If
    End Sub

    Private Sub txtPassword_LostFocus(sender As Object, e As EventArgs) Handles txtPassword.LostFocus
        txtPassword.PasswordChar = ""
        If txtPassword.Text = "" Then
            txtPassword.Text = "Contraseña"
            txtPassword.ForeColor = Color.DarkGray
        Else
            txtPassword.PasswordChar = "*"
            txtPassword.ForeColor = Color.Black
        End If
    End Sub
    Private Sub txtPassword_GotFocus(sender As Object, e As EventArgs) Handles txtPassword.GotFocus
        If txtPassword.Text = "Contraseña" Then
            txtPassword.Text = ""
            txtPassword.PasswordChar = "*"
            txtPassword.ForeColor = Color.Black
        End If
    End Sub

    Private Sub cmbPersonal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbPersonal.SelectedIndexChanged

    End Sub

    Private Sub cmbPersonal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbPersonal.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbBaseDatos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBaseDatos.SelectedIndexChanged

    End Sub

    Private Sub cmbBaseDatos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbBaseDatos.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbEmpresa_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbEmpresa.SelectedValueChanged
        If (TypeOf cmbEmpresa.SelectedValue IsNot DataRowView) Then
            Call CargaAgencias()
        End If
    End Sub
End Class
