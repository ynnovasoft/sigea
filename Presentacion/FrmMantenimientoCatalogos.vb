﻿Imports Business
Imports System.Windows.Forms
Public Class FrmMantenimientoCatalogos
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Private Sub FrmMantenimientoCatalogos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCtalogo()
        Call ListarDatos()
        cmbCatalogo.Select()
    End Sub
    Sub CargaCtalogo()
        Dim dt As New DataTable
        Dim obj As New BLCommons
        dt = obj.MostrarMaestroMantenimiento()
        Call CargaCombo(dt, cmbCatalogo)
    End Sub
    Sub ListarDatos()
        Dim dt As New DataTable
        Dim obj As New BLCommons
        dt = obj.MostrarMaestroMantenimientoValores(cmbCatalogo.SelectedValue)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Call ListarDatos()
        txtValor.Clear()
        txtDescripcion.Clear()
        txtValor.Enabled = True
        txtDescripcion.Enabled = True
        chNuevo.Checked = True
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnHome.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub cmbCatalogo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCatalogo.SelectedIndexChanged

    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

        If dtgListado.Rows.Count > 0 Then
            Dim senderGrid = DirectCast(sender, DataGridView)
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                If dtgListado.Columns(e.ColumnIndex).Name = "Eliminar" Then
                    If MsgBox("¿Esta seguro que desea eliminar el Item?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                        Try
                            Dim obj As New BLCommons
                            cMensaje = ""
                            If obj.EliminarMaestro(cmbCatalogo.SelectedValue,
                                               dtgListado.CurrentRow.Cells("cValor").Value, cMensaje) = True Then
                                If cMensaje <> "" Then
                                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                                    Exit Sub
                                End If
                                Call ListarDatos()
                                txtValor.Clear()
                                txtDescripcion.Clear()
                                chNuevo.Checked = True
                                MsgBox("Se eliminó correctamente el Item", MsgBoxStyle.Information, "MENSAJE")
                            Else
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                            End If
                        Catch ex As Exception
                            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "MENSAJE")
                        End Try
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub cmbCatalogo_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbCatalogo.SelectionChangeCommitted
        Call ListarDatos()
        txtValor.Clear()
        txtDescripcion.Clear()
        chNuevo.Checked = True
    End Sub
    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            txtValor.Text = dtgListado.CurrentRow.Cells("cValor").Value
            txtValor.Tag = dtgListado.CurrentRow.Cells("cValor").Value
            txtDescripcion.Text = dtgListado.CurrentRow.Cells("cDescripcion").Value
            chNuevo.Checked = False
        End If
    End Sub
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Try
            Dim obj As New BLCommons
            cMensaje = ""
            If obj.GrabarMaestro(cmbCatalogo.SelectedValue, IIf(txtValor.Tag = Nothing, "", txtValor.Tag), txtValor.Text, txtDescripcion.Text,
                                 chNuevo.Checked, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Call ListarDatos()
                txtValor.Clear()
                txtDescripcion.Clear()
                txtValor.Enabled = True
                txtDescripcion.Enabled = True
                chNuevo.Checked = True
                MsgBox("Se grabó correctamente el Item", MsgBoxStyle.Information, "MENSAJE")
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "MENSAJE")
        End Try
    End Sub
    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                txtValor.Text = dtgListado.CurrentRow.Cells("cValor").Value
                txtValor.Tag = dtgListado.CurrentRow.Cells("cValor").Value
                txtDescripcion.Text = dtgListado.CurrentRow.Cells("cDescripcion").Value
                chNuevo.Checked = False
            End If
        End If
    End Sub
    Private Sub txtValor_TextChanged(sender As Object, e As EventArgs) Handles txtValor.TextChanged

    End Sub
    Private Sub cmbCatalogo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbCatalogo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtDescripcion_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcion.TextChanged

    End Sub
    Private Sub txtValor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtValor.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSoloLetras(e)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtDescripcion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub chNuevo_CheckedChanged(sender As Object, e As EventArgs) Handles chNuevo.CheckedChanged

    End Sub
    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub chNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chNuevo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub FrmMantenimientoCatalogos_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class