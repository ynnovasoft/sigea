﻿Imports Business
Imports Entities
Public Class FrmMantenimientoSeries
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Private Sub FrmMantenimientoSeries_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call Limpiar()
        Call CargaCombos()
        Call CargaAgencias()
        Call ListarSeries()
        Call InterfaceEntrada()
        btnNuevo.Focus()
    End Sub
    Sub ListarSeries()
        Dim dt = New DataTable()
        Dim BLSeries As New BLSeries
        dt = BLSeries.Listar(MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Sub Limpiar()
        bNuevo = True
        txtObservaciones.Text = ""
        txtSerie.Text = ""
        txtCorrelativo.Text = ""
        txtSerie.Tag = 0
    End Sub
    Sub InterfaceEntrada()
        txtObservaciones.Enabled = False
        txtSerie.Enabled = False
        txtCorrelativo.Enabled = False
        cmbTipComp.Enabled = False
        btnGrabar.Enabled = False
        btnNuevo.Enabled = True
        cmbAgencia.Enabled = False
        dtgListado.Enabled = True
    End Sub
    Sub InterfaceNuevo()
        txtObservaciones.Enabled = True
        txtSerie.Enabled = True
        txtCorrelativo.Enabled = True
        cmbTipComp.Enabled = True
        btnGrabar.Enabled = True
        btnNuevo.Enabled = False
        cmbAgencia.Enabled = True
        dtgListado.Enabled = False
    End Sub

    Sub CargaCombos()
        Dim dt = New DataTable()
        Dim BLCommons As New BLCommons
        dt = BLCommons.MostrarMaestroUnico(30, "01,02,03,05,06,07,08,09,50,60,70,80,90,99,100,110")
        Call CargaCombo(dt, cmbTipComp)
    End Sub
    Sub CargaAgencias()
        Dim dt = New DataTable()
        Dim BLCommons As New BLCommons
        dt = BLCommons.MostrarAgencias(True, MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbAgencia)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If bNuevo = False Then
            Call Modificar()
            Call ListarSeries()
        Else
            Call Insertar()
            Call ListarSeries()
        End If
    End Sub
    Sub Modificar()
        Dim oDatos As New BESerie
        Dim BLSeries As New BLSeries
        cMensaje = ""
        oDatos.nCodSer = txtSerie.Tag
        oDatos.cSerie = txtSerie.Text
        oDatos.cNumCor = txtCorrelativo.Text
        oDatos.cObservaciones = txtObservaciones.Text
        oDatos.cTipCom = cmbTipComp.SelectedValue
        oDatos.nCodigoAgencia = cmbAgencia.SelectedValue

        If BLSeries.Grabar(oDatos, False, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se modificó correctamente la serie", MsgBoxStyle.Information, "MENSAJE")
            Call Limpiar()
            Call InterfaceEntrada()
            btnNuevo.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub Insertar()
        Dim oDatos As New BESerie
        Dim BLSeries As New BLSeries
        cMensaje = ""
        oDatos.cSerie = txtSerie.Text
        oDatos.cNumCor = txtCorrelativo.Text
        oDatos.cObservaciones = txtObservaciones.Text
        oDatos.cTipCom = cmbTipComp.SelectedValue
        oDatos.nCodigoAgencia = cmbAgencia.SelectedValue
        oDatos.nCodEmp = MDIPrincipal.CodigoEmpresa

        If BLSeries.Grabar(oDatos, True, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se registró correctamente la serie", MsgBoxStyle.Information, "MENSAJE")
            Call Limpiar()
            Call InterfaceEntrada()
            btnNuevo.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Call ListarSeries()
        Call Limpiar()
        Call InterfaceEntrada()
        btnNuevo.Focus()
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnHome.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        Dim oDatos As New BESerie
        Dim BLSeries As New BLSeries
        cMensaje = ""
        If dtgListado.Rows.Count > 0 Then
            Dim senderGrid = DirectCast(sender, DataGridView)
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                If dtgListado.Columns(e.ColumnIndex).Name = "Eliminar" Then
                    If MsgBox("¿Esta seguro que desea eliminar la serie?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                        oDatos.nCodSer = dtgListado.CurrentRow.Cells("nCodSer").Value
                        If BLSeries.Eliminar(oDatos, cMensaje) = True Then
                            If cMensaje <> "" Then
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                                Exit Sub
                            End If
                            MsgBox("Se eliminó correctamente la serie", MsgBoxStyle.Information, "MENSAJE")
                            Call Limpiar()
                            Call InterfaceEntrada()
                            Call ListarSeries()
                            btnNuevo.Focus()
                        Else
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        End If
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub txtSerie_TextChanged(sender As Object, e As EventArgs) Handles txtSerie.TextChanged

    End Sub
    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            cmbTipComp.SelectedValue = dtgListado.CurrentRow.Cells("cTipCom").Value
            cmbAgencia.SelectedValue = dtgListado.CurrentRow.Cells("nCodigoAgencia").Value
            txtSerie.Text = dtgListado.CurrentRow.Cells("cSerie").Value
            txtSerie.Tag = dtgListado.CurrentRow.Cells("nCodSer").Value
            txtCorrelativo.Text = dtgListado.CurrentRow.Cells("cNumCor").Value
            txtObservaciones.Text = dtgListado.CurrentRow.Cells("cObservaciones").Value

            bNuevo = False
            Call InterfaceNuevo()
            cmbTipComp.Focus()

        End If
    End Sub

    Private Sub txtCorrelativo_TextChanged(sender As Object, e As EventArgs) Handles txtCorrelativo.TextChanged

    End Sub

    Private Sub txtSerie_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSerie.KeyPress
        Call ValidaSoloLetras(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Call Limpiar()
        Call InterfaceNuevo()
        bNuevo = True
        cmbTipComp.Focus()
    End Sub

    Private Sub cmbTipComp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipComp.SelectedIndexChanged

    End Sub

    Private Sub txtCorrelativo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCorrelativo.KeyPress
        Call ValidaNumeros(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbAgencia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAgencia.SelectedIndexChanged

    End Sub

    Private Sub cmbTipComp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipComp.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub cmbAgencia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbAgencia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                cmbTipComp.SelectedValue = dtgListado.CurrentRow.Cells("cTipCom").Value
                cmbAgencia.SelectedValue = dtgListado.CurrentRow.Cells("nCodigoAgencia").Value
                txtSerie.Text = dtgListado.CurrentRow.Cells("cSerie").Value
                txtSerie.Tag = dtgListado.CurrentRow.Cells("nCodSer").Value
                txtCorrelativo.Text = dtgListado.CurrentRow.Cells("cNumCor").Value
                txtObservaciones.Text = dtgListado.CurrentRow.Cells("cObservaciones").Value

                bNuevo = False
                Call InterfaceNuevo()
                cmbTipComp.Focus()

            End If
        End If
    End Sub

    Private Sub FrmMantenimientoSeries_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevo.Enabled = True And btnNuevo.Visible = True Then
            Call btnNuevo_Click(sender, e)
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        End If
    End Sub
End Class