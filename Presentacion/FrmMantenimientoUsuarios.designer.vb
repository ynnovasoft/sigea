﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMantenimientoUsuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.btnHome = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtApelPat = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtApelMat = New System.Windows.Forms.TextBox()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodUsuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cApePat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cApeMat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNombres = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bActivo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bVendedor = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bPersonal = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bUsuario = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bActPre = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bGenExcCli = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bAjuInv = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bPagSuc = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.cCodUsuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cPassword = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Eliminar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNombres = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCodUsu = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.chbActivo = New System.Windows.Forms.CheckBox()
        Me.chbVendedor = New System.Windows.Forms.CheckBox()
        Me.chbUsuario = New System.Windows.Forms.CheckBox()
        Me.chbPersonal = New System.Windows.Forms.CheckBox()
        Me.chbActualizaPrecio = New System.Windows.Forms.CheckBox()
        Me.chbGeneraExcelClientes = New System.Windows.Forms.CheckBox()
        Me.chbAjustaInventario = New System.Windows.Forms.CheckBox()
        Me.chbPagoTodaSucursal = New System.Windows.Forms.CheckBox()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(983, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(177, 22)
        Me.lblTitulo.Text = "MANTENIMIENTO USUARIOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'btnHome
        '
        Me.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnHome.FlatAppearance.BorderSize = 0
        Me.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHome.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHome.Image = Global.Presentacion.My.Resources.Resources.Home_20
        Me.btnHome.Location = New System.Drawing.Point(936, 444)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Size = New System.Drawing.Size(35, 26)
        Me.btnHome.TabIndex = 15
        Me.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnHome.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(124, 444)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(101, 26)
        Me.btnGrabar.TabIndex = 14
        Me.btnGrabar.Text = "Grabar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 388)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 14)
        Me.Label2.TabIndex = 151
        Me.Label2.Text = "Apellido Paterno:"
        '
        'txtApelPat
        '
        Me.txtApelPat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApelPat.Location = New System.Drawing.Point(109, 380)
        Me.txtApelPat.MaxLength = 100
        Me.txtApelPat.Name = "txtApelPat"
        Me.txtApelPat.Size = New System.Drawing.Size(159, 22)
        Me.txtApelPat.TabIndex = 0
        Me.txtApelPat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(300, 388)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 14)
        Me.Label4.TabIndex = 153
        Me.Label4.Text = "Apellido Materno:"
        '
        'txtApelMat
        '
        Me.txtApelMat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApelMat.Location = New System.Drawing.Point(404, 380)
        Me.txtApelMat.MaxLength = 100
        Me.txtApelMat.Name = "txtApelMat"
        Me.txtApelMat.Size = New System.Drawing.Size(159, 22)
        Me.txtApelMat.TabIndex = 1
        Me.txtApelMat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodUsuario, Me.cApePat, Me.cApeMat, Me.cNombres, Me.bActivo, Me.bVendedor, Me.bPersonal, Me.bUsuario, Me.bActPre, Me.bGenExcCli, Me.bAjuInv, Me.bPagSuc, Me.cCodUsuario, Me.cPassword, Me.Eliminar})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(9, 29)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(813, 341)
        Me.dtgListado.TabIndex = 16
        '
        'nCodUsuario
        '
        Me.nCodUsuario.DataPropertyName = "nCodUsuario"
        Me.nCodUsuario.HeaderText = "nCodUsuario"
        Me.nCodUsuario.Name = "nCodUsuario"
        Me.nCodUsuario.ReadOnly = True
        Me.nCodUsuario.Visible = False
        '
        'cApePat
        '
        Me.cApePat.DataPropertyName = "cApePat"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cApePat.DefaultCellStyle = DataGridViewCellStyle2
        Me.cApePat.HeaderText = "Apellido Paterno"
        Me.cApePat.Name = "cApePat"
        Me.cApePat.ReadOnly = True
        Me.cApePat.Width = 150
        '
        'cApeMat
        '
        Me.cApeMat.DataPropertyName = "cApeMat"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cApeMat.DefaultCellStyle = DataGridViewCellStyle3
        Me.cApeMat.HeaderText = "Apellido Materno"
        Me.cApeMat.Name = "cApeMat"
        Me.cApeMat.ReadOnly = True
        Me.cApeMat.Width = 150
        '
        'cNombres
        '
        Me.cNombres.DataPropertyName = "cNombres"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cNombres.DefaultCellStyle = DataGridViewCellStyle4
        Me.cNombres.HeaderText = "Nombres"
        Me.cNombres.Name = "cNombres"
        Me.cNombres.ReadOnly = True
        Me.cNombres.Width = 130
        '
        'bActivo
        '
        Me.bActivo.DataPropertyName = "bActivo"
        Me.bActivo.HeaderText = "Activo"
        Me.bActivo.Name = "bActivo"
        Me.bActivo.ReadOnly = True
        Me.bActivo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bActivo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.bActivo.Width = 50
        '
        'bVendedor
        '
        Me.bVendedor.DataPropertyName = "bVendedor"
        Me.bVendedor.HeaderText = "Vendedor"
        Me.bVendedor.Name = "bVendedor"
        Me.bVendedor.ReadOnly = True
        Me.bVendedor.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bVendedor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.bVendedor.Width = 70
        '
        'bPersonal
        '
        Me.bPersonal.DataPropertyName = "bPersonal"
        Me.bPersonal.HeaderText = "Personal"
        Me.bPersonal.Name = "bPersonal"
        Me.bPersonal.ReadOnly = True
        Me.bPersonal.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bPersonal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.bPersonal.Width = 70
        '
        'bUsuario
        '
        Me.bUsuario.DataPropertyName = "bUsuario"
        Me.bUsuario.HeaderText = "Usuario"
        Me.bUsuario.Name = "bUsuario"
        Me.bUsuario.ReadOnly = True
        Me.bUsuario.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bUsuario.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.bUsuario.Width = 60
        '
        'bActPre
        '
        Me.bActPre.DataPropertyName = "bActPre"
        Me.bActPre.HeaderText = "Act.Precio"
        Me.bActPre.Name = "bActPre"
        Me.bActPre.ReadOnly = True
        Me.bActPre.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bActPre.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.bActPre.Width = 70
        '
        'bGenExcCli
        '
        Me.bGenExcCli.DataPropertyName = "bGenExcCli"
        Me.bGenExcCli.HeaderText = "bGenExcCli"
        Me.bGenExcCli.Name = "bGenExcCli"
        Me.bGenExcCli.ReadOnly = True
        Me.bGenExcCli.Visible = False
        '
        'bAjuInv
        '
        Me.bAjuInv.DataPropertyName = "bAjuInv"
        Me.bAjuInv.HeaderText = "bAjuInv"
        Me.bAjuInv.Name = "bAjuInv"
        Me.bAjuInv.ReadOnly = True
        Me.bAjuInv.Visible = False
        '
        'bPagSuc
        '
        Me.bPagSuc.DataPropertyName = "bPagSuc"
        Me.bPagSuc.HeaderText = "bPagSuc"
        Me.bPagSuc.Name = "bPagSuc"
        Me.bPagSuc.ReadOnly = True
        Me.bPagSuc.Visible = False
        '
        'cCodUsuario
        '
        Me.cCodUsuario.DataPropertyName = "cCodUsuario"
        Me.cCodUsuario.HeaderText = "cCodUsuario"
        Me.cCodUsuario.Name = "cCodUsuario"
        Me.cCodUsuario.ReadOnly = True
        Me.cCodUsuario.Visible = False
        '
        'cPassword
        '
        Me.cPassword.DataPropertyName = "cPassword"
        Me.cPassword.HeaderText = "cPassword"
        Me.cPassword.Name = "cPassword"
        Me.cPassword.ReadOnly = True
        Me.cPassword.Visible = False
        '
        'Eliminar
        '
        Me.Eliminar.HeaderText = "Acción"
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.ReadOnly = True
        Me.Eliminar.Text = "Eliminar"
        Me.Eliminar.UseColumnTextForButtonValue = True
        Me.Eliminar.Width = 60
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatAppearance.BorderSize = 0
        Me.btnNuevo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(9, 444)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(100, 26)
        Me.btnNuevo.TabIndex = 13
        Me.btnNuevo.Text = "Nuevo(F1)"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(601, 388)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 14)
        Me.Label5.TabIndex = 159
        Me.Label5.Text = "Nombres:"
        '
        'txtNombres
        '
        Me.txtNombres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombres.Location = New System.Drawing.Point(663, 380)
        Me.txtNombres.MaxLength = 100
        Me.txtNombres.Name = "txtNombres"
        Me.txtNombres.Size = New System.Drawing.Size(159, 22)
        Me.txtNombres.TabIndex = 2
        Me.txtNombres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 413)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 14)
        Me.Label6.TabIndex = 161
        Me.Label6.Text = "Código Usuario:"
        '
        'txtCodUsu
        '
        Me.txtCodUsu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodUsu.Location = New System.Drawing.Point(109, 405)
        Me.txtCodUsu.MaxLength = 20
        Me.txtCodUsu.Name = "txtCodUsu"
        Me.txtCodUsu.Size = New System.Drawing.Size(159, 22)
        Me.txtCodUsu.TabIndex = 3
        Me.txtCodUsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(340, 413)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(62, 14)
        Me.Label7.TabIndex = 163
        Me.Label7.Text = "Password:"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(404, 405)
        Me.txtPassword.MaxLength = 20
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(159, 22)
        Me.txtPassword.TabIndex = 4
        Me.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chbActivo
        '
        Me.chbActivo.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chbActivo.Location = New System.Drawing.Point(828, 32)
        Me.chbActivo.Name = "chbActivo"
        Me.chbActivo.Size = New System.Drawing.Size(143, 18)
        Me.chbActivo.TabIndex = 5
        Me.chbActivo.Text = "Activo"
        Me.chbActivo.UseVisualStyleBackColor = True
        '
        'chbVendedor
        '
        Me.chbVendedor.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chbVendedor.Location = New System.Drawing.Point(828, 80)
        Me.chbVendedor.Name = "chbVendedor"
        Me.chbVendedor.Size = New System.Drawing.Size(143, 18)
        Me.chbVendedor.TabIndex = 7
        Me.chbVendedor.Text = "Vendedor"
        Me.chbVendedor.UseVisualStyleBackColor = True
        '
        'chbUsuario
        '
        Me.chbUsuario.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chbUsuario.Location = New System.Drawing.Point(828, 56)
        Me.chbUsuario.Name = "chbUsuario"
        Me.chbUsuario.Size = New System.Drawing.Size(143, 18)
        Me.chbUsuario.TabIndex = 6
        Me.chbUsuario.Text = "Usuario"
        Me.chbUsuario.UseVisualStyleBackColor = True
        '
        'chbPersonal
        '
        Me.chbPersonal.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chbPersonal.Location = New System.Drawing.Point(828, 104)
        Me.chbPersonal.Name = "chbPersonal"
        Me.chbPersonal.Size = New System.Drawing.Size(143, 18)
        Me.chbPersonal.TabIndex = 8
        Me.chbPersonal.Text = "Personal"
        Me.chbPersonal.UseVisualStyleBackColor = True
        '
        'chbActualizaPrecio
        '
        Me.chbActualizaPrecio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chbActualizaPrecio.Location = New System.Drawing.Point(828, 128)
        Me.chbActualizaPrecio.Name = "chbActualizaPrecio"
        Me.chbActualizaPrecio.Size = New System.Drawing.Size(143, 18)
        Me.chbActualizaPrecio.TabIndex = 9
        Me.chbActualizaPrecio.Text = "Actualiza Precio"
        Me.chbActualizaPrecio.UseVisualStyleBackColor = True
        '
        'chbGeneraExcelClientes
        '
        Me.chbGeneraExcelClientes.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chbGeneraExcelClientes.Location = New System.Drawing.Point(828, 152)
        Me.chbGeneraExcelClientes.Name = "chbGeneraExcelClientes"
        Me.chbGeneraExcelClientes.Size = New System.Drawing.Size(143, 18)
        Me.chbGeneraExcelClientes.TabIndex = 10
        Me.chbGeneraExcelClientes.Text = "Genera Excel Clientes"
        Me.chbGeneraExcelClientes.UseVisualStyleBackColor = True
        '
        'chbAjustaInventario
        '
        Me.chbAjustaInventario.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chbAjustaInventario.Location = New System.Drawing.Point(828, 176)
        Me.chbAjustaInventario.Name = "chbAjustaInventario"
        Me.chbAjustaInventario.Size = New System.Drawing.Size(143, 18)
        Me.chbAjustaInventario.TabIndex = 11
        Me.chbAjustaInventario.Text = "Ajusta Inventario"
        Me.chbAjustaInventario.UseVisualStyleBackColor = True
        '
        'chbPagoTodaSucursal
        '
        Me.chbPagoTodaSucursal.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chbPagoTodaSucursal.Location = New System.Drawing.Point(828, 202)
        Me.chbPagoTodaSucursal.Name = "chbPagoTodaSucursal"
        Me.chbPagoTodaSucursal.Size = New System.Drawing.Size(143, 18)
        Me.chbPagoTodaSucursal.TabIndex = 12
        Me.chbPagoTodaSucursal.Text = "Pagos Sucursales"
        Me.chbPagoTodaSucursal.UseVisualStyleBackColor = True
        '
        'FrmMantenimientoUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(983, 485)
        Me.ControlBox = False
        Me.Controls.Add(Me.chbPagoTodaSucursal)
        Me.Controls.Add(Me.chbAjustaInventario)
        Me.Controls.Add(Me.chbGeneraExcelClientes)
        Me.Controls.Add(Me.chbActualizaPrecio)
        Me.Controls.Add(Me.chbUsuario)
        Me.Controls.Add(Me.chbPersonal)
        Me.Controls.Add(Me.chbVendedor)
        Me.Controls.Add(Me.chbActivo)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtCodUsu)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtNombres)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtApelMat)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtApelPat)
        Me.Controls.Add(Me.btnHome)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmMantenimientoUsuarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnMover As Panel
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents btnHome As Button
    Friend WithEvents btnGrabar As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents txtApelPat As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtApelMat As TextBox
    Friend WithEvents dtgListado As DataGridView
    Friend WithEvents btnNuevo As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents txtNombres As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtCodUsu As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents chbActivo As CheckBox
    Friend WithEvents chbVendedor As CheckBox
    Friend WithEvents chbUsuario As CheckBox
    Friend WithEvents chbPersonal As CheckBox
    Friend WithEvents chbActualizaPrecio As CheckBox
    Friend WithEvents chbGeneraExcelClientes As CheckBox
    Friend WithEvents chbAjustaInventario As CheckBox
    Friend WithEvents chbPagoTodaSucursal As CheckBox
    Friend WithEvents nCodUsuario As DataGridViewTextBoxColumn
    Friend WithEvents cApePat As DataGridViewTextBoxColumn
    Friend WithEvents cApeMat As DataGridViewTextBoxColumn
    Friend WithEvents cNombres As DataGridViewTextBoxColumn
    Friend WithEvents bActivo As DataGridViewCheckBoxColumn
    Friend WithEvents bVendedor As DataGridViewCheckBoxColumn
    Friend WithEvents bPersonal As DataGridViewCheckBoxColumn
    Friend WithEvents bUsuario As DataGridViewCheckBoxColumn
    Friend WithEvents bActPre As DataGridViewCheckBoxColumn
    Friend WithEvents bGenExcCli As DataGridViewCheckBoxColumn
    Friend WithEvents bAjuInv As DataGridViewCheckBoxColumn
    Friend WithEvents bPagSuc As DataGridViewCheckBoxColumn
    Friend WithEvents cCodUsuario As DataGridViewTextBoxColumn
    Friend WithEvents cPassword As DataGridViewTextBoxColumn
    Friend WithEvents Eliminar As DataGridViewButtonColumn
End Class
