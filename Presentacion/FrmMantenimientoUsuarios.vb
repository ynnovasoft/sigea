﻿Imports Business
Imports Entities
Public Class FrmMantenimientoUsuarios
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Private Sub FrmMantenimientoUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call Limpiar()
        Call ListarUsuarios()
        Call InterfaceEntrada()
        dtgListado.Select()
    End Sub
    Sub ListarUsuarios()
        Dim dt = New DataTable
        Dim BLUser As New BLUser
        dt = BLUser.Listar()
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Sub Limpiar()
        bNuevo = True
        txtPassword.Text = ""
        txtApelPat.Text = ""
        txtApelMat.Text = ""
        txtNombres.Text = ""
        txtCodUsu.Text = ""
        txtNombres.Tag = 0
        chbActivo.Checked = False
        chbVendedor.Checked = False
        chbPersonal.Checked = False
        chbUsuario.Checked = False
        chbActualizaPrecio.Checked = False
        chbGeneraExcelClientes.Checked = False
        chbAjustaInventario.Checked = False
        chbPagoTodaSucursal.Checked = False
    End Sub
    Sub InterfaceEntrada()
        txtNombres.Enabled = False
        txtApelPat.Enabled = False
        txtApelMat.Enabled = False
        txtCodUsu.Enabled = False
        btnGrabar.Enabled = False
        btnNuevo.Enabled = True
        txtPassword.Enabled = False
        dtgListado.Enabled = True
        chbActivo.Enabled = False
        chbVendedor.Enabled = False
        chbPersonal.Enabled = False
        chbUsuario.Enabled = False
        chbActualizaPrecio.Enabled = False
        chbGeneraExcelClientes.Enabled = False
        chbAjustaInventario.Enabled = False
        chbPagoTodaSucursal.Enabled = False
    End Sub
    Sub InterfaceNuevo()
        txtNombres.Enabled = True
        txtApelPat.Enabled = True
        txtApelMat.Enabled = True
        txtCodUsu.Enabled = True
        btnGrabar.Enabled = True
        btnNuevo.Enabled = False
        txtPassword.Enabled = True
        dtgListado.Enabled = False
        chbActivo.Enabled = True
        chbVendedor.Enabled = True
        chbPersonal.Enabled = True
        chbUsuario.Enabled = True
        chbActualizaPrecio.Enabled = True
        chbGeneraExcelClientes.Enabled = True
        chbAjustaInventario.Enabled = True
        chbPagoTodaSucursal.Enabled = True
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If bNuevo = False Then
            Call Modificar()
        Else
            Call Insertar()
        End If
        Call ListarUsuarios()
    End Sub
    Sub Modificar()
        Dim oDatos As New BEUser
        Dim BLUser As New BLUser
        cMensaje = ""
        oDatos.nCodUsuario = txtNombres.Tag
        oDatos.cApePat = txtApelPat.Text
        oDatos.cApeMat = txtApelMat.Text
        oDatos.cNombres = txtNombres.Text
        oDatos.cCodUsuario = txtCodUsu.Text
        oDatos.cPassword = txtPassword.Text
        oDatos.bActivo = chbActivo.Checked
        oDatos.bVendedor = chbVendedor.Checked
        oDatos.bPersonal = chbPersonal.Checked
        oDatos.bUsuario = chbUsuario.Checked
        oDatos.bActPre = chbActualizaPrecio.Checked
        oDatos.bGenExcCli = chbGeneraExcelClientes.Checked
        oDatos.bAjuInv = chbAjustaInventario.Checked
        oDatos.bPagSuc = chbPagoTodaSucursal.Checked
        If BLUser.Grabar(oDatos, False, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se modificó correctamente el usuario", MsgBoxStyle.Information, "MENSAJE")
            Call Limpiar()
            Call InterfaceEntrada()
            btnNuevo.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub Insertar()
        Dim oDatos As New BEUser
        Dim BLUser As New BLUser
        cMensaje = ""

        oDatos.cApePat = txtApelPat.Text
        oDatos.cApeMat = txtApelMat.Text
        oDatos.cNombres = txtNombres.Text
        oDatos.cCodUsuario = txtCodUsu.Text
        oDatos.cPassword = txtPassword.Text
        oDatos.bActivo = chbActivo.Checked
        oDatos.bVendedor = chbVendedor.Checked
        oDatos.bPersonal = chbPersonal.Checked
        oDatos.bUsuario = chbUsuario.Checked
        oDatos.bActPre = chbActualizaPrecio.Checked
        oDatos.bGenExcCli = chbGeneraExcelClientes.Checked
        oDatos.bAjuInv = chbAjustaInventario.Checked
        oDatos.bPagSuc = chbPagoTodaSucursal.Checked
        If BLUser.Grabar(oDatos, True, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se registró correctamente el usuario", MsgBoxStyle.Information, "MENSAJE")
            Call Limpiar()
            Call InterfaceEntrada()
            btnNuevo.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Call ListarUsuarios()
        Call Limpiar()
        Call InterfaceEntrada()
        dtgListado.Select()
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnHome.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        Dim oDatos As New BEUser
        Dim BLUser As New BLUser
        cMensaje = ""
        If dtgListado.Rows.Count > 0 Then
            Dim senderGrid = DirectCast(sender, DataGridView)
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                If dtgListado.Columns(e.ColumnIndex).Name = "Eliminar" Then
                    If MsgBox("¿Esta seguro que desea eliminar el usuario?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                        oDatos.nCodUsuario = dtgListado.CurrentRow.Cells("nCodUsuario").Value
                        If BLUser.Eliminar(oDatos, cMensaje) = True Then
                            If cMensaje <> "" Then
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                                Exit Sub
                            End If
                            MsgBox("Se eliminó correctamente el usuario", MsgBoxStyle.Information, "MENSAJE")
                            Call Limpiar()
                            Call InterfaceEntrada()
                            Call ListarUsuarios()
                            dtgListado.Select()
                        Else
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        End If
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            txtNombres.Text = dtgListado.CurrentRow.Cells("cNombres").Value
            chbActivo.Checked = dtgListado.CurrentRow.Cells("bActivo").Value
            chbVendedor.Checked = dtgListado.CurrentRow.Cells("bVendedor").Value
            txtPassword.Text = dtgListado.CurrentRow.Cells("cPassword").Value
            txtNombres.Tag = dtgListado.CurrentRow.Cells("nCodUsuario").Value
            txtApelPat.Text = dtgListado.CurrentRow.Cells("cApePat").Value
            txtApelMat.Text = dtgListado.CurrentRow.Cells("cApeMat").Value
            txtCodUsu.Text = dtgListado.CurrentRow.Cells("cCodUsuario").Value
            chbPersonal.Checked = dtgListado.CurrentRow.Cells("bPersonal").Value
            chbUsuario.Checked = dtgListado.CurrentRow.Cells("bUsuario").Value
            chbActualizaPrecio.Checked = dtgListado.CurrentRow.Cells("bActPre").Value
            chbGeneraExcelClientes.Checked = dtgListado.CurrentRow.Cells("bGenExcCli").Value
            chbAjustaInventario.Checked = dtgListado.CurrentRow.Cells("bAjuInv").Value
            chbPagoTodaSucursal.Checked = dtgListado.CurrentRow.Cells("bPagSuc").Value
            bNuevo = False
            Call InterfaceNuevo()
        End If
    End Sub
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Call Limpiar()
        Call InterfaceNuevo()
        bNuevo = True
    End Sub
    Private Sub txtApelPat_TextChanged(sender As Object, e As EventArgs) Handles txtApelPat.TextChanged

    End Sub
    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                txtNombres.Text = dtgListado.CurrentRow.Cells("cNombres").Value
                chbActivo.Checked = dtgListado.CurrentRow.Cells("bActivo").Value
                chbVendedor.Checked = dtgListado.CurrentRow.Cells("bVendedor").Value
                txtPassword.Text = dtgListado.CurrentRow.Cells("cPassword").Value
                txtNombres.Tag = dtgListado.CurrentRow.Cells("nCodUsuario").Value
                txtApelPat.Text = dtgListado.CurrentRow.Cells("cApePat").Value
                txtApelMat.Text = dtgListado.CurrentRow.Cells("cApeMat").Value
                txtCodUsu.Text = dtgListado.CurrentRow.Cells("cCodUsuario").Value
                chbPersonal.Checked = dtgListado.CurrentRow.Cells("bPersonal").Value
                chbUsuario.Checked = dtgListado.CurrentRow.Cells("bUsuario").Value
                chbActualizaPrecio.Checked = dtgListado.CurrentRow.Cells("bActPre").Value
                chbGeneraExcelClientes.Checked = dtgListado.CurrentRow.Cells("bGenExcCli").Value
                chbAjustaInventario.Checked = dtgListado.CurrentRow.Cells("bAjuInv").Value
                chbPagoTodaSucursal.Checked = dtgListado.CurrentRow.Cells("bPagSuc").Value
                bNuevo = False
                Call InterfaceNuevo()
            End If
        End If
    End Sub
    Private Sub txtApelMat_TextChanged(sender As Object, e As EventArgs) Handles txtApelMat.TextChanged

    End Sub
    Private Sub txtApelPat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtApelPat.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtNombres_TextChanged(sender As Object, e As EventArgs) Handles txtNombres.TextChanged

    End Sub
    Private Sub txtApelMat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtApelMat.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtCodUsu_TextChanged(sender As Object, e As EventArgs) Handles txtCodUsu.TextChanged

    End Sub
    Private Sub txtNombres_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombres.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtPassword_TextChanged(sender As Object, e As EventArgs) Handles txtPassword.TextChanged

    End Sub
    Private Sub txtCodUsu_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodUsu.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub chbActivo_CheckedChanged(sender As Object, e As EventArgs) Handles chbActivo.CheckedChanged

    End Sub
    Private Sub txtPassword_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPassword.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub chbVendedor_CheckedChanged(sender As Object, e As EventArgs) Handles chbVendedor.CheckedChanged

    End Sub
    Private Sub chbActivo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbActivo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub chbVendedor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbVendedor.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub chbPersonal_CheckedChanged(sender As Object, e As EventArgs) Handles chbPersonal.CheckedChanged

    End Sub
    Private Sub btnNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub chbUsuario_CheckedChanged(sender As Object, e As EventArgs) Handles chbUsuario.CheckedChanged

    End Sub
    Private Sub chbPersonal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbPersonal.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub chbActualizaPrecio_CheckedChanged(sender As Object, e As EventArgs) Handles chbActualizaPrecio.CheckedChanged

    End Sub

    Private Sub chbUsuario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbUsuario.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbGeneraExcelClientes_CheckedChanged(sender As Object, e As EventArgs) Handles chbGeneraExcelClientes.CheckedChanged

    End Sub

    Private Sub chbActualizaPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbActualizaPrecio.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbGeneraExcelClientes_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbGeneraExcelClientes.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbAjustaInventario_CheckedChanged(sender As Object, e As EventArgs) Handles chbAjustaInventario.CheckedChanged

    End Sub

    Private Sub FrmMantenimientoUsuarios_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 And btnNuevo.Enabled = True And btnNuevo.Visible = True Then
            Call btnNuevo_Click(sender, e)
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        End If
    End Sub

    Private Sub chbPagoTodaSucursal_CheckedChanged(sender As Object, e As EventArgs) Handles chbPagoTodaSucursal.CheckedChanged

    End Sub

    Private Sub chbAjustaInventario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbAjustaInventario.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbPagoTodaSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbPagoTodaSucursal.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class