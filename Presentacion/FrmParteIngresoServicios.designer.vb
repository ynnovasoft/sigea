﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmParteIngresoServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.cmbMotTraslado = New System.Windows.Forms.ComboBox()
        Me.txtProvedor = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFechaRegistro = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbMoneda = New System.Windows.Forms.ComboBox()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNumParte = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodCot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nNumOrd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodCat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUndMed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValVent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPorDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtVentasGravadas = New System.Windows.Forms.Label()
        Me.txtIGV = New System.Windows.Forms.Label()
        Me.txtImporteTotal = New System.Windows.Forms.Label()
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtAlmacen = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtTipDocRef = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtNumDocRefComp = New System.Windows.Forms.Label()
        Me.btnBuscarProveedor = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.btnFacturar = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnBuscarDocRef = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNumDocRef = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.txtDescuento = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtSubTotal = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtEmpresa = New System.Windows.Forms.Label()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1033, 25)
        Me.tlsTitulo.TabIndex = 0
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(196, 22)
        Me.lblTitulo.Text = "PARTE DE INGRESO SERVICIOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'cmbMotTraslado
        '
        Me.cmbMotTraslado.BackColor = System.Drawing.Color.White
        Me.cmbMotTraslado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMotTraslado.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMotTraslado.FormattingEnabled = True
        Me.cmbMotTraslado.Location = New System.Drawing.Point(107, 59)
        Me.cmbMotTraslado.Name = "cmbMotTraslado"
        Me.cmbMotTraslado.Size = New System.Drawing.Size(474, 22)
        Me.cmbMotTraslado.TabIndex = 1
        '
        'txtProvedor
        '
        Me.txtProvedor.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtProvedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtProvedor.Location = New System.Drawing.Point(107, 84)
        Me.txtProvedor.Name = "txtProvedor"
        Me.txtProvedor.Size = New System.Drawing.Size(443, 22)
        Me.txtProvedor.TabIndex = 5
        Me.txtProvedor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 14)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Proveedor:"
        '
        'dtpFechaRegistro
        '
        Me.dtpFechaRegistro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaRegistro.Location = New System.Drawing.Point(475, 34)
        Me.dtpFechaRegistro.Name = "dtpFechaRegistro"
        Me.dtpFechaRegistro.Size = New System.Drawing.Size(106, 22)
        Me.dtpFechaRegistro.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(381, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 14)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Fecha Registro:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 67)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 14)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Tipo Operación:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 117)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 14)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Moneda:"
        '
        'cmbMoneda
        '
        Me.cmbMoneda.BackColor = System.Drawing.Color.White
        Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMoneda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMoneda.FormattingEnabled = True
        Me.cmbMoneda.Location = New System.Drawing.Point(107, 109)
        Me.cmbMoneda.Name = "cmbMoneda"
        Me.cmbMoneda.Size = New System.Drawing.Size(159, 22)
        Me.cmbMoneda.TabIndex = 3
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Location = New System.Drawing.Point(492, 109)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(89, 22)
        Me.txtTipoCambio.TabIndex = 4
        Me.txtTipoCambio.Text = "0.0000"
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(411, 117)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 14)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Tipo Cambio:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 42)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(93, 14)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "N° Documento:"
        '
        'txtNumParte
        '
        Me.txtNumParte.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumParte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumParte.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumParte.Location = New System.Drawing.Point(107, 34)
        Me.txtNumParte.Name = "txtNumParte"
        Me.txtNumParte.Size = New System.Drawing.Size(121, 22)
        Me.txtNumParte.TabIndex = 18
        Me.txtNumParte.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodCot, Me.nCodProd, Me.nNumOrd, Me.cCodCat, Me.cDescripcion, Me.cUndMed, Me.nCantidad, Me.nPrecio, Me.nValVent, Me.nPorDes})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(11, 214)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dtgListado.Size = New System.Drawing.Size(1011, 259)
        Me.dtgListado.TabIndex = 13
        '
        'nCodCot
        '
        Me.nCodCot.DataPropertyName = "nCodCot"
        Me.nCodCot.HeaderText = "nCodCot"
        Me.nCodCot.Name = "nCodCot"
        Me.nCodCot.ReadOnly = True
        Me.nCodCot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodCot.Visible = False
        '
        'nCodProd
        '
        Me.nCodProd.DataPropertyName = "nCodProd"
        Me.nCodProd.HeaderText = "nCodProd"
        Me.nCodProd.Name = "nCodProd"
        Me.nCodProd.ReadOnly = True
        Me.nCodProd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodProd.Visible = False
        '
        'nNumOrd
        '
        Me.nNumOrd.DataPropertyName = "nNumOrd"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nNumOrd.DefaultCellStyle = DataGridViewCellStyle10
        Me.nNumOrd.HeaderText = "N°"
        Me.nNumOrd.Name = "nNumOrd"
        Me.nNumOrd.ReadOnly = True
        Me.nNumOrd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nNumOrd.Width = 35
        '
        'cCodCat
        '
        Me.cCodCat.DataPropertyName = "cCodCat"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cCodCat.DefaultCellStyle = DataGridViewCellStyle11
        Me.cCodCat.HeaderText = "Cod. Cat"
        Me.cCodCat.Name = "cCodCat"
        Me.cCodCat.ReadOnly = True
        Me.cCodCat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cCodCat.Width = 110
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.ReadOnly = True
        Me.cDescripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cDescripcion.Width = 400
        '
        'cUndMed
        '
        Me.cUndMed.DataPropertyName = "cUndMed"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cUndMed.DefaultCellStyle = DataGridViewCellStyle12
        Me.cUndMed.HeaderText = "Und"
        Me.cUndMed.Name = "cUndMed"
        Me.cUndMed.ReadOnly = True
        Me.cUndMed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cUndMed.Width = 86
        '
        'nCantidad
        '
        Me.nCantidad.DataPropertyName = "nCantidad"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = "1.00"
        Me.nCantidad.DefaultCellStyle = DataGridViewCellStyle13
        Me.nCantidad.HeaderText = "Cant."
        Me.nCantidad.Name = "nCantidad"
        Me.nCantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCantidad.Width = 80
        '
        'nPrecio
        '
        Me.nPrecio.DataPropertyName = "nPrecio"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N6"
        DataGridViewCellStyle14.NullValue = "0.000000"
        Me.nPrecio.DefaultCellStyle = DataGridViewCellStyle14
        Me.nPrecio.HeaderText = "Costo Unit."
        Me.nPrecio.Name = "nPrecio"
        Me.nPrecio.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nPrecio.Width = 90
        '
        'nValVent
        '
        Me.nValVent.DataPropertyName = "nValVent"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.Format = "N2"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.nValVent.DefaultCellStyle = DataGridViewCellStyle15
        Me.nValVent.HeaderText = "Sub Total"
        Me.nValVent.Name = "nValVent"
        Me.nValVent.ReadOnly = True
        Me.nValVent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nValVent.Width = 120
        '
        'nPorDes
        '
        Me.nPorDes.DataPropertyName = "nPorDes"
        DataGridViewCellStyle16.Format = "N5"
        DataGridViewCellStyle16.NullValue = "0.00000"
        Me.nPorDes.DefaultCellStyle = DataGridViewCellStyle16
        Me.nPorDes.HeaderText = "Desc.(%)"
        Me.nPorDes.Name = "nPorDes"
        Me.nPorDes.ReadOnly = True
        Me.nPorDes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nPorDes.Width = 66
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(689, 488)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(31, 14)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "IGV:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(859, 488)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(39, 14)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Total:"
        '
        'txtVentasGravadas
        '
        Me.txtVentasGravadas.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtVentasGravadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVentasGravadas.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVentasGravadas.Location = New System.Drawing.Point(526, 480)
        Me.txtVentasGravadas.Name = "txtVentasGravadas"
        Me.txtVentasGravadas.Size = New System.Drawing.Size(120, 22)
        Me.txtVentasGravadas.TabIndex = 35
        Me.txtVentasGravadas.Text = "0.00"
        Me.txtVentasGravadas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIGV
        '
        Me.txtIGV.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGV.Location = New System.Drawing.Point(724, 480)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.Size = New System.Drawing.Size(82, 22)
        Me.txtIGV.TabIndex = 36
        Me.txtIGV.Text = "0.00"
        Me.txtIGV.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteTotal.Location = New System.Drawing.Point(902, 480)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(120, 22)
        Me.txtImporteTotal.TabIndex = 37
        Me.txtImporteTotal.Text = "0.00"
        Me.txtImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMensaje
        '
        Me.lblMensaje.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblMensaje.Location = New System.Drawing.Point(454, 188)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(567, 22)
        Me.lblMensaje.TabIndex = 123
        Me.lblMensaje.Text = "MENSAJE DE ESTADO"
        Me.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(723, 117)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(97, 14)
        Me.Label4.TabIndex = 141
        Me.Label4.Text = "Almacen Origen:"
        '
        'txtAlmacen
        '
        Me.txtAlmacen.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtAlmacen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAlmacen.Location = New System.Drawing.Point(826, 109)
        Me.txtAlmacen.Name = "txtAlmacen"
        Me.txtAlmacen.Size = New System.Drawing.Size(196, 22)
        Me.txtAlmacen.TabIndex = 140
        Me.txtAlmacen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(695, 67)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(125, 14)
        Me.Label10.TabIndex = 139
        Me.Label10.Text = "Tipo Documento Ref:"
        '
        'txtTipDocRef
        '
        Me.txtTipDocRef.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtTipDocRef.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTipDocRef.Location = New System.Drawing.Point(826, 59)
        Me.txtTipDocRef.Name = "txtTipDocRef"
        Me.txtTipDocRef.Size = New System.Drawing.Size(196, 22)
        Me.txtTipDocRef.TabIndex = 138
        Me.txtTipDocRef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(694, 92)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(126, 14)
        Me.Label11.TabIndex = 137
        Me.Label11.Text = "Número  Documento:"
        '
        'txtNumDocRefComp
        '
        Me.txtNumDocRefComp.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumDocRefComp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumDocRefComp.Location = New System.Drawing.Point(826, 84)
        Me.txtNumDocRefComp.Name = "txtNumDocRefComp"
        Me.txtNumDocRefComp.Size = New System.Drawing.Size(196, 22)
        Me.txtNumDocRefComp.TabIndex = 136
        Me.txtNumDocRefComp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnBuscarProveedor
        '
        Me.btnBuscarProveedor.BackgroundImage = Global.Presentacion.My.Resources.Resources.BuscarArchivo_20
        Me.btnBuscarProveedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBuscarProveedor.FlatAppearance.BorderSize = 0
        Me.btnBuscarProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarProveedor.Location = New System.Drawing.Point(555, 84)
        Me.btnBuscarProveedor.Name = "btnBuscarProveedor"
        Me.btnBuscarProveedor.Size = New System.Drawing.Size(24, 22)
        Me.btnBuscarProveedor.TabIndex = 2
        Me.btnBuscarProveedor.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 116
        '
        'btnFacturar
        '
        Me.btnFacturar.FlatAppearance.BorderSize = 0
        Me.btnFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFacturar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFacturar.Image = Global.Presentacion.My.Resources.Resources.Facturar_22
        Me.btnFacturar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFacturar.Location = New System.Drawing.Point(923, 524)
        Me.btnFacturar.Name = "btnFacturar"
        Me.btnFacturar.Size = New System.Drawing.Size(99, 26)
        Me.btnFacturar.TabIndex = 12
        Me.btnFacturar.Text = "Grabar(F3)"
        Me.btnFacturar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFacturar.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(11, 524)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(118, 26)
        Me.btnGrabar.TabIndex = 11
        Me.btnGrabar.Text = "Registrar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Image = Global.Presentacion.My.Resources.Resources.Delete_20
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(108, 188)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(79, 22)
        Me.btnEliminar.TabIndex = 9
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAgregar.FlatAppearance.BorderSize = 0
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Image = Global.Presentacion.My.Resources.Resources.Add_20
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(11, 188)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(90, 22)
        Me.btnAgregar.TabIndex = 8
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnBuscarDocRef
        '
        Me.btnBuscarDocRef.BackgroundImage = Global.Presentacion.My.Resources.Resources.BuscarArchivo_20
        Me.btnBuscarDocRef.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBuscarDocRef.FlatAppearance.BorderSize = 0
        Me.btnBuscarDocRef.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarDocRef.Location = New System.Drawing.Point(998, 34)
        Me.btnBuscarDocRef.Name = "btnBuscarDocRef"
        Me.btnBuscarDocRef.Size = New System.Drawing.Size(24, 22)
        Me.btnBuscarDocRef.TabIndex = 6
        Me.btnBuscarDocRef.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(732, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 14)
        Me.Label1.TabIndex = 144
        Me.Label1.Text = "Número Salida:"
        '
        'txtNumDocRef
        '
        Me.txtNumDocRef.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumDocRef.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumDocRef.Location = New System.Drawing.Point(826, 34)
        Me.txtNumDocRef.Name = "txtNumDocRef"
        Me.txtNumDocRef.Size = New System.Drawing.Size(167, 22)
        Me.txtNumDocRef.TabIndex = 143
        Me.txtNumDocRef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(8, 156)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(78, 14)
        Me.Label18.TabIndex = 147
        Me.Label18.Text = "Comentarios:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Location = New System.Drawing.Point(107, 134)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservaciones.Size = New System.Drawing.Size(474, 36)
        Me.txtObservaciones.TabIndex = 5
        '
        'txtDescuento
        '
        Me.txtDescuento.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescuento.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescuento.Location = New System.Drawing.Point(308, 480)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(82, 22)
        Me.txtDescuento.TabIndex = 155
        Me.txtDescuento.Text = "0.00"
        Me.txtDescuento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(229, 488)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(75, 14)
        Me.Label22.TabIndex = 154
        Me.Label22.Text = "Descuentos:"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotal.Location = New System.Drawing.Point(76, 480)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.Size = New System.Drawing.Size(120, 22)
        Me.txtSubTotal.TabIndex = 153
        Me.txtSubTotal.Text = "0.00"
        Me.txtSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(8, 488)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(64, 14)
        Me.Label20.TabIndex = 152
        Me.Label20.Text = "Sub Total:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(422, 488)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(101, 14)
        Me.Label12.TabIndex = 156
        Me.Label12.Text = "Ventas Gravadas:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(643, 142)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(97, 14)
        Me.Label8.TabIndex = 158
        Me.Label8.Text = "Empresa Origen:"
        '
        'txtEmpresa
        '
        Me.txtEmpresa.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmpresa.Location = New System.Drawing.Point(746, 134)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(276, 22)
        Me.txtEmpresa.TabIndex = 157
        Me.txtEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmParteIngresoServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1033, 571)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtEmpresa)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtDescuento)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.txtSubTotal)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.btnBuscarDocRef)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNumDocRef)
        Me.Controls.Add(Me.btnBuscarProveedor)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtAlmacen)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtTipDocRef)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtNumDocRefComp)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.btnFacturar)
        Me.Controls.Add(Me.txtImporteTotal)
        Me.Controls.Add(Me.txtIGV)
        Me.Controls.Add(Me.txtVentasGravadas)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNumParte)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmbMoneda)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpFechaRegistro)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtProvedor)
        Me.Controls.Add(Me.cmbMotTraslado)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmParteIngresoServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents cmbMotTraslado As System.Windows.Forms.ComboBox
    Friend WithEvents txtProvedor As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaRegistro As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents txtTipoCambio As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNumParte As System.Windows.Forms.Label
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents txtVentasGravadas As Label
    Friend WithEvents txtIGV As Label
    Friend WithEvents txtImporteTotal As Label
    Friend WithEvents btnFacturar As System.Windows.Forms.Button
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents lblMensaje As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtAlmacen As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents txtTipDocRef As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtNumDocRefComp As Label
    Friend WithEvents btnBuscarProveedor As Button
    Friend WithEvents btnBuscarDocRef As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNumDocRef As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents txtObservaciones As TextBox
    Friend WithEvents txtDescuento As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents txtSubTotal As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtEmpresa As Label
    Friend WithEvents nCodCot As DataGridViewTextBoxColumn
    Friend WithEvents nCodProd As DataGridViewTextBoxColumn
    Friend WithEvents nNumOrd As DataGridViewTextBoxColumn
    Friend WithEvents cCodCat As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents cUndMed As DataGridViewTextBoxColumn
    Friend WithEvents nCantidad As DataGridViewTextBoxColumn
    Friend WithEvents nPrecio As DataGridViewTextBoxColumn
    Friend WithEvents nValVent As DataGridViewTextBoxColumn
    Friend WithEvents nPorDes As DataGridViewTextBoxColumn
End Class
