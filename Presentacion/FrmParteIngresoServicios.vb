﻿Imports Negocios
Imports Business
Imports Entidades
Imports Entities
Imports System.Windows.Forms
Public Class FrmParteIngresoServicios
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim BEAlmacen As New BEStore
    Dim BEClientes As New BECustomer
    Dim cMensaje As String = ""
    Dim nCodClienteProveedor As Integer = 0
    Dim CantidadPorducto As Decimal = 0
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion
    Dim Accion As Boolean = False
    Dim EditarDetalle As Boolean = False
    Private Sub FrmParteIngresoServicios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargarMotivoTraslado()
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosAlmacen()
        End If
    End Sub
    Sub InicioAlmacen(ByVal oBEAlmacen As BEStore, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEAlmacen = oBEAlmacen
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosAlmacen()
        txtTipoCambio.Text = FormatNumber(BEAlmacen.nTipCam, 4)
        txtProvedor.Text = BEClientes.cRazSoc
        txtNumParte.Tag = BEAlmacen.nCodalm
        txtNumParte.Text = BEAlmacen.cNumAlm
        txtTipDocRef.Text = BEAlmacen.cTipDocRef
        dtpFechaRegistro.Value = BEAlmacen.dFecReg
        txtNumDocRefComp.Text = BEAlmacen.NumDocRef
        txtNumDocRef.Text = BEAlmacen.nCodigoDocRef
        txtSubTotal.Text = FormatNumber(BEAlmacen.nSubTot, 2)
        txtDescuento.Text = FormatNumber(BEAlmacen.nMonDes, 2)
        txtVentasGravadas.Text = FormatNumber(BEAlmacen.nVenGra, 2)
        txtIGV.Text = FormatNumber(BEAlmacen.nIGV, 2)
        txtImporteTotal.Text = FormatNumber(BEAlmacen.nImpTot, 2)
        txtObservaciones.Text = BEAlmacen.cObservaciones
        cmbMotTraslado.SelectedValue = BEAlmacen.cMotTrasl
        cmbMoneda.SelectedValue = BEAlmacen.nTipMon
        txtAlmacen.Text = BEAlmacen.AgenciaOrigen
        txtEmpresa.Text = BEAlmacen.EmpresaOrigen

        Call RecuperarDetalle()
        If BEAlmacen.nEstado = 1 Then
            lblMensaje.Text = "INGRESO REGISTRADO(A) - " + BEAlmacen.cMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call CalcularTotales()
            Call InterfaceGrabar()
            If cmbMotTraslado.SelectedValue = "04" Or cmbMotTraslado.SelectedValue = "08" Then
                btnBuscarDocRef.Enabled = True
            Else
                btnBuscarDocRef.Enabled = False
            End If
            Call EnfocarFocus(7, 0)
            btnAgregar.Select()
        ElseIf BEAlmacen.nEstado = 2 Then
            lblMensaje.Text = "INGRESO FACTURAD0(A) - " + BEAlmacen.cMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceFacturar()
            Call EnfocarFocus(7, 0)
            dtgListado.Select()
        ElseIf BEAlmacen.nEstado = 3 Then
            lblMensaje.Text = "INGRESO ANULADO(A) - " + BEAlmacen.cMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceRelacionar()
            Call EnfocarFocus(7, 0)
            dtgListado.Select()
        ElseIf BEAlmacen.nEstado = 4 Then
            lblMensaje.Text = "INGRESO RELACIONADO(A) - " + BEAlmacen.cMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceRelacionar()
            Call EnfocarFocus(7, 0)
            dtgListado.Select()
        End If
    End Sub
    Sub CargarMotivoTraslado()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(150, "02,03,05,11")
        Call CargaCombo(dt, cmbMotTraslado)
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
    End Sub
    Sub Limpiar()
        cmbMotTraslado.SelectedValue = "01"
        CantidadPorducto = 0
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        lblMensaje.Text = ""
        txtObservaciones.Clear()
        lblMensaje.ForeColor = Color.Blue
        nCodClienteProveedor = 0
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioCompra, 4)
        txtProvedor.Text = ""
        txtNumDocRef.Text = 0
        txtNumParte.Text = ""
        txtSubTotal.Text = "0.00"
        txtDescuento.Text = "0.00"
        txtVentasGravadas.Text = "0.00"
        txtIGV.Text = "0.00"
        txtImporteTotal.Text = "0.00"
        Call LimpiaGrilla(dtgListado)
        txtTipDocRef.Text = ""
        txtAlmacen.Text = ""
        txtEmpresa.Text = ""
        txtNumDocRefComp.Text = ""
        EditarDetalle = False
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)

    End Sub

    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub
    Sub InterfaceNuevo()
        txtObservaciones.Enabled = True
        btnBuscarDocRef.Enabled = False
        dtgListado.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        txtTipoCambio.Enabled = False
        cmbMotTraslado.Enabled = True
        cmbMoneda.Enabled = True
        btnBuscarProveedor.Enabled = True
        dtpFechaRegistro.Enabled = False
        btnBuscarDocRef.Enabled = False
    End Sub
    Function ValidaDatos() As Boolean
        ValidaDatos = True
        If nCodClienteProveedor = 0 Then
            MsgBox("Debe registrar el cliente y/o proveedor ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            Exit Function
        End If
        If IsNothing(cmbMotTraslado.SelectedValue) Then
            MsgBox("Debe registrar el motivo de traslado ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbMotTraslado.Select()
            Exit Function
        End If
        If cmbMotTraslado.SelectedValue = "02" Or cmbMotTraslado.SelectedValue = "03" Or cmbMotTraslado.SelectedValue = "07" Then
            MsgBox("No se puede registrar un ingreso de Tipo: " + cmbMotTraslado.Text + ", por esta opción", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbMotTraslado.Select()
            Exit Function
        End If
        If cmbMotTraslado.SelectedValue = "05" And MDIPrincipal.AjustaInventario = False Then
            MsgBox("No cuenta con permisos de sistema para realizar el tipo de operación: " + cmbMotTraslado.Text, MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbMotTraslado.Select()
            Exit Function
        End If
        If cmbMotTraslado.SelectedValue = "04" And txtNumDocRef.Text = 0 Then
            MsgBox("Debe relacionar el parte de salida correspondiente", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            btnBuscarDocRef.Select()
            Exit Function
        End If
        If cmbMotTraslado.SelectedValue = "08" And txtNumDocRef.Text = 0 Then
            MsgBox("Debe relacionar el parte de salida correspondiente", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            btnBuscarDocRef.Select()
            Exit Function
        End If
    End Function
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If ValidaDatos() = False Then
            Exit Sub
        End If

        Dim oDatos As New BEStore
        cMensaje = ""
        oDatos.nCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.nCodCliProv = nCodClienteProveedor
        oDatos.dFecReg = dtpFechaRegistro.Value
        oDatos.nTipMov = 2
        oDatos.cTipDocRef = IIf(txtNumDocRef.Text = "0", 0, "05")
        oDatos.nCodigoDocRef = txtNumDocRef.Text
        oDatos.nTipCam = txtTipoCambio.Text
        oDatos.nTipMon = cmbMoneda.SelectedValue
        oDatos.cObservaciones = txtObservaciones.Text
        oDatos.cUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.cMotTrasl = cmbMotTraslado.SelectedValue
        oDatos.nCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.cCodPer = MDIPrincipal.CodigoPersonal
        oDatos.bServicio = True
        Dim BLAlmacen As New BLAlmacen
        If BLAlmacen.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
            txtNumParte.Tag = oDatos.nCodalm
            txtNumParte.Text = oDatos.cNumAlm
            lblMensaje.Text = "INGRESO REGISTRADO"
            Call RecuperarDetalle()
            Call CalcularTotales()
            Call InterfaceGrabar()
            btnAgregar.Select()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        If cmbMotTraslado.SelectedValue = "04" Then
            dtgListado.Columns("nCantidad").ReadOnly = True
            dtgListado.Columns("nPrecio").ReadOnly = True
        Else
            dtgListado.Columns("nCantidad").ReadOnly = Valor
            dtgListado.Columns("nPrecio").ReadOnly = Valor
        End If
    End Sub
    Sub InterfaceGrabar()
        Call BloquearEdicionGrilla(False)
        If cmbMotTraslado.SelectedValue = "04" Then
            btnAgregar.Enabled = False
            btnEliminar.Enabled = False
        Else
            btnAgregar.Enabled = True
            btnEliminar.Enabled = True
        End If
        txtObservaciones.Enabled = False
        dtgListado.Enabled = True
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        txtTipoCambio.Enabled = False
        cmbMotTraslado.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        btnBuscarProveedor.Enabled = False
        btnBuscarDocRef.Enabled = False
    End Sub
    Sub InterfaceFacturar()
        Call BloquearEdicionGrilla(True)
        txtObservaciones.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtTipoCambio.Enabled = False
        cmbMotTraslado.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtgListado.Enabled = True
        btnBuscarProveedor.Enabled = False
        btnBuscarDocRef.Enabled = False
    End Sub
    Sub InterfaceRelacionar()
        Call BloquearEdicionGrilla(True)
        txtObservaciones.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtTipoCambio.Enabled = False
        cmbMotTraslado.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtgListado.Enabled = True
        btnBuscarProveedor.Enabled = False
        btnBuscarDocRef.Enabled = False
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        FrmConsultaActivo.Inicio(txtNumParte.Tag, "06")
        FrmConsultaActivo = Nothing

        Call CalcularTotales()
        Call EnfocarFocus(7, dtgListado.Rows.Count - 1)
        dtgListado.Select()
    End Sub
    Sub CalcularTotales()
        Dim ValorVenta, IGV As Double
        If dtgListado.Rows.Count > 0 Then
            ValorVenta = 0
            IGV = 0
            For i = 0 To dtgListado.Rows.Count - 1
                ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
            Next
            txtSubTotal.Text = FormatNumber(ValorVenta, 2)
            txtDescuento.Text = FormatNumber(0, 2)
            txtVentasGravadas.Text = FormatNumber(ValorVenta, 2)
            IGV = FormatNumber(ValorVenta * 0.18, 2)
            txtIGV.Text = FormatNumber(IGV, 2)
            txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
        Else
            txtSubTotal.Text = FormatNumber(0, 2)
            txtDescuento.Text = FormatNumber(0, 2)
            txtVentasGravadas.Text = FormatNumber(0, 2)
            txtIGV.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
        End If
    End Sub
    Function AgregarProductoDetalle(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double) As String
        cMensaje = ""
        Dim BLAlmacen As New BLAlmacen
        If BLAlmacen.AgregarProducto(txtNumParte.Tag, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
        AgregarProductoDetalle = cMensaje
    End Function
    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dtgListado.Rows.Count > 0 Then
            Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
            Call EliminarProductoDetalle(txtNumParte.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value)
            Call RecuperarDetalle()
            Call CalcularTotales()
            If Fila <> 0 Then
                If dtgListado.Rows.Count - 1 < Fila Then
                    Fila = Fila - 1
                End If
            End If
            Call EnfocarFocus(7, Fila)
            dtgListado.Select()
        End If
    End Sub
    Sub EliminarProductoDetalle(ByVal CodCot As Integer, ByVal nCodProd As Integer)
        cMensaje = ""
        Dim BLAlmacen As New BLAlmacen
        If BLAlmacen.EliminarProducto(CodCot, nCodProd, EditarDetalle, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Select()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Public Sub RecuperarDetalle()
        Dim dt As New DataTable
        Dim BLAlmacen As New BLAlmacen
        dt = BLAlmacen.RecuperarDetalle(txtNumParte.Tag)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar un ingreso  sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            Exit Function
        End If
        Return validaFacturacion
    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BEStore
        If validaFacturacion() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea grabar el parte de Ingreso?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.nCodalm = txtNumParte.Tag
            oDatos.nVenGra = txtVentasGravadas.Text
            oDatos.nIGV = txtIGV.Text
            oDatos.nImpTot = txtImporteTotal.Text
            oDatos.cUsuActAud = MDIPrincipal.CodUsuario
            oDatos.cCodPer = MDIPrincipal.CodigoPersonal
            Dim BLAlmacen As New BLAlmacen
            If BLAlmacen.Facturar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If IsDBNull(dtgListado.CurrentRow.Cells("nCantidad").Value) Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        If dtgListado.CurrentRow.Cells("nCantidad").Value = "0" Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        cMensaje = ""
        Dim BLActive As New BLActive
        If BLActive.ModificarActivoAlmacen(txtNumParte.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value,
                                                      dtgListado.CurrentRow.Cells("nCantidad").Value, CantidadPorducto,
                                                      dtgListado.CurrentRow.Cells("nPrecio").Value, cMensaje) Then
            If (cMensaje.Trim).Length > 0 Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Call LimpiaGrilla(dtgListado)
            Call RecuperarDetalle()
            Call CalcularTotales()
            Call EnfocarFocus(Columna, Fila)

        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub EnfocarFocus(ByVal Columna As Integer, ByVal Fila As Integer)
        If dtgListado.Rows.Count > 0 Then
            Me.dtgListado.CurrentCell = Me.dtgListado(Columna, Fila)
        End If
    End Sub
    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub cmbMotTraslado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMotTraslado.SelectedIndexChanged

    End Sub
    Private Sub cmbMotTraslado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMotTraslado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub lblMensaje_Click(sender As Object, e As EventArgs) Handles lblMensaje.Click

    End Sub

    Private Sub dtgListado_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dtgListado.CellBeginEdit
        CantidadPorducto = dtgListado.CurrentRow.Cells("nCantidad").Value
    End Sub

    Private Sub btnBuscarDocRef_Click(sender As Object, e As EventArgs) Handles btnBuscarDocRef.Click
        Dim BEAlmacen As New BEStore
        Dim BEClientes As New BECustomer
        If cmbMotTraslado.SelectedValue = "04" Then
            FrmConsultaAlmacenIngreso.InicioIngreso(BEAlmacen, BEClientes)
            FrmConsultaAlmacenIngreso = Nothing
            If BEAlmacen.nCodalm <> 0 Then
                txtNumDocRef.Text = BEAlmacen.nCodalm
                txtAlmacen.Text = BEAlmacen.AgenciaOrigen
                txtEmpresa.Text = BEAlmacen.EmpresaOrigen
            End If
        ElseIf cmbMotTraslado.SelectedValue = "08" Then
            FrmConsultaAlmacenIngreso.InicioIngresoEmpreea(BEAlmacen, BEClientes)
            FrmConsultaAlmacenIngreso = Nothing
            If BEAlmacen.nCodalm <> 0 Then
                txtNumDocRef.Text = BEAlmacen.nCodalm
                txtAlmacen.Text = BEAlmacen.AgenciaOrigen
                txtEmpresa.Text = BEAlmacen.EmpresaOrigen
            End If
        End If
    End Sub
    Private Sub cmbMotTraslado_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMotTraslado.SelectionChangeCommitted
        If cmbMotTraslado.SelectedValue = "04" Or cmbMotTraslado.SelectedValue = "08" Then
            btnBuscarDocRef.Enabled = True
        Else
            btnBuscarDocRef.Enabled = False
        End If
        txtAlmacen.Text = ""
        txtNumDocRef.Text = 0
    End Sub
    Private Sub btnBuscarProveedor_Click(sender As Object, e As EventArgs) Handles btnBuscarProveedor.Click
        Dim BEProveedor As New BEProveedor
        FrmConsultaProveedor.Inicio(BEProveedor)
        FrmConsultaProveedor = Nothing
        If BEProveedor.gnCodProv <> 0 Then
            nCodClienteProveedor = BEProveedor.gnCodProv
            txtProvedor.Text = BEProveedor.gcRazSoc
        Else
            btnBuscarProveedor.Focus()
        End If
    End Sub

    Private Sub btnBuscarDocRef_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarDocRef.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub btnBuscarProveedor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarProveedor.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
        If (e.KeyCode = Keys.Return) Then
            e.Handled = True
            SendKeys.Send(“{TAB}”)
        End If
    End Sub

    Private Sub FrmParteIngresoServicios_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            Call btnFacturar_Click(sender, e)
        End If
    End Sub
End Class