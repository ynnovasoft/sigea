﻿Imports Negocios
Imports Entidades
Imports Entities
Imports Business
Imports System.Windows.Forms
Public Class FrmParteSalidaServicios
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim BEAlmacen As New BEStore
    Dim BEClientes As New BECustomer
    Dim cMensaje As String = ""
    Dim nCodClienteProveedor As Integer = 0
    Dim CantidadPorducto As Decimal = 0
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion
    Dim Accion As Boolean = False
    Private Sub FrmParteSalidaServicios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargarMotivoTraslado()
        Call CargaAgencias()
        Call CargaEmpresas()
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosAlmacen()
        End If
    End Sub
    Sub InicioAlmacen(ByVal oBEAlmacen As BEStore, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEAlmacen = oBEAlmacen
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosAlmacen()
        txtTipoCambio.Text = FormatNumber(BEAlmacen.nTipCam, 4)
        txtCliente.Text = BEClientes.cRazSoc
        txtNumParte.Tag = BEAlmacen.nCodalm
        txtNumParte.Text = BEAlmacen.cNumAlm
        dtpFechaRegistro.Value = BEAlmacen.dFecReg
        txtTipDocRef.Text = BEAlmacen.cTipDocRef
        txtNumDocRefComp.Text = BEAlmacen.NumDocRef
        txtAlmacen.Text = BEAlmacen.AgenciaOrigen
        txtSubTotal.Text = FormatNumber(BEAlmacen.nSubTot, 2)
        txtDescuento.Text = FormatNumber(BEAlmacen.nMonDes, 2)
        txtVentasGravadas.Text = FormatNumber(BEAlmacen.nVenGra, 2)
        txtIGV.Text = FormatNumber(BEAlmacen.nIGV, 2)
        txtImporteTotal.Text = FormatNumber(BEAlmacen.nImpTot, 2)
        cmbMotTraslado.SelectedValue = BEAlmacen.cMotTrasl
        cmbAlmDestino.SelectedValue = BEAlmacen.nCodAgeDes
        cmbEmpresaDestino.SelectedValue = BEAlmacen.nCodEmpDes
        cmbMoneda.SelectedValue = BEAlmacen.nTipMon
        txtObservaciones.Text = BEAlmacen.cObservaciones
        Call RecuperarDetalle()

        If BEAlmacen.nEstado = 1 Then
            lblMensaje.Text = "SALIDA REGISTRADO(A) - " + BEAlmacen.cMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call CalcularTotales()
            Call InterfaceGrabar()
        ElseIf BEAlmacen.nEstado = 2 Then
            lblMensaje.Text = "SALIDA FACTURAD0(A) - " + BEAlmacen.cMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceFacturar()
        ElseIf BEAlmacen.nEstado = 3 Then
            lblMensaje.Text = "SALIDA ANULADO(A) - " + BEAlmacen.cMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceRelacionar()
        ElseIf BEAlmacen.nEstado = 4 Then
            lblMensaje.Text = "SALIDA RELACIONADO(A) - " + BEAlmacen.cMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceRelacionar()
        End If
        dtgListado.Select()
        Call EnfocarFocus(6, 0)
    End Sub
    Sub CargarMotivoTraslado()
        Dim BLComunes As New BLCommons
        Dim dt As New DataTable
        dt = BLComunes.MostrarMaestroUnico(150, "05,11,12")
        Call CargaCombo(dt, cmbMotTraslado)
    End Sub
    Sub CargaEmpresas()
        Dim BLComunes As New BLCommons
        Dim dt As New DataTable
        dt = BLComunes.MostrarEmpresas(False)
        Call CargaCombo(dt, cmbEmpresaDestino)
    End Sub
    Sub CargaAgencias()
        Dim BLComunes As New BLCommons
        Dim dt As New DataTable
        dt = BLComunes.MostrarAgencias(False, MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbAlmDestino)
    End Sub
    Sub CargaCombos()
        Dim BLComunes As New BLCommons
        Dim dt As New DataTable
        dt = BLComunes.MostrarMaestro("60")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
    End Sub
    Sub Limpiar()
        CantidadPorducto = 0
        txtObservaciones.Clear()
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        lblMensaje.Text = ""
        lblMensaje.ForeColor = Color.Blue
        nCodClienteProveedor = 0
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        txtCliente.Text = ""
        txtNumParte.Text = ""
        txtNumParte.Tag = 0
        txtVentasGravadas.Text = "0.00"
        txtIGV.Text = "0.00"
        txtSubTotal.Text = "0.00"
        txtDescuento.Text = "0.00"
        txtImporteTotal.Text = "0.00"
        Call LimpiaGrilla(dtgListado)
        txtTipDocRef.Text = ""
        txtAlmacen.Text = MDIPrincipal.Agencia
        cmbMotTraslado.SelectedValue = 0
        cmbAlmDestino.SelectedValue = 0
        cmbEmpresaDestino.SelectedValue = 0
        txtNumDocRefComp.Text = ""
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)

    End Sub

    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub
    Sub InterfaceNuevo()
        dtgListado.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        txtObservaciones.Enabled = True
        txtTipoCambio.Enabled = False
        cmbMotTraslado.Enabled = True
        cmbMoneda.Enabled = True
        cmbAlmDestino.Enabled = False
        cmbEmpresaDestino.Enabled = False
        btnBuscarCliente.Enabled = True
        dtpFechaRegistro.Enabled = False
    End Sub
    Function ValidaDatos() As Boolean
        ValidaDatos = True
        If nCodClienteProveedor = 0 Then
            MsgBox("Debe registrar el cliente y/o proveedor ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            Exit Function
        End If
        If IsNothing(cmbMotTraslado.SelectedValue) Then
            MsgBox("Debe registrar el motivo de traslado ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbMotTraslado.Select()
            Exit Function
        End If
        If cmbMotTraslado.SelectedValue = "01" Then
            MsgBox("No se puede registrar una salida de Tipo: " + cmbMotTraslado.Text + ", por esta opción", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbMotTraslado.Select()
            Exit Function
        End If
        If cmbMotTraslado.SelectedValue = "05" And MDIPrincipal.AjustaInventario = False Then
            MsgBox("No cuenta con permisos de sistema para realizar el tipo de operación: " + cmbMotTraslado.Text, MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbMotTraslado.Select()
            Exit Function
        End If
        If cmbMotTraslado.SelectedValue = "04" Then
            If IsNothing(cmbAlmDestino.SelectedValue) Then
                MsgBox("Debe registrar el almacen de destino", MsgBoxStyle.Exclamation, "MENSAJE")
                ValidaDatos = False
                cmbAlmDestino.Select()
                Exit Function
            End If
            If cmbAlmDestino.SelectedValue = MDIPrincipal.CodigoAgencia Then
                MsgBox("El almacen de destino debe ser distinto al alamcen actual", MsgBoxStyle.Exclamation, "MENSAJE")
                ValidaDatos = False
                cmbAlmDestino.Select()
                Exit Function
            End If
        End If
        If cmbMotTraslado.SelectedValue = "08" Then
            If IsNothing(cmbEmpresaDestino.SelectedValue) Then
                MsgBox("Debe registrar la empresa de destino", MsgBoxStyle.Exclamation, "MENSAJE")
                ValidaDatos = False
                cmbEmpresaDestino.Select()
                Exit Function
            End If
            If cmbEmpresaDestino.SelectedValue = MDIPrincipal.CodigoEmpresa Then
                MsgBox("La empresa de destino debe ser distinto al almacen actual", MsgBoxStyle.Exclamation, "MENSAJE")
                ValidaDatos = False
                cmbEmpresaDestino.Select()
                Exit Function
            End If
        End If
    End Function
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If ValidaDatos() = False Then
            Exit Sub
        End If
        Dim oDatos As New BEStore
        Try
            cMensaje = ""

            oDatos.nCodigoAgencia = MDIPrincipal.CodigoAgencia
            oDatos.nCodAgeDes = cmbAlmDestino.SelectedValue
            oDatos.nCodCliProv = nCodClienteProveedor
            oDatos.dFecReg = dtpFechaRegistro.Value
            oDatos.nTipMov = 1
            oDatos.nTipCam = txtTipoCambio.Text
            oDatos.nTipMon = cmbMoneda.SelectedValue
            oDatos.cTipDocRef = 0
            oDatos.nCodigoDocRef = 0
            oDatos.cUsuRegAud = MDIPrincipal.CodUsuario
            oDatos.cObservaciones = txtObservaciones.Text
            oDatos.cMotTrasl = cmbMotTraslado.SelectedValue
            oDatos.nCodEmp = MDIPrincipal.CodigoEmpresa
            oDatos.nCodEmpDes = cmbEmpresaDestino.SelectedValue
            oDatos.cCodPer = MDIPrincipal.CodigoPersonal
            oDatos.bServicio = True
            Dim BLAlmacen As New BLAlmacen
            If BLAlmacen.Registrar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                txtNumParte.Tag = oDatos.nCodalm
                txtNumParte.Text = oDatos.cNumAlm
                lblMensaje.Text = "SALIDA REGISTRADA"
                Call RecuperarDetalle()
                Call InterfaceGrabar()
                btnAgregar.Focus()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "MENSAJE")
        End Try

    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        dtgListado.Columns("nCantidad").ReadOnly = Valor
    End Sub
    Sub InterfaceGrabar()
        Call BloquearEdicionGrilla(False)
        txtObservaciones.Enabled = False
        dtgListado.Enabled = True
        btnAgregar.Enabled = True
        btnEliminar.Enabled = True
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        txtTipoCambio.Enabled = False
        cmbMotTraslado.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        cmbAlmDestino.Enabled = False
        cmbEmpresaDestino.Enabled = False
        btnBuscarCliente.Enabled = False
    End Sub
    Sub InterfaceFacturar()
        Call BloquearEdicionGrilla(True)
        txtObservaciones.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtTipoCambio.Enabled = False
        cmbMotTraslado.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtgListado.Enabled = True
        cmbAlmDestino.Enabled = False
        cmbEmpresaDestino.Enabled = False
        btnBuscarCliente.Enabled = False
    End Sub
    Sub InterfaceRelacionar()
        Call BloquearEdicionGrilla(True)
        txtObservaciones.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtTipoCambio.Enabled = False
        cmbMotTraslado.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtgListado.Enabled = True
        cmbAlmDestino.Enabled = False
        cmbEmpresaDestino.Enabled = False
        btnBuscarCliente.Enabled = False
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        FrmConsultaActivo.Inicio(txtNumParte.Tag, "05")
        FrmConsultaActivo = Nothing

        Call CalcularTotales()
        dtgListado.Focus()
        Call EnfocarFocus(6, dtgListado.Rows.Count - 1)
    End Sub
    Sub CalcularTotales()
        Dim ValorVenta, IGV As Double
        If dtgListado.Rows.Count > 0 Then
            ValorVenta = 0
            IGV = 0
            For i = 0 To dtgListado.Rows.Count - 1
                ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
            Next
            txtSubTotal.Text = FormatNumber(ValorVenta, 2)
            txtDescuento.Text = FormatNumber(0, 2)
            txtVentasGravadas.Text = FormatNumber(ValorVenta, 2)
            IGV = FormatNumber(ValorVenta * 0.18, 2)
            txtIGV.Text = FormatNumber(IGV, 2)
            txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
        Else
            txtSubTotal.Text = FormatNumber(0, 2)
            txtDescuento.Text = FormatNumber(0, 2)
            txtVentasGravadas.Text = FormatNumber(0, 2)
            txtIGV.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
        End If
    End Sub
    Sub AgregarProductoDetalle(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim BLAlmacen As New BLAlmacen
        If BLAlmacen.AgregarProducto(txtNumParte.Tag, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If Fila <> 0 Then
            Fila = Fila - 1
        End If
        Call EliminarProductoDetalle(txtNumParte.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value)
        Call RecuperarDetalle()
        Call EnfocarFocus(Columna, Fila)
        Call CalcularTotales()
    End Sub
    Sub EliminarProductoDetalle(ByVal CodCot As Integer, ByVal nCodProd As Integer)
        cMensaje = ""
        Dim BLAlmacen As New BLAlmacen
        If BLAlmacen.EliminarProducto(CodCot, nCodProd, False, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Public Sub RecuperarDetalle()
        Dim dt As New DataTable
        Dim BLAlmacen As New BLAlmacen
        dt = BLAlmacen.RecuperarDetalle(txtNumParte.Tag)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar una salida  sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            Exit Function
        End If
        Return validaFacturacion
    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BEStore
        If validaFacturacion() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea grabar el parte de salida?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.nCodalm = txtNumParte.Tag
            oDatos.nVenGra = txtVentasGravadas.Text
            oDatos.nIGV = txtIGV.Text
            oDatos.nImpTot = txtImporteTotal.Text
            oDatos.cUsuActAud = MDIPrincipal.CodUsuario
            oDatos.cCodPer = MDIPrincipal.CodigoPersonal
            Dim BLAlmacen As New BLAlmacen
            If BLAlmacen.Facturar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If IsDBNull(dtgListado.CurrentRow.Cells("nCantidad").Value) Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        If dtgListado.CurrentRow.Cells("nCantidad").Value = "0" Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If

        cMensaje = ""
        Dim BLActive As New BLActive
        If BLActive.ModificarActivoAlmacen(txtNumParte.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value,
                                                      dtgListado.CurrentRow.Cells("nCantidad").Value, CantidadPorducto,
                                                      dtgListado.CurrentRow.Cells("nPrecio").Value, cMensaje) Then
            If (cMensaje.Trim).Length > 0 Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Call LimpiaGrilla(dtgListado)
            Call RecuperarDetalle()
            Call EnfocarFocus(Columna, Fila)
            Call CalcularTotales()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub EnfocarFocus(ByVal Columna As Integer, ByVal Fila As Integer)
        If dtgListado.Rows.Count > 0 Then
            Me.dtgListado.CurrentCell = Me.dtgListado(Columna, Fila)
        End If
    End Sub
    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub
    Private Sub cmbMotTraslado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMotTraslado.SelectedIndexChanged

    End Sub

    Private Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim BEClientes As New BECustomer
        FrmConsultaCliente.Inicio(BEClientes)
        FrmConsultaCliente = Nothing
        If BEClientes.nCodCli <> 0 Then
            nCodClienteProveedor = BEClientes.nCodCli
            txtCliente.Text = BEClientes.cRazSoc
            cmbAlmDestino.Focus()
        Else
            btnBuscarCliente.Focus()
        End If
    End Sub

    Private Sub cmbMotTraslado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMotTraslado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbAlmDestino_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAlmDestino.SelectedIndexChanged

    End Sub

    Private Sub btnBuscarCliente_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarCliente.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub lblMensaje_Click(sender As Object, e As EventArgs) Handles lblMensaje.Click

    End Sub

    Private Sub cmbAlmDestino_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbAlmDestino.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub dtgListado_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dtgListado.CellBeginEdit
        CantidadPorducto = dtgListado.CurrentRow.Cells("nCantidad").Value
    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMotTraslado_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMotTraslado.SelectionChangeCommitted
        If cmbMotTraslado.SelectedValue = "04" Then
            cmbAlmDestino.Enabled = True
            cmbEmpresaDestino.Enabled = False
        ElseIf cmbMotTraslado.SelectedValue = "08" Then
            cmbAlmDestino.Enabled = False
            cmbAlmDestino.SelectedValue = 0
            cmbEmpresaDestino.Enabled = True
        Else
            cmbAlmDestino.Enabled = False
            cmbEmpresaDestino.Enabled = False
            cmbAlmDestino.SelectedValue = 0
            cmbEmpresaDestino.SelectedValue = 0
        End If

    End Sub

    Private Sub cmbAlmDestino_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbAlmDestino.SelectionChangeCommitted

    End Sub

    Private Sub cmbEmpresaDestino_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbEmpresaDestino.SelectedIndexChanged

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub cmbEmpresaDestino_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbEmpresaDestino.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub FrmParteSalidaServicios_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            Call btnFacturar_Click(sender, e)
        End If
    End Sub
End Class