﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmPermisos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.cmbCargos = New System.Windows.Forms.ComboBox()
        Me.tvwPermisos = New System.Windows.Forms.TreeView()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.tlsTitulo.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbCargos
        '
        Me.cmbCargos.BackColor = System.Drawing.Color.White
        Me.cmbCargos.CausesValidation = False
        Me.cmbCargos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCargos.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.cmbCargos.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbCargos.FormattingEnabled = True
        Me.cmbCargos.Location = New System.Drawing.Point(138, 32)
        Me.cmbCargos.Margin = New System.Windows.Forms.Padding(2)
        Me.cmbCargos.Name = "cmbCargos"
        Me.cmbCargos.Size = New System.Drawing.Size(276, 24)
        Me.cmbCargos.TabIndex = 22
        '
        'tvwPermisos
        '
        Me.tvwPermisos.BackColor = System.Drawing.Color.White
        Me.tvwPermisos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tvwPermisos.CheckBoxes = True
        Me.tvwPermisos.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tvwPermisos.ForeColor = System.Drawing.Color.DodgerBlue
        Me.tvwPermisos.Location = New System.Drawing.Point(10, 58)
        Me.tvwPermisos.Margin = New System.Windows.Forms.Padding(2)
        Me.tvwPermisos.Name = "tvwPermisos"
        Me.tvwPermisos.Size = New System.Drawing.Size(548, 395)
        Me.tvwPermisos.TabIndex = 1
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(567, 25)
        Me.tlsTitulo.TabIndex = 23
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(196, 22)
        Me.lblTitulo.Text = "CONFIGURACION DE PERMISOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 117
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(115, 14)
        Me.Label6.TabIndex = 118
        Me.Label6.Text = "Seleccionar Usuario:"
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(229, 462)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(101, 26)
        Me.btnGrabar.TabIndex = 119
        Me.btnGrabar.Text = "Grabar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'FrmPermisos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(567, 497)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Controls.Add(Me.cmbCargos)
        Me.Controls.Add(Me.tvwPermisos)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmPermisos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Protected WithEvents cmbCargos As System.Windows.Forms.ComboBox
    Friend WithEvents tvwPermisos As TreeView
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
End Class
