﻿Imports Business
Public Class FrmPermisos
    Dim bCargando As Boolean
    Dim i As Integer = -1
    Dim cMensaje As String = ""
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False

    Private Sub FrmPermisos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call ListarMenus()
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLCommons As New BLCommons
        dt = BLCommons.RecuperUsuariosAcceso()
        Call CargaCombo(dt, cmbCargos)
    End Sub
    Sub ListarMenus()
        Dim BLSecurity As New BLSecurity
        Dim Dato As New DataTable
        Dato = BLSecurity.RecuperaMenu(cmbCargos.SelectedValue, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
            Dim sCadCargos As String
            Dim i As Integer
            Dim M As ToolStripMenuItem
            Dim nodePadre As TreeNode
            If bCargando Then
                Exit Sub
            End If
            bCargando = True
            tvwPermisos.Nodes.Clear()

            If cmbCargos.SelectedIndex < 0 Then
                Exit Sub
            End If

            sCadCargos = "''" & Me.cmbCargos.Text & "''"
            For i = 0 To MDIPrincipal.MenuStrip.Items.Count - 1
                If MDIPrincipal.MenuStrip.Items(i).ToString <> "System.Windows.Forms.ToolStripSeparator" Then
                    M = MDIPrincipal.MenuStrip.Items(i)
                    nodePadre = tvwPermisos.Nodes.Add(M.Name.ToString & "  -  " & M.Text)
                    If BuscarMenu(M.Name.ToString, Dato) > 0 Then
                        nodePadre.Checked = True
                    Else
                        nodePadre.Checked = False
                    End If

                    If M.DropDownItems.Count > 0 Then
                        ChildNodeAdd(M, nodePadre, Dato)

                    End If
                End If
            Next
            Dato = Nothing
            bCargando = False
    End Sub
    Private Sub RecorrerTreeView(ByRef Nodos As TreeNodeCollection, ByRef Dato As DataTable)
        Dim lsMenu As String = ""
        Dim lsNomMenu As String = ""
        Dim lsTextMenu As String = ""
        For Each Nodo As TreeNode In Nodos
            If Nodo.Nodes.Count = 0 Then
                If Nodo.Checked = True Then
                    i = i + 1
                    lsMenu = Nodo.Text
                    lsMenu = Replace(lsMenu, "  ", " ")
                    lsNomMenu = Trim(Mid(Nodo.Text, 1, InStr(Nodo.Text, "-") - 1))
                    lsTextMenu = Replace(lsMenu, lsNomMenu + " - ", "")
                    Call AsignarDato(Dato, (i), "cNomMenu", lsNomMenu)
                    Call AsignarDato(Dato, (i), "cDescripcion", lsTextMenu)
                    Call AsignarDato(Dato, (i), "nCodUsuario", cmbCargos.SelectedValue)

                    lsMenu = Replace(lsMenu, "  ", " ")
                    lsNomMenu = Trim(Mid(lsMenu, 1, InStr(lsMenu, "-") - 1))
                End If
            Else
                If Nodo.Checked = True Then
                    i = i + 1
                    lsMenu = Nodo.Text
                    lsMenu = Replace(lsMenu, "  ", " ")
                    lsNomMenu = Trim(Mid(Nodo.Text, 1, InStr(Nodo.Text, "-") - 1))
                    lsTextMenu = Replace(lsMenu, lsNomMenu + " - ", "")
                    Call AsignarDato(Dato, (i), "cNomMenu", lsNomMenu)
                    Call AsignarDato(Dato, (i), "cDescripcion", lsTextMenu)
                    Call AsignarDato(Dato, (i), "nCodUsuario", cmbCargos.SelectedValue)
                End If
                RecorrerTreeView(Nodo.Nodes, Dato)
            End If
        Next
    End Sub
    Sub ChildNodeAdd(ByVal TSMenuItem As ToolStripMenuItem, ByVal nodeOld As TreeNode, ByVal D As DataTable)
        Dim nodeNew As TreeNode
        Dim O As ToolStripMenuItem
        For j As Integer = 0 To TSMenuItem.DropDownItems.Count - 1
            If TSMenuItem.DropDownItems(j).ToString <> "System.Windows.Forms.ToolStripSeparator" Then
                O = TSMenuItem.DropDownItems(j)
                nodeNew = nodeOld.Nodes.Add(O.Name.ToString & "  -  " & O.Text)

                If BuscarMenu(O.Name.ToString, D) > 0 Then
                    nodeNew.Checked = True
                Else
                    nodeNew.Checked = False
                End If
                If O.DropDownItems.Count > 0 Then
                    ChildNodeAdd(O, nodeNew, D)
                End If
            End If
        Next

    End Sub

    Private Sub cboCargos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCargos.SelectedIndexChanged

    End Sub

    Private Sub cboCargos_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbCargos.SelectionChangeCommitted
        Call ListarMenus()
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Dim Dato As New DataTable
        Call CrearTabla(Dato, "Permisos")
            Call CrearAsignarColumna(Dato, "cNomMenu", GetType(String))
            Call CrearAsignarColumna(Dato, "cDescripcion", GetType(String))
            Call CrearAsignarColumna(Dato, "nCodUsuario", GetType(Integer))
            i = -1
            Call RecorrerTreeView(tvwPermisos.Nodes, Dato)
            cMensaje = ""
            Dim BLSecurity As New BLSecurity
            If BLSecurity.RegistraPermisos(Dato, cmbCargos.SelectedValue, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa, cMensaje) Then
                If Len(cMensaje.Trim) > 0 Then
                    MsgBox(cMensaje, MessageBoxIcon.Exclamation, "Mensaje")

                Else
                    MsgBox("Se regisrarón correctamente los permisos", MessageBoxIcon.Information, "Mensaje")
                    Call MDIPrincipal.MDIPrincipal_Load(sender, e)
                    Call ListarMenus()
                End If
            Else
                MsgBox("No se pudo registrar los permisos intente nuevamente, o verifique si existen permisos a registrar", MessageBoxIcon.Exclamation, "Mensaje")
            End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub FrmPermisos_LocationChanged(sender As Object, e As EventArgs) Handles Me.LocationChanged

    End Sub

    Private Sub tvwPermisos_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tvwPermisos.AfterSelect

    End Sub

    Private Sub tlsTitulo_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tlsTitulo.ItemClicked

    End Sub

    Private Sub FrmPermisos_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        End If
    End Sub
End Class