﻿Imports Negocios
Imports Business
Imports Entidades
Public Class FrmProductos
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Dim BEProductos As New BEProductos
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion
    Dim Accion As Boolean = False
    Private Sub FrmProductos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargaFamilia()
        'call CargaSubFamilia
        Call CargaEmpresas()

        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
            txtCodCat.Select()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call InterfaceNuevo()
            Call MostrarDatosProductos()
            txtCodCat.Select()
        End If
    End Sub
    Sub InicioProductos(ByVal oBEProductos As BEProductos, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEProductos = oBEProductos
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosProductos()
        bNuevo = False
        cmbEmpresa.Enabled = False
        txtSerie.Text = BEProductos.gcSerie
        txtCodCat.Text = BEProductos.gcCodCat
        txtCodCat.Tag = BEProductos.gnCodProd
        txtDescripcion.Text = BEProductos.gcDescripcion
        cmbMarca.SelectedValue = BEProductos.gcMarca
        cmbUndMed.SelectedValue = BEProductos.gcUndMed
        chbModifica.Checked = BEProductos.gbModDes
        txtMarca.Text = BEProductos.gcMarca
        cmbOrigenCompra.SelectedValue = BEProductos.gcCodOriCom
        cmbFamilia.SelectedValue = BEProductos.gcCodFam
        cmbEmpresa.SelectedValue = BEProductos.gnCodEmp
    End Sub
    Sub Limpiar()
        bNuevo = True
        txtSerie.Clear()
        txtCodCat.Text = ""
        txtCodCat.Tag = 0
        txtDescripcion.Clear()
        chbModifica.Checked = False
    End Sub
    Sub InterfaceNuevo()
        bNuevo = True
        txtCodCat.Enabled = True
        txtDescripcion.Enabled = True
        chbModifica.Enabled = True
        txtSerie.Enabled = True
        cmbUndMed.Enabled = True
        cmbMarca.Enabled = True
        cmbOrigenCompra.Enabled = True
        btnGrabar.Enabled = True
        cmbEmpresa.Enabled = True
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("120,130,430")
        Call CargaCombo(CreaDatoCombos(dt, 120), cmbUndMed)
        Call CargaCombo(CreaDatoCombos(dt, 130), cmbMarca)
        Call CargaCombo(CreaDatoCombos(dt, 430), cmbOrigenCompra)
        txtMarca.Text = cmbMarca.SelectedValue
    End Sub
    Sub CargaFamilia()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarFamilia(1)
        Call CargaCombo(dt, cmbFamilia)
        cmbFamilia.SelectedValue = "000"
    End Sub
    'Sub CargaSubFamilia()
    '    Dim dt As DataTable
    '    Dim BLComunes As New BLCommons
    '    dt = BLComunes.MostrarSubFamilia(cmbFamilia.SelectedValue)
    '    Call CargaCombo(dt, cmbSubFamilia)
    '    cmbSubFamilia.SelectedValue = "000"
    'End Sub
    Sub CargaEmpresas()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarEmpresas(True)
        Call CargaCombo(dt, cmbEmpresa)
    End Sub
    Private Sub txtCodCat_TextChanged(sender As Object, e As EventArgs) Handles txtCodCat.TextChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtCodCat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodCat.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtDescripcion_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcion.TextChanged

    End Sub

    Private Sub cmbUndMed_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbUndMed.SelectedIndexChanged

    End Sub

    Private Sub txtDescripcion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbUndMed_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbUndMed.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If bNuevo = False Then
            Call ModificarProducto()
        Else
            Call InsertarProducto()
        End If
    End Sub
    Sub ModificarProducto()
        Dim oDatos As New BEProductos
        cMensaje = ""
        oDatos.gnCodProd = txtCodCat.Tag
        oDatos.gcCodCat = txtCodCat.Text
        oDatos.gcDescripcion = txtDescripcion.Text
        oDatos.gcMarca = cmbMarca.SelectedValue
        oDatos.gcSerie = txtSerie.Text
        oDatos.gcUndMed = cmbUndMed.SelectedValue
        oDatos.gbModDes = chbModifica.Checked
        oDatos.gcCodOriCom = IIf(cmbOrigenCompra.SelectedValue Is Nothing, "", cmbOrigenCompra.SelectedValue)
        oDatos.gcCodFam = cmbFamilia.SelectedValue
        oDatos.gnCodEmp = cmbEmpresa.SelectedValue
        'oDatos.gnCodSubFam = cmbFamilia.SelectedValue
        Dim BLProductos As New BLProducts
        If BLProductos.GrabarProducto(oDatos, False, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("El producto se actualizó correctamente", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub InsertarProducto()
        Dim oDatos As New BEProductos
        cMensaje = ""
        oDatos.gcCodCat = txtCodCat.Text
        oDatos.gcDescripcion = txtDescripcion.Text
        oDatos.gcMarca = cmbMarca.SelectedValue
        oDatos.gcSerie = txtSerie.Text
        oDatos.gcUndMed = cmbUndMed.SelectedValue
        oDatos.gbModDes = chbModifica.Checked
        oDatos.gcCodOriCom = cmbOrigenCompra.SelectedValue
        oDatos.gcCodFam = cmbFamilia.SelectedValue
        oDatos.gnCodEmp = cmbEmpresa.SelectedValue
        Dim BLProductos As New BLProducts
        If BLProductos.GrabarProducto(oDatos, True, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("El producto se registró correctamente", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtSerie_TextChanged(sender As Object, e As EventArgs) Handles txtSerie.TextChanged

    End Sub

    Private Sub txtSerie_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSerie.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub cmbMarca_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMarca.SelectedIndexChanged

    End Sub

    Private Sub cmbMarca_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMarca.SelectionChangeCommitted
        txtMarca.Text = cmbMarca.SelectedValue
    End Sub

    Private Sub chbModifica_CheckedChanged(sender As Object, e As EventArgs) Handles chbModifica.CheckedChanged

    End Sub

    Private Sub cmbMarca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMarca.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbModifica_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbModifica.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbOrigenCompra_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbOrigenCompra.SelectedIndexChanged

    End Sub

    Private Sub FrmProductos_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        End If
    End Sub

    Private Sub cmbOrigenCompra_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbOrigenCompra.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbFamilia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFamilia.SelectedIndexChanged

    End Sub

    Private Sub cmbUndMed_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbUndMed.SelectedValueChanged

    End Sub

    Private Sub cmbSubFamilia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSubFamilia.SelectedIndexChanged

    End Sub

    Private Sub cmbFamilia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbFamilia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbSubFamilia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbSubFamilia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbFamilia_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbFamilia.SelectionChangeCommitted

    End Sub

    Private Sub cmbFamilia_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbFamilia.SelectedValueChanged
        If (TypeOf cmbFamilia.SelectedValue IsNot DataRowView) Then
            'Call CargaSubFamilia()
        End If
    End Sub
End Class