﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodProve = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazSoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipPer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipDocProv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDirProv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCorreo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cContactos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipPer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipDocProv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cApePat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cApeMat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNombres = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodDep = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodProv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodDis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtApelPat = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbTipoPersona = New System.Windows.Forms.ComboBox()
        Me.btnHome = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.cmbBusqueda = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtApelMat = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNombres = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbTipDocumento = New System.Windows.Forms.ComboBox()
        Me.txtNumDocumento = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtContactos = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCorreo = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbDepartamento = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmbProvincia = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbDistrito = New System.Windows.Forms.ComboBox()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(743, 29)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(185, 26)
        Me.lblTitulo.Text = "REGISTRO DE PROVEEDORES"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodProve, Me.cNumDoc, Me.cRazSoc, Me.TipPer, Me.TipDocProv, Me.cDirProv, Me.cCorreo, Me.cContactos, Me.nTipPer, Me.cTipDocProv, Me.cApePat, Me.cApeMat, Me.cNombres, Me.nCodDep, Me.nCodProv, Me.nCodDis})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(12, 239)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(720, 293)
        Me.dtgListado.TabIndex = 19
        '
        'nCodProve
        '
        Me.nCodProve.DataPropertyName = "nCodProve"
        Me.nCodProve.HeaderText = "nCodProve"
        Me.nCodProve.Name = "nCodProve"
        Me.nCodProve.ReadOnly = True
        Me.nCodProve.Visible = False
        '
        'cNumDoc
        '
        Me.cNumDoc.DataPropertyName = "cNumDoc"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumDoc.DefaultCellStyle = DataGridViewCellStyle2
        Me.cNumDoc.HeaderText = "N° Documento"
        Me.cNumDoc.Name = "cNumDoc"
        Me.cNumDoc.ReadOnly = True
        Me.cNumDoc.Width = 130
        '
        'cRazSoc
        '
        Me.cRazSoc.DataPropertyName = "cRazSoc"
        Me.cRazSoc.HeaderText = "Razon Social"
        Me.cRazSoc.Name = "cRazSoc"
        Me.cRazSoc.ReadOnly = True
        Me.cRazSoc.Width = 310
        '
        'TipPer
        '
        Me.TipPer.DataPropertyName = "TipPer"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipPer.DefaultCellStyle = DataGridViewCellStyle3
        Me.TipPer.HeaderText = "Tip Persona"
        Me.TipPer.Name = "TipPer"
        Me.TipPer.ReadOnly = True
        Me.TipPer.Width = 138
        '
        'TipDocProv
        '
        Me.TipDocProv.DataPropertyName = "TipDocProv"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipDocProv.DefaultCellStyle = DataGridViewCellStyle4
        Me.TipDocProv.HeaderText = "Tip Documento"
        Me.TipDocProv.Name = "TipDocProv"
        Me.TipDocProv.ReadOnly = True
        Me.TipDocProv.Width = 122
        '
        'cDirProv
        '
        Me.cDirProv.DataPropertyName = "cDirProv"
        Me.cDirProv.HeaderText = "cDirProv"
        Me.cDirProv.Name = "cDirProv"
        Me.cDirProv.ReadOnly = True
        Me.cDirProv.Visible = False
        '
        'cCorreo
        '
        Me.cCorreo.DataPropertyName = "cCorreo"
        Me.cCorreo.HeaderText = "cCorreo"
        Me.cCorreo.Name = "cCorreo"
        Me.cCorreo.ReadOnly = True
        Me.cCorreo.Visible = False
        '
        'cContactos
        '
        Me.cContactos.DataPropertyName = "cContactos"
        Me.cContactos.HeaderText = "cContactos"
        Me.cContactos.Name = "cContactos"
        Me.cContactos.ReadOnly = True
        Me.cContactos.Visible = False
        '
        'nTipPer
        '
        Me.nTipPer.DataPropertyName = "nTipPer"
        Me.nTipPer.HeaderText = "nTipPer"
        Me.nTipPer.Name = "nTipPer"
        Me.nTipPer.ReadOnly = True
        Me.nTipPer.Visible = False
        '
        'cTipDocProv
        '
        Me.cTipDocProv.DataPropertyName = "cTipDocProv"
        Me.cTipDocProv.HeaderText = "cTipDocProv"
        Me.cTipDocProv.Name = "cTipDocProv"
        Me.cTipDocProv.ReadOnly = True
        Me.cTipDocProv.Visible = False
        '
        'cApePat
        '
        Me.cApePat.DataPropertyName = "cApePat"
        Me.cApePat.HeaderText = "cApePat"
        Me.cApePat.Name = "cApePat"
        Me.cApePat.ReadOnly = True
        Me.cApePat.Visible = False
        '
        'cApeMat
        '
        Me.cApeMat.DataPropertyName = "cApeMat"
        Me.cApeMat.HeaderText = "cApeMat"
        Me.cApeMat.Name = "cApeMat"
        Me.cApeMat.ReadOnly = True
        Me.cApeMat.Visible = False
        '
        'cNombres
        '
        Me.cNombres.DataPropertyName = "cNombres"
        Me.cNombres.HeaderText = "cNombres"
        Me.cNombres.Name = "cNombres"
        Me.cNombres.ReadOnly = True
        Me.cNombres.Visible = False
        '
        'nCodDep
        '
        Me.nCodDep.DataPropertyName = "nCodDep"
        Me.nCodDep.HeaderText = "nCodDep"
        Me.nCodDep.Name = "nCodDep"
        Me.nCodDep.ReadOnly = True
        Me.nCodDep.Visible = False
        '
        'nCodProv
        '
        Me.nCodProv.DataPropertyName = "nCodProv"
        Me.nCodProv.HeaderText = "nCodProv"
        Me.nCodProv.Name = "nCodProv"
        Me.nCodProv.ReadOnly = True
        Me.nCodProv.Visible = False
        '
        'nCodDis
        '
        Me.nCodDis.DataPropertyName = "nCodDis"
        Me.nCodDis.HeaderText = "nCodDis"
        Me.nCodDis.Name = "nCodDis"
        Me.nCodDis.ReadOnly = True
        Me.nCodDis.Visible = False
        '
        'txtApelPat
        '
        Me.txtApelPat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApelPat.Location = New System.Drawing.Point(104, 64)
        Me.txtApelPat.Name = "txtApelPat"
        Me.txtApelPat.Size = New System.Drawing.Size(129, 23)
        Me.txtApelPat.TabIndex = 3
        Me.txtApelPat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(9, 71)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 16)
        Me.Label17.TabIndex = 125
        Me.Label17.Text = "Apellido Pat:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 44)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 16)
        Me.Label6.TabIndex = 124
        Me.Label6.Text = "Tip Persona:"
        '
        'cmbTipoPersona
        '
        Me.cmbTipoPersona.BackColor = System.Drawing.Color.White
        Me.cmbTipoPersona.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoPersona.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoPersona.FormattingEnabled = True
        Me.cmbTipoPersona.Location = New System.Drawing.Point(104, 36)
        Me.cmbTipoPersona.Name = "cmbTipoPersona"
        Me.cmbTipoPersona.Size = New System.Drawing.Size(152, 24)
        Me.cmbTipoPersona.TabIndex = 0
        '
        'btnHome
        '
        Me.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnHome.FlatAppearance.BorderSize = 0
        Me.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHome.Image = Global.Presentacion.My.Resources.Resources.Home_20
        Me.btnHome.Location = New System.Drawing.Point(697, 206)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Size = New System.Drawing.Size(35, 30)
        Me.btnHome.TabIndex = 18
        Me.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnHome.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(167, 206)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(80, 30)
        Me.btnEliminar.TabIndex = 15
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(88, 206)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(78, 30)
        Me.btnGrabar.TabIndex = 14
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatAppearance.BorderSize = 0
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(12, 206)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 30)
        Me.btnNuevo.TabIndex = 13
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 98)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 16)
        Me.Label1.TabIndex = 135
        Me.Label1.Text = "Razon Social:"
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(498, 209)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(199, 23)
        Me.txtBuscar.TabIndex = 17
        Me.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbBusqueda
        '
        Me.cmbBusqueda.BackColor = System.Drawing.Color.White
        Me.cmbBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBusqueda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbBusqueda.FormattingEnabled = True
        Me.cmbBusqueda.Location = New System.Drawing.Point(343, 209)
        Me.cmbBusqueda.Name = "cmbBusqueda"
        Me.cmbBusqueda.Size = New System.Drawing.Size(153, 24)
        Me.cmbBusqueda.TabIndex = 16
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(218, 538)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(285, 16)
        Me.Label5.TabIndex = 144
        Me.Label5.Text = "Doble Click / ENTER para seleccionar un registro"
        '
        'txtApelMat
        '
        Me.txtApelMat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApelMat.Location = New System.Drawing.Point(313, 64)
        Me.txtApelMat.Name = "txtApelMat"
        Me.txtApelMat.Size = New System.Drawing.Size(158, 23)
        Me.txtApelMat.TabIndex = 4
        Me.txtApelMat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(248, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 16)
        Me.Label2.TabIndex = 146
        Me.Label2.Text = "Apel Mat:"
        '
        'txtNombres
        '
        Me.txtNombres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombres.Location = New System.Drawing.Point(574, 64)
        Me.txtNombres.Name = "txtNombres"
        Me.txtNombres.Size = New System.Drawing.Size(158, 23)
        Me.txtNombres.TabIndex = 5
        Me.txtNombres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(507, 71)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 16)
        Me.Label4.TabIndex = 149
        Me.Label4.Text = "Nombres:"
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazonSocial.Location = New System.Drawing.Point(104, 91)
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(367, 23)
        Me.txtRazonSocial.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(261, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(99, 16)
        Me.Label7.TabIndex = 151
        Me.Label7.Text = "Tip Documento:"
        '
        'cmbTipDocumento
        '
        Me.cmbTipDocumento.BackColor = System.Drawing.Color.White
        Me.cmbTipDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipDocumento.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipDocumento.FormattingEnabled = True
        Me.cmbTipDocumento.Location = New System.Drawing.Point(363, 36)
        Me.cmbTipDocumento.Name = "cmbTipDocumento"
        Me.cmbTipDocumento.Size = New System.Drawing.Size(108, 24)
        Me.cmbTipDocumento.TabIndex = 1
        '
        'txtNumDocumento
        '
        Me.txtNumDocumento.Location = New System.Drawing.Point(597, 35)
        Me.txtNumDocumento.Name = "txtNumDocumento"
        Me.txtNumDocumento.Size = New System.Drawing.Size(135, 23)
        Me.txtNumDocumento.TabIndex = 2
        Me.txtNumDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(500, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(95, 16)
        Me.Label8.TabIndex = 153
        Me.Label8.Text = "N° Documento:"
        '
        'txtContactos
        '
        Me.txtContactos.Location = New System.Drawing.Point(546, 91)
        Me.txtContactos.Name = "txtContactos"
        Me.txtContactos.Size = New System.Drawing.Size(186, 23)
        Me.txtContactos.TabIndex = 7
        Me.txtContactos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(475, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 16)
        Me.Label3.TabIndex = 155
        Me.Label3.Text = "Contactos:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Location = New System.Drawing.Point(104, 117)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(367, 23)
        Me.txtDireccion.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(9, 124)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 16)
        Me.Label9.TabIndex = 157
        Me.Label9.Text = "Dirección:"
        '
        'txtCorreo
        '
        Me.txtCorreo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCorreo.Location = New System.Drawing.Point(104, 143)
        Me.txtCorreo.Name = "txtCorreo"
        Me.txtCorreo.Size = New System.Drawing.Size(367, 23)
        Me.txtCorreo.TabIndex = 9
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(9, 150)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 16)
        Me.Label10.TabIndex = 159
        Me.Label10.Text = "Correo:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(9, 178)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(94, 16)
        Me.Label11.TabIndex = 161
        Me.Label11.Text = "Departamento:"
        '
        'cmbDepartamento
        '
        Me.cmbDepartamento.BackColor = System.Drawing.Color.White
        Me.cmbDepartamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDepartamento.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbDepartamento.FormattingEnabled = True
        Me.cmbDepartamento.Location = New System.Drawing.Point(104, 170)
        Me.cmbDepartamento.Name = "cmbDepartamento"
        Me.cmbDepartamento.Size = New System.Drawing.Size(157, 24)
        Me.cmbDepartamento.TabIndex = 10
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(270, 178)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 16)
        Me.Label12.TabIndex = 163
        Me.Label12.Text = "Provincia:"
        '
        'cmbProvincia
        '
        Me.cmbProvincia.BackColor = System.Drawing.Color.White
        Me.cmbProvincia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProvincia.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbProvincia.FormattingEnabled = True
        Me.cmbProvincia.Location = New System.Drawing.Point(337, 170)
        Me.cmbProvincia.Name = "cmbProvincia"
        Me.cmbProvincia.Size = New System.Drawing.Size(168, 24)
        Me.cmbProvincia.TabIndex = 11
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(518, 178)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(53, 16)
        Me.Label13.TabIndex = 165
        Me.Label13.Text = "Distrito:"
        '
        'cmbDistrito
        '
        Me.cmbDistrito.BackColor = System.Drawing.Color.White
        Me.cmbDistrito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDistrito.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbDistrito.FormattingEnabled = True
        Me.cmbDistrito.Location = New System.Drawing.Point(577, 170)
        Me.cmbDistrito.Name = "cmbDistrito"
        Me.cmbDistrito.Size = New System.Drawing.Size(155, 24)
        Me.cmbDistrito.TabIndex = 12
        '
        'FrmProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(743, 562)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.cmbDistrito)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.cmbProvincia)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.cmbDepartamento)
        Me.Controls.Add(Me.txtCorreo)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtContactos)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtNumDocumento)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cmbTipDocumento)
        Me.Controls.Add(Me.txtNombres)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtApelMat)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cmbBusqueda)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.txtRazonSocial)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnHome)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.txtApelPat)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmbTipoPersona)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmProveedor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnMover As Panel
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents dtgListado As DataGridView
    Friend WithEvents txtApelPat As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cmbTipoPersona As ComboBox
    Friend WithEvents btnHome As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnGrabar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtBuscar As TextBox
    Friend WithEvents cmbBusqueda As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtApelMat As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtNombres As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbTipDocumento As System.Windows.Forms.ComboBox
    Friend WithEvents txtNumDocumento As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtContactos As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCorreo As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbDepartamento As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmbProvincia As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmbDistrito As System.Windows.Forms.ComboBox
    Friend WithEvents nCodProve As DataGridViewTextBoxColumn
    Friend WithEvents cNumDoc As DataGridViewTextBoxColumn
    Friend WithEvents cRazSoc As DataGridViewTextBoxColumn
    Friend WithEvents TipPer As DataGridViewTextBoxColumn
    Friend WithEvents TipDocProv As DataGridViewTextBoxColumn
    Friend WithEvents cDirProv As DataGridViewTextBoxColumn
    Friend WithEvents cCorreo As DataGridViewTextBoxColumn
    Friend WithEvents cContactos As DataGridViewTextBoxColumn
    Friend WithEvents nTipPer As DataGridViewTextBoxColumn
    Friend WithEvents cTipDocProv As DataGridViewTextBoxColumn
    Friend WithEvents cApePat As DataGridViewTextBoxColumn
    Friend WithEvents cApeMat As DataGridViewTextBoxColumn
    Friend WithEvents cNombres As DataGridViewTextBoxColumn
    Friend WithEvents nCodDep As DataGridViewTextBoxColumn
    Friend WithEvents nCodProv As DataGridViewTextBoxColumn
    Friend WithEvents nCodDis As DataGridViewTextBoxColumn
End Class
