﻿Imports Negocios
Imports Entidades
Imports Business
Imports System.Windows.Forms
Public Class FrmProveedor
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Private Sub FrmProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call Limpiar()
        Call CargaCombos()
        Call CargaDepartamento()
        Call CargaProvincia()
        Call CargaDistrito()
        Call ListarClientes()
        Call InterfaceEntrada()
        txtBuscar.Focus()
    End Sub
    Sub Limpiar()
        bNuevo = True
        cmbTipoPersona.SelectedValue = 1
        txtApelMat.Clear()
        txtApelPat.Clear()
        txtNombres.Clear()
        txtContactos.Clear()
        txtCorreo.Clear()
        txtDireccion.Clear()
        txtRazonSocial.Clear()
        txtNumDocumento.Clear()
        txtBuscar.Clear()
    End Sub
    Sub InterfaceNuevo()
        txtRazonSocial.Enabled = False
        txtNumDocumento.Enabled = True
        txtApelMat.Enabled = True
        txtApelPat.Enabled = True
        txtNombres.Enabled = True
        txtContactos.Enabled = True
        txtCorreo.Enabled = True
        txtDireccion.Enabled = True
        txtBuscar.Enabled = False
        btnNuevo.Enabled = False
        btnGrabar.Enabled = True
        btnEliminar.Enabled = False
        dtgListado.Enabled = False
        cmbBusqueda.Enabled = False

        cmbTipoPersona.Enabled = True
        cmbTipDocumento.Enabled = True
        cmbDepartamento.Enabled = True
        cmbProvincia.Enabled = True
        cmbDistrito.Enabled = True
    End Sub
    Sub InterfaceEntrada()
        txtRazonSocial.Enabled = False
        txtNumDocumento.Enabled = False
        txtApelMat.Enabled = False
        txtApelPat.Enabled = False
        txtNombres.Enabled = False
        txtContactos.Enabled = False
        txtCorreo.Enabled = False
        txtDireccion.Enabled = False
        txtBuscar.Enabled = True
        btnNuevo.Enabled = True
        btnGrabar.Enabled = False
        btnEliminar.Enabled = False
        dtgListado.Enabled = True
        cmbBusqueda.Enabled = True

        cmbTipoPersona.Enabled = False
        cmbTipDocumento.Enabled = False
        cmbDepartamento.Enabled = False
        cmbProvincia.Enabled = False
        cmbDistrito.Enabled = False
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("20,200,230")
        Call CargaCombo(CreaDatoCombos(dt, 20), cmbTipDocumento)
        Call CargaCombo(CreaDatoCombos(dt, 200), cmbBusqueda)
        Call CargaCombo(CreaDatoCombos(dt, 230), cmbTipoPersona)
        cmbTipoPersona.SelectedValue = 1
    End Sub
    Sub CargaDepartamento()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarDepartamento()
        Call CargaCombo(dt, cmbDepartamento)
        cmbDepartamento.SelectedValue = -1
    End Sub
    Sub CargaProvincia()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarProvincia(IIf(cmbDepartamento.SelectedValue = Nothing, "", cmbDepartamento.SelectedValue))
        Call CargaCombo(dt, cmbProvincia)
    End Sub
    Sub CargaDistrito()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarDistrito(IIf(cmbProvincia.SelectedValue = Nothing, "", cmbProvincia.SelectedValue))
        Call CargaCombo(dt, cmbDistrito)
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Call Limpiar()
        Call InterfaceNuevo()
        cmbTipoPersona.Focus()
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If bNuevo = False Then
            Call ModificarProducto()
            Call ListarClientes()
        Else
            Call InsertarProducto()
            Call ListarClientes()
        End If

    End Sub
    Sub ModificarProducto()
        Dim oDatos As New BEProveedor
        cMensaje = ""
        oDatos.gnCodProve = txtNumDocumento.Tag
        oDatos.gcApeMat = txtApelMat.Text
        oDatos.gcApePat = txtApelPat.Text
        oDatos.gcContactos = txtContactos.Text
        oDatos.gcCorreo = txtCorreo.Text
        oDatos.gcDirProv = txtDireccion.Text
        oDatos.gcNombres = txtNombres.Text
        oDatos.gcNumDoc = txtNumDocumento.Text
        oDatos.gcRazSoc = txtRazonSocial.Text
        oDatos.gcTipDocProv = cmbTipDocumento.SelectedValue
        oDatos.gnCodDep = IIf(cmbDepartamento.SelectedValue = Nothing, "", cmbDepartamento.SelectedValue)
        oDatos.gnCodDis = IIf(cmbDistrito.SelectedValue = Nothing, "", cmbDistrito.SelectedValue)
        oDatos.gnCodProv = IIf(cmbProvincia.SelectedValue = Nothing, "", cmbProvincia.SelectedValue)
        oDatos.gnTipPer = cmbTipoPersona.SelectedValue
        Dim BLProveedor As New BLProveedor
        If BLProveedor.Grabar(oDatos, False, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se modificó correctamente el proveedor", MsgBoxStyle.Information, "MENSAJE")
            Call Limpiar()
            Call InterfaceEntrada()
            txtBuscar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub InsertarProducto()
        Dim oDatos As New BEProveedor
        cMensaje = ""
        oDatos.gcApeMat = txtApelMat.Text
        oDatos.gcApePat = txtApelPat.Text
        oDatos.gcContactos = txtContactos.Text
        oDatos.gcCorreo = txtCorreo.Text
        oDatos.gcDirProv = txtDireccion.Text
        oDatos.gcNombres = txtNombres.Text
        oDatos.gcNumDoc = txtNumDocumento.Text
        oDatos.gcRazSoc = txtRazonSocial.Text
        oDatos.gcTipDocProv = cmbTipDocumento.SelectedValue
        oDatos.gnCodDep = IIf(cmbDepartamento.SelectedValue = Nothing, "", cmbDepartamento.SelectedValue)
        oDatos.gnCodDis = IIf(cmbDistrito.SelectedValue = Nothing, "", cmbDistrito.SelectedValue)
        oDatos.gnCodProv = IIf(cmbProvincia.SelectedValue = Nothing, "", cmbProvincia.SelectedValue)
        oDatos.gnTipPer = cmbTipoPersona.SelectedValue
        Dim BLProveedor As New BLProveedor
        If BLProveedor.Grabar(oDatos, True, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se registró correctamente el proveedor", MsgBoxStyle.Information, "MENSAJE")
            Call Limpiar()
            Call InterfaceEntrada()
            txtBuscar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbBusqueda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBusqueda.SelectedIndexChanged

    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Call ListarClientes()
    End Sub

    Private Sub cmbBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbBusqueda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim oDatos As New BEProveedor
        If MsgBox("¿Esta seguro que desea eliminar el proveedor?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodProve = txtNumDocumento.Tag
            Dim BLProveedor As New BLProveedor
            If BLProveedor.Eliminar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("Se eliminó correctamente el proveedor", MsgBoxStyle.Information, "MENSAJE")
                Call Limpiar()
                Call InterfaceEntrada()
                Call ListarClientes()
                txtBuscar.Focus()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Call Limpiar()
        Call InterfaceEntrada()
        txtBuscar.Focus()
    End Sub

    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnHome.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbBusqueda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbBusqueda.SelectionChangeCommitted
        txtBuscar.Clear()
        txtBuscar.Focus()
        Call ListarClientes()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Sub ListarClientes()
        Dim dt As DataTable
        Dim BLProveedor As New BLProveedor
        dt = BLProveedor.Listar(txtBuscar.Text, cmbBusqueda.SelectedValue)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Private Sub dtgListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellDoubleClick
        If dtgListado.Rows.Count > 0 Then
            txtNumDocumento.Tag = dtgListado.CurrentRow.Cells("nCodProve").Value
            txtNumDocumento.Text = dtgListado.CurrentRow.Cells("cNumDoc").Value
            txtRazonSocial.Text = dtgListado.CurrentRow.Cells("cRazSoc").Value
            txtDireccion.Text = dtgListado.CurrentRow.Cells("cDirProv").Value
            txtCorreo.Text = dtgListado.CurrentRow.Cells("cCorreo").Value
            txtContactos.Text = dtgListado.CurrentRow.Cells("cContactos").Value
            txtApelPat.Text = dtgListado.CurrentRow.Cells("cApePat").Value
            txtApelMat.Text = dtgListado.CurrentRow.Cells("cApeMat").Value
            txtNombres.Text = dtgListado.CurrentRow.Cells("cNombres").Value


            cmbTipoPersona.SelectedValue = dtgListado.CurrentRow.Cells("nTipPer").Value
            cmbTipDocumento.SelectedValue = dtgListado.CurrentRow.Cells("cTipDocProv").Value
            cmbDepartamento.SelectedValue = dtgListado.CurrentRow.Cells("nCodDep").Value
            Call CargaProvincia()
            cmbProvincia.SelectedValue = dtgListado.CurrentRow.Cells("nCodProv").Value
            Call CargaDistrito()
            cmbDistrito.SelectedValue = dtgListado.CurrentRow.Cells("nCodDis").Value
            bNuevo = False
            txtNumDocumento.Enabled = True
            txtContactos.Enabled = True
            txtCorreo.Enabled = True
            txtDireccion.Enabled = True
            btnGrabar.Enabled = True

            btnNuevo.Enabled = False
            txtBuscar.Enabled = False
            btnEliminar.Enabled = True
            dtgListado.Enabled = False
            cmbBusqueda.Enabled = False

            cmbTipoPersona.Enabled = True
            cmbTipDocumento.Enabled = True
            cmbDepartamento.Enabled = True
            cmbProvincia.Enabled = True
            cmbDistrito.Enabled = True
            If cmbTipoPersona.SelectedValue = 1 Then
                txtApelPat.Enabled = True
                txtApelMat.Enabled = True
                txtNombres.Enabled = True
                txtRazonSocial.Enabled = False
            Else
                txtApelPat.Enabled = False
                txtApelMat.Enabled = False
                txtNombres.Enabled = False
                txtRazonSocial.Enabled = True
            End If
            cmbTipoPersona.Focus()
        End If
    End Sub
    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub cmbTipoPersona_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoPersona.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub cmbTipoPersona_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoPersona.SelectedIndexChanged

    End Sub
    Private Sub cmbTipDocumento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipDocumento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub cmbTipDocumento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipDocumento.SelectedIndexChanged

    End Sub
    Private Sub txtNumDocumento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumDocumento.KeyPress
        Call ValidaEspacio(e)
        Call ValidaNumeros(e)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtNumDocumento_TextChanged(sender As Object, e As EventArgs) Handles txtNumDocumento.TextChanged

    End Sub
    Private Sub txtApelPat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtApelPat.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtApelPat_TextChanged(sender As Object, e As EventArgs) Handles txtApelPat.TextChanged

    End Sub

    Private Sub txtApelMat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtApelMat.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtApelMat_TextChanged(sender As Object, e As EventArgs) Handles txtApelMat.TextChanged

    End Sub

    Private Sub txtNombres_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombres.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtNombres_TextChanged(sender As Object, e As EventArgs) Handles txtNombres.TextChanged

    End Sub

    Private Sub txtRazonSocial_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRazonSocial.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtRazonSocial_TextChanged(sender As Object, e As EventArgs) Handles txtRazonSocial.TextChanged

    End Sub

    Private Sub txtContactos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtContactos.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtContactos_TextChanged(sender As Object, e As EventArgs) Handles txtContactos.TextChanged

    End Sub

    Private Sub txtDireccion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDireccion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtDireccion_TextChanged(sender As Object, e As EventArgs) Handles txtDireccion.TextChanged

    End Sub

    Private Sub txtCorreo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCorreo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtCorreo_TextChanged(sender As Object, e As EventArgs) Handles txtCorreo.TextChanged

    End Sub

    Private Sub cmbDepartamento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbDepartamento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbDepartamento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDepartamento.SelectedIndexChanged

    End Sub

    Private Sub cmbProvincia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbProvincia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbProvincia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbProvincia.SelectedIndexChanged

    End Sub

    Private Sub cmbDistrito_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbDistrito.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbDistrito_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDistrito.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoPersona_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoPersona.SelectionChangeCommitted
        If cmbTipoPersona.SelectedValue = 1 Then
            txtApelPat.Enabled = True
            txtApelMat.Enabled = True
            txtNombres.Enabled = True
            txtRazonSocial.Enabled = False
        Else
            txtApelPat.Enabled = False
            txtApelMat.Enabled = False
            txtNombres.Enabled = False
            txtRazonSocial.Enabled = True
        End If
    End Sub

    Private Sub cmbDepartamento_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbDepartamento.SelectionChangeCommitted
        Call CargaProvincia()
    End Sub

    Private Sub cmbProvincia_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbProvincia.SelectionChangeCommitted
        Call CargaDistrito()
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If dtgListado.Rows.Count > 0 Then
                txtNumDocumento.Tag = dtgListado.CurrentRow.Cells("nCodProve").Value
                txtNumDocumento.Text = dtgListado.CurrentRow.Cells("cNumDoc").Value
                txtRazonSocial.Text = dtgListado.CurrentRow.Cells("cRazSoc").Value
                txtDireccion.Text = dtgListado.CurrentRow.Cells("cDirProv").Value
                txtCorreo.Text = dtgListado.CurrentRow.Cells("cCorreo").Value
                txtContactos.Text = dtgListado.CurrentRow.Cells("cContactos").Value
                txtApelPat.Text = dtgListado.CurrentRow.Cells("cApePat").Value
                txtApelMat.Text = dtgListado.CurrentRow.Cells("cApeMat").Value
                txtNombres.Text = dtgListado.CurrentRow.Cells("cNombres").Value


                cmbTipoPersona.SelectedValue = dtgListado.CurrentRow.Cells("nTipPer").Value
                cmbTipDocumento.SelectedValue = dtgListado.CurrentRow.Cells("cTipDocProv").Value
                cmbDepartamento.SelectedValue = dtgListado.CurrentRow.Cells("nCodDep").Value
                Call CargaProvincia()
                cmbProvincia.SelectedValue = dtgListado.CurrentRow.Cells("nCodProv").Value
                Call CargaDistrito()
                cmbDistrito.SelectedValue = dtgListado.CurrentRow.Cells("nCodDis").Value
                bNuevo = False
                txtNumDocumento.Enabled = True
                txtContactos.Enabled = True
                txtCorreo.Enabled = True
                txtDireccion.Enabled = True
                btnGrabar.Enabled = True

                btnNuevo.Enabled = False
                txtBuscar.Enabled = False
                btnEliminar.Enabled = True
                dtgListado.Enabled = False
                cmbBusqueda.Enabled = False

                cmbTipoPersona.Enabled = True
                cmbTipDocumento.Enabled = True
                cmbDepartamento.Enabled = True
                cmbProvincia.Enabled = True
                cmbDistrito.Enabled = True
                If cmbTipoPersona.SelectedValue = 1 Then
                    txtApelPat.Enabled = True
                    txtApelMat.Enabled = True
                    txtNombres.Enabled = True
                    txtRazonSocial.Enabled = False
                Else
                    txtApelPat.Enabled = False
                    txtApelMat.Enabled = False
                    txtNombres.Enabled = False
                    txtRazonSocial.Enabled = True
                End If
                cmbTipoPersona.Focus()
            End If
        End If
    End Sub
End Class