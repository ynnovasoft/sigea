﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmRegistroCobroBloque
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRazonSocial = New System.Windows.Forms.Label()
        Me.txtMontoPagado = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMontoPago = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNumOpe = New System.Windows.Forms.TextBox()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodPed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumComp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CondPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MONTOTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MONTOPAGADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DEUDAFECHA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDeudaPend = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cmbMedioPago = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbBanco = New System.Windows.Forms.ComboBox()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cmbMoneda = New System.Windows.Forms.ComboBox()
        Me.txtEquivalente = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnEstadoCuenta = New System.Windows.Forms.Button()
        Me.btnExaminar = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbMonedaComprobante = New System.Windows.Forms.ComboBox()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(791, 29)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(304, 26)
        Me.lblTitulo.Text = "CANCELACIÓN DE COMPROBANTES POR BLOQUE"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 16)
        Me.Label1.TabIndex = 135
        Me.Label1.Text = "Cliente:"
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.BackColor = System.Drawing.SystemColors.Info
        Me.txtRazonSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRazonSocial.Location = New System.Drawing.Point(63, 33)
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(438, 23)
        Me.txtRazonSocial.TabIndex = 149
        Me.txtRazonSocial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMontoPagado
        '
        Me.txtMontoPagado.BackColor = System.Drawing.SystemColors.Info
        Me.txtMontoPagado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoPagado.Location = New System.Drawing.Point(142, 295)
        Me.txtMontoPagado.Name = "txtMontoPagado"
        Me.txtMontoPagado.Size = New System.Drawing.Size(125, 23)
        Me.txtMontoPagado.TabIndex = 160
        Me.txtMontoPagado.Text = "0.00"
        Me.txtMontoPagado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 303)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 16)
        Me.Label4.TabIndex = 159
        Me.Label4.Text = "Monto Pagado:"
        '
        'txtMontoPago
        '
        Me.txtMontoPago.Location = New System.Drawing.Point(654, 350)
        Me.txtMontoPago.Name = "txtMontoPago"
        Me.txtMontoPago.Size = New System.Drawing.Size(128, 23)
        Me.txtMontoPago.TabIndex = 7
        Me.txtMontoPago.Text = "0.00"
        Me.txtMontoPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(555, 357)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 158
        Me.Label5.Text = "Monto a Pagar:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(310, 330)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 16)
        Me.Label6.TabIndex = 162
        Me.Label6.Text = "N° Ope:"
        '
        'txtNumOpe
        '
        Me.txtNumOpe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumOpe.Location = New System.Drawing.Point(368, 323)
        Me.txtNumOpe.Name = "txtNumOpe"
        Me.txtNumOpe.Size = New System.Drawing.Size(141, 23)
        Me.txtNumOpe.TabIndex = 4
        Me.txtNumOpe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle33.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle33.Font = New System.Drawing.Font("Tahoma", 9.75!)
        DataGridViewCellStyle33.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle33
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodPed, Me.cNumComp, Me.dFecReg, Me.CondPag, Me.TipMon, Me.MONTOTOTAL, Me.MONTOPAGADO, Me.DEUDAFECHA})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(10, 61)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(772, 230)
        Me.dtgListado.TabIndex = 164
        '
        'nCodPed
        '
        Me.nCodPed.DataPropertyName = "nCodPed"
        Me.nCodPed.HeaderText = "nCodPed"
        Me.nCodPed.Name = "nCodPed"
        Me.nCodPed.ReadOnly = True
        Me.nCodPed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodPed.Visible = False
        '
        'cNumComp
        '
        Me.cNumComp.DataPropertyName = "cNumComp"
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumComp.DefaultCellStyle = DataGridViewCellStyle34
        Me.cNumComp.HeaderText = "N° Comprobante"
        Me.cNumComp.Name = "cNumComp"
        Me.cNumComp.ReadOnly = True
        Me.cNumComp.Width = 110
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle35
        Me.dFecReg.HeaderText = "Fecha Emisión"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dFecReg.Width = 85
        '
        'CondPag
        '
        Me.CondPag.DataPropertyName = "CondPag"
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.CondPag.DefaultCellStyle = DataGridViewCellStyle36
        Me.CondPag.HeaderText = "Condición Pago"
        Me.CondPag.Name = "CondPag"
        Me.CondPag.ReadOnly = True
        Me.CondPag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CondPag.Width = 169
        '
        'TipMon
        '
        Me.TipMon.DataPropertyName = "TipMon"
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TipMon.DefaultCellStyle = DataGridViewCellStyle37
        Me.TipMon.HeaderText = "Moneda"
        Me.TipMon.Name = "TipMon"
        Me.TipMon.ReadOnly = True
        Me.TipMon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TipMon.Width = 80
        '
        'MONTOTOTAL
        '
        Me.MONTOTOTAL.DataPropertyName = "MONTOTOTAL"
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle38.Format = "N2"
        Me.MONTOTOTAL.DefaultCellStyle = DataGridViewCellStyle38
        Me.MONTOTOTAL.HeaderText = "Importe Total"
        Me.MONTOTOTAL.Name = "MONTOTOTAL"
        Me.MONTOTOTAL.ReadOnly = True
        Me.MONTOTOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MONTOTOTAL.Width = 112
        '
        'MONTOPAGADO
        '
        Me.MONTOPAGADO.DataPropertyName = "MONTOPAGADO"
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle39.Format = "N2"
        DataGridViewCellStyle39.NullValue = Nothing
        Me.MONTOPAGADO.DefaultCellStyle = DataGridViewCellStyle39
        Me.MONTOPAGADO.HeaderText = "Monto Pagado"
        Me.MONTOPAGADO.Name = "MONTOPAGADO"
        Me.MONTOPAGADO.ReadOnly = True
        '
        'DEUDAFECHA
        '
        Me.DEUDAFECHA.DataPropertyName = "DEUDAFECHA"
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle40.Format = "N2"
        DataGridViewCellStyle40.NullValue = Nothing
        Me.DEUDAFECHA.DefaultCellStyle = DataGridViewCellStyle40
        Me.DEUDAFECHA.HeaderText = "Deuda Pendiente"
        Me.DEUDAFECHA.Name = "DEUDAFECHA"
        Me.DEUDAFECHA.ReadOnly = True
        Me.DEUDAFECHA.Width = 112
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(284, 303)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 16)
        Me.Label2.TabIndex = 163
        Me.Label2.Text = "Deuda Pend:"
        '
        'txtDeudaPend
        '
        Me.txtDeudaPend.BackColor = System.Drawing.SystemColors.Info
        Me.txtDeudaPend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDeudaPend.Location = New System.Drawing.Point(368, 296)
        Me.txtDeudaPend.Name = "txtDeudaPend"
        Me.txtDeudaPend.Size = New System.Drawing.Size(141, 23)
        Me.txtDeudaPend.TabIndex = 164
        Me.txtDeudaPend.Text = "0.00"
        Me.txtDeudaPend.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(530, 303)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(69, 16)
        Me.Label20.TabIndex = 172
        Me.Label20.Text = "Med.Pago:"
        '
        'cmbMedioPago
        '
        Me.cmbMedioPago.BackColor = System.Drawing.Color.White
        Me.cmbMedioPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMedioPago.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMedioPago.FormattingEnabled = True
        Me.cmbMedioPago.Location = New System.Drawing.Point(602, 295)
        Me.cmbMedioPago.Name = "cmbMedioPago"
        Me.cmbMedioPago.Size = New System.Drawing.Size(180, 24)
        Me.cmbMedioPago.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 330)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(47, 16)
        Me.Label11.TabIndex = 176
        Me.Label11.Text = "Banco:"
        '
        'cmbBanco
        '
        Me.cmbBanco.BackColor = System.Drawing.Color.White
        Me.cmbBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBanco.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbBanco.FormattingEnabled = True
        Me.cmbBanco.Location = New System.Drawing.Point(60, 326)
        Me.cmbBanco.Name = "cmbBanco"
        Me.cmbBanco.Size = New System.Drawing.Size(207, 24)
        Me.cmbBanco.TabIndex = 3
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Location = New System.Drawing.Point(654, 323)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(128, 23)
        Me.txtTipoCambio.TabIndex = 5
        Me.txtTipoCambio.Text = "0.0000"
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(556, 329)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(85, 16)
        Me.Label13.TabIndex = 178
        Me.Label13.Text = "Tipo Cambio:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(306, 357)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 16)
        Me.Label14.TabIndex = 180
        Me.Label14.Text = "Moneda:"
        '
        'cmbMoneda
        '
        Me.cmbMoneda.BackColor = System.Drawing.Color.White
        Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMoneda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMoneda.FormattingEnabled = True
        Me.cmbMoneda.Location = New System.Drawing.Point(368, 349)
        Me.cmbMoneda.Name = "cmbMoneda"
        Me.cmbMoneda.Size = New System.Drawing.Size(141, 24)
        Me.cmbMoneda.TabIndex = 6
        '
        'txtEquivalente
        '
        Me.txtEquivalente.BackColor = System.Drawing.SystemColors.Info
        Me.txtEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEquivalente.Location = New System.Drawing.Point(654, 376)
        Me.txtEquivalente.Name = "txtEquivalente"
        Me.txtEquivalente.Size = New System.Drawing.Size(128, 23)
        Me.txtEquivalente.TabIndex = 182
        Me.txtEquivalente.Text = "0.00"
        Me.txtEquivalente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(556, 383)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(43, 16)
        Me.Label16.TabIndex = 181
        Me.Label16.Text = "Monto"
        '
        'btnEstadoCuenta
        '
        Me.btnEstadoCuenta.FlatAppearance.BorderSize = 0
        Me.btnEstadoCuenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEstadoCuenta.Image = Global.Presentacion.My.Resources.Resources.EECC
        Me.btnEstadoCuenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEstadoCuenta.Location = New System.Drawing.Point(10, 412)
        Me.btnEstadoCuenta.Name = "btnEstadoCuenta"
        Me.btnEstadoCuenta.Size = New System.Drawing.Size(132, 30)
        Me.btnEstadoCuenta.TabIndex = 9
        Me.btnEstadoCuenta.Text = "Estado Cuenta"
        Me.btnEstadoCuenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEstadoCuenta.UseVisualStyleBackColor = True
        '
        'btnExaminar
        '
        Me.btnExaminar.FlatAppearance.BorderSize = 0
        Me.btnExaminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExaminar.Image = Global.Presentacion.My.Resources.Resources.BuscarArchivo_20
        Me.btnExaminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExaminar.Location = New System.Drawing.Point(694, 31)
        Me.btnExaminar.Name = "btnExaminar"
        Me.btnExaminar.Size = New System.Drawing.Size(88, 30)
        Me.btnExaminar.TabIndex = 1
        Me.btnExaminar.Text = "Examinar"
        Me.btnExaminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExaminar.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(704, 412)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(78, 30)
        Me.btnGrabar.TabIndex = 8
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 118
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(507, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 16)
        Me.Label3.TabIndex = 184
        Me.Label3.Text = "Moneda:"
        '
        'cmbMonedaComprobante
        '
        Me.cmbMonedaComprobante.BackColor = System.Drawing.Color.White
        Me.cmbMonedaComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMonedaComprobante.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMonedaComprobante.FormattingEnabled = True
        Me.cmbMonedaComprobante.Location = New System.Drawing.Point(567, 33)
        Me.cmbMonedaComprobante.Name = "cmbMonedaComprobante"
        Me.cmbMonedaComprobante.Size = New System.Drawing.Size(121, 24)
        Me.cmbMonedaComprobante.TabIndex = 0
        '
        'FrmRegistroCobroBloque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(791, 450)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbMonedaComprobante)
        Me.Controls.Add(Me.btnEstadoCuenta)
        Me.Controls.Add(Me.txtEquivalente)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.cmbMoneda)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.cmbBanco)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.cmbMedioPago)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDeudaPend)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtNumOpe)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtMontoPagado)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtMontoPago)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.btnExaminar)
        Me.Controls.Add(Me.txtRazonSocial)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmRegistroCobroBloque"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnMover As Panel
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents btnGrabar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRazonSocial As System.Windows.Forms.Label
    Friend WithEvents btnExaminar As System.Windows.Forms.Button
    Friend WithEvents txtMontoPagado As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMontoPago As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtNumOpe As TextBox
    Friend WithEvents dtgListado As DataGridView
    Friend WithEvents Label2 As Label
    Friend WithEvents txtDeudaPend As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents cmbMedioPago As ComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents cmbBanco As ComboBox
    Friend WithEvents txtTipoCambio As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents cmbMoneda As ComboBox
    Friend WithEvents txtEquivalente As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents btnEstadoCuenta As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents cmbMonedaComprobante As ComboBox
    Friend WithEvents nCodPed As DataGridViewTextBoxColumn
    Friend WithEvents cNumComp As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents CondPag As DataGridViewTextBoxColumn
    Friend WithEvents TipMon As DataGridViewTextBoxColumn
    Friend WithEvents MONTOTOTAL As DataGridViewTextBoxColumn
    Friend WithEvents MONTOPAGADO As DataGridViewTextBoxColumn
    Friend WithEvents DEUDAFECHA As DataGridViewTextBoxColumn
End Class
