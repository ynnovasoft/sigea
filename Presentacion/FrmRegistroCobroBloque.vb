﻿Imports Business
Imports System.Configuration
Imports Entities
Public Class FrmRegistroCobroBloque
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Private Sub FrmRegistroCobroBloque_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargaMedioPago()
        Call Limpiar()
        Call InterfaceEntrada()
        btnExaminar.Focus()
    End Sub
    Sub CargaMedioPago()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(80, "1,2,3,4,5,9,10,11")
        Call CargaCombo(dt, cmbMedioPago)
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60,340")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMonedaComprobante)
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
        Label16.Text = "Monto Dolares:"
        Call CargaCombo(CreaDatoCombos(dt, 340), cmbBanco)
    End Sub
    Sub Limpiar()
        txtEquivalente.Text = "0.00"
        txtDeudaPend.Text = "0.00"
        txtRazonSocial.Text = ""
        txtMontoPagado.Text = "0.00"
        txtMontoPago.Text = "0.00"
        txtRazonSocial.Tag = 0
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        txtNumOpe.Text = ""
        cmbMedioPago.SelectedValue = 1
        cmbBanco.SelectedValue = 0
        Call LimpiaGrilla(dtgListado)
    End Sub
    Sub InterfaceEntrada()
        cmbMedioPago.Enabled = False
        txtNumOpe.Enabled = False
        txtMontoPago.Enabled = False
        btnGrabar.Enabled = False
        btnEstadoCuenta.Enabled = False
        dtgListado.Enabled = False
        cmbBanco.Enabled = False
        txtTipoCambio.Enabled = False
        cmbMonedaComprobante.Enabled = False
        cmbMoneda.Enabled = False
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If CDbl(txtMontoPago.Text) = 0 Then
            MsgBox("Debe registrar el monto de pago", MsgBoxStyle.Exclamation, "MENSAJE")
            txtMontoPago.Focus()
            Exit Sub
        End If
        If CDbl(txtDeudaPend.Text) = 0 Then
            MsgBox("El documento que intenta cancelar ya se encuentra cancelado, no existe monto pendiente de pago", MsgBoxStyle.Exclamation, "MENSAJE")
            btnExaminar.Focus()
            Exit Sub
        End If
        If cmbMonedaComprobante.SelectedValue = 1 Then
            If CDbl(txtDeudaPend.Text) <> CDbl(txtMontoPago.Text) Then
                MsgBox("El monto de pago debria ser igual a la deuda pendiente", MsgBoxStyle.Exclamation, "MENSAJE")
                txtMontoPago.Focus()
                Exit Sub
            End If
        Else
            If cmbMoneda.SelectedValue = 1 Then
                If CDbl(txtDeudaPend.Text) > CDbl(txtEquivalente.Text) Then
                    MsgBox("El monto de pago debria ser igual a la deuda pendiente", MsgBoxStyle.Exclamation, "MENSAJE")
                    txtMontoPago.Focus()
                    Exit Sub
                End If
            Else
                If CDbl(txtDeudaPend.Text) <> CDbl(txtMontoPago.Text) Then
                    MsgBox("El monto de pago debria ser igual a la deuda pendiente", MsgBoxStyle.Exclamation, "MENSAJE")
                    txtMontoPago.Focus()
                    Exit Sub
                End If
            End If
        End If
        Dim Monto As Double
        For i = 0 To dtgListado.Rows.Count - 1
            Monto = 0
            If cmbMonedaComprobante.SelectedValue = 1 Then
                Monto = dtgListado.Rows(i).Cells("DEUDAFECHA").Value
            Else
                If cmbMoneda.SelectedValue = 1 Then
                    Monto = dtgListado.Rows(i).Cells("DEUDAFECHA").Value * CDbl(txtTipoCambio.Text)
                Else
                    Monto = dtgListado.Rows(i).Cells("DEUDAFECHA").Value
                End If
            End If
            Call RegistrarPago(dtgListado.Rows(i).Cells("nCodPed").Value, Monto)
        Next
        Call Limpiar()
        Call InterfaceEntrada()
        btnExaminar.Select()

    End Sub
    Sub RegistrarPago(ByVal Codigo As Integer, ByVal Monto As Double)
        cMensaje = ""
        Dim BLComunes As New BLCommons
        If BLComunes.RegistrarPago(Codigo, Monto, 1, MDIPrincipal.CodUsuario,
                                   cmbMedioPago.SelectedValue, MDIPrincipal.FechaSistema,
                                   txtNumOpe.Text, MDIPrincipal.CodigoAgencia, 1, cmbMoneda.SelectedValue,
                                   cmbBanco.SelectedValue, txtTipoCambio.Text,
                                   MDIPrincipal.CodigoEmpresa, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Sub CalculaMontos()
        Dim MontoPagado, Total As Double
        If dtgListado.Rows.Count > 0 Then
            MontoPagado = 0
            Total = 0
            For i = 0 To dtgListado.Rows.Count - 1
                MontoPagado = MontoPagado + CDbl(dtgListado.Rows(i).Cells("MONTOPAGADO").Value)
                Total = Total + CDbl(dtgListado.Rows(i).Cells("MONTOTOTAL").Value)
            Next
            txtDeudaPend.Text = FormatNumber(Total - MontoPagado, 2)
            txtMontoPagado.Text = FormatNumber(MontoPagado, 2)
            txtMontoPago.Text = FormatNumber(txtDeudaPend.Text, 2)
        Else
            txtMontoPago.Text = FormatNumber(0, 2)
            txtDeudaPend.Text = FormatNumber(0, 2)
            txtMontoPago.Text = FormatNumber(0, 2)
        End If
    End Sub
    Sub ObtenerComprobantes()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.RecuperarComprobantesCreditoBloque(txtRazonSocial.Tag, cmbMonedaComprobante.SelectedValue, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
        If dtgListado.Rows.Count > 0 Then
            dtgListado.Enabled = True
            cmbMedioPago.Enabled = True
            btnGrabar.Enabled = True
            btnEstadoCuenta.Enabled = True
            txtMontoPago.Enabled = True
            txtNumOpe.Enabled = True
            cmbBanco.Enabled = False
            txtTipoCambio.Enabled = True
            cmbMoneda.Enabled = True
            cmbMedioPago.Select()
        Else
            dtgListado.Enabled = False
            cmbMedioPago.Enabled = False
            btnGrabar.Enabled = False
            btnEstadoCuenta.Enabled = False
            txtMontoPago.Enabled = False
            txtNumOpe.Enabled = False
            cmbBanco.Enabled = False
            txtTipoCambio.Enabled = False
            cmbMoneda.Enabled = False
        End If
        Call CalculaMontos()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub btnExaminar_Click(sender As Object, e As EventArgs) Handles btnExaminar.Click
        Dim BEClientes As New BECustomer
        FrmConsultaCliente.Inicio(BEClientes)
        FrmConsultaCliente = Nothing
        If BEClientes.nCodCli <> 0 Then
            txtRazonSocial.Tag = BEClientes.nCodCli
            txtRazonSocial.Text = BEClientes.cRazSoc
            cmbMonedaComprobante.Enabled = True
            Call ObtenerComprobantes()
        End If
    End Sub

    Private Sub txtNumOpe_TextChanged(sender As Object, e As EventArgs) Handles txtNumOpe.TextChanged

    End Sub

    Private Sub btnExaminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnExaminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMontoPago_TextChanged(sender As Object, e As EventArgs) Handles txtMontoPago.TextChanged
        If txtMontoPago.Text = "" Then
            txtMontoPago.Text = 0
        End If
        If txtTipoCambio.Text = "" Then
            txtTipoCambio.Text = 0
        End If
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / IIf(txtTipoCambio.Text = "", 0, txtTipoCambio.Text), 2)
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * IIf(txtTipoCambio.Text = "", 0, txtTipoCambio.Text), 2)
        End If
    End Sub

    Private Sub txtNumOpe_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumOpe.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMontoPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMontoPago.KeyPress
        Call ValidaDecimales(e, txtMontoPago)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMontoPago_GotFocus(sender As Object, e As EventArgs) Handles txtMontoPago.GotFocus
        Call ValidaFormatoMonedaGot(txtMontoPago)
    End Sub

    Private Sub cmbMedioPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMedioPago.SelectedIndexChanged

    End Sub

    Private Sub txtMontoPago_LostFocus(sender As Object, e As EventArgs) Handles txtMontoPago.LostFocus
        Call ValidaFormatoMonedaLost(txtMontoPago)
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub cmbMedioPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMedioPago.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbBanco_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBanco.SelectedIndexChanged

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        ElseIf e.KeyCode = Keys.Delete Then
            e.Handled = True
            dtgListado.Rows.RemoveAt(dtgListado.CurrentRow.Index)
        End If
        Call CalculaMontos()
    End Sub

    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub cmbBanco_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbBanco.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMontoPagado_Click(sender As Object, e As EventArgs) Handles txtMontoPagado.Click

    End Sub

    Private Sub cmbMedioPago_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMedioPago.SelectionChangeCommitted
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        txtNumOpe.Text = ""
        If cmbMedioPago.SelectedValue = 4 Or cmbMedioPago.SelectedValue = 5 Then
            cmbBanco.SelectedValue = 1
            cmbBanco.Enabled = True
            cmbBanco.Focus()
        Else
            cmbBanco.SelectedValue = 0
            cmbBanco.Enabled = False
            txtNumOpe.Focus()
        End If
    End Sub

    Private Sub cmbBanco_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbBanco.SelectionChangeCommitted
        txtNumOpe.Focus()
    End Sub

    Private Sub cmbMoneda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMoneda.SelectionChangeCommitted
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / txtTipoCambio.Text, 2)
            Label16.Text = "Monto Dolares:"
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * txtTipoCambio.Text, 2)
            Label16.Text = "Monto Soles:"
        End If
    End Sub

    Private Sub txtMontoPago_Leave(sender As Object, e As EventArgs) Handles txtMontoPago.Leave
        If txtMontoPago.Text = "" Then
            txtMontoPago.Text = 0
        End If
        If txtTipoCambio.Text = "" Then
            txtTipoCambio.Text = 0
        End If
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / txtTipoCambio.Text, 2)
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * txtTipoCambio.Text, 2)
        End If
    End Sub

    Private Sub btnEstadoCuenta_Click(sender As Object, e As EventArgs) Handles btnEstadoCuenta.Click
        Dim ModeloEstadoCuenta As String = ConfigurationManager.AppSettings("ModeloEstadoCuenta")
        Dim RutaEstadoCuenta As String = ConfigurationManager.AppSettings("RutaEstadoCuenta")
        RutaEstadoCuenta = RutaEstadoCuenta + "EE-CC-" + txtRazonSocial.Text + ".pdf"
        Dim Modalidad As Integer = 0
        Dim Result As DialogResult = MsgBox("Seleccione la modalidad de Estado de Cuenta: " & vbNewLine & " " & vbNewLine & "(SI)=Deuda vencida pendiente" & vbNewLine & "(NO)=Deuda total pendiente", MessageBoxIcon.Information + vbYesNoCancel, "MENSAJE")
        If Result = vbYes Then
            Modalidad = 1
        ElseIf Result = vbNo Then
            Modalidad = 2
        Else
            Exit Sub
        End If

        If ModeloEstadoCuenta = "1" Then
            Call CrearPdf_TA4_EECC_Modelo_1(txtRazonSocial.Tag, txtRazonSocial.Text, Modalidad)
            Process.Start(RutaEstadoCuenta)
        ElseIf ModeloEstadoCuenta = "2" Then
            Call CrearPdf_TA4_EECC_Modelo_2(txtRazonSocial.Tag, txtRazonSocial.Text, Modalidad)
            Process.Start(RutaEstadoCuenta)
        ElseIf ModeloEstadoCuenta = "3" Then

        ElseIf ModeloEstadoCuenta = "4" Then

        ElseIf ModeloEstadoCuenta = "5" Then

        End If
    End Sub

    Private Sub txtTipoCambio_Leave(sender As Object, e As EventArgs) Handles txtTipoCambio.Leave
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / txtTipoCambio.Text, 2)
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * txtTipoCambio.Text, 2)
        End If
    End Sub

    Private Sub btnEstadoCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEstadoCuenta.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMonedaComprobante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMonedaComprobante.SelectedIndexChanged

    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub cmbMonedaComprobante_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMonedaComprobante.SelectionChangeCommitted
        Call ObtenerComprobantes()
    End Sub

    Private Sub cmbMonedaComprobante_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbMonedaComprobante.SelectedValueChanged
        If (TypeOf cmbMonedaComprobante.SelectedValue IsNot DataRowView) Then
            Call ObtenerComprobantes()
        End If
    End Sub

    Private Sub cmbMonedaComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMonedaComprobante.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub FrmRegistroCobroBloque_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class