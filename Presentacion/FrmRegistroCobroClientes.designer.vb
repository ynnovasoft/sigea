﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmRegistroCobroClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNumDoc = New System.Windows.Forms.Label()
        Me.txtRazonSocial = New System.Windows.Forms.Label()
        Me.txtMontoTotal = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMontoPagado = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMontoPago = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNumOpe = New System.Windows.Forms.TextBox()
        Me.txtNumCom = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.Eliminar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.nCodRegPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MedioPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumOpe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipMon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nTipCam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtMoneda = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtFechaReg = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtFechaVenc = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDeudaPend = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cmbMedioPago = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCondPago = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbBanco = New System.Windows.Forms.ComboBox()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cmbMoneda = New System.Windows.Forms.ComboBox()
        Me.txtEquivalente = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(796, 25)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(218, 22)
        Me.lblTitulo.Text = "CANCELACIÓN DE COMPROBANTES"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(586, 37)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(50, 14)
        Me.Label17.TabIndex = 125
        Me.Label17.Text = "N° Doc:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 14)
        Me.Label1.TabIndex = 135
        Me.Label1.Text = "Cliente:"
        '
        'txtNumDoc
        '
        Me.txtNumDoc.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumDoc.Location = New System.Drawing.Point(640, 29)
        Me.txtNumDoc.Name = "txtNumDoc"
        Me.txtNumDoc.Size = New System.Drawing.Size(147, 22)
        Me.txtNumDoc.TabIndex = 148
        Me.txtNumDoc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRazonSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRazonSocial.Location = New System.Drawing.Point(97, 29)
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(404, 22)
        Me.txtRazonSocial.TabIndex = 149
        Me.txtRazonSocial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMontoTotal
        '
        Me.txtMontoTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtMontoTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoTotal.Location = New System.Drawing.Point(640, 77)
        Me.txtMontoTotal.Name = "txtMontoTotal"
        Me.txtMontoTotal.Size = New System.Drawing.Size(147, 22)
        Me.txtMontoTotal.TabIndex = 155
        Me.txtMontoTotal.Text = "0.00"
        Me.txtMontoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(558, 85)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 14)
        Me.Label8.TabIndex = 154
        Me.Label8.Text = "Monto Total:"
        '
        'txtMontoPagado
        '
        Me.txtMontoPagado.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtMontoPagado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoPagado.Location = New System.Drawing.Point(105, 285)
        Me.txtMontoPagado.Name = "txtMontoPagado"
        Me.txtMontoPagado.Size = New System.Drawing.Size(162, 22)
        Me.txtMontoPagado.TabIndex = 160
        Me.txtMontoPagado.Text = "0.00"
        Me.txtMontoPagado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 293)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 14)
        Me.Label4.TabIndex = 159
        Me.Label4.Text = "Monto Pagado:"
        '
        'txtMontoPago
        '
        Me.txtMontoPago.Location = New System.Drawing.Point(659, 333)
        Me.txtMontoPago.Name = "txtMontoPago"
        Me.txtMontoPago.Size = New System.Drawing.Size(128, 22)
        Me.txtMontoPago.TabIndex = 5
        Me.txtMontoPago.Text = "0.00"
        Me.txtMontoPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(560, 342)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(90, 14)
        Me.Label5.TabIndex = 158
        Me.Label5.Text = "Monto a Pagar:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(310, 317)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 14)
        Me.Label6.TabIndex = 162
        Me.Label6.Text = "N° Ope:"
        '
        'txtNumOpe
        '
        Me.txtNumOpe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumOpe.Location = New System.Drawing.Point(367, 309)
        Me.txtNumOpe.Name = "txtNumOpe"
        Me.txtNumOpe.Size = New System.Drawing.Size(141, 22)
        Me.txtNumOpe.TabIndex = 2
        Me.txtNumOpe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNumCom
        '
        Me.txtNumCom.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumCom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumCom.Location = New System.Drawing.Point(97, 53)
        Me.txtNumCom.Name = "txtNumCom"
        Me.txtNumCom.Size = New System.Drawing.Size(147, 22)
        Me.txtNumCom.TabIndex = 163
        Me.txtNumCom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 14)
        Me.Label3.TabIndex = 162
        Me.Label3.Text = "N° Comp:"
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Eliminar, Me.nCodRegPag, Me.dFecReg, Me.MedioPago, Me.cBanco, Me.cNumOpe, Me.cMoneda, Me.nMonPag, Me.nTipMon, Me.nTipCam})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(10, 113)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(777, 152)
        Me.dtgListado.TabIndex = 164
        '
        'Eliminar
        '
        Me.Eliminar.HeaderText = "Acción"
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.ReadOnly = True
        Me.Eliminar.Text = "Eliminar"
        Me.Eliminar.UseColumnTextForButtonValue = True
        Me.Eliminar.Width = 60
        '
        'nCodRegPag
        '
        Me.nCodRegPag.DataPropertyName = "nCodRegPag"
        Me.nCodRegPag.HeaderText = "nCodRegPag"
        Me.nCodRegPag.Name = "nCodRegPag"
        Me.nCodRegPag.ReadOnly = True
        Me.nCodRegPag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodRegPag.Visible = False
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle2
        Me.dFecReg.HeaderText = "F.Cancelación"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        Me.dFecReg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'MedioPago
        '
        Me.MedioPago.DataPropertyName = "MedioPago"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.MedioPago.DefaultCellStyle = DataGridViewCellStyle3
        Me.MedioPago.HeaderText = "Medio Pago"
        Me.MedioPago.Name = "MedioPago"
        Me.MedioPago.ReadOnly = True
        Me.MedioPago.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MedioPago.Width = 152
        '
        'cBanco
        '
        Me.cBanco.DataPropertyName = "cBanco"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cBanco.DefaultCellStyle = DataGridViewCellStyle4
        Me.cBanco.HeaderText = "Banco"
        Me.cBanco.Name = "cBanco"
        Me.cBanco.ReadOnly = True
        Me.cBanco.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cBanco.Width = 129
        '
        'cNumOpe
        '
        Me.cNumOpe.DataPropertyName = "cNumOpe"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumOpe.DefaultCellStyle = DataGridViewCellStyle5
        Me.cNumOpe.HeaderText = "N° Ope"
        Me.cNumOpe.Name = "cNumOpe"
        Me.cNumOpe.ReadOnly = True
        Me.cNumOpe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cNumOpe.Width = 152
        '
        'cMoneda
        '
        Me.cMoneda.DataPropertyName = "cMoneda"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cMoneda.DefaultCellStyle = DataGridViewCellStyle6
        Me.cMoneda.HeaderText = "Moneda"
        Me.cMoneda.Name = "cMoneda"
        Me.cMoneda.ReadOnly = True
        Me.cMoneda.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cMoneda.Width = 80
        '
        'nMonPag
        '
        Me.nMonPag.DataPropertyName = "nMonPag"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        Me.nMonPag.DefaultCellStyle = DataGridViewCellStyle7
        Me.nMonPag.HeaderText = "Mont Pag."
        Me.nMonPag.Name = "nMonPag"
        Me.nMonPag.ReadOnly = True
        Me.nMonPag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nTipMon
        '
        Me.nTipMon.DataPropertyName = "nTipMon"
        Me.nTipMon.HeaderText = "nTipMon"
        Me.nTipMon.Name = "nTipMon"
        Me.nTipMon.ReadOnly = True
        Me.nTipMon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nTipMon.Visible = False
        '
        'nTipCam
        '
        Me.nTipCam.DataPropertyName = "nTipCam"
        Me.nTipCam.HeaderText = "nTipCam"
        Me.nTipCam.Name = "nTipCam"
        Me.nTipCam.ReadOnly = True
        Me.nTipCam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nTipCam.Visible = False
        '
        'txtMoneda
        '
        Me.txtMoneda.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtMoneda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMoneda.Location = New System.Drawing.Point(351, 53)
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(150, 22)
        Me.txtMoneda.TabIndex = 166
        Me.txtMoneda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(293, 61)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 14)
        Me.Label7.TabIndex = 165
        Me.Label7.Text = "Moneda:"
        '
        'txtFechaReg
        '
        Me.txtFechaReg.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtFechaReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFechaReg.Location = New System.Drawing.Point(640, 53)
        Me.txtFechaReg.Name = "txtFechaReg"
        Me.txtFechaReg.Size = New System.Drawing.Size(147, 22)
        Me.txtFechaReg.TabIndex = 168
        Me.txtFechaReg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(581, 61)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(55, 14)
        Me.Label10.TabIndex = 167
        Me.Label10.Text = "Fec.Reg:"
        '
        'txtFechaVenc
        '
        Me.txtFechaVenc.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtFechaVenc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFechaVenc.Location = New System.Drawing.Point(351, 77)
        Me.txtFechaVenc.Name = "txtFechaVenc"
        Me.txtFechaVenc.Size = New System.Drawing.Size(150, 22)
        Me.txtFechaVenc.TabIndex = 170
        Me.txtFechaVenc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(285, 85)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(62, 14)
        Me.Label12.TabIndex = 169
        Me.Label12.Text = "Fec.Venc:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(283, 293)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 14)
        Me.Label2.TabIndex = 163
        Me.Label2.Text = "Deuda Pend:"
        '
        'txtDeudaPend
        '
        Me.txtDeudaPend.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtDeudaPend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDeudaPend.Location = New System.Drawing.Point(367, 285)
        Me.txtDeudaPend.Name = "txtDeudaPend"
        Me.txtDeudaPend.Size = New System.Drawing.Size(141, 22)
        Me.txtDeudaPend.TabIndex = 164
        Me.txtDeudaPend.Text = "0.00"
        Me.txtDeudaPend.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(535, 293)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(65, 14)
        Me.Label20.TabIndex = 172
        Me.Label20.Text = "Med.Pago:"
        '
        'cmbMedioPago
        '
        Me.cmbMedioPago.BackColor = System.Drawing.Color.White
        Me.cmbMedioPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMedioPago.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMedioPago.FormattingEnabled = True
        Me.cmbMedioPago.Location = New System.Drawing.Point(607, 285)
        Me.cmbMedioPago.Name = "cmbMedioPago"
        Me.cmbMedioPago.Size = New System.Drawing.Size(180, 22)
        Me.cmbMedioPago.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 85)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 14)
        Me.Label9.TabIndex = 173
        Me.Label9.Text = "Cond.Pago:"
        '
        'txtCondPago
        '
        Me.txtCondPago.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtCondPago.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCondPago.Location = New System.Drawing.Point(97, 77)
        Me.txtCondPago.Name = "txtCondPago"
        Me.txtCondPago.Size = New System.Drawing.Size(147, 22)
        Me.txtCondPago.TabIndex = 174
        Me.txtCondPago.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 317)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(44, 14)
        Me.Label11.TabIndex = 176
        Me.Label11.Text = "Banco:"
        '
        'cmbBanco
        '
        Me.cmbBanco.BackColor = System.Drawing.Color.White
        Me.cmbBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBanco.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbBanco.FormattingEnabled = True
        Me.cmbBanco.Location = New System.Drawing.Point(60, 309)
        Me.cmbBanco.Name = "cmbBanco"
        Me.cmbBanco.Size = New System.Drawing.Size(207, 22)
        Me.cmbBanco.TabIndex = 1
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Location = New System.Drawing.Point(659, 309)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(128, 22)
        Me.txtTipoCambio.TabIndex = 3
        Me.txtTipoCambio.Text = "0.0000"
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(560, 317)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 14)
        Me.Label13.TabIndex = 178
        Me.Label13.Text = "Tipo Cambio:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(310, 342)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(54, 14)
        Me.Label14.TabIndex = 180
        Me.Label14.Text = "Moneda:"
        '
        'cmbMoneda
        '
        Me.cmbMoneda.BackColor = System.Drawing.Color.White
        Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMoneda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMoneda.FormattingEnabled = True
        Me.cmbMoneda.Location = New System.Drawing.Point(367, 333)
        Me.cmbMoneda.Name = "cmbMoneda"
        Me.cmbMoneda.Size = New System.Drawing.Size(141, 22)
        Me.cmbMoneda.TabIndex = 4
        '
        'txtEquivalente
        '
        Me.txtEquivalente.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtEquivalente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEquivalente.Location = New System.Drawing.Point(659, 357)
        Me.txtEquivalente.Name = "txtEquivalente"
        Me.txtEquivalente.Size = New System.Drawing.Size(128, 22)
        Me.txtEquivalente.TabIndex = 182
        Me.txtEquivalente.Text = "0.00"
        Me.txtEquivalente.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(560, 365)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(42, 14)
        Me.Label16.TabIndex = 181
        Me.Label16.Text = "Monto"
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(685, 393)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(102, 26)
        Me.btnGrabar.TabIndex = 6
        Me.btnGrabar.Text = "Grabar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 118
        '
        'FrmRegistroCobroClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 434)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtEquivalente)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.cmbMoneda)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.cmbBanco)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtCondPago)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.cmbMedioPago)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtFechaVenc)
        Me.Controls.Add(Me.txtDeudaPend)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtFechaReg)
        Me.Controls.Add(Me.txtNumOpe)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtMoneda)
        Me.Controls.Add(Me.txtMontoPagado)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtMontoPago)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.txtMontoTotal)
        Me.Controls.Add(Me.txtNumCom)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtRazonSocial)
        Me.Controls.Add(Me.txtNumDoc)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmRegistroCobroClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnMover As Panel
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents Label17 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNumDoc As System.Windows.Forms.Label
    Friend WithEvents txtRazonSocial As System.Windows.Forms.Label
    Friend WithEvents txtMontoTotal As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMontoPagado As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMontoPago As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNumCom As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtNumOpe As TextBox
    Friend WithEvents dtgListado As DataGridView
    Friend WithEvents txtMoneda As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtFechaReg As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents txtFechaVenc As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtDeudaPend As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents cmbMedioPago As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtCondPago As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents cmbBanco As ComboBox
    Friend WithEvents txtTipoCambio As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents cmbMoneda As ComboBox
    Friend WithEvents txtEquivalente As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Eliminar As DataGridViewButtonColumn
    Friend WithEvents nCodRegPag As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents MedioPago As DataGridViewTextBoxColumn
    Friend WithEvents cBanco As DataGridViewTextBoxColumn
    Friend WithEvents cNumOpe As DataGridViewTextBoxColumn
    Friend WithEvents cMoneda As DataGridViewTextBoxColumn
    Friend WithEvents nMonPag As DataGridViewTextBoxColumn
    Friend WithEvents nTipMon As DataGridViewTextBoxColumn
    Friend WithEvents nTipCam As DataGridViewTextBoxColumn
End Class
