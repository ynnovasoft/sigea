﻿Imports Entidades
Imports Business
Imports Entities
Public Class FrmRegistroCobroClientes
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim BEPedido As New BEPedido
    Dim BEClientes As New BECustomer
    Dim ModalidadVentana As Integer = 1 '2=Registrar Pago
    Dim Accion As Boolean = False
    Private Sub FrmRegistroCobroBloque_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargaMedioPago()
        If ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosComprobante()
        End If
    End Sub
    Sub InicioCancelacion(ByVal oBEPedido As BEPedido, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)
        BEPedido = oBEPedido
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosComprobante()
        txtNumCom.Tag = BEPedido.gnCodPed
        txtNumCom.Text = BEPedido.gcNumComp
        txtRazonSocial.Text = BEClientes.cRazSoc
        txtNumDoc.Text = BEClientes.cNumDoc
        txtNumDoc.Tag = BEClientes.nCodCli
        txtFechaReg.Text = BEPedido.gdFecReg
        txtFechaVenc.Text = BEPedido.gdFecVen
        txtMoneda.Text = BEPedido.gcOrdComp
        cmbMoneda.SelectedValue = BEPedido.gnTipMon
        If cmbMoneda.SelectedValue = 1 Then
            Label16.Text = "Monto Dolares:"
        Else
            Label16.Text = "Monto Soles:"
        End If
        txtMontoTotal.Text = FormatNumber(BEPedido.gnImpTot, 2)
        txtCondPago.Text = BEPedido.gcMensaje
        Call ObtenerPagos()
        If BEPedido.gnCondPag = 2 Then
            If MsgBox("¡¡¡AVISO!!! Este comprobante  se encuentra asignado en modalidad de cobro por letras, ¿Desea realizar una amortización previa del comprobante?", MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
                cmbMedioPago.Enabled = True
                btnGrabar.Enabled = True
                txtMontoPago.Enabled = True
                txtNumOpe.Enabled = True
                cmbBanco.Enabled = False
                txtTipoCambio.Enabled = True
                cmbMoneda.Enabled = True
            Else
                cmbMedioPago.Enabled = False
                btnGrabar.Enabled = False
                txtMontoPago.Enabled = False
                txtNumOpe.Enabled = False
                cmbBanco.Enabled = False
                txtTipoCambio.Enabled = False
                cmbMoneda.Enabled = False
                dtgListado.Enabled = False
            End If
        Else
            cmbMedioPago.Enabled = True
            btnGrabar.Enabled = True
            txtMontoPago.Enabled = True
            txtNumOpe.Enabled = True
            cmbBanco.Enabled = False
            txtTipoCambio.Enabled = True
            cmbMoneda.Enabled = True
        End If
        cmbMedioPago.Select()
    End Sub
    Sub CargaMedioPago()
        Dim dt As New DataTable
        Dim BLCommons As New BLCommons
        dt = BLCommons.MostrarMaestroUnico(80, "1,2,3,4,5,9,10,11")
        Call CargaCombo(dt, cmbMedioPago)
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60,340")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
        Label16.Text = "Monto Dolares:"
        Call CargaCombo(CreaDatoCombos(dt, 340), cmbBanco)
    End Sub
    Sub Limpiar()
        txtEquivalente.Text = "0.00"
        txtNumDoc.Text = ""
        txtDeudaPend.Text = "0.00"
        txtNumCom.Text = ""
        txtNumCom.Tag = 0
        txtRazonSocial.Text = ""
        txtMontoTotal.Text = "0.00"
        txtMontoPagado.Text = "0.00"
        txtMontoPago.Text = "0.00"
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        txtNumOpe.Text = ""
        txtMoneda.Text = ""
        txtCondPago.Text = ""
        txtFechaReg.Text = ""
        txtFechaVenc.Text = ""
        cmbMedioPago.SelectedValue = 1
        cmbBanco.SelectedValue = 0
        txtNumDoc.Tag = 0
        Call LimpiaGrilla(dtgListado)
    End Sub
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If CDbl(txtMontoPago.Text) = 0 Then
            MsgBox("Debe registrar el monto de pago", MsgBoxStyle.Exclamation, "MENSAJE")
            txtMontoPago.Select()
            Exit Sub
        End If
        If CDbl(txtDeudaPend.Text) = 0 Then
            MsgBox("El documento que intenta cancelar ya se encuentra cancelado, no existe monto pendiente de pago", MsgBoxStyle.Exclamation, "MENSAJE")
            Accion = True
            Me.Close()
        End If
        Call RegistrarPago()
    End Sub
    Sub RegistrarPago()
        cMensaje = ""
        Dim Obj As New BLCommons
        If Obj.RegistrarPago(txtNumCom.Tag, txtMontoPago.Text, 1, MDIPrincipal.CodUsuario,
                                   cmbMedioPago.SelectedValue, MDIPrincipal.FechaSistema,
                                   txtNumOpe.Text, MDIPrincipal.CodigoAgencia, 1, cmbMoneda.SelectedValue,
                                   cmbBanco.SelectedValue, txtTipoCambio.Text,
                                   MDIPrincipal.CodigoEmpresa, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("El pago se registró correctamente", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Sub CalculaMontos()
        Dim MontoPagado, Total As Double
        If dtgListado.Rows.Count > 0 Then
            MontoPagado = 0
            Total = 0
            For i = 0 To dtgListado.Rows.Count - 1
                If dtgListado.Rows(i).Cells("nTipMon").Value = Convert.ToInt16(IIf(txtMoneda.Text = "SOLES", 1, 2)) Then
                    MontoPagado = MontoPagado + CDbl(dtgListado.Rows(i).Cells("nMonPag").Value)
                ElseIf Convert.ToInt16(IIf(txtMoneda.Text = "SOLES", 1, 2)) = 1 And dtgListado.Rows(i).Cells("nTipMon").Value = 2 Then
                    MontoPagado = MontoPagado + FormatNumber(CDbl(dtgListado.Rows(i).Cells("nMonPag").Value) * CDbl(dtgListado.Rows(i).Cells("nTipCam").Value), 2)
                ElseIf Convert.ToInt16(IIf(txtMoneda.Text = "SOLES", 1, 2)) = 2 And dtgListado.Rows(i).Cells("nTipMon").Value = 1 Then
                    MontoPagado = MontoPagado + FormatNumber(CDbl(dtgListado.Rows(i).Cells("nMonPag").Value) / CDbl(dtgListado.Rows(i).Cells("nTipCam").Value), 2)
                End If
            Next
            Total = txtMontoTotal.Text
            txtDeudaPend.Text = FormatNumber(IIf((Total - MontoPagado) < 0, 0, (Total - MontoPagado)), 2)
            txtMontoPagado.Text = FormatNumber(MontoPagado, 2)
        Else
            txtMontoPagado.Text = FormatNumber(0, 2)
            txtDeudaPend.Text = FormatNumber(txtMontoTotal.Text, 2)
        End If
    End Sub
    Sub ObtenerPagos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.RecuperarDetallePagos(txtNumCom.Tag, 1)
        Call LlenaAGridView(dt, dtgListado)
        If dt.Rows.Count > 0 Then
            dtgListado.Enabled = True
        Else
            dtgListado.Enabled = False
        End If
        Call CalculaMontos()
    End Sub
    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub
    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub txtNumOpe_TextChanged(sender As Object, e As EventArgs) Handles txtNumOpe.TextChanged

    End Sub
    Private Sub txtMontoPago_TextChanged(sender As Object, e As EventArgs) Handles txtMontoPago.TextChanged

    End Sub
    Private Sub txtNumOpe_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumOpe.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtMontoPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMontoPago.KeyPress

        Call ValidaDecimales(e, txtMontoPago)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtMontoPago_GotFocus(sender As Object, e As EventArgs) Handles txtMontoPago.GotFocus
        Call ValidaFormatoMonedaGot(txtMontoPago)
    End Sub
    Private Sub cmbMedioPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMedioPago.SelectedIndexChanged

    End Sub
    Private Sub txtMontoPago_LostFocus(sender As Object, e As EventArgs) Handles txtMontoPago.LostFocus
        Call ValidaFormatoMonedaLost(txtMontoPago)
    End Sub
    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        If dtgListado.Rows.Count > 0 Then
            If dtgListado.Columns(e.ColumnIndex).Name = "Eliminar" Then
                Dim Obj As New BLCommons
                cMensaje = ""
                Obj.ValidaEstadoMesContable(dtgListado.CurrentRow.Cells("nCodRegPag").Value, dtgListado.CurrentRow.Cells("dFecReg").Value,
                                    6, cMensaje)
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                If MsgBox("¿Esta seguro que desea eliminar el pago?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                    cMensaje = ""
                    If Obj.EliminarDetallePagos(dtgListado.CurrentRow.Cells("nCodRegPag").Value, 1, cMensaje) = True Then
                        If cMensaje <> "" Then
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                            Exit Sub
                        End If
                        Accion = True
                        Me.Close()
                    Else
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub cmbMedioPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMedioPago.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub cmbBanco_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBanco.SelectedIndexChanged

    End Sub
    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
    End Sub
    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub
    Private Sub cmbBanco_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbBanco.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub
    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub
    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub
    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub
    Private Sub txtMontoPagado_Click(sender As Object, e As EventArgs) Handles txtMontoPagado.Click

    End Sub
    Private Sub cmbMedioPago_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMedioPago.SelectionChangeCommitted
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        txtNumOpe.Text = ""
        If cmbMedioPago.SelectedValue = 4 Or cmbMedioPago.SelectedValue = 5 Then
            cmbBanco.SelectedValue = 1
            cmbBanco.Enabled = True
            cmbBanco.Select()
        Else
            cmbBanco.SelectedValue = 0
            cmbBanco.Enabled = False
            txtNumOpe.Select()
        End If
    End Sub
    Private Sub cmbBanco_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbBanco.SelectionChangeCommitted
        txtNumOpe.Select()
    End Sub
    Private Sub cmbMoneda_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbMoneda.SelectionChangeCommitted
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(IIf(txtMontoPago.Text = "", 0, txtMontoPago.Text) / txtTipoCambio.Text, 2)
            Label16.Text = "Monto Dolares:"
        Else
            txtEquivalente.Text = FormatNumber(IIf(txtMontoPago.Text = "", 0, txtMontoPago.Text) * txtTipoCambio.Text, 2)
            Label16.Text = "Monto Soles:"
        End If
    End Sub
    Private Sub txtMontoPago_Leave(sender As Object, e As EventArgs) Handles txtMontoPago.Leave
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(IIf(txtMontoPago.Text = "", 0, txtMontoPago.Text) / txtTipoCambio.Text, 2)
        Else
            txtEquivalente.Text = FormatNumber(IIf(txtMontoPago.Text = "", 0, txtMontoPago.Text) * txtTipoCambio.Text, 2)
        End If
    End Sub
    Private Sub txtTipoCambio_Leave(sender As Object, e As EventArgs) Handles txtTipoCambio.Leave
        If cmbMoneda.SelectedValue = 1 Then
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text / txtTipoCambio.Text, 2)
        Else
            txtEquivalente.Text = FormatNumber(txtMontoPago.Text * txtTipoCambio.Text, 2)
        End If
    End Sub
    Private Sub FrmRegistroCobroBloque_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class