﻿Imports Negocios
Imports Business
Imports Entidades
Imports System.Windows.Forms
Public Class FrmRegistroCompraExterna
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodProveedor As Integer = 0
    Private Sub FrmRegistroCompraExterna_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaCombos()
        Call CargarTipoComprobante()
        Call Limpiar()
        Call InterfaceEntrada()
        btnNuevo.Focus()
    End Sub
    Sub CargarTipoComprobante()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(30, "01,03")
        Call CargaCombo(dt, cmbComprobante)
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60,70")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
    End Sub
    Sub Limpiar()
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        dtpFechaEmision.Value = MDIPrincipal.FechaSistema
        nCodProveedor = 0
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioCompra, 4)
        txtProveedor.Text = ""
        txtNumCompra.Text = ""
        txtNumCompra.Tag = 0
        txtObservaciones.Text = ""
        txtVentasGravadas.Text = "0.00"
        txtNumComprobante.Text = ""
        txtIGV.Text = "0.00"
        txtImporteTotal.Text = "0.00"
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)

    End Sub

    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Call Limpiar()
        Call InterfaceNuevo()
        dtpFechaRegistro.Focus()

    End Sub
    Sub InterfaceNuevo()
        txtNumComprobante.Enabled = True
        cmbComprobante.Enabled = True
        btnNuevo.Enabled = False
        btnGrabar.Enabled = True
        txtVentasGravadas.Enabled = True
        txtIGV.Enabled = True
        txtImporteTotal.Enabled = True
        btnAnular.Enabled = False
        btnBuscar.Enabled = False
        txtObservaciones.Enabled = True
        txtTipoCambio.Enabled = False
        cmbMoneda.Enabled = True
        btnBuscarProveedor.Enabled = True
        dtpFechaRegistro.Enabled = True
        dtpFechaEmision.Enabled = True
    End Sub

    Private Sub btnNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Function ValidaDatos() As Boolean
        ValidaDatos = True
        If nCodProveedor = 0 Then
            MsgBox("Debe registrar el proveedor ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            btnBuscarProveedor.Focus()
            Exit Function
        End If
        If txtNumComprobante.Text.Trim.Length = 0 Then
            MsgBox("Debe registrar el número de comprobante", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtNumComprobante.Focus()
            Exit Function
        End If
        If CDbl(txtVentasGravadas.Text) = 0 Then
            MsgBox("Debe registrar las ventas grabadas", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtVentasGravadas.Focus()
            Exit Function
        End If
        If CDbl(txtImporteTotal.Text) = 0 Then
            MsgBox("Debe registrar el importe total", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtImporteTotal.Focus()
            Exit Function
        End If

    End Function
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click

        If ValidaDatos() = False Then
            Exit Sub
        End If
        Dim oDatos As New BERegistroCompraExterna
        cMensaje = ""
            oDatos.gnCodProve = nCodProveedor
            oDatos.gdFecReg = dtpFechaRegistro.Value
            oDatos.gdFecEmi = dtpFechaEmision.Value
            oDatos.gcTipCom = cmbComprobante.SelectedValue
            oDatos.gcNumComRef = txtNumComprobante.Text
            oDatos.gnTipCam = txtTipoCambio.Text
            oDatos.gnTipMon = cmbMoneda.SelectedValue
            oDatos.gcObservaciones = txtObservaciones.Text
            oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
            oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
            oDatos.gnVenGra = txtVentasGravadas.Text
            oDatos.gnIGV = txtIGV.Text
            oDatos.gnImpTot = txtImporteTotal.Text
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        Dim BLRegistroCompraExterna As New BLRegistroCompraExterna
        If BLRegistroCompraExterna.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            txtNumCompra.Text = oDatos.gcNumCom
            txtNumCompra.Tag = oDatos.gnCodCom
            Call InterfaceEntrada()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        Dim Obj As New BLCommons
        Obj.ValidaEstadoMesContable(txtNumCompra.Tag, dtpFechaRegistro.Value,
                                    5, cMensaje)
        If cMensaje <> "" Then
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If
        Dim oDatos As New BERegistroCompraExterna
        If MsgBox("¿Esta seguro que desea anular la compra?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodCom = txtNumCompra.Tag
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            Dim BLRegistroCompraExterna As New BLRegistroCompraExterna
            If BLRegistroCompraExterna.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Call Limpiar()
                Call InterfaceEntrada()
                btnNuevo.Focus()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If

    End Sub

    Private Sub btnAnular_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnular.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim BEProveedor As New BEProveedor
        Dim BERegistroCompraExterna As New BERegistroCompraExterna
        FrmConsultaComprasExternas.InicioCompras(BERegistroCompraExterna, BEProveedor)
        FrmConsultaComprasExternas = Nothing
        If BERegistroCompraExterna.gnCodCom <> 0 Then
            Call Limpiar()
            txtNumCompra.Tag = BERegistroCompraExterna.gnCodCom
            txtNumCompra.Text = BERegistroCompraExterna.gcNumCom
            txtObservaciones.Text = BERegistroCompraExterna.gcObservaciones
            txtTipoCambio.Text = FormatNumber(BERegistroCompraExterna.gnTipCam, 4)
            dtpFechaRegistro.Value = BERegistroCompraExterna.gdFecReg
            dtpFechaEmision.Value = BERegistroCompraExterna.gdFecEmi
            txtProveedor.Text = BEProveedor.gcRazSoc
            txtVentasGravadas.Text = FormatNumber(BERegistroCompraExterna.gnVenGra, 2)
            txtIGV.Text = FormatNumber(BERegistroCompraExterna.gnIGV, 2)
            txtImporteTotal.Text = FormatNumber(BERegistroCompraExterna.gnImpTot, 2)
            cmbMoneda.SelectedValue = BERegistroCompraExterna.gnTipMon
            txtNumComprobante.Text = BERegistroCompraExterna.gcNumComRef
            cmbComprobante.SelectedValue = BERegistroCompraExterna.gcTipCom

            Call InterfaceEntrada()
            btnAnular.Enabled = True
            btnNuevo.Enabled = False
        End If
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Call Limpiar()
        Call InterfaceEntrada()
        btnNuevo.Focus()
    End Sub
    Sub InterfaceEntrada()
        txtNumComprobante.Enabled = False
        cmbComprobante.Enabled = False
        btnNuevo.Enabled = True
        btnGrabar.Enabled = False
        btnAnular.Enabled = False
        btnBuscar.Enabled = True
        txtTipoCambio.Enabled = False
        cmbMoneda.Enabled = False
        txtVentasGravadas.Enabled = False
        txtIGV.Enabled = False
        txtImporteTotal.Enabled = False
        txtObservaciones.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaEmision.Enabled = False
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnHome.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub btnBuscarProveedor_Click(sender As Object, e As EventArgs) Handles btnBuscarProveedor.Click
        Dim BEProveedor As New BEProveedor
        FrmConsultaProveedor.Inicio(BEProveedor)
        FrmConsultaProveedor = Nothing
        If BEProveedor.gnCodProv <> 0 Then
            nCodProveedor = BEProveedor.gnCodProv
            txtProveedor.Text = BEProveedor.gcRazSoc
        Else
            btnBuscarProveedor.Focus()
        End If
    End Sub
    Private Sub btnBuscarProveedor_ImeModeChanged(sender As Object, e As EventArgs) Handles btnBuscarProveedor.ImeModeChanged
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbComprobante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbComprobante.SelectedIndexChanged

    End Sub

    Private Sub btnBuscarProveedor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarProveedor.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtNumComprobante_TextChanged(sender As Object, e As EventArgs) Handles txtNumComprobante.TextChanged

    End Sub

    Private Sub cmbComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbComprobante.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtVentasGravadas_TextChanged(sender As Object, e As EventArgs) Handles txtVentasGravadas.TextChanged

    End Sub

    Private Sub txtNumComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumComprobante.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtIGV_TextChanged(sender As Object, e As EventArgs) Handles txtIGV.TextChanged

    End Sub

    Private Sub txtVentasGravadas_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtVentasGravadas.KeyPress
        Call ValidaDecimales(e, txtVentasGravadas)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtImporteTotal_TextChanged(sender As Object, e As EventArgs) Handles txtImporteTotal.TextChanged

    End Sub

    Private Sub txtIGV_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIGV.KeyPress
        Call ValidaDecimales(e, txtIGV)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtImporteTotal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtImporteTotal.KeyPress
        Call ValidaDecimales(e, txtImporteTotal)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtVentasGravadas_GotFocus(sender As Object, e As EventArgs) Handles txtVentasGravadas.GotFocus
        Call ValidaFormatoMonedaGot(txtVentasGravadas)
    End Sub

    Private Sub txtVentasGravadas_LostFocus(sender As Object, e As EventArgs) Handles txtVentasGravadas.LostFocus
        Call ValidaFormatoMonedaLost(txtVentasGravadas)
    End Sub

    Private Sub txtIGV_GotFocus(sender As Object, e As EventArgs) Handles txtIGV.GotFocus
        Call ValidaFormatoMonedaGot(txtIGV)
    End Sub

    Private Sub txtIGV_LostFocus(sender As Object, e As EventArgs) Handles txtIGV.LostFocus
        Call ValidaFormatoMonedaLost(txtIGV)
    End Sub

    Private Sub txtImporteTotal_GotFocus(sender As Object, e As EventArgs) Handles txtImporteTotal.GotFocus
        Call ValidaFormatoMonedaGot(txtImporteTotal)
    End Sub

    Private Sub dtpFechaEmision_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaEmision.ValueChanged

    End Sub

    Private Sub txtImporteTotal_LostFocus(sender As Object, e As EventArgs) Handles txtImporteTotal.LostFocus
        Call ValidaFormatoMonedaLost(txtImporteTotal)
    End Sub

    Private Sub dtpFechaEmision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaEmision.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class