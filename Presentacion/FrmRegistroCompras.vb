﻿Imports Negocios
Imports Entidades
Imports Entities
Imports Business
Imports System.Windows.Forms
Public Class FrmRegistroCompras
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodProveedor As Integer = 0
    Dim CantidadPorducto As Decimal = 0
    Dim BERegistroCompra As New BEPurchase
    Dim BEProveedor As New BEProveedor
    Dim BEOrdenCompra As New BEOrdenCompra
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion, 3=RegistroOrdenCompra
    Dim Accion As Boolean = False
    Private Sub FrmRegistroCompras_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview=TRUE
        Call CargaCombos()
        Call CargaMoneda()
        Call CargarTipoComprobante()
        Call CargarMotivoTraslado()
        Call CargarAgencia()
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosCompra()
        ElseIf ModalidadVentana = 3 Then
            Call Limpiar()
            Call InterfaceNuevo()
            Call RegistrarDatosOrdenCompra()
        End If
    End Sub
    Sub InicioOrdenCompra(ByVal oBEOrdenCompra As BEOrdenCompra, ByVal oBEProveedor As BEProveedor, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEOrdenCompra = oBEOrdenCompra
        BEProveedor = oBEProveedor
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub InicioCompras(ByVal oBERegistroCompra As BEPurchase, ByVal oBEProveedor As BEProveedor, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BERegistroCompra = oBERegistroCompra
        BEProveedor = oBEProveedor
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub RegistrarDatosOrdenCompra()
        chbAdjuntar.Checked = True
        btnBuscarProveedor.Enabled = False
        nCodProveedor = BEOrdenCompra.gnCodProve
        txtOrdenCompra.Tag = BEOrdenCompra.gnCodOrdCom
        txtOrdenCompra.Text = BEOrdenCompra.gcNumOrd
        txtObservaciones.Text = BEOrdenCompra.gcObservaciones
        txtTipoCambio.Text = FormatNumber(BEOrdenCompra.gnTipCam, 4)
        txtProveedor.Text = BEProveedor.gcRazSoc
        cmbCondPago.SelectedValue = BEOrdenCompra.gnCondPag
        cmbMoneda.SelectedValue = BEOrdenCompra.gnTipMon
        cmbMotivoTras.SelectedValue = BEOrdenCompra.gcTipMov
        btnGrabar.Focus()
    End Sub
    Sub MostrarDatosCompra()
        txtNumComprobante.Text = BERegistroCompra.cNumComRef
        txtGuia.Text = BERegistroCompra.cNumGuiRef
        txtObservaciones.Text = BERegistroCompra.cObservaciones
        txtTipoCambio.Text = FormatNumber(BERegistroCompra.nTipCam, 4)
        dtpFechaVencimiento.Value = BERegistroCompra.dFecVen
        dtpFechaRegistro.Value = BERegistroCompra.dFecReg
        dtpFechaEmision.Value = BERegistroCompra.dFecEmi
        txtNumCompra.Text = BERegistroCompra.cNumCom
        txtNumCompra.Tag = BERegistroCompra.nCodCom
        cmbAgenciaIngreso.SelectedValue = BERegistroCompra.nCodAgeIng
        txtProveedor.Text = BEProveedor.gcRazSoc
        txtVentasGravadas.Text = FormatNumber(BERegistroCompra.nVenGra, 2)
        txtIGV.Text = FormatNumber(BERegistroCompra.nIGV, 2)
        txtImporteTotal.Text = FormatNumber(BERegistroCompra.nImpTot, 2)
        cmbMotivoTras.SelectedValue = BERegistroCompra.cTipMov
        cmbComprobante.SelectedValue = BERegistroCompra.cTipCom
        cmbCondPago.SelectedValue = BERegistroCompra.nCondPag
        cmbMoneda.SelectedValue = BERegistroCompra.nTipMon
        txtOrdenCompra.Text = BERegistroCompra.cNumOrd
        chbParcial.Checked = BERegistroCompra.bIngPar
        If BERegistroCompra.nCodOrdCom <> 0 Then
            chbAdjuntar.Checked = True
        Else
            chbAdjuntar.Checked = False
        End If
        Call RecuperarDetalle()

        If BERegistroCompra.nEstado = 1 Then
            lblMensaje.Text = "COMPRA REGISTRADO(A) - " + BERegistroCompra._cMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call CalcularTotales(cmbMotivoTras.SelectedValue)
            Call InterfaceGrabar()
        ElseIf BERegistroCompra.nEstado = 2 Then
            lblMensaje.Text = "COMPRA FACTURADO(A) - " + BERegistroCompra._cMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceFacturar()
        ElseIf BERegistroCompra.nEstado = 3 Then
            lblMensaje.Text = "COMPRA ANULADO(A) - " + BERegistroCompra._cMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceFacturar()
        End If
        dtgListado.Focus()
        Call EnfocarFocus(6, 0)
    End Sub
    Sub CargaMoneda()
        Dim obj As New BLCommons
        Dim dt As New DataTable
        dt = obj.MostrarMaestro("60")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
    End Sub
    Sub CargaCombos()
        Dim obj As New BLCommons
        Dim dt As New DataTable
        dt = obj.MostrarMaestroUnico(70, "1,2,3,4,5,6,7,8")
        Call CargaCombo(dt, cmbCondPago)
    End Sub
    Sub CargarAgencia()
        Dim obj As New BLCommons
        Dim dt As New DataTable
        dt = obj.MostrarAgencias(False, MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbAgenciaIngreso)
    End Sub
    Sub CargarTipoComprobante()
        Dim obj As New BLCommons
        Dim dt As New DataTable
        dt = obj.MostrarMaestroUnico(30, "01,03")
        Call CargaCombo(dt, cmbComprobante)
    End Sub
    Sub CargarMotivoTraslado()
        Dim obj As New BLCommons
        Dim dt As New DataTable
        dt = obj.MostrarMaestroUnico(150, "02,03")
        Call CargaCombo(dt, cmbMotivoTras)
    End Sub
    Sub Limpiar()
        dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        dtpFechaEmision.Value = MDIPrincipal.FechaSistema
        CantidadPorducto = 0
        LimpiaGrilla(dtgListado)
        txtOrdenCompra.Text = ""
        txtOrdenCompra.Tag = 0
        lblMensaje.Text = ""
        lblMensaje.ForeColor = Color.Blue
        nCodProveedor = 0
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioCompra, 4)
        txtNumComprobante.Text = ""
        txtProveedor.Text = ""
        txtGuia.Text = ""
        txtNumCompra.Text = ""
        txtNumCompra.Tag = 0
        cmbCondPago.SelectedValue = 1
        txtObservaciones.Text = ""
        txtVentasGravadas.Text = "0.00"
        txtIGV.Text = "0.00"
        txtImporteTotal.Text = "0.00"
        chbParcial.Checked = False
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)

    End Sub

    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub dtpFechaVencimiento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaVencimiento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaVencimiento_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaVencimiento.ValueChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub cmbCondPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbCondPago.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbCondPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCondPago.SelectedIndexChanged

    End Sub

    Private Sub cmbComprobante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbComprobante.SelectedIndexChanged

    End Sub

    Private Sub cmbComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbComprobante.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Sub InterfaceNuevo()
        cmbAgenciaIngreso.Enabled = True
        chbParcial.Enabled = True
        chbAdjuntar.Enabled = False
        dtgListado.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        txtObservaciones.Enabled = True
        txtOrdenCompra.Enabled = True
        txtTipoCambio.Enabled = False
        cmbComprobante.Enabled = True
        cmbCondPago.Enabled = True
        cmbMoneda.Enabled = True
        cmbMotivoTras.Enabled = True
        txtNumComprobante.Enabled = True
        txtGuia.Enabled = True
        dtpFechaEmision.Enabled = True
        btnBuscarProveedor.Enabled = True
        dtpFechaRegistro.Enabled = True
        dtpFechaVencimiento.Enabled = True
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If nCodProveedor = 0 Then
            MsgBox("Debe registrar el proveedor ", MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If
        Dim oDatos As New BEPurchase
        cMensaje = ""
        oDatos.nCodProve = nCodProveedor
        oDatos.dFecReg = dtpFechaRegistro.Value
        oDatos.dFecVen = dtpFechaVencimiento.Value
        oDatos.dFecEmi = dtpFechaEmision.Value
        oDatos.nCondPag = cmbCondPago.SelectedValue
        oDatos.nTipCam = txtTipoCambio.Text
        oDatos.cTipCom = cmbComprobante.SelectedValue
        oDatos.nTipMon = cmbMoneda.SelectedValue
        oDatos.cTipMov = cmbMotivoTras.SelectedValue
        oDatos.nCodAgeIng = cmbAgenciaIngreso.SelectedValue
        oDatos.cNumComRef = txtNumComprobante.Text
        oDatos.cNumGuiRef = txtGuia.Text
        oDatos.nCodOrdCom = txtOrdenCompra.Tag
        oDatos.cTipDocAdj = IIf(chbAdjuntar.Checked = True, "07", 0)
        oDatos.cObservaciones = txtObservaciones.Text
        oDatos.cUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.nCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.bIngPar = chbParcial.Checked
        oDatos.nCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.cCodPer = MDIPrincipal.CodigoPersonal
        oDatos.bServicio = False
        Dim BLPurchase As New BLPurchase
        If BLPurchase.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
            txtNumCompra.Tag = oDatos.nCodCom
            txtNumCompra.Text = oDatos.cNumCom
            lblMensaje.Text = "COMPRA REGISTRADA"
            Call RecuperarDetalle()
            Call CalcularTotales(cmbMotivoTras.SelectedValue)
            Call InterfaceGrabar()
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        dtgListado.Columns("nCantidad").ReadOnly = Valor
        dtgListado.Columns("nPrecio").ReadOnly = Valor
    End Sub
    Sub InterfaceGrabar()
        cmbAgenciaIngreso.Enabled = False
        Call BloquearEdicionGrilla(False)
        chbParcial.Enabled = False
        chbAdjuntar.Enabled = False
        dtgListado.Enabled = True
        btnAgregar.Enabled = True
        btnEliminar.Enabled = True
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        txtOrdenCompra.Enabled = False
        txtObservaciones.Enabled = False
        txtTipoCambio.Enabled = False
        cmbComprobante.Enabled = False
        cmbCondPago.Enabled = False
        cmbMoneda.Enabled = False
        cmbMotivoTras.Enabled = False
        txtNumComprobante.Enabled = False
        txtGuia.Enabled = False
        dtpFechaEmision.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
    End Sub
    Sub InterfaceFacturar()
        cmbAgenciaIngreso.Enabled = False
        Call BloquearEdicionGrilla(True)
        chbParcial.Enabled = False
        chbAdjuntar.Enabled = False
        dtgListado.Enabled = True
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtTipoCambio.Enabled = False
        cmbComprobante.Enabled = False
        cmbCondPago.Enabled = False
        cmbMotivoTras.Enabled = False
        cmbMoneda.Enabled = False
        txtNumComprobante.Enabled = False
        txtObservaciones.Enabled = False
        txtGuia.Enabled = False
        dtpFechaEmision.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        FrmConsultaProducto.Inicio(txtNumCompra.Tag, "80")
        FrmConsultaProducto = Nothing

        Call CalcularTotales(cmbMotivoTras.SelectedValue)
        dtgListado.Focus()
        Call EnfocarFocus(6, dtgListado.Rows.Count - 1)
    End Sub
    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If Fila <> 0 Then
            Fila = Fila - 1
        End If
        Call EliminarProductoDetalle(txtNumCompra.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value)
        Call RecuperarDetalle()
        Call CalcularTotales(cmbMotivoTras.SelectedValue)
        dtgListado.Focus()
        Call EnfocarFocus(6, Fila)
    End Sub
    Sub EliminarProductoDetalle(ByVal CodCot As Integer, ByVal nCodProd As Integer)
        Dim BLPurchase As New BLPurchase
        cMensaje = ""
        If BLPurchase.EliminarProducto(CodCot, nCodProd, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Public Sub RecuperarDetalle()
        Dim dt As New DataTable
        Dim BLPurchase As New BLPurchase
        dt = BLPurchase.RecuperarDetalle(txtNumCompra.Tag)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Sub CalcularTotales(ByVal Codigo As String)
        Dim ValorVenta, TOTAL, IGV As Double
        If dtgListado.Rows.Count > 0 Then
            TOTAL = 0
            ValorVenta = 0
            IGV = 0
            If Codigo = "02" Then
                For i = 0 To dtgListado.Rows.Count - 1
                    ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
                Next
                txtVentasGravadas.Text = FormatNumber(ValorVenta, 2)
                IGV = FormatNumber(ValorVenta * 0.18, 2)
                txtIGV.Text = FormatNumber(IGV, 2)
                txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
            Else
                For i = 0 To dtgListado.Rows.Count - 1
                    ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
                    TOTAL = TOTAL + CDbl(dtgListado.Rows(i).Cells("nSubTot").Value)
                Next
                IGV = FormatNumber(TOTAL - ValorVenta, 2)
                txtVentasGravadas.Text = FormatNumber(ValorVenta, 2)
                txtIGV.Text = FormatNumber(IGV, 2)
                txtImporteTotal.Text = FormatNumber(TOTAL, 2)
            End If

        Else
            txtVentasGravadas.Text = FormatNumber(0, 2)
            txtIGV.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
        End If
    End Sub
    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar una compra sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            Exit Function
        End If
        Return validaFacturacion
    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BEPurchase
        If validaFacturacion() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea grabar la compra?", MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
            cMensaje = ""
            oDatos.nCodCom = txtNumCompra.Tag
            oDatos.nVenGra = txtVentasGravadas.Text
            oDatos.nIGV = txtIGV.Text
            oDatos.nImpTot = txtImporteTotal.Text
            oDatos.cUsuActAud = MDIPrincipal.CodUsuario
            oDatos.cCodPer = MDIPrincipal.CodigoPersonal
            Dim BLPurchase As New BLPurchase
            If BLPurchase.Facturar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                MsgBox("La compra N°:" + txtNumCompra.Text + " fue grabado correctamente", MsgBoxStyle.Information, "MENSAJE")
                Accion = True
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub
    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If IsDBNull(dtgListado.CurrentRow.Cells("nCantidad").Value) Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        If dtgListado.CurrentRow.Cells("nCantidad").Value = "0" Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        cMensaje = ""
        Dim BLPurchase As New BLPurchase
        If BLPurchase.ModificarProducto(txtNumCompra.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value,
                                                  dtgListado.CurrentRow.Cells("nCantidad").Value,
                                                    CantidadPorducto,
                                                  dtgListado.CurrentRow.Cells("nPrecio").Value, cMensaje) Then
            If (cMensaje.Trim).Length > 0 Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            LimpiaGrilla(dtgListado)
            Call RecuperarDetalle()
            Call CalcularTotales(cmbMotivoTras.SelectedValue)
            Call EnfocarFocus(Columna, Fila)
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub EnfocarFocus(ByVal Columna As Integer, ByVal Fila As Integer)
        If dtgListado.Rows.Count > 0 Then
            Me.dtgListado.CurrentCell = Me.dtgListado(Columna, Fila)
            If Fila <> dtgListado.Rows.Count - 1 Then
                SendKeys.Send(“{UP}”)
            End If
            SendKeys.Send(“{TAB}”)
        End If
    End Sub
    Private Sub cmbTipoRef_SelectionChangeCommitted(sender As Object, e As EventArgs)
        Call Limpiar()
        txtTipoCambio.Text = MDIPrincipal.TipoCambioCompra
    End Sub

    Private Sub btnBuscarProveedor_Click(sender As Object, e As EventArgs) Handles btnBuscarProveedor.Click
        Dim BEProveedor As New BEProveedor
        FrmConsultaProveedor.Inicio(BEProveedor)
        FrmConsultaProveedor = Nothing
        If BEProveedor.gnCodProv <> 0 Then
            nCodProveedor = BEProveedor.gnCodProv
            txtProveedor.Text = BEProveedor.gcRazSoc
        Else
            btnBuscarProveedor.Focus()
        End If
    End Sub

    Private Sub dtpFechaEmision_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaEmision.ValueChanged
        dtpFechaVencimiento.Value = dtpFechaEmision.Value
    End Sub

    Private Sub dtgListado_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dtgListado.CellBeginEdit
        CantidadPorducto = dtgListado.CurrentRow.Cells("nCantidad").Value
    End Sub

    Private Sub cmbMotivoTras_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMotivoTras.SelectedIndexChanged

    End Sub

    Private Sub dtpFechaEmision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaEmision.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMotivoTras_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMotivoTras.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtNumComprobante_TextChanged(sender As Object, e As EventArgs) Handles txtNumComprobante.TextChanged

    End Sub

    Private Sub btnBuscarProveedor_ImeModeChanged(sender As Object, e As EventArgs) Handles btnBuscarProveedor.ImeModeChanged
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtGuia_TextChanged(sender As Object, e As EventArgs) Handles txtGuia.TextChanged

    End Sub

    Private Sub txtNumComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumComprobante.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub txtGuia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtGuia.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbAdjuntar_CheckedChanged(sender As Object, e As EventArgs) Handles chbAdjuntar.CheckedChanged

    End Sub

    Private Sub chbParcial_CheckedChanged(sender As Object, e As EventArgs) Handles chbParcial.CheckedChanged

    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbAgenciaIngreso_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAgenciaIngreso.SelectedIndexChanged

    End Sub

    Private Sub chbParcial_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbParcial.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub lblMensaje_Click(sender As Object, e As EventArgs) Handles lblMensaje.Click

    End Sub

    Private Sub cmbAgenciaIngreso_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbAgenciaIngreso.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
        If (e.KeyCode = Keys.Return) Then
            e.Handled = True
            SendKeys.Send(“{TAB}”)
        End If
    End Sub

    Private Sub FrmRegistroCompras_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            Call btnFacturar_Click(sender, e)
        End If
    End Sub

    Private Sub cmbCondPago_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbCondPago.SelectionChangeCommitted
        If cmbCondPago.SelectedValue = 1 Or cmbCondPago.SelectedValue = 2 Then
            dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
        ElseIf cmbCondPago.SelectedValue = 3 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 7, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 4 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 15, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 5 Or cmbCondPago.SelectedValue = 9 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 30, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 6 Or cmbCondPago.SelectedValue = 10 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 45, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 7 Or cmbCondPago.SelectedValue = 11 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 60, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 12 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 75, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 8 Or cmbCondPago.SelectedValue = 13 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 90, dtpFechaRegistro.Value)
        End If
    End Sub

    Private Sub cmbCondPago_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbCondPago.SelectedValueChanged
        If (TypeOf cmbCondPago.SelectedValue IsNot DataRowView) Then
            If cmbCondPago.SelectedValue = 1 Or cmbCondPago.SelectedValue = 2 Then
                dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
            ElseIf cmbCondPago.SelectedValue = 3 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 7, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 4 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 15, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 5 Or cmbCondPago.SelectedValue = 9 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 30, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 6 Or cmbCondPago.SelectedValue = 10 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 45, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 7 Or cmbCondPago.SelectedValue = 11 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 60, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 12 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 75, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 8 Or cmbCondPago.SelectedValue = 13 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 90, dtpFechaRegistro.Value)
            End If
        End If
    End Sub
End Class