﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmRegistroCotizacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbCondPago = New System.Windows.Forms.ComboBox()
        Me.txtNumDoc = New System.Windows.Forms.TextBox()
        Me.txtCliente = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpFechaRegistro = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbMoneda = New System.Windows.Forms.ComboBox()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtpFechaVencimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNumCotizacion = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodCot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nNumOrd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodCat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUndMed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nSubTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPorDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nSubTotNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValVent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtVentasGravadas = New System.Windows.Forms.Label()
        Me.txtIGV = New System.Windows.Forms.Label()
        Me.txtImporteTotal = New System.Windows.Forms.Label()
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.txtDescuento = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtSubTotal = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.txtTiempoEntrega = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnClonar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnBuscarCliente = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.btnFacturar = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtBuscarCodCat = New System.Windows.Forms.TextBox()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1217, 25)
        Me.tlsTitulo.TabIndex = 0
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(168, 22)
        Me.lblTitulo.Text = "REGISTRO DE COTIZACIÓN"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        Me.btnCerrar.ToolTipText = "Cerrar <ESC>"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "N° Documento:"
        '
        'cmbCondPago
        '
        Me.cmbCondPago.BackColor = System.Drawing.Color.White
        Me.cmbCondPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCondPago.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbCondPago.FormattingEnabled = True
        Me.cmbCondPago.Location = New System.Drawing.Point(1025, 56)
        Me.cmbCondPago.Name = "cmbCondPago"
        Me.cmbCondPago.Size = New System.Drawing.Size(178, 22)
        Me.cmbCondPago.TabIndex = 7
        '
        'txtNumDoc
        '
        Me.txtNumDoc.Location = New System.Drawing.Point(116, 56)
        Me.txtNumDoc.Name = "txtNumDoc"
        Me.txtNumDoc.Size = New System.Drawing.Size(147, 22)
        Me.txtNumDoc.TabIndex = 0
        Me.txtNumDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCliente
        '
        Me.txtCliente.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCliente.Location = New System.Drawing.Point(116, 80)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(474, 22)
        Me.txtCliente.TabIndex = 5
        Me.txtCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDireccion
        '
        Me.txtDireccion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDireccion.Location = New System.Drawing.Point(116, 104)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(474, 22)
        Me.txtDireccion.TabIndex = 6
        Me.txtDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 14)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Cliente:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 14)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Dirección:"
        '
        'dtpFechaRegistro
        '
        Me.dtpFechaRegistro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaRegistro.Location = New System.Drawing.Point(487, 32)
        Me.dtpFechaRegistro.Name = "dtpFechaRegistro"
        Me.dtpFechaRegistro.Size = New System.Drawing.Size(103, 22)
        Me.dtpFechaRegistro.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(393, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 14)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Fecha Registro:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(923, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 14)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Condición  Pago:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(719, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 14)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Moneda:"
        '
        'cmbMoneda
        '
        Me.cmbMoneda.BackColor = System.Drawing.Color.White
        Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMoneda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMoneda.FormattingEnabled = True
        Me.cmbMoneda.Location = New System.Drawing.Point(777, 56)
        Me.cmbMoneda.Name = "cmbMoneda"
        Me.cmbMoneda.Size = New System.Drawing.Size(114, 22)
        Me.cmbMoneda.TabIndex = 6
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Location = New System.Drawing.Point(1131, 32)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(72, 22)
        Me.txtTipoCambio.TabIndex = 5
        Me.txtTipoCambio.Text = "0.0000"
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(1050, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 14)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Tipo Cambio:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(369, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(115, 14)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Fecha Vencimiento:"
        '
        'dtpFechaVencimiento
        '
        Me.dtpFechaVencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVencimiento.Location = New System.Drawing.Point(487, 56)
        Me.dtpFechaVencimiento.Name = "dtpFechaVencimiento"
        Me.dtpFechaVencimiento.Size = New System.Drawing.Size(103, 22)
        Me.dtpFechaVencimiento.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 40)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(83, 14)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "N° Cotización:"
        '
        'txtNumCotizacion
        '
        Me.txtNumCotizacion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumCotizacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumCotizacion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtNumCotizacion.Location = New System.Drawing.Point(116, 32)
        Me.txtNumCotizacion.Name = "txtNumCotizacion"
        Me.txtNumCotizacion.Size = New System.Drawing.Size(147, 22)
        Me.txtNumCotizacion.TabIndex = 18
        Me.txtNumCotizacion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodCot, Me.nCodProd, Me.nNumOrd, Me.cCodCat, Me.cDescripcion, Me.cUndMed, Me.nCantidad, Me.nValUni, Me.nPreUni, Me.nSubTot, Me.nPorDes, Me.nMonDes, Me.nSubTotNet, Me.nValVent})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(11, 192)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dtgListado.Size = New System.Drawing.Size(1192, 259)
        Me.dtgListado.TabIndex = 12
        '
        'nCodCot
        '
        Me.nCodCot.DataPropertyName = "nCodCot"
        Me.nCodCot.HeaderText = "nCodCot"
        Me.nCodCot.Name = "nCodCot"
        Me.nCodCot.ReadOnly = True
        Me.nCodCot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodCot.Visible = False
        '
        'nCodProd
        '
        Me.nCodProd.DataPropertyName = "nCodProd"
        Me.nCodProd.HeaderText = "nCodProd"
        Me.nCodProd.Name = "nCodProd"
        Me.nCodProd.ReadOnly = True
        Me.nCodProd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodProd.Visible = False
        '
        'nNumOrd
        '
        Me.nNumOrd.DataPropertyName = "nNumOrd"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.nNumOrd.DefaultCellStyle = DataGridViewCellStyle2
        Me.nNumOrd.HeaderText = "N°"
        Me.nNumOrd.Name = "nNumOrd"
        Me.nNumOrd.ReadOnly = True
        Me.nNumOrd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nNumOrd.Width = 35
        '
        'cCodCat
        '
        Me.cCodCat.DataPropertyName = "cCodCat"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cCodCat.DefaultCellStyle = DataGridViewCellStyle3
        Me.cCodCat.HeaderText = "Cod. Cat"
        Me.cCodCat.Name = "cCodCat"
        Me.cCodCat.ReadOnly = True
        Me.cCodCat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cCodCat.Width = 120
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cDescripcion.DefaultCellStyle = DataGridViewCellStyle4
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cDescripcion.Width = 300
        '
        'cUndMed
        '
        Me.cUndMed.DataPropertyName = "cUndMed"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cUndMed.DefaultCellStyle = DataGridViewCellStyle5
        Me.cUndMed.HeaderText = "Und"
        Me.cUndMed.Name = "cUndMed"
        Me.cUndMed.ReadOnly = True
        Me.cUndMed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cUndMed.Width = 80
        '
        'nCantidad
        '
        Me.nCantidad.DataPropertyName = "nCantidad"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = "1.00"
        Me.nCantidad.DefaultCellStyle = DataGridViewCellStyle6
        Me.nCantidad.HeaderText = "Cantidad"
        Me.nCantidad.Name = "nCantidad"
        Me.nCantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCantidad.Width = 80
        '
        'nValUni
        '
        Me.nValUni.DataPropertyName = "nValUni"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N6"
        DataGridViewCellStyle7.NullValue = "0.000000"
        Me.nValUni.DefaultCellStyle = DataGridViewCellStyle7
        Me.nValUni.HeaderText = "Valor.Uni"
        Me.nValUni.Name = "nValUni"
        Me.nValUni.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nPreUni
        '
        Me.nPreUni.DataPropertyName = "nPreUni"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N6"
        Me.nPreUni.DefaultCellStyle = DataGridViewCellStyle8
        Me.nPreUni.HeaderText = "Precio.Uni"
        Me.nPreUni.Name = "nPreUni"
        Me.nPreUni.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nSubTot
        '
        Me.nSubTot.DataPropertyName = "nSubTot"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.nSubTot.DefaultCellStyle = DataGridViewCellStyle9
        Me.nSubTot.HeaderText = "Sub Total"
        Me.nSubTot.Name = "nSubTot"
        Me.nSubTot.ReadOnly = True
        Me.nSubTot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nPorDes
        '
        Me.nPorDes.DataPropertyName = "nPorDes"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N5"
        DataGridViewCellStyle10.NullValue = "0.00000"
        Me.nPorDes.DefaultCellStyle = DataGridViewCellStyle10
        Me.nPorDes.HeaderText = "Desc.(%)"
        Me.nPorDes.Name = "nPorDes"
        Me.nPorDes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nPorDes.Width = 66
        '
        'nMonDes
        '
        Me.nMonDes.DataPropertyName = "nMonDes"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = "0.00"
        Me.nMonDes.DefaultCellStyle = DataGridViewCellStyle11
        Me.nMonDes.HeaderText = "Monto Desc."
        Me.nMonDes.Name = "nMonDes"
        Me.nMonDes.ReadOnly = True
        Me.nMonDes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nMonDes.Width = 90
        '
        'nSubTotNet
        '
        Me.nSubTotNet.DataPropertyName = "nSubTotNet"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.nSubTotNet.DefaultCellStyle = DataGridViewCellStyle12
        Me.nSubTotNet.HeaderText = "Sub Total Neto"
        Me.nSubTotNet.Name = "nSubTotNet"
        Me.nSubTotNet.ReadOnly = True
        Me.nSubTotNet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nValVent
        '
        Me.nValVent.DataPropertyName = "nValVent"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.nValVent.DefaultCellStyle = DataGridViewCellStyle13
        Me.nValVent.HeaderText = "Valor Venta"
        Me.nValVent.Name = "nValVent"
        Me.nValVent.ReadOnly = True
        Me.nValVent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nValVent.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(468, 464)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(101, 14)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Ventas Gravadas:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(775, 464)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(31, 14)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "IGV:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(997, 464)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(87, 14)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Importe Total:"
        '
        'txtVentasGravadas
        '
        Me.txtVentasGravadas.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtVentasGravadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVentasGravadas.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtVentasGravadas.Location = New System.Drawing.Point(573, 458)
        Me.txtVentasGravadas.Name = "txtVentasGravadas"
        Me.txtVentasGravadas.Size = New System.Drawing.Size(123, 22)
        Me.txtVentasGravadas.TabIndex = 35
        Me.txtVentasGravadas.Text = "0.00"
        Me.txtVentasGravadas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIGV
        '
        Me.txtIGV.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtIGV.Location = New System.Drawing.Point(810, 458)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.Size = New System.Drawing.Size(97, 22)
        Me.txtIGV.TabIndex = 36
        Me.txtIGV.Text = "0.00"
        Me.txtIGV.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotal.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtImporteTotal.Location = New System.Drawing.Point(1088, 458)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(115, 22)
        Me.txtImporteTotal.TabIndex = 37
        Me.txtImporteTotal.Text = "0.00"
        Me.txtImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMensaje
        '
        Me.lblMensaje.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblMensaje.Location = New System.Drawing.Point(547, 167)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(656, 22)
        Me.lblMensaje.TabIndex = 123
        Me.lblMensaje.Text = "MENSAJE DE ESTADO"
        Me.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescuento
        '
        Me.txtDescuento.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescuento.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtDescuento.Location = New System.Drawing.Point(310, 458)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(85, 22)
        Me.txtDescuento.TabIndex = 139
        Me.txtDescuento.Text = "0.00"
        Me.txtDescuento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(231, 464)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(75, 14)
        Me.Label22.TabIndex = 138
        Me.Label22.Text = "Descuentos:"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotal.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtSubTotal.Location = New System.Drawing.Point(76, 458)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.Size = New System.Drawing.Size(111, 22)
        Me.txtSubTotal.TabIndex = 137
        Me.txtSubTotal.Text = "0.00"
        Me.txtSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(8, 464)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(64, 14)
        Me.Label20.TabIndex = 136
        Me.Label20.Text = "Sub Total:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(685, 134)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(89, 14)
        Me.Label18.TabIndex = 147
        Me.Label18.Text = "Observaciones:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservaciones.Location = New System.Drawing.Point(777, 81)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservaciones.Size = New System.Drawing.Size(426, 69)
        Me.txtObservaciones.TabIndex = 8
        '
        'txtTiempoEntrega
        '
        Me.txtTiempoEntrega.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTiempoEntrega.Location = New System.Drawing.Point(116, 128)
        Me.txtTiempoEntrega.Name = "txtTiempoEntrega"
        Me.txtTiempoEntrega.Size = New System.Drawing.Size(474, 22)
        Me.txtTiempoEntrega.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 136)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(99, 14)
        Me.Label10.TabIndex = 150
        Me.Label10.Text = "Tiempo Entrega:"
        '
        'btnClonar
        '
        Me.btnClonar.FlatAppearance.BorderSize = 0
        Me.btnClonar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnClonar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnClonar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClonar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClonar.Image = Global.Presentacion.My.Resources.Resources.Clonar_22
        Me.btnClonar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClonar.Location = New System.Drawing.Point(590, 508)
        Me.btnClonar.Name = "btnClonar"
        Me.btnClonar.Size = New System.Drawing.Size(75, 26)
        Me.btnClonar.TabIndex = 18
        Me.btnClonar.Text = "Clonar"
        Me.btnClonar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClonar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.FlatAppearance.BorderSize = 0
        Me.btnEditar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEditar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditar.Image = Global.Presentacion.My.Resources.Resources.Edit_20
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.Location = New System.Drawing.Point(510, 508)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(70, 26)
        Me.btnEditar.TabIndex = 17
        Me.btnEditar.Text = "Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnBuscarCliente
        '
        Me.btnBuscarCliente.BackgroundImage = Global.Presentacion.My.Resources.Resources.BuscarArchivo_20
        Me.btnBuscarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBuscarCliente.FlatAppearance.BorderSize = 0
        Me.btnBuscarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarCliente.Location = New System.Drawing.Point(267, 55)
        Me.btnBuscarCliente.Name = "btnBuscarCliente"
        Me.btnBuscarCliente.Size = New System.Drawing.Size(27, 22)
        Me.btnBuscarCliente.TabIndex = 1
        Me.btnBuscarCliente.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 2)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 116
        '
        'btnFacturar
        '
        Me.btnFacturar.FlatAppearance.BorderSize = 0
        Me.btnFacturar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnFacturar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFacturar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFacturar.Image = Global.Presentacion.My.Resources.Resources.Facturar_22
        Me.btnFacturar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFacturar.Location = New System.Drawing.Point(1106, 508)
        Me.btnFacturar.Name = "btnFacturar"
        Me.btnFacturar.Size = New System.Drawing.Size(97, 26)
        Me.btnFacturar.TabIndex = 15
        Me.btnFacturar.Text = "Grabar(F3)"
        Me.btnFacturar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFacturar.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(11, 508)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(118, 26)
        Me.btnGrabar.TabIndex = 14
        Me.btnGrabar.Text = "Registrar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEliminar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Image = Global.Presentacion.My.Resources.Resources.Delete_20
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(238, 167)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(79, 22)
        Me.btnEliminar.TabIndex = 11
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAgregar.FlatAppearance.BorderSize = 0
        Me.btnAgregar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAgregar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Image = Global.Presentacion.My.Resources.Resources.Add_20
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(149, 167)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(83, 22)
        Me.btnAgregar.TabIndex = 10
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'txtBuscarCodCat
        '
        Me.txtBuscarCodCat.Location = New System.Drawing.Point(11, 167)
        Me.txtBuscarCodCat.Name = "txtBuscarCodCat"
        Me.txtBuscarCodCat.Size = New System.Drawing.Size(133, 22)
        Me.txtBuscarCodCat.TabIndex = 9
        Me.txtBuscarCodCat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'FrmRegistroCotizacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1217, 548)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtBuscarCodCat)
        Me.Controls.Add(Me.btnClonar)
        Me.Controls.Add(Me.txtTiempoEntrega)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.txtDescuento)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.txtSubTotal)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.btnBuscarCliente)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.btnFacturar)
        Me.Controls.Add(Me.txtImporteTotal)
        Me.Controls.Add(Me.txtIGV)
        Me.Controls.Add(Me.txtVentasGravadas)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNumCotizacion)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.dtpFechaVencimiento)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmbMoneda)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpFechaRegistro)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.txtNumDoc)
        Me.Controls.Add(Me.cmbCondPago)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmRegistroCotizacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbCondPago As System.Windows.Forms.ComboBox
    Friend WithEvents txtNumDoc As System.Windows.Forms.TextBox
    Friend WithEvents txtCliente As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaRegistro As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents txtTipoCambio As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaVencimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNumCotizacion As System.Windows.Forms.Label
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents txtVentasGravadas As Label
    Friend WithEvents txtIGV As Label
    Friend WithEvents txtImporteTotal As Label
    Friend WithEvents btnFacturar As System.Windows.Forms.Button
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents lblMensaje As Label
    Friend WithEvents btnBuscarCliente As Button
    Friend WithEvents txtDescuento As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents txtSubTotal As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents txtObservaciones As TextBox
    Friend WithEvents btnEditar As Button
    Friend WithEvents txtTiempoEntrega As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents btnClonar As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents nCodCot As DataGridViewTextBoxColumn
    Friend WithEvents nCodProd As DataGridViewTextBoxColumn
    Friend WithEvents nNumOrd As DataGridViewTextBoxColumn
    Friend WithEvents cCodCat As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents cUndMed As DataGridViewTextBoxColumn
    Friend WithEvents nCantidad As DataGridViewTextBoxColumn
    Friend WithEvents nValUni As DataGridViewTextBoxColumn
    Friend WithEvents nPreUni As DataGridViewTextBoxColumn
    Friend WithEvents nSubTot As DataGridViewTextBoxColumn
    Friend WithEvents nPorDes As DataGridViewTextBoxColumn
    Friend WithEvents nMonDes As DataGridViewTextBoxColumn
    Friend WithEvents nSubTotNet As DataGridViewTextBoxColumn
    Friend WithEvents nValVent As DataGridViewTextBoxColumn
    Friend WithEvents txtBuscarCodCat As TextBox
End Class
