﻿Imports Negocios
Imports Business
Imports Entidades
Imports Entities
Imports System.Configuration

Public Class FrmRegistroCotizacion
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodCli As Integer = 0
    Dim BECotizacion As New BECotizacion
    Dim BEClientes As New BECustomer
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion,
    Dim Accion As Boolean = False
    Private Sub FrmRegistroCotizacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosCotizacion()
        End If
    End Sub
    Sub InicioCotizacion(ByVal oBECotizacion As BECotizacion, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BECotizacion = oBECotizacion
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosCotizacion()
        txtNumDoc.Text = BEClientes.cNumDoc
        txtTipoCambio.Text = FormatNumber(BECotizacion.gnTipCam, 4)
        txtCliente.Text = BEClientes.cRazSoc
        txtDireccion.Text = BEClientes.cDirCli
        dtpFechaRegistro.Value = BECotizacion.gdFecReg
        dtpFechaVencimiento.Value = BECotizacion.gdFecVen
        txtNumCotizacion.Text = BECotizacion.gcNumCot
        txtNumCotizacion.Tag = BECotizacion.gnCodCot
        txtSubTotal.Text = FormatNumber(BECotizacion.gnSubTot, 2)
        txtDescuento.Text = FormatNumber(BECotizacion.gnMonDes, 2)
        txtVentasGravadas.Text = FormatNumber(BECotizacion.gnVenGra, 2)
        txtIGV.Text = FormatNumber(BECotizacion.gnIGV, 2)
        txtImporteTotal.Text = FormatNumber(BECotizacion.gnImpTot, 2)
        cmbCondPago.SelectedValue = BECotizacion.gnCondPag
        cmbMoneda.SelectedValue = BECotizacion.gnTipMon
        txtTiempoEntrega.Text = BECotizacion.gcTieEnt
        txtObservaciones.Text = BECotizacion.gcObservaciones
        nCodCli = BECotizacion.gnCodCli
        Call RecuperarDetalle()

        If BECotizacion.gnEstado = 1 Then
            lblMensaje.Text = "COTIZACIÓN REGISTRADO(A) - " + BECotizacion.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call CalcularTotales()
            Call InterfaceGrabar()
            Call EnfocarFocus(6, 0)
            txtBuscarCodCat.Select()
        ElseIf BECotizacion.gnEstado = 2 Then
            lblMensaje.Text = "COTIZACIÓN FACTURADO(A) - " + BECotizacion.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceFacturar()
            Call EnfocarFocus(6, 0)
            dtgListado.Select()
        ElseIf BECotizacion.gnEstado = 3 Then
            lblMensaje.Text = "COTIZACIÓN ANULADO(A) - " + BECotizacion.gcMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceRelacionar()
            btnClonar.Enabled = False
            Call EnfocarFocus(6, 0)
            dtgListado.Select()
        ElseIf BECotizacion.gnEstado = 4 Then
            lblMensaje.Text = "COTIZACIÓN RELACIONADO(A) - " + BECotizacion.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceRelacionar()
            Call EnfocarFocus(6, 0)
            dtgListado.Select()
        End If
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60,70")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
        Call CargaCombo(CreaDatoCombos(dt, 70), cmbCondPago)
    End Sub
    Sub Limpiar()
        dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        lblMensaje.Text = ""
        lblMensaje.ForeColor = Color.DodgerBlue
        nCodCli = 0
        txtNumDoc.Clear()
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        txtCliente.Text = ""
        txtDireccion.Text = ""
        txtNumCotizacion.Text = ""
        txtNumCotizacion.Tag = 0
        txtVentasGravadas.Text = "0.00"
        txtSubTotal.Text = "0.00"
        txtDescuento.Text = "0.00"
        txtIGV.Text = "0.00"
        txtImporteTotal.Text = "0.00"
        txtObservaciones.Clear()
        txtTiempoEntrega.Clear()
        cmbMoneda.SelectedValue = MDIPrincipal.CodigoMonedaPrecio
        Call LimpiaGrilla(dtgListado)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtNumDoc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumDoc.KeyPress
        Call ValidaEspacio(e)
        Call ValidaNumeros(e)
        Call ValidaSonidoEnter(e)
        If e.KeyChar = Chr(13) Then
            Call ObtenerCliente()
        End If
    End Sub
    Sub ObtenerCliente()
        Dim dt As DataTable
        Dim BLCliente As New BLCliente
        dt = BLCliente.Obtener(txtNumDoc.Text, 2)
        If dt.Rows.Count > 0 Then
            txtCliente.Text = ObtenerDato("Cliente", dt)
            txtDireccion.Text = ObtenerDato("Direccion", dt)
            nCodCli = ObtenerDato("nCodCli", dt)
        Else
            Dim TipoDocumento As Integer = 0
            If txtNumDoc.Text.Trim.Length = 8 Then
                TipoDocumento = 1
            ElseIf txtNumDoc.Text.Trim.Length = 11 Then
                TipoDocumento = 6
            Else
                MsgBox("Ingrese un número de documento válido", MsgBoxStyle.Exclamation, "MENSAJE")
                TipoDocumento = 0
                nCodCli = 0
                txtCliente.Text = ""
                txtDireccion.Text = ""
                txtNumDoc.Focus()
                Exit Sub
            End If
            Dim Cliente As New BECustomer
            cMensaje = ConsultarCliente(Cliente, txtNumDoc.Text.Trim, TipoDocumento)
            If cMensaje = "" Then
                Dim oDatos As New BECustomer
                txtCliente.Text = Cliente.cRazSoc
                txtDireccion.Text = Cliente.cDirCli
                oDatos.bAgeRet = Cliente.bAgeRet
                oDatos.cContactos = ""
                oDatos.cApeMat = ""
                oDatos.cApePat = ""
                oDatos.cNombres = ""
                oDatos.cCorreo = ""
                oDatos.cDirCli = txtDireccion.Text
                oDatos.cNumDoc = txtNumDoc.Text.Trim
                oDatos.cRazSoc = txtCliente.Text
                oDatos.cTipDocCli = Cliente.cTipDocCli
                oDatos.cCodUbiDep = Cliente.cCodUbiDep
                oDatos.cCodUbiDis = Cliente.cCodUbiDis
                oDatos.cCodUbiProv = Cliente.cCodUbiProv
                oDatos.nTipPer = IIf(TipoDocumento = 1, 1, IIf(Microsoft.VisualBasic.Left(txtNumDoc.Text.Trim, 1) = 1, 1, 2))
                oDatos.nTipCli = 0
                If Cliente.cTipDocCli = "1" Then
                    oDatos.cApeMat = Cliente.cApeMat
                    oDatos.cApePat = Cliente.cApePat
                    oDatos.cNombres = Cliente.cNombres
                End If
                cMensaje = ""
                Dim Obj As New BLCliente
                If Obj.Grabar(oDatos, True, cMensaje) = True Then
                    If cMensaje <> "" Then
                        nCodCli = 0
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        Exit Sub
                    End If
                    nCodCli = oDatos.nCodCli
                Else
                    nCodCli = 0
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                nCodCli = 0
                txtCliente.Text = ""
                txtDireccion.Text = ""
                txtNumDoc.Focus()
            End If
        End If
    End Sub

    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)

    End Sub

    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub dtpFechaVencimiento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaVencimiento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaVencimiento_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaVencimiento.ValueChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub cmbCondPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbCondPago.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbCondPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCondPago.SelectedIndexChanged

    End Sub
    Sub InterfaceNuevo()
        dtgListado.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        txtNumDoc.Enabled = True
        btnBuscarCliente.Enabled = True
        txtTipoCambio.Enabled = False
        cmbCondPago.Enabled = True
        cmbMoneda.Enabled = True
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = True
        btnEditar.Enabled = False
        btnClonar.Enabled = False
        txtObservaciones.Enabled = True
        txtTiempoEntrega.Enabled = True
        txtBuscarCodCat.Enabled = False
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If nCodCli = 0 Then
            MsgBox("Debe registrar el cliente ", MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If
        Dim oDatos As New BECotizacion
        cMensaje = ""
        oDatos.gnCodCli = nCodCli
        oDatos.gdFecReg = dtpFechaRegistro.Value
        oDatos.gdFecVen = dtpFechaVencimiento.Value
        oDatos.gnCondPag = cmbCondPago.SelectedValue
        oDatos.gnTipCam = txtTipoCambio.Text
        oDatos.gnTipMon = cmbMoneda.SelectedValue
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.gcTieEnt = txtTiempoEntrega.Text
        oDatos.gcObservaciones = txtObservaciones.Text
        oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
        Dim Obj As New BLCotizacion
        If Obj.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
            txtNumCotizacion.Text = oDatos.gcNumCot
            txtNumCotizacion.Tag = oDatos.gnCodCot
            lblMensaje.Text = "COTIZACIÓN REGISTRADA"
            Call InterfaceGrabar()
            txtBuscarCodCat.Select()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        If MDIPrincipal.CodigoPrecioLista = 1 Then
            dtgListado.Columns("nValUni").ReadOnly = True
            dtgListado.Columns("nPreUni").ReadOnly = Valor
        Else
            dtgListado.Columns("nValUni").ReadOnly = Valor
            dtgListado.Columns("nPreUni").ReadOnly = True
        End If
        dtgListado.Columns("nCantidad").ReadOnly = Valor
        dtgListado.Columns("nPorDes").ReadOnly = Valor
        dtgListado.Columns("cDescripcion").ReadOnly = Valor
    End Sub
    Sub InterfaceGrabar()
        Call BloquearEdicionGrilla(False)
        dtgListado.Enabled = True
        btnAgregar.Enabled = True
        btnEliminar.Enabled = True
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        txtNumDoc.Enabled = True
        btnBuscarCliente.Enabled = True
        txtTipoCambio.Enabled = False
        cmbCondPago.Enabled = True
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
        btnEditar.Enabled = False
        btnClonar.Enabled = False
        txtObservaciones.Enabled = False
        txtTiempoEntrega.Enabled = False
        txtBuscarCodCat.Enabled = True
    End Sub
    Sub InterfaceFacturar()
        Call BloquearEdicionGrilla(True)
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        txtTipoCambio.Enabled = False
        cmbCondPago.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
        dtgListado.Enabled = True
        btnEditar.Enabled = True
        btnClonar.Enabled = True
        txtObservaciones.Enabled = False
        txtTiempoEntrega.Enabled = False
        txtBuscarCodCat.Enabled = False
    End Sub
    Sub InterfaceRelacionar()
        Call BloquearEdicionGrilla(True)
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        txtTipoCambio.Enabled = False
        cmbCondPago.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
        dtgListado.Enabled = True
        btnEditar.Enabled = False
        btnClonar.Enabled = True
        txtObservaciones.Enabled = False
        txtTiempoEntrega.Enabled = False
        txtBuscarCodCat.Enabled = False
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        FrmConsultaProducto.Inicio(txtNumCotizacion.Tag, "02")
        FrmConsultaProducto = Nothing
        Call CalcularTotales()
        Call EnfocarFocus(6, dtgListado.Rows.Count - 1)
        dtgListado.Select()
    End Sub
    Sub CalcularTotales()
        Dim ValorVenta, TOTAL, IGV, DESCUENTO As Double
        If dtgListado.Rows.Count > 0 Then
            ValorVenta = 0
            TOTAL = 0
            IGV = 0
            DESCUENTO = 0
            For i = 0 To dtgListado.Rows.Count - 1
                ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
                TOTAL = TOTAL + CDbl(dtgListado.Rows(i).Cells("nSubTot").Value)
                DESCUENTO = DESCUENTO + (CDbl(dtgListado.Rows(i).Cells("nMonDes").Value))
            Next
            txtSubTotal.Text = FormatNumber(ValorVenta, 2)
            txtDescuento.Text = FormatNumber(DESCUENTO, 2)
            txtVentasGravadas.Text = FormatNumber(ValorVenta - DESCUENTO, 2)
            If DESCUENTO > 0 Then
                ValorVenta = txtVentasGravadas.Text
                IGV = FormatNumber(ValorVenta * 0.18, 2)
                txtIGV.Text = FormatNumber(IGV, 2)
                txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
            Else
                If MDIPrincipal.CodigoPrecioLista = 1 Then
                    IGV = FormatNumber(TOTAL - ValorVenta, 2)
                    txtIGV.Text = FormatNumber(IGV, 2)
                    txtImporteTotal.Text = FormatNumber(TOTAL, 2)
                Else
                    IGV = FormatNumber(ValorVenta * 0.18, 2)
                    txtIGV.Text = FormatNumber(IGV, 2)
                    txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
                End If

            End If
        Else
            txtSubTotal.Text = FormatNumber(0, 2)
            txtDescuento.Text = FormatNumber(0, 2)
            txtVentasGravadas.Text = FormatNumber(0, 2)
            txtIGV.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
        End If
    End Sub
    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dtgListado.Rows.Count > 0 Then
            Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
            Call EliminarProductoDetalle(txtNumCotizacion.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value)
            Call RecuperarDetalle()
            Call CalcularTotales()
            If Fila <> 0 Then
                If dtgListado.Rows.Count - 1 < Fila Then
                    Fila = Fila - 1
                End If
            End If
            Call EnfocarFocus(6, Fila)
            dtgListado.Select()
        End If
    End Sub
    Sub EliminarProductoDetalle(ByVal CodCot As Integer, ByVal nCodProd As Integer)
        Dim BLCotizacion As New BLCotizacion
        cMensaje = ""
        If BLCotizacion.EliminarProducto(CodCot, nCodProd, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Public Sub RecuperarDetalle()
        Dim dt As DataTable
        Dim BLCotizacion As New BLCotizacion
        dt = BLCotizacion.RecuperarDetalle(txtNumCotizacion.Tag)
        Call LlenaAGridView(dt, dtgListado)
        If MDIPrincipal.CodigoPrecioLista = 1 Then
            dtgListado.Columns("nValUni").DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192)
            dtgListado.Columns("nPreUni").DefaultCellStyle.BackColor = Color.White
        Else
            dtgListado.Columns("nValUni").DefaultCellStyle.BackColor = Color.White
            dtgListado.Columns("nPreUni").DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192)
        End If
    End Sub
    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar una cotización sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            Exit Function
        End If

        Return validaFacturacion

    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BECotizacion
        If validaFacturacion() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea grabar la cotización?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodCli = nCodCli
            oDatos.gnCodCot = txtNumCotizacion.Tag
            oDatos.gnSubTot = txtSubTotal.Text
            oDatos.gnMonDes = txtDescuento.Text
            oDatos.gnVenGra = txtVentasGravadas.Text
            oDatos.gnIGV = txtIGV.Text
            oDatos.gnImpTot = txtImporteTotal.Text
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            oDatos.gnCondPag = cmbCondPago.SelectedValue
            Dim BLCotizacion As New BLCotizacion
            If BLCotizacion.Facturar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                lblMensaje.Text = "COTIZACIÓN FACTURADA"
                Dim ModeloCotizacion As String = ConfigurationManager.AppSettings("ModeloCotizacion")
                Dim RutaCotizacion As String = ""
                If MDIPrincipal.CodigoEmpresa = 1 Then
                    RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion1")
                ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                    RutaCotizacion = ConfigurationManager.AppSettings("RutaCotizacion2")
                End If
                RutaCotizacion = RutaCotizacion + "COTIZACION N° " + txtNumCotizacion.Text.Trim + " - " + txtCliente.Text.Trim + ".pdf"
                If ModeloCotizacion = "1" Then
                    Call CrearCotizacion_Pdf_TA4_Modelo_1(txtNumCotizacion.Tag)
                    Process.Start(RutaCotizacion)
                ElseIf ModeloCotizacion = "2" Then
                    Call CrearCotizacion_Pdf_TA4_Modelo_2(txtNumCotizacion.Tag)
                    Process.Start(RutaCotizacion)
                ElseIf ModeloCotizacion = "3" Then
                    If MDIPrincipal.CodigoEmpresa = 1 Then
                        Call CrearCotizacion_Pdf_TA4_FERINDMELISSA(txtNumCotizacion.Tag)
                    ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                        Call CrearCotizacionPdf_TA4_GROUPTOOLS(txtNumCotizacion.Tag)
                    End If
                    Process.Start(RutaCotizacion)
                ElseIf ModeloCotizacion = "4" Then
                    Call CrearCotizacion_Pdf_TA4_MAQHER(txtNumCotizacion.Tag)
                    Process.Start(RutaCotizacion)
                ElseIf ModeloCotizacion = "5" Then
                    Call CrearCotizacion_Pdf_TA4_Modelo_1(txtNumCotizacion.Tag)
                    Process.Start(RutaCotizacion)
                End If
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If IsDBNull(dtgListado.CurrentRow.Cells("nCantidad").Value) Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        If dtgListado.CurrentRow.Cells("nCantidad").Value = "0" Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        cMensaje = ""
        Dim Precio As Decimal = IIf(MDIPrincipal.CodigoPrecioLista = 0, dtgListado.CurrentRow.Cells("nValUni").Value, dtgListado.CurrentRow.Cells("nPreUni").Value)
        Dim BLCotizacion As New BLCotizacion
        If BLCotizacion.ModificarProducto(txtNumCotizacion.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value,
                                                dtgListado.CurrentRow.Cells("nCantidad").Value, Precio,
                                          dtgListado.CurrentRow.Cells("nPorDes").Value,
                                          dtgListado.CurrentRow.Cells("cDescripcion").Value, cMensaje) Then
            If (cMensaje.Trim).Length > 0 Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Call LimpiaGrilla(dtgListado)
            Call RecuperarDetalle()
            Call CalcularTotales()
            Call EnfocarFocus(Columna, Fila)
            If Fila <> dtgListado.Rows.Count - 1 Then
                SendKeys.Send(“{UP}”)
            End If
            SendKeys.Send(“{TAB}”)

        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If

    End Sub
    Sub EnfocarFocus(ByVal Columna As Integer, ByVal Fila As Integer)
        If dtgListado.Rows.Count > 0 Then
            Me.dtgListado.CurrentCell = Me.dtgListado(Columna, Fila)
        End If
    End Sub
    Private Sub btnImprimir_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub txtNumDoc_TextChanged(sender As Object, e As EventArgs) Handles txtNumDoc.TextChanged

    End Sub

    Private Sub btnImprimir_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim BEClientes As New BECustomer
        FrmConsultaCliente.Inicio(BEClientes)
        FrmConsultaCliente = Nothing
        If BEClientes.nCodCli <> 0 Then
            nCodCli = BEClientes.nCodCli
            txtCliente.Text = BEClientes.cRazSoc
            txtDireccion.Text = BEClientes.cDirCli
            txtNumDoc.Text = BEClientes.cNumDoc
            dtpFechaRegistro.Focus()
        Else
            btnBuscarCliente.Focus()
        End If
    End Sub

    Private Sub btnBuscarCliente_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarCliente.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtTiempoEntrega_TextChanged(sender As Object, e As EventArgs) Handles txtTiempoEntrega.TextChanged

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
        If (e.KeyCode = Keys.Return) Then
            e.Handled = True
            SendKeys.Send(“{TAB}”)
        End If
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub txtTiempoEntrega_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTiempoEntrega.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        Dim Negocio As New BLCotizacion
        Dim oDatos As New BECotizacion
        oDatos.gnCodCot = txtNumCotizacion.Tag
        oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
        oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
        If Negocio.Editar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
        Call InterfaceGrabar()
        Call EnfocarFocus(6, 0)
        dtgListado.Select()

    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress

    End Sub

    Private Sub btnClonar_Click(sender As Object, e As EventArgs) Handles btnClonar.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea duplicar(Clonar) este comprobante?", MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
            Dim oDatos As New BECotizacion
            cMensaje = ""
            oDatos.gnCodCot = txtNumCotizacion.Tag
            oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal

            Dim Obj As New BLCotizacion
            If Obj.Clonar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                txtNumCotizacion.Text = oDatos.gcNumCot
                txtNumCotizacion.Tag = oDatos.gnCodCot
                txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
                dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
                dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
                lblMensaje.Text = "COTIZACIÓN REGISTRADA"
                Call RecuperarDetalle()
                Call InterfaceGrabar()
                btnGrabar.Select()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnEditar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEditar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnClonar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnClonar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub lblMensaje_Click(sender As Object, e As EventArgs) Handles lblMensaje.Click

    End Sub

    Private Sub txtBuscarCodCat_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarCodCat.TextChanged

    End Sub

    Private Sub FrmRegistroCotizacion_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            Call btnFacturar_Click(sender, e)
        End If
    End Sub

    Private Sub txtBuscarCodCat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscarCodCat.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
        If e.KeyChar = Chr(13) Then
            Call ObtenerProducto()
        End If
    End Sub
    Sub ObtenerProducto()
        Dim dt As DataTable
        Dim CodProd As Integer = 0
        Dim BLProductos As New BLProducts
        dt = BLProductos.ObtenerProducto(txtBuscarCodCat.Text, 1, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
        If dt.Rows.Count > 0 Then
            CodProd = ObtenerDato("nCodProd", dt)
            cMensaje = ""
            cMensaje = AgregarProductoDetalle(CodProd, 1, 0)
            If cMensaje <> "" Then
                txtBuscarCodCat.SelectAll()
            Else
                Call RecuperarDetalle()
                Call CalcularTotales()
                txtBuscarCodCat.Clear()
                Call EnfocarFocus(5, dtgListado.Rows.Count - 1)
                txtBuscarCodCat.Select()
            End If
        Else
            MsgBox("El código catálogo registrado no existe", MsgBoxStyle.Exclamation, "MENSAJE")
            txtBuscarCodCat.SelectAll()
        End If
    End Sub
    Function AgregarProductoDetalle(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double) As String
        cMensaje = ""
        Dim Obj As New BLCotizacion
        If Obj.AgregarProducto(txtNumCotizacion.Tag, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
        AgregarProductoDetalle = cMensaje
    End Function

    Private Sub cmbCondPago_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbCondPago.SelectionChangeCommitted

    End Sub

    Private Sub cmbCondPago_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbCondPago.SelectedValueChanged
        If (TypeOf cmbCondPago.SelectedValue IsNot DataRowView) Then
            If cmbCondPago.SelectedValue = 1 Or cmbCondPago.SelectedValue = 2 Then
                dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
            ElseIf cmbCondPago.SelectedValue = 3 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 7, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 4 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 15, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 5 Or cmbCondPago.SelectedValue = 9 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 30, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 6 Or cmbCondPago.SelectedValue = 10 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 45, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 7 Or cmbCondPago.SelectedValue = 11 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 60, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 12 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 75, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 8 Or cmbCondPago.SelectedValue = 13 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 90, dtpFechaRegistro.Value)
            End If
        End If
    End Sub
End Class