﻿Imports Negocios
Imports Business
Imports Entidades
Imports System.Windows.Forms
Public Class FrmRegistroGastos
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim CobroPago As Integer
    Private Sub FrmRegistroGastos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaCombos()
        Call CargaMedioPago()
        Call Listar()
        Call Limpiar()
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60,310,350")
            Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
            Call CargaCombo(CreaDatoCombos(dt, 310), cmbTipoGasto)
            Call CargaCombo(CreaDatoCombos(dt, 350), cmbFondo)

    End Sub
    Sub CargaMedioPago()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(80, "1")
        Call CargaCombo(dt, cmbMedioPago)
    End Sub
    Sub Listar()
        Dim dt As DataTable
        Dim BLGasto As New BLGasto
        dt = BLGasto.Listar(dtpFecIni.Value, dtpFecFin.Value, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Sub Limpiar()
        dtpdFecReg.Text = MDIPrincipal.FechaSistema
        dtpFecIni.Value = MDIPrincipal.FechaSistema
        dtpFecIni.Value = DateAdd(DateInterval.Month, -1, dtpFecIni.Value)
        txtMonto.Text = "0.00"
        txtObservaciones.Text = ""
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Dim oDatos As New BEGasto
        If CDbl(txtMonto.Text) = 0 Then
            MsgBox("Debe registrar el monto de gasto", MsgBoxStyle.Exclamation, "MENSAJE")
            txtMonto.Focus()
            Exit Sub
        End If
        cMensaje = ""
            oDatos.gcUsuReg = MDIPrincipal.CodUsuario
            oDatos.gdFecReg = MDIPrincipal.FechaSistema
            oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
            oDatos.gnMedPag = cmbMedioPago.SelectedValue
            oDatos.gnTipGas = cmbTipoGasto.SelectedValue
            oDatos.gnTipMon = cmbMoneda.SelectedValue
            oDatos.gnMonto = txtMonto.Text
            oDatos.gnFonDesc = cmbFondo.SelectedValue
            oDatos.gcObservaciones = txtObservaciones.Text
            oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.gcNumCom = txtNumCom.Text
        Dim BLGasto As New BLGasto
        If BLGasto.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se registró correctamente", MsgBoxStyle.Information, "MENSAJE")
            Call Limpiar()
            Call Listar()
            cmbTipoGasto.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub


    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub cmbTipoGasto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoGasto.SelectedIndexChanged

    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True

    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoGasto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoGasto.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_TextChanged(sender As Object, e As EventArgs) Handles txtMonto.TextChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress
        Call ValidaDecimales(e, txtMonto)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_GotFocus(sender As Object, e As EventArgs) Handles txtMonto.GotFocus
        Call ValidaFormatoMonedaGot(txtMonto)
    End Sub

    Private Sub cmbMedioPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMedioPago.SelectedIndexChanged

    End Sub

    Private Sub txtMonto_LostFocus(sender As Object, e As EventArgs) Handles txtMonto.LostFocus
        Call ValidaFormatoMonedaLost(txtMonto)
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub cmbMedioPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMedioPago.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbFondo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFondo.SelectedIndexChanged

    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecIni.ValueChanged
        Call Listar()
    End Sub

    Private Sub cmbFondo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbFondo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecFin.ValueChanged
        Call Listar()
    End Sub

    Private Sub dtpFecIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecIni.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        cMensaje = ""
        Dim BLGasto As New BLGasto
        If dtgListado.Rows.Count > 0 Then
                Dim senderGrid = DirectCast(sender, DataGridView)
                If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                    If dtgListado.Columns(e.ColumnIndex).Name = "Eliminar" Then
                        If MsgBox("¿Esta seguro que desea eliminar el gasto?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                            If BLGasto.Eliminar(dtgListado.CurrentRow.Cells("nCodGas").Value, cMensaje) = True Then
                                If cMensaje <> "" Then
                                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                                    Exit Sub
                                End If
                                Call Listar()
                            Else
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                            End If
                        End If
                    End If
                    If dtgListado.Columns(e.ColumnIndex).Name = "Imprimir" Then
                        FrmReportes.ImpresionGasto(dtgListado.CurrentRow.Cells("nCodGas").Value)
                        FrmReportes = Nothing
                    End If
                End If
            End If
    End Sub

    Private Sub txtNumCom_TextChanged(sender As Object, e As EventArgs) Handles txtNumCom.TextChanged

    End Sub

    Private Sub dtpFecFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtNumCom_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumCom.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub
End Class