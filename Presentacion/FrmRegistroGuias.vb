﻿Imports System.Configuration
Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Public Class FrmRegistroGuias
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodCli As Integer = 0
    Dim CantidadPorducto As Decimal = 0
    Dim BEGuia As New BEGuia
    Dim BEPedido As New BEPedido
    Dim BEClientes As New BECustomer
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion, 3=RegistroComprobante
    Dim Accion As Boolean = False
    Private Sub FrmRegistroGuias_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargarTipoReferencia()
        Call CargarMotivoTraslado()
        Call CargarModalidadTraslado()
        Call CargaSeries()
        Call CargaCombos()

        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosGuia()
        ElseIf ModalidadVentana = 3 Then
            Call Limpiar()
            Call InterfaceNuevo()
            Call RegistrarDatosComprobante()
        End If
    End Sub
    Sub InicioFacturacion(ByVal oBEPedido As BEPedido, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEPedido = oBEPedido
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub InicioGuia(ByVal oBEGuia As BEGuia, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEGuia = oBEGuia
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub RegistrarDatosComprobante()
        cmbTipoRef.SelectedValue = BEPedido.gcTipCom
        txtNumDocRef.Text = BEPedido.gcNumComp
        txtNumDocRef.Tag = BEPedido.gnCodPed
        txtNumDoc.Text = BEClientes.cNumDoc
        nCodCli = BEPedido.gnCodCli
        txtCliente.Text = BEClientes.cRazSoc
        txtDireccion.Text = BEClientes.cDirCli
        txtDirPart.Text = MDIPrincipal.DirAgencia
        txtAlmacen.Text = MDIPrincipal.Agencia
        cmbMoneda.SelectedValue = BEPedido.gnTipMon
        chbAdjuntar.Checked = True
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        chbParcial.Enabled = True
        Call CargarDirecciones()
    End Sub
    Sub MostrarDatosGuia()
        txtNumDoc.Text = BEClientes.cNumDoc
        txtCliente.Text = BEClientes.cRazSoc
        txtDireccion.Text = BEClientes.cDirCli
        txtDirPart.Text = BEGuia.gcDirPart
        txtAlmacen.Text = BEGuia.gAlmacen
        cmbTipoRef.SelectedValue = BEGuia.gcTipDocAdj
        cmbMotivoTrasl.SelectedValue = BEGuia.gcMotTrasl
        cmbSerie.SelectedValue = BEGuia.gnCodSer
        txtNumGuia.Text = BEGuia.gcNumGuia
        dtpFechaRegistro.Value = BEGuia.gdFecReg
        dtpFechaTraslado.Value = BEGuia.gdFecTras
        nCodCli = BEGuia.gnCodCli
        Call CargarDirecciones()
        cmbDirLleg.SelectedValue = BEGuia.gnCodDirCli
        txtNumDocRef.Text = BEGuia.gcNumDocAdj
        txtNumDocRef.Tag = BEGuia.gnCodCot
        txtNumGuia.Tag = BEGuia.gnCodGuia
        txtTipDocComp.Text = BEGuia.gcTipoCompAdj
        txtNumDocRefComp.Text = BEGuia.gcNumDocAdj
        cmbMoneda.SelectedValue = BEGuia.gnTipMon
        chbParcial.Checked = BEGuia.gbParcial
        cmbModalidadTraslado.SelectedValue = BEGuia.gcModTras

        If BEGuia.gnCodCot <> 0 Then
            chbAdjuntar.Checked = True
        Else
            chbAdjuntar.Checked = False
            cmbTipoRef.SelectedValue = 0
        End If

        Call RecuperarDetalle()
        If BEGuia.gnEstado = 1 Then
            lblMensaje.Text = "GUIA REGISTRADO(A) - " + BEGuia.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceGrabar()
            Call EnfocarFocus(5, 0)
            txtBuscarCodCat.Select()
        ElseIf BEGuia.gnEstado = 2 Then
            lblMensaje.Text = "GUIA FACTURADO(A) - " + BEGuia.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceFacturar()
            Call EnfocarFocus(5, 0)
            dtgListado.Select()
        ElseIf BEGuia.gnEstado = 3 Then
            lblMensaje.Text = "GUIA ANULADO(A) - " + BEGuia.gcMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceRelacionar()
            Call EnfocarFocus(5, 0)
            dtgListado.Select()
        ElseIf BEGuia.gnEstado = 4 Then
            lblMensaje.Text = "GUIA RELACIONADO(A) - " + BEGuia.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceRelacionar()
            Call EnfocarFocus(5, 0)
            dtgListado.Select()
        End If
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
    End Sub
    Sub CargarDirecciones()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarDireccionesCliente(nCodCli)
        Call CargaCombo(dt, cmbDirLleg)
    End Sub
    Sub CargarTipoReferencia()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(370, "01,03,99")
        Call CargaCombo(dt, cmbTipoRef)
    End Sub
    Sub CargarMotivoTraslado()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(150, "01")
        Call CargaCombo(dt, cmbMotivoTrasl)
    End Sub
    Sub CargarModalidadTraslado()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(260, "02")
        Call CargaCombo(dt, cmbModalidadTraslado)
    End Sub
    Sub Limpiar()
        chbAdjuntar.Checked = False
        chbParcial.Checked = False
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        dtpFechaTraslado.Value = MDIPrincipal.FechaSistema
        LimpiaGrilla(dtgListado)
        txtNumDocRef.Text = ""
        txtNumDocRef.Tag = 0
        CantidadPorducto = 0
        lblMensaje.Text = ""
        lblMensaje.ForeColor = Color.DodgerBlue
        nCodCli = 0
        txtNumDoc.Clear()
        txtCliente.Text = ""
        txtDireccion.Text = ""
        txtNumGuia.Text = ""
        txtNumGuia.Tag = 0
        txtDirPart.Text = ""
        txtNumDocRefComp.Text = ""
        txtTipDocComp.Text = ""
        cmbMotivoTrasl.SelectedValue = "01"
        cmbModalidadTraslado.SelectedValue = "02"
        txtAlmacen.Text = ""
        cmbTipoRef.SelectedValue = 0
        txtAlmacen.Text = MDIPrincipal.Agencia
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtNumDoc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumDoc.KeyPress
        Call ValidaEspacio(e)
        Call ValidaNumeros(e)
        Call ValidaSonidoEnter(e)
        If e.KeyChar = Chr(13) Then
            Call ObtenerCliente()
        End If
    End Sub
    Sub ObtenerCliente()
        Dim dt As DataTable
        Dim BLCliente As New BLCliente
        dt = BLCliente.Obtener(txtNumDoc.Text, 2)
        If dt.Rows.Count > 0 Then
            txtCliente.Text = ObtenerDato("Cliente", dt)
            txtDireccion.Text = ObtenerDato("Direccion", dt)
            nCodCli = ObtenerDato("nCodCli", dt)
            txtDirPart.Text = MDIPrincipal.DirAgencia
            txtAlmacen.Text = MDIPrincipal.Agencia
            Call CargarDirecciones()
        Else
            Dim TipoDocumento As Integer = 0
            If txtNumDoc.Text.Trim.Length = 8 Then
                TipoDocumento = 1
            ElseIf txtNumDoc.Text.Trim.Length = 11 Then
                TipoDocumento = 6
            Else
                MsgBox("Ingrese un número de documento válido", MsgBoxStyle.Exclamation, "MENSAJE")
                TipoDocumento = 0
                nCodCli = 0
                txtCliente.Text = ""
                txtDireccion.Text = ""
                txtNumDoc.Focus()
                Exit Sub
            End If
            Dim Cliente As New BECustomer
            cMensaje = ConsultarCliente(Cliente, txtNumDoc.Text.Trim, TipoDocumento)
            If cMensaje = "" Then
                Dim oDatos As New BECustomer
                txtCliente.Text = Cliente.cRazSoc
                txtDireccion.Text = Cliente.cDirCli
                oDatos.bAgeRet = Cliente.bAgeRet
                oDatos.cContactos = ""
                oDatos.cApeMat = ""
                oDatos.cApePat = ""
                oDatos.cNombres = ""
                oDatos.cCorreo = ""
                oDatos.cDirCli = txtDireccion.Text
                oDatos.cNumDoc = txtNumDoc.Text.Trim
                oDatos.cRazSoc = txtCliente.Text
                oDatos.cTipDocCli = Cliente.cTipDocCli
                oDatos.cCodUbiDep = Cliente.cCodUbiDep
                oDatos.cCodUbiDis = Cliente.cCodUbiDis
                oDatos.cCodUbiProv = Cliente.cCodUbiProv
                oDatos.nTipPer = IIf(TipoDocumento = 1, 1, IIf(Microsoft.VisualBasic.Left(txtNumDoc.Text.Trim, 1) = 1, 1, 2))
                oDatos.nTipCli = 0
                If Cliente.cTipDocCli = "1" Then
                    oDatos.cApeMat = Cliente.cApeMat
                    oDatos.cApePat = Cliente.cApePat
                    oDatos.cNombres = Cliente.cNombres
                End If
                cMensaje = ""
                Dim Obj As New BLCliente
                If Obj.Grabar(oDatos, True, cMensaje) = True Then
                    If cMensaje <> "" Then
                        nCodCli = 0
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        Exit Sub
                    End If
                    nCodCli = oDatos.nCodCli
                Else
                    nCodCli = 0
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                nCodCli = 0
                txtCliente.Text = ""
                txtDireccion.Text = ""
                txtNumDoc.Focus()
            End If
        End If
    End Sub
    Sub InterfaceNuevo()
        chbParcial.Enabled = False
        cmbMoneda.Enabled = False
        cmbMotivoTrasl.Enabled = False
        cmbModalidadTraslado.Enabled = True
        dtgListado.Enabled = False
        txtBuscarCodCat.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        txtNumDoc.Enabled = True
        btnBuscarCliente.Enabled = True
        cmbSerie.Enabled = True
        dtpFechaRegistro.Enabled = False
        dtpFechaTraslado.Enabled = True
        cmbTipoRef.Enabled = False
        cmbDirLleg.Enabled = True
        chbAdjuntar.Enabled = False
    End Sub
    Function ValidaDatos() As Boolean
        ValidaDatos = True
        If nCodCli = 0 Then
            MsgBox("Debe registrar el cliente ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtNumDoc.Select()
            Exit Function
        End If
        If IsNothing(cmbSerie.SelectedValue) Then
            MsgBox("Debe registrar la serie ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbSerie.Select()
            Exit Function
        End If
        If IsNothing(cmbDirLleg.SelectedValue) Then
            MsgBox("Debe registrar la dirección de llegada ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbDirLleg.Select()
            Exit Function
        End If
        If IsNothing(cmbMotivoTrasl.SelectedValue) Then
            MsgBox("Debe registrar el motivo de traslado", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbMotivoTrasl.Select()
            Exit Function
        End If
        If IsNothing(cmbModalidadTraslado.SelectedValue) Then
            MsgBox("Debe registrar la modalidad de traslado", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbModalidadTraslado.Select()
            Exit Function
        End If
    End Function
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Dim GuiaElectronica As String = ConfigurationManager.AppSettings("GuiaElectronica")
        If ValidaDatos() = False Then
            Exit Sub
        End If
        Dim oDatos As New BEGuia
        cMensaje = ""
        oDatos.gnCodCli = nCodCli
        oDatos.gnCodCot = txtNumDocRef.Tag
        oDatos.gdFecReg = dtpFechaRegistro.Value
        oDatos.gcMotTrasl = cmbMotivoTrasl.SelectedValue
        oDatos.gcDirPart = txtDirPart.Text
        oDatos.gnCodDirCli = cmbDirLleg.SelectedValue
        oDatos.gcTipGuia = "09"
        oDatos.gnCodSer = cmbSerie.SelectedValue
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gnTipMon = cmbMoneda.SelectedValue
        oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.gcTipDocAdj = IIf(chbAdjuntar.Checked = True, cmbTipoRef.SelectedValue, 0)
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
        oDatos.gbParcial = chbParcial.Checked
        oDatos.gbElectronico = GuiaElectronica
        oDatos.gdFecTras = dtpFechaTraslado.Value
        oDatos.gcModTras = cmbModalidadTraslado.SelectedValue
        Dim BLGuia As New BLGuia
        If BLGuia.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
            txtNumGuia.Tag = oDatos.gnCodGuia
            lblMensaje.Text = "GUIA REGISTRADA"
            Call RecuperarDetalle()
            Call InterfaceGrabar()
            txtBuscarCodCat.Select()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        If chbAdjuntar.Checked = True Then
            dtgListado.Columns("nCantidad").ReadOnly = True
            dtgListado.Columns("cDescripcion").ReadOnly = True
        Else
            dtgListado.Columns("nCantidad").ReadOnly = Valor
            dtgListado.Columns("cDescripcion").ReadOnly = Valor
        End If
    End Sub
    Sub InterfaceGrabar()
        If chbAdjuntar.Checked = True Then
            btnAgregar.Enabled = False
            txtBuscarCodCat.Enabled = False
            If chbParcial.Checked = True Then
                btnEliminar.Enabled = True
            Else
                btnEliminar.Enabled = False
            End If
        Else
            btnAgregar.Enabled = True
            txtBuscarCodCat.Enabled = True
            btnEliminar.Enabled = True
        End If
        cmbMoneda.Enabled = False
        Call BloquearEdicionGrilla(False)
        dtgListado.Enabled = True
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        cmbMotivoTrasl.Enabled = False
        cmbModalidadTraslado.Enabled = False
        cmbSerie.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaTraslado.Enabled = False
        cmbTipoRef.Enabled = False
        cmbDirLleg.Enabled = False
        chbAdjuntar.Enabled = False
        chbParcial.Enabled = False
    End Sub
    Sub InterfaceFacturar()
        chbParcial.Enabled = False
        cmbMoneda.Enabled = False
        Call BloquearEdicionGrilla(True)
        dtgListado.Enabled = True
        cmbTipoRef.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        cmbMotivoTrasl.Enabled = False
        cmbModalidadTraslado.Enabled = False
        cmbSerie.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaTraslado.Enabled = False
        cmbDirLleg.Enabled = False
        chbAdjuntar.Enabled = False
        txtBuscarCodCat.Enabled = False
    End Sub
    Sub InterfaceRelacionar()
        chbParcial.Enabled = False
        cmbMoneda.Enabled = False
        dtgListado.Enabled = True
        Call BloquearEdicionGrilla(True)
        cmbTipoRef.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        cmbMotivoTrasl.Enabled = False
        cmbModalidadTraslado.Enabled = False
        cmbSerie.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaTraslado.Enabled = False
        cmbDirLleg.Enabled = False
        chbAdjuntar.Enabled = False
        txtBuscarCodCat.Enabled = False
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        FrmConsultaProducto.Inicio(txtNumGuia.Tag, "09")
        FrmConsultaProducto = Nothing
        Call EnfocarFocus(5, dtgListado.Rows.Count - 1)
        dtgListado.Select()
    End Sub
    Function AgregarProductoDetalle(ByVal CodProd As Integer, ByVal Cantidad As Decimal) As String
        cMensaje = ""
        Dim BLGuia As New BLGuia
        If BLGuia.AgregarProducto(txtNumGuia.Tag, CodProd, Cantidad, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
        AgregarProductoDetalle = cMensaje
    End Function

    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnEditar_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dtgListado.Rows.Count > 0 Then
            Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
            Call EliminarProductoDetalle(txtNumGuia.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value)
            Call RecuperarDetalle()
            If Fila <> 0 Then
                If dtgListado.Rows.Count - 1 < Fila Then
                    Fila = Fila - 1
                End If
            End If
            Call EnfocarFocus(5, Fila)
            dtgListado.Select()
        End If
    End Sub
    Sub EliminarProductoDetalle(ByVal CodGuia As Integer, ByVal nCodProd As Integer)
        cMensaje = ""
        Dim BLGuia As New BLGuia
        If BLGuia.EliminarProducto(CodGuia, nCodProd, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Select()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Public Sub RecuperarDetalle()
        Dim dt As DataTable
        Dim BLGuia As New BLGuia
        dt = BLGuia.RecuperarDetalle(txtNumGuia.Tag)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar una guia sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            Exit Function
        End If
        Return validaFacturacion
    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BEGuia
        If validaFacturacion() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea facturar la guia?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodGuia = txtNumGuia.Tag
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            Dim BLGuia As New BLGuia
            If BLGuia.Facturar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                txtNumGuia.Text = oDatos.gcNumGuia
                Dim GuiaElectronica As String = ConfigurationManager.AppSettings("GuiaElectronica")
                If GuiaElectronica = "1" Then
                    cMensaje = ""
                    Dim cNumTickSun As String = ""
                    cMensaje = GenerarGuiaElectronica(txtNumGuia.Tag, cNumTickSun)
                    If cMensaje <> "" Then
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE SUNAT")
                    Else
                        cMensaje = ""
                        cMensaje = ConsultarEstadoEnvioGuia("09", txtNumGuia.Text, cNumTickSun, txtNumGuia.Tag)
                        If cMensaje <> "" Then
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE SUNAT")
                        End If
                    End If
                Else
                    If cmbTipoRef.SelectedValue = "01" Or cmbTipoRef.SelectedValue = "03" Then
                        If chbParcial.Checked = False Then
                            cMensaje = ""
                            cMensaje = GenerarComprobanteElectronico(txtNumDocRef.Tag, cmbTipoRef.SelectedValue)
                            If cMensaje <> "" Then
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE SUNAT")
                            End If
                        End If
                    End If
                End If
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Sub CargaSeries()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarSeries("09", MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbSerie)
    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub txtBuscarCodCat_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarCodCat.TextChanged

    End Sub
    Sub ObtenerProducto()
        Dim dt As DataTable
        Dim CodProd As Integer = 0
        Dim BLProductos As New BLProducts
        dt = BLProductos.ObtenerProducto(txtBuscarCodCat.Text, 1, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
        If dt.Rows.Count > 0 Then
            CodProd = ObtenerDato("nCodProd", dt)
            cMensaje = ""
            cMensaje = AgregarProductoDetalle(CodProd, 1)
            If cMensaje <> "" Then
                txtBuscarCodCat.SelectAll()
            Else
                Call RecuperarDetalle()
                txtBuscarCodCat.Clear()
                Call EnfocarFocus(4, dtgListado.Rows.Count - 1)
                txtBuscarCodCat.Select()
            End If
        Else
            MsgBox("El código catálogo del producto ingresado no existe", MsgBoxStyle.Exclamation, "MENSAJE")
            txtBuscarCodCat.SelectAll()
        End If
    End Sub

    Private Sub txtBuscarCodCat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscarCodCat.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
        If e.KeyChar = Chr(13) Then
            Call ObtenerProducto()
        End If
    End Sub
    Private Sub chbAdjuntar_CheckedChanged(sender As Object, e As EventArgs) Handles chbAdjuntar.CheckedChanged

    End Sub

    Private Sub chbAdjuntar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbAdjuntar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbSerie_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbSerie.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtNumDoc_TextChanged(sender As Object, e As EventArgs) Handles txtNumDoc.TextChanged

    End Sub

    Private Sub cmbTipoRef_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoRef.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoRef_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoRef.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtDirPart_TextChanged(sender As Object, e As EventArgs)

    End Sub
    Private Sub cmbDirLleg_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDirLleg.SelectedIndexChanged

    End Sub

    Private Sub txtDirPart_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMotivoTrasl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMotivoTrasl.SelectedIndexChanged

    End Sub

    Private Sub cmbDirLleg_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbDirLleg.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMotivoTrasl_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMotivoTrasl.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If IsDBNull(dtgListado.CurrentRow.Cells("nCantidad").Value) Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        If dtgListado.CurrentRow.Cells("nCantidad").Value = "0" Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        cMensaje = ""
        Dim BLGuia As New BLGuia
        If BLGuia.ModificarProducto(txtNumGuia.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value,
                                        dtgListado.CurrentRow.Cells("nCantidad").Value, CantidadPorducto,
                                    dtgListado.CurrentRow.Cells("cDescripcion").Value, cMensaje) Then
            If (cMensaje.Trim).Length > 0 Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Call LimpiaGrilla(dtgListado)
            Call RecuperarDetalle()
            Call EnfocarFocus(Columna, Fila)
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub EnfocarFocus(ByVal Columna As Integer, ByVal Fila As Integer)
        If dtgListado.Rows.Count > 0 Then
            Me.dtgListado.CurrentCell = Me.dtgListado(Columna, Fila)
        End If
    End Sub

    Private Sub chbAdjuntar_CheckStateChanged(sender As Object, e As EventArgs) Handles chbAdjuntar.CheckStateChanged

    End Sub
    Private Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim BEClientes As New BECustomer
        FrmConsultaCliente.Inicio(BEClientes)
        FrmConsultaCliente = Nothing
        If BEClientes.nCodCli <> 0 Then
            nCodCli = BEClientes.nCodCli
            txtCliente.Text = BEClientes.cRazSoc
            txtDireccion.Text = BEClientes.cDirCli
            txtNumDoc.Text = BEClientes.cNumDoc
            txtDirPart.Text = MDIPrincipal.DirAgencia
            txtAlmacen.Text = MDIPrincipal.Agencia
            Call CargarDirecciones()
            dtpFechaRegistro.Select()
        Else
            btnBuscarCliente.Select()
        End If
    End Sub

    Private Sub tlsTitulo_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tlsTitulo.ItemClicked

    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub btnBuscarCliente_ImeModeChanged(sender As Object, e As EventArgs) Handles btnBuscarCliente.ImeModeChanged
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtgListado_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dtgListado.CellBeginEdit
        CantidadPorducto = dtgListado.CurrentRow.Cells("nCantidad").Value
    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
        If (e.KeyCode = Keys.Return) Then
            e.Handled = True
            SendKeys.Send(“{TAB}”)
        End If
    End Sub

    Private Sub chbParcial_CheckedChanged(sender As Object, e As EventArgs) Handles chbParcial.CheckedChanged

    End Sub

    Private Sub cmbTipoGuia_SelectionChangeCommitted(sender As Object, e As EventArgs)
        Call CargaSeries()
    End Sub

    Private Sub chbParcial_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbParcial.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub lblMensaje_Click(sender As Object, e As EventArgs) Handles lblMensaje.Click

    End Sub

    Private Sub cmbModalidadTraslado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbModalidadTraslado.SelectedIndexChanged

    End Sub

    Private Sub FrmRegistroGuias_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            Call btnFacturar_Click(sender, e)
        End If
    End Sub

    Private Sub cmbModalidadTraslado_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbModalidadTraslado.SelectionChangeCommitted

    End Sub

    Private Sub dtpFechaTraslado_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaTraslado.ValueChanged

    End Sub

    Private Sub cmbModalidadTraslado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbModalidadTraslado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaTraslado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaTraslado.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class