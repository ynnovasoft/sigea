﻿Imports Negocios
Imports Business
Imports Entidades
Imports Entities
Public Class FrmRegistroLetras
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodCli As Integer = 0
    Dim CodigoLetra As Integer = 0
    Private Sub FrmRegistroLetras_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaCombos()
        Call Limpiar()
        Call InterfaceEntrada()
        btnNuevo.Select()
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
    End Sub
    Sub Limpiar()
        lblEstado.ForeColor = Color.DodgerBlue
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        dtpFecGir.Value = MDIPrincipal.FechaSistema
        dtpFecVen.Value = MDIPrincipal.FechaSistema
        nCodCli = 0
        CodigoLetra = 0
        txtCliente.Text = ""
        txtNumLetraRef.Text = ""
        chbParcial.Checked = False
        txtNumLetra.Text = ""
        lblEstado.Text = ""
        txtMonto.Text = "0.00"
        txtSubTotal.Text = "0.00"
        Call LimpiaGrilla(dtgListado)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Call Limpiar()
        Call InterfaceNuevo()
        btnBuscarCliente.Select()
    End Sub
    Sub InterfaceNuevo()
        dtgListado.Enabled = False
        btnNuevo.Enabled = False
        btnGrabar.Enabled = True
        btnBuscar.Enabled = False
        btnGestionar.Enabled = False
        btnBuscarCliente.Enabled = True
        dtpFechaRegistro.Enabled = False
        dtpFecVen.Enabled = False
        dtpFecGir.Enabled = False
        cmbMoneda.Enabled = False
        chbParcial.Enabled = False
        txtMonto.Enabled = False
    End Sub
    Private Sub btnNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Function validacion() As Boolean
        validacion = True
        If nCodCli = 0 Then
            validacion = False
            MsgBox("Debe registrar el cliente ", MsgBoxStyle.Exclamation, "MENSAJE")
            btnBuscarCliente.Select()
            Exit Function
        End If
        If dtgListado.Rows.Count <= 0 Then
            validacion = False
            MsgBox("No se puede facturar una letra sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            dtgListado.Select()
            Exit Function
        End If
        If CDbl(txtMonto.Text) = 0 Then
            validacion = False
            MsgBox("No se puede facturar una letra con el importe total igual a CERO", MsgBoxStyle.Exclamation, "Mensaje")
            txtMonto.Select()
            Exit Function
        End If
        Return validacion

    End Function
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If validacion() = False Then
            Exit Sub
        End If
        Dim oDatos As New BELetras
        If MsgBox("¿Esta seguro que desea registrar la letra?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodCli = nCodCli
            oDatos.gdFecReg = dtpFechaRegistro.Value
            oDatos.gdFecGir = dtpFecGir.Value
            oDatos.gdFecVen = dtpFecVen.Value
            oDatos.gnMonto = txtMonto.Text
            oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
            oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
            oDatos.gbParcial = chbParcial.Checked
            oDatos.gnTipMon = cmbMoneda.SelectedValue
            oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
            Dim BLLetras As New BLLetras
            If BLLetras.Registrar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                txtNumLetra.Tag = oDatos.gnCodLet
                txtNumLetra.Text = oDatos.gcNumLet
                Dim MontoTotal As Double = txtMonto.Text
                Dim MontoDetalle As Double = 0
                For i = 0 To dtgListado.Rows.Count - 1
                    oDatos.gnCodLet = txtNumLetra.Tag
                    oDatos.gnCodCli = dtgListado.Rows(i).Cells("nCodPed").Value
                    oDatos.gnMonto = dtgListado.Rows(i).Cells("nMonto").Value
                    MontoDetalle = dtgListado.Rows(i).Cells("nMonPag").Value
                    If MontoTotal >= MontoDetalle Then
                        If MontoDetalle > 0 Then
                            oDatos.gnMonAmo = MontoDetalle
                            MontoTotal = MontoTotal - MontoDetalle
                        Else
                            oDatos.gnMonAmo = 0
                            MontoTotal = MontoTotal - 0
                        End If
                    ElseIf MontoTotal > 0 Then
                        If MontoDetalle > 0 Then
                            oDatos.gnMonAmo = MontoTotal
                            MontoTotal = MontoTotal - MontoTotal
                        Else
                            oDatos.gnMonAmo = 0
                            MontoTotal = MontoTotal - 0
                        End If
                    ElseIf MontoTotal = 0 Then
                        oDatos.gnMonAmo = 0
                    End If
                    BLLetras.RegistrarDetalle(oDatos, cMensaje)
                Next
                Call InterfaceEntrada()
                lblEstado.Text = "LETRA VIGENTE"
                btnImprimir.Enabled = True
                btnGestionar.Enabled = True
                btnNuevo.Enabled = False
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub
    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim BEClientes As New BECustomer
        Dim BELetras As New BELetras
        FrmConsultaLetras.InicioLetras(BELetras, BEClientes)
        FrmConsultaLetras = Nothing
        If BELetras.gnCodLet <> 0 Then

            txtNumLetra.Tag = BELetras.gnCodLet
            txtNumLetra.Text = BELetras.gcNumLet
            dtpFechaRegistro.Value = BELetras.gdFecReg
            dtpFecGir.Value = BELetras.gdFecGir
            dtpFecVen.Value = BELetras.gdFecVen
            CodigoLetra = BELetras.gnCodLet
            txtSubTotal.Text = FormatNumber(BELetras.gnMonto, 2)
            txtMonto.Text = FormatNumber(BELetras.gnMonto, 2)
            txtCliente.Text = BEClientes.cRazSoc
            txtNumLetraRef.Text = BELetras.gcNumLetRef
            cmbMoneda.SelectedValue = BELetras.gnTipMon
            chbParcial.Checked = BELetras.gbParcial

            Call InterfaceEntrada()
            dtgListado.Enabled = True
            btnNuevo.Enabled = False
            btnBuscar.Enabled = True
            Call RecuperarDetalle(BELetras.gnCodLet, 1)
            dtgListado.Enabled = True
            dtpFecVen.Enabled = False
            dtpFecGir.Enabled = False
            cmbMoneda.Enabled = False
            chbParcial.Enabled = False
            txtMonto.Enabled = False
            If BELetras.gnEstado = 1 Then
                lblEstado.ForeColor = Color.DodgerBlue
                lblEstado.Text = "LETRA VIGENTE"
                btnGestionar.Enabled = True
                btnImprimir.Enabled = True
            ElseIf BELetras.gnEstado = 2 Then
                lblEstado.ForeColor = Color.DodgerBlue
                lblEstado.Text = "LETRA PROTESTADA"
                btnImprimir.Enabled = False
                btnGestionar.Enabled = False
            ElseIf BELetras.gnEstado = 5 Then
                lblEstado.ForeColor = Color.DodgerBlue
                lblEstado.Text = "LETRA RENOVADA"
                btnGestionar.Enabled = False
                btnImprimir.Enabled = False
            ElseIf BELetras.gnEstado = 4 Then
                lblEstado.ForeColor = Color.DodgerBlue
                lblEstado.Text = "LETRA CANCELADA"
                btnGestionar.Enabled = False
                btnImprimir.Enabled = False
            ElseIf BELetras.gnEstado = 6 Then
                lblEstado.ForeColor = Color.DodgerBlue
                lblEstado.Text = "LETRA PROTESTADA/VIGENTE"
                btnGestionar.Enabled = True
                btnImprimir.Enabled = False
            ElseIf BELetras.gnEstado = 3 Then
                lblEstado.ForeColor = Color.Red
                lblEstado.Text = "LETRA ANULADA"
                btnGestionar.Enabled = False
                btnImprimir.Enabled = False
            End If
            btnGestionar.Select()
        End If
    End Sub

    Sub RecuperarDetalle(ByVal Codigo As Integer, ByVal TipoBusqueda As Integer)
        Dim dt As New DataTable
        Dim BLLetras As New BLLetras
        dt = BLLetras.RecuperarDetalle(Codigo, TipoBusqueda)
        Call LlenaAGridView(dt, dtgListado)
        If dtgListado.Rows.Count > 0 Then
            dtgListado.Enabled = True
            dtpFecVen.Enabled = True
            dtpFecGir.Enabled = True
            cmbMoneda.Enabled = True
            chbParcial.Enabled = True
            txtMonto.Enabled = True
        Else
            dtgListado.Enabled = False
            dtpFecVen.Enabled = False
            dtpFecGir.Enabled = False
            cmbMoneda.Enabled = False
            chbParcial.Enabled = False
            txtMonto.Enabled = False
        End If
    End Sub

    Private Sub btnBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Call Limpiar()
        Call InterfaceEntrada()
        btnNuevo.Select()
    End Sub
    Sub InterfaceEntrada()
        dtgListado.Enabled = False
        btnNuevo.Enabled = True
        btnGrabar.Enabled = False
        btnGestionar.Enabled = False
        btnBuscar.Enabled = True
        btnImprimir.Enabled = False
        btnBuscarCliente.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFecVen.Enabled = False
        chbParcial.Enabled = False
        dtpFecGir.Enabled = False
        cmbMoneda.Enabled = False
        txtMonto.Enabled = False
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnHome.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub
    Sub CalcularTotales()
        Dim TOTAL As Double
        If dtgListado.Rows.Count > 0 Then
            TOTAL = 0
            For i = 0 To dtgListado.Rows.Count - 1
                TOTAL = TOTAL + CDbl(dtgListado.Rows(i).Cells("nMonPag").Value)
            Next
            txtSubTotal.Text = FormatNumber(TOTAL, 2)
            txtMonto.Text = FormatNumber(TOTAL, 2)
        Else
            txtMonto.Text = FormatNumber(0, 2)
            txtSubTotal.Text = FormatNumber(TOTAL, 2)
        End If
    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub
    Private Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim BEClientes As New BECustomer
        FrmConsultaCliente.Inicio(BEClientes)
        FrmConsultaCliente = Nothing
        If BEClientes.nCodCli <> 0 Then
            nCodCli = BEClientes.nCodCli
            txtCliente.Text = BEClientes.cRazSoc
            Call RecuperarDetalle(nCodCli, 0)
            Call CalcularTotales()
            chbParcial.Select()
        Else
            btnBuscarCliente.Select()
        End If
    End Sub

    Private Sub btnBuscarCliente_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarCliente.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFecGir_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecGir.ValueChanged

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If CodigoLetra = 0 Then
            If e.KeyCode = Keys.Delete Then
                e.Handled = True
                dtgListado.Rows.RemoveAt(dtgListado.CurrentRow.Index)
            End If
            Call CalcularTotales()
        End If
    End Sub

    Private Sub dtpFecVen_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecVen.ValueChanged

    End Sub

    Private Sub dtpFecGir_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecGir.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnGestionar_Click(sender As Object, e As EventArgs) Handles btnGestionar.Click
        Dim Respuesta As Boolean = False
        FrmGestionLetras.Inicio(txtMonto.Text, Respuesta, txtNumLetra.Tag, cmbMoneda.SelectedValue)
        FrmGestionLetras = Nothing
        If Respuesta Then
            Call Limpiar()
            Call InterfaceEntrada()
            btnNuevo.Focus()
        Else
            btnGestionar.Select()
        End If
    End Sub

    Private Sub dtpFecVen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecVen.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        FrmReportes.ImpresionLetras(txtNumLetra.Tag)
        FrmReportes = Nothing
    End Sub

    Private Sub btnGestionar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGestionar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub btnImprimir_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnImprimir.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbParcial_CheckedChanged(sender As Object, e As EventArgs) Handles chbParcial.CheckedChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_TextChanged(sender As Object, e As EventArgs) Handles txtMonto.TextChanged

    End Sub

    Private Sub chbParcial_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbParcial.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress
        Call ValidaDecimales(e, txtMonto)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMonto_GotFocus(sender As Object, e As EventArgs) Handles txtMonto.GotFocus
        Call ValidaFormatoMonedaGot(txtMonto)
    End Sub

    Private Sub txtMonto_LostFocus(sender As Object, e As EventArgs) Handles txtMonto.LostFocus
        Call ValidaFormatoMonedaLost(txtMonto)
    End Sub
End Class