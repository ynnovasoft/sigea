﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmRegistroNotaDebito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.cmbTipoNota = New System.Windows.Forms.ComboBox()
        Me.txtCliente = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFechaRegistro = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbMoneda = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNumComprobante = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodPed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nNumOrd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodCat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUndMed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nSubTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValVent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtVentasGravadas = New System.Windows.Forms.Label()
        Me.txtIGV = New System.Windows.Forms.Label()
        Me.txtImporteTotal = New System.Windows.Forms.Label()
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.btnFacturar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.txtNumComRef = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbTipComRef = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMotivo = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cmbSerie = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(727, 29)
        Me.tlsTitulo.TabIndex = 0
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(177, 26)
        Me.lblTitulo.Text = "REGISTRO DE NOTA DÉBITO"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'cmbTipoNota
        '
        Me.cmbTipoNota.BackColor = System.Drawing.Color.White
        Me.cmbTipoNota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoNota.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoNota.FormattingEnabled = True
        Me.cmbTipoNota.Location = New System.Drawing.Point(100, 88)
        Me.cmbTipoNota.Name = "cmbTipoNota"
        Me.cmbTipoNota.Size = New System.Drawing.Size(323, 24)
        Me.cmbTipoNota.TabIndex = 3
        '
        'txtCliente
        '
        Me.txtCliente.BackColor = System.Drawing.SystemColors.Info
        Me.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCliente.Location = New System.Drawing.Point(100, 62)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(323, 23)
        Me.txtCliente.TabIndex = 5
        Me.txtCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 16)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Cliente:"
        '
        'dtpFechaRegistro
        '
        Me.dtpFechaRegistro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaRegistro.Location = New System.Drawing.Point(317, 36)
        Me.dtpFechaRegistro.Name = "dtpFechaRegistro"
        Me.dtpFechaRegistro.Size = New System.Drawing.Size(106, 23)
        Me.dtpFechaRegistro.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(239, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "F. Registro:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 96)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Tipo Nota:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(533, 122)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 16)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Moneda:"
        '
        'cmbMoneda
        '
        Me.cmbMoneda.BackColor = System.Drawing.Color.White
        Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMoneda.Enabled = False
        Me.cmbMoneda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMoneda.FormattingEnabled = True
        Me.cmbMoneda.Location = New System.Drawing.Point(594, 114)
        Me.cmbMoneda.Name = "cmbMoneda"
        Me.cmbMoneda.Size = New System.Drawing.Size(125, 24)
        Me.cmbMoneda.TabIndex = 6
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 43)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 16)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "N° Nota:"
        '
        'txtNumComprobante
        '
        Me.txtNumComprobante.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumComprobante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumComprobante.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumComprobante.Location = New System.Drawing.Point(100, 36)
        Me.txtNumComprobante.Name = "txtNumComprobante"
        Me.txtNumComprobante.Size = New System.Drawing.Size(124, 23)
        Me.txtNumComprobante.TabIndex = 18
        Me.txtNumComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodPed, Me.nCodProd, Me.nNumOrd, Me.cCodCat, Me.nCantidad, Me.cUndMed, Me.cDescripcion, Me.nValUni, Me.nPreUni, Me.nSubTot, Me.nValVent})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(11, 201)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.ReadOnly = True
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(708, 125)
        Me.dtgListado.TabIndex = 22
        '
        'nCodPed
        '
        Me.nCodPed.DataPropertyName = "nCodPed"
        Me.nCodPed.HeaderText = "nCodPed"
        Me.nCodPed.Name = "nCodPed"
        Me.nCodPed.ReadOnly = True
        Me.nCodPed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodPed.Visible = False
        '
        'nCodProd
        '
        Me.nCodProd.DataPropertyName = "nCodProd"
        Me.nCodProd.HeaderText = "nCodProd"
        Me.nCodProd.Name = "nCodProd"
        Me.nCodProd.ReadOnly = True
        Me.nCodProd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodProd.Visible = False
        '
        'nNumOrd
        '
        Me.nNumOrd.DataPropertyName = "nNumOrd"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nNumOrd.DefaultCellStyle = DataGridViewCellStyle2
        Me.nNumOrd.HeaderText = "N°"
        Me.nNumOrd.Name = "nNumOrd"
        Me.nNumOrd.ReadOnly = True
        Me.nNumOrd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nNumOrd.Visible = False
        Me.nNumOrd.Width = 25
        '
        'cCodCat
        '
        Me.cCodCat.DataPropertyName = "cCodCat"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cCodCat.DefaultCellStyle = DataGridViewCellStyle3
        Me.cCodCat.HeaderText = "Cod. Cat"
        Me.cCodCat.Name = "cCodCat"
        Me.cCodCat.ReadOnly = True
        Me.cCodCat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cCodCat.Visible = False
        Me.cCodCat.Width = 110
        '
        'nCantidad
        '
        Me.nCantidad.DataPropertyName = "nCantidad"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = "1"
        Me.nCantidad.DefaultCellStyle = DataGridViewCellStyle4
        Me.nCantidad.HeaderText = "Cant."
        Me.nCantidad.Name = "nCantidad"
        Me.nCantidad.ReadOnly = True
        Me.nCantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCantidad.Visible = False
        Me.nCantidad.Width = 60
        '
        'cUndMed
        '
        Me.cUndMed.DataPropertyName = "cUndMed"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cUndMed.DefaultCellStyle = DataGridViewCellStyle5
        Me.cUndMed.HeaderText = "Und"
        Me.cUndMed.Name = "cUndMed"
        Me.cUndMed.ReadOnly = True
        Me.cUndMed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cUndMed.Visible = False
        Me.cUndMed.Width = 60
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.ReadOnly = True
        Me.cDescripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cDescripcion.Width = 358
        '
        'nValUni
        '
        Me.nValUni.DataPropertyName = "nValUni"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N6"
        DataGridViewCellStyle6.NullValue = "0.000000"
        Me.nValUni.DefaultCellStyle = DataGridViewCellStyle6
        Me.nValUni.HeaderText = "Valor Unit."
        Me.nValUni.Name = "nValUni"
        Me.nValUni.ReadOnly = True
        Me.nValUni.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nValUni.Width = 110
        '
        'nPreUni
        '
        Me.nPreUni.DataPropertyName = "nPreUni"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N6"
        DataGridViewCellStyle7.NullValue = "0.000000"
        Me.nPreUni.DefaultCellStyle = DataGridViewCellStyle7
        Me.nPreUni.HeaderText = "Precio Unit."
        Me.nPreUni.Name = "nPreUni"
        Me.nPreUni.ReadOnly = True
        Me.nPreUni.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nSubTot
        '
        Me.nSubTot.DataPropertyName = "nSubTot"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.nSubTot.DefaultCellStyle = DataGridViewCellStyle8
        Me.nSubTot.HeaderText = "Sub Total"
        Me.nSubTot.Name = "nSubTot"
        Me.nSubTot.ReadOnly = True
        Me.nSubTot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nSubTot.Width = 120
        '
        'nValVent
        '
        Me.nValVent.DataPropertyName = "nValVent"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.nValVent.DefaultCellStyle = DataGridViewCellStyle9
        Me.nValVent.HeaderText = "Valor Venta"
        Me.nValVent.Name = "nValVent"
        Me.nValVent.ReadOnly = True
        Me.nValVent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nValVent.Visible = False
        Me.nValVent.Width = 120
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(8, 340)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 16)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "SubTotal:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(292, 340)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 16)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "IGV:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(520, 340)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 16)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Importe Total:"
        '
        'txtVentasGravadas
        '
        Me.txtVentasGravadas.BackColor = System.Drawing.SystemColors.Info
        Me.txtVentasGravadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVentasGravadas.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVentasGravadas.Location = New System.Drawing.Point(75, 333)
        Me.txtVentasGravadas.Name = "txtVentasGravadas"
        Me.txtVentasGravadas.Size = New System.Drawing.Size(94, 23)
        Me.txtVentasGravadas.TabIndex = 35
        Me.txtVentasGravadas.Text = "0.00"
        Me.txtVentasGravadas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIGV
        '
        Me.txtIGV.BackColor = System.Drawing.SystemColors.Info
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGV.Location = New System.Drawing.Point(328, 333)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.Size = New System.Drawing.Size(82, 23)
        Me.txtIGV.TabIndex = 36
        Me.txtIGV.Text = "0.00"
        Me.txtIGV.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.BackColor = System.Drawing.SystemColors.Info
        Me.txtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteTotal.Location = New System.Drawing.Point(614, 333)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(103, 23)
        Me.txtImporteTotal.TabIndex = 37
        Me.txtImporteTotal.Text = "0.00"
        Me.txtImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMensaje
        '
        Me.lblMensaje.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblMensaje.Location = New System.Drawing.Point(178, 141)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(539, 25)
        Me.lblMensaje.TabIndex = 123
        Me.lblMensaje.Text = "MENSAJE DE ESTADO"
        Me.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 116
        '
        'btnFacturar
        '
        Me.btnFacturar.FlatAppearance.BorderSize = 0
        Me.btnFacturar.FlatAppearance.CheckedBackColor = System.Drawing.Color.DodgerBlue
        Me.btnFacturar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnFacturar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFacturar.Image = Global.Presentacion.My.Resources.Resources.Facturar_22
        Me.btnFacturar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFacturar.Location = New System.Drawing.Point(612, 378)
        Me.btnFacturar.Name = "btnFacturar"
        Me.btnFacturar.Size = New System.Drawing.Size(107, 30)
        Me.btnFacturar.TabIndex = 13
        Me.btnFacturar.Text = "Facturar(F3)"
        Me.btnFacturar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFacturar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.FlatAppearance.BorderSize = 0
        Me.btnAnular.FlatAppearance.CheckedBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnular.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnular.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnular.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnular.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnular.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnular.Location = New System.Drawing.Point(298, 378)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(97, 30)
        Me.btnAnular.TabIndex = 15
        Me.btnAnular.Text = "Anular(F4)"
        Me.btnAnular.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatAppearance.CheckedBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(11, 378)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(114, 30)
        Me.btnGrabar.TabIndex = 12
        Me.btnGrabar.Text = "Registrar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'txtNumComRef
        '
        Me.txtNumComRef.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumComRef.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumComRef.Location = New System.Drawing.Point(594, 62)
        Me.txtNumComRef.Name = "txtNumComRef"
        Me.txtNumComRef.Size = New System.Drawing.Size(125, 23)
        Me.txtNumComRef.TabIndex = 126
        Me.txtNumComRef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(460, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 16)
        Me.Label1.TabIndex = 127
        Me.Label1.Text = "N° Comprobante Ref:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(458, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(91, 16)
        Me.Label4.TabIndex = 129
        Me.Label4.Text = "Tip.Comp Ref:"
        '
        'cmbTipComRef
        '
        Me.cmbTipComRef.BackColor = System.Drawing.Color.White
        Me.cmbTipComRef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipComRef.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipComRef.FormattingEnabled = True
        Me.cmbTipComRef.Location = New System.Drawing.Point(550, 36)
        Me.cmbTipComRef.Name = "cmbTipComRef"
        Me.cmbTipComRef.Size = New System.Drawing.Size(169, 24)
        Me.cmbTipComRef.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 122)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 16)
        Me.Label7.TabIndex = 135
        Me.Label7.Text = "Motivo:"
        '
        'txtMotivo
        '
        Me.txtMotivo.Location = New System.Drawing.Point(100, 115)
        Me.txtMotivo.Name = "txtMotivo"
        Me.txtMotivo.Size = New System.Drawing.Size(323, 23)
        Me.txtMotivo.TabIndex = 5
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(548, 96)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(43, 16)
        Me.Label16.TabIndex = 137
        Me.Label16.Text = "Serie:"
        '
        'cmbSerie
        '
        Me.cmbSerie.BackColor = System.Drawing.Color.White
        Me.cmbSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSerie.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbSerie.FormattingEnabled = True
        Me.cmbSerie.Location = New System.Drawing.Point(594, 88)
        Me.cmbSerie.Name = "cmbSerie"
        Me.cmbSerie.Size = New System.Drawing.Size(125, 24)
        Me.cmbSerie.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 180)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 16)
        Me.Label8.TabIndex = 139
        Me.Label8.Text = "Descripción:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Location = New System.Drawing.Point(89, 173)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(402, 23)
        Me.txtDescripcion.TabIndex = 7
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(530, 180)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 16)
        Me.Label10.TabIndex = 141
        Me.Label10.Text = "Precio:"
        '
        'txtPrecio
        '
        Me.txtPrecio.Location = New System.Drawing.Point(580, 173)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(88, 23)
        Me.txtPrecio.TabIndex = 8
        Me.txtPrecio.Text = "0.00"
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnEliminar
        '
        Me.btnEliminar.BackgroundImage = Global.Presentacion.My.Resources.Resources.Delete_20
        Me.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Location = New System.Drawing.Point(700, 171)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(19, 25)
        Me.btnEliminar.TabIndex = 10
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.BackgroundImage = Global.Presentacion.My.Resources.Resources.Add_20
        Me.btnAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAgregar.FlatAppearance.BorderSize = 0
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Location = New System.Drawing.Point(677, 171)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(21, 25)
        Me.btnAgregar.TabIndex = 9
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'FrmRegistroNotaDebito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(727, 415)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtPrecio)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.cmbSerie)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtMotivo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbTipComRef)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNumComRef)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.btnFacturar)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.txtImporteTotal)
        Me.Controls.Add(Me.txtIGV)
        Me.Controls.Add(Me.txtVentasGravadas)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNumComprobante)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmbMoneda)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpFechaRegistro)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.cmbTipoNota)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmRegistroNotaDebito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents cmbTipoNota As System.Windows.Forms.ComboBox
    Friend WithEvents txtCliente As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaRegistro As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNumComprobante As System.Windows.Forms.Label
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents txtVentasGravadas As Label
    Friend WithEvents txtIGV As Label
    Friend WithEvents txtImporteTotal As Label
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnFacturar As System.Windows.Forms.Button
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents lblMensaje As Label
    Friend WithEvents txtNumComRef As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cmbTipComRef As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtMotivo As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cmbSerie As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtDescripcion As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtPrecio As TextBox
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnAgregar As Button
    Friend WithEvents nCodPed As DataGridViewTextBoxColumn
    Friend WithEvents nCodProd As DataGridViewTextBoxColumn
    Friend WithEvents nNumOrd As DataGridViewTextBoxColumn
    Friend WithEvents cCodCat As DataGridViewTextBoxColumn
    Friend WithEvents nCantidad As DataGridViewTextBoxColumn
    Friend WithEvents cUndMed As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents nValUni As DataGridViewTextBoxColumn
    Friend WithEvents nPreUni As DataGridViewTextBoxColumn
    Friend WithEvents nSubTot As DataGridViewTextBoxColumn
    Friend WithEvents nValVent As DataGridViewTextBoxColumn
    Friend WithEvents ToolTip As ToolTip
End Class
