﻿Imports Negocios
Imports Entidades
Imports Business
Imports Entities
Imports System.Windows.Forms
Public Class FrmRegistroNotaDebito
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodCli As Integer = 0
    Dim TipoCambio As Double = 0
    Dim BEClientes As New BECustomer
    Dim BEPedido As New BEPedido
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion, 3=RegistroFacturacion
    Dim Accion As Boolean = False
    Private Sub FrmRegistroNotaDebito_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargaCombosComprobante()
        Call CargaSeries()
        If ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosNota()
        ElseIf ModalidadVentana = 3 Then
            Call Limpiar()
            Call InterfaceNuevo()
            Call RegistrarDatosFacturacion()
        End If
    End Sub
    Sub InicioFacturacion(ByVal oBEPedido As BEPedido, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)
        BEPedido = oBEPedido
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosNota()
        nCodCli = BEPedido.gnCodCli
        dtpFechaRegistro.Value = BEPedido.gdFecReg
        cmbTipoNota.SelectedValue = BEPedido.gcCodTipNot
        txtMotivo.Text = BEPedido.gcMotNot
        txtNumComRef.Text = BEPedido.gcNumComRef
        cmbTipComRef.SelectedValue = BEPedido.gcTipoComRef
        txtNumComprobante.Text = BEPedido.gcNumComp
        txtCliente.Text = BEClientes.cRazSoc
        txtNumComprobante.Tag = BEPedido.gnCodPed
        dtpFechaRegistro.Value = BEPedido.gdFecReg
        txtVentasGravadas.Text = FormatNumber(BEPedido.gnVenGra, 2)
        txtIGV.Text = FormatNumber(BEPedido.gnIGV, 2)
        txtImporteTotal.Text = FormatNumber(BEPedido.gnImpTot, 2)
        cmbMoneda.SelectedValue = BEPedido.gnTipMon
        Call CargaSeries()
        cmbSerie.SelectedValue = BEPedido.gnCodSer
        Call RecuperarDetalle()

        If BEPedido.gnEstado = 1 Then
            lblMensaje.Text = "NOTA REGISTRADO(A) - " + BEPedido.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call CalcularTotales()
            Call InterfaceGrabar()
            txtDescripcion.Focus()
        ElseIf BEPedido.gnEstado = 2 Then
            lblMensaje.Text = "NOTA FACTURADO(A) - " + BEPedido.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceFacturar()
        ElseIf BEPedido.gnEstado = 3 Then
            lblMensaje.Text = "NOTA ANULADO(A) - " + BEPedido.gcMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceFacturar()
        End If
    End Sub
    Sub RegistrarDatosFacturacion()
        txtNumComRef.Text = BEPedido.gcNumComp
        txtNumComRef.Tag = BEPedido.gnCodPed
        nCodCli = BEPedido.gnCodCli
        txtCliente.Text = BEClientes.cRazSoc
        cmbMoneda.SelectedValue = BEPedido.gnTipMon
        TipoCambio = BEPedido.gnTipCam
        cmbTipoNota.Focus()
    End Sub
    Sub CargaCombosComprobante()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(30, "01,03")
        Call CargaCombo(dt, cmbTipComRef)
    End Sub
    Sub CargaSeries()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarSeries("08", MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbSerie)
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60,330")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
        Call CargaCombo(CreaDatoCombos(dt, 330), cmbTipoNota)

    End Sub
    Sub Limpiar()
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        lblMensaje.Text = ""
        lblMensaje.ForeColor = Color.Blue
        nCodCli = 0
        txtCliente.Text = ""
        txtNumComRef.Text = ""
        txtNumComRef.Tag = 0
        txtNumComprobante.Text = ""
        txtVentasGravadas.Text = "0.00"
        txtMotivo.Clear()
        TipoCambio = 0
        txtIGV.Text = "0.00"
        txtDescripcion.Text = ""
        txtPrecio.Text = "0.00"
        txtImporteTotal.Text = "0.00"
        Call LimpiaGrilla(dtgListado)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Sub InterfaceNuevo()
        dtgListado.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        btnAnular.Enabled = False
        cmbSerie.Enabled = True
        txtMotivo.Enabled = True
        cmbTipoNota.Enabled = True
        cmbTipComRef.Enabled = True
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        txtDescripcion.Enabled = False
        txtPrecio.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
    End Sub
    Function ValidaDatos() As Boolean
        ValidaDatos = True
        If nCodCli = 0 Then
            MsgBox("Debe registrar el cliente ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            Exit Function
        End If
        If IsNothing(cmbSerie.SelectedValue) Then
            MsgBox("Debe registrar la serie ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbSerie.Focus()
            Exit Function
        End If
        If txtMotivo.Text.Trim = "" Then
            MsgBox("Debe registrar el motivo o sustento de la nota de débito ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtMotivo.Focus()
            Exit Function
        End If
    End Function
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If ValidaDatos() = False Then
            Exit Sub
        End If
        Dim oDatos As New BEPedido
        cMensaje = ""
        oDatos.gnCodCli = nCodCli
        oDatos.gdFecReg = dtpFechaRegistro.Value
        oDatos.gdFecVen = dtpFechaRegistro.Value
        oDatos.gnCondPag = 1
        oDatos.gcTipCom = "08"
        oDatos.gcCodTipNot = cmbTipoNota.SelectedValue
        oDatos.gnTipMon = cmbMoneda.SelectedValue
        oDatos.gcMotNot = txtMotivo.Text
        oDatos.gcTipoComRef = cmbTipComRef.SelectedValue
        oDatos.gnCodDocRef = txtNumComRef.Tag
        oDatos.gcNumComRef = txtNumComRef.Text
        oDatos.gnCodSer = cmbSerie.SelectedValue
        oDatos.gcTipOpe = "0101"
        oDatos.gnTipAfecIGV = "10"
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.gnCodCot = 0
        oDatos.gnTipCam = TipoCambio
        oDatos.gcOrdComp = ""
        oDatos.gcTipDocAdj = 0
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.gnCodVen = MDIPrincipal.nCodUsuario
        oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
        oDatos.gcCodGui = ""
        oDatos.gbAplDedAnt = False
        Dim BLPedido As New BLPedido
        If BLPedido.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
            txtNumComprobante.Tag = oDatos.gnCodPed
            lblMensaje.Text = "NOTA DE DÉBITO REGISTRADA"
            Call RecuperarDetalle()
            Call CalcularTotales()
            Call InterfaceGrabar()
            txtDescripcion.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Sub InterfaceGrabar()
        dtgListado.Enabled = True
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        btnAnular.Enabled = True
        txtMotivo.Enabled = False
        cmbTipoNota.Enabled = False
        cmbTipComRef.Enabled = False
        cmbSerie.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        txtDescripcion.Enabled = True
        txtPrecio.Enabled = True
        btnAgregar.Enabled = True
        btnEliminar.Enabled = True
    End Sub
    Sub InterfaceFacturar()
        cmbSerie.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        btnAnular.Enabled = False
        txtMotivo.Enabled = False
        cmbTipoNota.Enabled = False
        cmbTipComRef.Enabled = False
        cmbMoneda.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtgListado.Enabled = True
        txtDescripcion.Enabled = False
        txtPrecio.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
    End Sub
    Private Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        Dim oDatos As New BEPedido
        If MsgBox("¿Esta seguro que desea anular la nota de débito?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodPed = txtNumComprobante.Tag
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            oDatos.gcMotAnu = "Error de Registro"
            Dim BLPedido As New BLPedido
            If BLPedido.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If

    End Sub
    Sub CalcularTotales()
        Dim ValorVenta, TOTAL, IGV As Double
        If dtgListado.Rows.Count > 0 Then
            ValorVenta = 0
            TOTAL = 0
            IGV = 0
            For i = 0 To dtgListado.Rows.Count - 1
                ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
                TOTAL = TOTAL + CDbl(dtgListado.Rows(i).Cells("nSubTot").Value)
            Next

            txtVentasGravadas.Text = FormatNumber(ValorVenta, 2)
            If MDIPrincipal.CodigoPrecioLista = 1 Then
                IGV = FormatNumber(TOTAL - ValorVenta, 2)
                txtIGV.Text = FormatNumber(IGV, 2)
                txtImporteTotal.Text = FormatNumber(TOTAL, 2)
            Else
                IGV = FormatNumber(ValorVenta * 0.18, 2)
                txtIGV.Text = FormatNumber(IGV, 2)
                txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
            End If

        Else
            txtVentasGravadas.Text = FormatNumber(0, 2)
            txtIGV.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
        End If
    End Sub
    Sub RecuperarDetalle()
        Dim dt As New DataTable
        Dim BLPedido As New BLPedido
        dt = BLPedido.RecuperarDetalle(txtNumComprobante.Tag)
        Call LlenaAGridView(dt, dtgListado)
    End Sub

    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar una nota de débito sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            Exit Function
        End If
        Return validaFacturacion
    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BEPedido
        If validaFacturacion() = False Then
            Exit Sub
        End If
        Try
            If MsgBox("¿Esta seguro que desea facturar la nota de débito?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
                cMensaje = ""
                oDatos.gnCodCli = nCodCli
                oDatos.gnCodPed = txtNumComprobante.Tag
                oDatos.gnVenGra = txtVentasGravadas.Text
                oDatos.gnIGV = txtIGV.Text
                oDatos.gnImpTot = txtImporteTotal.Text
                oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
                oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
                oDatos.gnCondPag = 0
                Dim BLPedido As New BLPedido
                If BLPedido.Facturar(oDatos, cMensaje) = True Then
                    If cMensaje <> "" Then
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        Exit Sub
                    End If
                    Accion = True
                    MsgBox("El comproante N°:" + oDatos.gcNumComp + " fue facturado correctamente", MsgBoxStyle.Information, "MENSAJE")
                    cMensaje = ""
                    cMensaje = GenerarComprobanteElectronico(txtNumComprobante.Tag, "08")
                    If cMensaje <> "" Then
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE SUNAT")
                    End If
                    Me.Close()
                Else
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "MENSAJE")
        End Try
    End Sub
    Sub InterfaceEntrada()
        dtgListado.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        btnAnular.Enabled = False
        txtMotivo.Enabled = False
        cmbTipoNota.Enabled = False
        cmbTipComRef.Enabled = False
        cmbMoneda.Enabled = False
        cmbSerie.Enabled = False
        dtpFechaRegistro.Enabled = False
        txtDescripcion.Enabled = False
        txtPrecio.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbTipComRef_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipComRef.SelectedIndexChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipComRef_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipComRef.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoNota_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoNota.SelectedIndexChanged

    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoNota_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoNota.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMotivo_TextChanged(sender As Object, e As EventArgs) Handles txtMotivo.TextChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtMotivo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMotivo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAnular_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnular.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbSerie_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSerie.SelectedIndexChanged

    End Sub

    Private Sub cmbSerie_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbSerie.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipComRef_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipComRef.SelectionChangeCommitted
        Call Limpiar()
    End Sub

    Private Sub txtDescripcion_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcion.TextChanged

    End Sub
    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
        If (e.KeyCode = Keys.Return) Then
            e.Handled = True
            SendKeys.Send(“{TAB}”)
        End If
    End Sub

    Private Sub txtPrecio_TextChanged(sender As Object, e As EventArgs) Handles txtPrecio.TextChanged

    End Sub

    Private Sub txtDescripcion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        Call ValidaDecimales(e, txtPrecio)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtPrecio_GotFocus(sender As Object, e As EventArgs) Handles txtPrecio.GotFocus
        Call ValidaFormatoMonedaGot(txtPrecio)
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If CDbl(txtPrecio.Text) = 0 Then
            MsgBox("Debe registrar el precio  de la nota de débito ", MsgBoxStyle.Exclamation, "MENSAJE")
            txtPrecio.Focus()
            Exit Sub
        End If
        If txtDescripcion.Text.Trim = "" Then
            MsgBox("Debe registrar la descripción del item  de la nota de débito ", MsgBoxStyle.Exclamation, "MENSAJE")
            txtDescripcion.Focus()
            Exit Sub
        End If
        cMensaje = ""
        Dim BLPedido As New BLPedido
        If BLPedido.AgregarProductoNotas(txtNumComprobante.Tag, txtPrecio.Text, txtDescripcion.Text, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Call RecuperarDetalle()
            Call CalcularTotales()
            txtDescripcion.Clear()
            txtPrecio.Text = "0.00"
            btnGrabar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub txtPrecio_LostFocus(sender As Object, e As EventArgs) Handles txtPrecio.LostFocus
        Call ValidaFormatoMonedaLost(txtPrecio)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        cMensaje = ""
        Dim BLPedido As New BLPedido
        If BLPedido.EliminarProducto(txtNumComprobante.Tag, 0, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Call RecuperarDetalle()
            Call CalcularTotales()
            btnGrabar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub FrmRegistroNotaDebito_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            Call btnFacturar_Click(sender, e)
        ElseIf e.KeyCode = Keys.F4 And btnAnular.Enabled = True And btnAnular.Visible = True Then
            Call btnAnular_Click(sender, e)
        End If
    End Sub
End Class