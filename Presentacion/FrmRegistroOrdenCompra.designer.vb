﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmRegistroOrdenCompra
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.cmbCondPago = New System.Windows.Forms.ComboBox()
        Me.txtProveedor = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFechaRegistro = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbMoneda = New System.Windows.Forms.ComboBox()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtpFechaVencimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNumCompra = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nNumOrd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodCat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUndMed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nSubTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValVent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtVentasGravadas = New System.Windows.Forms.Label()
        Me.txtIGV = New System.Windows.Forms.Label()
        Me.txtImporteTotal = New System.Windows.Forms.Label()
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.txtBuscarCodCat = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.btnBuscarProveedor = New System.Windows.Forms.Button()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.btnFacturar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cmbMotivoTras = New System.Windows.Forms.ComboBox()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.LightSkyBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(785, 29)
        Me.tlsTitulo.TabIndex = 0
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.LightSkyBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.Black
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(240, 26)
        Me.lblTitulo.Text = "REGISTRO DE ORDEN COMPRA LOCAL"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'cmbCondPago
        '
        Me.cmbCondPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCondPago.FormattingEnabled = True
        Me.cmbCondPago.Location = New System.Drawing.Point(529, 88)
        Me.cmbCondPago.Name = "cmbCondPago"
        Me.cmbCondPago.Size = New System.Drawing.Size(246, 24)
        Me.cmbCondPago.TabIndex = 5
        '
        'txtProveedor
        '
        Me.txtProveedor.BackColor = System.Drawing.SystemColors.Info
        Me.txtProveedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtProveedor.Location = New System.Drawing.Point(106, 63)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Size = New System.Drawing.Size(351, 23)
        Me.txtProveedor.TabIndex = 5
        Me.txtProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 16)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Proveedor:"
        '
        'dtpFechaRegistro
        '
        Me.dtpFechaRegistro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaRegistro.Location = New System.Drawing.Point(382, 35)
        Me.dtpFechaRegistro.Name = "dtpFechaRegistro"
        Me.dtpFechaRegistro.Size = New System.Drawing.Size(103, 23)
        Me.dtpFechaRegistro.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(447, 95)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Cond. Pago:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(586, 123)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 16)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Moneda:"
        '
        'cmbMoneda
        '
        Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMoneda.FormattingEnabled = True
        Me.cmbMoneda.Location = New System.Drawing.Point(647, 115)
        Me.cmbMoneda.Name = "cmbMoneda"
        Me.cmbMoneda.Size = New System.Drawing.Size(128, 24)
        Me.cmbMoneda.TabIndex = 7
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Location = New System.Drawing.Point(714, 62)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(61, 23)
        Me.txtTipoCambio.TabIndex = 3
        Me.txtTipoCambio.Text = "0.0000"
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(626, 69)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 16)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Tipo Cambio:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(613, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 16)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "F. Venc:"
        '
        'dtpFechaVencimiento
        '
        Me.dtpFechaVencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVencimiento.Location = New System.Drawing.Point(672, 35)
        Me.dtpFechaVencimiento.Name = "dtpFechaVencimiento"
        Me.dtpFechaVencimiento.Size = New System.Drawing.Size(103, 23)
        Me.dtpFechaVencimiento.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 43)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 16)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "N° Orden:"
        '
        'txtNumCompra
        '
        Me.txtNumCompra.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumCompra.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumCompra.Location = New System.Drawing.Point(106, 36)
        Me.txtNumCompra.Name = "txtNumCompra"
        Me.txtNumCompra.Size = New System.Drawing.Size(127, 23)
        Me.txtNumCompra.TabIndex = 18
        Me.txtNumCompra.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.PowderBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodCom, Me.nCodProd, Me.nNumOrd, Me.cCodCat, Me.cDescripcion, Me.cUndMed, Me.nCantidad, Me.nPrecio, Me.nSubTot, Me.nValVent})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.Location = New System.Drawing.Point(11, 194)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dtgListado.Size = New System.Drawing.Size(764, 218)
        Me.dtgListado.TabIndex = 22
        '
        'nCodCom
        '
        Me.nCodCom.DataPropertyName = "nCodCom"
        Me.nCodCom.HeaderText = "nCodCom"
        Me.nCodCom.Name = "nCodCom"
        Me.nCodCom.ReadOnly = True
        Me.nCodCom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodCom.Visible = False
        '
        'nCodProd
        '
        Me.nCodProd.DataPropertyName = "nCodProd"
        Me.nCodProd.HeaderText = "nCodProd"
        Me.nCodProd.Name = "nCodProd"
        Me.nCodProd.ReadOnly = True
        Me.nCodProd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodProd.Visible = False
        '
        'nNumOrd
        '
        Me.nNumOrd.DataPropertyName = "nNumOrd"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.nNumOrd.DefaultCellStyle = DataGridViewCellStyle2
        Me.nNumOrd.HeaderText = "N°"
        Me.nNumOrd.Name = "nNumOrd"
        Me.nNumOrd.ReadOnly = True
        Me.nNumOrd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nNumOrd.Visible = False
        Me.nNumOrd.Width = 25
        '
        'cCodCat
        '
        Me.cCodCat.DataPropertyName = "cCodCat"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cCodCat.DefaultCellStyle = DataGridViewCellStyle3
        Me.cCodCat.HeaderText = "Cod. Cat"
        Me.cCodCat.Name = "cCodCat"
        Me.cCodCat.ReadOnly = True
        Me.cCodCat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.ReadOnly = True
        Me.cDescripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cDescripcion.Width = 265
        '
        'cUndMed
        '
        Me.cUndMed.DataPropertyName = "cUndMed"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cUndMed.DefaultCellStyle = DataGridViewCellStyle4
        Me.cUndMed.HeaderText = "Und"
        Me.cUndMed.Name = "cUndMed"
        Me.cUndMed.ReadOnly = True
        Me.cUndMed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cUndMed.Width = 80
        '
        'nCantidad
        '
        Me.nCantidad.DataPropertyName = "nCantidad"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = "1.00"
        Me.nCantidad.DefaultCellStyle = DataGridViewCellStyle5
        Me.nCantidad.HeaderText = "Cant."
        Me.nCantidad.Name = "nCantidad"
        Me.nCantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCantidad.Width = 80
        '
        'nPrecio
        '
        Me.nPrecio.DataPropertyName = "nPrecio"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N6"
        DataGridViewCellStyle6.NullValue = "0.000000"
        Me.nPrecio.DefaultCellStyle = DataGridViewCellStyle6
        Me.nPrecio.HeaderText = "Costo Unit."
        Me.nPrecio.Name = "nPrecio"
        Me.nPrecio.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nPrecio.Width = 110
        '
        'nSubTot
        '
        Me.nSubTot.DataPropertyName = "nSubTot"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = "0.00"
        Me.nSubTot.DefaultCellStyle = DataGridViewCellStyle7
        Me.nSubTot.HeaderText = "Sub Total"
        Me.nSubTot.Name = "nSubTot"
        Me.nSubTot.ReadOnly = True
        Me.nSubTot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nSubTot.Width = 105
        '
        'nValVent
        '
        Me.nValVent.DataPropertyName = "nValVent"
        Me.nValVent.HeaderText = "nValVent"
        Me.nValVent.Name = "nValVent"
        Me.nValVent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nValVent.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(8, 424)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 16)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Sub Total:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(324, 424)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 16)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "IGV:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(575, 424)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 16)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Importe Total:"
        '
        'txtVentasGravadas
        '
        Me.txtVentasGravadas.BackColor = System.Drawing.SystemColors.Info
        Me.txtVentasGravadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVentasGravadas.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVentasGravadas.Location = New System.Drawing.Point(79, 417)
        Me.txtVentasGravadas.Name = "txtVentasGravadas"
        Me.txtVentasGravadas.Size = New System.Drawing.Size(94, 23)
        Me.txtVentasGravadas.TabIndex = 35
        Me.txtVentasGravadas.Text = "0.00"
        Me.txtVentasGravadas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIGV
        '
        Me.txtIGV.BackColor = System.Drawing.SystemColors.Info
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGV.Location = New System.Drawing.Point(360, 417)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.Size = New System.Drawing.Size(82, 23)
        Me.txtIGV.TabIndex = 36
        Me.txtIGV.Text = "0.00"
        Me.txtIGV.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.BackColor = System.Drawing.SystemColors.Info
        Me.txtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteTotal.Location = New System.Drawing.Point(669, 417)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(106, 23)
        Me.txtImporteTotal.TabIndex = 37
        Me.txtImporteTotal.Text = "0.00"
        Me.txtImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMensaje
        '
        Me.lblMensaje.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.ForeColor = System.Drawing.Color.Blue
        Me.lblMensaje.Location = New System.Drawing.Point(213, 168)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(560, 25)
        Me.lblMensaje.TabIndex = 123
        Me.lblMensaje.Text = "MENSAJE DE ESTADO"
        Me.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBuscarCodCat
        '
        Me.txtBuscarCodCat.Location = New System.Drawing.Point(11, 169)
        Me.txtBuscarCodCat.Name = "txtBuscarCodCat"
        Me.txtBuscarCodCat.Size = New System.Drawing.Size(103, 23)
        Me.txtBuscarCodCat.TabIndex = 8
        Me.txtBuscarCodCat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(304, 42)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(75, 16)
        Me.Label11.TabIndex = 128
        Me.Label11.Text = "F. Registro:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(8, 139)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(96, 16)
        Me.Label18.TabIndex = 137
        Me.Label18.Text = "Observaciones:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Location = New System.Drawing.Point(105, 116)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservaciones.Size = New System.Drawing.Size(333, 39)
        Me.txtObservaciones.TabIndex = 6
        '
        'btnBuscarProveedor
        '
        Me.btnBuscarProveedor.BackgroundImage = Global.Presentacion.My.Resources.Resources.BuscarArchivo_20
        Me.btnBuscarProveedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBuscarProveedor.FlatAppearance.BorderSize = 0
        Me.btnBuscarProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarProveedor.Location = New System.Drawing.Point(461, 62)
        Me.btnBuscarProveedor.Name = "btnBuscarProveedor"
        Me.btnBuscarProveedor.Size = New System.Drawing.Size(27, 25)
        Me.btnBuscarProveedor.TabIndex = 2
        Me.btnBuscarProveedor.UseVisualStyleBackColor = True
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.LightSkyBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 116
        '
        'btnFacturar
        '
        Me.btnFacturar.FlatAppearance.BorderSize = 0
        Me.btnFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFacturar.Image = Global.Presentacion.My.Resources.Resources.Facturar_22
        Me.btnFacturar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFacturar.Location = New System.Drawing.Point(700, 462)
        Me.btnFacturar.Name = "btnFacturar"
        Me.btnFacturar.Size = New System.Drawing.Size(75, 30)
        Me.btnFacturar.TabIndex = 13
        Me.btnFacturar.Text = "Grabar"
        Me.btnFacturar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFacturar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.FlatAppearance.BorderSize = 0
        Me.btnAnular.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnular.Image = Global.Presentacion.My.Resources.Resources.Anular_22
        Me.btnAnular.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAnular.Location = New System.Drawing.Point(337, 462)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 30)
        Me.btnAnular.TabIndex = 14
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(11, 462)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(91, 30)
        Me.btnGrabar.TabIndex = 12
        Me.btnGrabar.Text = "Registrar"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.BackgroundImage = Global.Presentacion.My.Resources.Resources.Delete_20
        Me.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Location = New System.Drawing.Point(137, 168)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(18, 25)
        Me.btnEliminar.TabIndex = 10
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.BackgroundImage = Global.Presentacion.My.Resources.Resources.Add_20
        Me.btnAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAgregar.FlatAppearance.BorderSize = 0
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Location = New System.Drawing.Point(115, 168)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(20, 25)
        Me.btnAgregar.TabIndex = 9
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(8, 96)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(58, 16)
        Me.Label16.TabIndex = 139
        Me.Label16.Text = "Tip.Ope:"
        '
        'cmbMotivoTras
        '
        Me.cmbMotivoTras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMotivoTras.FormattingEnabled = True
        Me.cmbMotivoTras.Location = New System.Drawing.Point(106, 88)
        Me.cmbMotivoTras.Name = "cmbMotivoTras"
        Me.cmbMotivoTras.Size = New System.Drawing.Size(237, 24)
        Me.cmbMotivoTras.TabIndex = 4
        '
        'FrmRegistroOrdenCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(785, 500)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.cmbMotivoTras)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.btnBuscarProveedor)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtBuscarCodCat)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.btnFacturar)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.txtImporteTotal)
        Me.Controls.Add(Me.txtIGV)
        Me.Controls.Add(Me.txtVentasGravadas)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNumCompra)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.dtpFechaVencimiento)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmbMoneda)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dtpFechaRegistro)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtProveedor)
        Me.Controls.Add(Me.cmbCondPago)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmRegistroOrdenCompra"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents cmbCondPago As System.Windows.Forms.ComboBox
    Friend WithEvents txtProveedor As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaRegistro As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents txtTipoCambio As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaVencimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNumCompra As System.Windows.Forms.Label
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents txtVentasGravadas As Label
    Friend WithEvents txtIGV As Label
    Friend WithEvents txtImporteTotal As Label
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnFacturar As System.Windows.Forms.Button
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents lblMensaje As Label
    Friend WithEvents txtBuscarCodCat As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents btnBuscarProveedor As Button
    Friend WithEvents Label18 As Label
    Friend WithEvents txtObservaciones As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cmbMotivoTras As ComboBox
    Friend WithEvents nCodCom As DataGridViewTextBoxColumn
    Friend WithEvents nCodProd As DataGridViewTextBoxColumn
    Friend WithEvents nNumOrd As DataGridViewTextBoxColumn
    Friend WithEvents cCodCat As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents cUndMed As DataGridViewTextBoxColumn
    Friend WithEvents nCantidad As DataGridViewTextBoxColumn
    Friend WithEvents nPrecio As DataGridViewTextBoxColumn
    Friend WithEvents nSubTot As DataGridViewTextBoxColumn
    Friend WithEvents nValVent As DataGridViewTextBoxColumn
End Class
