﻿Imports Negocios
Imports Entidades
Imports Business
Imports System.Windows.Forms
Public Class FrmRegistroOrdenCompra
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodProveedor As Integer = 0
    Dim CantidadPorducto As Decimal = 0
    Dim BEOrdenCompra As New BEOrdenCompra
    Dim BEProveedor As New BEProveedor
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion
    Dim Accion As Boolean = False
    Private Sub FrmRegistroOrdenCompra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargaMoneda()
        Call CargarMotivoTraslado()
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosCompra()
        End If
    End Sub
    Sub InicioOrdenCompra(ByVal oBEOrdenCompra As BEOrdenCompra, ByVal oBEProveedor As BEProveedor, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEOrdenCompra = oBEOrdenCompra
        BEProveedor = oBEProveedor
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosCompra()
        txtObservaciones.Text = BEOrdenCompra.gcObservaciones
        txtTipoCambio.Text = FormatNumber(BEOrdenCompra.gnTipCam, 4)
        dtpFechaVencimiento.Value = BEOrdenCompra.gdFecVen
        dtpFechaRegistro.Value = BEOrdenCompra.gdFecReg
        txtNumCompra.Text = BEOrdenCompra.gcNumOrd
        txtNumCompra.Tag = BEOrdenCompra.gnCodOrdCom
        txtProveedor.Text = BEProveedor.gcRazSoc
        txtVentasGravadas.Text = FormatNumber(BEOrdenCompra.gnVenGra, 2)
        txtIGV.Text = FormatNumber(BEOrdenCompra.gnIGV, 2)
        txtImporteTotal.Text = FormatNumber(BEOrdenCompra.gnImpTot, 2)
        cmbCondPago.SelectedValue = BEOrdenCompra.gnCondPag
        cmbMoneda.SelectedValue = BEOrdenCompra.gnTipMon
        cmbMotivoTras.SelectedValue = BEOrdenCompra.gcTipMov
        Call RecuperarDetalle()
        If BEOrdenCompra.gnEstado = 1 Then
            lblMensaje.Text = "ORDEN REGISTRADO(A) - " + BEOrdenCompra.g_cMensaje
            lblMensaje.ForeColor = Color.Blue
            Call CalcularTotales(cmbMotivoTras.SelectedValue)
            Call InterfaceGrabar()
        ElseIf BEOrdenCompra.gnEstado = 2 Then
            lblMensaje.Text = "ORDEN FACTURADO(A) - " + BEOrdenCompra.g_cMensaje
            lblMensaje.ForeColor = Color.Blue
            Call InterfaceFacturar()
        ElseIf BEOrdenCompra.gnEstado = 3 Then
            lblMensaje.Text = "ORDEN ANULADO(A) - " + BEOrdenCompra.g_cMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceFacturar()
        ElseIf BEOrdenCompra.gnEstado = 4 Then
            lblMensaje.Text = "ORDEN RELACIONADO(A) - " + BEOrdenCompra.g_cMensaje
            lblMensaje.ForeColor = Color.Blue
            Call InterfaceRelacionar()
        End If
        dtgListado.Focus()
        Call EnfocarFocus(6, 0)
    End Sub

    Sub CargaMoneda()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(70, "1,2,3,4,5,6,7,8")
        Call CargaCombo(dt, cmbCondPago)
    End Sub
    Sub CargarMotivoTraslado()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(150, "02,03")
        Call CargaCombo(dt, cmbMotivoTras)
    End Sub
    Sub Limpiar()
        dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        CantidadPorducto = 0
        LimpiaGrilla(dtgListado)
        lblMensaje.Text = ""
        lblMensaje.ForeColor = Color.Blue
        nCodProveedor = 0
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioCompra, 4)
        txtProveedor.Text = ""
        txtNumCompra.Text = ""
        txtNumCompra.Tag = 0
        txtObservaciones.Text = ""
        txtVentasGravadas.Text = "0.00"
        txtIGV.Text = "0.00"
        txtImporteTotal.Text = "0.00"
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)

    End Sub

    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub

    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub

    Private Sub dtpFechaVencimiento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaVencimiento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaVencimiento_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaVencimiento.ValueChanged

    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub cmbCondPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbCondPago.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbCondPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCondPago.SelectedIndexChanged

    End Sub
    Sub InterfaceNuevo()
        cmbMotivoTras.Enabled = True
        dtgListado.Enabled = False
        txtBuscarCodCat.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        btnAnular.Enabled = False
        txtObservaciones.Enabled = True
        txtTipoCambio.Enabled = False
        cmbCondPago.Enabled = True
        cmbMoneda.Enabled = True
        btnBuscarProveedor.Enabled = True
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = True
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If nCodProveedor = 0 Then
            MsgBox("Debe registrar el proveedor ", MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If
        Dim oDatos As New BEOrdenCompra
        cMensaje = ""
        oDatos.gnCodProve = nCodProveedor
        oDatos.gdFecReg = dtpFechaRegistro.Value
        oDatos.gdFecVen = dtpFechaVencimiento.Value
        oDatos.gnCondPag = cmbCondPago.SelectedValue
        oDatos.gnTipCam = txtTipoCambio.Text
        oDatos.gnTipMon = cmbMoneda.SelectedValue
        oDatos.gcObservaciones = txtObservaciones.Text
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.gcTipMov = cmbMotivoTras.SelectedValue
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
        Dim BLOrdenCompra As New BLOrdenCompra
        If BLOrdenCompra.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
            txtNumCompra.Tag = oDatos.gnCodOrdCom
            txtNumCompra.Text = oDatos.gcNumOrd
            lblMensaje.Text = "ORDEN REGISTRADA"
            Call InterfaceGrabar()
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If

    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        dtgListado.Columns("nCantidad").ReadOnly = Valor
        dtgListado.Columns("nPrecio").ReadOnly = Valor
    End Sub
    Sub InterfaceGrabar()
        Call BloquearEdicionGrilla(False)
        dtgListado.Enabled = True
        btnAgregar.Enabled = True
        btnEliminar.Enabled = True
        txtBuscarCodCat.Enabled = True
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        btnAnular.Enabled = True
        txtObservaciones.Enabled = False
        txtTipoCambio.Enabled = False
        cmbCondPago.Enabled = False
        cmbMoneda.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
        cmbMotivoTras.Enabled = False

    End Sub
    Sub InterfaceRelacionar()
        Call BloquearEdicionGrilla(True)
        dtgListado.Enabled = True
        cmbMotivoTras.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        txtBuscarCodCat.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        btnAnular.Enabled = False
        txtTipoCambio.Enabled = False
        cmbCondPago.Enabled = False
        cmbMoneda.Enabled = False
        txtObservaciones.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
    End Sub
    Sub InterfaceFacturar()
        Call BloquearEdicionGrilla(True)
        dtgListado.Enabled = True
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        cmbMotivoTras.Enabled = False
        txtBuscarCodCat.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        btnAnular.Enabled = True
        txtTipoCambio.Enabled = False
        cmbCondPago.Enabled = False
        cmbMoneda.Enabled = False
        txtObservaciones.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        FrmConsultaProducto.Inicio(txtNumCompra.Tag, "70")
        FrmConsultaProducto = Nothing
        Call CalcularTotales(cmbMotivoTras.SelectedValue)
        dtgListado.Focus()
        Call EnfocarFocus(6, dtgListado.Rows.Count - 1)
    End Sub
    Sub AgregarProductoDetalle(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double)
        cMensaje = ""
        Dim BLOrdenCompra As New BLOrdenCompra
        If BLOrdenCompra.AgregarProducto(txtNumCompra.Tag, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnEditar_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If Fila <> 0 Then
            Fila = Fila - 1
        End If
        Call EliminarProductoDetalle(txtNumCompra.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value)
        Call RecuperarDetalle()
        Call CalcularTotales(cmbMotivoTras.SelectedValue)
        dtgListado.Focus()
        Call EnfocarFocus(Columna, Fila)
    End Sub
    Sub EliminarProductoDetalle(ByVal CodCot As Integer, ByVal nCodProd As Integer)
        cMensaje = ""
        Dim BLOrdenCompra As New BLOrdenCompra
        If BLOrdenCompra.EliminarProducto(CodCot, nCodProd, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        Dim oDatos As New BEOrdenCompra
        If MsgBox("¿Esta seguro que desea anular la orden de compra?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodOrdCom = txtNumCompra.Tag
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            Dim BLOrdenCompra As New BLOrdenCompra
            If BLOrdenCompra.Anular(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnAnular_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAnular.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Public Sub RecuperarDetalle()
        Dim dt As New DataTable
        Dim BLOrdenCompra As New BLOrdenCompra
        dt = BLOrdenCompra.RecuperarDetalle(txtNumCompra.Tag)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Sub CalcularTotales(ByVal Codigo As String)
        Dim ValorVenta, TOTAL, IGV As Double
        If dtgListado.Rows.Count > 0 Then
            TOTAL = 0
            ValorVenta = 0
            IGV = 0
            If Codigo = "02" Then
                For i = 0 To dtgListado.Rows.Count - 1
                    ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
                Next
                txtVentasGravadas.Text = FormatNumber(ValorVenta, 2)
                IGV = FormatNumber(ValorVenta * 0.18, 2)
                txtIGV.Text = FormatNumber(IGV, 2)
                txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
            Else
                For i = 0 To dtgListado.Rows.Count - 1
                    ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
                    TOTAL = TOTAL + CDbl(dtgListado.Rows(i).Cells("nSubTot").Value)
                Next
                IGV = FormatNumber(TOTAL - ValorVenta, 2)
                txtVentasGravadas.Text = FormatNumber(ValorVenta, 2)
                txtIGV.Text = FormatNumber(IGV, 2)
                txtImporteTotal.Text = FormatNumber(TOTAL, 2)
            End If

        Else
            txtVentasGravadas.Text = FormatNumber(0, 2)
            txtIGV.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
        End If
    End Sub

    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar una orden sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            Exit Function
        End If
        Return validaFacturacion
    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BEOrdenCompra
        If validaFacturacion() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea grabar la orden?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodOrdCom = txtNumCompra.Tag
            oDatos.gnVenGra = txtVentasGravadas.Text
            oDatos.gnIGV = txtIGV.Text
            oDatos.gnImpTot = txtImporteTotal.Text
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            Dim BLOrdenCompra As New BLOrdenCompra
            If BLOrdenCompra.Facturar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                MsgBox("La orden de compra N°:" + txtNumCompra.Text + " fue grabado correctamente", MsgBoxStyle.Information, "MENSAJE")
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub txtBuscarCodCat_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarCodCat.TextChanged

    End Sub
    Sub ObtenerProducto()
        Dim CodProd As Integer = 0
        Dim dt As New DataTable
        Dim BLProductos As New BLProducts
        dt = BLProductos.ObtenerProducto(txtBuscarCodCat.Text, 1, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
        If dt.Rows.Count > 0 Then
            CodProd = ObtenerDato("nCodProd", dt)
            Call AgregarProductoDetalle(CodProd, 1, 0)
            Call RecuperarDetalle()
            Call CalcularTotales(cmbMotivoTras.SelectedValue)
            txtBuscarCodCat.Clear()
            txtBuscarCodCat.Select()
            Call EnfocarFocus(6, dtgListado.Rows.Count - 1)
        Else
            MsgBox("El código catálogo registrado no existe", MsgBoxStyle.Exclamation, "MENSAJE")
            txtBuscarCodCat.Focus()
        End If

    End Sub
    Private Sub txtBuscarCodCat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscarCodCat.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
        If e.KeyChar = Chr(13) Then
            Call ObtenerProducto()
        End If
    End Sub

    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If IsDBNull(dtgListado.CurrentRow.Cells("nCantidad").Value) Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        If dtgListado.CurrentRow.Cells("nCantidad").Value = "0" Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        cMensaje = ""
        Dim BLOrdenCompra As New BLOrdenCompra
        If BLOrdenCompra.ModificarProducto(txtNumCompra.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value,
                                                      dtgListado.CurrentRow.Cells("nCantidad").Value,
                                                        CantidadPorducto,
                                                      dtgListado.CurrentRow.Cells("nPrecio").Value, cMensaje) Then
            If (cMensaje.Trim).Length > 0 Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            LimpiaGrilla(dtgListado)
            Call RecuperarDetalle()
            Call EnfocarFocus(Columna, Fila)
            Call CalcularTotales(cmbMotivoTras.SelectedValue)
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub EnfocarFocus(ByVal Columna As Integer, ByVal Fila As Integer)
        If dtgListado.Rows.Count > 0 Then
            Me.dtgListado.CurrentCell = Me.dtgListado(Columna, Fila)
            If Fila <> dtgListado.Rows.Count - 1 Then
                SendKeys.Send(“{UP}”)
            End If
            SendKeys.Send(“{TAB}”)
        End If
    End Sub
    Private Sub btnBuscarProveedor_Click(sender As Object, e As EventArgs) Handles btnBuscarProveedor.Click
        Dim BEProveedor As New BEProveedor
        FrmConsultaProveedor.Inicio(BEProveedor)
        FrmConsultaProveedor = Nothing
        If BEProveedor.gnCodProv <> 0 Then
            nCodProveedor = BEProveedor.gnCodProv
            txtProveedor.Text = BEProveedor.gcRazSoc
        Else
            btnBuscarProveedor.Focus()
        End If
    End Sub

    Private Sub dtgListado_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dtgListado.CellBeginEdit
        CantidadPorducto = dtgListado.CurrentRow.Cells("nCantidad").Value
    End Sub
    Private Sub btnBuscarProveedor_ImeModeChanged(sender As Object, e As EventArgs) Handles btnBuscarProveedor.ImeModeChanged
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnBuscarProveedor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarProveedor.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMotivoTras_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMotivoTras.SelectedIndexChanged

    End Sub

    Private Sub cmbMotivoTras_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMotivoTras.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub lblMensaje_Click(sender As Object, e As EventArgs) Handles lblMensaje.Click

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
        If (e.KeyCode = Keys.Return) Then
            e.Handled = True
            SendKeys.Send(“{TAB}”)
        End If
    End Sub

    Private Sub FrmRegistroOrdenCompra_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
            'ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            '    Call btnGrabar_Click(sender, e)
            'ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            '    Call btnFacturar_Click(sender, e)
            'ElseIf e.KeyCode = Keys.F4 And btnAnular.Enabled = True And btnAnular.Visible = True Then
            '    Call btnAnular_Click(sender, e)
        End If
    End Sub
    Private Sub cmbCondPago_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbCondPago.SelectionChangeCommitted
        If cmbCondPago.SelectedValue = 1 Or cmbCondPago.SelectedValue = 2 Then
            dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
        ElseIf cmbCondPago.SelectedValue = 3 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 7, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 4 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 15, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 5 Or cmbCondPago.SelectedValue = 9 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 30, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 6 Or cmbCondPago.SelectedValue = 10 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 45, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 7 Or cmbCondPago.SelectedValue = 11 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 60, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 12 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 75, dtpFechaRegistro.Value)
        ElseIf cmbCondPago.SelectedValue = 8 Or cmbCondPago.SelectedValue = 13 Then
            dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 90, dtpFechaRegistro.Value)
        End If
    End Sub
End Class