﻿Imports Negocios
Imports Entidades
Imports Business
Imports System.Windows.Forms
Public Class FrmRegistroOrdenCompraImportacion
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodProveedor As Integer = 0
    Dim CantidadPorducto As Decimal = 0
    Dim BEOrdenCompraImportacion As New BEOrdenCompraImportacion
    Dim BEProveedor As New BEProveedor
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion
    Dim Accion As Boolean = False
    Private Sub FrmRegistroOrdenCompraImportacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaMoneda()
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosCompra()
        End If
    End Sub
    Sub InicioOrdenCompra(ByVal oBEOrdenCompra As BEOrdenCompraImportacion, ByVal oBEProveedor As BEProveedor, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)

        BEOrdenCompraImportacion = oBEOrdenCompra
        BEProveedor = oBEProveedor
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub MostrarDatosCompra()
        txtObservaciones.Text = BEOrdenCompraImportacion.gcObservaciones
        dtpFechaRegistro.Value = BEOrdenCompraImportacion.gdFecReg
        txtNumCompra.Text = BEOrdenCompraImportacion.gcNumOrd
        txtNumCompra.Tag = BEOrdenCompraImportacion.gnCodOrdCom
        txtProveedor.Text = BEProveedor.gcRazSoc
        txtImporteTotal.Text = FormatNumber(BEOrdenCompraImportacion.gnImpTotPre, 2)
        cmbMoneda.SelectedValue = BEOrdenCompraImportacion.gnTipMon
        Call RecuperarDetalle()
        If BEOrdenCompraImportacion.gnEstado = 1 Then
            lblMensaje.Text = "ORDEN REGISTRADO(A) - " + BEOrdenCompraImportacion.g_cMensaje
            lblMensaje.ForeColor = Color.Blue
            Call CalcularTotales()
            Call InterfaceGrabar()
        ElseIf BEOrdenCompraImportacion.gnEstado = 2 Then
            lblMensaje.Text = "ORDEN FACTURADO(A) - " + BEOrdenCompraImportacion.g_cMensaje
            lblMensaje.ForeColor = Color.Blue
            Call InterfaceFacturar()
        ElseIf BEOrdenCompraImportacion.gnEstado = 3 Then
            lblMensaje.Text = "ORDEN ANULADO(A) - " + BEOrdenCompraImportacion.g_cMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceFacturar()
        ElseIf BEOrdenCompraImportacion.gnEstado = 4 Then
            lblMensaje.Text = "ORDEN RELACIONADO(A) - " + BEOrdenCompraImportacion.g_cMensaje
            lblMensaje.ForeColor = Color.Blue
            Call InterfaceRelacionar()
        End If
        dtgListado.Focus()
        Call EnfocarFocus(6, 0)
    End Sub

    Sub CargaMoneda()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(60, "2")
        Call CargaCombo(dt, cmbMoneda)
    End Sub
    Sub Limpiar()
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        CantidadPorducto = 0
        LimpiaGrilla(dtgListado)
        lblMensaje.Text = ""
        lblMensaje.ForeColor = Color.Blue
        nCodProveedor = 0
        txtProveedor.Text = ""
        txtNumCompra.Text = ""
        txtNumCompra.Tag = 0
        txtObservaciones.Text = ""
        txtImporteTotal.Text = "0.00"
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub
    Sub InterfaceNuevo()
        dtgListado.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        txtObservaciones.Enabled = True
        cmbMoneda.Enabled = True
        btnBuscarProveedor.Enabled = True
        dtpFechaRegistro.Enabled = False
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If nCodProveedor = 0 Then
            MsgBox("Debe registrar el proveedor ", MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Sub
        End If
        Dim oDatos As New BEOrdenCompraImportacion
        cMensaje = ""
        oDatos.gnCodProve = nCodProveedor
        oDatos.gdFecReg = dtpFechaRegistro.Value
        oDatos.gnTipMon = cmbMoneda.SelectedValue
        oDatos.gcObservaciones = txtObservaciones.Text
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
        Dim Obj As New BLOrdenCompraImportacion
        If Obj.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
            txtNumCompra.Tag = oDatos.gnCodOrdCom
            txtNumCompra.Text = oDatos.gcNumOrd
            lblMensaje.Text = "ORDEN REGISTRADA"
            Call InterfaceGrabar()
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If

    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        dtgListado.Columns("nCantidad").ReadOnly = Valor
        dtgListado.Columns("nPreUni").ReadOnly = Valor
    End Sub
    Sub InterfaceGrabar()
        Call BloquearEdicionGrilla(False)
        dtgListado.Enabled = True
        btnAgregar.Enabled = True
        btnEliminar.Enabled = True
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        txtObservaciones.Enabled = False
        cmbMoneda.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
    End Sub
    Sub InterfaceRelacionar()
        Call BloquearEdicionGrilla(True)
        dtgListado.Enabled = True
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        cmbMoneda.Enabled = False
        txtObservaciones.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
    End Sub
    Sub InterfaceFacturar()
        Call BloquearEdicionGrilla(True)
        dtgListado.Enabled = True
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        cmbMoneda.Enabled = False
        txtObservaciones.Enabled = False
        btnBuscarProveedor.Enabled = False
        dtpFechaRegistro.Enabled = False
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Public Sub RecuperarDetalle()
        Dim dt As New DataTable
        Dim Obj As New BLOrdenCompraImportacion
        dt = Obj.RecuperarDetalle(txtNumCompra.Tag)
        Call LlenaAGridView(dt, dtgListado)
    End Sub
    Sub CalcularTotales()
        Dim TOTAL As Double
        If dtgListado.Rows.Count > 0 Then
            TOTAL = 0
            For i = 0 To dtgListado.Rows.Count - 1
                TOTAL = TOTAL + CDbl(dtgListado.Rows(i).Cells("nTotPre").Value)
            Next
            txtImporteTotal.Text = FormatNumber(TOTAL, 2)
        Else
            txtImporteTotal.Text = FormatNumber(0, 2)
        End If
    End Sub

    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar una orden sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            Exit Function
        End If
        Return validaFacturacion
    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BEOrdenCompra
        If validaFacturacion() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea grabar la orden?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodOrdCom = txtNumCompra.Tag
            oDatos.gnImpTot = txtImporteTotal.Text
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            Dim BLOrdenCompra As New BLOrdenCompra
            If BLOrdenCompra.Facturar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                MsgBox("La orden de compra N°:" + txtNumCompra.Text + " fue grabado correctamente", MsgBoxStyle.Information, "MENSAJE")
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If IsDBNull(dtgListado.CurrentRow.Cells("nCantidad").Value) Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        If dtgListado.CurrentRow.Cells("nCantidad").Value = "0" Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        cMensaje = ""
        Dim Obj As New BLOrdenCompraImportacion
        If Obj.ModificarProducto(txtNumCompra.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value,
                                                      dtgListado.CurrentRow.Cells("nCantidad").Value,
                                                        CantidadPorducto,
                                                      dtgListado.CurrentRow.Cells("nPreUni").Value, cMensaje) Then
            If (cMensaje.Trim).Length > 0 Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            LimpiaGrilla(dtgListado)
            Call RecuperarDetalle()
            Call EnfocarFocus(Columna, Fila)
            Call CalcularTotales()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub EnfocarFocus(ByVal Columna As Integer, ByVal Fila As Integer)
        If dtgListado.Rows.Count > 0 Then
            Me.dtgListado.CurrentCell = Me.dtgListado(Columna, Fila)
            If Fila <> dtgListado.Rows.Count - 1 Then
                SendKeys.Send(“{UP}”)
            End If
            SendKeys.Send(“{TAB}”)
        End If
    End Sub
    Private Sub btnBuscarProveedor_Click(sender As Object, e As EventArgs) Handles btnBuscarProveedor.Click
        Dim BEProveedor As New BEProveedor
        FrmConsultaProveedor.Inicio(BEProveedor)
        FrmConsultaProveedor = Nothing
        If BEProveedor.gnCodProv <> 0 Then
            nCodProveedor = BEProveedor.gnCodProv
            txtProveedor.Text = BEProveedor.gcRazSoc
        Else
            btnBuscarProveedor.Focus()
        End If
    End Sub

    Private Sub dtgListado_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dtgListado.CellBeginEdit
        CantidadPorducto = dtgListado.CurrentRow.Cells("nCantidad").Value
    End Sub
    Private Sub btnBuscarProveedor_ImeModeChanged(sender As Object, e As EventArgs) Handles btnBuscarProveedor.ImeModeChanged
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnBuscarProveedor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarProveedor.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub lblMensaje_Click(sender As Object, e As EventArgs) Handles lblMensaje.Click

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
        If (e.KeyCode = Keys.Return) Then
            e.Handled = True
            SendKeys.Send(“{TAB}”)
        End If
    End Sub

    Private Sub FrmRegistroOrdenCompraImportacion_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
            'ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            '    Call btnGrabar_Click(sender, e)
            'ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            '    Call btnFacturar_Click(sender, e)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If Fila <> 0 Then
            Fila = Fila - 1
        End If
        Call EliminarProductoDetalle(txtNumCompra.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value)
        Call RecuperarDetalle()
        Call CalcularTotales()
        dtgListado.Focus()
        Call EnfocarFocus(Columna, Fila)
    End Sub
    Sub EliminarProductoDetalle(ByVal CodCot As Integer, ByVal nCodProd As Integer)
        cMensaje = ""
        Dim Obj As New BLOrdenCompraImportacion
        If Obj.EliminarProducto(CodCot, nCodProd, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Focus()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        FrmConsultaProducto.Inicio(txtNumCompra.Tag, "100")
        FrmConsultaProducto = Nothing
        Call CalcularTotales()
        dtgListado.Focus()
        Call EnfocarFocus(6, dtgListado.Rows.Count - 1)
    End Sub

    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub tabGeneral_Click(sender As Object, e As EventArgs) Handles tabGeneral.Click

    End Sub

    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class