﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmRegistroRetenciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.txtCliente = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFechaRegistro = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNumRegistro = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.nCodPed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNumComp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonRet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nImpNetPag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtImporteTotal = New System.Windows.Forms.Label()
        Me.txtMontoRetencion = New System.Windows.Forms.Label()
        Me.txtImporteTotalNeto = New System.Windows.Forms.Label()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.btnHome = New System.Windows.Forms.Button()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnBuscarCliente = New System.Windows.Forms.Button()
        Me.txtNumRetencion = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpFechaEmision = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmbMoneda = New System.Windows.Forms.ComboBox()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(683, 29)
        Me.tlsTitulo.TabIndex = 0
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(182, 26)
        Me.lblTitulo.Text = "APLICACIÓN  DE RETENCIÓN"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'txtCliente
        '
        Me.txtCliente.BackColor = System.Drawing.SystemColors.Info
        Me.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCliente.Location = New System.Drawing.Point(100, 62)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(322, 23)
        Me.txtCliente.TabIndex = 5
        Me.txtCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 16)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Cliente:"
        '
        'dtpFechaRegistro
        '
        Me.dtpFechaRegistro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaRegistro.Location = New System.Drawing.Point(314, 36)
        Me.dtpFechaRegistro.Name = "dtpFechaRegistro"
        Me.dtpFechaRegistro.Size = New System.Drawing.Size(108, 23)
        Me.dtpFechaRegistro.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(236, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "F. Registro:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 43)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 16)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "N° Registro:"
        '
        'txtNumRegistro
        '
        Me.txtNumRegistro.BackColor = System.Drawing.SystemColors.Info
        Me.txtNumRegistro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumRegistro.Location = New System.Drawing.Point(100, 36)
        Me.txtNumRegistro.Name = "txtNumRegistro"
        Me.txtNumRegistro.Size = New System.Drawing.Size(126, 23)
        Me.txtNumRegistro.TabIndex = 18
        Me.txtNumRegistro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 9.75!)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodPed, Me.Fecha, Me.cNumComp, Me.cMoneda, Me.nImpTot, Me.nMonRet, Me.nImpNetPag})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(11, 91)
        Me.dtgListado.Name = "dtgListado"
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListado.Size = New System.Drawing.Size(663, 236)
        Me.dtgListado.TabIndex = 8
        '
        'nCodPed
        '
        Me.nCodPed.DataPropertyName = "nCodPed"
        Me.nCodPed.HeaderText = "nCodPed"
        Me.nCodPed.Name = "nCodPed"
        Me.nCodPed.ReadOnly = True
        Me.nCodPed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodPed.Visible = False
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle9
        Me.Fecha.HeaderText = "Fec.Emisión"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Fecha.Width = 90
        '
        'cNumComp
        '
        Me.cNumComp.DataPropertyName = "cNumComp"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumComp.DefaultCellStyle = DataGridViewCellStyle10
        Me.cNumComp.HeaderText = "N° Comprobante"
        Me.cNumComp.Name = "cNumComp"
        Me.cNumComp.ReadOnly = True
        Me.cNumComp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cNumComp.Width = 140
        '
        'cMoneda
        '
        Me.cMoneda.DataPropertyName = "cMoneda"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cMoneda.DefaultCellStyle = DataGridViewCellStyle11
        Me.cMoneda.HeaderText = "Moneda"
        Me.cMoneda.Name = "cMoneda"
        Me.cMoneda.ReadOnly = True
        Me.cMoneda.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nImpTot
        '
        Me.nImpTot.DataPropertyName = "nImpTot"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = "0.00"
        Me.nImpTot.DefaultCellStyle = DataGridViewCellStyle12
        Me.nImpTot.HeaderText = "Imp.Total"
        Me.nImpTot.Name = "nImpTot"
        Me.nImpTot.ReadOnly = True
        Me.nImpTot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nImpTot.Width = 110
        '
        'nMonRet
        '
        Me.nMonRet.DataPropertyName = "nMonRet"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = "0.00"
        Me.nMonRet.DefaultCellStyle = DataGridViewCellStyle13
        Me.nMonRet.HeaderText = "Imp.Retención"
        Me.nMonRet.Name = "nMonRet"
        Me.nMonRet.ReadOnly = True
        Me.nMonRet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nMonRet.Width = 110
        '
        'nImpNetPag
        '
        Me.nImpNetPag.DataPropertyName = "nImpNetPag"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = "0.00"
        Me.nImpNetPag.DefaultCellStyle = DataGridViewCellStyle14
        Me.nImpNetPag.HeaderText = "Imp.Neto"
        Me.nImpNetPag.Name = "nImpNetPag"
        Me.nImpNetPag.ReadOnly = True
        Me.nImpNetPag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nImpNetPag.Width = 110
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(437, 365)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(91, 16)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Importe Total:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(417, 389)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(112, 16)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "Importe Retenido:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(410, 413)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(120, 16)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Importe Neto Pago:"
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.BackColor = System.Drawing.SystemColors.Info
        Me.txtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteTotal.Location = New System.Drawing.Point(532, 358)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(142, 23)
        Me.txtImporteTotal.TabIndex = 35
        Me.txtImporteTotal.Text = "0.00"
        Me.txtImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMontoRetencion
        '
        Me.txtMontoRetencion.BackColor = System.Drawing.SystemColors.Info
        Me.txtMontoRetencion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoRetencion.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMontoRetencion.Location = New System.Drawing.Point(532, 382)
        Me.txtMontoRetencion.Name = "txtMontoRetencion"
        Me.txtMontoRetencion.Size = New System.Drawing.Size(142, 23)
        Me.txtMontoRetencion.TabIndex = 36
        Me.txtMontoRetencion.Text = "0.00"
        Me.txtMontoRetencion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtImporteTotalNeto
        '
        Me.txtImporteTotalNeto.BackColor = System.Drawing.SystemColors.Info
        Me.txtImporteTotalNeto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotalNeto.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteTotalNeto.Location = New System.Drawing.Point(532, 406)
        Me.txtImporteTotalNeto.Name = "txtImporteTotalNeto"
        Me.txtImporteTotalNeto.Size = New System.Drawing.Size(142, 23)
        Me.txtImporteTotalNeto.TabIndex = 37
        Me.txtImporteTotalNeto.Text = "0.00"
        Me.txtImporteTotalNeto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 116
        '
        'btnHome
        '
        Me.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnHome.FlatAppearance.BorderSize = 0
        Me.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHome.Image = Global.Presentacion.My.Resources.Resources.Home_20
        Me.btnHome.Location = New System.Drawing.Point(639, 444)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Size = New System.Drawing.Size(35, 30)
        Me.btnHome.TabIndex = 8
        Me.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnHome.UseVisualStyleBackColor = True
        '
        'btnBuscar
        '
        Me.btnBuscar.FlatAppearance.BorderSize = 0
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.Image = Global.Presentacion.My.Resources.Resources.Search_22
        Me.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuscar.Location = New System.Drawing.Point(563, 444)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 30)
        Me.btnBuscar.TabIndex = 7
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(87, 444)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(78, 30)
        Me.btnGrabar.TabIndex = 6
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatAppearance.BorderSize = 0
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Image = Global.Presentacion.My.Resources.Resources.New_22
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(11, 444)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 30)
        Me.btnNuevo.TabIndex = 5
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnBuscarCliente
        '
        Me.btnBuscarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBuscarCliente.FlatAppearance.BorderSize = 0
        Me.btnBuscarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarCliente.Image = Global.Presentacion.My.Resources.Resources.BuscarArchivo_20
        Me.btnBuscarCliente.Location = New System.Drawing.Point(423, 61)
        Me.btnBuscarCliente.Name = "btnBuscarCliente"
        Me.btnBuscarCliente.Size = New System.Drawing.Size(22, 25)
        Me.btnBuscarCliente.TabIndex = 2
        Me.btnBuscarCliente.UseVisualStyleBackColor = True
        '
        'txtNumRetencion
        '
        Me.txtNumRetencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumRetencion.Location = New System.Drawing.Point(546, 62)
        Me.txtNumRetencion.Name = "txtNumRetencion"
        Me.txtNumRetencion.Size = New System.Drawing.Size(128, 23)
        Me.txtNumRetencion.TabIndex = 3
        Me.txtNumRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(457, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 16)
        Me.Label4.TabIndex = 125
        Me.Label4.Text = "N° Retención:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(11, 331)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(371, 16)
        Me.Label1.TabIndex = 126
        Me.Label1.Text = "Para eliminar un registro de la lista presione SUPRIMIR/DELETE"
        '
        'dtpFechaEmision
        '
        Me.dtpFechaEmision.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaEmision.Location = New System.Drawing.Point(571, 36)
        Me.dtpFechaEmision.Name = "dtpFechaEmision"
        Me.dtpFechaEmision.Size = New System.Drawing.Size(103, 23)
        Me.dtpFechaEmision.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(495, 41)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 16)
        Me.Label5.TabIndex = 128
        Me.Label5.Text = "F. Emisión:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(471, 339)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 16)
        Me.Label8.TabIndex = 150
        Me.Label8.Text = "Moneda:"
        '
        'cmbMoneda
        '
        Me.cmbMoneda.BackColor = System.Drawing.Color.White
        Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMoneda.FormattingEnabled = True
        Me.cmbMoneda.Location = New System.Drawing.Point(532, 331)
        Me.cmbMoneda.Name = "cmbMoneda"
        Me.cmbMoneda.Size = New System.Drawing.Size(142, 24)
        Me.cmbMoneda.TabIndex = 4
        '
        'FrmRegistroRetenciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(683, 483)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cmbMoneda)
        Me.Controls.Add(Me.dtpFechaEmision)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNumRetencion)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnBuscarCliente)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.btnHome)
        Me.Controls.Add(Me.txtImporteTotalNeto)
        Me.Controls.Add(Me.txtMontoRetencion)
        Me.Controls.Add(Me.txtImporteTotal)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNumRegistro)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpFechaRegistro)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmRegistroRetenciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents txtCliente As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaRegistro As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNumRegistro As System.Windows.Forms.Label
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents btnBuscar As Button
    Friend WithEvents txtImporteTotal As Label
    Friend WithEvents txtMontoRetencion As Label
    Friend WithEvents txtImporteTotalNeto As Label
    Friend WithEvents btnHome As System.Windows.Forms.Button
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents btnBuscarCliente As Button
    Friend WithEvents txtNumRetencion As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents dtpFechaEmision As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents cmbMoneda As ComboBox
    Friend WithEvents nCodPed As DataGridViewTextBoxColumn
    Friend WithEvents Fecha As DataGridViewTextBoxColumn
    Friend WithEvents cNumComp As DataGridViewTextBoxColumn
    Friend WithEvents cMoneda As DataGridViewTextBoxColumn
    Friend WithEvents nImpTot As DataGridViewTextBoxColumn
    Friend WithEvents nMonRet As DataGridViewTextBoxColumn
    Friend WithEvents nImpNetPag As DataGridViewTextBoxColumn
End Class
