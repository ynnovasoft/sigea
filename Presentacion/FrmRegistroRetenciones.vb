﻿Imports Negocios
Imports Business
Imports Entidades
Imports Entities
Public Class FrmRegistroRetenciones
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodCli As Integer = 0
    Dim CodigoRetencion As Integer = 0
    Private Sub FrmRegistroRetenciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaCombos()
        Call Limpiar()
        Call InterfaceEntrada()
        btnNuevo.Focus()
    End Sub
    Sub Limpiar()
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        dtpFechaEmision.Value = MDIPrincipal.FechaSistema
        nCodCli = 0
        CodigoRetencion = 0
        txtCliente.Text = ""
        txtNumRegistro.Text = ""
        txtNumRetencion.Text = ""
        txtImporteTotal.Text = "0.00"
        txtMontoRetencion.Text = "0.00"
        txtImporteTotalNeto.Text = "0.00"
        Call LimpiaGrilla(dtgListado)
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Call Limpiar()
        Call InterfaceNuevo()
        btnBuscarCliente.Focus()
    End Sub
    Sub InterfaceNuevo()
        txtNumRetencion.Enabled = True
        dtgListado.Enabled = False
        btnNuevo.Enabled = False
        btnGrabar.Enabled = True
        btnBuscar.Enabled = False
        btnBuscarCliente.Enabled = True
        dtpFechaRegistro.Enabled = False
        dtpFechaEmision.Enabled = True
        cmbMoneda.Enabled = True
    End Sub
    Private Sub btnNuevo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnNuevo.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Function validacion() As Boolean
        validacion = True
        If nCodCli = 0 Then
            validacion = False
            MsgBox("Debe registrar el cliente ", MsgBoxStyle.Exclamation, "MENSAJE")
            btnBuscarCliente.Focus()
            Exit Function
        End If
        If dtgListado.Rows.Count <= 0 Then
            validacion = False
            MsgBox("No se puede facturar una  retención sin ningun item", MsgBoxStyle.Exclamation, "Mensaje")
            dtgListado.Focus()
            Exit Function
        End If
        If txtNumRetencion.Text = "" Then
            validacion = False
            MsgBox("Debe registrar el número de retención", MsgBoxStyle.Exclamation, "Mensaje")
            txtNumRetencion.Focus()
            Exit Function
        End If
        Return validacion

    End Function
    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If validacion() = False Then
            Exit Sub
        End If
        Dim oDatos As New BERetencion
        If MsgBox("¿Esta seguro que desea registrar la retención?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbYes Then
            cMensaje = ""
            oDatos.gnCodCli = nCodCli
            oDatos.gdFecReg = dtpFechaRegistro.Value
            oDatos.gdFecEmi = dtpFechaEmision.Value
            oDatos.gcNumRet = txtNumRetencion.Text
            oDatos.gnImpTot = txtImporteTotal.Text
            oDatos.gnMonRet = txtMontoRetencion.Text
            oDatos.gnImpNetPag = txtImporteTotalNeto.Text
            oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
            oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
            oDatos.gnTipMon = cmbMoneda.SelectedValue
            oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
            Dim BLRetencion As New BLRetencion
            If BLRetencion.Registrar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                txtNumRegistro.Text = oDatos.gnCodRet
                For i = 0 To dtgListado.Rows.Count - 1
                    oDatos.gnCodRet = txtNumRegistro.Text
                    oDatos.gcNumRet = txtNumRetencion.Text
                    oDatos.gnCodCli = dtgListado.Rows(i).Cells("nCodPed").Value
                    oDatos.gnImpTot = dtgListado.Rows(i).Cells("nImpTot").Value
                    oDatos.gnMonRet = dtgListado.Rows(i).Cells("nMonRet").Value
                    oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
                    oDatos.gnImpNetPag = dtgListado.Rows(i).Cells("nImpNetPag").Value
                    oDatos.gdFecReg = dtpFechaRegistro.Value
                    oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
                    oDatos.gnTipMon = cmbMoneda.SelectedValue
                    oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
                    BLRetencion.RegistrarDetalle(oDatos, cMensaje)
                Next
                Call Limpiar()
                Call InterfaceEntrada()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        dtgListado.Columns("nMonRet").ReadOnly = Valor
    End Sub
    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim BEClientes As New BECustomer
        Dim BERetencion As New BERetencion
        Call Limpiar()
        FrmConsultaRetencion.InicioRetencion(BERetencion, BEClientes)
        FrmConsultaRetencion = Nothing
        If BERetencion.gnCodRet <> 0 Then

            txtNumRegistro.Text = BERetencion.gnCodRet
            dtpFechaRegistro.Value = BERetencion.gdFecReg
            dtpFechaEmision.Value = BERetencion.gdFecEmi
            CodigoRetencion = BERetencion.gnCodRet
            txtImporteTotal.Text = FormatNumber(BERetencion.gnImpTot, 2)
            txtMontoRetencion.Text = FormatNumber(BERetencion.gnMonRet, 2)
            txtImporteTotalNeto.Text = FormatNumber(BERetencion.gnImpNetPag, 2)
            txtNumRetencion.Text = BERetencion.gcNumRet
            cmbMoneda.SelectedValue = BERetencion.gnTipMon

            txtCliente.Text = BEClientes.cRazSoc
            Call InterfaceEntrada()
            btnNuevo.Enabled = False
            btnBuscar.Enabled = True
            Call RecuperarDetalle(BERetencion.gnCodRet, 1)

            Call BloquearEdicionGrilla(True)
            btnHome.Focus()
        End If
    End Sub

    Sub RecuperarDetalle(ByVal Codigo As Integer, ByVal TipoBusqueda As Integer)
        Dim dt As DataTable
        Dim BLRetencion As New BLRetencion
        dt = BLRetencion.RecuperarDetalle(Codigo, TipoBusqueda, MDIPrincipal.CodigoEmpresa)
        Call LlenaAGridView(dt, dtgListado)
        If dtgListado.Rows.Count > 0 Then
            dtgListado.Enabled = True
        Else
            dtgListado.Enabled = False
        End If
        Call CalcularTotales()
    End Sub

    Private Sub btnBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Call Limpiar()
        Call InterfaceEntrada()
        btnNuevo.Focus()
    End Sub
    Sub InterfaceEntrada()
        dtpFechaEmision.Enabled = False
        dtgListado.Enabled = False
        btnNuevo.Enabled = True
        txtNumRetencion.Enabled = False
        btnGrabar.Enabled = False
        btnBuscar.Enabled = True
        btnBuscarCliente.Enabled = False
        dtpFechaRegistro.Enabled = False
        cmbMoneda.Enabled = False
    End Sub

    Private Sub btnHome_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnHome.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick

    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        'Dim TOTAL, RETENCION As Double
        'RETENCION = 0
        'TOTAL = 0
        'TOTAL = CDbl(dtgListado.CurrentRow.Cells("nImpTot").Value)
        'RETENCION = CDbl(dtgListado.CurrentRow.Cells("nMonRet").Value)
        'dtgListado.CurrentRow.Cells("nImpNetPag").Value = FormatNumber(TOTAL - RETENCION, 2)
        'Call CalcularTotales()
    End Sub
    Sub CalcularTotales()
        Dim TOTAL, RETENCION, NETO As Double
        If dtgListado.Rows.Count > 0 Then
            RETENCION = 0
            TOTAL = 0
            NETO = 0
            For i = 0 To dtgListado.Rows.Count - 1
                TOTAL = TOTAL + CDbl(dtgListado.Rows(i).Cells("nImpTot").Value)
                RETENCION = RETENCION + CDbl(dtgListado.Rows(i).Cells("nMonRet").Value)
                NETO = NETO + CDbl(dtgListado.Rows(i).Cells("nImpNetPag").Value)
            Next
            txtImporteTotal.Text = FormatNumber(TOTAL, 2)
            txtMontoRetencion.Text = FormatNumber(RETENCION, 2)
            txtImporteTotalNeto.Text = FormatNumber(NETO, 2)
        Else
            txtImporteTotal.Text = FormatNumber(0, 2)
            txtMontoRetencion.Text = FormatNumber(0, 2)
            txtImporteTotalNeto.Text = FormatNumber(0, 2)
        End If
    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub

    Private Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim BEClientes As New BECustomer
        FrmConsultaCliente.Inicio(BEClientes)
        FrmConsultaCliente = Nothing
        If BEClientes.nCodCli <> 0 Then
            nCodCli = BEClientes.nCodCli
            txtCliente.Text = BEClientes.cRazSoc
            Call RecuperarDetalle(nCodCli, 0)
            dtpFechaRegistro.Focus()
        Else
            btnBuscarCliente.Focus()
        End If
    End Sub

    Private Sub txtNumRetencion_TextChanged(sender As Object, e As EventArgs) Handles txtNumRetencion.TextChanged

    End Sub

    Private Sub btnBuscarCliente_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarCliente.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtNumRetencion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumRetencion.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaEmision_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaEmision.ValueChanged

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If CodigoRetencion = 0 Then
            If e.KeyCode = Keys.Delete Then
                e.Handled = True
                dtgListado.Rows.RemoveAt(dtgListado.CurrentRow.Index)
            End If
            Call CalcularTotales()
        End If
    End Sub

    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click

    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub dtpFechaEmision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaEmision.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class