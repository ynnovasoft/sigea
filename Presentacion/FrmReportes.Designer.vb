﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReportes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.RptGeneral = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.SuspendLayout()
        '
        'RptGeneral
        '
        Me.RptGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RptGeneral.DocumentMapWidth = 56
        ReportDataSource2.Name = "Listado"
        ReportDataSource2.Value = Nothing
        Me.RptGeneral.LocalReport.DataSources.Add(ReportDataSource2)
        Me.RptGeneral.LocalReport.ReportEmbeddedResource = "Presentacion.Report1.rdlc"
        Me.RptGeneral.Location = New System.Drawing.Point(0, 0)
        Me.RptGeneral.Name = "RptGeneral"
        Me.RptGeneral.Size = New System.Drawing.Size(1284, 641)
        Me.RptGeneral.TabIndex = 0
        '
        'FrmReportes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1284, 641)
        Me.Controls.Add(Me.RptGeneral)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmReportes"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents RptGeneral As Microsoft.Reporting.WinForms.ReportViewer
End Class
