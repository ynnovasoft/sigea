﻿Imports Microsoft.Reporting.WinForms
Public Class FrmReportes
    Private Sub FrmReportes_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Public Sub ReporteGeneralCompras(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Compras"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodUsuario, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Text = "Reporte General de Compras"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteDetalladoPorProducto(ByVal FechaIni As Date, ByVal FechaFin As Date, ByVal Agencia As Integer,
                                      ByVal Codigo As Integer, ByVal Catalogo As String, ByVal Descripcion As String,
                                      ByVal Movimiento As Integer, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Detallado Kardex Por Producto"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prFechaIni As New ReportParameter()
            prFechaIni.Name = "dFechaIni"
            prFechaIni.Values.Add(FechaIni)

            Dim prFechaFin As New ReportParameter()
            prFechaFin.Name = "dFechaFin"
            prFechaFin.Values.Add(FechaFin)

            Dim prAgencia As New ReportParameter()
            prAgencia.Name = "nCodigoAgencia"
            prAgencia.Values.Add(Agencia)

            Dim prnCodProd As New ReportParameter()
            prnCodProd.Name = "nCodProd"
            prnCodProd.Values.Add(Codigo)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim prCbTodos As New ReportParameter()
            prCbTodos.Name = "bTodos"
            prCbTodos.Values.Add(True)

            Dim prnTipMov As New ReportParameter()
            prnTipMov.Name = "nTipMov"
            prnTipMov.Values.Add(Movimiento)


            Dim prcCodCat As New ReportParameter()
            prcCodCat.Name = "cCodCat"
            prcCodCat.Values.Add(Catalogo)

            Dim prcDescripcion As New ReportParameter()
            prcDescripcion.Name = "cDescripcion"
            prcDescripcion.Values.Add(Descripcion)

            Dim parameters() As ReportParameter = {prFechaIni, prFechaFin, prCbTodos,
            prAgencia, prnCodProd, prCodEmpresa, prnTipMov, prcCodCat, prcDescripcion}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1090
            Me.Text = "Kardex Detallado Por Producto"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteInventarioConsolidadoProducto(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Inventario General Productos"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1140
            Me.Text = "Inventario Consolidado Producto"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteInventarioConsolidadoProductoGerencia(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = ""
            If MDIPrincipal.CodigoEmpresa = 1 Then
                psUbiNomReporte = "/Reportes/DBNegocio/RPT_DBNegocio_Inventario General Productos Gerencia"
            Else
                psUbiNomReporte = "/Reportes/DBNegocio/RPT_DBNegocio_Inventario General Productos Gerencia_2"
            End If
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1140
            Me.Text = "Inventario Consolidado Producto"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteInventarioPorProducto(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Inventario Por Producto"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte


            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim prCbTodos As New ReportParameter()
            prCbTodos.Name = "bTodos"
            prCbTodos.Values.Add(False)

            Dim parameters() As ReportParameter = {prCodUsuario, prCbTodos, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1090
            Me.Text = "Inventario Por Producto"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteInventarioPorMarca(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Inventario Por Marca"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte


            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim prCbTodos As New ReportParameter()
            prCbTodos.Name = "bTodos"
            prCbTodos.Values.Add(False)

            Dim parameters() As ReportParameter = {prCodUsuario, prCbTodos, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1090
            Me.Text = "Inventario Por Marca"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteGeneralCuentasPorCobrar(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Documento Credito Clientes"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)


            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodUsuario, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Text = "Reporte General Cuentas Por Cobrar"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteGeneralLetrasPorCobrar(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Documento Letras Clientes"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte


            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Width = 1215
            Me.Text = "Reporte General  Letras Por Cobrar"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteGeneralCuentasPorCobrarXCliente(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Documento Credito Por Cliente"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)


            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodUsuario, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Text = "Reporte Cuentas Por Cobrar Por Cliente"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteGeneralCreditoproveedores(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Documento Credito Proveedores"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)


            Dim parameters() As ReportParameter = {prCodUsuario, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Text = " Comprobantes A Créditos Proveedores"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ImpresionGuia(ByVal Codigo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Impresion Guia"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prnCodGuia As New ReportParameter()
            prnCodGuia.Name = "nCodGuia"
            prnCodGuia.Values.Add(Codigo)


            Dim parameters() As ReportParameter = {prnCodGuia}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Height = 500
            Me.Width = 900
            Me.Text = "Impresión de Guía"
            RptGeneral.ShowExportButton = False
            RptGeneral.ShowFindControls = False
            RptGeneral.ShowPageNavigationControls = False
            RptGeneral.ShowRefreshButton = False
            RptGeneral.ShowZoomControl = False
            RptGeneral.ShowBackButton = False
            RptGeneral.ShowStopButton = False
            Me.ShowDialog()

        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteOperacionesCaja(ByVal CodUsuario As String, ByVal Agencia As Integer, ByVal Fecha As Date,
                                  ByVal Titulo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Apertura Cierre Operaciones"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "cUsuario"
            prCodUsuario.Values.Add(CodUsuario)

            Dim prAgencia As New ReportParameter()
            prAgencia.Name = "Agencia"
            prAgencia.Values.Add(Agencia)

            Dim prFecha As New ReportParameter()
            prFecha.Name = "Fecha"
            prFecha.Values.Add(Fecha)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)


            Dim parameters() As ReportParameter = {prFecha, prCodUsuario, prCodEmpresa, prAgencia}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Width = 750
            Me.Text = Titulo
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ImpresionFactura(ByVal Codigo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Impresion Factura"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prnCodPed As New ReportParameter()
            prnCodPed.Name = "nCodPed"
            prnCodPed.Values.Add(Codigo)


            Dim parameters() As ReportParameter = {prnCodPed}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Height = 500
            Me.Width = 900
            Me.Text = "Impresión de Factura"
            RptGeneral.ShowExportButton = False
            RptGeneral.ShowFindControls = False
            RptGeneral.ShowPageNavigationControls = False
            RptGeneral.ShowRefreshButton = False
            RptGeneral.ShowZoomControl = False
            RptGeneral.ShowBackButton = False
            RptGeneral.ShowStopButton = False
            Me.ShowDialog()

        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ImpresionBoleta(ByVal Codigo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Impresion Boleta"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prnCodPed As New ReportParameter()
            prnCodPed.Name = "nCodPed"
            prnCodPed.Values.Add(Codigo)


            Dim parameters() As ReportParameter = {prnCodPed}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Height = 500
            Me.Width = 900
            Me.Text = "Impresión de Boleta"
            RptGeneral.ShowExportButton = False
            RptGeneral.ShowFindControls = False
            RptGeneral.ShowPageNavigationControls = False
            RptGeneral.ShowRefreshButton = False
            RptGeneral.ShowZoomControl = False
            RptGeneral.ShowBackButton = False
            RptGeneral.ShowStopButton = False
            Me.ShowDialog()

        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ImpresionTicket(ByVal Codigo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Impresion Ticket"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prnCodPed As New ReportParameter()
            prnCodPed.Name = "nCodPed"
            prnCodPed.Values.Add(Codigo)


            Dim parameters() As ReportParameter = {prnCodPed}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Height = 500
            Me.Width = 900
            Me.Text = "Impresión de Ticket Venta"
            RptGeneral.ShowExportButton = False
            RptGeneral.ShowFindControls = False
            RptGeneral.ShowPageNavigationControls = False
            RptGeneral.ShowRefreshButton = False
            RptGeneral.ShowZoomControl = False
            RptGeneral.ShowBackButton = False
            RptGeneral.ShowStopButton = False
            Me.ShowDialog()

        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ImpresionAlmacen(ByVal Codigo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Imprimir Almacen"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prnCodAlm As New ReportParameter()
            prnCodAlm.Name = "nCodAlm"
            prnCodAlm.Values.Add(Codigo)


            Dim parameters() As ReportParameter = {prnCodAlm}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Width = 755
            Me.Text = "Impresión de Alamcen"
            RptGeneral.ShowExportButton = True
            RptGeneral.ShowFindControls = False
            RptGeneral.ShowPageNavigationControls = False
            RptGeneral.ShowRefreshButton = False
            RptGeneral.ShowZoomControl = False
            RptGeneral.ShowBackButton = False
            RptGeneral.ShowStopButton = False
            Me.ShowDialog()

        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ImpresionGasto(ByVal Codigo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Imprimir Gasto"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prnCodGas As New ReportParameter()
            prnCodGas.Name = "nCodGas"
            prnCodGas.Values.Add(Codigo)


            Dim parameters() As ReportParameter = {prnCodGas}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Width = 755
            Me.Text = "Impresión de Gasto"
            RptGeneral.ShowExportButton = True
            RptGeneral.ShowFindControls = False
            RptGeneral.ShowPageNavigationControls = False
            RptGeneral.ShowRefreshButton = False
            RptGeneral.ShowZoomControl = False
            RptGeneral.ShowBackButton = False
            RptGeneral.ShowStopButton = False
            Me.ShowDialog()

        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ImpresionOrdenCompra(ByVal Codigo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Imprimir Orden Compra"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prnCodOrdCom As New ReportParameter()
            prnCodOrdCom.Name = "nCodOrdCom"
            prnCodOrdCom.Values.Add(Codigo)


            Dim parameters() As ReportParameter = {prnCodOrdCom}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Width = 755
            Me.Text = "Impresión de Orden Compra"
            RptGeneral.ShowExportButton = True
            RptGeneral.ShowFindControls = False
            RptGeneral.ShowPageNavigationControls = False
            RptGeneral.ShowRefreshButton = False
            RptGeneral.ShowZoomControl = False
            RptGeneral.ShowBackButton = False
            RptGeneral.ShowStopButton = False
            Me.ShowDialog()

        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteGeneralComprasContabilidad(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Compras Contabilidad"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Text = "Reporte General de Compras Contabilidad"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteGeneralVentasContabilidad(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Ventas Contabilidad"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Text = "Reporte General de Ventas Contabilidad"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteDetalladoVentasPorCliente(ByVal FechaIni As Date, ByVal FechaFin As Date,
                                            ByVal CodCli As Integer, ByVal Moneda As Integer, ByVal Sucursal As Integer,
                                            Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Ventas Detallado Por Cliente"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prFechaIni As New ReportParameter()
            prFechaIni.Name = "dFechaIni"
            prFechaIni.Values.Add(FechaIni)

            Dim prFechaFin As New ReportParameter()
            prFechaFin.Name = "dFechaFin"
            prFechaFin.Values.Add(FechaFin)

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodCli As New ReportParameter()
            prCodCli.Name = "nCodCli"
            prCodCli.Values.Add(CodCli)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim prnTipMon As New ReportParameter()
            prnTipMon.Name = "nTipMon"
            prnTipMon.Values.Add(Moneda)

            Dim prnCodAge As New ReportParameter()
            prnCodAge.Name = "nCodAge"
            prnCodAge.Values.Add(Sucursal)

            Dim parameters() As ReportParameter = {prFechaIni, prFechaFin, prCodUsuario, prCodCli, prCodEmpresa, prnTipMon, prnCodAge}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1245
            Me.Text = "Detallado Ventas Por Cliente"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteDetalladoVentasPorMarca(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)

        RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Ventas Detallado Por Marca"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1245
            Me.Text = "Detallado Ventas Por Marca"
            Me.ShowDialog()
    End Sub
    Public Sub ReporteDetalladoVentasPorProducto(ByVal FechaIni As Date, ByVal FechaFin As Date,
                                            ByVal CodCli As Integer, ByVal Moneda As Integer, ByVal Sucursal As Integer,
                                            Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Ventas Detallado Por Producto"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prFechaIni As New ReportParameter()
            prFechaIni.Name = "dFechaIni"
            prFechaIni.Values.Add(FechaIni)

            Dim prFechaFin As New ReportParameter()
            prFechaFin.Name = "dFechaFin"
            prFechaFin.Values.Add(FechaFin)

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodProd As New ReportParameter()
            prCodProd.Name = "nCodProd"
            prCodProd.Values.Add(CodCli)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim prnTipMon As New ReportParameter()
            prnTipMon.Name = "nTipMon"
            prnTipMon.Values.Add(Moneda)

            Dim prnCodAge As New ReportParameter()
            prnCodAge.Name = "nCodAge"
            prnCodAge.Values.Add(Sucursal)

            Dim parameters() As ReportParameter = {prFechaIni, prFechaFin, prCodUsuario, prCodProd, prCodEmpresa, prnTipMon, prnCodAge}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1245
            Me.Text = "Detallado Ventas Por Producto"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteDetalladoVentasVendedor(ByVal FechaIni As Date, ByVal FechaFin As Date,
                                           ByVal CodVen As Integer, ByVal Moneda As Integer, ByVal Sucursal As Integer,
                                           Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Ventas Detallado Por Vendedor"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prFechaIni As New ReportParameter()
            prFechaIni.Name = "dFechaIni"
            prFechaIni.Values.Add(FechaIni)

            Dim prFechaFin As New ReportParameter()
            prFechaFin.Name = "dFechaFin"
            prFechaFin.Values.Add(FechaFin)

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodVen As New ReportParameter()
            prCodVen.Name = "nCodVen"
            prCodVen.Values.Add(CodVen)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim prnTipMon As New ReportParameter()
            prnTipMon.Name = "nTipMon"
            prnTipMon.Values.Add(Moneda)

            Dim prnCodAge As New ReportParameter()
            prnCodAge.Name = "nCodAge"
            prnCodAge.Values.Add(Sucursal)

            Dim parameters() As ReportParameter = {prFechaIni, prFechaFin, prCodUsuario, prCodVen, prCodEmpresa, prnTipMon, prnCodAge}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1130
            Me.Text = "Detallado Ventas Por Vendedor"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteDetalladoVentas(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Ventas Detallado"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte


            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)


            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodUsuario, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1130
            Me.Text = "Detallado Ventas"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ImpresionLetras(ByVal Codigo As String, Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Impresion Letra"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prnCodLet As New ReportParameter()
            prnCodLet.Name = "nCodLet"
            prnCodLet.Values.Add(Codigo)


            Dim parameters() As ReportParameter = {prnCodLet}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Height = 500
            Me.Width = 900
            Me.Text = "Impresión de Letra"
            RptGeneral.ShowExportButton = False
            RptGeneral.ShowFindControls = False
            RptGeneral.ShowPageNavigationControls = False
            RptGeneral.ShowRefreshButton = False
            RptGeneral.ShowZoomControl = False
            RptGeneral.ShowBackButton = False
            RptGeneral.ShowStopButton = False
            Me.ShowDialog()

        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteDetalladoGeneralGeneral(ByVal FechaIni As Date, ByVal FechaFin As Date,
                                            ByVal CodCli As Integer,
                                            Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Letras"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prFechaIni As New ReportParameter()
            prFechaIni.Name = "dFechaIni"
            prFechaIni.Values.Add(FechaIni)

            Dim prFechaFin As New ReportParameter()
            prFechaFin.Name = "dFechaFin"
            prFechaFin.Values.Add(FechaFin)

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodCli As New ReportParameter()
            prCodCli.Name = "nCodCli"
            prCodCli.Values.Add(CodCli)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prFechaIni, prFechaFin, prCodUsuario, prCodCli, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1165
            Me.Text = "General Letras"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteDetalladoPorMarca(ByVal FechaIni As Date, ByVal FechaFin As Date, ByVal Agencia As Integer,
                                      ByVal CodFamilia As String,
                                       Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = False
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Detallado Kardex Por Marca"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prFechaIni As New ReportParameter()
            prFechaIni.Name = "dFechaIni"
            prFechaIni.Values.Add(FechaIni)

            Dim prFechaFin As New ReportParameter()
            prFechaFin.Name = "dFechaFin"
            prFechaFin.Values.Add(FechaFin)

            Dim prAgencia As New ReportParameter()
            prAgencia.Name = "nCodigoAgencia"
            prAgencia.Values.Add(Agencia)

            Dim prCodFamilia As New ReportParameter()
            prCodFamilia.Name = "cCodFam"
            prCodFamilia.Values.Add(CodFamilia)

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim prCbTodos As New ReportParameter()
            prCbTodos.Name = "bTodos"
            prCbTodos.Values.Add(True)

            Dim parameters() As ReportParameter = {prCodUsuario, prFechaIni, prFechaFin, prCbTodos,
            prAgencia, prCodFamilia, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1090
            Me.Text = "Kardex Detallado Por Marca"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteDetalladoVentasDia(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Ventas Detallado Por Dia"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte


            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1130
            Me.Text = "Detallado Ventas Por Día"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReporteStockMinimoPorProducto(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Stock Minimo Por Producto"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim prCbTodos As New ReportParameter()
            prCbTodos.Name = "bTodos"
            prCbTodos.Values.Add(False)

            Dim parameters() As ReportParameter = {prCodUsuario, prCbTodos, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 1090
            Me.Text = "Reporte Stock Mínimo"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReportesInventarioConsolidadoPorMarca(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_General Inventario Consolidado Por Marca"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte

            Dim prCodUsuario As New ReportParameter()
            prCodUsuario.Name = "CodUsuario"
            prCodUsuario.Values.Add(MDIPrincipal.CodUsuario)

            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodUsuario, prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()

            Me.Width = 920
            Me.Text = "Inventario Consolidado Por Marca"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
    Public Sub ReportePagosComprobante(Optional ByVal Mode As DisplayMode = DisplayMode.Normal)
        Try
            RptGeneral.ProcessingMode = ProcessingMode.Remote
            RptGeneral.ShowParameterPrompts = True
            Dim psUbiNomReporte As String = "/Reportes/DBNegocio/RPT_DBNegocio_Consulta Pagos Comprobantes"
            Dim ServerReport As ServerReport
            ServerReport = RptGeneral.ServerReport
            Dim Credentials As System.Net.ICredentials
            Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim rsCredentials As ReportServerCredentials
            rsCredentials = ServerReport.ReportServerCredentials

            rsCredentials.NetworkCredentials = Credentials
            ServerReport.ReportServerUrl = New Uri(MDIPrincipal.RlServidor)
            ServerReport.ReportPath = psUbiNomReporte


            Dim prCodEmpresa As New ReportParameter()
            prCodEmpresa.Name = "nCodEmp"
            prCodEmpresa.Values.Add(MDIPrincipal.CodigoEmpresa)

            Dim parameters() As ReportParameter = {prCodEmpresa}
            ServerReport.SetParameters(parameters)

            RptGeneral.SetDisplayMode(Mode)
            RptGeneral.RefreshReport()
            Me.Width = 750
            Me.Text = "Reporte Cosnulta Pagos Comprobante"
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message & Chr(13) & "Comuníquese con el Administrador de la Red", MsgBoxStyle.Exclamation, "Mensaje")
        End Try
    End Sub
End Class