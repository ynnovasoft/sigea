﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmStockPendienteProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCadCot = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.Label()
        Me.cmbMarca = New System.Windows.Forms.ComboBox()
        Me.Ingresos = New System.Windows.Forms.TabPage()
        Me.dtgListadoIngreso = New System.Windows.Forms.DataGridView()
        Me.cNumCompI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecRegI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUsuRegAudI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipComI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCantidadI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Salidas = New System.Windows.Forms.TabPage()
        Me.dtgListadoSalidas = New System.Windows.Forms.DataGridView()
        Me.cNumComp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dFecReg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUsuRegAud = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTipCom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tlsTitulo.SuspendLayout()
        Me.Ingresos.SuspendLayout()
        CType(Me.dtgListadoIngreso, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Salidas.SuspendLayout()
        CType(Me.dtgListadoSalidas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 3)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 22)
        Me.pnMover.TabIndex = 118
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(615, 29)
        Me.tlsTitulo.TabIndex = 117
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 26)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(273, 26)
        Me.lblTitulo.Text = "CONSULTA STOCK REGISTRADO PRODUCTO"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(26, 26)
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(315, 42)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(89, 16)
        Me.Label17.TabIndex = 125
        Me.Label17.Text = "Cód.Catálogo:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 16)
        Me.Label1.TabIndex = 135
        Me.Label1.Text = "Descripción:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 137
        Me.Label3.Text = "Marca:"
        '
        'txtCadCot
        '
        Me.txtCadCot.BackColor = System.Drawing.SystemColors.Info
        Me.txtCadCot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCadCot.Location = New System.Drawing.Point(407, 35)
        Me.txtCadCot.Name = "txtCadCot"
        Me.txtCadCot.Size = New System.Drawing.Size(201, 23)
        Me.txtCadCot.TabIndex = 148
        Me.txtCadCot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.SystemColors.Info
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.Location = New System.Drawing.Point(107, 62)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(501, 23)
        Me.txtDescripcion.TabIndex = 149
        Me.txtDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbMarca
        '
        Me.cmbMarca.BackColor = System.Drawing.Color.White
        Me.cmbMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarca.Enabled = False
        Me.cmbMarca.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMarca.FormattingEnabled = True
        Me.cmbMarca.Location = New System.Drawing.Point(107, 34)
        Me.cmbMarca.Name = "cmbMarca"
        Me.cmbMarca.Size = New System.Drawing.Size(192, 24)
        Me.cmbMarca.TabIndex = 0
        '
        'Ingresos
        '
        Me.Ingresos.Controls.Add(Me.dtgListadoIngreso)
        Me.Ingresos.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ingresos.Location = New System.Drawing.Point(4, 25)
        Me.Ingresos.Name = "Ingresos"
        Me.Ingresos.Padding = New System.Windows.Forms.Padding(3)
        Me.Ingresos.Size = New System.Drawing.Size(590, 291)
        Me.Ingresos.TabIndex = 1
        Me.Ingresos.Text = "Ingresos"
        Me.Ingresos.UseVisualStyleBackColor = True
        '
        'dtgListadoIngreso
        '
        Me.dtgListadoIngreso.AllowUserToAddRows = False
        Me.dtgListadoIngreso.AllowUserToDeleteRows = False
        Me.dtgListadoIngreso.AllowUserToResizeColumns = False
        Me.dtgListadoIngreso.AllowUserToResizeRows = False
        Me.dtgListadoIngreso.BackgroundColor = System.Drawing.Color.White
        Me.dtgListadoIngreso.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListadoIngreso.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dtgListadoIngreso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListadoIngreso.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cNumCompI, Me.dFecRegI, Me.cUsuRegAudI, Me.cTipComI, Me.nCantidadI})
        Me.dtgListadoIngreso.EnableHeadersVisualStyles = False
        Me.dtgListadoIngreso.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListadoIngreso.Location = New System.Drawing.Point(4, 4)
        Me.dtgListadoIngreso.Name = "dtgListadoIngreso"
        Me.dtgListadoIngreso.ReadOnly = True
        Me.dtgListadoIngreso.RowHeadersVisible = False
        Me.dtgListadoIngreso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListadoIngreso.Size = New System.Drawing.Size(581, 281)
        Me.dtgListadoIngreso.TabIndex = 9
        '
        'cNumCompI
        '
        Me.cNumCompI.DataPropertyName = "cNumComp"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumCompI.DefaultCellStyle = DataGridViewCellStyle14
        Me.cNumCompI.HeaderText = "N° Comprobante"
        Me.cNumCompI.Name = "cNumCompI"
        Me.cNumCompI.ReadOnly = True
        Me.cNumCompI.Width = 120
        '
        'dFecRegI
        '
        Me.dFecRegI.DataPropertyName = "dFecReg"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle15.Format = "d"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.dFecRegI.DefaultCellStyle = DataGridViewCellStyle15
        Me.dFecRegI.HeaderText = "Fecha Registro"
        Me.dFecRegI.Name = "dFecRegI"
        Me.dFecRegI.ReadOnly = True
        '
        'cUsuRegAudI
        '
        Me.cUsuRegAudI.DataPropertyName = "cUsuRegAud"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle16.NullValue = "0.00"
        Me.cUsuRegAudI.DefaultCellStyle = DataGridViewCellStyle16
        Me.cUsuRegAudI.HeaderText = "Usuario"
        Me.cUsuRegAudI.Name = "cUsuRegAudI"
        Me.cUsuRegAudI.ReadOnly = True
        Me.cUsuRegAudI.Width = 87
        '
        'cTipComI
        '
        Me.cTipComI.DataPropertyName = "cTipCom"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cTipComI.DefaultCellStyle = DataGridViewCellStyle17
        Me.cTipComI.HeaderText = "Tipo Comprobante"
        Me.cTipComI.Name = "cTipComI"
        Me.cTipComI.ReadOnly = True
        Me.cTipComI.Width = 200
        '
        'nCantidadI
        '
        Me.nCantidadI.DataPropertyName = "nCantidad"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle18.Format = "N0"
        DataGridViewCellStyle18.NullValue = "0"
        Me.nCantidadI.DefaultCellStyle = DataGridViewCellStyle18
        Me.nCantidadI.HeaderText = "Cantidad"
        Me.nCantidadI.Name = "nCantidadI"
        Me.nCantidadI.ReadOnly = True
        Me.nCantidadI.Width = 70
        '
        'Salidas
        '
        Me.Salidas.Controls.Add(Me.dtgListadoSalidas)
        Me.Salidas.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Salidas.Location = New System.Drawing.Point(4, 25)
        Me.Salidas.Name = "Salidas"
        Me.Salidas.Padding = New System.Windows.Forms.Padding(3)
        Me.Salidas.Size = New System.Drawing.Size(590, 291)
        Me.Salidas.TabIndex = 0
        Me.Salidas.Text = "Salidas"
        Me.Salidas.UseVisualStyleBackColor = True
        '
        'dtgListadoSalidas
        '
        Me.dtgListadoSalidas.AllowUserToAddRows = False
        Me.dtgListadoSalidas.AllowUserToDeleteRows = False
        Me.dtgListadoSalidas.AllowUserToResizeColumns = False
        Me.dtgListadoSalidas.AllowUserToResizeRows = False
        Me.dtgListadoSalidas.BackgroundColor = System.Drawing.Color.White
        Me.dtgListadoSalidas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListadoSalidas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.dtgListadoSalidas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListadoSalidas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cNumComp, Me.dFecReg, Me.cUsuRegAud, Me.cTipCom, Me.nCantidad})
        Me.dtgListadoSalidas.EnableHeadersVisualStyles = False
        Me.dtgListadoSalidas.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListadoSalidas.Location = New System.Drawing.Point(4, 4)
        Me.dtgListadoSalidas.Name = "dtgListadoSalidas"
        Me.dtgListadoSalidas.ReadOnly = True
        Me.dtgListadoSalidas.RowHeadersVisible = False
        Me.dtgListadoSalidas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListadoSalidas.Size = New System.Drawing.Size(581, 281)
        Me.dtgListadoSalidas.TabIndex = 8
        '
        'cNumComp
        '
        Me.cNumComp.DataPropertyName = "cNumComp"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cNumComp.DefaultCellStyle = DataGridViewCellStyle20
        Me.cNumComp.HeaderText = "N° Comprobante"
        Me.cNumComp.Name = "cNumComp"
        Me.cNumComp.ReadOnly = True
        Me.cNumComp.Width = 120
        '
        'dFecReg
        '
        Me.dFecReg.DataPropertyName = "dFecReg"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle21.Format = "d"
        DataGridViewCellStyle21.NullValue = Nothing
        Me.dFecReg.DefaultCellStyle = DataGridViewCellStyle21
        Me.dFecReg.HeaderText = "Fecha Registro"
        Me.dFecReg.Name = "dFecReg"
        Me.dFecReg.ReadOnly = True
        '
        'cUsuRegAud
        '
        Me.cUsuRegAud.DataPropertyName = "cUsuRegAud"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle22.NullValue = "0.00"
        Me.cUsuRegAud.DefaultCellStyle = DataGridViewCellStyle22
        Me.cUsuRegAud.HeaderText = "Usuario"
        Me.cUsuRegAud.Name = "cUsuRegAud"
        Me.cUsuRegAud.ReadOnly = True
        Me.cUsuRegAud.Width = 87
        '
        'cTipCom
        '
        Me.cTipCom.DataPropertyName = "cTipCom"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.cTipCom.DefaultCellStyle = DataGridViewCellStyle23
        Me.cTipCom.HeaderText = "Tipo Comprobante"
        Me.cTipCom.Name = "cTipCom"
        Me.cTipCom.ReadOnly = True
        Me.cTipCom.Width = 200
        '
        'nCantidad
        '
        Me.nCantidad.DataPropertyName = "nCantidad"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle24.Format = "N0"
        DataGridViewCellStyle24.NullValue = "0"
        Me.nCantidad.DefaultCellStyle = DataGridViewCellStyle24
        Me.nCantidad.HeaderText = "Cantidad"
        Me.nCantidad.Name = "nCantidad"
        Me.nCantidad.ReadOnly = True
        Me.nCantidad.Width = 70
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.Salidas)
        Me.TabControl1.Controls.Add(Me.Ingresos)
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(10, 98)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(598, 320)
        Me.TabControl1.TabIndex = 183
        '
        'FrmStockPendienteProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(615, 427)
        Me.ControlBox = False
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.txtCadCot)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbMarca)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmStockPendienteProducto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        Me.Ingresos.ResumeLayout(False)
        CType(Me.dtgListadoIngreso, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Salidas.ResumeLayout(False)
        CType(Me.dtgListadoSalidas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnMover As Panel
    Friend WithEvents tlsTitulo As ToolStrip
    Friend WithEvents btnMover As ToolStripButton
    Friend WithEvents lblTitulo As ToolStripLabel
    Friend WithEvents btnCerrar As ToolStripButton
    Friend WithEvents Label17 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtCadCot As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.Label
    Friend WithEvents cmbMarca As ComboBox
    Friend WithEvents Ingresos As TabPage
    Friend WithEvents Salidas As TabPage
    Friend WithEvents dtgListadoSalidas As DataGridView
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents cNumComp As DataGridViewTextBoxColumn
    Friend WithEvents dFecReg As DataGridViewTextBoxColumn
    Friend WithEvents cUsuRegAud As DataGridViewTextBoxColumn
    Friend WithEvents cTipCom As DataGridViewTextBoxColumn
    Friend WithEvents nCantidad As DataGridViewTextBoxColumn
    Friend WithEvents dtgListadoIngreso As DataGridView
    Friend WithEvents cNumCompI As DataGridViewTextBoxColumn
    Friend WithEvents dFecRegI As DataGridViewTextBoxColumn
    Friend WithEvents cUsuRegAudI As DataGridViewTextBoxColumn
    Friend WithEvents cTipComI As DataGridViewTextBoxColumn
    Friend WithEvents nCantidadI As DataGridViewTextBoxColumn
End Class
