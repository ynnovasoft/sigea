﻿Imports Negocios
Imports Business
Imports System.Windows.Forms
Public Class FrmStockPendienteProducto
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Private Sub FrmStockPendienteProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call ListarMovimientos(txtCadCot.Tag, 1)
        Call ListarMovimientos(txtCadCot.Tag, 2)
    End Sub
    Sub Inicio(ByVal nCodProd As Integer, ByVal cMarca As String, ByVal cCodCat As String, ByVal cDescripcion As String)
        txtCadCot.Tag = nCodProd
        cmbMarca.SelectedValue = cMarca
        txtCadCot.Text = cCodCat
        txtDescripcion.Text = cDescripcion
        Me.ShowDialog()
    End Sub
    Sub CargaCombos()
        Dim dt As New DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("130")
        Call CargaCombo(CreaDatoCombos(dt, 130), cmbMarca)
    End Sub
    Sub ListarMovimientos(ByVal Codigo As Integer, ByVal Movimiento As Integer)
        Dim dt As New DataTable
        Dim Obj As New BLAlmacen
        dt = Obj.ConsultaStockRegistradoProducto(Codigo, Movimiento, MDIPrincipal.CodigoAgencia)
        If Movimiento = 1 Then
            Call LlenaAGridView(dt, dtgListadoSalidas)
        Else
            Call LlenaAGridView(dt, dtgListadoIngreso)
        End If
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub FrmStockPendienteProducto_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class