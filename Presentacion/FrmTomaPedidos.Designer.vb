﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTomaPedidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tlsTitulo = New System.Windows.Forms.ToolStrip()
        Me.btnMover = New System.Windows.Forms.ToolStripButton()
        Me.lblTitulo = New System.Windows.Forms.ToolStripLabel()
        Me.btnCerrar = New System.Windows.Forms.ToolStripButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbCondPago = New System.Windows.Forms.ComboBox()
        Me.txtNumDoc = New System.Windows.Forms.TextBox()
        Me.txtCliente = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpFechaRegistro = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbMoneda = New System.Windows.Forms.ComboBox()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtpFechaVencimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNumComprobante = New System.Windows.Forms.Label()
        Me.dtgListado = New System.Windows.Forms.DataGridView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtVentasGravadas = New System.Windows.Forms.Label()
        Me.txtIGV = New System.Windows.Forms.Label()
        Me.txtImporteTotal = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cmbComprobante = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cmbSerie = New System.Windows.Forms.ComboBox()
        Me.txtOrdenCompra = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.txtNumDocRef = New System.Windows.Forms.Label()
        Me.pnMover = New System.Windows.Forms.Panel()
        Me.btnFacturar = New System.Windows.Forms.Button()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.cmbTipoRef = New System.Windows.Forms.ComboBox()
        Me.chbAdjuntar = New System.Windows.Forms.CheckBox()
        Me.chbGuia = New System.Windows.Forms.CheckBox()
        Me.btnBuscarCliente = New System.Windows.Forms.Button()
        Me.txtNumGuia = New System.Windows.Forms.Label()
        Me.txtImporteNeto = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtMontoRetencion = New System.Windows.Forms.Label()
        Me.lblMontoRetencion = New System.Windows.Forms.Label()
        Me.lblRetencion = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmbVendedor = New System.Windows.Forms.ComboBox()
        Me.txtSubTotal = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtDescuento = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.btnClonar = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.cmbOperacion = New System.Windows.Forms.ComboBox()
        Me.txtAnticipos = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.chbAnticipos = New System.Windows.Forms.CheckBox()
        Me.btnCuotas = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.chbServicios = New System.Windows.Forms.CheckBox()
        Me.btnRetencion = New System.Windows.Forms.Button()
        Me.txtBuscarCodCat = New System.Windows.Forms.TextBox()
        Me.txtLetras = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.nCodPed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodProd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nNumOrd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCodCat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUndMed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPreUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nSubTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nPorDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nMonDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nSubTotNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nValVent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nCodAnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Anticipo = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.tlsTitulo.SuspendLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlsTitulo
        '
        Me.tlsTitulo.AutoSize = False
        Me.tlsTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.tlsTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.tlsTitulo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsTitulo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMover, Me.lblTitulo, Me.btnCerrar})
        Me.tlsTitulo.Location = New System.Drawing.Point(0, 0)
        Me.tlsTitulo.Name = "tlsTitulo"
        Me.tlsTitulo.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsTitulo.Size = New System.Drawing.Size(1214, 25)
        Me.tlsTitulo.TabIndex = 0
        Me.tlsTitulo.Text = "ToolStrip1"
        '
        'btnMover
        '
        Me.btnMover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnMover.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMover.Name = "btnMover"
        Me.btnMover.Size = New System.Drawing.Size(23, 22)
        Me.btnMover.Text = "ToolStripButton2"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.Color.DodgerBlue
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(122, 22)
        Me.lblTitulo.Text = "TOMA DE PEDIDOS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCerrar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCerrar.Image = Global.Presentacion.My.Resources.Resources.Cerrar
        Me.btnCerrar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(23, 22)
        Me.btnCerrar.ToolTipText = "Cerrar <ESC>"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "N° Documento:"
        '
        'cmbCondPago
        '
        Me.cmbCondPago.BackColor = System.Drawing.Color.White
        Me.cmbCondPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCondPago.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCondPago.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbCondPago.FormattingEnabled = True
        Me.cmbCondPago.Location = New System.Drawing.Point(938, 104)
        Me.cmbCondPago.Name = "cmbCondPago"
        Me.cmbCondPago.Size = New System.Drawing.Size(265, 22)
        Me.cmbCondPago.TabIndex = 13
        '
        'txtNumDoc
        '
        Me.txtNumDoc.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumDoc.ForeColor = System.Drawing.Color.Black
        Me.txtNumDoc.Location = New System.Drawing.Point(127, 56)
        Me.txtNumDoc.Name = "txtNumDoc"
        Me.txtNumDoc.Size = New System.Drawing.Size(159, 22)
        Me.txtNumDoc.TabIndex = 0
        Me.txtNumDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCliente
        '
        Me.txtCliente.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCliente.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCliente.ForeColor = System.Drawing.Color.Black
        Me.txtCliente.Location = New System.Drawing.Point(127, 80)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(602, 22)
        Me.txtCliente.TabIndex = 5
        Me.txtCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDireccion
        '
        Me.txtDireccion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDireccion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDireccion.ForeColor = System.Drawing.Color.Black
        Me.txtDireccion.Location = New System.Drawing.Point(127, 104)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(602, 22)
        Me.txtDireccion.TabIndex = 6
        Me.txtDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 14)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Cliente:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 14)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Dirección:"
        '
        'dtpFechaRegistro
        '
        Me.dtpFechaRegistro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaRegistro.Location = New System.Drawing.Point(625, 56)
        Me.dtpFechaRegistro.Name = "dtpFechaRegistro"
        Me.dtpFechaRegistro.Size = New System.Drawing.Size(104, 22)
        Me.dtpFechaRegistro.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(526, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 14)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Fecha Registro:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(820, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 14)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Condición Pago:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 136)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 14)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Moneda:"
        '
        'cmbMoneda
        '
        Me.cmbMoneda.BackColor = System.Drawing.Color.White
        Me.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMoneda.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMoneda.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbMoneda.FormattingEnabled = True
        Me.cmbMoneda.Location = New System.Drawing.Point(127, 128)
        Me.cmbMoneda.Name = "cmbMoneda"
        Me.cmbMoneda.Size = New System.Drawing.Size(136, 22)
        Me.cmbMoneda.TabIndex = 3
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.Black
        Me.txtTipoCambio.Location = New System.Drawing.Point(1142, 56)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(61, 22)
        Me.txtTipoCambio.TabIndex = 11
        Me.txtTipoCambio.Text = "0.0000"
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(1061, 63)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 14)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Tipo Cambio:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(820, 63)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(115, 14)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Fecha Vencimiento:"
        '
        'dtpFechaVencimiento
        '
        Me.dtpFechaVencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVencimiento.Location = New System.Drawing.Point(938, 56)
        Me.dtpFechaVencimiento.Name = "dtpFechaVencimiento"
        Me.dtpFechaVencimiento.Size = New System.Drawing.Size(110, 22)
        Me.dtpFechaVencimiento.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(9, 40)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(103, 14)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "N° Comprobante:"
        '
        'txtNumComprobante
        '
        Me.txtNumComprobante.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumComprobante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumComprobante.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumComprobante.ForeColor = System.Drawing.Color.Black
        Me.txtNumComprobante.Location = New System.Drawing.Point(127, 32)
        Me.txtNumComprobante.Name = "txtNumComprobante"
        Me.txtNumComprobante.Size = New System.Drawing.Size(159, 22)
        Me.txtNumComprobante.TabIndex = 20
        Me.txtNumComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgListado
        '
        Me.dtgListado.AllowUserToAddRows = False
        Me.dtgListado.AllowUserToDeleteRows = False
        Me.dtgListado.AllowUserToResizeColumns = False
        Me.dtgListado.AllowUserToResizeRows = False
        Me.dtgListado.BackgroundColor = System.Drawing.Color.White
        Me.dtgListado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nCodPed, Me.nCodProd, Me.nNumOrd, Me.cCodCat, Me.cDescripcion, Me.cUndMed, Me.nCantidad, Me.nValUni, Me.nPreUni, Me.nSubTot, Me.nPorDes, Me.nMonDes, Me.nSubTotNet, Me.nValVent, Me.nCodAnt, Me.Anticipo})
        Me.dtgListado.EnableHeadersVisualStyles = False
        Me.dtgListado.GridColor = System.Drawing.Color.DodgerBlue
        Me.dtgListado.Location = New System.Drawing.Point(11, 233)
        Me.dtgListado.Name = "dtgListado"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgListado.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dtgListado.RowHeadersVisible = False
        Me.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dtgListado.Size = New System.Drawing.Size(1192, 259)
        Me.dtgListado.TabIndex = 25
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(560, 505)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(101, 14)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Ventas Gravadas:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(817, 505)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(31, 14)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "IGV:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(1003, 505)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(87, 14)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Importe Total:"
        '
        'txtVentasGravadas
        '
        Me.txtVentasGravadas.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtVentasGravadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVentasGravadas.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtVentasGravadas.ForeColor = System.Drawing.Color.Black
        Me.txtVentasGravadas.Location = New System.Drawing.Point(675, 499)
        Me.txtVentasGravadas.Name = "txtVentasGravadas"
        Me.txtVentasGravadas.Size = New System.Drawing.Size(111, 22)
        Me.txtVentasGravadas.TabIndex = 35
        Me.txtVentasGravadas.Text = "0.00"
        Me.txtVentasGravadas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIGV
        '
        Me.txtIGV.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtIGV.ForeColor = System.Drawing.Color.Black
        Me.txtIGV.Location = New System.Drawing.Point(853, 499)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.Size = New System.Drawing.Size(101, 22)
        Me.txtIGV.TabIndex = 36
        Me.txtIGV.Text = "0.00"
        Me.txtIGV.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotal.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtImporteTotal.ForeColor = System.Drawing.Color.Black
        Me.txtImporteTotal.Location = New System.Drawing.Point(1093, 499)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(110, 22)
        Me.txtImporteTotal.TabIndex = 37
        Me.txtImporteTotal.Text = "0.00"
        Me.txtImporteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(277, 136)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(106, 14)
        Me.Label15.TabIndex = 39
        Me.Label15.Text = "Tip Comprobante:"
        '
        'cmbComprobante
        '
        Me.cmbComprobante.BackColor = System.Drawing.Color.White
        Me.cmbComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbComprobante.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbComprobante.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbComprobante.FormattingEnabled = True
        Me.cmbComprobante.Location = New System.Drawing.Point(387, 128)
        Me.cmbComprobante.Name = "cmbComprobante"
        Me.cmbComprobante.Size = New System.Drawing.Size(171, 22)
        Me.cmbComprobante.TabIndex = 4
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(598, 136)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(38, 14)
        Me.Label16.TabIndex = 41
        Me.Label16.Text = "Serie:"
        '
        'cmbSerie
        '
        Me.cmbSerie.BackColor = System.Drawing.Color.White
        Me.cmbSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSerie.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSerie.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbSerie.FormattingEnabled = True
        Me.cmbSerie.Location = New System.Drawing.Point(639, 128)
        Me.cmbSerie.Name = "cmbSerie"
        Me.cmbSerie.Size = New System.Drawing.Size(90, 22)
        Me.cmbSerie.TabIndex = 5
        '
        'txtOrdenCompra
        '
        Me.txtOrdenCompra.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrdenCompra.ForeColor = System.Drawing.Color.Black
        Me.txtOrdenCompra.Location = New System.Drawing.Point(127, 152)
        Me.txtOrdenCompra.Name = "txtOrdenCompra"
        Me.txtOrdenCompra.Size = New System.Drawing.Size(136, 22)
        Me.txtOrdenCompra.TabIndex = 6
        Me.txtOrdenCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(9, 160)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(108, 14)
        Me.Label17.TabIndex = 42
        Me.Label17.Text = "Orden de Compra:"
        '
        'lblMensaje
        '
        Me.lblMensaje.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblMensaje.Location = New System.Drawing.Point(554, 208)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(649, 22)
        Me.lblMensaje.TabIndex = 123
        Me.lblMensaje.Text = "MENSAJE DE ESTADO"
        Me.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumDocRef
        '
        Me.txtNumDocRef.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumDocRef.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumDocRef.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumDocRef.ForeColor = System.Drawing.Color.Black
        Me.txtNumDocRef.Location = New System.Drawing.Point(938, 32)
        Me.txtNumDocRef.Name = "txtNumDocRef"
        Me.txtNumDocRef.Size = New System.Drawing.Size(265, 22)
        Me.txtNumDocRef.TabIndex = 124
        Me.txtNumDocRef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnMover
        '
        Me.pnMover.BackColor = System.Drawing.Color.DodgerBlue
        Me.pnMover.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.pnMover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pnMover.Location = New System.Drawing.Point(2, 1)
        Me.pnMover.Name = "pnMover"
        Me.pnMover.Size = New System.Drawing.Size(25, 19)
        Me.pnMover.TabIndex = 116
        '
        'btnFacturar
        '
        Me.btnFacturar.FlatAppearance.BorderSize = 0
        Me.btnFacturar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnFacturar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFacturar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFacturar.Image = Global.Presentacion.My.Resources.Resources.Facturar_22
        Me.btnFacturar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFacturar.Location = New System.Drawing.Point(1093, 569)
        Me.btnFacturar.Name = "btnFacturar"
        Me.btnFacturar.Size = New System.Drawing.Size(110, 26)
        Me.btnFacturar.TabIndex = 22
        Me.btnFacturar.Text = "Facturar(F3)"
        Me.btnFacturar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFacturar.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.FlatAppearance.BorderSize = 0
        Me.btnGrabar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Image = Global.Presentacion.My.Resources.Resources.Save_22
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(12, 570)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(118, 26)
        Me.btnGrabar.TabIndex = 21
        Me.btnGrabar.Text = "Registrar(F2)"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEliminar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Image = Global.Presentacion.My.Resources.Resources.Delete_20
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(239, 208)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(79, 22)
        Me.btnEliminar.TabIndex = 20
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAgregar.FlatAppearance.BorderSize = 0
        Me.btnAgregar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAgregar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Image = Global.Presentacion.My.Resources.Resources.Add_20
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(149, 208)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(84, 22)
        Me.btnAgregar.TabIndex = 19
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'cmbTipoRef
        '
        Me.cmbTipoRef.BackColor = System.Drawing.Color.White
        Me.cmbTipoRef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoRef.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipoRef.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbTipoRef.FormattingEnabled = True
        Me.cmbTipoRef.Location = New System.Drawing.Point(735, 32)
        Me.cmbTipoRef.Name = "cmbTipoRef"
        Me.cmbTipoRef.Size = New System.Drawing.Size(202, 22)
        Me.cmbTipoRef.TabIndex = 9
        '
        'chbAdjuntar
        '
        Me.chbAdjuntar.AutoSize = True
        Me.chbAdjuntar.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.chbAdjuntar.ForeColor = System.Drawing.Color.Black
        Me.chbAdjuntar.Location = New System.Drawing.Point(530, 35)
        Me.chbAdjuntar.Name = "chbAdjuntar"
        Me.chbAdjuntar.Size = New System.Drawing.Size(197, 18)
        Me.chbAdjuntar.TabIndex = 8
        Me.chbAdjuntar.Text = "Comprobante generado desde:"
        Me.chbAdjuntar.UseVisualStyleBackColor = True
        '
        'chbGuia
        '
        Me.chbGuia.AutoSize = True
        Me.chbGuia.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.chbGuia.ForeColor = System.Drawing.Color.Black
        Me.chbGuia.Location = New System.Drawing.Point(280, 155)
        Me.chbGuia.Name = "chbGuia"
        Me.chbGuia.Size = New System.Drawing.Size(100, 18)
        Me.chbGuia.TabIndex = 7
        Me.chbGuia.Text = "Con Guia N° :"
        Me.chbGuia.UseVisualStyleBackColor = True
        '
        'btnBuscarCliente
        '
        Me.btnBuscarCliente.BackgroundImage = Global.Presentacion.My.Resources.Resources.BuscarArchivo_20
        Me.btnBuscarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBuscarCliente.FlatAppearance.BorderSize = 0
        Me.btnBuscarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarCliente.Location = New System.Drawing.Point(288, 56)
        Me.btnBuscarCliente.Name = "btnBuscarCliente"
        Me.btnBuscarCliente.Size = New System.Drawing.Size(27, 22)
        Me.btnBuscarCliente.TabIndex = 1
        Me.btnBuscarCliente.UseVisualStyleBackColor = True
        '
        'txtNumGuia
        '
        Me.txtNumGuia.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtNumGuia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumGuia.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumGuia.ForeColor = System.Drawing.Color.Black
        Me.txtNumGuia.Location = New System.Drawing.Point(387, 152)
        Me.txtNumGuia.Name = "txtNumGuia"
        Me.txtNumGuia.Size = New System.Drawing.Size(342, 22)
        Me.txtNumGuia.TabIndex = 16
        Me.txtNumGuia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtImporteNeto
        '
        Me.txtImporteNeto.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtImporteNeto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteNeto.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtImporteNeto.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtImporteNeto.Location = New System.Drawing.Point(1093, 522)
        Me.txtImporteNeto.Name = "txtImporteNeto"
        Me.txtImporteNeto.Size = New System.Drawing.Size(110, 22)
        Me.txtImporteNeto.TabIndex = 127
        Me.txtImporteNeto.Text = "0.00"
        Me.txtImporteNeto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(992, 528)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(98, 14)
        Me.Label11.TabIndex = 126
        Me.Label11.Text = "Imp. Neto Pago:"
        '
        'txtMontoRetencion
        '
        Me.txtMontoRetencion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtMontoRetencion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoRetencion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtMontoRetencion.ForeColor = System.Drawing.Color.Red
        Me.txtMontoRetencion.Location = New System.Drawing.Point(675, 522)
        Me.txtMontoRetencion.Name = "txtMontoRetencion"
        Me.txtMontoRetencion.Size = New System.Drawing.Size(111, 22)
        Me.txtMontoRetencion.TabIndex = 129
        Me.txtMontoRetencion.Text = "0.00"
        Me.txtMontoRetencion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMontoRetencion
        '
        Me.lblMontoRetencion.AutoSize = True
        Me.lblMontoRetencion.Location = New System.Drawing.Point(561, 528)
        Me.lblMontoRetencion.Name = "lblMontoRetencion"
        Me.lblMontoRetencion.Size = New System.Drawing.Size(55, 14)
        Me.lblMontoRetencion.TabIndex = 128
        Me.lblMontoRetencion.Text = "Mensaje:"
        '
        'lblRetencion
        '
        Me.lblRetencion.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetencion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblRetencion.Location = New System.Drawing.Point(12, 533)
        Me.lblRetencion.Name = "lblRetencion"
        Me.lblRetencion.Size = New System.Drawing.Size(321, 22)
        Me.lblRetencion.TabIndex = 130
        Me.lblRetencion.Text = "MENSAJE"
        Me.lblRetencion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(820, 160)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(65, 14)
        Me.Label10.TabIndex = 131
        Me.Label10.Text = "Vendedor:"
        '
        'cmbVendedor
        '
        Me.cmbVendedor.BackColor = System.Drawing.Color.White
        Me.cmbVendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVendedor.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbVendedor.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbVendedor.FormattingEnabled = True
        Me.cmbVendedor.Location = New System.Drawing.Point(938, 152)
        Me.cmbVendedor.Name = "cmbVendedor"
        Me.cmbVendedor.Size = New System.Drawing.Size(265, 22)
        Me.cmbVendedor.TabIndex = 14
        '
        'txtSubTotal
        '
        Me.txtSubTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotal.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtSubTotal.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotal.Location = New System.Drawing.Point(79, 499)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.Size = New System.Drawing.Size(102, 22)
        Me.txtSubTotal.TabIndex = 133
        Me.txtSubTotal.Text = "0.00"
        Me.txtSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(8, 505)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(64, 14)
        Me.Label20.TabIndex = 132
        Me.Label20.Text = "Sub Total:"
        '
        'txtDescuento
        '
        Me.txtDescuento.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescuento.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtDescuento.ForeColor = System.Drawing.Color.Black
        Me.txtDescuento.Location = New System.Drawing.Point(450, 499)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(84, 22)
        Me.txtDescuento.TabIndex = 135
        Me.txtDescuento.Text = "0.00"
        Me.txtDescuento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(369, 505)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(75, 14)
        Me.Label22.TabIndex = 134
        Me.Label22.Text = "Descuentos:"
        '
        'btnClonar
        '
        Me.btnClonar.FlatAppearance.BorderSize = 0
        Me.btnClonar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnClonar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnClonar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClonar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClonar.Image = Global.Presentacion.My.Resources.Resources.Clonar_22
        Me.btnClonar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClonar.Location = New System.Drawing.Point(464, 570)
        Me.btnClonar.Name = "btnClonar"
        Me.btnClonar.Size = New System.Drawing.Size(79, 26)
        Me.btnClonar.TabIndex = 23
        Me.btnClonar.Text = "Clonar"
        Me.btnClonar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClonar.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(820, 87)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(94, 14)
        Me.Label18.TabIndex = 136
        Me.Label18.Text = "Tipo Operación:"
        '
        'cmbOperacion
        '
        Me.cmbOperacion.BackColor = System.Drawing.Color.White
        Me.cmbOperacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperacion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOperacion.ForeColor = System.Drawing.Color.DodgerBlue
        Me.cmbOperacion.FormattingEnabled = True
        Me.cmbOperacion.Location = New System.Drawing.Point(938, 80)
        Me.cmbOperacion.Name = "cmbOperacion"
        Me.cmbOperacion.Size = New System.Drawing.Size(265, 22)
        Me.cmbOperacion.TabIndex = 12
        '
        'txtAnticipos
        '
        Me.txtAnticipos.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtAnticipos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAnticipos.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtAnticipos.ForeColor = System.Drawing.Color.Black
        Me.txtAnticipos.Location = New System.Drawing.Point(259, 499)
        Me.txtAnticipos.Name = "txtAnticipos"
        Me.txtAnticipos.Size = New System.Drawing.Size(96, 22)
        Me.txtAnticipos.TabIndex = 141
        Me.txtAnticipos.Text = "0.00"
        Me.txtAnticipos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(192, 505)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(60, 14)
        Me.Label24.TabIndex = 140
        Me.Label24.Text = "Anticipos:"
        '
        'chbAnticipos
        '
        Me.chbAnticipos.AutoSize = True
        Me.chbAnticipos.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.chbAnticipos.ForeColor = System.Drawing.Color.Black
        Me.chbAnticipos.Location = New System.Drawing.Point(1071, 181)
        Me.chbAnticipos.Name = "chbAnticipos"
        Me.chbAnticipos.Size = New System.Drawing.Size(134, 18)
        Me.chbAnticipos.TabIndex = 17
        Me.chbAnticipos.Text = "Deducción anticipos"
        Me.chbAnticipos.UseVisualStyleBackColor = True
        '
        'btnCuotas
        '
        Me.btnCuotas.FlatAppearance.BorderSize = 0
        Me.btnCuotas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnCuotas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnCuotas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCuotas.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCuotas.Image = Global.Presentacion.My.Resources.Resources.Clonar_22
        Me.btnCuotas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCuotas.Location = New System.Drawing.Point(820, 178)
        Me.btnCuotas.Name = "btnCuotas"
        Me.btnCuotas.Size = New System.Drawing.Size(81, 21)
        Me.btnCuotas.TabIndex = 15
        Me.btnCuotas.Text = "Cuotas"
        Me.btnCuotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCuotas.UseVisualStyleBackColor = True
        '
        'chbServicios
        '
        Me.chbServicios.AutoSize = True
        Me.chbServicios.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.chbServicios.ForeColor = System.Drawing.Color.Black
        Me.chbServicios.Location = New System.Drawing.Point(938, 181)
        Me.chbServicios.Name = "chbServicios"
        Me.chbServicios.Size = New System.Drawing.Size(109, 18)
        Me.chbServicios.TabIndex = 16
        Me.chbServicios.Text = "Venta Servicios"
        Me.chbServicios.UseVisualStyleBackColor = True
        '
        'btnRetencion
        '
        Me.btnRetencion.FlatAppearance.BorderSize = 0
        Me.btnRetencion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRetencion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.btnRetencion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRetencion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetencion.Image = Global.Presentacion.My.Resources.Resources.Caja_32
        Me.btnRetencion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRetencion.Location = New System.Drawing.Point(565, 569)
        Me.btnRetencion.Name = "btnRetencion"
        Me.btnRetencion.Size = New System.Drawing.Size(130, 26)
        Me.btnRetencion.TabIndex = 24
        Me.btnRetencion.Text = "Apl.Retención"
        Me.btnRetencion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRetencion.UseVisualStyleBackColor = True
        '
        'txtBuscarCodCat
        '
        Me.txtBuscarCodCat.Location = New System.Drawing.Point(11, 208)
        Me.txtBuscarCodCat.Name = "txtBuscarCodCat"
        Me.txtBuscarCodCat.Size = New System.Drawing.Size(133, 22)
        Me.txtBuscarCodCat.TabIndex = 18
        Me.txtBuscarCodCat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtLetras
        '
        Me.txtLetras.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLetras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLetras.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLetras.ForeColor = System.Drawing.Color.Black
        Me.txtLetras.Location = New System.Drawing.Point(938, 128)
        Me.txtLetras.Name = "txtLetras"
        Me.txtLetras.Size = New System.Drawing.Size(265, 22)
        Me.txtLetras.TabIndex = 142
        Me.txtLetras.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(820, 136)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(62, 14)
        Me.Label21.TabIndex = 143
        Me.Label21.Text = "N° Letras:"
        '
        'nCodPed
        '
        Me.nCodPed.DataPropertyName = "nCodPed"
        Me.nCodPed.HeaderText = "nCodPed"
        Me.nCodPed.Name = "nCodPed"
        Me.nCodPed.ReadOnly = True
        Me.nCodPed.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCodPed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodPed.Visible = False
        '
        'nCodProd
        '
        Me.nCodProd.DataPropertyName = "nCodProd"
        Me.nCodProd.HeaderText = "nCodProd"
        Me.nCodProd.Name = "nCodProd"
        Me.nCodProd.ReadOnly = True
        Me.nCodProd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCodProd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodProd.Visible = False
        '
        'nNumOrd
        '
        Me.nNumOrd.DataPropertyName = "nNumOrd"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.nNumOrd.DefaultCellStyle = DataGridViewCellStyle2
        Me.nNumOrd.HeaderText = "N°"
        Me.nNumOrd.Name = "nNumOrd"
        Me.nNumOrd.ReadOnly = True
        Me.nNumOrd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nNumOrd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nNumOrd.Width = 35
        '
        'cCodCat
        '
        Me.cCodCat.DataPropertyName = "cCodCat"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cCodCat.DefaultCellStyle = DataGridViewCellStyle3
        Me.cCodCat.HeaderText = "Cod. Cat"
        Me.cCodCat.Name = "cCodCat"
        Me.cCodCat.ReadOnly = True
        Me.cCodCat.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cCodCat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cCodCat.Width = 120
        '
        'cDescripcion
        '
        Me.cDescripcion.DataPropertyName = "cDescripcion"
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cDescripcion.DefaultCellStyle = DataGridViewCellStyle4
        Me.cDescripcion.HeaderText = "Descripción"
        Me.cDescripcion.Name = "cDescripcion"
        Me.cDescripcion.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cDescripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cDescripcion.Width = 300
        '
        'cUndMed
        '
        Me.cUndMed.DataPropertyName = "cUndMed"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle5.Format = "N0"
        Me.cUndMed.DefaultCellStyle = DataGridViewCellStyle5
        Me.cUndMed.HeaderText = "Und"
        Me.cUndMed.Name = "cUndMed"
        Me.cUndMed.ReadOnly = True
        Me.cUndMed.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cUndMed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cUndMed.Width = 80
        '
        'nCantidad
        '
        Me.nCantidad.DataPropertyName = "nCantidad"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = "1.00"
        Me.nCantidad.DefaultCellStyle = DataGridViewCellStyle6
        Me.nCantidad.HeaderText = "Cant."
        Me.nCantidad.Name = "nCantidad"
        Me.nCantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCantidad.Width = 80
        '
        'nValUni
        '
        Me.nValUni.DataPropertyName = "nValUni"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N6"
        DataGridViewCellStyle7.NullValue = "0.000000"
        Me.nValUni.DefaultCellStyle = DataGridViewCellStyle7
        Me.nValUni.HeaderText = "Valor Unit."
        Me.nValUni.Name = "nValUni"
        Me.nValUni.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nValUni.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nPreUni
        '
        Me.nPreUni.DataPropertyName = "nPreUni"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N6"
        DataGridViewCellStyle8.NullValue = "0.000000"
        Me.nPreUni.DefaultCellStyle = DataGridViewCellStyle8
        Me.nPreUni.HeaderText = "Precio Unit."
        Me.nPreUni.Name = "nPreUni"
        Me.nPreUni.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nPreUni.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nSubTot
        '
        Me.nSubTot.DataPropertyName = "nSubTot"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = "0.00"
        Me.nSubTot.DefaultCellStyle = DataGridViewCellStyle9
        Me.nSubTot.HeaderText = "Sub Total"
        Me.nSubTot.Name = "nSubTot"
        Me.nSubTot.ReadOnly = True
        Me.nSubTot.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nSubTot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nPorDes
        '
        Me.nPorDes.DataPropertyName = "nPorDes"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N5"
        DataGridViewCellStyle10.NullValue = "0.00000"
        Me.nPorDes.DefaultCellStyle = DataGridViewCellStyle10
        Me.nPorDes.HeaderText = "Desc.(%)"
        Me.nPorDes.Name = "nPorDes"
        Me.nPorDes.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nPorDes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nPorDes.Width = 66
        '
        'nMonDes
        '
        Me.nMonDes.DataPropertyName = "nMonDes"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = "0.00"
        Me.nMonDes.DefaultCellStyle = DataGridViewCellStyle11
        Me.nMonDes.HeaderText = "Monto Desc."
        Me.nMonDes.Name = "nMonDes"
        Me.nMonDes.ReadOnly = True
        Me.nMonDes.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nMonDes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nMonDes.Width = 90
        '
        'nSubTotNet
        '
        Me.nSubTotNet.DataPropertyName = "nSubTotNet"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.nSubTotNet.DefaultCellStyle = DataGridViewCellStyle12
        Me.nSubTotNet.HeaderText = "Sub Total Neto"
        Me.nSubTotNet.Name = "nSubTotNet"
        Me.nSubTotNet.ReadOnly = True
        Me.nSubTotNet.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nSubTotNet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'nValVent
        '
        Me.nValVent.DataPropertyName = "nValVent"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.nValVent.DefaultCellStyle = DataGridViewCellStyle13
        Me.nValVent.HeaderText = "nValVent"
        Me.nValVent.Name = "nValVent"
        Me.nValVent.ReadOnly = True
        Me.nValVent.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nValVent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nValVent.Visible = False
        '
        'nCodAnt
        '
        Me.nCodAnt.DataPropertyName = "nCodAnt"
        Me.nCodAnt.HeaderText = "nCodAnt"
        Me.nCodAnt.Name = "nCodAnt"
        Me.nCodAnt.ReadOnly = True
        Me.nCodAnt.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.nCodAnt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.nCodAnt.Visible = False
        '
        'Anticipo
        '
        Me.Anticipo.HeaderText = "Acción"
        Me.Anticipo.Name = "Anticipo"
        Me.Anticipo.ReadOnly = True
        Me.Anticipo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Anticipo.Text = "Anticipo"
        Me.Anticipo.UseColumnTextForButtonValue = True
        Me.Anticipo.Width = 60
        '
        'FrmTomaPedidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1214, 611)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtLetras)
        Me.Controls.Add(Me.txtBuscarCodCat)
        Me.Controls.Add(Me.btnRetencion)
        Me.Controls.Add(Me.chbServicios)
        Me.Controls.Add(Me.btnCuotas)
        Me.Controls.Add(Me.chbAnticipos)
        Me.Controls.Add(Me.txtAnticipos)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cmbOperacion)
        Me.Controls.Add(Me.btnClonar)
        Me.Controls.Add(Me.txtDescuento)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.txtSubTotal)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmbVendedor)
        Me.Controls.Add(Me.lblRetencion)
        Me.Controls.Add(Me.txtMontoRetencion)
        Me.Controls.Add(Me.lblMontoRetencion)
        Me.Controls.Add(Me.txtImporteNeto)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtNumGuia)
        Me.Controls.Add(Me.btnBuscarCliente)
        Me.Controls.Add(Me.chbGuia)
        Me.Controls.Add(Me.chbAdjuntar)
        Me.Controls.Add(Me.cmbTipoRef)
        Me.Controls.Add(Me.txtNumDocRef)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.pnMover)
        Me.Controls.Add(Me.btnFacturar)
        Me.Controls.Add(Me.txtOrdenCompra)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.cmbSerie)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.cmbComprobante)
        Me.Controls.Add(Me.txtImporteTotal)
        Me.Controls.Add(Me.txtIGV)
        Me.Controls.Add(Me.txtVentasGravadas)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.dtgListado)
        Me.Controls.Add(Me.txtNumComprobante)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.dtpFechaVencimiento)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmbMoneda)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpFechaRegistro)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.txtNumDoc)
        Me.Controls.Add(Me.cmbCondPago)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tlsTitulo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmTomaPedidos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tlsTitulo.ResumeLayout(False)
        Me.tlsTitulo.PerformLayout()
        CType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tlsTitulo As System.Windows.Forms.ToolStrip
    Friend WithEvents lblTitulo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnMover As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCerrar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbCondPago As System.Windows.Forms.ComboBox
    Friend WithEvents txtNumDoc As System.Windows.Forms.TextBox
    Friend WithEvents txtCliente As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaRegistro As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents txtTipoCambio As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaVencimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNumComprobante As System.Windows.Forms.Label
    Friend WithEvents dtgListado As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As Label
    Friend WithEvents btnGrabar As Button
    Friend WithEvents txtVentasGravadas As Label
    Friend WithEvents txtIGV As Label
    Friend WithEvents txtImporteTotal As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents cmbComprobante As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cmbSerie As ComboBox
    Friend WithEvents txtOrdenCompra As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents btnFacturar As System.Windows.Forms.Button
    Friend WithEvents pnMover As System.Windows.Forms.Panel
    Friend WithEvents lblMensaje As Label
    Friend WithEvents txtNumDocRef As Label
    Friend WithEvents cmbTipoRef As System.Windows.Forms.ComboBox
    Friend WithEvents chbAdjuntar As CheckBox
    Friend WithEvents chbGuia As CheckBox
    Friend WithEvents btnBuscarCliente As Button
    Friend WithEvents txtNumGuia As Label
    Friend WithEvents txtImporteNeto As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtMontoRetencion As Label
    Friend WithEvents lblMontoRetencion As Label
    Friend WithEvents lblRetencion As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents cmbVendedor As ComboBox
    Friend WithEvents txtSubTotal As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents txtDescuento As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents btnClonar As Button
    Friend WithEvents Label18 As Label
    Friend WithEvents cmbOperacion As ComboBox
    Friend WithEvents txtAnticipos As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents chbAnticipos As CheckBox
    Friend WithEvents btnCuotas As Button
    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents chbServicios As CheckBox
    Friend WithEvents btnRetencion As Button
    Friend WithEvents txtBuscarCodCat As TextBox
    Friend WithEvents txtLetras As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents nCodPed As DataGridViewTextBoxColumn
    Friend WithEvents nCodProd As DataGridViewTextBoxColumn
    Friend WithEvents nNumOrd As DataGridViewTextBoxColumn
    Friend WithEvents cCodCat As DataGridViewTextBoxColumn
    Friend WithEvents cDescripcion As DataGridViewTextBoxColumn
    Friend WithEvents cUndMed As DataGridViewTextBoxColumn
    Friend WithEvents nCantidad As DataGridViewTextBoxColumn
    Friend WithEvents nValUni As DataGridViewTextBoxColumn
    Friend WithEvents nPreUni As DataGridViewTextBoxColumn
    Friend WithEvents nSubTot As DataGridViewTextBoxColumn
    Friend WithEvents nPorDes As DataGridViewTextBoxColumn
    Friend WithEvents nMonDes As DataGridViewTextBoxColumn
    Friend WithEvents nSubTotNet As DataGridViewTextBoxColumn
    Friend WithEvents nValVent As DataGridViewTextBoxColumn
    Friend WithEvents nCodAnt As DataGridViewTextBoxColumn
    Friend WithEvents Anticipo As DataGridViewButtonColumn
End Class
