﻿Imports Negocios
Imports Entidades
Imports Business
Imports System.Configuration
Imports Entities
Public Class FrmTomaPedidos
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim nCodCli As Integer = 0
    Dim CantidadPorducto As Decimal = 0
    Dim bRetencion As Boolean = False
    Dim CodigoGuias As String = ""
    Dim EstadoComprobante As Integer = 1
    Dim BEClientes As New BECustomer
    Dim BEPedido As New BEPedido
    Dim objBEGuia As New List(Of BEGuia)
    Dim BEGuia As New BEGuia
    Dim BECotizacion As New BECotizacion
    Dim ModalidadVentana As Integer = 1 '1=Nuevo, 2=Visuaizacion, 3=RegistroGuia, 4=RegistroCotizacion
    Dim Accion As Boolean = False
    Dim AplicaDetraccion As Boolean = False
    Private Sub FrmTomaPedidos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        KeyPreview = True
        Call CargaCombos()
        Call CargaVendedores()
        Call CargarTipoReferencia()
        Call CargarTipoComprobante()
        Call CargaSeries()
        If ModalidadVentana = 1 Then
            Call Limpiar()
            Call InterfaceNuevo()
        ElseIf ModalidadVentana = 2 Then
            Call Limpiar()
            Call MostrarDatosPedido()
        ElseIf ModalidadVentana = 3 Then
            Call Limpiar()
            Call InterfaceNuevo()
            Call RegistrarDatosGuia()
        ElseIf ModalidadVentana = 4 Then
            Call Limpiar()
            Call InterfaceNuevo()
            Call RegistrarDatosCotizacion()
        End If
    End Sub
    Sub InicioFacturacion(ByVal oBEPedido As BEPedido, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                          ByRef bAccion As Boolean)
        BEPedido = oBEPedido
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub InicioGuia(ByVal oBEGuia As List(Of BEGuia), ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                   ByRef bAccion As Boolean)
        objBEGuia = oBEGuia
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub InicioCotizacion(ByVal oBECotizacion As BECotizacion, ByVal oBEClientes As BECustomer, ByVal pnModalidadVentana As Integer,
                         ByRef bAccion As Boolean)
        BECotizacion = oBECotizacion
        BEClientes = oBEClientes
        ModalidadVentana = pnModalidadVentana
        Me.ShowDialog()
        bAccion = Accion
    End Sub
    Sub RegistrarDatosCotizacion()
        txtNumDocRef.Tag = BECotizacion.gnCodCot
        txtNumDocRef.Text = BECotizacion.gcNumCot
        txtNumDoc.Text = BEClientes.cNumDoc
        nCodCli = BECotizacion.gnCodCli
        txtTipoCambio.Text = FormatNumber(BECotizacion.gnTipCam, 4)
        txtCliente.Text = BEClientes.cRazSoc
        txtDireccion.Text = BEClientes.cDirCli
        cmbCondPago.SelectedValue = BECotizacion.gnCondPag
        bRetencion = BEClientes.bAgeRet

        chbAdjuntar.Checked = True
        cmbTipoRef.SelectedValue = "02"
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        chbGuia.Checked = False
        cmbMoneda.Enabled = False
        cmbMoneda.SelectedValue = BECotizacion.gnTipMon
        cmbComprobante.Select()
    End Sub
    Sub RegistrarDatosGuia()
        Dim Guias As String = ""
        For i = 0 To objBEGuia.Count - 1
            If i = 0 Then
                Guias = objBEGuia(i).gcNumGuia
                CodigoGuias = objBEGuia(i).gnCodGuia.ToString
            Else
                Guias = Guias + ", " + objBEGuia(i).gcNumGuia
                CodigoGuias = CodigoGuias + ", " + objBEGuia(i).gnCodGuia.ToString
            End If
        Next
        txtNumGuia.Text = Guias
        txtNumDocRef.Text = Guias
        txtNumDoc.Text = BEClientes.cNumDoc
        nCodCli = BEClientes.nCodCli
        txtCliente.Text = BEClientes.cRazSoc
        txtDireccion.Text = BEClientes.cDirCli
        cmbMoneda.SelectedValue = BEClientes.nTipPer
        bRetencion = BEClientes.bAgeRet
        chbAdjuntar.Checked = True
        cmbTipoRef.SelectedValue = "09"
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        chbGuia.Checked = True
        chbGuia.Enabled = False
        cmbMoneda.Enabled = True
        If bRetencion Then
            lblRetencion.Visible = True
        Else
            lblRetencion.Visible = False
        End If
        cmbComprobante.Select()
    End Sub
    Sub MostrarDatosPedido()
        chbAnticipos.Checked = BEPedido.gbAplDedAnt
        EstadoComprobante = BEPedido.gnEstado
        txtNumDoc.Text = BEClientes.cNumDoc
        txtNumDocRef.Tag = BEPedido.gnCodCot
        txtNumDocRef.Text = BEPedido.gcNumAdj
        txtOrdenCompra.Text = BEPedido.gcOrdComp
        txtTipoCambio.Text = FormatNumber(BEPedido.gnTipCam, 4)
        txtCliente.Text = BEClientes.cRazSoc
        txtDireccion.Text = BEClientes.cDirCli
        txtNumComprobante.Tag = BEPedido.gnCodPed
        dtpFechaRegistro.Value = BEPedido.gdFecReg
        dtpFechaVencimiento.Value = BEPedido.gdFecVen
        txtNumComprobante.Text = BEPedido.gcNumComp
        txtSubTotal.Text = FormatNumber(BEPedido.gnSubTot, 2)
        txtDescuento.Text = FormatNumber(BEPedido.gnMonDes, 2)
        txtAnticipos.Text = FormatNumber(BEPedido.gnMonAnt, 2)
        txtVentasGravadas.Text = FormatNumber(BEPedido.gnVenGra, 2)
        txtIGV.Text = FormatNumber(BEPedido.gnIGV, 2)
        txtImporteTotal.Text = FormatNumber(BEPedido.gnImpTot, 2)
        cmbComprobante.SelectedValue = BEPedido.gcTipCom
        cmbCondPago.SelectedValue = BEPedido.gnCondPag
        cmbMoneda.SelectedValue = BEPedido.gnTipMon
        Call CargaSeries()
        cmbSerie.SelectedValue = BEPedido.gnCodSer
        cmbTipoRef.SelectedValue = BEPedido.gcTipDocAdj
        chbGuia.Checked = BEPedido.gbAplGuia
        txtMontoRetencion.Text = FormatNumber(BEPedido.gnMonRet, 2)
        txtImporteNeto.Text = FormatNumber(BEPedido.gnImpNetPag, 2)
        bRetencion = BEClientes.bAgeRet
        cmbVendedor.SelectedValue = BEPedido.gnCodVen
        cmbOperacion.SelectedValue = BEPedido.gcTipOpe
        chbServicios.Checked = BEPedido.gbVenSer
        AplicaDetraccion = BEPedido.gbAplDet
        nCodCli = BEPedido.gnCodCli

        If cmbComprobante.SelectedValue <> "99" Then
            If AplicaDetraccion = True Then
                lblRetencion.Visible = True
                lblRetencion.Text = "OPERACIÓN SUJETA A DETRACCIÓN"
                lblMontoRetencion.Visible = True
                lblMontoRetencion.Text = "Monto Detracción"
                txtMontoRetencion.Visible = True
            Else
                If bRetencion And IIf(cmbMoneda.SelectedValue = 1, CDbl(txtImporteTotal.Text), (CDbl(txtImporteTotal.Text) * CDbl(txtTipoCambio.Text))) > CDbl(700) Then
                    lblRetencion.Visible = True
                    lblRetencion.Text = "OPERACIÓN AFECTA A RETENCIÓN"
                    lblMontoRetencion.Visible = True
                    lblMontoRetencion.Text = "Monto Retención"
                    txtMontoRetencion.Visible = True
                Else
                    lblRetencion.Visible = False
                    lblRetencion.Text = "MENSAJE"
                    lblMontoRetencion.Visible = False
                    lblMontoRetencion.Text = "Mensaje"
                    txtMontoRetencion.Visible = False
                End If
            End If
        End If
        If BEPedido.gcTipDocAdj <> "0" Then
            chbAdjuntar.Checked = True
        Else
            chbAdjuntar.Checked = False
            cmbTipoRef.SelectedValue = 0
        End If

        If chbGuia.Checked = True Then
            Dim Obj As New BLPedido
            Dim Guias As String = ""
            Dim oGuias As New DataTable
            oGuias = Obj.ObtenerGuias(BEPedido.gnCodPed, 0, "", cmbComprobante.SelectedValue)
            If oGuias.Rows.Count > 0 Then
                For i = 0 To oGuias.Rows.Count - 1
                    If i = 0 Then
                        Guias = ObtenerDato("NumeroGuiaRemision", oGuias, i)
                    Else
                        Guias = Guias + ", " + ObtenerDato("NumeroGuiaRemision", oGuias, i)
                    End If
                Next
            End If
            txtNumGuia.Text = Guias
        End If

        If cmbTipoRef.SelectedValue = "09" Then
            Dim BLPedido As New BLPedido
            Dim Guias As String = ""
            Dim oGuias As New DataTable
            oGuias = BLPedido.ObtenerGuias(0, 1, BEPedido.gcCodGui, "")
            If oGuias.Rows.Count > 0 Then
                For i = 0 To oGuias.Rows.Count - 1
                    If i = 0 Then
                        Guias = ObtenerDato("NumeroGuiaRemision", oGuias, i)
                    Else
                        Guias = Guias + ", " + ObtenerDato("NumeroGuiaRemision", oGuias, i)
                    End If
                Next
            End If
            txtNumDocRef.Text = Guias
        End If
        If BEPedido.gnCretoLet = 2 Then
            Dim BLPedido As New BLPedido
            Dim Letras As String = ""
            Dim oLetras As New DataTable
            oLetras = BLPedido.ObtenerLetras(BEPedido.gnCodPed)
            If oLetras.Rows.Count > 0 Then
                For i = 0 To oLetras.Rows.Count - 1
                    If i = 0 Then
                        Letras = ObtenerDato("NumeroLetra", oLetras, i)
                    Else
                        Letras = Letras + ", " + ObtenerDato("NumeroLetra", oLetras, i)
                    End If
                Next
            End If
            txtLetras.Text = Letras
        End If

        Call RecuperarDetalle()

        If BEPedido.gnEstado = 1 Then
            lblMensaje.Text = "PEDIDO REGISTRADO(A) - " + BEPedido.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call CalcularTotales()
            Call InterfaceGrabar()
            Call EnfocarFocus(7, 0)
            txtBuscarCodCat.Select()
        ElseIf BEPedido.gnEstado = 2 Then
            lblMensaje.Text = "PEDIDO FACTURADO(A) - " + BEPedido.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceFacturar()
            Call EnfocarFocus(7, 0)
            dtgListado.Select()
        ElseIf BEPedido.gnEstado = 3 Then
            lblMensaje.Text = "PEDIDO ANULADO(A) - " + BEPedido.gcMensaje
            lblMensaje.ForeColor = Color.Red
            Call InterfaceFacturar()
            btnClonar.Enabled = False
            btnCuotas.Enabled = False
            Call EnfocarFocus(7, 0)
            dtgListado.Select()
        ElseIf BEPedido.gnEstado = 4 Then
            lblMensaje.Text = "PEDIDO RELACIONADO(A) - " + BEPedido.gcMensaje
            lblMensaje.ForeColor = Color.DodgerBlue
            Call InterfaceFacturar()
            Call EnfocarFocus(7, 0)
            dtgListado.Select()
        End If
    End Sub
    Sub CargarTipoReferencia()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(370, "02,09")
        Call CargaCombo(dt, cmbTipoRef)
    End Sub
    Sub CargarTipoComprobante()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestroUnico(30, "01,03,99")
        Call CargaCombo(dt, cmbComprobante)
    End Sub
    Sub CargaVendedores()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarVendedores()
        Call CargaCombo(dt, cmbVendedor)
    End Sub
    Sub CargaCombos()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarMaestro("60,70,80,400")
        Call CargaCombo(CreaDatoCombos(dt, 60), cmbMoneda)
        Call CargaCombo(CreaDatoCombos(dt, 70), cmbCondPago)
        Call CargaCombo(CreaDatoCombos(dt, 400), cmbOperacion)
    End Sub
    Sub Limpiar()
        EstadoComprobante = 1
        CodigoGuias = ""
        bRetencion = False
        lblRetencion.Visible = False
        lblRetencion.Text = "MENSAJE"
        lblMontoRetencion.Visible = False
        lblMontoRetencion.Text = "MENSAJE"
        txtMontoRetencion.Visible = False
        txtImporteNeto.Text = "0.00"
        txtMontoRetencion.Text = "0.00"
        txtNumGuia.Text = ""
        txtLetras.Text = ""
        dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
        dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
        CantidadPorducto = 0
        Call LimpiaGrilla(dtgListado)
        txtNumDocRef.Text = ""
        txtNumDocRef.Tag = 0
        lblMensaje.Text = ""
        lblMensaje.ForeColor = Color.DodgerBlue
        nCodCli = 0
        txtNumDoc.Clear()
        txtOrdenCompra.Clear()
        txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
        txtNumComprobante.Text = ""
        txtCliente.Text = ""
        txtDireccion.Text = ""
        txtNumComprobante.Tag = 0
        txtVentasGravadas.Text = "0.00"
        txtSubTotal.Text = "0.00"
        txtDescuento.Text = "0.00"
        txtAnticipos.Text = "0.00"
        txtIGV.Text = "0.00"
        txtImporteTotal.Text = "0.00"
        cmbCondPago.SelectedValue = 1
        cmbComprobante.SelectedValue = "99"
        cmbOperacion.SelectedValue = "0101"
        cmbVendedor.SelectedValue = MDIPrincipal.nCodigoPersonal
        chbGuia.Checked = False
        chbServicios.Checked = False
        chbAnticipos.Checked = False
        chbAdjuntar.Checked = False
        cmbTipoRef.SelectedValue = 0
        AplicaDetraccion = False
        cmbMoneda.SelectedValue = MDIPrincipal.CodigoMonedaPrecio
        Call CargaSeries()
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Private Sub txtNumDoc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumDoc.KeyPress
        Call ValidaEspacio(e)
        Call ValidaNumeros(e)
        Call ValidaSonidoEnter(e)
        If e.KeyChar = Chr(13) Then
            Call ObtenerCliente()
        End If
    End Sub
    Sub ObtenerCliente()
        Dim dt As DataTable
        Dim BLCliente As New BLCliente
        dt = BLCliente.Obtener(txtNumDoc.Text, 2)
        If dt.Rows.Count > 0 Then
            txtCliente.Text = ObtenerDato("Cliente", dt)
            txtDireccion.Text = ObtenerDato("Direccion", dt)
            nCodCli = ObtenerDato("nCodCli", dt)
            bRetencion = ObtenerDato("bAgeRet", dt)
            If ObtenerDato("nCodVen", dt) <> 0 Then
                cmbVendedor.SelectedValue = ObtenerDato("nCodVen", dt)
                cmbVendedor.Enabled = False
            Else
                cmbVendedor.Enabled = True
            End If
            If bRetencion Then
                lblRetencion.Visible = True
            Else
                lblRetencion.Visible = False
            End If
        Else
            Dim TipoDocumento As Integer = 0
            If txtNumDoc.Text.Trim.Length = 8 Then
                TipoDocumento = 1
            ElseIf txtNumDoc.Text.Trim.Length = 11 Then
                TipoDocumento = 6
            Else
                MsgBox("Ingrese un número de documento válido", MsgBoxStyle.Exclamation, "MENSAJE")
                TipoDocumento = 0
                nCodCli = 0
                txtCliente.Text = ""
                txtDireccion.Text = ""
                bRetencion = False
                txtNumDoc.Select()
                Exit Sub
            End If
            Dim Cliente As New BECustomer
            cMensaje = ConsultarCliente(Cliente, txtNumDoc.Text.Trim, TipoDocumento)
            If cMensaje = "" Then
                Dim oDatos As New BECustomer
                txtCliente.Text = Cliente.cRazSoc
                txtDireccion.Text = Cliente.cDirCli
                oDatos.bAgeRet = Cliente.bAgeRet
                If Cliente.bAgeRet = True Then
                    lblRetencion.Visible = False
                Else
                    lblRetencion.Visible = False
                End If
                oDatos.cContactos = ""
                oDatos.cApeMat = ""
                oDatos.cApePat = ""
                oDatos.cNombres = ""
                oDatos.cCorreo = ""
                oDatos.cDirCli = txtDireccion.Text
                oDatos.cNumDoc = txtNumDoc.Text.Trim
                oDatos.cRazSoc = txtCliente.Text
                oDatos.cTipDocCli = Cliente.cTipDocCli
                oDatos.cCodUbiDep = Cliente.cCodUbiDep
                oDatos.cCodUbiDis = Cliente.cCodUbiDis
                oDatos.cCodUbiProv = Cliente.cCodUbiProv
                oDatos.nTipPer = IIf(TipoDocumento = 1, 1, IIf(Microsoft.VisualBasic.Left(txtNumDoc.Text.Trim, 1) = 1, 1, 2))
                oDatos.nTipCli = 0
                If Cliente.cTipDocCli = "1" Then
                    oDatos.cApeMat = Cliente.cApeMat
                    oDatos.cApePat = Cliente.cApePat
                    oDatos.cNombres = Cliente.cNombres
                End If

                cMensaje = ""
                Dim Obj As New BLCliente
                If Obj.Grabar(oDatos, True, cMensaje) = True Then
                    If cMensaje <> "" Then
                        nCodCli = 0
                        MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                        Exit Sub
                    End If
                    nCodCli = oDatos.nCodCli
                Else
                    nCodCli = 0
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                TipoDocumento = 0
                nCodCli = 0
                txtCliente.Text = ""
                txtDireccion.Text = ""
                bRetencion = False
                txtNumDoc.Select()
            End If
        End If
    End Sub
    Private Sub txtNumDoc_TextChanged(sender As Object, e As EventArgs) Handles txtNumDoc.TextChanged

    End Sub
    Private Sub txtTipoCambio_GotFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.GotFocus
        Call ValidaFormatoMonedaGot(txtTipoCambio, 4)
    End Sub
    Private Sub txtTipoCambio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambio.KeyPress
        Call ValidaDecimales(e, txtTipoCambio)
        Call ValidaSonidoEnter(e)

    End Sub
    Private Sub txtTipoCambio_LostFocus(sender As Object, e As EventArgs) Handles txtTipoCambio.LostFocus
        Call ValidaFormatoMonedaLost(txtTipoCambio, 4)
    End Sub
    Private Sub txtTipoCambio_TextChanged(sender As Object, e As EventArgs) Handles txtTipoCambio.TextChanged

    End Sub
    Private Sub dtpFechaVencimiento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaVencimiento.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub dtpFechaVencimiento_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaVencimiento.ValueChanged

    End Sub
    Private Sub dtpFechaRegistro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaRegistro.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub dtpFechaRegistro_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRegistro.ValueChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub cmbCondPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbCondPago.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbCondPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCondPago.SelectedIndexChanged

    End Sub

    Private Sub cmbComprobante_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbComprobante.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbSerie_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSerie.SelectedIndexChanged

    End Sub

    Private Sub cmbSerie_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbSerie.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Sub InterfaceNuevo()
        chbAdjuntar.Checked = False
        cmbTipoRef.SelectedValue = 0
        chbAnticipos.Enabled = True
        dtgListado.Enabled = False
        cmbVendedor.Enabled = True
        chbAdjuntar.Enabled = False
        chbGuia.Enabled = True
        chbServicios.Enabled = True
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = True
        btnFacturar.Enabled = False
        txtNumDoc.Enabled = True
        btnBuscarCliente.Enabled = True
        txtOrdenCompra.Enabled = True
        txtTipoCambio.Enabled = False
        cmbComprobante.Enabled = True
        cmbCondPago.Enabled = True
        txtBuscarCodCat.Enabled = False
        cmbMoneda.Enabled = True
        cmbOperacion.Enabled = True
        cmbSerie.Enabled = True
        cmbTipoRef.Enabled = False
        btnRetencion.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = True
        btnClonar.Enabled = False
        btnCuotas.Enabled = False
    End Sub
    Function ValidaDatos() As Boolean
        ValidaDatos = True
        If nCodCli = 0 Then
            MsgBox("Debe registrar el cliente ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtNumDoc.Select()
            Exit Function
        End If
        If txtNumDoc.Text.Length = 8 Or txtNumDoc.Text.Length = 11 Or txtNumDoc.Text.Length = 12 Then
        Else
            MsgBox("El número de documento del cliente es incorrecto", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            txtNumDoc.Select()
            Exit Function
        End If
        If IsNothing(cmbSerie.SelectedValue) Then
            MsgBox("Debe registrar la serie ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbSerie.Select()
            Exit Function
        End If
        If IsNothing(cmbCondPago.SelectedValue) Then
            MsgBox("Debe registrar la condición de pago ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbCondPago.Select()
            Exit Function
        End If
        If IsNothing(cmbVendedor.SelectedValue) Then
            MsgBox("Debe registrar el vendedor ", MsgBoxStyle.Exclamation, "MENSAJE")
            ValidaDatos = False
            cmbVendedor.Select()
            Exit Function
        End If
        If cmbComprobante.SelectedValue <> "99" Then
            If cmbComprobante.SelectedValue = "01" Then
                If txtNumDoc.Text.Length <> 11 Then
                    ValidaDatos = False
                    MsgBox("Para generar una factura es necesario registrar un cliente de tipo persona  jurídica", MsgBoxStyle.Exclamation, "MENSAJE")
                    txtNumDoc.Select()
                    Exit Function
                End If
            Else
                If txtNumDoc.Text.Length = 8 Or txtNumDoc.Text.Length = 12 Then
                Else
                    ValidaDatos = False
                    MsgBox("Para generar una boleta es necesario registrar un cliente de tipo persona natural", MsgBoxStyle.Exclamation, "MENSAJE")
                    txtNumDoc.Select()
                    Exit Function
                End If
            End If
        End If
    End Function

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        If ValidaDatos() = False Then
            Exit Sub
        End If
        Dim oDatos As New BEPedido
        cMensaje = ""
        oDatos.gnCodCli = nCodCli
        oDatos.gnCodCot = txtNumDocRef.Tag
        oDatos.gdFecReg = dtpFechaRegistro.Value
        oDatos.gdFecVen = dtpFechaVencimiento.Value
        oDatos.gnCondPag = cmbCondPago.SelectedValue
        oDatos.gnTipCam = txtTipoCambio.Text
        oDatos.gcTipCom = cmbComprobante.SelectedValue
        oDatos.gnTipMon = cmbMoneda.SelectedValue
        oDatos.gcTipOpe = cmbOperacion.SelectedValue
        oDatos.gnTipAfecIGV = "10"
        oDatos.gnCodSer = cmbSerie.SelectedValue
        oDatos.gcOrdComp = txtOrdenCompra.Text
        oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
        oDatos.gnCodigoAgencia = MDIPrincipal.CodigoAgencia
        oDatos.gcTipDocAdj = IIf(chbAdjuntar.Checked = True, cmbTipoRef.SelectedValue, 0)
        oDatos.gbAplGuia = chbGuia.Checked
        oDatos.gnCodVen = cmbVendedor.SelectedValue
        oDatos.gnCodEmp = MDIPrincipal.CodigoEmpresa
        oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
        oDatos.gcCodGui = CodigoGuias
        oDatos.gbAplDedAnt = chbAnticipos.Checked
        oDatos.gbVenSer = chbServicios.Checked
        Dim BLPedido As New BLPedido
        If BLPedido.Registrar(oDatos, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Accion = True
            txtNumComprobante.Tag = oDatos.gnCodPed
            EstadoComprobante = 1
            lblMensaje.Text = "PEDIDO REGISTRADO"
            Call RecuperarDetalle()
            Call CalcularTotales()
            Call InterfaceGrabar()
            txtBuscarCodCat.Select()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub BloquearEdicionGrilla(ByVal Valor As Boolean)
        If cmbTipoRef.SelectedValue = "09" Then
            dtgListado.Columns("nCantidad").ReadOnly = True
        Else
            dtgListado.Columns("nCantidad").ReadOnly = Valor
        End If
        If MDIPrincipal.CodigoPrecioLista = 1 Then
            dtgListado.Columns("nValUni").ReadOnly = True
            dtgListado.Columns("nPreUni").ReadOnly = Valor
        Else
            dtgListado.Columns("nValUni").ReadOnly = Valor
            dtgListado.Columns("nPreUni").ReadOnly = True
        End If
        dtgListado.Columns("nPorDes").ReadOnly = Valor
        dtgListado.Columns("cDescripcion").ReadOnly = Valor
    End Sub
    Sub InterfaceGrabar()
        Call BloquearEdicionGrilla(False)
        chbAnticipos.Enabled = False
        dtgListado.Enabled = True
        txtBuscarCodCat.Enabled = True
        chbAdjuntar.Enabled = False
        chbGuia.Enabled = False
        btnRetencion.Enabled = False
        chbServicios.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = True
        If chbAdjuntar.Checked Then
            txtNumDoc.Enabled = False
            btnBuscarCliente.Enabled = False
            cmbCondPago.Enabled = False
            chbGuia.Enabled = False
            txtOrdenCompra.Enabled = False
            cmbComprobante.Enabled = False
            dtpFechaVencimiento.Enabled = False
            cmbVendedor.Enabled = False
        Else
            txtNumDoc.Enabled = True
            btnBuscarCliente.Enabled = True
            cmbCondPago.Enabled = True
            chbGuia.Enabled = True
            txtOrdenCompra.Enabled = True
            cmbComprobante.Enabled = True
            dtpFechaVencimiento.Enabled = True
            cmbVendedor.Enabled = True
        End If
        txtTipoCambio.Enabled = False
        cmbMoneda.Enabled = False
        cmbOperacion.Enabled = False
        cmbSerie.Enabled = False
        dtpFechaRegistro.Enabled = False
        cmbTipoRef.Enabled = False
        btnClonar.Enabled = False
        If cmbCondPago.SelectedValue = 1 Or cmbCondPago.SelectedValue = 2 Then
            btnCuotas.Enabled = False
        Else
            btnCuotas.Enabled = True
        End If
        If cmbTipoRef.SelectedValue = "09" Then
            btnAgregar.Enabled = False
            btnEliminar.Enabled = False
        Else
            btnAgregar.Enabled = True
            btnEliminar.Enabled = True
        End If
    End Sub
    Sub InterfaceFacturar()
        Call BloquearEdicionGrilla(True)
        chbAnticipos.Enabled = False
        chbGuia.Enabled = False
        If bRetencion = True Then
            btnRetencion.Enabled = True
        Else
            btnRetencion.Enabled = False
        End If

        chbServicios.Enabled = False
        cmbVendedor.Enabled = False
        dtgListado.Enabled = True
        chbAdjuntar.Enabled = False
        cmbTipoRef.Enabled = False
        btnAgregar.Enabled = False
        btnEliminar.Enabled = False
        btnGrabar.Enabled = False
        btnFacturar.Enabled = False
        txtBuscarCodCat.Enabled = False
        txtNumDoc.Enabled = False
        btnBuscarCliente.Enabled = False
        txtTipoCambio.Enabled = False
        cmbComprobante.Enabled = False
        cmbCondPago.Enabled = False
        cmbMoneda.Enabled = False
        cmbOperacion.Enabled = False
        cmbSerie.Enabled = False
        dtpFechaRegistro.Enabled = False
        dtpFechaVencimiento.Enabled = False
        btnClonar.Enabled = True
        If cmbCondPago.SelectedValue = 1 Or cmbCondPago.SelectedValue = 2 Then
            btnCuotas.Enabled = False
        Else
            btnCuotas.Enabled = True
        End If
        txtOrdenCompra.Enabled = False
    End Sub

    Private Sub btnGrabar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnGrabar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        FrmConsultaProducto.Inicio(txtNumComprobante.Tag, cmbComprobante.SelectedValue)
        FrmConsultaProducto = Nothing
        Call CalcularTotales()
        Call EnfocarFocus(7, dtgListado.Rows.Count - 1)
        dtgListado.Select()
    End Sub
    Private Sub txtOrdenCompra_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOrdenCompra.KeyPress
        Call ValidaSoloLetras(e)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtOrdenCompra_TextChanged(sender As Object, e As EventArgs) Handles txtOrdenCompra.TextChanged

    End Sub

    Private Sub btnAgregar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnAgregar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEditar_KeyPress(sender As Object, e As KeyPressEventArgs)
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dtgListado.Rows.Count > 0 Then
            Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
            Call EliminarProductoDetalle(txtNumComprobante.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value)
            Call RecuperarDetalle()
            Call CalcularTotales()
            If Fila <> 0 Then
                If dtgListado.Rows.Count - 1 < Fila Then
                    Fila = Fila - 1
                End If
            End If
            Call EnfocarFocus(7, Fila)
            dtgListado.Select()
        End If
    End Sub
    Sub EliminarProductoDetalle(ByVal CodCot As Integer, ByVal nCodProd As Integer)
        cMensaje = ""
        Dim BLPedido As New BLPedido
        If BLPedido.EliminarProducto(CodCot, nCodProd, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            btnAgregar.Select()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Private Sub btnEliminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnEliminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Public Sub RecuperarDetalle()
        Dim dt As DataTable
        Dim BLPedido As New BLPedido
        dt = BLPedido.RecuperarDetalle(txtNumComprobante.Tag)
        Call LlenaAGridView(dt, dtgListado)
        If MDIPrincipal.CodigoPrecioLista = 1 Then
            dtgListado.Columns("nValUni").DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192)
            dtgListado.Columns("nPreUni").DefaultCellStyle.BackColor = Color.White
        Else
            dtgListado.Columns("nValUni").DefaultCellStyle.BackColor = Color.White
            dtgListado.Columns("nPreUni").DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 192)
        End If
    End Sub
    Sub CalcularTotales()
        Dim ValorVenta, TOTAL, IGV, RETENCIONDETRACCION, DESCUENTO, ANTICIPO As Double
        If dtgListado.Rows.Count > 0 Then
            ValorVenta = 0
            TOTAL = 0
            IGV = 0
            RETENCIONDETRACCION = 0
            DESCUENTO = 0
            ANTICIPO = 0
            For i = 0 To dtgListado.Rows.Count - 1
                If dtgListado.Rows(i).Cells("nCodAnt").Value = 0 Then
                    ValorVenta = ValorVenta + CDbl(dtgListado.Rows(i).Cells("nValVent").Value)
                    TOTAL = TOTAL + CDbl(dtgListado.Rows(i).Cells("nSubTot").Value)
                    DESCUENTO = DESCUENTO + (CDbl(dtgListado.Rows(i).Cells("nMonDes").Value))
                End If
                If dtgListado.Rows(i).Cells("nCodAnt").Value <> 0 Then
                    ANTICIPO = ANTICIPO + CDbl(dtgListado.Rows(i).Cells("nValVent").Value) * -1
                End If
            Next
            txtSubTotal.Text = FormatNumber(ValorVenta, 2)
            txtAnticipos.Text = FormatNumber(ANTICIPO, 2)
            txtDescuento.Text = FormatNumber(DESCUENTO, 2)
            txtVentasGravadas.Text = FormatNumber(ValorVenta - DESCUENTO - ANTICIPO, 2)

            If DESCUENTO > 0 Or ANTICIPO > 0 Then
                ValorVenta = txtVentasGravadas.Text
                IGV = FormatNumber(ValorVenta * 0.18, 2)
                txtIGV.Text = FormatNumber(IGV, 2)
                txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
            Else
                If MDIPrincipal.CodigoPrecioLista = 1 Then
                    IGV = FormatNumber(TOTAL - ValorVenta, 2)
                    txtIGV.Text = FormatNumber(IGV, 2)
                    txtImporteTotal.Text = FormatNumber(TOTAL, 2)
                Else
                    IGV = FormatNumber(ValorVenta * 0.18, 2)
                    txtIGV.Text = FormatNumber(IGV, 2)
                    txtImporteTotal.Text = FormatNumber(ValorVenta + IGV, 2)
                End If
            End If
            TOTAL = CDbl(txtImporteTotal.Text)
            If cmbComprobante.SelectedValue <> "99" Then
                If chbServicios.Checked And IIf(cmbMoneda.SelectedValue = 1, TOTAL, (TOTAL * CDbl(txtTipoCambio.Text))) > CDbl(700) Then
                    RETENCIONDETRACCION = FormatNumber(TOTAL * MDIPrincipal.PorcentajeDetraccion, 2)
                    lblRetencion.Visible = True
                    lblRetencion.Text = "OPERACIÓN SUJETA A DETRACCIÓN"
                    AplicaDetraccion = True
                    lblMontoRetencion.Visible = True
                    lblMontoRetencion.Text = "Monto Detracción"
                    txtMontoRetencion.Visible = True
                Else
                    RETENCIONDETRACCION = 0
                    lblRetencion.Visible = False
                    lblRetencion.Text = "MENSAJE"
                    AplicaDetraccion = False
                    lblMontoRetencion.Visible = False
                    lblMontoRetencion.Text = "Mensaje"
                    txtMontoRetencion.Visible = False
                End If

                If AplicaDetraccion = True Then
                    lblRetencion.Visible = True
                    lblRetencion.Text = "OPERACIÓN SUJETA A DETRACCIÓN"
                Else
                    If bRetencion And IIf(cmbMoneda.SelectedValue = 1, TOTAL, (TOTAL * CDbl(txtTipoCambio.Text))) > CDbl(700) Then
                        RETENCIONDETRACCION = FormatNumber(TOTAL * MDIPrincipal.PorcentajeRetencion, 2)
                        lblRetencion.Visible = True
                        lblRetencion.Text = "OPERACIÓN AFECTA A RETENCIÓN"
                        lblMontoRetencion.Visible = True
                        lblMontoRetencion.Text = "Monto Retención"
                        txtMontoRetencion.Visible = True
                    Else
                        RETENCIONDETRACCION = 0
                        lblRetencion.Visible = False
                        lblRetencion.Text = "MENSAJE"
                        lblMontoRetencion.Visible = False
                        lblMontoRetencion.Text = "Mensaje"
                        txtMontoRetencion.Visible = False
                    End If
                End If
            End If
            txtMontoRetencion.Text = FormatNumber(RETENCIONDETRACCION, 2)
            txtImporteNeto.Text = FormatNumber(TOTAL - RETENCIONDETRACCION, 2)

        Else
            txtSubTotal.Text = FormatNumber(0, 2)
            txtDescuento.Text = FormatNumber(0, 2)
            txtAnticipos.Text = FormatNumber(0, 2)
            txtVentasGravadas.Text = FormatNumber(0, 2)
            txtIGV.Text = FormatNumber(0, 2)
            txtImporteTotal.Text = FormatNumber(0, 2)
            txtMontoRetencion.Text = FormatNumber(0, 2)
            txtImporteNeto.Text = FormatNumber(0, 2)
        End If
    End Sub
    Function validaFacturacion() As Boolean
        validaFacturacion = True
        If dtgListado.Rows.Count <= 0 Then
            validaFacturacion = False
            MsgBox("No se puede facturar un pedido sin ningun item", MsgBoxStyle.Exclamation, "MENSAJE")
            Exit Function
        End If
        Return validaFacturacion

    End Function
    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        Dim oDatos As New BEPedido
        If validaFacturacion() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea facturar el pedido?", MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
            If dtpFechaRegistro.Value < Today.Date Then
                If MsgBox("La fecha de emisión del comprobante es menor a la fecha actual, ¿esta seguro que desea facturar el pedido con fecha anterior?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbNo Then
                    Exit Sub
                End If
            End If
            cMensaje = ""
            oDatos.gnCodCli = nCodCli
            oDatos.gnCodPed = txtNumComprobante.Tag
            oDatos.gnSubTot = txtSubTotal.Text
            oDatos.gnMonDes = txtDescuento.Text
            oDatos.gnMonAnt = txtAnticipos.Text
            oDatos.gnVenGra = txtVentasGravadas.Text
            oDatos.gnIGV = txtIGV.Text
            oDatos.gnImpTot = txtImporteTotal.Text
            oDatos.gcUsuActAud = MDIPrincipal.CodUsuario
            oDatos.gnMonRet = txtMontoRetencion.Text
            oDatos.gnImpNetPag = txtImporteNeto.Text
            oDatos.gnCondPag = cmbCondPago.SelectedValue
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            oDatos.gbAplDet = AplicaDetraccion
            oDatos.gnCodVen = cmbVendedor.SelectedValue
            oDatos.gcTipCom = cmbComprobante.SelectedValue
            oDatos.gbAplGuia = chbGuia.Checked
            oDatos.gcOrdComp = txtOrdenCompra.Text
            oDatos.gnCodSer = cmbSerie.SelectedValue
            oDatos.gdFecVen = dtpFechaVencimiento.Value
            Dim BLPedido As New BLPedido
            If BLPedido.Facturar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                Accion = True
                EstadoComprobante = 2
                txtNumComprobante.Text = oDatos.gcNumComp
                If cmbComprobante.SelectedValue <> "99" Then
                    Dim GuiaElectronica As String = ConfigurationManager.AppSettings("GuiaElectronica")
                    If GuiaElectronica = "1" Then
                        cMensaje = ""
                        cMensaje = GenerarComprobanteElectronico(txtNumComprobante.Tag, cmbComprobante.SelectedValue)
                        If cMensaje <> "" Then
                            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE SUNAT")
                        End If
                    Else
                        If chbGuia.Checked = False Then
                            cMensaje = ""
                            cMensaje = GenerarComprobanteElectronico(txtNumComprobante.Tag, cmbComprobante.SelectedValue)
                            If cMensaje <> "" Then
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE SUNAT")
                            End If
                        Else
                            If cmbTipoRef.SelectedValue = "09" Then
                                cMensaje = ""
                                cMensaje = GenerarComprobanteElectronico(txtNumComprobante.Tag, cmbComprobante.SelectedValue)
                                If cMensaje <> "" Then
                                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE SUNAT")
                                End If
                            End If
                        End If
                    End If
                End If
                Me.Close()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub btnFacturar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnFacturar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub
    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 4, MousePosition.Y - Location.Y - eY - 5))
    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub cmbComprobante_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbComprobante.SelectionChangeCommitted

    End Sub
    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        If dtgListado.Rows.Count > 0 Then
            Dim senderGrid = DirectCast(sender, DataGridView)
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                If chbAnticipos.Checked = False Then
                    Exit Sub
                End If
                If dtgListado.Columns(e.ColumnIndex).Name = "Anticipo" Then
                    If EstadoComprobante = 1 Then
                        Dim BEClientes As New BECustomer
                        Dim BEPedido As New BEPedido
                        'FrmConsultaPedidos.InicioPedido(BEPedido, BEClientes, "01,03,99")
                        'FrmConsultaPedidos = Nothing
                        If BEPedido.gnCodPed <> 0 Then
                            cMensaje = ""
                            Dim BLPedido As New BLPedido
                            If BLPedido.ModificarProductoAnticipo(BEPedido.gnCodPed, txtNumComprobante.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value, cMensaje) = True Then
                                If cMensaje <> "" Then
                                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                                    Exit Sub
                                End If
                                Call RecuperarDetalle()
                                Call CalcularTotales()
                                btnAgregar.Select()
                            Else
                                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Sub CargaSeries()
        Dim dt As DataTable
        Dim BLComunes As New BLCommons
        dt = BLComunes.MostrarSeries(cmbComprobante.SelectedValue, MDIPrincipal.CodigoAgencia,
                                     MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbSerie)
    End Sub

    Private Sub dtgListado_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dtgListado.DataError

    End Sub
    Private Sub dtgListado_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellEndEdit
        Dim Columna As Integer = dtgListado.CurrentCell.ColumnIndex
        Dim Fila As Integer = dtgListado.CurrentCell.RowIndex
        If IsDBNull(dtgListado.CurrentRow.Cells("nCantidad").Value) Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        If dtgListado.CurrentRow.Cells("nCantidad").Value = "0" Then
            dtgListado.CurrentRow.Cells("nCantidad").Value = "1"
        End If
        cMensaje = ""
        Dim Precio As Decimal = IIf(MDIPrincipal.CodigoPrecioLista = 0, dtgListado.CurrentRow.Cells("nValUni").Value, dtgListado.CurrentRow.Cells("nPreUni").Value)
        Dim BLPedido As New BLPedido
        If BLPedido.ModificarProducto(txtNumComprobante.Tag, dtgListado.CurrentRow.Cells("nCodProd").Value,
                                                      dtgListado.CurrentRow.Cells("nCantidad").Value, CantidadPorducto, Precio,
                                       dtgListado.CurrentRow.Cells("nPorDes").Value,
                                      dtgListado.CurrentRow.Cells("cDescripcion").Value, cMensaje) Then
            If (cMensaje.Trim).Length > 0 Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            Call LimpiaGrilla(dtgListado)
            Call RecuperarDetalle()
            Call CalcularTotales()
            Call EnfocarFocus(Columna, Fila)
            If Fila <> dtgListado.Rows.Count - 1 Then
                SendKeys.Send(“{UP}”)
            End If
            SendKeys.Send(“{TAB}”)
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
    End Sub
    Sub EnfocarFocus(ByVal Columna As Integer, ByVal Fila As Integer)
        If dtgListado.Rows.Count > 0 Then
            Me.dtgListado.CurrentCell = Me.dtgListado(Columna, Fila)
        End If
    End Sub
    Private Sub dtgListado_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dtgListado.CellBeginEdit
        CantidadPorducto = dtgListado.CurrentRow.Cells("nCantidad").Value
    End Sub

    Private Sub lblMensaje_Click(sender As Object, e As EventArgs) Handles lblMensaje.Click

    End Sub

    Private Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim BEClientes As New BECustomer
        FrmConsultaCliente.Inicio(BEClientes)
        FrmConsultaCliente = Nothing
        If BEClientes.nCodCli <> 0 Then
            nCodCli = BEClientes.nCodCli
            txtCliente.Text = BEClientes.cRazSoc
            txtDireccion.Text = BEClientes.cDirCli
            txtNumDoc.Text = BEClientes.cNumDoc
            bRetencion = BEClientes.bAgeRet
            If BEClientes.nCodVen <> 0 Then
                cmbVendedor.SelectedValue = BEClientes.nCodVen
                cmbVendedor.Enabled = False
            Else
                cmbVendedor.Enabled = True
            End If
            dtpFechaRegistro.Select()
        Else
            btnBuscarCliente.Select()
        End If

    End Sub

    Private Sub cmbCondPago_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbCondPago.SelectionChangeCommitted

    End Sub

    Private Sub chbGuia_CheckedChanged(sender As Object, e As EventArgs) Handles chbGuia.CheckedChanged

    End Sub

    Private Sub btnBuscarCliente_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnBuscarCliente.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbGuia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbGuia.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbVendedor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbVendedor.SelectedIndexChanged

    End Sub

    Private Sub dtgListado_KeyDown(sender As Object, e As KeyEventArgs) Handles dtgListado.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If
        If (e.KeyCode = Keys.Return) Then
            e.Handled = True
            SendKeys.Send(“{TAB}”)
        End If
    End Sub

    Private Sub tlsTitulo_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tlsTitulo.ItemClicked

    End Sub
    Private Sub btnClonar_Click(sender As Object, e As EventArgs) Handles btnClonar.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        If MsgBox("¿Esta seguro que desea duplicar(Clonar) este comprobante?", MessageBoxIcon.Question + vbYesNo, "MENSAJE") = vbYes Then
            Dim oDatos As New BEPedido
            cMensaje = ""
            oDatos.gnCodPed = txtNumComprobante.Tag
            oDatos.gcUsuRegAud = MDIPrincipal.CodUsuario
            oDatos.gcCodPer = MDIPrincipal.CodigoPersonal
            Dim BLPedido As New BLPedido
            If BLPedido.Clonar(oDatos, cMensaje) = True Then
                If cMensaje <> "" Then
                    MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                    Exit Sub
                End If
                txtNumComprobante.Text = ""
                txtNumComprobante.Tag = oDatos.gnCodPed
                txtTipoCambio.Text = FormatNumber(MDIPrincipal.TipoCambioVenta, 4)
                dtpFechaRegistro.Value = MDIPrincipal.FechaSistema
                dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
                lblMensaje.Text = "PEDIDO REGISTRADO"
                Accion = True
                Call RecuperarDetalle()
                Call CalcularTotales()
                Call InterfaceGrabar()
                btnFacturar.Select()
            Else
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        End If
    End Sub

    Private Sub cmbVendedor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbVendedor.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbOperacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbOperacion.SelectedIndexChanged

    End Sub

    Private Sub btnClonar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnClonar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbOperacion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbOperacion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnCuotas_Click(sender As Object, e As EventArgs) Handles btnCuotas.Click
        If EstadoComprobante = 1 Or EstadoComprobante = 2 Then
            FrmGestionCuotas.Inicio(txtNumComprobante.Tag, True)
            FrmGestionCuotas = Nothing
        Else
            FrmGestionCuotas.Inicio(txtNumComprobante.Tag, False)
            FrmGestionCuotas = Nothing
        End If

    End Sub

    Private Sub cmbOperacion_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbOperacion.SelectionChangeCommitted

    End Sub

    Private Sub btnCuotas_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnCuotas.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub chbServicios_CheckedChanged(sender As Object, e As EventArgs) Handles chbServicios.CheckedChanged

    End Sub

    Private Sub FrmTomaPedidos_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F2 And btnGrabar.Enabled = True And btnGrabar.Visible = True Then
            Call btnGrabar_Click(sender, e)
        ElseIf e.KeyCode = Keys.F3 And btnFacturar.Enabled = True And btnFacturar.Visible = True Then
            Call btnFacturar_Click(sender, e)
        End If
    End Sub

    Private Sub btnRetencion_Click(sender As Object, e As EventArgs) Handles btnRetencion.Click
        cMensaje = ""
        Dim BLPedido As New BLPedido
        If BLPedido.AplicaRetencion(txtNumComprobante.Tag, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                Exit Sub
            End If
            MsgBox("Se aplicó la retencion correctamente al comprobante", MsgBoxStyle.Information, "MENSAJE")
            Accion = True
            Me.Close()
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If

    End Sub

    Private Sub chbServicios_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbServicios.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnRetencion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnRetencion.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbTipoRef_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoRef.SelectedIndexChanged

    End Sub

    Private Sub cmbComprobante_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbComprobante.SelectedValueChanged
        If (TypeOf cmbComprobante.SelectedValue IsNot DataRowView) Then
            Call CargaSeries()
        End If
    End Sub

    Private Sub cmbCondPago_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbCondPago.SelectedValueChanged
        If (TypeOf cmbCondPago.SelectedValue IsNot DataRowView) Then
            If cmbCondPago.SelectedValue = 1 Or cmbCondPago.SelectedValue = 2 Then
                dtpFechaVencimiento.Value = MDIPrincipal.FechaSistema
            ElseIf cmbCondPago.SelectedValue = 3 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 7, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 4 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 15, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 5 Or cmbCondPago.SelectedValue = 9 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 30, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 6 Or cmbCondPago.SelectedValue = 10 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 45, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 7 Or cmbCondPago.SelectedValue = 11 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 60, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 12 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 75, dtpFechaRegistro.Value)
            ElseIf cmbCondPago.SelectedValue = 8 Or cmbCondPago.SelectedValue = 13 Then
                dtpFechaVencimiento.Value = DateAdd(DateInterval.Day, 90, dtpFechaRegistro.Value)
            End If
            If cmbCondPago.SelectedValue = 1 Or cmbCondPago.SelectedValue = 2 Then
                btnCuotas.Enabled = False
            Else
                btnCuotas.Enabled = True
            End If
        End If
    End Sub

    Private Sub cmbComprobante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbComprobante.SelectedIndexChanged

    End Sub

    Private Sub txtBuscarCodCat_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarCodCat.TextChanged

    End Sub

    Private Sub cmbOperacion_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbOperacion.SelectedValueChanged
        If (TypeOf cmbOperacion.SelectedValue IsNot DataRowView) Then
            If cmbOperacion.SelectedValue = "0101" Then
                chbAnticipos.Enabled = True
            Else
                chbAnticipos.Enabled = False
                chbAnticipos.Checked = False
            End If
        End If
    End Sub

    Private Sub txtBuscarCodCat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscarCodCat.KeyPress
        Call ValidaEspacio(e)
        Call ValidaSonidoEnter(e)
        If e.KeyChar = Chr(13) Then
            Call ObtenerProducto()
        End If
    End Sub
    Sub ObtenerProducto()
        Dim dt As DataTable
        Dim CodProd As Integer = 0
        Dim BLProductos As New BLProducts
        dt = BLProductos.ObtenerProducto(txtBuscarCodCat.Text, 1, MDIPrincipal.CodigoAgencia, MDIPrincipal.CodigoEmpresa)
        If dt.Rows.Count > 0 Then
            CodProd = ObtenerDato("nCodProd", dt)
            cMensaje = ""
            cMensaje = AgregarProductoDetalle(CodProd, 1, 0)
            If cMensaje <> "" Then
                txtBuscarCodCat.SelectAll()
            Else
                Call RecuperarDetalle()
                Call CalcularTotales()
                txtBuscarCodCat.Clear()
                Call EnfocarFocus(6, dtgListado.Rows.Count - 1)
                txtBuscarCodCat.Select()
            End If
        Else
            MsgBox("El código catálogo registrado no existe", MsgBoxStyle.Exclamation, "MENSAJE")
            txtBuscarCodCat.SelectAll()
        End If
    End Sub

    Private Sub chbAnticipos_CheckedChanged(sender As Object, e As EventArgs) Handles chbAnticipos.CheckedChanged

    End Sub

    Function AgregarProductoDetalle(ByVal CodProd As Integer, ByVal Cantidad As Decimal, ByVal Precio As Double) As String
        cMensaje = ""
        Dim Obj As New BLPedido
        If Obj.AgregarProducto(txtNumComprobante.Tag, CodProd, Cantidad, Precio, cMensaje) = True Then
            If cMensaje <> "" Then
                MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
            End If
        Else
            MsgBox(cMensaje, MsgBoxStyle.Exclamation, "MENSAJE")
        End If
        AgregarProductoDetalle = cMensaje
    End Function

    Private Sub chbAdjuntar_CheckedChanged(sender As Object, e As EventArgs) Handles chbAdjuntar.CheckedChanged

    End Sub

    Private Sub chbAnticipos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chbAnticipos.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class