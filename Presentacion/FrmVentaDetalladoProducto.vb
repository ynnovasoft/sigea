﻿Imports Business
Imports Entidades
Imports System.Windows.Forms
Public Class FrmVentaDetalladoProducto
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Private Sub FrmVentaDetalladoProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call CargaSucursal()
        Call CargaMoneda()
        dtpFechaInicio.Value = DateAdd(DateInterval.Month, -1, dtpFechaFin.Value)
        txtCliente.Tag = 0
        dtpFechaInicio.Focus()
    End Sub
    Sub CargaSucursal()
        Dim dt As DataTable
        Dim BLCommons As New BLCommons
        dt = BLCommons.MostrarAgencias(True, MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbSucursal)
    End Sub
    Sub CargaMoneda()
        Dim dt As DataTable
        Dim BLCommons As New BLCommons
        dt = BLCommons.MostrarMoneda()
        Call CargaCombo(dt, cmbMoneda)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub dtpFechaInicio_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaInicio.ValueChanged

    End Sub

    Private Sub btnExaminar_Click(sender As Object, e As EventArgs) Handles btnExaminar.Click
        Dim BEProductos As New BEProductos
        FrmConsultaProductoMant.Inicio(BEProductos)
        FrmConsultaProductoMant = Nothing
        If BEProductos.gnCodProd <> 0 Then
            txtCliente.Tag = BEProductos.gnCodProd
            txtCliente.Text = BEProductos.gcDescripcion
        End If
    End Sub

    Private Sub dtpFechaFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaFin.ValueChanged

    End Sub

    Private Sub dtpFechaInicio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaInicio.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnVerInforme_Click(sender As Object, e As EventArgs) Handles btnVerInforme.Click
        If txtCliente.Tag = 0 Then
            MsgBox("Debe seleccionar un producto", MsgBoxStyle.Exclamation, "MENSAJE")
            btnExaminar.Focus()
            Exit Sub
        End If
        FrmReportes.ReporteDetalladoVentasPorProducto(dtpFechaInicio.Text, dtpFechaFin.Text, txtCliente.Tag,
                                                       cmbMoneda.SelectedValue, cmbSucursal.SelectedValue)
        FrmReportes = Nothing

    End Sub

    Private Sub btnExaminar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnExaminar.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub txtCliente_Click(sender As Object, e As EventArgs) Handles txtCliente.Click

    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub btnVerInforme_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnVerInforme.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbSucursal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSucursal.SelectedIndexChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbSucursal.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class