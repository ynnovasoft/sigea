﻿Imports Business
Imports System.Windows.Forms
Public Class FrmVentaDetalladoVendedor
    Dim eX, eY As Integer
    Dim Arrastre As Boolean = False
    Dim cMensaje As String = ""
    Dim bNuevo As Boolean = True
    Private Sub FrmVentaDetalladoVendedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpFechaInicio.Value = DateAdd(DateInterval.Month, -1, dtpFechaFin.Value)
        Call CargaVendedores()
        Call CargaSucursal()
        Call CargaMoneda()
        dtpFechaInicio.Focus()
    End Sub
    Sub CargaVendedores()
        Dim dt As New DataTable
        Dim BLCommons As New BLCommons
        dt = BLCommons.MostrarVendedores()
        Call CargaCombo(dt, cmbVendedor)
    End Sub
    Sub CargaSucursal()
        Dim dt As New DataTable
        Dim BLCommons As New BLCommons
        dt = BLCommons.MostrarAgencias(True, MDIPrincipal.CodigoEmpresa)
        Call CargaCombo(dt, cmbSucursal)
    End Sub
    Sub CargaMoneda()
        Dim dt As New DataTable
        Dim BLCommons As New BLCommons
        dt = BLCommons.MostrarMoneda()
        Call CargaCombo(dt, cmbMoneda)
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub pnMover_Paint(sender As Object, e As PaintEventArgs) Handles pnMover.Paint

    End Sub

    Private Sub pnMover_MouseUp(sender As Object, e As MouseEventArgs) Handles pnMover.MouseUp
        Arrastre = False
    End Sub

    Private Sub pnMover_MouseMove(sender As Object, e As MouseEventArgs) Handles pnMover.MouseMove
        If Arrastre Then Location = PointToScreen(New Point(MousePosition.X - Location.X - eX - 8, MousePosition.Y - Location.Y - eY - 157))
    End Sub

    Private Sub pnMover_MouseDown(sender As Object, e As MouseEventArgs) Handles pnMover.MouseDown
        eX = e.X
        eY = e.Y
        Arrastre = True
    End Sub

    Private Sub dtpFechaInicio_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaInicio.ValueChanged

    End Sub

    Private Sub dtpFechaFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaFin.ValueChanged

    End Sub

    Private Sub dtpFechaInicio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaInicio.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub dtpFechaFin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaFin.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub btnVerInforme_Click(sender As Object, e As EventArgs) Handles btnVerInforme.Click
        FrmReportes.ReporteDetalladoVentasVendedor(dtpFechaInicio.Text, dtpFechaFin.Text, cmbVendedor.SelectedValue,
                                                   cmbMoneda.SelectedValue, cmbSucursal.SelectedValue)
        FrmReportes = Nothing

    End Sub

    Private Sub cmbVendedor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbVendedor.SelectedIndexChanged

    End Sub

    Private Sub btnVerInforme_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnVerInforme.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMoneda.SelectedIndexChanged

    End Sub

    Private Sub cmbVendedor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbVendedor.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbSucursal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSucursal.SelectedIndexChanged

    End Sub

    Private Sub cmbMoneda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbMoneda.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub

    Private Sub cmbSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbSucursal.KeyPress
        Call ValidaSonidoEnter(e)
    End Sub
End Class