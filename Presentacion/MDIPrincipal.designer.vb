﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MDIPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDIPrincipal))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.MnuSistemas = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuAccesosSistema = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuAnulacionDocumentos = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuMantenimientoSeries = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuMantenimientoCatálogos = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuMantenimientoUsuarios = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuMantenimientoClientes = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuMantenimientoProveedores = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuVentas = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroDeCotización = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuTomaDePedidos = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroDeGuias = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuListaDePrecios = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuReportesVentas = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDetalladoVentas = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDetalladoVentasPorCliente = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDetalladoVentasPorArticulo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDetalladoVentasPorVendedor = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDetalladoVentasPorDia = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDetalladoVentasPorMarca = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuAlmacen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mNUParteDeSalida = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuActualizaciónYCuadreDeInventario = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuReportesAlmacen = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuInventarioConsolidadoProductoGerencia = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuInventarioConsolidadoPorMarca = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuStockMinimoPorProducto = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuGeneralDeInventario = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuPorMarcaDeInventario = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuCompras = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroOrdenCompra = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroDeCompras = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuReportesCompras = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuGeneralCompras = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuImportacion = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuOrdenDeCompraImportacion = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuContabilidad = New System.Windows.Forms.ToolStripMenuItem()
        Me.AperturaCierreContable = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroCompraExterna = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroLetras = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuReportesContabilidad = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuReporteCompras = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuReporteVentas = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuLetras = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuCaja = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroGastos = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuOperacionesDia = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuOperacionesCaja = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuConsultaDeOperaciones = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuCuentasXCobrar = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuAplicacionDeRetencion = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroCobrosCliente = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuCancelaciónDeComprobantesPorBloque = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuConsultaDeComprabantesAfectosRetencion = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuReportesCXC = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDocumentosCréditoClientes = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDetalladoCuentasXCobrarPorCleinte = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuConsultaDePagos = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuCuentasXPagar = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuRegistroPagosProveedor = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuReportesCXP = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuDocumentosCréditoProveedores = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuServicios = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuMantenimientoActivos = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuCompraLocalServicios = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuComprasServicios = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuMovimientosAlmacenServicios = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuGestionDeCaja = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblCodUsuario = New System.Windows.Forms.ToolStripLabel()
        Me.txtCodUsuario = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.tlBarra = New System.Windows.Forms.ToolStrip()
        Me.lblFecha = New System.Windows.Forms.ToolStripLabel()
        Me.txtFechaSistema = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.lblAgencia = New System.Windows.Forms.ToolStripLabel()
        Me.txtAgencia = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.lblTipoMonedaPrecio = New System.Windows.Forms.ToolStripLabel()
        Me.txtTipoMonedaPrecio = New System.Windows.Forms.ToolStripLabel()
        Me.lblTipoPrecio = New System.Windows.Forms.ToolStripLabel()
        Me.txtTipoPrecList = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.lblCaja = New System.Windows.Forms.ToolStripLabel()
        Me.txtCaja = New System.Windows.Forms.ToolStripLabel()
        Me.lblVersion = New System.Windows.Forms.ToolStripLabel()
        Me.MnuProductos = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuParteIngreso = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuParteSalida = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStripAccesos = New System.Windows.Forms.ToolStrip()
        Me.btnAccesosSistema = New System.Windows.Forms.ToolStripButton()
        Me.btnRegistroUsuarios = New System.Windows.Forms.ToolStripButton()
        Me.btnCliente = New System.Windows.Forms.ToolStripButton()
        Me.btnProveedor = New System.Windows.Forms.ToolStripButton()
        Me.btnRegistroCotizacion = New System.Windows.Forms.ToolStripButton()
        Me.btnRegistroGuias = New System.Windows.Forms.ToolStripButton()
        Me.btnTomaPedidos = New System.Windows.Forms.ToolStripButton()
        Me.lblMensajeCorte = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.txtTipoCambioC = New System.Windows.Forms.ToolStripLabel()
        Me.lblTipoCambioC = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.txtTipoCambioV = New System.Windows.Forms.ToolStripLabel()
        Me.lblTipoCambioV = New System.Windows.Forms.ToolStripLabel()
        Me.MenuStrip.SuspendLayout()
        Me.tlBarra.SuspendLayout()
        Me.MenuStripAccesos.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.AutoSize = False
        Me.MenuStrip.BackColor = System.Drawing.Color.White
        Me.MenuStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MenuStrip.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuSistemas, Me.MnuVentas, Me.MnuAlmacen, Me.MnuCompras, Me.MnuImportacion, Me.MnuContabilidad, Me.MnuCaja, Me.MnuCuentasXCobrar, Me.MnuCuentasXPagar, Me.MnuServicios})
        Me.MenuStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Padding = New System.Windows.Forms.Padding(7, 3, 0, 3)
        Me.MenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip.Size = New System.Drawing.Size(972, 23)
        Me.MenuStrip.Stretch = False
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'MnuSistemas
        '
        Me.MnuSistemas.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuAccesosSistema, Me.MnuAnulacionDocumentos, Me.MnuMantenimientoSeries, Me.MnuMantenimientoCatálogos, Me.MnuMantenimientoUsuarios, Me.MnuMantenimientoClientes, Me.MnuMantenimientoProveedores})
        Me.MnuSistemas.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuSistemas.ForeColor = System.Drawing.Color.Black
        Me.MnuSistemas.Image = Global.Presentacion.My.Resources.Resources.System
        Me.MnuSistemas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuSistemas.Name = "MnuSistemas"
        Me.MnuSistemas.Size = New System.Drawing.Size(88, 20)
        Me.MnuSistemas.Text = "Sistemas"
        '
        'MnuAccesosSistema
        '
        Me.MnuAccesosSistema.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuAccesosSistema.Name = "MnuAccesosSistema"
        Me.MnuAccesosSistema.Size = New System.Drawing.Size(235, 22)
        Me.MnuAccesosSistema.Text = "Accesos Sistema"
        '
        'MnuAnulacionDocumentos
        '
        Me.MnuAnulacionDocumentos.Name = "MnuAnulacionDocumentos"
        Me.MnuAnulacionDocumentos.Size = New System.Drawing.Size(235, 22)
        Me.MnuAnulacionDocumentos.Text = "Anulación Documentos"
        '
        'MnuMantenimientoSeries
        '
        Me.MnuMantenimientoSeries.Name = "MnuMantenimientoSeries"
        Me.MnuMantenimientoSeries.Size = New System.Drawing.Size(235, 22)
        Me.MnuMantenimientoSeries.Text = "Mantenimiento Series"
        '
        'MnuMantenimientoCatálogos
        '
        Me.MnuMantenimientoCatálogos.Name = "MnuMantenimientoCatálogos"
        Me.MnuMantenimientoCatálogos.Size = New System.Drawing.Size(235, 22)
        Me.MnuMantenimientoCatálogos.Text = "Mantenimiento Catálogos"
        '
        'MnuMantenimientoUsuarios
        '
        Me.MnuMantenimientoUsuarios.Name = "MnuMantenimientoUsuarios"
        Me.MnuMantenimientoUsuarios.Size = New System.Drawing.Size(235, 22)
        Me.MnuMantenimientoUsuarios.Text = "Mantenimiento Usuarios"
        '
        'MnuMantenimientoClientes
        '
        Me.MnuMantenimientoClientes.Name = "MnuMantenimientoClientes"
        Me.MnuMantenimientoClientes.Size = New System.Drawing.Size(235, 22)
        Me.MnuMantenimientoClientes.Text = "Mantenimiento Clientes"
        '
        'MnuMantenimientoProveedores
        '
        Me.MnuMantenimientoProveedores.Name = "MnuMantenimientoProveedores"
        Me.MnuMantenimientoProveedores.Size = New System.Drawing.Size(235, 22)
        Me.MnuMantenimientoProveedores.Text = "Mantenimiento Proveedores"
        '
        'MnuVentas
        '
        Me.MnuVentas.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuRegistroDeCotización, Me.MnuTomaDePedidos, Me.MnuRegistroDeGuias, Me.MnuListaDePrecios, Me.MnuReportesVentas})
        Me.MnuVentas.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuVentas.ForeColor = System.Drawing.Color.Black
        Me.MnuVentas.Image = Global.Presentacion.My.Resources.Resources.TomaPedidos32
        Me.MnuVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuVentas.Name = "MnuVentas"
        Me.MnuVentas.Size = New System.Drawing.Size(75, 20)
        Me.MnuVentas.Text = "Ventas"
        '
        'MnuRegistroDeCotización
        '
        Me.MnuRegistroDeCotización.Name = "MnuRegistroDeCotización"
        Me.MnuRegistroDeCotización.Size = New System.Drawing.Size(181, 22)
        Me.MnuRegistroDeCotización.Text = "Cotización"
        '
        'MnuTomaDePedidos
        '
        Me.MnuTomaDePedidos.Name = "MnuTomaDePedidos"
        Me.MnuTomaDePedidos.Size = New System.Drawing.Size(181, 22)
        Me.MnuTomaDePedidos.Text = "Facturación"
        '
        'MnuRegistroDeGuias
        '
        Me.MnuRegistroDeGuias.Name = "MnuRegistroDeGuias"
        Me.MnuRegistroDeGuias.Size = New System.Drawing.Size(181, 22)
        Me.MnuRegistroDeGuias.Text = "Guias"
        '
        'MnuListaDePrecios
        '
        Me.MnuListaDePrecios.Name = "MnuListaDePrecios"
        Me.MnuListaDePrecios.Size = New System.Drawing.Size(181, 22)
        Me.MnuListaDePrecios.Text = "Lista De Productos"
        '
        'MnuReportesVentas
        '
        Me.MnuReportesVentas.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuDetalladoVentas, Me.MnuDetalladoVentasPorCliente, Me.MnuDetalladoVentasPorArticulo, Me.MnuDetalladoVentasPorVendedor, Me.MnuDetalladoVentasPorDia, Me.MnuDetalladoVentasPorMarca})
        Me.MnuReportesVentas.Name = "MnuReportesVentas"
        Me.MnuReportesVentas.Size = New System.Drawing.Size(181, 22)
        Me.MnuReportesVentas.Text = "Reportes"
        '
        'MnuDetalladoVentas
        '
        Me.MnuDetalladoVentas.Name = "MnuDetalladoVentas"
        Me.MnuDetalladoVentas.Size = New System.Drawing.Size(254, 22)
        Me.MnuDetalladoVentas.Text = "Detallado Ventas"
        '
        'MnuDetalladoVentasPorCliente
        '
        Me.MnuDetalladoVentasPorCliente.Name = "MnuDetalladoVentasPorCliente"
        Me.MnuDetalladoVentasPorCliente.Size = New System.Drawing.Size(254, 22)
        Me.MnuDetalladoVentasPorCliente.Text = "Detallado Ventas Por Cliente"
        '
        'MnuDetalladoVentasPorArticulo
        '
        Me.MnuDetalladoVentasPorArticulo.Name = "MnuDetalladoVentasPorArticulo"
        Me.MnuDetalladoVentasPorArticulo.Size = New System.Drawing.Size(254, 22)
        Me.MnuDetalladoVentasPorArticulo.Text = "Detallado Ventas Por Articulo"
        '
        'MnuDetalladoVentasPorVendedor
        '
        Me.MnuDetalladoVentasPorVendedor.Name = "MnuDetalladoVentasPorVendedor"
        Me.MnuDetalladoVentasPorVendedor.Size = New System.Drawing.Size(254, 22)
        Me.MnuDetalladoVentasPorVendedor.Text = "Detallado Ventas Por Vendedor"
        '
        'MnuDetalladoVentasPorDia
        '
        Me.MnuDetalladoVentasPorDia.Name = "MnuDetalladoVentasPorDia"
        Me.MnuDetalladoVentasPorDia.Size = New System.Drawing.Size(254, 22)
        Me.MnuDetalladoVentasPorDia.Text = "Detallado Ventas Por Día"
        '
        'MnuDetalladoVentasPorMarca
        '
        Me.MnuDetalladoVentasPorMarca.Name = "MnuDetalladoVentasPorMarca"
        Me.MnuDetalladoVentasPorMarca.Size = New System.Drawing.Size(254, 22)
        Me.MnuDetalladoVentasPorMarca.Text = "Detallado Ventas Por Marca"
        '
        'MnuAlmacen
        '
        Me.MnuAlmacen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mNUParteDeSalida, Me.MnuActualizaciónYCuadreDeInventario, Me.MnuReportesAlmacen})
        Me.MnuAlmacen.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuAlmacen.Image = Global.Presentacion.My.Resources.Resources.Productos32
        Me.MnuAlmacen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuAlmacen.Name = "MnuAlmacen"
        Me.MnuAlmacen.Size = New System.Drawing.Size(85, 20)
        Me.MnuAlmacen.Text = "Almacen"
        '
        'mNUParteDeSalida
        '
        Me.mNUParteDeSalida.Name = "mNUParteDeSalida"
        Me.mNUParteDeSalida.Size = New System.Drawing.Size(251, 22)
        Me.mNUParteDeSalida.Text = "Movimientos de Almacen"
        '
        'MnuActualizaciónYCuadreDeInventario
        '
        Me.MnuActualizaciónYCuadreDeInventario.Name = "MnuActualizaciónYCuadreDeInventario"
        Me.MnuActualizaciónYCuadreDeInventario.Size = New System.Drawing.Size(251, 22)
        Me.MnuActualizaciónYCuadreDeInventario.Text = "Gestión y Cuadre de inventario"
        '
        'MnuReportesAlmacen
        '
        Me.MnuReportesAlmacen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuInventarioConsolidadoProductoGerencia, Me.MnuInventarioConsolidadoPorMarca, Me.MnuStockMinimoPorProducto, Me.MnuGeneralDeInventario, Me.MnuPorMarcaDeInventario})
        Me.MnuReportesAlmacen.Name = "MnuReportesAlmacen"
        Me.MnuReportesAlmacen.Size = New System.Drawing.Size(251, 22)
        Me.MnuReportesAlmacen.Text = "Reportes"
        '
        'MnuInventarioConsolidadoProductoGerencia
        '
        Me.MnuInventarioConsolidadoProductoGerencia.Name = "MnuInventarioConsolidadoProductoGerencia"
        Me.MnuInventarioConsolidadoProductoGerencia.Size = New System.Drawing.Size(314, 22)
        Me.MnuInventarioConsolidadoProductoGerencia.Text = "Inventario Consolidado Producto Gerencia"
        '
        'MnuInventarioConsolidadoPorMarca
        '
        Me.MnuInventarioConsolidadoPorMarca.Name = "MnuInventarioConsolidadoPorMarca"
        Me.MnuInventarioConsolidadoPorMarca.Size = New System.Drawing.Size(314, 22)
        Me.MnuInventarioConsolidadoPorMarca.Text = "Inventario Consolidado Por Marca"
        '
        'MnuStockMinimoPorProducto
        '
        Me.MnuStockMinimoPorProducto.Name = "MnuStockMinimoPorProducto"
        Me.MnuStockMinimoPorProducto.Size = New System.Drawing.Size(314, 22)
        Me.MnuStockMinimoPorProducto.Text = "Stock Mínimo Por Producto"
        '
        'MnuGeneralDeInventario
        '
        Me.MnuGeneralDeInventario.Name = "MnuGeneralDeInventario"
        Me.MnuGeneralDeInventario.Size = New System.Drawing.Size(314, 22)
        Me.MnuGeneralDeInventario.Text = "Inventario Por Producto"
        '
        'MnuPorMarcaDeInventario
        '
        Me.MnuPorMarcaDeInventario.Name = "MnuPorMarcaDeInventario"
        Me.MnuPorMarcaDeInventario.Size = New System.Drawing.Size(314, 22)
        Me.MnuPorMarcaDeInventario.Text = "Inventario Por Marca"
        '
        'MnuCompras
        '
        Me.MnuCompras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuRegistroOrdenCompra, Me.MnuRegistroDeCompras, Me.MnuReportesCompras})
        Me.MnuCompras.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuCompras.Image = Global.Presentacion.My.Resources.Resources.Compras32
        Me.MnuCompras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuCompras.Name = "MnuCompras"
        Me.MnuCompras.Size = New System.Drawing.Size(114, 20)
        Me.MnuCompras.Text = "Compra Local"
        '
        'MnuRegistroOrdenCompra
        '
        Me.MnuRegistroOrdenCompra.Name = "MnuRegistroOrdenCompra"
        Me.MnuRegistroOrdenCompra.Size = New System.Drawing.Size(160, 22)
        Me.MnuRegistroOrdenCompra.Text = "Orden Compra"
        '
        'MnuRegistroDeCompras
        '
        Me.MnuRegistroDeCompras.Name = "MnuRegistroDeCompras"
        Me.MnuRegistroDeCompras.Size = New System.Drawing.Size(160, 22)
        Me.MnuRegistroDeCompras.Text = "Compras"
        '
        'MnuReportesCompras
        '
        Me.MnuReportesCompras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuGeneralCompras})
        Me.MnuReportesCompras.Name = "MnuReportesCompras"
        Me.MnuReportesCompras.Size = New System.Drawing.Size(160, 22)
        Me.MnuReportesCompras.Text = "Reportes"
        '
        'MnuGeneralCompras
        '
        Me.MnuGeneralCompras.Name = "MnuGeneralCompras"
        Me.MnuGeneralCompras.Size = New System.Drawing.Size(175, 22)
        Me.MnuGeneralCompras.Text = "General Compras"
        '
        'MnuImportacion
        '
        Me.MnuImportacion.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuOrdenDeCompraImportacion})
        Me.MnuImportacion.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuImportacion.Image = Global.Presentacion.My.Resources.Resources.Importacion_32
        Me.MnuImportacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuImportacion.Name = "MnuImportacion"
        Me.MnuImportacion.Size = New System.Drawing.Size(104, 20)
        Me.MnuImportacion.Text = "Importación"
        '
        'MnuOrdenDeCompraImportacion
        '
        Me.MnuOrdenDeCompraImportacion.Name = "MnuOrdenDeCompraImportacion"
        Me.MnuOrdenDeCompraImportacion.Size = New System.Drawing.Size(229, 22)
        Me.MnuOrdenDeCompraImportacion.Text = "Registro Orden de Compra"
        '
        'MnuContabilidad
        '
        Me.MnuContabilidad.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AperturaCierreContable, Me.MnuRegistroCompraExterna, Me.MnuRegistroLetras, Me.MnuReportesContabilidad})
        Me.MnuContabilidad.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuContabilidad.Image = Global.Presentacion.My.Resources.Resources.Contabilidad_32
        Me.MnuContabilidad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuContabilidad.Name = "MnuContabilidad"
        Me.MnuContabilidad.Size = New System.Drawing.Size(106, 20)
        Me.MnuContabilidad.Text = "Contabilidad"
        '
        'AperturaCierreContable
        '
        Me.AperturaCierreContable.Name = "AperturaCierreContable"
        Me.AperturaCierreContable.Size = New System.Drawing.Size(219, 22)
        Me.AperturaCierreContable.Text = "Apertura Cierre Contable"
        '
        'MnuRegistroCompraExterna
        '
        Me.MnuRegistroCompraExterna.Name = "MnuRegistroCompraExterna"
        Me.MnuRegistroCompraExterna.Size = New System.Drawing.Size(219, 22)
        Me.MnuRegistroCompraExterna.Text = "Compra Externa"
        '
        'MnuRegistroLetras
        '
        Me.MnuRegistroLetras.Name = "MnuRegistroLetras"
        Me.MnuRegistroLetras.Size = New System.Drawing.Size(219, 22)
        Me.MnuRegistroLetras.Text = "Letras"
        '
        'MnuReportesContabilidad
        '
        Me.MnuReportesContabilidad.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuReporteCompras, Me.MnuReporteVentas, Me.MnuLetras})
        Me.MnuReportesContabilidad.Name = "MnuReportesContabilidad"
        Me.MnuReportesContabilidad.Size = New System.Drawing.Size(219, 22)
        Me.MnuReportesContabilidad.Text = "Reportes"
        '
        'MnuReporteCompras
        '
        Me.MnuReporteCompras.Name = "MnuReporteCompras"
        Me.MnuReporteCompras.Size = New System.Drawing.Size(127, 22)
        Me.MnuReporteCompras.Text = "Compras"
        '
        'MnuReporteVentas
        '
        Me.MnuReporteVentas.Name = "MnuReporteVentas"
        Me.MnuReporteVentas.Size = New System.Drawing.Size(127, 22)
        Me.MnuReporteVentas.Text = "Ventas"
        '
        'MnuLetras
        '
        Me.MnuLetras.Name = "MnuLetras"
        Me.MnuLetras.Size = New System.Drawing.Size(127, 22)
        Me.MnuLetras.Text = "Letras"
        '
        'MnuCaja
        '
        Me.MnuCaja.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuRegistroGastos, Me.MnuOperacionesDia, Me.MnuOperacionesCaja, Me.MnuConsultaDeOperaciones})
        Me.MnuCaja.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuCaja.Image = Global.Presentacion.My.Resources.Resources.Caja_32
        Me.MnuCaja.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuCaja.Name = "MnuCaja"
        Me.MnuCaja.Size = New System.Drawing.Size(62, 20)
        Me.MnuCaja.Text = "Caja"
        '
        'MnuRegistroGastos
        '
        Me.MnuRegistroGastos.Name = "MnuRegistroGastos"
        Me.MnuRegistroGastos.Size = New System.Drawing.Size(218, 22)
        Me.MnuRegistroGastos.Text = "Gastos"
        '
        'MnuOperacionesDia
        '
        Me.MnuOperacionesDia.Name = "MnuOperacionesDia"
        Me.MnuOperacionesDia.Size = New System.Drawing.Size(218, 22)
        Me.MnuOperacionesDia.Text = "Operaciones Día"
        '
        'MnuOperacionesCaja
        '
        Me.MnuOperacionesCaja.Name = "MnuOperacionesCaja"
        Me.MnuOperacionesCaja.Size = New System.Drawing.Size(218, 22)
        Me.MnuOperacionesCaja.Text = "Operaciones Caja"
        '
        'MnuConsultaDeOperaciones
        '
        Me.MnuConsultaDeOperaciones.Name = "MnuConsultaDeOperaciones"
        Me.MnuConsultaDeOperaciones.Size = New System.Drawing.Size(218, 22)
        Me.MnuConsultaDeOperaciones.Text = "Consulta de Operaciones"
        '
        'MnuCuentasXCobrar
        '
        Me.MnuCuentasXCobrar.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuAplicacionDeRetencion, Me.MnuRegistroCobrosCliente, Me.MnuCancelaciónDeComprobantesPorBloque, Me.MnuConsultaDeComprabantesAfectosRetencion, Me.MnuReportesCXC})
        Me.MnuCuentasXCobrar.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuCuentasXCobrar.Image = Global.Presentacion.My.Resources.Resources.CuentasxCobrar_32
        Me.MnuCuentasXCobrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuCuentasXCobrar.Name = "MnuCuentasXCobrar"
        Me.MnuCuentasXCobrar.Size = New System.Drawing.Size(135, 20)
        Me.MnuCuentasXCobrar.Text = "Cuentas x Cobrar"
        '
        'MnuAplicacionDeRetencion
        '
        Me.MnuAplicacionDeRetencion.Name = "MnuAplicacionDeRetencion"
        Me.MnuAplicacionDeRetencion.Size = New System.Drawing.Size(327, 22)
        Me.MnuAplicacionDeRetencion.Text = "Aplicación de Retencion"
        '
        'MnuRegistroCobrosCliente
        '
        Me.MnuRegistroCobrosCliente.Name = "MnuRegistroCobrosCliente"
        Me.MnuRegistroCobrosCliente.Size = New System.Drawing.Size(327, 22)
        Me.MnuRegistroCobrosCliente.Text = "Cancelación de Comprobantes"
        '
        'MnuCancelaciónDeComprobantesPorBloque
        '
        Me.MnuCancelaciónDeComprobantesPorBloque.Name = "MnuCancelaciónDeComprobantesPorBloque"
        Me.MnuCancelaciónDeComprobantesPorBloque.Size = New System.Drawing.Size(327, 22)
        Me.MnuCancelaciónDeComprobantesPorBloque.Text = "Cancelación de Comprobantes por Bloque"
        '
        'MnuConsultaDeComprabantesAfectosRetencion
        '
        Me.MnuConsultaDeComprabantesAfectosRetencion.Name = "MnuConsultaDeComprabantesAfectosRetencion"
        Me.MnuConsultaDeComprabantesAfectosRetencion.Size = New System.Drawing.Size(327, 22)
        Me.MnuConsultaDeComprabantesAfectosRetencion.Text = "Consulta de Comprobantes Afecto Retencón"
        '
        'MnuReportesCXC
        '
        Me.MnuReportesCXC.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuDocumentosCréditoClientes, Me.MnuDetalladoCuentasXCobrarPorCleinte, Me.MnuConsultaDePagos})
        Me.MnuReportesCXC.Name = "MnuReportesCXC"
        Me.MnuReportesCXC.Size = New System.Drawing.Size(327, 22)
        Me.MnuReportesCXC.Text = "Reportes"
        '
        'MnuDocumentosCréditoClientes
        '
        Me.MnuDocumentosCréditoClientes.Name = "MnuDocumentosCréditoClientes"
        Me.MnuDocumentosCréditoClientes.Size = New System.Drawing.Size(300, 22)
        Me.MnuDocumentosCréditoClientes.Text = "Detallado Cuentas X Cobrar"
        '
        'MnuDetalladoCuentasXCobrarPorCleinte
        '
        Me.MnuDetalladoCuentasXCobrarPorCleinte.Name = "MnuDetalladoCuentasXCobrarPorCleinte"
        Me.MnuDetalladoCuentasXCobrarPorCleinte.Size = New System.Drawing.Size(300, 22)
        Me.MnuDetalladoCuentasXCobrarPorCleinte.Text = "Detallado Cuentas X Cobrar Por Cliente"
        '
        'MnuConsultaDePagos
        '
        Me.MnuConsultaDePagos.Name = "MnuConsultaDePagos"
        Me.MnuConsultaDePagos.Size = New System.Drawing.Size(300, 22)
        Me.MnuConsultaDePagos.Text = "Consulta de Pagos"
        '
        'MnuCuentasXPagar
        '
        Me.MnuCuentasXPagar.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuRegistroPagosProveedor, Me.MnuReportesCXP})
        Me.MnuCuentasXPagar.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuCuentasXPagar.Image = Global.Presentacion.My.Resources.Resources.CuentasxPagar_32
        Me.MnuCuentasXPagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuCuentasXPagar.Name = "MnuCuentasXPagar"
        Me.MnuCuentasXPagar.Size = New System.Drawing.Size(129, 20)
        Me.MnuCuentasXPagar.Text = "Cuentas x Pagar"
        '
        'MnuRegistroPagosProveedor
        '
        Me.MnuRegistroPagosProveedor.Name = "MnuRegistroPagosProveedor"
        Me.MnuRegistroPagosProveedor.Size = New System.Drawing.Size(217, 22)
        Me.MnuRegistroPagosProveedor.Text = "Cancelación de Compras"
        '
        'MnuReportesCXP
        '
        Me.MnuReportesCXP.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuDocumentosCréditoProveedores})
        Me.MnuReportesCXP.Name = "MnuReportesCXP"
        Me.MnuReportesCXP.Size = New System.Drawing.Size(217, 22)
        Me.MnuReportesCXP.Text = "Reportes"
        '
        'MnuDocumentosCréditoProveedores
        '
        Me.MnuDocumentosCréditoProveedores.Name = "MnuDocumentosCréditoProveedores"
        Me.MnuDocumentosCréditoProveedores.Size = New System.Drawing.Size(266, 22)
        Me.MnuDocumentosCréditoProveedores.Text = "Documentos Crédito Proveedores"
        '
        'MnuServicios
        '
        Me.MnuServicios.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuMantenimientoActivos, Me.MnuCompraLocalServicios, Me.MnuMovimientosAlmacenServicios})
        Me.MnuServicios.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuServicios.Image = Global.Presentacion.My.Resources.Resources.Servicios_32
        Me.MnuServicios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MnuServicios.Name = "MnuServicios"
        Me.MnuServicios.Size = New System.Drawing.Size(87, 20)
        Me.MnuServicios.Text = "Servicios"
        '
        'MnuMantenimientoActivos
        '
        Me.MnuMantenimientoActivos.Name = "MnuMantenimientoActivos"
        Me.MnuMantenimientoActivos.Size = New System.Drawing.Size(200, 22)
        Me.MnuMantenimientoActivos.Text = "Listado Activos"
        '
        'MnuCompraLocalServicios
        '
        Me.MnuCompraLocalServicios.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuComprasServicios})
        Me.MnuCompraLocalServicios.Name = "MnuCompraLocalServicios"
        Me.MnuCompraLocalServicios.Size = New System.Drawing.Size(200, 22)
        Me.MnuCompraLocalServicios.Text = "Compra Local"
        '
        'MnuComprasServicios
        '
        Me.MnuComprasServicios.Name = "MnuComprasServicios"
        Me.MnuComprasServicios.Size = New System.Drawing.Size(127, 22)
        Me.MnuComprasServicios.Text = "Compras"
        '
        'MnuMovimientosAlmacenServicios
        '
        Me.MnuMovimientosAlmacenServicios.Name = "MnuMovimientosAlmacenServicios"
        Me.MnuMovimientosAlmacenServicios.Size = New System.Drawing.Size(200, 22)
        Me.MnuMovimientosAlmacenServicios.Text = "Movimientos Almacen"
        '
        'MnuGestionDeCaja
        '
        Me.MnuGestionDeCaja.Name = "MnuGestionDeCaja"
        Me.MnuGestionDeCaja.Size = New System.Drawing.Size(192, 22)
        Me.MnuGestionDeCaja.Text = "Gestión de Caja"
        '
        'lblCodUsuario
        '
        Me.lblCodUsuario.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblCodUsuario.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.lblCodUsuario.ForeColor = System.Drawing.Color.Black
        Me.lblCodUsuario.Name = "lblCodUsuario"
        Me.lblCodUsuario.Size = New System.Drawing.Size(56, 24)
        Me.lblCodUsuario.Text = "Usuario:"
        '
        'txtCodUsuario
        '
        Me.txtCodUsuario.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.txtCodUsuario.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.txtCodUsuario.Name = "txtCodUsuario"
        Me.txtCodUsuario.Size = New System.Drawing.Size(0, 24)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 27)
        '
        'tlBarra
        '
        Me.tlBarra.AutoSize = False
        Me.tlBarra.BackColor = System.Drawing.Color.White
        Me.tlBarra.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tlBarra.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tlBarra.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlBarra.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblFecha, Me.txtFechaSistema, Me.ToolStripSeparator2, Me.lblAgencia, Me.txtAgencia, Me.ToolStripSeparator4, Me.lblCodUsuario, Me.txtCodUsuario, Me.ToolStripSeparator5, Me.lblTipoMonedaPrecio, Me.txtTipoMonedaPrecio, Me.ToolStripSeparator3, Me.lblTipoPrecio, Me.txtTipoPrecList, Me.ToolStripSeparator6, Me.lblCaja, Me.txtCaja, Me.lblVersion})
        Me.tlBarra.Location = New System.Drawing.Point(0, 440)
        Me.tlBarra.Name = "tlBarra"
        Me.tlBarra.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.tlBarra.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlBarra.Size = New System.Drawing.Size(972, 27)
        Me.tlBarra.TabIndex = 14
        Me.tlBarra.Text = "ToolStrip1"
        '
        'lblFecha
        '
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(47, 24)
        Me.lblFecha.Text = "Fecha:"
        '
        'txtFechaSistema
        '
        Me.txtFechaSistema.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.txtFechaSistema.Name = "txtFechaSistema"
        Me.txtFechaSistema.Size = New System.Drawing.Size(0, 24)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'lblAgencia
        '
        Me.lblAgencia.Name = "lblAgencia"
        Me.lblAgencia.Size = New System.Drawing.Size(62, 24)
        Me.lblAgencia.Text = "Sucursal:"
        '
        'txtAgencia
        '
        Me.txtAgencia.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.txtAgencia.Name = "txtAgencia"
        Me.txtAgencia.Size = New System.Drawing.Size(0, 24)
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 27)
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 27)
        '
        'lblTipoMonedaPrecio
        '
        Me.lblTipoMonedaPrecio.ForeColor = System.Drawing.Color.Black
        Me.lblTipoMonedaPrecio.Name = "lblTipoMonedaPrecio"
        Me.lblTipoMonedaPrecio.Size = New System.Drawing.Size(117, 24)
        Me.lblTipoMonedaPrecio.Text = "Moneda Prec.Lista:"
        '
        'txtTipoMonedaPrecio
        '
        Me.txtTipoMonedaPrecio.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.txtTipoMonedaPrecio.Name = "txtTipoMonedaPrecio"
        Me.txtTipoMonedaPrecio.Size = New System.Drawing.Size(0, 24)
        '
        'lblTipoPrecio
        '
        Me.lblTipoPrecio.Name = "lblTipoPrecio"
        Me.lblTipoPrecio.Size = New System.Drawing.Size(68, 24)
        Me.lblTipoPrecio.Text = "Prec.Lista:"
        '
        'txtTipoPrecList
        '
        Me.txtTipoPrecList.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.txtTipoPrecList.Name = "txtTipoPrecList"
        Me.txtTipoPrecList.Size = New System.Drawing.Size(0, 24)
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 27)
        '
        'lblCaja
        '
        Me.lblCaja.Name = "lblCaja"
        Me.lblCaja.Size = New System.Drawing.Size(39, 24)
        Me.lblCaja.Text = "Caja:"
        '
        'txtCaja
        '
        Me.txtCaja.Name = "txtCaja"
        Me.txtCaja.Size = New System.Drawing.Size(0, 24)
        '
        'lblVersion
        '
        Me.lblVersion.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.lblVersion.ForeColor = System.Drawing.Color.Blue
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(0, 24)
        '
        'MnuProductos
        '
        Me.MnuProductos.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MnuProductos.Name = "MnuProductos"
        Me.MnuProductos.Size = New System.Drawing.Size(153, 22)
        Me.MnuProductos.Text = "Productos"
        Me.MnuProductos.Visible = False
        '
        'MnuParteIngreso
        '
        Me.MnuParteIngreso.Name = "MnuParteIngreso"
        Me.MnuParteIngreso.Size = New System.Drawing.Size(153, 22)
        Me.MnuParteIngreso.Text = "Parte Ingreso"
        Me.MnuParteIngreso.Visible = False
        '
        'MnuParteSalida
        '
        Me.MnuParteSalida.Name = "MnuParteSalida"
        Me.MnuParteSalida.Size = New System.Drawing.Size(153, 22)
        Me.MnuParteSalida.Text = "Parte Salida"
        Me.MnuParteSalida.Visible = False
        '
        'MenuStripAccesos
        '
        Me.MenuStripAccesos.BackColor = System.Drawing.Color.White
        Me.MenuStripAccesos.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnAccesosSistema, Me.btnRegistroUsuarios, Me.btnCliente, Me.btnProveedor, Me.btnRegistroCotizacion, Me.btnRegistroGuias, Me.btnTomaPedidos, Me.lblMensajeCorte, Me.ToolStripSeparator1, Me.txtTipoCambioC, Me.lblTipoCambioC, Me.ToolStripSeparator7, Me.txtTipoCambioV, Me.lblTipoCambioV})
        Me.MenuStripAccesos.Location = New System.Drawing.Point(0, 23)
        Me.MenuStripAccesos.Name = "MenuStripAccesos"
        Me.MenuStripAccesos.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStripAccesos.Size = New System.Drawing.Size(972, 25)
        Me.MenuStripAccesos.TabIndex = 20
        Me.MenuStripAccesos.Text = "ToolStrip1"
        '
        'btnAccesosSistema
        '
        Me.btnAccesosSistema.BackColor = System.Drawing.Color.White
        Me.btnAccesosSistema.BackgroundImage = Global.Presentacion.My.Resources.Resources.Login
        Me.btnAccesosSistema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnAccesosSistema.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnAccesosSistema.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnAccesosSistema.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnAccesosSistema.Name = "btnAccesosSistema"
        Me.btnAccesosSistema.Size = New System.Drawing.Size(23, 22)
        Me.btnAccesosSistema.Text = "Accesos Sistema"
        Me.btnAccesosSistema.Visible = False
        '
        'btnRegistroUsuarios
        '
        Me.btnRegistroUsuarios.BackgroundImage = Global.Presentacion.My.Resources.Resources.user
        Me.btnRegistroUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRegistroUsuarios.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRegistroUsuarios.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnRegistroUsuarios.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRegistroUsuarios.Name = "btnRegistroUsuarios"
        Me.btnRegistroUsuarios.Size = New System.Drawing.Size(23, 22)
        Me.btnRegistroUsuarios.Text = "Mantenimiento de Usuarios"
        Me.btnRegistroUsuarios.Visible = False
        '
        'btnCliente
        '
        Me.btnCliente.BackgroundImage = Global.Presentacion.My.Resources.Resources.Cliente_32
        Me.btnCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnCliente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCliente.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnCliente.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCliente.Name = "btnCliente"
        Me.btnCliente.Size = New System.Drawing.Size(23, 22)
        Me.btnCliente.Text = "Mantenimiento Clientes"
        Me.btnCliente.Visible = False
        '
        'btnProveedor
        '
        Me.btnProveedor.BackgroundImage = Global.Presentacion.My.Resources.Resources.Proveedor_32
        Me.btnProveedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnProveedor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnProveedor.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnProveedor.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnProveedor.Name = "btnProveedor"
        Me.btnProveedor.Size = New System.Drawing.Size(23, 22)
        Me.btnProveedor.Text = "Mantenimiento Proveedor"
        Me.btnProveedor.Visible = False
        '
        'btnRegistroCotizacion
        '
        Me.btnRegistroCotizacion.BackgroundImage = Global.Presentacion.My.Resources.Resources.TomaPedidos32
        Me.btnRegistroCotizacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRegistroCotizacion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRegistroCotizacion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegistroCotizacion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRegistroCotizacion.Name = "btnRegistroCotizacion"
        Me.btnRegistroCotizacion.Size = New System.Drawing.Size(23, 22)
        Me.btnRegistroCotizacion.Text = "Cotización"
        Me.btnRegistroCotizacion.Visible = False
        '
        'btnRegistroGuias
        '
        Me.btnRegistroGuias.BackgroundImage = Global.Presentacion.My.Resources.Resources.Title_20
        Me.btnRegistroGuias.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRegistroGuias.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRegistroGuias.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnRegistroGuias.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRegistroGuias.Name = "btnRegistroGuias"
        Me.btnRegistroGuias.Size = New System.Drawing.Size(23, 22)
        Me.btnRegistroGuias.Text = "Guías"
        Me.btnRegistroGuias.Visible = False
        '
        'btnTomaPedidos
        '
        Me.btnTomaPedidos.BackColor = System.Drawing.Color.White
        Me.btnTomaPedidos.BackgroundImage = Global.Presentacion.My.Resources.Resources.Pedidos_32
        Me.btnTomaPedidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnTomaPedidos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnTomaPedidos.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnTomaPedidos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnTomaPedidos.Name = "btnTomaPedidos"
        Me.btnTomaPedidos.Size = New System.Drawing.Size(23, 22)
        Me.btnTomaPedidos.Text = "Facturación"
        Me.btnTomaPedidos.ToolTipText = "Facturación"
        Me.btnTomaPedidos.Visible = False
        '
        'lblMensajeCorte
        '
        Me.lblMensajeCorte.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.lblMensajeCorte.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeCorte.ForeColor = System.Drawing.Color.Red
        Me.lblMensajeCorte.Name = "lblMensajeCorte"
        Me.lblMensajeCorte.Size = New System.Drawing.Size(0, 22)
        Me.lblMensajeCorte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'txtTipoCambioC
        '
        Me.txtTipoCambioC.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.txtTipoCambioC.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.txtTipoCambioC.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.txtTipoCambioC.Name = "txtTipoCambioC"
        Me.txtTipoCambioC.Size = New System.Drawing.Size(0, 22)
        '
        'lblTipoCambioC
        '
        Me.lblTipoCambioC.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.lblTipoCambioC.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.lblTipoCambioC.Name = "lblTipoCambioC"
        Me.lblTipoCambioC.Size = New System.Drawing.Size(77, 22)
        Me.lblTipoCambioC.Text = "T.C Compra"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'txtTipoCambioV
        '
        Me.txtTipoCambioV.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.txtTipoCambioV.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.txtTipoCambioV.ForeColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.txtTipoCambioV.Name = "txtTipoCambioV"
        Me.txtTipoCambioV.Size = New System.Drawing.Size(0, 22)
        '
        'lblTipoCambioV
        '
        Me.lblTipoCambioV.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.lblTipoCambioV.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.lblTipoCambioV.Name = "lblTipoCambioV"
        Me.lblTipoCambioV.Size = New System.Drawing.Size(65, 22)
        Me.lblTipoCambioV.Text = "T.C Venta"
        '
        'MDIPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = Global.Presentacion.My.Resources.Resources.logo_INNOVASOFT
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(972, 467)
        Me.Controls.Add(Me.MenuStripAccesos)
        Me.Controls.Add(Me.tlBarra)
        Me.Controls.Add(Me.MenuStrip)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ForeColor = System.Drawing.Color.Black
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Name = "MDIPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.tlBarra.ResumeLayout(False)
        Me.tlBarra.PerformLayout()
        Me.MenuStripAccesos.ResumeLayout(False)
        Me.MenuStripAccesos.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents MnuSistemas As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MnuAccesosSistema As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MnuVentas As ToolStripMenuItem
    Friend WithEvents MnuGestionDeCaja As ToolStripMenuItem
    Friend WithEvents lblCodUsuario As ToolStripLabel
    Friend WithEvents txtCodUsuario As ToolStripLabel
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents tlBarra As ToolStrip
    Friend WithEvents MnuProductos As ToolStripMenuItem
    Friend WithEvents MnuParteIngreso As ToolStripMenuItem
    Friend WithEvents MnuParteSalida As ToolStripMenuItem
    Friend WithEvents MnuTomaDePedidos As ToolStripMenuItem
    Friend WithEvents MnuAlmacen As ToolStripMenuItem
    Friend WithEvents MnuCompras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MnuReportesCompras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MnuRegistroDeCotización As ToolStripMenuItem
    Friend WithEvents MnuRegistroDeGuias As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblTipoPrecio As ToolStripLabel
    Friend WithEvents txtTipoPrecList As ToolStripLabel
    Friend WithEvents mNUParteDeSalida As ToolStripMenuItem
    Friend WithEvents lblFecha As ToolStripLabel
    Friend WithEvents txtFechaSistema As ToolStripLabel
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents MnuRegistroDeCompras As ToolStripMenuItem
    Friend WithEvents MnuListaDePrecios As ToolStripMenuItem
    Friend WithEvents MnuAnulacionDocumentos As ToolStripMenuItem
    Friend WithEvents MnuCaja As ToolStripMenuItem
    Friend WithEvents MnuOperacionesDia As ToolStripMenuItem
    Friend WithEvents MnuMantenimientoSeries As ToolStripMenuItem
    Friend WithEvents MnuRegistroOrdenCompra As ToolStripMenuItem
    Friend WithEvents MnuCuentasXCobrar As ToolStripMenuItem
    Friend WithEvents MnuCuentasXPagar As ToolStripMenuItem
    Friend WithEvents lblAgencia As ToolStripLabel
    Friend WithEvents txtAgencia As ToolStripLabel
    Friend WithEvents MnuReportesCXC As ToolStripMenuItem
    Friend WithEvents MnuRegistroCobrosCliente As ToolStripMenuItem
    Friend WithEvents MnuRegistroPagosProveedor As ToolStripMenuItem
    Friend WithEvents MnuDocumentosCréditoClientes As ToolStripMenuItem
    Friend WithEvents MnuReportesCXP As ToolStripMenuItem
    Friend WithEvents MnuDocumentosCréditoProveedores As ToolStripMenuItem
    Friend WithEvents MnuGeneralCompras As ToolStripMenuItem
    Friend WithEvents MnuContabilidad As ToolStripMenuItem
    Friend WithEvents MnuRegistroCompraExterna As ToolStripMenuItem
    Friend WithEvents MnuAplicacionDeRetencion As ToolStripMenuItem
    Friend WithEvents MnuConsultaDeComprabantesAfectosRetencion As ToolStripMenuItem
    Friend WithEvents MnuRegistroLetras As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As ToolStripSeparator
    Friend WithEvents MnuOperacionesCaja As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As ToolStripSeparator
    Friend WithEvents lblCaja As ToolStripLabel
    Friend WithEvents txtCaja As ToolStripLabel
    Friend WithEvents MnuConsultaDeOperaciones As ToolStripMenuItem
    Friend WithEvents MnuRegistroGastos As ToolStripMenuItem
    Friend WithEvents MnuReportesContabilidad As ToolStripMenuItem
    Friend WithEvents MnuReporteCompras As ToolStripMenuItem
    Friend WithEvents MnuReporteVentas As ToolStripMenuItem
    Friend WithEvents MnuReportesVentas As ToolStripMenuItem
    Friend WithEvents MnuDetalladoVentasPorCliente As ToolStripMenuItem
    Friend WithEvents MnuDetalladoVentasPorArticulo As ToolStripMenuItem
    Friend WithEvents MnuDetalladoVentas As ToolStripMenuItem
    Friend WithEvents MnuDetalladoVentasPorVendedor As ToolStripMenuItem
    Friend WithEvents MnuMantenimientoCatálogos As ToolStripMenuItem
    Friend WithEvents MnuDetalladoCuentasXCobrarPorCleinte As ToolStripMenuItem
    Friend WithEvents MenuStripAccesos As ToolStrip
    Friend WithEvents btnAccesosSistema As ToolStripButton
    Friend WithEvents btnTomaPedidos As ToolStripButton
    Friend WithEvents btnCliente As ToolStripButton
    Friend WithEvents MnuMantenimientoClientes As ToolStripMenuItem
    Friend WithEvents MnuMantenimientoProveedores As ToolStripMenuItem
    Friend WithEvents btnProveedor As ToolStripButton
    Friend WithEvents MnuMantenimientoUsuarios As ToolStripMenuItem
    Friend WithEvents btnRegistroUsuarios As ToolStripButton
    Friend WithEvents MnuReportesAlmacen As ToolStripMenuItem
    Friend WithEvents MnuGeneralDeInventario As ToolStripMenuItem
    Friend WithEvents MnuPorMarcaDeInventario As ToolStripMenuItem
    Friend WithEvents MnuDetalladoVentasPorDia As ToolStripMenuItem
    Friend WithEvents MnuLetras As ToolStripMenuItem
    Friend WithEvents MnuInventarioConsolidadoProductoGerencia As ToolStripMenuItem
    Friend WithEvents lblMensajeCorte As ToolStripLabel
    Friend WithEvents lblVersion As ToolStripLabel
    Friend WithEvents txtTipoCambioC As ToolStripLabel
    Friend WithEvents lblTipoCambioC As ToolStripLabel
    Friend WithEvents ToolStripSeparator7 As ToolStripSeparator
    Friend WithEvents lblTipoCambioV As ToolStripLabel
    Friend WithEvents txtTipoCambioV As ToolStripLabel
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents lblTipoMonedaPrecio As ToolStripLabel
    Friend WithEvents txtTipoMonedaPrecio As ToolStripLabel
    Friend WithEvents MnuServicios As ToolStripMenuItem
    Friend WithEvents MnuMantenimientoActivos As ToolStripMenuItem
    Friend WithEvents MnuActualizaciónYCuadreDeInventario As ToolStripMenuItem
    Friend WithEvents MnuCancelaciónDeComprobantesPorBloque As ToolStripMenuItem
    Friend WithEvents MnuStockMinimoPorProducto As ToolStripMenuItem
    Friend WithEvents MnuInventarioConsolidadoPorMarca As ToolStripMenuItem
    Friend WithEvents MnuImportacion As ToolStripMenuItem
    Friend WithEvents MnuOrdenDeCompraImportacion As ToolStripMenuItem
    Friend WithEvents btnRegistroGuias As ToolStripButton
    Friend WithEvents btnRegistroCotizacion As ToolStripButton
    Friend WithEvents MnuConsultaDePagos As ToolStripMenuItem
    Friend WithEvents MnuCompraLocalServicios As ToolStripMenuItem
    Friend WithEvents MnuComprasServicios As ToolStripMenuItem
    Friend WithEvents MnuMovimientosAlmacenServicios As ToolStripMenuItem
    Friend WithEvents MnuDetalladoVentasPorMarca As ToolStripMenuItem
    Friend WithEvents AperturaCierreContable As ToolStripMenuItem
End Class
