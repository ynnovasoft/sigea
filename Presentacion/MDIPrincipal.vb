﻿Imports System.Windows.Forms
Imports Negocios
Public Class MDIPrincipal
    Public CodUsuario As String = ""
    Public nCodUsuario As Integer
    Public Usuario As String = ""
    Public RlServidor As String = ""
    Private M, N, O As New ToolStripMenuItem
    Public Agencia As String = ""
    Public CodigoAgencia As Integer
    Dim DatoMenus As DataTable
    Public AperturaDia As Integer = 0
    Public DirAgencia As String = ""
    Public TipoPrecioLista As String = ""
    Public FechaSistema As Date
    Public TipoCambioCompra As Double = 0
    Public TipoCambioVenta As Double = 0
    Public PorcentajeRetencion As Double = 0
    Public AperturaCaja As Boolean = False
    Public CodigoEmpresa As Integer = 0
    Public Empresa As String = ""
    Public MontoApertura As Double = 0
    Public CodigoPrecioLista As Integer = 0
    Public CodigoPersonal As String = ""
    Public Personal As String = ""
    Public MensajeCorte As String = ""
    Public Version As String = ""
    Public CodigoMonedaPrecio As Integer = 0
    Public TipoMonedaPrecio As String = ""
    Public nCodigoPersonal As Integer = 0
    Public PorcentajeDetraccion As Double = 0
    Public NumeroRuc As String = ""
    Public CodigoBaseDatos As Integer = 1
    Public ActualizaPrecio As Boolean = False
    Public GeneraExcelClientes As Boolean = False
    Public AjustaInventario As Boolean = False
    Public PagoSucursal As Boolean = False

    Sub CargaDatosGenerales()
        Dim dt As New DataTable
        Dim BLSecurity As New BLSecurity
        dt = BLSecurity.RecuperaDatosLogueo(CodUsuario, CodigoAgencia, CodigoEmpresa, CodigoPersonal)
        nCodUsuario = ObtenerDato("nCodUsuario", dt)
        Usuario = ObtenerDato("Usuario", dt)
        RlServidor = ObtenerDato("RutaServidor", dt)
        Agencia = ObtenerDato("Agencia", dt)
        CodigoAgencia = ObtenerDato("CodigoAgencia", dt)
        DirAgencia = ObtenerDato("Direccion", dt)
        TipoPrecioLista = ObtenerDato("TipoPrecioLista", dt)
        FechaSistema = ObtenerDato("dFechaSistema", dt)
        TipoCambioCompra = ObtenerDato("nTipCamCom", dt)
        TipoCambioVenta = ObtenerDato("nTipCamVen", dt)
        AperturaDia = ObtenerDato("AperturaDia", dt)
        AperturaCaja = ObtenerDato("AperturaCaja", dt)
        PorcentajeRetencion = ObtenerDato("nPorRet", dt)
        CodigoEmpresa = ObtenerDato("nCodEmp", dt)
        Empresa = ObtenerDato("Empresa", dt)
        MontoApertura = ObtenerDato("MontoApertura", dt)
        CodigoPrecioLista = ObtenerDato("CodigoPrecioLista", dt)
        Personal = ObtenerDato("Personal", dt)
        Version = ObtenerDato("Version", dt)
        CodigoMonedaPrecio = ObtenerDato("CodigoMonedaPrecio", dt)
        TipoMonedaPrecio = ObtenerDato("TipoMonedaPrecio", dt)
        nCodigoPersonal = ObtenerDato("nCodigoPersonal", dt)
        PorcentajeDetraccion = ObtenerDato("nPorDet", dt)
        NumeroRuc = ObtenerDato("cNumRuc", dt)
        ActualizaPrecio = ObtenerDato("bActPre", dt)
        GeneraExcelClientes = ObtenerDato("bGenExcCli", dt)
        AjustaInventario = ObtenerDato("bAjuInv", dt)
        PagoSucursal = ObtenerDato("bPagSuc", dt)
    End Sub

    Private Sub MDIPrincipal_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Application.Exit()
    End Sub
    Public Sub MDIPrincipal_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim ctl As Control
        Dim ctlMDI As MdiClient
        For Each ctl In Me.Controls
            Try
                ctlMDI = CType(ctl, MdiClient)
                ctlMDI.BackColor = Me.BackColor
            Catch exc As InvalidCastException
            End Try
        Next
        Call CargaDatosGenerales()
        Me.txtCodUsuario.Text = CodUsuario
        Me.Text = "BIENVENIDO: " & Personal & " ~ " & Empresa
        Me.txtTipoPrecList.Text = TipoPrecioLista
        Me.txtFechaSistema.Text = FechaSistema
        Me.txtAgencia.Text = Agencia
        Me.txtTipoCambioC.Text = FormatNumber(TipoCambioCompra, 4)
        Me.txtTipoCambioV.Text = FormatNumber(TipoCambioVenta, 4)
        Me.txtCaja.Text = IIf(AperturaCaja = True, "APERTURADA", "CERRADA")
        Me.lblVersion.Text = Version
        Me.txtTipoMonedaPrecio.Text = TipoMonedaPrecio
        If MensajeCorte = "" Then
            Me.lblMensajeCorte.Text = "Licencia SIGEA(Sistema de Gestión Administrativo): ACTIVA"
            lblMensajeCorte.ForeColor = Color.Blue
        Else
            Me.lblMensajeCorte.Text = MensajeCorte
            lblMensajeCorte.ForeColor = Color.Red
        End If

        If AperturaCaja = True Then
            txtCaja.ForeColor = Color.Blue
        Else
            txtCaja.ForeColor = Color.Red
        End If
        Call CargarMenus()
        Call CargarAccesoRapido()
    End Sub
    Sub CargarMenus()
        Dim BLSecurity As New BLSecurity
        DatoMenus = BLSecurity.RecuperaMenu(Me.nCodUsuario, CodigoAgencia, CodigoEmpresa)
        For i = 0 To MenuStrip.Items.Count - 1
            M = MenuStrip.Items(i)
            If BuscarMenu(M.Name.ToString, DatoMenus) > 0 Then
                M.Visible = True
                For l = 0 To M.DropDownItems.Count - 1
                    N = M.DropDownItems(l)
                    If BuscarMenu(N.Name.ToString, DatoMenus) > 0 Then
                        N.Visible = True
                        For k = 0 To N.DropDownItems.Count - 1
                            O = N.DropDownItems(k)
                            If BuscarMenu(O.Name.ToString, DatoMenus) > 0 Then
                                O.Visible = True
                            Else
                                O.Visible = False
                            End If
                        Next
                    Else
                        N.Visible = False
                    End If
                Next
            Else
                M.Visible = False
            End If
        Next
    End Sub
    Sub CargarAccesoRapido()
        Dim BLSecurity As New BLSecurity
        DatoMenus = BLSecurity.RecuperaMenu(Me.nCodUsuario, CodigoAgencia, CodigoEmpresa)
        If BuscarMenu("MnuAccesosSistema", DatoMenus) Then
            btnAccesosSistema.Visible = True
        Else
            btnAccesosSistema.Visible = False
        End If
        If BuscarMenu("MnuMantenimientoUsuarios", DatoMenus) Then
            btnRegistroUsuarios.Visible = True
        Else
            btnRegistroUsuarios.Visible = False
        End If
        If BuscarMenu("MnuMantenimientoClientes", DatoMenus) Then
            btnCliente.Visible = True
        Else
            btnCliente.Visible = False
        End If
        If BuscarMenu("MnuMantenimientoProveedores", DatoMenus) Then
            btnProveedor.Visible = True
        Else
            btnProveedor.Visible = False
        End If
        If BuscarMenu("MnuRegistroDeCotización", DatoMenus) Then
            btnRegistroCotizacion.Visible = True
        Else
            btnRegistroCotizacion.Visible = False
        End If
        If BuscarMenu("MnuRegistroDeGuias", DatoMenus) Then
            btnRegistroGuias.Visible = True
        Else
            btnRegistroGuias.Visible = False
        End If
        If BuscarMenu("MnuTomaDePedidos", DatoMenus) Then
            btnTomaPedidos.Visible = True
        Else
            btnTomaPedidos.Visible = False
        End If
    End Sub
    Private Sub MnuAccesosSistema_Click(sender As Object, e As EventArgs) Handles MnuAccesosSistema.Click
        FrmPermisos.MdiParent = Me
        FrmPermisos.Show()
    End Sub

    Private Sub MnuRegistroDeCotización_Click(sender As Object, e As EventArgs) Handles MnuRegistroDeCotización.Click
        FrmConsultaCotizacion.MdiParent = Me
        FrmConsultaCotizacion.Show()
    End Sub

    Private Sub MnuTomaPedidos_Click(sender As Object, e As EventArgs) Handles MnuTomaDePedidos.Click
        FrmConsultaImpresionComprobantes.MdiParent = Me
        FrmConsultaImpresionComprobantes.Show()
    End Sub

    Private Sub mNUParteDeSalida_Click(sender As Object, e As EventArgs) Handles mNUParteDeSalida.Click
        FrmConsultaAlmacen.MdiParent = Me
        FrmConsultaAlmacen.Show()
    End Sub

    Private Sub MnuRegistroCompras_Click(sender As Object, e As EventArgs) Handles MnuRegistroDeCompras.Click
        FrmConsultaCompras.MdiParent = Me
        FrmConsultaCompras.Show()
    End Sub

    Private Sub MnuRegistroDeGuias_Click(sender As Object, e As EventArgs) Handles MnuRegistroDeGuias.Click
        FrmConsultaGuia.MdiParent = Me
        FrmConsultaGuia.Show()
    End Sub

    Private Sub MnuListaDePrecios_Click(sender As Object, e As EventArgs) Handles MnuListaDePrecios.Click
        FrmConsultaListaPrecios.MdiParent = Me
        FrmConsultaListaPrecios.Show()
    End Sub

    Private Sub MnuAnulacionDocumentos_Click(sender As Object, e As EventArgs) Handles MnuAnulacionDocumentos.Click
        FrmAnulacionDocumentos.MdiParent = Me
        FrmAnulacionDocumentos.Show()
    End Sub

    Private Sub MnuMantenimientoSeries_Click(sender As Object, e As EventArgs) Handles MnuMantenimientoSeries.Click
        FrmMantenimientoSeries.MdiParent = Me
        FrmMantenimientoSeries.Show()
    End Sub

    Private Sub MnuRegistroOrdenCompra_Click(sender As Object, e As EventArgs) Handles MnuRegistroOrdenCompra.Click
        FrmConsultaOrdenCompras.MdiParent = Me
        FrmConsultaOrdenCompras.Show()
    End Sub

    Private Sub MnuRegistroCobrosCliente_Click(sender As Object, e As EventArgs) Handles MnuRegistroCobrosCliente.Click
        FrmConsultaDocumentosCreditoClientes.MdiParent = Me
        FrmConsultaDocumentosCreditoClientes.Show()
    End Sub

    Private Sub MnuRegistroPagosProveedor_Click(sender As Object, e As EventArgs) Handles MnuRegistroPagosProveedor.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        FrmRegistroCobroProveedores.MdiParent = Me
        FrmRegistroCobroProveedores.Show()
    End Sub

    Private Sub MnuDocumentosCréditoClientes_Click(sender As Object, e As EventArgs) Handles MnuDocumentosCréditoClientes.Click
        FrmReportes.ReporteGeneralCuentasPorCobrar()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuDocumentosCréditoProveedores_Click(sender As Object, e As EventArgs) Handles MnuDocumentosCréditoProveedores.Click
        FrmReportes.ReporteGeneralCreditoproveedores()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuGeneralCompras_Click(sender As Object, e As EventArgs) Handles MnuGeneralCompras.Click
        FrmReportes.ReporteGeneralCompras()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuRegistroCompraExterna_Click(sender As Object, e As EventArgs) Handles MnuRegistroCompraExterna.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        FrmRegistroCompraExterna.MdiParent = Me
        FrmRegistroCompraExterna.Show()
    End Sub

    Private Sub MnuAplicacionDeRetencion_Click(sender As Object, e As EventArgs) Handles MnuAplicacionDeRetencion.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        FrmRegistroRetenciones.MdiParent = Me
        FrmRegistroRetenciones.Show()
    End Sub
    Private Sub MnuConsultaDeComprabantesAfectosRetencion_Click(sender As Object, e As EventArgs) Handles MnuConsultaDeComprabantesAfectosRetencion.Click
        FrmConsultaComprobanteRetencion.MdiParent = Me
        FrmConsultaComprobanteRetencion.Show()
    End Sub

    Private Sub MnuRegistroLetras_Click(sender As Object, e As EventArgs) Handles MnuRegistroLetras.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        FrmRegistroLetras.MdiParent = Me
        FrmRegistroLetras.Show()
    End Sub

    Private Sub MnuOperacionesDia_Click(sender As Object, e As EventArgs) Handles MnuOperacionesDia.Click
        FrmAperturaCierreDia.MdiParent = Me
        FrmAperturaCierreDia.Show()
    End Sub
    Private Sub MnuConsultaDeOperaciones_Click(sender As Object, e As EventArgs) Handles MnuConsultaDeOperaciones.Click
        FrmConsultaOperaciones.MdiParent = Me
        FrmConsultaOperaciones.Show()
    End Sub

    Private Sub MnuRegistroGastos_Click_1(sender As Object, e As EventArgs) Handles MnuRegistroGastos.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        FrmRegistroGastos.MdiParent = Me
        FrmRegistroGastos.Show()
    End Sub

    Private Sub MnuReporteCompras_Click(sender As Object, e As EventArgs) Handles MnuReporteCompras.Click
        FrmReportes.ReporteGeneralComprasContabilidad()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuDetalladoVentasPorCliente_Click(sender As Object, e As EventArgs) Handles MnuDetalladoVentasPorCliente.Click
        FrmVentaDetalladoClientes.MdiParent = Me
        FrmVentaDetalladoClientes.Show()
    End Sub

    Private Sub MnuDetalladoVentasPorArticulo_Click(sender As Object, e As EventArgs) Handles MnuDetalladoVentasPorArticulo.Click
        FrmVentaDetalladoProducto.MdiParent = Me
        FrmVentaDetalladoProducto.Show()
    End Sub

    Private Sub MnuDetalladoVentasPorVendedor_Click(sender As Object, e As EventArgs) Handles MnuDetalladoVentasPorVendedor.Click
        FrmVentaDetalladoVendedor.MdiParent = Me
        FrmVentaDetalladoVendedor.Show()
    End Sub

    Private Sub MnuDetalladoVentas_Click(sender As Object, e As EventArgs) Handles MnuDetalladoVentas.Click
        FrmReportes.ReporteDetalladoVentas()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuMantenimientoCatálogos_Click(sender As Object, e As EventArgs) Handles MnuMantenimientoCatálogos.Click
        FrmMantenimientoCatalogos.MdiParent = Me
        FrmMantenimientoCatalogos.Show()
    End Sub

    Private Sub MnuDetalladoCuentasXCobrarPorCleinte_Click(sender As Object, e As EventArgs) Handles MnuDetalladoCuentasXCobrarPorCleinte.Click
        FrmReportes.ReporteGeneralCuentasPorCobrarXCliente()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuReporteVentas_Click(sender As Object, e As EventArgs) Handles MnuReporteVentas.Click
        FrmReportes.ReporteGeneralVentasContabilidad()
        FrmReportes = Nothing
    End Sub

    Private Sub btnAccesosSistema_Click(sender As Object, e As EventArgs) Handles btnAccesosSistema.Click
        FrmPermisos.MdiParent = Me
        FrmPermisos.Show()
    End Sub

    Private Sub MnuOperacionesCaja_Click(sender As Object, e As EventArgs) Handles MnuOperacionesCaja.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        FrmCierreOperacionesCaja.MdiParent = Me
        FrmCierreOperacionesCaja.Show()
    End Sub

    Private Sub btnTomaPedidos_Click(sender As Object, e As EventArgs) Handles btnTomaPedidos.Click
        FrmConsultaImpresionComprobantes.MdiParent = Me
        FrmConsultaImpresionComprobantes.Show()
    End Sub

    Private Sub btnCliente_Click(sender As Object, e As EventArgs) Handles btnCliente.Click
        FrmListadoClientes.MdiParent = Me
        FrmListadoClientes.Show()
    End Sub

    Private Sub MnuMantenimientoClientes_Click(sender As Object, e As EventArgs) Handles MnuMantenimientoClientes.Click
        FrmListadoClientes.MdiParent = Me
        FrmListadoClientes.Show()
    End Sub

    Private Sub MnuMantenimientoProveedores_Click(sender As Object, e As EventArgs) Handles MnuMantenimientoProveedores.Click
        FrmProveedor.MdiParent = Me
        FrmProveedor.Show()
    End Sub

    Private Sub btnProveedor_Click(sender As Object, e As EventArgs) Handles btnProveedor.Click
        FrmProveedor.MdiParent = Me
        FrmProveedor.Show()
    End Sub

    Private Sub MnuMantenimientoUsuarios_Click(sender As Object, e As EventArgs) Handles MnuMantenimientoUsuarios.Click
        FrmMantenimientoUsuarios.MdiParent = Me
        FrmMantenimientoUsuarios.Show()
    End Sub

    Private Sub btnRegistroUsuarios_Click(sender As Object, e As EventArgs) Handles btnRegistroUsuarios.Click
        FrmMantenimientoUsuarios.MdiParent = Me
        FrmMantenimientoUsuarios.Show()
    End Sub

    Private Sub MnuGeneralDeInventario_Click(sender As Object, e As EventArgs) Handles MnuGeneralDeInventario.Click
        FrmReportes.ReporteInventarioPorProducto()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuPorMarcaDeInventario_Click(sender As Object, e As EventArgs) Handles MnuPorMarcaDeInventario.Click
        FrmReportes.ReporteInventarioPorMarca()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuDetalladoVentasPorDia_Click(sender As Object, e As EventArgs) Handles MnuDetalladoVentasPorDia.Click
        FrmReportes.ReporteDetalladoVentasDia()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuLetras_Click(sender As Object, e As EventArgs) Handles MnuLetras.Click
        FrmReportes.ReporteGeneralLetrasPorCobrar()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuMantenimientoActivos_Click(sender As Object, e As EventArgs) Handles MnuMantenimientoActivos.Click
        FrmListarActivos.MdiParent = Me
        FrmListarActivos.Show()
    End Sub

    Private Sub MnuActualizaciónYCuadreDeInventario_Click(sender As Object, e As EventArgs) Handles MnuActualizaciónYCuadreDeInventario.Click
        FrmConsultaInventario.MdiParent = Me
        FrmConsultaInventario.Show()
    End Sub

    Private Sub MnuCancelaciónDeComprobantesPorBloque_Click(sender As Object, e As EventArgs) Handles MnuCancelaciónDeComprobantesPorBloque.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        If ValidaAperturaCaja() = False Then
            Exit Sub
        End If
        FrmRegistroCobroBloque.MdiParent = Me
        FrmRegistroCobroBloque.Show()
    End Sub

    Private Sub MnuStockMinimoPorProducto_Click(sender As Object, e As EventArgs) Handles MnuStockMinimoPorProducto.Click
        FrmReportes.ReporteStockMinimoPorProducto()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuOrdenDeCompraImportacion_Click(sender As Object, e As EventArgs) Handles MnuOrdenDeCompraImportacion.Click
        FrmConsultaOrdenCompraImportacion.MdiParent = Me
        FrmConsultaOrdenCompraImportacion.Show()
    End Sub

    Private Sub btnRegistroCotizacion_Click(sender As Object, e As EventArgs) Handles btnRegistroCotizacion.Click
        FrmConsultaCotizacion.MdiParent = Me
        FrmConsultaCotizacion.Show()
    End Sub

    Private Sub btnRegistroGuias_Click(sender As Object, e As EventArgs) Handles btnRegistroGuias.Click
        FrmConsultaGuia.MdiParent = Me
        FrmConsultaGuia.Show()
    End Sub

    Private Sub MnuConsultaDePagos_Click(sender As Object, e As EventArgs) Handles MnuConsultaDePagos.Click
        FrmReportes.ReportePagosComprobante()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuInventarioConsolidadoPorMarca_Click(sender As Object, e As EventArgs) Handles MnuInventarioConsolidadoPorMarca.Click
        FrmReportes.ReportesInventarioConsolidadoPorMarca()
        FrmReportes = Nothing
    End Sub

    Private Sub MnuComprasServicios_Click(sender As Object, e As EventArgs) Handles MnuComprasServicios.Click
        FrmConsultaComprasServicios.MdiParent = Me
        FrmConsultaComprasServicios.Show()
    End Sub

    Private Sub MnuMovimientosAlmacenServicios_Click(sender As Object, e As EventArgs) Handles MnuMovimientosAlmacenServicios.Click
        FrmConsultaAlmacenServicios.MdiParent = Me
        FrmConsultaAlmacenServicios.Show()
    End Sub

    Private Sub MnuDetalladoVentasPorMarca_Click(sender As Object, e As EventArgs) Handles MnuDetalladoVentasPorMarca.Click
        FrmReportes.ReporteDetalladoVentasPorMarca()
        FrmReportes = Nothing
    End Sub

    Private Sub AperturaCierreContableToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AperturaCierreContable.Click
        If ValidaAperturaDia() = False Then
            Exit Sub
        End If
        FrmAperturaCierreContable.MdiParent = Me
        FrmAperturaCierreContable.Show()
    End Sub

    Private Sub MnuInventarioConsolidadoProductoGerencia_Click(sender As Object, e As EventArgs) Handles MnuInventarioConsolidadoProductoGerencia.Click
        FrmReportes.ReporteInventarioConsolidadoProductoGerencia()
        FrmReportes = Nothing
    End Sub

    Private Sub MDIPrincipal_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown

    End Sub
End Class
