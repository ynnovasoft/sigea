﻿Imports System.Data.SqlClient
Module Recuperadores
    Dim cmd As New SqlCommand
    Dim adaptador As New SqlDataAdapter
    Dim MontoCuota As Double
    Public Sub LlenaAGridView(ByVal oData As DataTable,
                                      ByRef grv As DataGridView,
                                    Optional ByVal bSoloDBpropertyName As Boolean = True)
        grv.DataSource = oData
        If bSoloDBpropertyName Then
            For i = 0 To grv.Columns.Count - 1
                If grv.Columns(i).DataPropertyName = grv.Columns(i).HeaderText Then
                    grv.Columns(i).Visible = False
                End If
            Next
        End If
        oData = Nothing
    End Sub
    Public Sub CargaCombo(ByVal oData As DataTable, ByVal cboLista As ComboBox)
        cboLista.DataSource = oData
        cboLista.DisplayMember = oData.Columns(1).ColumnName
        cboLista.ValueMember = oData.Columns(0).ColumnName
        oData = Nothing
    End Sub
    Public Sub CargaComboGrilla(ByVal oData As DataTable, ByVal cboLista As DataGridViewComboBoxColumn)
        cboLista.DataSource = oData
        cboLista.DisplayMember = oData.Columns(1).ColumnName
        cboLista.ValueMember = oData.Columns(0).ColumnName
        oData = Nothing
    End Sub
    Public Function ObtenerDato(ByVal cNomCampo As String,
    ByRef DTS As DataTable, Optional ByVal nFila As Integer = 0) As String
        Return DTS.Rows(nFila)(cNomCampo)
        DTS = Nothing
    End Function
    Public Function BuscarMenu(ByVal cNomMenu As String, ByRef DTS As DataTable) As Integer
        Dim Valor As Integer = 0
        For i = 0 To DTS.Rows.Count - 1
            If DTS.Rows(i)("cNomMenu") = cNomMenu Then
                Valor = Valor + 1
            End If
        Next
        BuscarMenu = Valor
    End Function
    Sub AsignarDato(ByRef Objeto As DataTable, ByVal nNumFilas As Integer, ByVal cNombColumna As String, ByVal cValor As String)
        Try
            If Objeto.Rows.Count - 1 <> nNumFilas And Objeto.Rows.Count - 1 < nNumFilas Then
                Dim Fila As DataRow = Objeto.NewRow
                Objeto.Rows.Add(Fila)
            End If
            Objeto.Rows(nNumFilas).Item(cNombColumna) = cValor
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
        End Try
    End Sub
    Sub CrearTabla(ByRef Objeto As DataTable, ByVal cNombTabla As String)
        Try
            Dim dt As New DataTable(cNombTabla)
            Objeto = dt
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally

        End Try
    End Sub
    Sub CrearAsignarColumna(ByRef Objeto As DataTable, ByVal cNombColumna As String, ByVal cTipoDato As System.Type, Optional ByVal bAutoincrement As Boolean = False)
        Try
            Dim NameColumn As New DataColumn(cNombColumna)
            NameColumn.DataType = cTipoDato
            NameColumn.AutoIncrement = bAutoincrement
            Objeto.Columns.Add(NameColumn)
            If bAutoincrement = True Then
                Objeto.Constraints.Add("Key1", NameColumn, True)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
        End Try
    End Sub
    Public Sub LimpiaGrilla(ByVal grv As DataGridView)
        Dim i, j As Integer
        i = grv.Rows.Count
        j = grv.Rows.Count - 1
        If grv.Rows.Count > 0 Then
            While i >= grv.Rows.Count - 1 And j >= 0
                If j >= 0 Then
                    grv.Rows.RemoveAt(j)
                End If
                j = j - 1
            End While
        End If
    End Sub
    Public Function SumarColumnaGrilla(ByVal dgt As DataGridView, ByVal cNomColumna As String) As Double
        Dim SubTotal As Double = 0
        For Each Row As DataGridViewRow In dgt.Rows
            SubTotal += Val(Row.Cells(cNomColumna).Value)
        Next
        Return SubTotal
    End Function
    Function CreaDatoCombos(ByVal Dato As DataTable, ByVal Codigo As Integer) As DataTable
        Dim dtr As DataRow()
        Try

            dtr = Dato.Select("Codigo = " & Codigo)
            Dato = Nothing

            Call CrearTabla(Dato, "Combos")
            Call CrearAsignarColumna(Dato, "Valor", GetType(String))
            Call CrearAsignarColumna(Dato, "Descripcion", GetType(String))

            For i As Integer = 0 To dtr.Length - 1
                AsignarDato(Dato, i, "Valor", dtr(i).ItemArray(1))
                AsignarDato(Dato, i, "Descripcion", dtr(i).ItemArray(2))
            Next
            Return Dato
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
End Module
