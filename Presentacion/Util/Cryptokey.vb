﻿Imports System.Security.Cryptography

Public Class Cryptokey
    Public Shared Function DecodeRsaPrivateKey(ByVal privateKeyBytes As Byte()) As RSACryptoServiceProvider
        Dim provider As RSACryptoServiceProvider
        Dim rd As System.IO.BinaryReader = New System.IO.BinaryReader(New System.IO.MemoryStream(privateKeyBytes))

        Try
            Dim num As UShort = rd.ReadUInt16()

            If num = &H8130 Then
                rd.ReadByte()
            ElseIf num = &H8230 Then
                rd.ReadInt16()
            Else
                Return Nothing
            End If

            If rd.ReadUInt16() <> &H102 Then
                provider = Nothing
            ElseIf rd.ReadByte() <> 0 Then
                provider = Nothing
            Else
                Dim parameters1 As CspParameters = New CspParameters()
                parameters1.Flags = CspProviderFlags.NoFlags
                parameters1.KeyContainerName = Guid.NewGuid().ToString().ToUpperInvariant()
                parameters1.ProviderType = If(((Environment.OSVersion.Version.Major > 5) OrElse ((Environment.OSVersion.Version.Major = 5) AndAlso (Environment.OSVersion.Version.Minor >= 1))), &H18, 1)
                Dim parameters As RSAParameters = New RSAParameters With {
                    .Modulus = rd.ReadBytes(Helpers.DecodeIntegerSize(rd))
                }
                Dim traits As RSAParameterTraits = New RSAParameterTraits(parameters.Modulus.Length * 8)
                parameters.Modulus = Helpers.AlignBytes(parameters.Modulus, traits.size_Mod)
                parameters.Exponent = Helpers.AlignBytes(rd.ReadBytes(Helpers.DecodeIntegerSize(rd)), traits.size_Exp)
                parameters.D = Helpers.AlignBytes(rd.ReadBytes(Helpers.DecodeIntegerSize(rd)), traits.size_D)
                parameters.P = Helpers.AlignBytes(rd.ReadBytes(Helpers.DecodeIntegerSize(rd)), traits.size_P)
                parameters.Q = Helpers.AlignBytes(rd.ReadBytes(Helpers.DecodeIntegerSize(rd)), traits.size_Q)
                parameters.DP = Helpers.AlignBytes(rd.ReadBytes(Helpers.DecodeIntegerSize(rd)), traits.size_DP)
                parameters.DQ = Helpers.AlignBytes(rd.ReadBytes(Helpers.DecodeIntegerSize(rd)), traits.size_DQ)
                parameters.InverseQ = Helpers.AlignBytes(rd.ReadBytes(Helpers.DecodeIntegerSize(rd)), traits.size_InvQ)
                Dim provider1 As RSACryptoServiceProvider = New RSACryptoServiceProvider(parameters1)
                provider1.ImportParameters(parameters)
                provider = provider1
            End If
        Catch __unusedException1__ As Exception
            provider = Nothing
        Finally
            rd.Close()
        End Try
        Return provider
    End Function
End Class

