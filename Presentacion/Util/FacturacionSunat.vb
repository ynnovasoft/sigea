﻿
Imports System.Configuration
Imports System.Net
Imports System.Security.Cryptography
Imports System.Security.Cryptography.X509Certificates
Imports System.Security.Cryptography.Xml
Imports System.ServiceModel
Imports System.Text
Imports System.Xml
Imports System
Imports System.IO
Imports DASunat.ModeloEntidades
Imports DASunat.ServicioGuia
Imports DASunat.ServicioComprobantes
Imports Negocios

Public Class FacturacionSunat
    Public Property RutaEmpresa As String
    Public Property UsuarioFEEmpresa As String
    Public Property PasswordFEEmpresa As String
    Public Property RutaFERPTA As String
    Public Property RutaFEXML As String
    Public Property RutaFECERT As String
    Public Property RutaFECERTClave As String
    Public Property RutaFEENVIO As String
    Public Property RutaFECDRS As String
    Public Property RutaFECONSULTA As String
    Public Function EnviarComprobante(ByVal comprobante As Comprobante, ByVal guia As List(Of ListaGuia),
                                      ByVal anticipo As List(Of Anticipo), ByVal cuotas As List(Of Cuotas),
                                      ByVal Codigo As Integer) As String
        Dim Mensaje As String = ""
        Try
            If MDIPrincipal.CodigoEmpresa = 1 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            End If
            RutaFERPTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFERPTA")
            RutaFEXML = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEXML")
            RutaFECERT = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERT")
            RutaFECERTClave = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERTClave")
            RutaFEENVIO = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEENVIO")
            RutaFECDRS = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECDRS")
            RutaFECONSULTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECONSULTA")

            Dim xml As StringBuilder = Nothing
            If comprobante.CodigoTipoComprobante = "01" Then
                xml = CrearXmlFactura(comprobante, guia, anticipo, cuotas)
            ElseIf comprobante.CodigoTipoComprobante = "03" Then
                xml = CrearXmlBoleta(comprobante, guia, cuotas)
            ElseIf comprobante.CodigoTipoComprobante = "07" Then
                xml = CrearXmlNotaCredito(comprobante, cuotas)
            ElseIf comprobante.CodigoTipoComprobante = "08" Then
                xml = CrearXmlNotaDebito(comprobante)
            End If
            Dim DataXml As Byte() = GenerarXml(xml)
            Dim NombreArchivo As String = comprobante.NumeroDocumentoEmisor & "-" & comprobante.CodigoTipoComprobante & "-" & comprobante.SerieNumeroComprobante
            Dim RutaXml As String = RutaFEXML & "/" & NombreArchivo & ".xml"
            Dim RutaZipEnvio As String = RutaFEENVIO & "/" & NombreArchivo & ".zip"
            If IO.File.Exists(RutaXml) Then IO.File.Delete(RutaXml)
            SaveData(DataXml, RutaXml)
            FirmarXml(RutaXml)
            If IO.File.Exists(RutaZipEnvio) Then IO.File.Delete(RutaZipEnvio)
            Comprimir(RutaXml, RutaZipEnvio)
            Try
                Dim responsecode As Integer = 0
                Dim description As String = ""
                Mensaje = EnviarDocumentoSunat(NombreArchivo, responsecode, description)
                Dim CodigoHas As String = ""
                If Mensaje = "" Then
                    CodigoHas = ObtenerDigestValueSunat(RutaFERPTA & "/R-" & NombreArchivo & ".xml")
                    Dim obj As New BLFacturacion
                    obj.ActualizarRespuestaSunat(Codigo, responsecode, description, "", CodigoHas, 1, Mensaje)
                End If
            Catch exception As FaultException
                Mensaje = "Presentacion-Util-FacturacionSunat MetodoEjecutado: EnviarComprobante - EnviarDocumentoSunat " + exception.Message
            End Try
        Catch ex As Exception
            Mensaje = "Presentacion-Util-FacturacionSunat MetodoEjecutado: EnviarComprobante - " + ex.Message
        End Try
        Return Mensaje
    End Function
    Public Function EnviarComunicacionBajaComprobante(ByVal Codigo As Integer, ByVal numerocomprobante As String,
                                                      ByVal comprobante As ComprobanteBaja, ByRef CodigoRespuesta As Integer,
                                                      ByVal TipoComprobante As String) As String
        Dim Mensaje As String = ""
        Try
            If MDIPrincipal.CodigoEmpresa = 1 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            End If
            RutaFERPTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFERPTA")
            RutaFEXML = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEXML")
            RutaFECERT = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERT")
            RutaFECERTClave = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERTClave")
            RutaFEENVIO = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEENVIO")
            RutaFECDRS = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECDRS")
            RutaFECONSULTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECONSULTA")

            Dim xml As StringBuilder = Nothing
            If TipoComprobante = "01" Or TipoComprobante = "07" Or TipoComprobante = "08" Then
                xml = CrearXmlComunicacionBaja_FT_NC_ND(comprobante)
            ElseIf TipoComprobante = "03" Then
                xml = CrearXmlComunicacionBaja_BV(comprobante)
            End If
            Dim docxml As String = comprobante.NumDocEmisor & "-" & comprobante.IdentResumen & ".xml"
            Dim doczip As String = comprobante.NumDocEmisor & "-" & comprobante.IdentResumen & ".zip"
            Dim data() As Byte
            data = GenerarXml(xml)

            Dim rutaxml As String = RutaFEXML
            Dim rutazip As String = RutaFEENVIO

            Dim rutaarchivo As String = rutaxml & "/" & docxml
            doczip = rutazip & "/" & doczip
            If System.IO.File.Exists(rutaarchivo) Then
                System.IO.File.Delete(rutaarchivo)
            End If

            Call SaveData(data, rutaarchivo)

            Call FirmarXml(rutaarchivo)
            If System.IO.File.Exists(doczip) Then
                System.IO.File.Delete(doczip)
            End If
            Call Comprimir(rutaarchivo, doczip)
            Try
                Dim contentFile As Byte() = System.IO.File.ReadAllBytes(doczip)
                If MDIPrincipal.CodigoEmpresa = 1 Then
                    UsuarioFEEmpresa = ConfigurationManager.AppSettings("UsuarioFEEmpresa")
                    PasswordFEEmpresa = ConfigurationManager.AppSettings("PasswordFEEmpresa")
                ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                    UsuarioFEEmpresa = ConfigurationManager.AppSettings("UsuarioFEEmpresa2")
                    PasswordFEEmpresa = ConfigurationManager.AppSettings("PasswordFEEmpresa2")
                End If
                Dim oservicio As servicioSunat.billServiceClient
                oservicio = New servicioSunat.billServiceClient("BillServicePort")
                ServicePointManager.UseNagleAlgorithm = True
                ServicePointManager.Expect100Continue = False
                ServicePointManager.CheckCertificateRevocationList = True
                Dim behavior = New PasswordDigestBehavior(UsuarioFEEmpresa, PasswordFEEmpresa)
                oservicio.Endpoint.EndpointBehaviors.Add(behavior)

                oservicio.Open()
                Dim numeroticket As String = oservicio.sendSummary(System.IO.Path.GetFileName(doczip), contentFile, "")
                oservicio.Close()

                Dim NombreXml As String = "R-" & docxml

                Dim obj As New BLFacturacion
                obj.RegistrarTicketRespuestaBajaSunat(Codigo, numeroticket, NombreXml, 1, Mensaje)

                Mensaje = ActualizarEstadoSunatComprobante(Codigo, numeroticket, NombreXml, CodigoRespuesta)
            Catch exception As FaultException
                Mensaje = exception.Message
            End Try
        Catch exception As Exception
            Mensaje = exception.Message
        End Try
        Return Mensaje
    End Function
    Private Function CrearXmlFactura(ByVal factura As Comprobante, ByVal guia As List(Of ListaGuia), ByVal anticipo As List(Of Anticipo),
                                     ByVal cuotas As List(Of Cuotas)) As StringBuilder
        Dim builder As StringBuilder = New StringBuilder()
        Dim builder2 As StringBuilder = New StringBuilder()
        builder2.Append("<?xml version=""1.0"" encoding =""UTF-8""?>")
        builder2.Append("<Invoice xmlns=""urn:oasis:names:specification:ubl:schema:xsd:Invoice-2""")
        builder2.Append(" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2""")
        builder2.Append(" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2""")
        builder2.Append(" xmlns:ccts=""urn:un:unece:uncefact:documentation:2""")
        builder2.Append(" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#""")
        builder2.Append(" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2""")
        builder2.Append(" xmlns:qdt=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2""")
        builder2.Append(" xmlns:udt=""urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2""")
        builder2.Append(" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">")
        builder2.Append("<ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent/></ext:UBLExtension></ext:UBLExtensions>")
        builder2.Append("<cbc:UBLVersionID>2.1</cbc:UBLVersionID>")
        builder2.Append("<cbc:CustomizationID>2.0</cbc:CustomizationID>")
        builder2.Append("<cbc:ID>" & factura.SerieNumeroComprobante & "</cbc:ID>")
        builder2.Append("<cbc:IssueDate>" & factura.FechaEmision & "</cbc:IssueDate>")
        builder2.Append("<cbc:IssueTime>" & factura.HoraEmision & "</cbc:IssueTime>")
        builder2.Append("<cbc:DueDate>" & factura.FechaVencimiento & "</cbc:DueDate>")
        builder2.Append("<cbc:InvoiceTypeCode listID=""" & factura.CodigoTipoOperacion & """")
        builder2.Append(" listAgencyName=""PE:SUNAT"" ")
        builder2.Append(" listName=""Tipo de Documento"" ")
        builder2.Append(" listURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01"">" & factura.CodigoTipoComprobante & "</cbc:InvoiceTypeCode>")
        builder2.Append(" <cbc:Note><![CDATA[" & factura.MontoEnLetras & "]]></cbc:Note>")
        builder2.Append(" <cbc:DocumentCurrencyCode listID=""ISO 4217 Alpha"" listName=""Currency"" listAgencyName=""United Nations Economic Commission for Europe"">")
        builder2.Append(factura.CodigoTipoMoneda & "</cbc:DocumentCurrencyCode>")
        builder2.Append("<cbc:LineCountNumeric>" & factura.Detalle.Count.ToString() & "</cbc:LineCountNumeric>")
        If factura.NumeroOrdenCompra <> "" Then
            builder2.Append("<cac:OrderReference><cbc:ID>" & factura.NumeroOrdenCompra & "</cbc:ID></cac:OrderReference>")
        End If
        For Each objguia As ListaGuia In guia
            builder2.Append("<cac:DespatchDocumentReference>")
            builder2.Append("<cbc:ID>" & objguia.NumeroGuiaRemision & "</cbc:ID>")
            builder2.Append("<cbc:DocumentTypeCode>" & objguia.CodigoGuiaRemision & "</cbc:DocumentTypeCode>")
            builder2.Append("</cac:DespatchDocumentReference>")
        Next
        For Each objanticipo As Anticipo In anticipo
            builder2.Append("<cac:AdditionalDocumentReference>")
            builder2.Append("<cbc:ID>" & objanticipo.NumeroComprobanteAnticipo & "</cbc:ID>")
            builder2.Append("<cbc:DocumentTypeCode>02</cbc:DocumentTypeCode>")
            builder2.Append("<cbc:DocumentType>ANTICIPO</cbc:DocumentType>")
            builder2.Append("<cbc:DocumentStatusCode>1</cbc:DocumentStatusCode>")
            builder2.Append("<cac:IssuerParty>")
            builder2.Append("<cac:PartyIdentification>")
            Dim InstructionID As String = String.Concat("<cbc:ID schemeID=""", factura.TipoDocumentoAdquirente, """>", factura.DocumentoAdquirente, "</cbc:ID>")
            builder2.Append(InstructionID)
            builder2.Append("</cac:PartyIdentification>")
            builder2.Append("</cac:IssuerParty>")
            builder2.Append("</cac:AdditionalDocumentReference>")
        Next
        builder2.Append("<cac:Signature>")
        builder2.Append("<cbc:ID>" & factura.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("<cac:SignatoryParty>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID>" & factura.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name><![CDATA[" & factura.NombreComercialEmisor & "]]></cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("</cac:SignatoryParty>")
        builder2.Append("<cac:DigitalSignatureAttachment>")
        builder2.Append("<cac:ExternalReference>")
        builder2.Append("<cbc:URI>SignatureSP</cbc:URI>")
        builder2.Append("</cac:ExternalReference>")
        builder2.Append("</cac:DigitalSignatureAttachment>")
        builder2.Append("</cac:Signature>")
        builder2.Append("<cac:AccountingSupplierParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID schemeID=""" & factura.TipoDocumentoEmisor & """ ")
        builder2.Append("schemeName=""Documento de Identidad"" ")
        builder2.Append("schemeAgencyName=""PE:SUNAT"" ")
        builder2.Append("schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">" & factura.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name><![CDATA[" & factura.NombreComercialEmisor & "]]></cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & factura.RazonSocialEmisor & "]]></cbc:RegistrationName>")
        builder2.Append("<cac:RegistrationAddress>")
        builder2.Append("<cbc:AddressTypeCode>" & factura.CodigoFiscalEmisor & "</cbc:AddressTypeCode>")
        builder2.Append("<cac:AddressLine>")
        builder2.Append("<cbc:Line><![CDATA[" & factura.DireccionEmisor & "]]></cbc:Line>")
        builder2.Append("</cac:AddressLine>")
        builder2.Append("</cac:RegistrationAddress>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:AccountingSupplierParty>")
        builder2.Append("<cac:AccountingCustomerParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID schemeID=""" & factura.TipoDocumentoAdquirente & """ ")
        builder2.Append("schemeName=""Documento de Identidad"" ")
        builder2.Append("schemeAgencyName=""PE:SUNAT"" ")
        builder2.Append("schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">" & factura.DocumentoAdquirente & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & factura.RazonSocialAdquirente & "]]></cbc:RegistrationName>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:AccountingCustomerParty>")
        If factura.CondicionPago = 1 Then
            builder2.Append("<cac:PaymentTerms>")
            builder2.Append("<cbc:ID>FormaPago</cbc:ID>")
            builder2.Append("<cbc:PaymentMeansID>Contado</cbc:PaymentMeansID>")
            builder2.Append("</cac:PaymentTerms>")
        Else
            builder2.Append("<cac:PaymentTerms>")
            builder2.Append("<cbc:ID>FormaPago</cbc:ID>")
            builder2.Append("<cbc:PaymentMeansID>Credito</cbc:PaymentMeansID>")
            Dim linAmount As String = String.Concat("<cbc:Amount currencyID=""", factura.CodigoTipoMoneda, """>", factura.ImporteNetoPago.ToString("#0.00"), "</cbc:Amount>")
            builder2.Append(linAmount)
            builder2.Append("</cac:PaymentTerms>")
            For Each objcuotas As Cuotas In cuotas
                builder2.Append("<cac:PaymentTerms>")
                builder2.Append("<cbc:ID>FormaPago</cbc:ID>")
                builder2.Append("<cbc:PaymentMeansID>" & objcuotas.CodigoCuota & "</cbc:PaymentMeansID>")
                Dim linAmount1 As String = String.Concat("<cbc:Amount currencyID=""", factura.CodigoTipoMoneda, """>", objcuotas.MontoCuota.ToString("#0.00"), "</cbc:Amount>")
                builder2.Append(linAmount1)
                builder2.Append("<cbc:PaymentDueDate>" & objcuotas.FechaVencimiento & "</cbc:PaymentDueDate>")
                builder2.Append("</cac:PaymentTerms>")
            Next
        End If
        For Each objanticipo As Anticipo In anticipo
            builder2.Append("<cac:PrepaidPayment>")
            builder2.Append("<cbc:ID>1</cbc:ID>")
            Dim PaidAmount As String = String.Concat("<cbc:PaidAmount currencyID=""", objanticipo.CodigoTipoMoneda, """>", objanticipo.ImporteTotalAnticipo.ToString("#0.00"), "</cbc:PaidAmount>")
            builder2.Append(PaidAmount)
            builder2.Append("</cac:PrepaidPayment>")
        Next
        If factura.TotalAnticipos <> "0.00" Then
            builder2.Append("<cac:AllowanceCharge>")
            builder2.Append("<cbc:ChargeIndicator>false</cbc:ChargeIndicator>")
            builder2.Append("<cbc:AllowanceChargeReasonCode>04</cbc:AllowanceChargeReasonCode>")
            builder2.Append("<cbc:MultiplierFactorNumeric>1</cbc:MultiplierFactorNumeric>")
            Dim Amount As String = String.Concat("<cbc:Amount currencyID=""", factura.CodigoTipoMoneda, """>", factura.TotalAnticipos.ToString("#0.00"), "</cbc:Amount>")
            builder2.Append(Amount)
            Dim BaseAmount As String = String.Concat("<cbc:BaseAmount currencyID=""", factura.CodigoTipoMoneda, """>", factura.TotalAnticipos.ToString("#0.00"), "</cbc:BaseAmount>")
            builder2.Append(BaseAmount)
            builder2.Append("</cac:AllowanceCharge>")
        End If
        builder2.Append("<cac:TaxTotal>")
        Dim taxtot_1 As String = String.Concat("<cbc:TaxAmount currencyID=""", factura.CodigoTipoMoneda, """>", factura.MontoTotalImpuestos.ToString("#0.00"), "</cbc:TaxAmount>")
        builder2.Append(taxtot_1)
        builder2.Append("<cac:TaxSubtotal>")
        Dim taxsub_1 As String = String.Concat("<cbc:TaxableAmount currencyID=""", factura.CodigoTipoMoneda, """>", factura.TotalOperacionesGravadas.ToString("#0.00"), "</cbc:TaxableAmount>")
        builder2.Append(taxsub_1)
        Dim taxsub_2 As String = String.Concat("<cbc:TaxAmount currencyID=""", factura.CodigoTipoMoneda, """>", factura.SumatoriaIGV.ToString("#0.00"), "</cbc:TaxAmount>")
        builder2.Append(taxsub_2)
        builder2.Append("<cac:TaxCategory>")
        builder2.Append("<cac:TaxScheme>")
        builder2.Append("<cbc:ID>" & factura.TipoTributo & "</cbc:ID>")
        builder2.Append("<cbc:Name>" & factura.NombreTributo & "</cbc:Name>")
        builder2.Append("<cbc:TaxTypeCode>" & factura.TipoCodigoTributo & "</cbc:TaxTypeCode>")
        builder2.Append("</cac:TaxScheme>")
        builder2.Append("</cac:TaxCategory>")
        builder2.Append("</cac:TaxSubtotal>")
        builder2.Append("</cac:TaxTotal>")
        builder2.Append("<cac:LegalMonetaryTotal>")
        Dim linea1 As String = String.Concat("<cbc:LineExtensionAmount currencyID=""", factura.CodigoTipoMoneda, """>", factura.TotalValorVenta.ToString("#0.00"), "</cbc:LineExtensionAmount>")
        builder2.Append(linea1)
        Dim linea2 As String = String.Concat("<cbc:TaxInclusiveAmount currencyID=""", factura.CodigoTipoMoneda, """>", factura.TotalPrecioVenta.ToString("#0.00"), "</cbc:TaxInclusiveAmount>")
        builder2.Append(linea2)
        If factura.TotalAnticipos <> "0.00" Then
            Dim anticipo1 As String = String.Concat("<cbc:PrepaidAmount currencyID=""", factura.CodigoTipoMoneda, """>", factura.TotalAnticipos.ToString("#0.00"), "</cbc:PrepaidAmount>")
            builder2.Append(anticipo1)
        End If
        Dim linea3 As String = String.Concat("<cbc:PayableAmount currencyID=""", factura.CodigoTipoMoneda, """>", factura.ImporteTotalComprobante.ToString("#0.00"), "</cbc:PayableAmount>")
        builder2.Append(linea3)
        builder2.Append("</cac:LegalMonetaryTotal>")
        Dim lineadetalle As String = ""
        For Each detalle As Detalle In factura.Detalle
            builder2.Append("<cac:InvoiceLine>")
            builder2.Append("<cbc:ID>" & detalle.NumeroItem & "</cbc:ID>")
            lineadetalle = String.Concat("<cbc:InvoicedQuantity unitCode=""", detalle.UnidadMedida, """>", detalle.Cantidad.ToString("#0.00"), "</cbc:InvoicedQuantity>")
            builder2.Append(lineadetalle)
            lineadetalle = String.Concat("<cbc:LineExtensionAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.ValorVentaItem.ToString("#0.00"), "</cbc:LineExtensionAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:PricingReference>")
            builder2.Append("<cac:AlternativeConditionPrice>")
            lineadetalle = String.Concat("<cbc:PriceAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.PrecioUnitario.ToString("#0.000000"), "</cbc:PriceAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cbc:PriceTypeCode>" & detalle.CodigoTipoPrecio & "</cbc:PriceTypeCode>")
            builder2.Append("</cac:AlternativeConditionPrice>")
            builder2.Append("</cac:PricingReference>")
            If detalle.MontoDescuento <> "0.00" Then
                builder2.Append("<cac:AllowanceCharge>")
                builder2.Append("<cbc:ChargeIndicator>" & detalle.IndicadorDescuento & "</cbc:ChargeIndicator>")
                builder2.Append("<cbc:AllowanceChargeReasonCode>" & detalle.CodigoDescuento & "</cbc:AllowanceChargeReasonCode>")
                builder2.Append("<cbc:MultiplierFactorNumeric>" & detalle.FactorPorcDescuento & "</cbc:MultiplierFactorNumeric>")
                lineadetalle = String.Concat("<cbc:Amount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoDescuento.ToString("#0.00"), "</cbc:Amount>")
                builder2.Append(lineadetalle)
                lineadetalle = String.Concat("<cbc:BaseAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.BaseImponibleDescuento.ToString("#0.00"), "</cbc:BaseAmount>")
                builder2.Append(lineadetalle)
                builder2.Append("</cac:AllowanceCharge>")
            End If
            builder2.Append("<cac:TaxTotal>")
            lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoTributoItem.ToString("#0.00"), "</cbc:TaxAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:TaxSubtotal>")
            lineadetalle = String.Concat("<cbc:TaxableAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoOperacion.ToString("#0.00"), "</cbc:TaxableAmount>")
            builder2.Append(lineadetalle)
            lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoTributoItem.ToString("#0.00"), "</cbc:TaxAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:TaxCategory>")
            builder2.Append("<cbc:Percent>" & detalle.PorcentajeImpuestos.ToString("#0.00") & "</cbc:Percent>")
            builder2.Append("<cbc:TaxExemptionReasonCode listName=""Afectacion del IGV"" listAgencyName=""PE:SUNAT"" ")
            builder2.Append(" listURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"">")
            builder2.Append(detalle.CodigoAfectacionIgv)
            builder2.Append("</cbc:TaxExemptionReasonCode>")
            builder2.Append("<cac:TaxScheme>")
            builder2.Append("<cbc:ID>" & detalle.CategoriaImpuestos & "</cbc:ID>")
            builder2.Append("<cbc:Name>" & detalle.NombreTributo & "</cbc:Name>")
            builder2.Append("<cbc:TaxTypeCode>" & detalle.CodigoTributo & "</cbc:TaxTypeCode>")
            builder2.Append("</cac:TaxScheme>")
            builder2.Append("</cac:TaxCategory>")
            builder2.Append("</cac:TaxSubtotal>")
            builder2.Append("</cac:TaxTotal>")
            builder2.Append("<cac:Item>")
            builder2.Append("<cbc:Description><![CDATA[" & detalle.DescripcionProducto & "]]></cbc:Description>")
            builder2.Append("<cac:SellersItemIdentification><cbc:ID>" & detalle.CodigoProducto & "</cbc:ID></cac:SellersItemIdentification>")
            builder2.Append("</cac:Item>")
            builder2.Append("<cac:Price>")
            lineadetalle = String.Concat("<cbc:PriceAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.ValorUnitarioxItem.ToString("#0.000000"), "</cbc:PriceAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("</cac:Price>")
            builder2.Append("</cac:InvoiceLine>")
        Next
        builder2.Append("</Invoice>")
        builder.AppendLine(builder2.ToString())
        Return builder
    End Function
    Private Function CrearXmlBoleta(ByVal boleta As Comprobante, ByVal guia As List(Of ListaGuia), ByVal cuotas As List(Of Cuotas)) As StringBuilder
        Dim builder As StringBuilder = New StringBuilder()
        Dim builder2 As StringBuilder = New StringBuilder()
        builder2.Append("<?xml version=""1.0"" encoding =""UTF-8""?>")
        builder2.Append("<Invoice xmlns=""urn:oasis:names:specification:ubl:schema:xsd:Invoice-2""")
        builder2.Append(" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2""")
        builder2.Append(" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2""")
        builder2.Append(" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#""")
        builder2.Append(" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"">")
        builder2.Append("<ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent/></ext:UBLExtension></ext:UBLExtensions>")
        builder2.Append("<cbc:UBLVersionID>2.1</cbc:UBLVersionID>")
        builder2.Append("<cbc:CustomizationID>2.0</cbc:CustomizationID>")
        builder2.Append("<cbc:ID>" & boleta.SerieNumeroComprobante & "</cbc:ID>")
        builder2.Append("<cbc:IssueDate>" & boleta.FechaEmision & "</cbc:IssueDate>")
        builder2.Append("<cbc:IssueTime>" & boleta.HoraEmision & "</cbc:IssueTime>")
        builder2.Append("<cbc:DueDate>" & boleta.FechaVencimiento & "</cbc:DueDate>")
        builder2.Append("<cbc:InvoiceTypeCode listID=""" & boleta.CodigoTipoOperacion & """")
        builder2.Append(" listAgencyName=""PE:SUNAT"" ")
        builder2.Append(" listName=""Tipo de Documento"" ")
        builder2.Append(" listURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01"">" & boleta.CodigoTipoComprobante & "</cbc:InvoiceTypeCode>")
        builder2.Append(" <cbc:Note><![CDATA[" & boleta.MontoEnLetras & "]]></cbc:Note>")
        builder2.Append(" <cbc:DocumentCurrencyCode listID=""ISO 4217 Alpha"" listName=""Currency"" listAgencyName=""United Nations Economic Commission for Europe"">")
        builder2.Append(boleta.CodigoTipoMoneda & "</cbc:DocumentCurrencyCode>")
        If boleta.NumeroOrdenCompra <> "" Then
            builder2.Append("<cac:OrderReference><cbc:ID>" & boleta.NumeroOrdenCompra & "</cbc:ID></cac:OrderReference>")
        End If
        For Each objguia As ListaGuia In guia
            builder2.Append("<cac:DespatchDocumentReference>")
            builder2.Append("<cbc:ID>" & objguia.NumeroGuiaRemision & "</cbc:ID>")
            builder2.Append("<cbc:DocumentTypeCode>" & objguia.CodigoGuiaRemision & "</cbc:DocumentTypeCode>")
            builder2.Append("</cac:DespatchDocumentReference>")
        Next
        builder2.Append("<cac:Signature>")
        builder2.Append("<cbc:ID>IDSignKG</cbc:ID>")
        builder2.Append("<cac:SignatoryParty>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID>" & boleta.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name><![CDATA[" & boleta.RazonSocialEmisor & "]]></cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("</cac:SignatoryParty>")
        builder2.Append("<cac:DigitalSignatureAttachment>")
        builder2.Append("<cac:ExternalReference>")
        builder2.Append("<cbc:URI>SignatureSP</cbc:URI>")
        builder2.Append("</cac:ExternalReference>")
        builder2.Append("</cac:DigitalSignatureAttachment>")
        builder2.Append("</cac:Signature>")
        builder2.Append("<cac:AccountingSupplierParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID schemeID=""" & boleta.TipoDocumentoEmisor & """ ")
        builder2.Append("schemeName=""Documento de Identidad"" ")
        builder2.Append("schemeAgencyName=""PE:SUNAT"" ")
        builder2.Append("schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">" & boleta.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name><![CDATA[" & boleta.NombreComercialEmisor & "]]></cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & boleta.RazonSocialEmisor & "]]></cbc:RegistrationName>")
        builder2.Append("<cac:RegistrationAddress>")
        builder2.Append("<cbc:AddressTypeCode>" & boleta.CodigoFiscalEmisor & "</cbc:AddressTypeCode>")
        builder2.Append("<cac:AddressLine>")
        builder2.Append("<cbc:Line><![CDATA[" & boleta.DireccionEmisor & "]]></cbc:Line>")
        builder2.Append("</cac:AddressLine>")
        builder2.Append("</cac:RegistrationAddress>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:AccountingSupplierParty>")
        builder2.Append("<cac:AccountingCustomerParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID schemeID=""" & boleta.TipoDocumentoAdquirente & """ ")
        builder2.Append("schemeName=""Documento de Identidad"" ")
        builder2.Append("schemeAgencyName=""PE:SUNAT"" ")
        builder2.Append("schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">" & boleta.DocumentoAdquirente & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & boleta.RazonSocialAdquirente & "]]></cbc:RegistrationName>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:AccountingCustomerParty>")
        If boleta.CondicionPago = 1 Then
            builder2.Append("<cac:PaymentTerms>")
            builder2.Append("<cbc:ID>FormaPago</cbc:ID>")
            builder2.Append("<cbc:PaymentMeansID>Contado</cbc:PaymentMeansID>")
            builder2.Append("</cac:PaymentTerms>")
        Else
            builder2.Append("<cac:PaymentTerms>")
            builder2.Append("<cbc:ID>FormaPago</cbc:ID>")
            builder2.Append("<cbc:PaymentMeansID>Credito</cbc:PaymentMeansID>")
            Dim linAmount As String = String.Concat("<cbc:Amount currencyID=""", boleta.CodigoTipoMoneda, """>", boleta.ImporteNetoPago.ToString("#0.00"), "</cbc:Amount>")
            builder2.Append(linAmount)
            builder2.Append("</cac:PaymentTerms>")
            For Each objcuotas As Cuotas In cuotas
                builder2.Append("<cac:PaymentTerms>")
                builder2.Append("<cbc:ID>FormaPago</cbc:ID>")
                builder2.Append("<cbc:PaymentMeansID>" & objcuotas.CodigoCuota & "</cbc:PaymentMeansID>")
                Dim linAmount1 As String = String.Concat("<cbc:Amount currencyID=""", boleta.CodigoTipoMoneda, """>", objcuotas.MontoCuota.ToString("#0.00"), "</cbc:Amount>")
                builder2.Append(linAmount1)
                builder2.Append("<cbc:PaymentDueDate>" & objcuotas.FechaVencimiento & "</cbc:PaymentDueDate>")
                builder2.Append("</cac:PaymentTerms>")
            Next
        End If
        builder2.Append("<cac:TaxTotal>")
        Dim taxtot_1 As String = String.Concat("<cbc:TaxAmount currencyID=""", boleta.CodigoTipoMoneda, """>", boleta.MontoTotalImpuestos.ToString("#0.00"), "</cbc:TaxAmount>")
        builder2.Append(taxtot_1)
        builder2.Append("<cac:TaxSubtotal>")
        Dim taxsub_1 As String = String.Concat("<cbc:TaxableAmount currencyID=""", boleta.CodigoTipoMoneda, """>", boleta.TotalOperacionesGravadas.ToString("#0.00"), "</cbc:TaxableAmount>")
        builder2.Append(taxsub_1)
        Dim taxsub_2 As String = String.Concat("<cbc:TaxAmount currencyID=""", boleta.CodigoTipoMoneda, """>", boleta.SumatoriaIGV.ToString("#0.00"), "</cbc:TaxAmount>")
        builder2.Append(taxsub_2)
        builder2.Append("<cac:TaxCategory>")
        builder2.Append("<cac:TaxScheme>")
        builder2.Append("<cbc:ID>" & boleta.TipoTributo & "</cbc:ID>")
        builder2.Append("<cbc:Name>" & boleta.NombreTributo & "</cbc:Name>")
        builder2.Append("<cbc:TaxTypeCode>" & boleta.TipoCodigoTributo & "</cbc:TaxTypeCode>")
        builder2.Append("</cac:TaxScheme>")
        builder2.Append("</cac:TaxCategory>")
        builder2.Append("</cac:TaxSubtotal>")
        builder2.Append("</cac:TaxTotal>")
        builder2.Append("<cac:LegalMonetaryTotal>")
        Dim linea1 As String = String.Concat("<cbc:LineExtensionAmount currencyID=""", boleta.CodigoTipoMoneda, """>", boleta.TotalValorVenta.ToString("#0.00"), "</cbc:LineExtensionAmount>")
        builder2.Append(linea1)
        Dim linea2 As String = String.Concat("<cbc:TaxInclusiveAmount currencyID=""", boleta.CodigoTipoMoneda, """>", boleta.TotalPrecioVenta.ToString("#0.00"), "</cbc:TaxInclusiveAmount>")
        builder2.Append(linea2)
        Dim linea3 As String = String.Concat("<cbc:PayableAmount currencyID=""", boleta.CodigoTipoMoneda, """>", boleta.ImporteTotalComprobante.ToString("#0.00"), "</cbc:PayableAmount>")
        builder2.Append(linea3)
        builder2.Append("</cac:LegalMonetaryTotal>")
        Dim lineadetalle As String = ""
        For Each detalle As Detalle In boleta.Detalle
            builder2.Append("<cac:InvoiceLine>")
            builder2.Append("<cbc:ID>" & detalle.NumeroItem & "</cbc:ID>")
            lineadetalle = String.Concat("<cbc:InvoicedQuantity unitCode=""", detalle.UnidadMedida, """>", detalle.Cantidad.ToString("#0"), "</cbc:InvoicedQuantity>")
            builder2.Append(lineadetalle)
            lineadetalle = String.Concat("<cbc:LineExtensionAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.ValorVentaItem.ToString("#0.00"), "</cbc:LineExtensionAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:PricingReference>")
            builder2.Append("<cac:AlternativeConditionPrice>")
            lineadetalle = String.Concat("<cbc:PriceAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.PrecioUnitario.ToString("#0.000000"), "</cbc:PriceAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cbc:PriceTypeCode>" & detalle.CodigoTipoPrecio & "</cbc:PriceTypeCode>")
            builder2.Append("</cac:AlternativeConditionPrice>")
            builder2.Append("</cac:PricingReference>")
            If detalle.MontoDescuento <> "0.00" Then
                builder2.Append("<cac:AllowanceCharge>")
                builder2.Append("<cbc:ChargeIndicator>" & detalle.IndicadorDescuento & "</cbc:ChargeIndicator>")
                builder2.Append("<cbc:AllowanceChargeReasonCode>" & detalle.CodigoDescuento & "</cbc:AllowanceChargeReasonCode>")
                builder2.Append("<cbc:MultiplierFactorNumeric>" & detalle.FactorPorcDescuento & "</cbc:MultiplierFactorNumeric>")
                lineadetalle = String.Concat("<cbc:Amount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoDescuento.ToString("#0.00"), "</cbc:Amount>")
                builder2.Append(lineadetalle)
                lineadetalle = String.Concat("<cbc:BaseAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.BaseImponibleDescuento.ToString("#0.00"), "</cbc:BaseAmount>")
                builder2.Append(lineadetalle)
                builder2.Append("</cac:AllowanceCharge>")
            End If
            builder2.Append("<cac:TaxTotal>")
            lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoTributoItem.ToString("#0.00"), "</cbc:TaxAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:TaxSubtotal>")
            lineadetalle = String.Concat("<cbc:TaxableAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoOperacion.ToString("#0.00"), "</cbc:TaxableAmount>")
            builder2.Append(lineadetalle)
            lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoTributoItem.ToString("#0.00"), "</cbc:TaxAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:TaxCategory>")
            builder2.Append("<cbc:Percent>" & detalle.PorcentajeImpuestos.ToString("#0.00") & "</cbc:Percent>")
            builder2.Append("<cbc:TaxExemptionReasonCode listName=""Afectacion del IGV"" listAgencyName=""PE:SUNAT"" ")
            builder2.Append(" listURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"">")
            builder2.Append(detalle.CodigoAfectacionIgv)
            builder2.Append("</cbc:TaxExemptionReasonCode>")
            builder2.Append("<cac:TaxScheme>")
            builder2.Append("<cbc:ID>" & detalle.CategoriaImpuestos & "</cbc:ID>")
            builder2.Append("<cbc:Name>" & detalle.NombreTributo & "</cbc:Name>")
            builder2.Append("<cbc:TaxTypeCode>" & detalle.CodigoTributo & "</cbc:TaxTypeCode>")
            builder2.Append("</cac:TaxScheme>")
            builder2.Append("</cac:TaxCategory>")
            builder2.Append("</cac:TaxSubtotal>")
            builder2.Append("</cac:TaxTotal>")
            builder2.Append("<cac:Item>")
            builder2.Append("<cbc:Description><![CDATA[" & detalle.DescripcionProducto & "]]></cbc:Description>")
            builder2.Append("<cac:SellersItemIdentification><cbc:ID>" & detalle.CodigoProducto & "</cbc:ID></cac:SellersItemIdentification>")
            builder2.Append("</cac:Item>")
            builder2.Append("<cac:Price>")
            lineadetalle = String.Concat("<cbc:PriceAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.ValorUnitarioxItem.ToString("#0.000000"), "</cbc:PriceAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("</cac:Price>")
            builder2.Append("</cac:InvoiceLine>")
        Next
        builder2.Append("</Invoice>")
        builder.AppendLine(builder2.ToString())
        Return builder
    End Function
    Private Function CrearXmlNotaCredito(ByVal notas As Comprobante, ByVal cuotas As List(Of Cuotas)) As StringBuilder
        Dim builder As StringBuilder = New StringBuilder()
        Dim builder2 As StringBuilder = New StringBuilder()
        builder2.Append("<?xml version=""1.0"" encoding =""UTF-8""?>")
        builder2.Append("<CreditNote xmlns=""urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2""")
        builder2.Append(" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2""")
        builder2.Append(" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2""")
        builder2.Append(" xmlns:ccts=""urn:un:unece:uncefact:documentation:2""")
        builder2.Append(" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#""")
        builder2.Append(" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2""")
        builder2.Append(" xmlns:qdt=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2""")
        builder2.Append(" xmlns:udt=""urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2""")
        builder2.Append(" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">")
        builder2.Append("<ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent/></ext:UBLExtension></ext:UBLExtensions>")
        builder2.Append("<cbc:UBLVersionID>2.1</cbc:UBLVersionID>")
        builder2.Append("<cbc:CustomizationID>2.0</cbc:CustomizationID>")
        builder2.Append("<cbc:ID>" & notas.SerieNumeroComprobante & "</cbc:ID>")
        builder2.Append("<cbc:IssueDate>" & notas.FechaEmision & "</cbc:IssueDate>")
        builder2.Append("<cbc:IssueTime>" & notas.HoraEmision & "</cbc:IssueTime>")
        builder2.Append("<cbc:DocumentCurrencyCode>" & notas.CodigoTipoMoneda & "</cbc:DocumentCurrencyCode>")
        builder2.Append("<cac:DiscrepancyResponse>")
        builder2.Append("<cbc:ReferenceID>" & notas.SerieNumeroDocAfectado & "</cbc:ReferenceID>")
        builder2.Append("<cbc:ResponseCode>" & notas.CodigoTipoNota & "</cbc:ResponseCode>")
        builder2.Append("<cbc:Description><![CDATA[" & notas.MotivoNota & "]]></cbc:Description>")
        builder2.Append("</cac:DiscrepancyResponse>")
        builder2.Append("<cac:BillingReference>")
        builder2.Append("<cac:InvoiceDocumentReference>")
        builder2.Append("<cbc:ID>" & notas.SerieNumeroDocAfectado & "</cbc:ID>")
        builder2.Append("<cbc:DocumentTypeCode>" & notas.TipoDocumentoAfectado & "</cbc:DocumentTypeCode>")
        builder2.Append("</cac:InvoiceDocumentReference>")
        builder2.Append("</cac:BillingReference>")
        builder2.Append("<cac:Signature>")
        builder2.Append("<cbc:ID>IDSignKG</cbc:ID>")
        builder2.Append("<cac:SignatoryParty>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID>" & notas.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name><![CDATA[" & notas.NombreComercialEmisor & "]]></cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("</cac:SignatoryParty>")
        builder2.Append("<cac:DigitalSignatureAttachment>")
        builder2.Append("<cac:ExternalReference>")
        builder2.Append("<cbc:URI>SignatureSP</cbc:URI>")
        builder2.Append("</cac:ExternalReference>")
        builder2.Append("</cac:DigitalSignatureAttachment>")
        builder2.Append("</cac:Signature>")
        builder2.Append("<cac:AccountingSupplierParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID schemeID=""" & notas.TipoDocumentoEmisor & """ ")
        builder2.Append("schemeName=""Documento de Identidad"" ")
        builder2.Append("schemeAgencyName=""PE:SUNAT"" ")
        builder2.Append("schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">" & notas.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name><![CDATA[" & notas.NombreComercialEmisor & "]]></cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & notas.RazonSocialEmisor & "]]></cbc:RegistrationName>")
        builder2.Append("<cac:RegistrationAddress>")
        builder2.Append("<cbc:AddressTypeCode>" & notas.CodigoFiscalEmisor & "</cbc:AddressTypeCode>")
        builder2.Append("<cac:AddressLine>")
        builder2.Append("<cbc:Line><![CDATA[" & notas.DireccionEmisor & "]]></cbc:Line>")
        builder2.Append("</cac:AddressLine>")
        builder2.Append("</cac:RegistrationAddress>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:AccountingSupplierParty>")
        builder2.Append("<cac:AccountingCustomerParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID schemeID=""" & notas.TipoDocumentoAdquirente & """ ")
        builder2.Append("schemeName=""Documento de Identidad"" ")
        builder2.Append("schemeAgencyName=""PE:SUNAT"" ")
        builder2.Append("schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">" & notas.DocumentoAdquirente & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & notas.RazonSocialAdquirente & "]]></cbc:RegistrationName>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:AccountingCustomerParty>")
        If notas.CodigoTipoNota = "13" Then
            builder2.Append("<cac:PaymentTerms>")
            builder2.Append("<cbc:ID>FormaPago</cbc:ID>")
            builder2.Append("<cbc:PaymentMeansID>Credito</cbc:PaymentMeansID>")
            Dim linAmount As String = String.Concat("<cbc:Amount currencyID=""", notas.CodigoTipoMoneda, """>", notas.MontoNetoPagoDocAfectado.ToString("#0.00"), "</cbc:Amount>")
            builder2.Append(linAmount)
            builder2.Append("</cac:PaymentTerms>")
            For Each objcuotas As Cuotas In cuotas
                builder2.Append("<cac:PaymentTerms>")
                builder2.Append("<cbc:ID>FormaPago</cbc:ID>")
                builder2.Append("<cbc:PaymentMeansID>" & objcuotas.CodigoCuota & "</cbc:PaymentMeansID>")
                Dim linAmount1 As String = String.Concat("<cbc:Amount currencyID=""", notas.CodigoTipoMoneda, """>", objcuotas.MontoCuota.ToString("#0.00"), "</cbc:Amount>")
                builder2.Append(linAmount1)
                builder2.Append("<cbc:PaymentDueDate>" & objcuotas.FechaVencimiento & "</cbc:PaymentDueDate>")
                builder2.Append("</cac:PaymentTerms>")
            Next
        End If
        builder2.Append("<cac:TaxTotal>")
        Dim taxtot_1 As String = String.Concat("<cbc:TaxAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.MontoTotalImpuestos.ToString("#0.00"), "</cbc:TaxAmount>")
        builder2.Append(taxtot_1)
        builder2.Append("<cac:TaxSubtotal>")
        Dim taxsub_1 As String = String.Concat("<cbc:TaxableAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.TotalOperacionesGravadas.ToString("#0.00"), "</cbc:TaxableAmount>")
        builder2.Append(taxsub_1)
        Dim taxsub_2 As String = String.Concat("<cbc:TaxAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.SumatoriaIGV.ToString("#0.00"), "</cbc:TaxAmount>")
        builder2.Append(taxsub_2)
        builder2.Append("<cac:TaxCategory>")
        builder2.Append("<cac:TaxScheme>")
        builder2.Append("<cbc:ID>" & notas.TipoTributo & "</cbc:ID>")
        builder2.Append("<cbc:Name>" & notas.NombreTributo & "</cbc:Name>")
        builder2.Append("<cbc:TaxTypeCode>" & notas.TipoCodigoTributo & "</cbc:TaxTypeCode>")
        builder2.Append("</cac:TaxScheme>")
        builder2.Append("</cac:TaxCategory>")
        builder2.Append("</cac:TaxSubtotal>")
        builder2.Append("</cac:TaxTotal>")
        builder2.Append("<cac:LegalMonetaryTotal>")
        Dim linea1 As String = String.Concat("<cbc:LineExtensionAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.TotalValorVenta.ToString("#0.00"), "</cbc:LineExtensionAmount>")
        builder2.Append(linea1)
        Dim linea2 As String = String.Concat("<cbc:TaxInclusiveAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.TotalPrecioVenta.ToString("#0.00"), "</cbc:TaxInclusiveAmount>")
        builder2.Append(linea2)
        Dim linea3 As String = String.Concat("<cbc:PayableAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.ImporteTotalComprobante.ToString("#0.00"), "</cbc:PayableAmount>")
        builder2.Append(linea3)
        builder2.Append("</cac:LegalMonetaryTotal>")
        Dim lineadetalle As String = ""
        For Each detalle As Detalle In notas.Detalle
            builder2.Append("<cac:CreditNoteLine>")
            builder2.Append("<cbc:ID>" & detalle.NumeroItem & "</cbc:ID>")
            lineadetalle = String.Concat("<cbc:CreditedQuantity unitCode=""", detalle.UnidadMedida, """>", detalle.Cantidad.ToString("#0.00"), "</cbc:CreditedQuantity>")
            builder2.Append(lineadetalle)
            lineadetalle = String.Concat("<cbc:LineExtensionAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.ValorVentaItem.ToString("#0.00"), "</cbc:LineExtensionAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:PricingReference>")
            builder2.Append("<cac:AlternativeConditionPrice>")
            lineadetalle = String.Concat("<cbc:PriceAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.PrecioUnitario.ToString("#0.000000"), "</cbc:PriceAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cbc:PriceTypeCode>" & detalle.CodigoTipoPrecio & "</cbc:PriceTypeCode>")
            builder2.Append("</cac:AlternativeConditionPrice>")
            builder2.Append("</cac:PricingReference>")
            builder2.Append("<cac:TaxTotal>")
            lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoTributoItem.ToString("#0.00"), "</cbc:TaxAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:TaxSubtotal>")
            lineadetalle = String.Concat("<cbc:TaxableAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoOperacion.ToString("#0.00"), "</cbc:TaxableAmount>")
            builder2.Append(lineadetalle)
            lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoTributoItem.ToString("#0.00"), "</cbc:TaxAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:TaxCategory>")
            builder2.Append("<cbc:Percent>" & detalle.PorcentajeImpuestos.ToString("#0.00") & "</cbc:Percent>")
            builder2.Append("<cbc:TaxExemptionReasonCode listName=""Afectacion del IGV"" listAgencyName=""PE:SUNAT"" ")
            builder2.Append(" listURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"">")
            builder2.Append(detalle.CodigoAfectacionIgv)
            builder2.Append("</cbc:TaxExemptionReasonCode>")
            builder2.Append("<cac:TaxScheme>")
            builder2.Append("<cbc:ID>" & detalle.CategoriaImpuestos & "</cbc:ID>")
            builder2.Append("<cbc:Name>" & detalle.NombreTributo & "</cbc:Name>")
            builder2.Append("<cbc:TaxTypeCode>" & detalle.CodigoTributo & "</cbc:TaxTypeCode>")
            builder2.Append("</cac:TaxScheme>")
            builder2.Append("</cac:TaxCategory>")
            builder2.Append("</cac:TaxSubtotal>")
            builder2.Append("</cac:TaxTotal>")
            builder2.Append("<cac:Item>")
            builder2.Append("<cbc:Description><![CDATA[" & detalle.DescripcionProducto & "]]></cbc:Description>")
            builder2.Append("<cac:SellersItemIdentification><cbc:ID>" & detalle.CodigoProducto & "</cbc:ID></cac:SellersItemIdentification>")
            builder2.Append("</cac:Item>")
            builder2.Append("<cac:Price>")
            lineadetalle = String.Concat("<cbc:PriceAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.ValorUnitarioxItem.ToString("#0.000000"), "</cbc:PriceAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("</cac:Price>")
            builder2.Append("</cac:CreditNoteLine>")
        Next
        builder2.Append("</CreditNote>")
        builder.AppendLine(builder2.ToString())
        Return builder
    End Function
    Private Function CrearXmlNotaDebito(ByVal notas As Comprobante) As StringBuilder
        Dim builder As StringBuilder = New StringBuilder()
        Dim builder2 As StringBuilder = New StringBuilder()
        builder2.Append("<?xml version=""1.0"" encoding =""UTF-8""?>")
        builder2.Append("<DebitNote xmlns=""urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2""")
        builder2.Append(" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2""")
        builder2.Append(" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2""")
        builder2.Append(" xmlns:ccts=""urn:un:unece:uncefact:documentation:2""")
        builder2.Append(" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#""")
        builder2.Append(" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2""")
        builder2.Append(" xmlns:qdt=""urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2""")
        builder2.Append(" xmlns:udt=""urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2""")
        builder2.Append(" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">")
        builder2.Append("<ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent/></ext:UBLExtension></ext:UBLExtensions>")
        builder2.Append("<cbc:UBLVersionID>2.1</cbc:UBLVersionID>")
        builder2.Append("<cbc:CustomizationID>2.0</cbc:CustomizationID>")
        builder2.Append("<cbc:ID>" & notas.SerieNumeroComprobante & "</cbc:ID>")
        builder2.Append("<cbc:IssueDate>" & notas.FechaEmision & "</cbc:IssueDate>")
        builder2.Append("<cbc:IssueTime>" & notas.HoraEmision & "</cbc:IssueTime>")
        builder2.Append("<cbc:DocumentCurrencyCode>" & notas.CodigoTipoMoneda & "</cbc:DocumentCurrencyCode>")
        builder2.Append("<cac:DiscrepancyResponse>")
        builder2.Append("<cbc:ReferenceID>" & notas.SerieNumeroDocAfectado & "</cbc:ReferenceID>")
        builder2.Append("<cbc:ResponseCode>" & notas.CodigoTipoNota & "</cbc:ResponseCode>")
        builder2.Append("<cbc:Description><![CDATA[" & notas.MotivoNota & "]]></cbc:Description>")
        builder2.Append("</cac:DiscrepancyResponse>")
        builder2.Append("<cac:BillingReference>")
        builder2.Append("<cac:InvoiceDocumentReference>")
        builder2.Append("<cbc:ID>" & notas.SerieNumeroDocAfectado & "</cbc:ID>")
        builder2.Append("<cbc:DocumentTypeCode>" & notas.TipoDocumentoAfectado & "</cbc:DocumentTypeCode>")
        builder2.Append("</cac:InvoiceDocumentReference>")
        builder2.Append("</cac:BillingReference>")
        builder2.Append("<cac:Signature>")
        builder2.Append("<cbc:ID>IDSignKG</cbc:ID>")
        builder2.Append("<cac:SignatoryParty>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID>" & notas.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name><![CDATA[" & notas.NombreComercialEmisor & "]]></cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("</cac:SignatoryParty>")
        builder2.Append("<cac:DigitalSignatureAttachment>")
        builder2.Append("<cac:ExternalReference>")
        builder2.Append("<cbc:URI>SignatureSP</cbc:URI>")
        builder2.Append("</cac:ExternalReference>")
        builder2.Append("</cac:DigitalSignatureAttachment>")
        builder2.Append("</cac:Signature>")
        builder2.Append("<cac:AccountingSupplierParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID schemeID=""" & notas.TipoDocumentoEmisor & """ ")
        builder2.Append("schemeName=""Documento de Identidad"" ")
        builder2.Append("schemeAgencyName=""PE:SUNAT"" ")
        builder2.Append("schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">" & notas.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name><![CDATA[" & notas.NombreComercialEmisor & "]]></cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & notas.RazonSocialEmisor & "]]></cbc:RegistrationName>")
        builder2.Append("<cac:RegistrationAddress>")
        builder2.Append("<cbc:AddressTypeCode>" & notas.CodigoFiscalEmisor & "</cbc:AddressTypeCode>")
        builder2.Append("<cac:AddressLine>")
        builder2.Append("<cbc:Line><![CDATA[" & notas.DireccionEmisor & "]]></cbc:Line>")
        builder2.Append("</cac:AddressLine>")
        builder2.Append("</cac:RegistrationAddress>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:AccountingSupplierParty>")
        builder2.Append("<cac:AccountingCustomerParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID schemeID=""" & notas.TipoDocumentoAdquirente & """ ")
        builder2.Append("schemeName=""Documento de Identidad"" ")
        builder2.Append("schemeAgencyName=""PE:SUNAT"" ")
        builder2.Append("schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">" & notas.DocumentoAdquirente & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & notas.RazonSocialAdquirente & "]]></cbc:RegistrationName>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:AccountingCustomerParty>")
        builder2.Append("<cac:TaxTotal>")
        Dim taxtot_1 As String = String.Concat("<cbc:TaxAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.MontoTotalImpuestos.ToString("#0.00"), "</cbc:TaxAmount>")
        builder2.Append(taxtot_1)
        builder2.Append("<cac:TaxSubtotal>")
        Dim taxsub_1 As String = String.Concat("<cbc:TaxableAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.TotalOperacionesGravadas.ToString("#0.00"), "</cbc:TaxableAmount>")
        builder2.Append(taxsub_1)
        Dim taxsub_2 As String = String.Concat("<cbc:TaxAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.SumatoriaIGV.ToString("#0.00"), "</cbc:TaxAmount>")
        builder2.Append(taxsub_2)
        builder2.Append("<cac:TaxCategory>")
        builder2.Append("<cac:TaxScheme>")
        builder2.Append("<cbc:ID>" & notas.TipoTributo & "</cbc:ID>")
        builder2.Append("<cbc:Name>" & notas.NombreTributo & "</cbc:Name>")
        builder2.Append("<cbc:TaxTypeCode>" & notas.TipoCodigoTributo & "</cbc:TaxTypeCode>")
        builder2.Append("</cac:TaxScheme>")
        builder2.Append("</cac:TaxCategory>")
        builder2.Append("</cac:TaxSubtotal>")
        builder2.Append("</cac:TaxTotal>")
        builder2.Append("<cac:RequestedMonetaryTotal>")
        Dim linea1 As String = String.Concat("<cbc:LineExtensionAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.TotalValorVenta.ToString("#0.00"), "</cbc:LineExtensionAmount>")
        builder2.Append(linea1)
        Dim linea2 As String = String.Concat("<cbc:TaxInclusiveAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.TotalPrecioVenta.ToString("#0.00"), "</cbc:TaxInclusiveAmount>")
        builder2.Append(linea2)
        Dim linea3 As String = String.Concat("<cbc:PayableAmount currencyID=""", notas.CodigoTipoMoneda, """>", notas.ImporteTotalComprobante.ToString("#0.00"), "</cbc:PayableAmount>")
        builder2.Append(linea3)
        builder2.Append("</cac:RequestedMonetaryTotal>")
        Dim lineadetalle As String = ""
        For Each detalle As Detalle In notas.Detalle
            builder2.Append("<cac:DebitNoteLine>")
            builder2.Append("<cbc:ID>" & detalle.NumeroItem & "</cbc:ID>")
            lineadetalle = String.Concat("<cbc:DebitedQuantity unitCode=""", detalle.UnidadMedida, """>", detalle.Cantidad.ToString("#0"), "</cbc:DebitedQuantity>")
            builder2.Append(lineadetalle)
            lineadetalle = String.Concat("<cbc:LineExtensionAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.ValorVentaItem.ToString("#0.00"), "</cbc:LineExtensionAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:PricingReference>")
            builder2.Append("<cac:AlternativeConditionPrice>")
            lineadetalle = String.Concat("<cbc:PriceAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.PrecioUnitario.ToString("#0.000000"), "</cbc:PriceAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cbc:PriceTypeCode>" & detalle.CodigoTipoPrecio & "</cbc:PriceTypeCode>")
            builder2.Append("</cac:AlternativeConditionPrice>")
            builder2.Append("</cac:PricingReference>")
            builder2.Append("<cac:TaxTotal>")
            lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoTributoItem.ToString("#0.00"), "</cbc:TaxAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:TaxSubtotal>")
            lineadetalle = String.Concat("<cbc:TaxableAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoOperacion.ToString("#0.00"), "</cbc:TaxableAmount>")
            builder2.Append(lineadetalle)
            lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.MontoTributoItem.ToString("#0.00"), "</cbc:TaxAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:TaxCategory>")
            builder2.Append("<cbc:Percent>" & detalle.PorcentajeImpuestos.ToString("#0.00") & "</cbc:Percent>")
            builder2.Append("<cbc:TaxExemptionReasonCode listName=""Afectacion del IGV"" listAgencyName=""PE:SUNAT"" ")
            builder2.Append(" listURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"">")
            builder2.Append(detalle.CodigoAfectacionIgv)
            builder2.Append("</cbc:TaxExemptionReasonCode>")
            builder2.Append("<cac:TaxScheme>")
            builder2.Append("<cbc:ID>" & detalle.CategoriaImpuestos & "</cbc:ID>")
            builder2.Append("<cbc:Name>" & detalle.NombreTributo & "</cbc:Name>")
            builder2.Append("<cbc:TaxTypeCode>" & detalle.CodigoTributo & "</cbc:TaxTypeCode>")
            builder2.Append("</cac:TaxScheme>")
            builder2.Append("</cac:TaxCategory>")
            builder2.Append("</cac:TaxSubtotal>")
            builder2.Append("</cac:TaxTotal>")
            builder2.Append("<cac:Item>")
            builder2.Append("<cbc:Description><![CDATA[" & detalle.DescripcionProducto & "]]></cbc:Description>")
            builder2.Append("</cac:Item>")
            builder2.Append("<cac:Price>")
            lineadetalle = String.Concat("<cbc:PriceAmount currencyID=""", detalle.CodigoTipoMoneda, """>", detalle.ValorUnitarioxItem.ToString("#0.000000"), "</cbc:PriceAmount>")
            builder2.Append(lineadetalle)
            builder2.Append("</cac:Price>")
            builder2.Append("</cac:DebitNoteLine>")
        Next
        builder2.Append("</DebitNote>")
        builder.AppendLine(builder2.ToString())
        Return builder
    End Function
    Public Function GenerarXml(ByVal xml As StringBuilder) As Byte()
        Dim name As String = "UTF-8"
        Dim settings As XmlWriterSettings = New XmlWriterSettings()
        settings.Indent = True
        settings.OmitXmlDeclaration = True
        settings.Encoding = Encoding.GetEncoding(name)
        Dim output As System.IO.MemoryStream = New System.IO.MemoryStream()
        Using writer As XmlWriter = XmlWriter.Create(output, settings)
            writer.WriteRaw(xml.ToString())
        End Using
        If Not output.CanRead Then
            Throw New ArgumentException()
        End If
        If output.CanSeek Then
            output.Seek(0L, System.IO.SeekOrigin.Begin)
        End If
        Dim buffer As Byte() = New Byte(output.Length - 1) {}
        output.Read(buffer, 0, buffer.Length)
        Return buffer
    End Function
    Private Function CrearXmlComunicacionBaja_FT_NC_ND(ByVal comunicacionbaja As ComprobanteBaja) As StringBuilder
        Dim xmlfinal As StringBuilder = New StringBuilder()
        Dim encoding As String = "UTF-8"
        Dim docxml As StringBuilder = New StringBuilder()
        docxml.Append("<?xml version=""1.0"" encoding=""" & encoding & """ standalone=""no""?>")
        docxml.Append(" <VoidedDocuments xmlns=""urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1""")
        docxml.Append(" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2""")
        docxml.Append(" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2""")
        docxml.Append(" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#""")
        docxml.Append(" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2""")
        docxml.Append(" xmlns:sac=""urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1""")
        docxml.Append(" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">")
        docxml.Append("<ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent></ext:ExtensionContent></ext:UBLExtension></ext:UBLExtensions>")
        docxml.Append("<cbc:UBLVersionID>2.0</cbc:UBLVersionID>")
        docxml.Append("<cbc:CustomizationID>1.0</cbc:CustomizationID>")
        docxml.Append("<cbc:ID>" + comunicacionbaja.IdentResumen + "</cbc:ID>")
        docxml.Append("<cbc:ReferenceDate>" + comunicacionbaja.FecEmiDocumento + "</cbc:ReferenceDate>")
        docxml.Append("<cbc:IssueDate>" + comunicacionbaja.FecGenDocumento + "</cbc:IssueDate>")
        docxml.Append("<cac:Signature><cbc:ID>" + comunicacionbaja.NumDocEmisor + "</cbc:ID>")
        docxml.Append("<cac:SignatoryParty><cac:PartyIdentification>")
        docxml.Append("<cbc:ID>" + comunicacionbaja.NumDocEmisor + "</cbc:ID></cac:PartyIdentification>")
        docxml.Append("<cac:PartyName>")
        docxml.Append("<cbc:Name><![CDATA[" + comunicacionbaja.NomComEmisor + "]]></cbc:Name>")
        docxml.Append("</cac:PartyName></cac:SignatoryParty>")
        docxml.Append("<cac:DigitalSignatureAttachment><cac:ExternalReference>")
        docxml.Append("<cbc:URI>SignatureSP</cbc:URI>")
        docxml.Append("</cac:ExternalReference></cac:DigitalSignatureAttachment>")
        docxml.Append("</cac:Signature>")
        docxml.Append("<cac:AccountingSupplierParty>")
        docxml.Append("<cbc:CustomerAssignedAccountID>" + comunicacionbaja.NumDocEmisor + "</cbc:CustomerAssignedAccountID>")
        docxml.Append("<cbc:AdditionalAccountID>" + comunicacionbaja.TipDocEmisor + "</cbc:AdditionalAccountID>")
        docxml.Append("<cac:Party><cac:PartyLegalEntity><cbc:RegistrationName><![CDATA[" + comunicacionbaja.NomComEmisor + "]]></cbc:RegistrationName></cac:PartyLegalEntity></cac:Party>")
        docxml.Append("</cac:AccountingSupplierParty>")
        docxml.Append("<sac:VoidedDocumentsLine><cbc:LineID>" + comunicacionbaja.Item.ToString() + "</cbc:LineID>")
        docxml.Append("<cbc:DocumentTypeCode>" + comunicacionbaja.TipoDocumento + "</cbc:DocumentTypeCode>")
        docxml.Append("<sac:DocumentSerialID>" + comunicacionbaja.SerieDocumento + "</sac:DocumentSerialID>")
        docxml.Append("<sac:DocumentNumberID>" + comunicacionbaja.NumCorreDocu + "</sac:DocumentNumberID>")
        docxml.Append("<sac:VoidReasonDescription>" + comunicacionbaja.MotivoBaja + "</sac:VoidReasonDescription>")
        docxml.Append("</sac:VoidedDocumentsLine>")
        docxml.Append("</VoidedDocuments>")
        xmlfinal.AppendLine(docxml.ToString())
        Return xmlfinal
    End Function
    Private Function CrearXmlComunicacionBaja_BV(ByVal comunicacionbaja As ComprobanteBaja) As StringBuilder
        Dim xmlfinal As StringBuilder = New StringBuilder()
        Dim encoding As String = "UTF-8"
        Dim docxml As StringBuilder = New StringBuilder()
        Dim lineadetalle As String = ""
        docxml.Append("<?xml version=""1.0"" encoding=""" & encoding & """ standalone=""no""?>")
        docxml.Append(" <SummaryDocuments xmlns=""urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1""")
        docxml.Append(" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2""")
        docxml.Append(" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2""")
        docxml.Append(" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#""")
        docxml.Append(" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2""")
        docxml.Append(" xmlns:sac=""urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1"">")
        docxml.Append("<ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent></ext:ExtensionContent></ext:UBLExtension></ext:UBLExtensions>")
        docxml.Append("<cbc:UBLVersionID>2.0</cbc:UBLVersionID>")
        docxml.Append("<cbc:CustomizationID>1.1</cbc:CustomizationID>")
        docxml.Append("<cbc:ID>" + comunicacionbaja.IdentResumen + "</cbc:ID>")
        docxml.Append("<cbc:ReferenceDate>" + comunicacionbaja.FecEmiDocumento + "</cbc:ReferenceDate>")
        docxml.Append("<cbc:IssueDate>" + comunicacionbaja.FecGenDocumento + "</cbc:IssueDate>")
        docxml.Append("<cac:Signature><cbc:ID>" + comunicacionbaja.NumDocEmisor + "</cbc:ID>")
        docxml.Append("<cac:SignatoryParty><cac:PartyIdentification>")
        docxml.Append("<cbc:ID>" + comunicacionbaja.NumDocEmisor + "</cbc:ID></cac:PartyIdentification>")
        docxml.Append("<cac:PartyName>")
        docxml.Append("<cbc:Name><![CDATA[" + comunicacionbaja.NomComEmisor + "]]></cbc:Name>")
        docxml.Append("</cac:PartyName></cac:SignatoryParty>")
        docxml.Append("<cac:DigitalSignatureAttachment><cac:ExternalReference>")
        docxml.Append("<cbc:URI>SignatureSP</cbc:URI>")
        docxml.Append("</cac:ExternalReference></cac:DigitalSignatureAttachment>")
        docxml.Append("</cac:Signature>")
        docxml.Append("<cac:AccountingSupplierParty>")
        docxml.Append("<cbc:CustomerAssignedAccountID>" + comunicacionbaja.NumDocEmisor + "</cbc:CustomerAssignedAccountID>")
        docxml.Append("<cbc:AdditionalAccountID>" + comunicacionbaja.TipDocEmisor + "</cbc:AdditionalAccountID>")
        docxml.Append("<cac:Party><cac:PartyLegalEntity><cbc:RegistrationName><![CDATA[" + comunicacionbaja.NomComEmisor + "]]></cbc:RegistrationName></cac:PartyLegalEntity></cac:Party>")
        docxml.Append("</cac:AccountingSupplierParty>")
        docxml.Append("<sac:SummaryDocumentsLine><cbc:LineID>" + comunicacionbaja.Item.ToString() + "</cbc:LineID>")
        docxml.Append("<cbc:DocumentTypeCode>" + comunicacionbaja.TipoDocumento + "</cbc:DocumentTypeCode>")
        docxml.Append("<cbc:ID>" + comunicacionbaja.SerieDocumento + "-" + comunicacionbaja.NumCorreDocu + "</cbc:ID>")
        docxml.Append("<cac:AccountingCustomerParty>")
        docxml.Append("<cbc:CustomerAssignedAccountID>" + comunicacionbaja.NumeroDocumentoReceptor.ToString() + "</cbc:CustomerAssignedAccountID>")
        docxml.Append("<cbc:AdditionalAccountID>" + comunicacionbaja.TipoDocumentoReceptor.ToString() + "</cbc:AdditionalAccountID>")
        docxml.Append("</cac:AccountingCustomerParty>")
        docxml.Append("<cac:Status>")
        docxml.Append("<cbc:ConditionCode>3</cbc:ConditionCode>")
        docxml.Append("</cac:Status>")
        lineadetalle = String.Concat("<sac:TotalAmount currencyID=""", comunicacionbaja.TipoMoneda, """>", comunicacionbaja.TotalComprobante.ToString("#0.00"), "</sac:TotalAmount>")
        docxml.Append(lineadetalle)
        docxml.Append("<sac:BillingPayment>")
        lineadetalle = String.Concat("<cbc:PaidAmount currencyID=""", comunicacionbaja.TipoMoneda, """>", comunicacionbaja.MontoTotalGravado.ToString("#0.00"), "</cbc:PaidAmount>")
        docxml.Append(lineadetalle)
        docxml.Append("<cbc:InstructionID>01</cbc:InstructionID>")
        docxml.Append("</sac:BillingPayment>")
        docxml.Append("<cac:TaxTotal>")
        lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", comunicacionbaja.TipoMoneda, """>", comunicacionbaja.MontoIGV.ToString("#0.00"), "</cbc:TaxAmount>")
        docxml.Append(lineadetalle)
        docxml.Append("<cac:TaxSubtotal>")
        lineadetalle = String.Concat("<cbc:TaxAmount currencyID=""", comunicacionbaja.TipoMoneda, """>", comunicacionbaja.MontoIGV.ToString("#0.00"), "</cbc:TaxAmount>")
        docxml.Append(lineadetalle)
        docxml.Append("<cac:TaxCategory>")
        docxml.Append("<cac:TaxScheme>")
        docxml.Append("<cbc:ID>1000</cbc:ID>")
        docxml.Append("<cbc:Name>IGV</cbc:Name>")
        docxml.Append("<cbc:TaxTypeCode>VAT</cbc:TaxTypeCode>")
        docxml.Append("</cac:TaxScheme>")
        docxml.Append("</cac:TaxCategory>")
        docxml.Append("</cac:TaxSubtotal>")
        docxml.Append("</cac:TaxTotal>")
        docxml.Append("</sac:SummaryDocumentsLine>")
        docxml.Append("</SummaryDocuments>")
        xmlfinal.AppendLine(docxml.ToString())
        Return xmlfinal
    End Function
    Public Function SaveData(ByVal Data As Byte(), ByVal filename As String) As Boolean
        Try
            Dim writer1 As System.IO.BinaryWriter = New System.IO.BinaryWriter(System.IO.File.OpenWrite(filename), Encoding.GetEncoding(&H4E4))
            writer1.Write(Data)
            writer1.Flush()
            writer1.Close()
        Catch
            Return False
        End Try
        Return True
    End Function
    Public Sub FirmarXml(ByVal pathxml As String)
        Dim str2 As String
        Dim document As XmlDocument = New XmlDocument With {.PreserveWhitespace = True}
        document.Load(pathxml)
        Dim path As String = Me.RutaFECERTClave
        Dim certificate As X509Certificate2 = New X509Certificate2(Me.RutaFECERT)
        Using reader As System.IO.TextReader = New System.IO.StreamReader(path)
            str2 = reader.ReadToEnd()
        End Using
        Dim provider As RSACryptoServiceProvider = Cryptokey.DecodeRsaPrivateKey(Helpers.GetBytesFromPEM(str2, emStringType.RsaPrivateKey))
        Dim xml1 As SignedXml = New SignedXml(document)
        xml1.SigningKey = provider
        Dim reference As Reference = New Reference With {.Uri = ""}
        reference.AddTransform(New XmlDsigEnvelopedSignatureTransform())
        xml1.AddReference(reference)
        Dim info As KeyInfo = New KeyInfo()
        Dim chain1 As X509Chain = New X509Chain()
        chain1.Build(certificate)
        Dim element1 As X509ChainElement = chain1.ChainElements(0)
        Dim clause As KeyInfoX509Data = New KeyInfoX509Data(element1.Certificate)
        clause.AddSubjectName(element1.Certificate.Subject)
        info.AddClause(clause)
        xml1.KeyInfo = info
        xml1.ComputeSignature()
        Dim xml As XmlElement = xml1.GetXml()
        xml.Prefix = "ds"
        xml1.ComputeSignature()
        For Each node As XmlNode In xml.SelectNodes("descendant-or-self::*[namespace-uri()='http://www.w3.org/2000/09/xmldsig#']")
            If node.LocalName = "Signature" Then
                Dim attribute As XmlAttribute = document.CreateAttribute("Id")
                attribute.Value = "SignatureSP"
                node.Attributes.Append(attribute)
                Exit For
            End If
        Next
        Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(document.NameTable)
        Dim xpath As String = ""

        If pathxml.Contains("-01-") Then
            nsmgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
            nsmgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2")
            nsmgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2")
            nsmgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
            nsmgr.AddNamespace("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2")
            nsmgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
            nsmgr.AddNamespace("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2")
            nsmgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#")
            xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent"
        End If

        If pathxml.Contains("-03-") Then
            nsmgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
            nsmgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2")
            nsmgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2")
            nsmgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
            nsmgr.AddNamespace("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2")
            nsmgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
            nsmgr.AddNamespace("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2")
            nsmgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#")
            xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent"
        End If

        If pathxml.Contains("-07-") Then
            nsmgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
            nsmgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2")
            nsmgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2")
            nsmgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
            nsmgr.AddNamespace("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2")
            nsmgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
            nsmgr.AddNamespace("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2")
            nsmgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
            xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent"
        End If

        If pathxml.Contains("-08-") Then
            nsmgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
            nsmgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2")
            nsmgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2")
            nsmgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
            nsmgr.AddNamespace("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2")
            nsmgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
            nsmgr.AddNamespace("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2")
            nsmgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
            xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent"
        End If

        If pathxml.Contains("-RA-") Then
            nsmgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1")
            nsmgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
            nsmgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
            nsmgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
            xpath = "/tns:VoidedDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent"
        End If
        If pathxml.Contains("-RC-") Then
            nsmgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1")
            nsmgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
            nsmgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
            nsmgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1")
            nsmgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
            xpath = "/tns:SummaryDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent"
        End If

        If pathxml.Contains("-09-") Then
            nsmgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:DespatchAdvice-2")
            nsmgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
            nsmgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2")
            nsmgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
            nsmgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#")
            xpath = "/tns:DespatchAdvice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent"
        End If
        document.SelectSingleNode(xpath, nsmgr).AppendChild(document.ImportNode(xml, True))
        document.Save(pathxml)
    End Sub
    Public Function Comprimir(ByVal cnombrearchivoOrigen As String, ByVal cnombreArchivoDestino As String) As String
        Dim zip As Ionic.Zip.ZipFile = New Ionic.Zip.ZipFile()
        zip.AddFile(cnombrearchivoOrigen, "")
        zip.Save(cnombreArchivoDestino)
        Dim rpta As String = "OK"
        Return rpta
    End Function
    Public Sub Descomprimir(ByVal cNombreArchivoOrigen As String, ByVal cNombreArchivoDestino As String)
        Using zip As New Ionic.Zip.ZipFile(cNombreArchivoOrigen)
            zip.ExtractAll(cNombreArchivoDestino)
        End Using
    End Sub
    Public Function EnviarDocumentoSunat(ByVal NombreArchivo As String, ByRef responsecode As Integer,
                                         ByRef description As String) As String
        Dim Mensaje As String = ""
        Dim cNombreArchivoZip As String = NombreArchivo & ".zip"
        Dim RutaArchivoZip As String = RutaFEENVIO & "/" & cNombreArchivoZip
        Dim bitArray As Byte() = System.IO.File.ReadAllBytes(RutaArchivoZip)
        Try
            If MDIPrincipal.CodigoEmpresa = 1 Then
                UsuarioFEEmpresa = ConfigurationManager.AppSettings("UsuarioFEEmpresa")
                PasswordFEEmpresa = ConfigurationManager.AppSettings("PasswordFEEmpresa")
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                UsuarioFEEmpresa = ConfigurationManager.AppSettings("UsuarioFEEmpresa2")
                PasswordFEEmpresa = ConfigurationManager.AppSettings("PasswordFEEmpresa2")
            End If
            Dim oservicio As servicioSunat.billServiceClient
            oservicio = New servicioSunat.billServiceClient("BillServicePort")
            ServicePointManager.UseNagleAlgorithm = True
            ServicePointManager.Expect100Continue = False
            ServicePointManager.CheckCertificateRevocationList = True
            Dim behavior = New PasswordDigestBehavior(UsuarioFEEmpresa, PasswordFEEmpresa)
            oservicio.Endpoint.EndpointBehaviors.Add(behavior)

            oservicio.Open()
            Dim returnByte As Byte() = oservicio.sendBill(cNombreArchivoZip, bitArray, "")
            oservicio.Close()

            If IO.File.Exists(RutaFECDRS & "R-" & cNombreArchivoZip) Then IO.File.Delete(RutaFECDRS & "R-" & cNombreArchivoZip)

            Dim fs As System.IO.FileStream = New System.IO.FileStream(RutaFECDRS & "R-" & cNombreArchivoZip, System.IO.FileMode.Create)
            fs.Write(returnByte, 0, returnByte.Length)
            fs.Close()

            Dim RutaRespuesta As String = RutaFERPTA & "R-" & cNombreArchivoZip.Split(".")(0) & ".xml"

            If IO.File.Exists(RutaRespuesta) Then IO.File.Delete(RutaRespuesta)
            Descomprimir(RutaFECDRS & "R-" & cNombreArchivoZip, RutaFERPTA)

            Dim XmlSunatRespuesta As XmlDocument = New XmlDocument()
            XmlSunatRespuesta.Load(RutaRespuesta)
            Dim ar As XmlNodeList = XmlSunatRespuesta.GetElementsByTagName("ar:ApplicationResponse")
            Dim dr As XmlNodeList = (CType(ar(0), XmlElement)).GetElementsByTagName("cac:DocumentResponse")
            Dim lista As XmlNodeList = (CType(dr(0), XmlElement)).GetElementsByTagName("cac:Response")

            For Each nodo As XmlNode In lista
                responsecode = nodo("cbc:ResponseCode").InnerText
                description = nodo("cbc:Description").InnerText
            Next
            Mensaje = ""
        Catch ex As FaultException
            Mensaje = "Presentacion-Util-FacturacionSunat MetodoEjecutado: EnviarDocumentoSunat - " + ex.Message
        End Try
        Return Mensaje
    End Function
    Public Shared Function ObtenerDigestValueSunat(ByVal archxml As String) As String
        Dim oXmlSunat As XmlDocument = New XmlDocument()
        oXmlSunat.Load(archxml)
        Dim manager As XmlNamespaceManager = New XmlNamespaceManager(oXmlSunat.NameTable)
        manager.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2")
        manager.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#")
        Dim valor As String = oXmlSunat.SelectSingleNode("//ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestValue", manager).InnerText
        Return valor
    End Function
    Public Function ActualizarEstadoSunatComprobante(ByVal Codigo As Integer, ByVal numeroticket As String, NombreXml As String, ByRef CodigoRespuesta As Integer) As String
        Dim Mensaje As String = ""
        CodigoRespuesta = 0
        Try
            Dim oSalida As Byte()
            Dim rutaconsulta As String
            Dim nombrearchivo As String
            If MDIPrincipal.CodigoEmpresa = 1 Then
                UsuarioFEEmpresa = ConfigurationManager.AppSettings("UsuarioFEEmpresa")
                PasswordFEEmpresa = ConfigurationManager.AppSettings("PasswordFEEmpresa")
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                UsuarioFEEmpresa = ConfigurationManager.AppSettings("UsuarioFEEmpresa2")
                PasswordFEEmpresa = ConfigurationManager.AppSettings("PasswordFEEmpresa2")
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            End If

            Dim osunat As New FacturacionSunat
            RutaFERPTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFERPTA")
            RutaFEXML = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEXML")
            RutaFECERT = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERT")
            RutaFECERTClave = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERTClave")
            RutaFEENVIO = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEENVIO")
            RutaFECDRS = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECDRS")
            RutaFECONSULTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECONSULTA")

            Dim oservicio As servicioSunat.billServiceClient
            oservicio = New servicioSunat.billServiceClient("BillServicePort")
            ServicePointManager.UseNagleAlgorithm = True
            ServicePointManager.Expect100Continue = False
            ServicePointManager.CheckCertificateRevocationList = True
            Dim behavior = New PasswordDigestBehavior(UsuarioFEEmpresa, PasswordFEEmpresa)
            oservicio.Endpoint.EndpointBehaviors.Add(behavior)

            oservicio.Open()
            Dim estadorpta As servicioSunat.statusResponse = oservicio.getStatus(numeroticket)
            oservicio.Close()

            Dim responsecode As String = ""
            Dim descripcion As String = ""

            responsecode = estadorpta.statusCode

            If responsecode = 0 Then
                CodigoRespuesta = 1
                nombrearchivo = numeroticket & ".zip"
                rutaconsulta = RutaFECONSULTA
                nombrearchivo = rutaconsulta & "/" & nombrearchivo
                oSalida = estadorpta.content
                SaveData(oSalida, nombrearchivo)

                Descomprimir(RutaFECONSULTA & numeroticket & ".zip", RutaFECONSULTA)
                numeroticket = RutaFECONSULTA & NombreXml.Split(".")(0) & ".xml"
                Dim xmlsunatrpta As XmlDocument = New XmlDocument()
                xmlsunatrpta.Load(numeroticket)

                Dim ar As XmlNodeList = xmlsunatrpta.GetElementsByTagName("ar:ApplicationResponse")
                Dim dr As XmlNodeList = (CType(ar(0), XmlElement)).GetElementsByTagName("cac:DocumentResponse")
                Dim lista As XmlNodeList = (CType(dr(0), XmlElement)).GetElementsByTagName("cac:Response")

                For Each nodo As XmlNode In lista
                    descripcion = nodo("cbc:Description").InnerText
                Next
            ElseIf responsecode = 98 Then
                CodigoRespuesta = -1
                descripcion = "En Proceso"
            ElseIf responsecode = 99 Then
                CodigoRespuesta = -2
                nombrearchivo = numeroticket & ".zip"
                rutaconsulta = RutaFECONSULTA
                nombrearchivo = rutaconsulta & "/" & nombrearchivo
                oSalida = estadorpta.content
                SaveData(oSalida, nombrearchivo)

                Descomprimir(RutaFECONSULTA & numeroticket, RutaFECONSULTA)
                numeroticket = RutaFECONSULTA & "R-" & numeroticket.Split(".")(0) & ".xml"
                Dim xmlsunatrpta As XmlDocument = New XmlDocument()
                xmlsunatrpta.Load(numeroticket)

                Dim ar As XmlNodeList = xmlsunatrpta.GetElementsByTagName("ar:ApplicationResponse")
                Dim dr As XmlNodeList = (CType(ar(0), XmlElement)).GetElementsByTagName("cac:DocumentResponse")
                Dim lista As XmlNodeList = (CType(dr(0), XmlElement)).GetElementsByTagName("cac:Response")

                For Each nodo As XmlNode In lista
                    descripcion = nodo("cbc:Description").InnerText
                Next
            End If

            Dim obj As New BLFacturacion
            Mensaje = ""
            If obj.ActualizarRespuestaSunat(Codigo, CodigoRespuesta, descripcion, "", "", 1, Mensaje) Then
                If Mensaje <> "" Then
                    MsgBox(Mensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                End If
            End If
        Catch ex As System.ServiceModel.FaultException
            Mensaje = ex.Message
        End Try
        Return Mensaje
    End Function
    Public Function ConsultarCDR(ByVal Codigo As Integer, ByVal ruc As String, ByVal tipo As String, ByVal serie As String, ByVal numero As String) As String
        Dim Mensaje As String = ""
        Try
            Dim oSalida As Byte()
            Dim nombrearchivo As String

            If MDIPrincipal.CodigoEmpresa = 1 Then
                UsuarioFEEmpresa = ConfigurationManager.AppSettings("UsuarioFEEmpresa")
                PasswordFEEmpresa = ConfigurationManager.AppSettings("PasswordFEEmpresa")
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                UsuarioFEEmpresa = ConfigurationManager.AppSettings("UsuarioFEEmpresa2")
                PasswordFEEmpresa = ConfigurationManager.AppSettings("PasswordFEEmpresa2")
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            End If

            RutaFERPTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFERPTA")
            RutaFEXML = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEXML")
            RutaFECERT = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERT")
            RutaFECERTClave = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERTClave")
            RutaFEENVIO = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEENVIO")
            RutaFECDRS = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECDRS")
            RutaFECONSULTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECONSULTA")

            Dim oservicio As New consultaSunat.billServiceClient
            ServicePointManager.UseNagleAlgorithm = True
            ServicePointManager.Expect100Continue = False
            ServicePointManager.CheckCertificateRevocationList = True

            oservicio.Open()
            Dim estadorpta As consultaSunat.statusResponse = oservicio.getStatusCdr(ruc, tipo, serie, numero)
            oservicio.Close()

            Dim responsecode As String = ""
            Dim descripcion As String = ""
            responsecode = estadorpta.statusCode
            descripcion = estadorpta.statusMessage

            If MsgBox(descripcion & ": ¿Desea continuar con el proceso?", MessageBoxIcon.Question + vbYesNo, "Mensaje") = vbNo Then
                Return Mensaje
                Exit Function
            End If

            nombrearchivo = "R-" & ruc & "-" & tipo & "-" & serie & "-" & numero & ".zip"
            nombrearchivo = RutaFECDRS & "/" & nombrearchivo
            oSalida = estadorpta.content
            SaveData(oSalida, nombrearchivo)

            Dim rutaxmlsunatrpta As String = RutaFERPTA

            Descomprimir(nombrearchivo, rutaxmlsunatrpta)

            Dim nombrexml As String = rutaxmlsunatrpta & "R-" & ruc & "-" & tipo & "-" & serie & "-" & numero & ".xml"
            Dim xmlsunatrpta As XmlDocument = New XmlDocument()
            xmlsunatrpta.Load(nombrexml)
            Dim ar As XmlNodeList = xmlsunatrpta.GetElementsByTagName("ar:ApplicationResponse")
            Dim dr As XmlNodeList = (CType(ar(0), XmlElement)).GetElementsByTagName("cac:DocumentResponse")
            Dim lista As XmlNodeList = (CType(dr(0), XmlElement)).GetElementsByTagName("cac:Response")

            For Each nodo As XmlNode In lista
                responsecode = nodo("cbc:ResponseCode").InnerText
                descripcion = nodo("cbc:Description").InnerText
            Next
            Dim CodigoHas As String = ""
            Mensaje = ""
            CodigoHas = ObtenerDigestValueSunat(nombrexml)
            Dim obj As New BLFacturacion
            If obj.ActualizarRespuestaSunat(Codigo, responsecode, descripcion, "", CodigoHas, 1, Mensaje) Then
                If Mensaje <> "" Then
                    MsgBox(Mensaje, MsgBoxStyle.Exclamation, "MENSAJE")
                End If
            End If
            Return Mensaje
        Catch ex As System.ServiceModel.FaultException
            Mensaje = ex.Message
        End Try
        Return Mensaje
    End Function
    Public Function EnviarGuia(ByVal guia As Guia, ByVal Codigo As Integer, ByRef cNumTickSun As String) As String
        Dim Mensaje As String = ""
        Try
            If MDIPrincipal.CodigoEmpresa = 1 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            End If
            RutaFERPTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFERPTA")
            RutaFEXML = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEXML")
            RutaFECERT = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERT")
            RutaFECERTClave = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECERTClave")
            RutaFEENVIO = RutaEmpresa + ConfigurationManager.AppSettings("RutaFEENVIO")
            RutaFECDRS = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECDRS")
            RutaFECONSULTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECONSULTA")

            Dim Xml As StringBuilder = Nothing
            Xml = CrearXmlGuia(guia)
            Dim DataXml As Byte() = GenerarXml(Xml)
            Dim NombreArchivo As String = guia.NumeroDocumentoEmisor & "-" & guia.CodigoTipoGuia & "-" & guia.SerieNumeroGuia
            Dim RutaXml As String = RutaFEXML & "/" & NombreArchivo & ".xml"
            Dim RutaZip As String = RutaFEENVIO & "/" & NombreArchivo & ".zip"
            If IO.File.Exists(RutaXml) Then IO.File.Delete(RutaXml)
            SaveData(DataXml, RutaXml)
            Call FirmarXml(RutaXml)
            If IO.File.Exists(RutaZip) Then IO.File.Delete(RutaZip)
            Comprimir(RutaXml, RutaZip)
            Dim bitArray As Byte() = System.IO.File.ReadAllBytes(RutaZip)
            Dim bitArray64 As String = Convert.ToBase64String(bitArray)
            Dim Hash As String = CalcularHashArchivo(RutaZip)
            Dim oDatoGeneral As New BERequestEnvioguiaGeneral
            Dim oDato As New BERequestEnvioguia
            oDato.nomArchivo = NombreArchivo & ".zip"
            oDato.arcGreZip = bitArray64.ToString
            oDato.hashZip = Hash.ToLower
            oDatoGeneral.archivo = oDato
            Dim numTicket As String = ""
            Mensaje = ""
            Dim obj As New GuiaElectronica
            Mensaje = obj.EnviarGuia(MDIPrincipal.CodigoEmpresa, NombreArchivo, oDatoGeneral, numTicket)
            If Mensaje = "" Then
                cNumTickSun = numTicket
                Dim objt As New BLFacturacion
                objt.RegistraNumeroTicketEnvioSunat(Codigo, numTicket, 2, Mensaje)
            End If
        Catch ex As Exception
            Mensaje = "Presentacion-Util-FacturacionSunat MetodoEjecutado: EnviarGuia - " + ex.Message
        End Try
        Return Mensaje
    End Function
    Public Function ConsultarEstadoEnvioGuia(ByVal tipoguia As String, ByVal serienumero As String, ByVal numTicket As String,
                                             ByVal Codigo As Integer) As String
        Dim Mensaje As String = ""
        Try
            If MDIPrincipal.CodigoEmpresa = 1 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa")
            ElseIf MDIPrincipal.CodigoEmpresa = 2 Then
                RutaEmpresa = ConfigurationManager.AppSettings("RutaEmpresa2")
            End If
            RutaFERPTA = RutaEmpresa + ConfigurationManager.AppSettings("RutaFERPTA")
            RutaFECDRS = RutaEmpresa + ConfigurationManager.AppSettings("RutaFECDRS")
            Dim NombreArchivo As String = MDIPrincipal.NumeroRuc & "-" & tipoguia & "-" & serienumero
            Mensaje = ""
            Dim obj As New GuiaElectronica
            Mensaje = obj.ConsultarEstadoEnvio(MDIPrincipal.CodigoEmpresa, numTicket, RutaFECDRS, NombreArchivo)
            If Mensaje = "" Then
                Dim RutaRespuesta As String = RutaFERPTA & "R-" & NombreArchivo & ".xml"
                If IO.File.Exists(RutaRespuesta) Then IO.File.Delete(RutaRespuesta)
                Descomprimir(RutaFECDRS & "R-" & NombreArchivo & ".zip", RutaFERPTA)
                Dim XmlSunatRespuesta As XmlDocument = New XmlDocument()
                XmlSunatRespuesta.Load(RutaRespuesta)
                Dim ar As XmlNodeList = XmlSunatRespuesta.GetElementsByTagName("ar:ApplicationResponse")
                Dim dr As XmlNodeList = (CType(ar(0), XmlElement)).GetElementsByTagName("cac:DocumentResponse")
                Dim listaRespuesta As XmlNodeList = (CType(dr(0), XmlElement)).GetElementsByTagName("cac:Response")
                Dim responsecode As Integer = 0
                Dim Description As String = ""
                For Each nodo As XmlNode In listaRespuesta
                    responsecode = Convert.ToInt16(nodo("cbc:ResponseCode").InnerText)
                    Description = nodo("cbc:Description").InnerText
                Next
                Dim CodigoQR As String = ""
                Dim CodigoHas As String = ""
                Dim listaQR As XmlNodeList = (CType(dr(0), XmlElement)).GetElementsByTagName("cac:DocumentReference")
                If responsecode = 0 Then
                    CodigoHas = ObtenerDigestValueSunat(RutaRespuesta)
                    For Each nodo As XmlNode In listaQR
                        CodigoQR = nodo("cbc:DocumentDescription").InnerText
                    Next
                Else
                    Description = "CODIGO ERROR: " + responsecode.ToString + " - MOTIVO DE RECHAZO: " + Description.ToString
                End If
                Dim objt As New BLFacturacion
                objt.ActualizarRespuestaSunat(Codigo, responsecode, Description, CodigoQR, CodigoHas, 2, Mensaje)
            End If
        Catch ex As Exception
            Mensaje = "Presentacion-Util-FacturacionSunat MetodoEjecutado: ConsultarEstadoEnvio - " + ex.Message
        End Try
        Return Mensaje
    End Function
    Private Function CrearXmlGuia(ByVal guia As Guia) As StringBuilder
        Dim builder As StringBuilder = New StringBuilder()
        Dim builder2 As StringBuilder = New StringBuilder()
        Dim lineadetalle As String = ""
        builder2.Append("<?xml version=""1.0"" encoding=""ISO-8859-1"" standalone=""no""?>")
        builder2.Append("<DespatchAdvice xmlns=""urn:oasis:names:specification:ubl:schema:xsd:DespatchAdvice-2""")
        builder2.Append(" xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2""")
        builder2.Append(" xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2""")
        builder2.Append(" xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2""")
        builder2.Append(" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"">")
        builder2.Append("<ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent/></ext:UBLExtension></ext:UBLExtensions>")
        builder2.Append("<cbc:UBLVersionID>2.1</cbc:UBLVersionID>")
        builder2.Append("<cbc:CustomizationID>2.0</cbc:CustomizationID>")
        builder2.Append("<cbc:ID>" & guia.SerieNumeroGuia & "</cbc:ID>")
        builder2.Append("<cbc:IssueDate>" & guia.FechaEmision & "</cbc:IssueDate>")
        builder2.Append("<cbc:IssueTime>" & guia.HoraEmision & "</cbc:IssueTime>")
        builder2.Append("<cbc:DespatchAdviceTypeCode>" & guia.CodigoTipoGuia & "</cbc:DespatchAdviceTypeCode>")
        If guia.TipoDocumentoAdjunto = "01" Or guia.TipoDocumentoAdjunto = "03" Then
            builder2.Append("<cac:AdditionalDocumentReference>")
            builder2.Append("<cbc:ID>" & guia.NumDocumentoAdjunto & "</cbc:ID>")
            builder2.Append("<cbc:DocumentTypeCode>" & guia.TipoDocumentoAdjunto & "</cbc:DocumentTypeCode>")
            builder2.Append("<cbc:DocumentType>" & guia.DesCompAdjDocumentoAdjunto & "</cbc:DocumentType>")
            builder2.Append("<cac:IssuerParty>")
            builder2.Append("<cac:PartyIdentification>")
            lineadetalle = String.Concat("<cbc:ID schemeID=""", guia.TipoDocumentoEmisor, """ schemeAgencyName=""PE:SUNAT"" schemeURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"">", guia.NumeroDocumentoEmisor, "</cbc:ID>")
            builder2.Append(lineadetalle)
            builder2.Append("</cac:PartyIdentification>")
            builder2.Append("</cac:IssuerParty>")
            builder2.Append("</cac:AdditionalDocumentReference>")
        End If
        builder2.Append("<cac:Signature>")
        builder2.Append("<cbc:ID>IDSignKG</cbc:ID>")
        builder2.Append("<cac:SignatoryParty>")
        builder2.Append("<cac:PartyIdentification>")
        builder2.Append("<cbc:ID>" & guia.NumeroDocumentoEmisor & "</cbc:ID>")
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyName>")
        builder2.Append("<cbc:Name>" & guia.RazonSocialEmisor & "</cbc:Name>")
        builder2.Append("</cac:PartyName>")
        builder2.Append("</cac:SignatoryParty>")
        builder2.Append("<cac:DigitalSignatureAttachment>")
        builder2.Append("<cac:ExternalReference>")
        builder2.Append("<cbc:URI>SignatureSP</cbc:URI>")
        builder2.Append("</cac:ExternalReference>")
        builder2.Append("</cac:DigitalSignatureAttachment>")
        builder2.Append("</cac:Signature>")
        builder2.Append("<cac:DespatchSupplierParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        lineadetalle = String.Concat("<cbc:ID schemeID=""", guia.TipoDocumentoEmisor, """>", guia.NumeroDocumentoEmisor, "</cbc:ID>")
        builder2.Append(lineadetalle)
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & guia.RazonSocialEmisor & "]]></cbc:RegistrationName>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:DespatchSupplierParty>")
        builder2.Append("<cac:DeliveryCustomerParty>")
        builder2.Append("<cac:Party>")
        builder2.Append("<cac:PartyIdentification>")
        lineadetalle = String.Concat("<cbc:ID schemeID=""", guia.TipoDocumentoAdquirente, """>", guia.DocumentoAdquirente, "</cbc:ID>")
        builder2.Append(lineadetalle)
        builder2.Append("</cac:PartyIdentification>")
        builder2.Append("<cac:PartyLegalEntity>")
        builder2.Append("<cbc:RegistrationName><![CDATA[" & guia.RazonSocialAdquirente & "]]></cbc:RegistrationName>")
        builder2.Append("</cac:PartyLegalEntity>")
        builder2.Append("</cac:Party>")
        builder2.Append("</cac:DeliveryCustomerParty>")
        builder2.Append("<cac:Shipment>")
        builder2.Append("<cbc:ID>1</cbc:ID>")
        builder2.Append("<cbc:HandlingCode listAgencyName=""PE:SUNAT"" listName=""Motivo de traslado"" listURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo20"">" & guia.MotivoTraslado & "</cbc:HandlingCode>")
        builder2.Append("<cbc:GrossWeightMeasure unitCode=""KGM"">" & guia.PesoBrutoBienes.ToString("#0.000") & "</cbc:GrossWeightMeasure>")
        builder2.Append("<cbc:SpecialInstructions>SUNAT_Envio_IndicadorTrasladoVehiculoM1L</cbc:SpecialInstructions>")
        builder2.Append("<cac:ShipmentStage>")
        builder2.Append("<cbc:TransportModeCode listName=""Modalidad de traslado"" listAgencyName=""PE:SUNAT"" listURI=""urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo18"">" & guia.ModalidadTraslado & "</cbc:TransportModeCode>")
        builder2.Append("<cac:TransitPeriod>")
        builder2.Append("<cbc:StartDate>" & guia.FechaInicioTraslado & "</cbc:StartDate>")
        builder2.Append("</cac:TransitPeriod>")
        builder2.Append("</cac:ShipmentStage>")
        builder2.Append("<cac:Delivery>")
        builder2.Append("<cac:DeliveryAddress>")
        builder2.Append("<cbc:ID schemeName=""Ubigeos"" schemeAgencyName=""PE:INEI"">" & guia.UbigeoLlegada & "</cbc:ID>")
        builder2.Append("<cac:AddressLine>")
        builder2.Append("<cbc:Line>" & guia.DireccionLlegada & "</cbc:Line>")
        builder2.Append("</cac:AddressLine>")
        builder2.Append("</cac:DeliveryAddress>")
        builder2.Append("<cac:Despatch>")
        builder2.Append("<cac:DespatchAddress>")
        builder2.Append("<cbc:ID schemeName=""Ubigeos"" schemeAgencyName=""PE:INEI"">" & guia.UbigeoPartida & "</cbc:ID>")
        lineadetalle = String.Concat("<cbc:AddressTypeCode listID=""", guia.NumeroDocumentoEmisor, """ listAgencyName=""PE:SUNAT"" listName=""Establecimientos anexos"">" & guia.CodigoEstablecimientoPartida & "</cbc:AddressTypeCode>")
        builder2.Append(lineadetalle)
        builder2.Append("<cac:AddressLine>")
        builder2.Append("<cbc:Line>" & guia.DireccionPartida & "</cbc:Line>")
        builder2.Append("</cac:AddressLine>")
        builder2.Append("</cac:DespatchAddress>")
        builder2.Append("</cac:Despatch>")
        builder2.Append("</cac:Delivery>")
        builder2.Append("</cac:Shipment>")

        For Each detalle As DetalleGuia In guia.Detalle
            builder2.Append("<cac:DespatchLine>")
            builder2.Append("<cbc:ID>" & detalle.NumeroItem & "</cbc:ID>")
            lineadetalle = String.Concat("<cbc:DeliveredQuantity unitCode=""", detalle.UnidadMedida, """ unitCodeListID=""UN/ECE rec 20"" unitCodeListAgencyName=""United Nations Economic Commission for Europe"">", detalle.Cantidad.ToString("#0.00"), "</cbc:DeliveredQuantity>")
            builder2.Append(lineadetalle)
            builder2.Append("<cac:OrderLineReference><cbc:LineID>" & detalle.NumeroItem & "</cbc:LineID></cac:OrderLineReference>")
            builder2.Append("<cac:Item>")
            builder2.Append("<cbc:Description><![CDATA[" & detalle.DescripcionProducto & "]]></cbc:Description>")

            builder2.Append("<cac:SellersItemIdentification><cbc:ID>" & detalle.CodigoProducto & "</cbc:ID></cac:SellersItemIdentification>")
            builder2.Append("</cac:Item>")
            builder2.Append("</cac:DespatchLine>")
        Next
        builder2.Append("</DespatchAdvice>")
        builder.AppendLine(builder2.ToString())
        Return builder
    End Function
    Public Function ConsultaValidezComprobante(ByVal TipoComprobate As String, ByVal Serie As String,
                                               ByVal NumeroComprobate As Integer, ByVal FechaEmision As Date,
                                               ByVal Monto As Decimal, ByRef response As BEResponseValidezComprobante) As String
        Dim Mensaje As String = ""
        Try
            Dim oDatoRequest As New BERequestValidezComprobante
            oDatoRequest.numRuc = MDIPrincipal.NumeroRuc
            oDatoRequest.codComp = TipoComprobate
            oDatoRequest.numeroSerie = Serie
            oDatoRequest.numero = NumeroComprobate
            oDatoRequest.fechaEmision = FechaEmision
            oDatoRequest.monto = FormatNumber(Monto, 2)

            Mensaje = ""
            Dim ResponseDato As New BEResponseValidezComprobante
            Dim obj As New ComprobantesElectronicos
            Mensaje = obj.ConsultaValidez(MDIPrincipal.CodigoEmpresa, oDatoRequest, ResponseDato)
            If Mensaje = "" Then
                response = ResponseDato
            End If
        Catch ex As Exception
            Mensaje = "Presentacion-Util-FacturacionSunat MetodoEjecutado: ConsultaValidezComrpobante - " + ex.Message
        End Try
        Return Mensaje
    End Function
    Public Function CalcularHashArchivo(ByVal filename As String) As String
        Dim hash As HashAlgorithm = New SHA256Managed()
        Dim fs As FileStream = New FileStream(filename, FileMode.Open, FileAccess.Read)
        Dim byteArray As Byte() = hash.ComputeHash(fs)
        Dim sb As StringBuilder = New StringBuilder(byteArray.Length)
        For i = 0 To byteArray.Length - 1
            sb.Append(byteArray(i).ToString("X2"))
        Next
        Dim resul As String = sb.ToString()
        fs.Close()
        Return resul
    End Function
End Class
