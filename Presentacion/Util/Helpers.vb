﻿Imports MessagingToolkit.QRCode.Codec
Public Class Helpers
    Public Shared Function GetBytesFromPEM(ByVal pemString As String, ByVal type As emStringType) As Byte()
        Dim str As String
        Dim str2 As String
        If type = emStringType.Certificate Then
            str = "-----BEGIN CERTIFICATE-----"
            str2 = "-----END CERTIFICATE-----"
        Else
            If type <> emStringType.RsaPrivateKey Then
                Return Nothing
            End If
            str = "-----BEGIN RSA PRIVATE KEY-----"
            str2 = "-----END RSA PRIVATE KEY-----"
        End If
        Dim startIndex As Integer = pemString.IndexOf(str) + str.Length
        Return Convert.FromBase64String(pemString.Substring(startIndex, pemString.IndexOf(str2, startIndex) - startIndex))
    End Function
    Public Shared Function AlignBytes(ByVal inputBytes As Byte(), ByVal alignSize As Integer) As Byte()
        Dim length As Integer = inputBytes.Length
        If (alignSize = -1) OrElse (length >= alignSize) Then
            Return inputBytes
        End If
        Dim buffer As Byte() = New Byte(alignSize - 1) {}
        For i As Integer = 0 To length - 1
            buffer(i + (alignSize - length)) = inputBytes(i)
        Next
        Return buffer
    End Function
    Public Shared Function DecodeIntegerSize(ByVal rd As System.IO.BinaryReader) As Integer
        Dim num2 As Integer

        If rd.ReadByte() <> 2 Then
            Return 0
        End If
        Dim num As Byte = rd.ReadByte()
        If num = &H81 Then
            num2 = rd.ReadByte()
        ElseIf num <> 130 Then
            num2 = num
        Else
            Dim num3 As Byte = rd.ReadByte()
            Dim num4 As Byte = rd.ReadByte()
            Dim buffer1 As Byte() = New Byte() {num4, num3}
            num2 = BitConverter.ToUInt16(buffer1, 0)
        End If
        While rd.ReadByte() = 0
            num2 -= 1
        End While
        rd.BaseStream.Seek(-1L, System.IO.SeekOrigin.Current)
        Return num2
    End Function
    Public Function CreateQR(ByVal pcadena As String) As Byte()
        Dim imageBytes As Byte()
        Dim encoder As QRCodeEncoder = New QRCodeEncoder()
        Dim img As Bitmap = encoder.Encode(pcadena)
        Dim QR As Image = CType(img, Image)

        Using ms As IO.MemoryStream = New IO.MemoryStream()
            QR.Save(ms, System.Drawing.Imaging.ImageFormat.Png)
            imageBytes = ms.ToArray()
        End Using

        Return imageBytes
    End Function
End Class
