﻿Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Dispatcher
Imports System.Xml
Imports Microsoft.Web.Services3.Security.Tokens

Public Class PasswordDigestMessageInspector
    Implements IClientMessageInspector

    Private Usuario As String
    Private Password As String

    Public Sub New(username As String, password As String)
        Me.Usuario = username
        Me.Password = password
    End Sub

    Public Sub AfterReceiveReply(ByRef reply As Message, correlationState As Object) Implements IClientMessageInspector.AfterReceiveReply

    End Sub

    Public Function BeforeSendRequest(ByRef request As Message, channel As IClientChannel) As Object Implements IClientMessageInspector.BeforeSendRequest


        Dim token As UsernameToken = New UsernameToken(Me.Usuario, Me.Password, PasswordOption.SendPlainText)
        Dim securityToken As XmlElement = token.GetXml(New XmlDocument())

        Dim securityHeader As MessageHeader = MessageHeader.CreateHeader("Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", securityToken, False)
        request.Headers.Add(securityHeader)

        Return Convert.DBNull

    End Function

End Class
