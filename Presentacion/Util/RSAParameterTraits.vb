﻿Friend Class RSAParameterTraits
    Public size_Mod As Integer = -1
    Public size_Exp As Integer = -1
    Public size_D As Integer = -1
    Public size_P As Integer = -1
    Public size_Q As Integer = -1
    Public size_DP As Integer = -1
    Public size_DQ As Integer = -1
    Public size_InvQ As Integer = -1

    Public Sub New(ByVal modulusLengthInBits As Integer)
        Dim num As Integer = -1
        Dim num2 As Double = Math.Log(CDbl(modulusLengthInBits), 2.0)
        num = If((num2 <> (CInt(num2))), (CInt(Math.Pow(2.0, CDbl((CInt((num2 + 1.0))))))), modulusLengthInBits)

        If num = &H400 Then
            Me.size_Mod = &H80
            Me.size_Exp = -1
            Me.size_D = &H80
            Me.size_P = &H40
            Me.size_Q = &H40
            Me.size_DP = &H40
            Me.size_DQ = &H40
            Me.size_InvQ = &H40
        ElseIf num = &H800 Then
            Me.size_Mod = &H100
            Me.size_Exp = -1
            Me.size_D = &H100
            Me.size_P = &H80
            Me.size_Q = &H80
            Me.size_DP = &H80
            Me.size_DQ = &H80
            Me.size_InvQ = &H80
        ElseIf num = &H1000 Then
            Me.size_Mod = &H200
            Me.size_Exp = -1
            Me.size_D = &H200
            Me.size_P = &H100
            Me.size_Q = &H100
            Me.size_DP = &H100
            Me.size_DQ = &H100
            Me.size_InvQ = &H100
        End If
    End Sub
End Class
