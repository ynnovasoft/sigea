﻿Imports System.Globalization
Imports System.Text

Module Validadores
    Public Sub ValidaNumeros(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
    Public Sub ValidaEspacio(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsWhiteSpace(e.KeyChar) Then
            e.Handled = True
        Else
            e.Handled = False
        End If
    End Sub
    Public Sub ValidaSoloLetras(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsPunctuation(e.KeyChar) Then
            e.Handled = True
        ElseIf Char.IsSymbol(e.KeyChar) Then
            e.Handled = True
        ElseIf Char.IsSurrogate(e.KeyChar) Then
            e.Handled = True
        ElseIf Char.IsLowSurrogate(e.KeyChar) Then
            e.Handled = True
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = True
        ElseIf Char.IsWhiteSpace(e.KeyChar) Then
            e.Handled = True
        Else
            e.Handled = False
        End If
    End Sub
    Public Sub ValidaDecimales(ByVal e As System.Windows.Forms.KeyPressEventArgs, ByVal Txt As TextBox)
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf (e.KeyChar) = Chr(46) Then
            If InStr(Txt.Text, e.KeyChar) > 0 Then
                e.Handled = True
            Else
                e.Handled = False
            End If
        Else
            e.Handled = True
        End If
    End Sub
    Public Sub ValidaLetras(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
    Public Sub ValidaLetrasNumeros(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
    Public Sub ValidaSonidoEnter(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar = Chr(13) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub
    Sub AplicaIconoError(ByVal sender As Object, ByRef Errormensaje As ErrorProvider)
        If DirectCast(sender, TextBox).Text.Length > 0 Then
            Errormensaje.SetError(sender, "")
        Else
            Errormensaje.SetError(sender, "Este campo es obligatorio")
        End If
    End Sub
    Sub ValidaFormatoMonedaGot(ByRef Txt As TextBox, Optional ByVal nDecimales As Integer = 2)
        If IsError(Txt.Text) Then
            Txt.Text = "0.00"
        End If
        If IsDBNull(Txt.Text) Or IsNothing(Txt.Text) Then
            Txt.Text = "0.00"
        End If
        If CDbl(Txt.Text.Trim) <= 0 Then
            Txt.Clear()
        Else
            Txt.Text = FormatNumber(IIf(Txt.Text = "", 0, Txt.Text), nDecimales)
        End If
    End Sub
    Sub ValidaFormatoMonedaLost(ByRef Txt As TextBox, Optional ByVal nDecimales As Integer = 2)
        If IsError(Txt.Text) Then
            Txt.Text = "0.00"
        End If
        If IsDBNull(Txt.Text) Or IsNothing(Txt.Text) Then
            Txt.Text = "0.00"
        Else
            Txt.Text = FormatNumber(IIf(Txt.Text = "", 0, Txt.Text), nDecimales)
        End If
    End Sub
    Sub ValidaMoneda(ByRef Txt As TextBox, Optional ByVal nDecimales As Integer = 2)
        If IsError(Txt.Text) Then
            Txt.Text = "0.00"
        End If
        If IsDBNull(Txt.Text) Or IsNothing(Txt.Text) Then
            Txt.Text = "0.00"
        Else
            Txt.Text = FormatNumber(IIf(Txt.Text = "", 0, Txt.Text), nDecimales)
        End If
    End Sub
    Function ValidaAperturaCaja() As Boolean
        If MDIPrincipal.AperturaCaja = True Then
            ValidaAperturaCaja = True
        End If
        If MDIPrincipal.AperturaCaja = False Then
            MsgBox("Es necesario aperturar su CAJA para realizar cualquier operación relacionada al módulo de CAJA", MsgBoxStyle.Critical, "Aviso")
            ValidaAperturaCaja = False
        End If
        Return ValidaAperturaCaja
    End Function
    Function ValidaAperturaDia() As Boolean
        If MDIPrincipal.AperturaDia = 2 Then
            ValidaAperturaDia = True
        End If
        If MDIPrincipal.AperturaDia = 0 Then
            MsgBox("Es necesario aperturar el DÍA para realizar cualquier operación relacionada al DÍA", MsgBoxStyle.Critical, "Aviso")
            ValidaAperturaDia = False
        End If
        If MDIPrincipal.AperturaDia = 1 Then
            MsgBox("El DÍA aperturado en el sistema es menor al DÍA actual, es recomendable cerrar y apeturar el nuevo DÍA", MsgBoxStyle.Exclamation, "Aviso")
            ValidaAperturaDia = True
        End If
        Return ValidaAperturaDia
    End Function
    Public Function EliminaTildes(ByVal Dato As String) As String

        Dim stFormD As String = Dato.Normalize(NormalizationForm.FormD)
        Dim sb As New StringBuilder()
        For ich As Integer = 0 To stFormD.Length - 1
            Dim uc As UnicodeCategory = CharUnicodeInfo.GetUnicodeCategory(stFormD(ich))
            If uc <> UnicodeCategory.NonSpacingMark Then
                sb.Append(stFormD(ich))
            End If
        Next
        Return (sb.ToString().Normalize(NormalizationForm.FormC))
    End Function
End Module
